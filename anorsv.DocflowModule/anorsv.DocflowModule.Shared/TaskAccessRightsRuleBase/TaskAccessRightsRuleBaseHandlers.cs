﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.TaskAccessRightsRuleBase;

namespace anorsv.DocflowModule
{
  partial class TaskAccessRightsRuleBaseMembersSharedCollectionHandlers
  {

    public virtual void MembersAdded(Sungero.Domain.Shared.CollectionPropertyAddedEventArgs e)
    {
      _added.RightType = TaskAccessRightsRuleBaseMembers.RightType.Read;
    }
  }

  partial class TaskAccessRightsRuleBaseSharedHandlers
  {

    public virtual void IsAccessChanged(Sungero.Domain.Shared.BooleanPropertyChangedEventArgs e)
    {
      if(!Equals(e.NewValue, e.OldValue) && e.NewValue != null)
        _obj.State.Properties.Members.IsEnabled = e.NewValue.Value;
    }
  }

}