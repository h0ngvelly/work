﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.LNA;

namespace anorsv.DocflowModule
{
  partial class LNASharedHandlers
  {

    public virtual void ORDslineChanged(anorsv.DocflowModule.Shared.LNAORDslineChangedEventArgs e)
    {
      if (_obj.ORDsline != null)
      {
        var approvers = _obj.ORDsline.Approvers;
        _obj.ApproversORDsline.Clear();
        foreach(var approver in approvers)                    
          _obj.ApproversORDsline.AddNew().Approver = Recipients.As(approver.Approver);
      }
      else
        _obj.ApproversORDsline.Clear();
    }

    public override void SignESanorsvChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
    {
      base.SignESanorsvChanged(e);
      
      // Синхронизироват значения Подписать ЭП и Способ подписания
        _obj.SignESsline = (_obj.SignESanorsv.Equals(anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES)) ? (true) : (false);
    }

    public override void DocumentKindChanged(Sungero.Docflow.Shared.OfficialDocumentDocumentKindChangedEventArgs e)
    {
      if (e.NewValue != e.OldValue && e.NewValue != null && _obj.Topicanorsv != null)
        _obj.Topicanorsv = null;
      
      base.DocumentKindChanged(e);
      
      if (e.NewValue != null)
      {
        var docKind = anorsv.AnorsvMainSolution.DocumentKinds.As(e.NewValue);
              
        // Заполнить Подписанта по Основному подписанту из Вида документа
        _obj.OurSignatory = docKind.DocKindSignatoryanorsv;
        // Заполнить Способ подписания из Вида документа
        if (docKind.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignESanorsv))
          _obj.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES;
        if (docKind.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignOnPaper))
          _obj.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignOnPaper;
        
        // Для ЛНА кроме по общ.деятельности Направление деятельности скрыто, очистить значение
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        if (lnaGenActivitiesKind != null && e.NewValue != lnaGenActivitiesKind && _obj.ORDsline != null)
          _obj.ORDsline = null;
      }
    }

  }
}