using System;
using Sungero.Core;

namespace anorsv.DocflowModule.Constants
{
  public static class Module
  {
    #region Связи
    
    // Имя типа связи "Вводит в действие"
    [Sungero.Core.PublicAttribute]
    public const string PutIntoActionRelationName = "PutIntoAction";
    
    #endregion
    
    #region Типы документов
    [Public]
    public static class DocTypes
    {
      [Public]
      public static readonly Guid FederalProjectTypeGuid = Guid.Parse("6f54ecbb-31d6-48b5-935b-d19ecc946ddd");
      [Public]
      public static readonly Guid MemoTypeGUID = Guid.Parse("95af409b-83fe-4697-a805-5a86ceec33f5");
    }
    #endregion
    
    #region Виды документов
    [Public]
    public static class DocKinds
    {
      [Sungero.Core.Public]
      public static readonly Guid DocKind = Guid.Parse("14a59623-89a2-4ea8-b6e9-2ad4365f358c");
      [Public]
      public static readonly Guid OrderLNAImplKindGuid = Guid.Parse("695FB6A5-B1CD-4D72-B9C2-BAB8C9524433");
      [Public]
      public static readonly Guid FPSocialElevatorsKindGuid = Guid.Parse("C1DB07BA-D960-4181-A4F5-40A37A1C4105");
      [Public]
      public static readonly Guid FPYouthOfRussiaKindGuid = Guid.Parse("C21679A8-6D95-4FEC-9D5A-A9490F7CE9CB");
      [Public]
      public static readonly Guid FPArtificialIntelligenceKindGuid = Guid.Parse("CD527576-1414-4EAF-99ED-B370E55011D2");
      [Public]
      public static readonly Guid FPProfessionalismKindGuid = Guid.Parse("EA4B8C87-4337-4E81-9C91-EBCA905FA73E");
      [Public]
      public static readonly Guid LNAGeneralActivitiesKindGuid = Guid.Parse("EA4FD9E5-F87A-47F7-AFA7-A87E256EAD33");
    }
    #endregion
    
    #region Роли
    [Public]
    public static class Roles
    {
      [Public]
      public static class FederalProjectsCreatingRole
      {
        [Public]
        public const string RoleName = "АНОРСВ. Пользователи с правом создания Федеральных проектов";
        [Public]
        public const string RoleDescription = "Пользователи, имеющие право на создание документов с типом Федеральный проект";
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("0EFE04D8-1752-4390-A441-1C91C98F52E9");
      }
      [Public]
      public static class PZGD
      {
        [Public]
        public const string RoleName = "Первый заместитель генерального директора";
        [Public]
        public const string RoleDescription = "ПЗГД. Первый заместитель генерального директора.";
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("774B431D-3001-4638-B316-511405873AEC");
      }
      [Public]
      public static class GeneralDirectorAssistant
      {
        [Public]
        public const string RoleName = "Ассистент ГД";
        [Public]
        public const string RoleDescription = "Сотрудник, который оказывает техническую помощь в подписании документов ГД на сайте сервиса МЧД.";
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("4CE18013-7385-4665-9132-E5A1C8988A80");
      }
      [Public]
      public static class ContractLegalDepartmentManager
      {
        [Public]
        public const string RoleName = "Руководитель ДПО";
        [Public]
        public const string RoleDescription = "Сотрудник, указанный в поле Руководитель карточки подразделения «Договорно-правовой отдел».";
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("C3347D30-5114-4532-AE23-CBE4007D95D6");
      }
      [Public]
      public static class Lawyers
      {
        [Public]
        public const string RoleName = "Юристы";
        [Public]
        public const string RoleDescription = "Сотрудники, которым в карточке доверенности/электронной доверенности доступно действие Отозвать доверенность.";
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("9E3F8938-95BE-40C1-A887-72A0F0B22D5C");
      }

    }
    #endregion
    
    // Дата последнего обновления прав задач по регламенту.
    public const string LastAccessRightsApprovalTasksUpdateDate = "LastAccessRightsApprovalTasksUpdateDate";
    
    // Выдача прав на Задачи по регламенту.
    public const string GrantRightsMode = "GrantRightsMode";
    public const string GrantRightsModeByJob = "Job";
    public const string GrantRightsModeByAsyncHandler = "Async";
    
    [Sungero.Core.PublicAttribute]
    public const string DontUpdateModified = "DontUpdateModified";
    
    /// <summary>
    /// Отметка о выдаче прав на задачу по регламенту.
    /// </summary>
    public const string TaskAccessRights = "TaskAccessRights";
  }
}
