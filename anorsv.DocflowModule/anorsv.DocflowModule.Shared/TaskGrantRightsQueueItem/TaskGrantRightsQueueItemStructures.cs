﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.DocflowModule.Structures.TaskGrantRightsQueueItem
{
  /// <summary>
  /// Прокси класс для элемента очереди.
  /// </summary>  
  partial class ProxyQueueItem
  {
    public int Id { get; set; }
    
    public int TaskId { get; set; }
    
    public int AccessRightsRu { get; set; }
  }
}