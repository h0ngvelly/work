﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.DocflowModule.Structures.Module
{
  [Public]
  partial class ZipModel
  {
    public List<anorsv.DocflowModule.Structures.Module.IExportedFile> Files {get; set;}
  }
  
  /// <summary>
  /// 
  /// </summary>
  [Public]
  partial class ExportedFile
  {
    public byte[] Body {get; set;}
    
    public string FileName {get; set;}
    
    public DateTime DateTime {get; set;}
    
    public long Size {get; set;}
  }
}