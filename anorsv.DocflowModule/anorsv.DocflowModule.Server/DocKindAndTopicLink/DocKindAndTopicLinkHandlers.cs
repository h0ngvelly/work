﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.DocKindAndTopicLink;

namespace anorsv.DocflowModule
{
  partial class DocKindAndTopicLinkServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      _obj.Name = "Возможность создания " + _obj.CreatedDocKind.DisplayValue + " для " + _obj.MainDocKind.DisplayValue; 
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.FromAppTask = false;
      _obj.FromMainDoc = false;
    }
  }

}