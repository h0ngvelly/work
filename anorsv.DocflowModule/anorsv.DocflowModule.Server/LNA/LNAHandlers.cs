﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.LNA;

namespace anorsv.DocflowModule
{
  partial class LNACreatingFromServerHandler
  {

    public override void CreatingFrom(Sungero.Domain.CreatingFromEventArgs e)
    {
      e.Without(_info.Properties.Orderanorsv);
      base.CreatingFrom(e);
    }
  }

  partial class LNAORDslinePropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ORDslineFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(x => Equals(_obj.DocumentKind, x.DocumentKind));
      return query;
    }
  }

  partial class LNAAuthorPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> AuthorFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.AuthorFiltering(query, e);
      var users = Substitutions.ActiveSubstitutedUsers.ToList();
      if (!Users.Current.IncludedIn(Roles.Administrators) && users.Any())
      {
        users.Add(Users.Current);
        query = query.Where(x => users.Contains(Sungero.CoreEntities.Users.As(x)));
      }
      return query;
    }
  }

  partial class LNAServerHandlers
  {

    public override void BeforeSaveHistory(Sungero.Content.DocumentHistoryEventArgs e)
    {
      base.BeforeSaveHistory(e);
      
      var isChangeRelationAction = e.Action == Sungero.CoreEntities.History.Action.ChangeRelation;
      if (isChangeRelationAction)
      {
        if (e.OperationDetailed.ToString().Equals(anorsv.RelationModule.PublicConstants.Module.SystemOperations.RemoveRelation))
        {
          // При изменении связи проверить для ЛНА по общ.деятельности наличие обязательной связи
          var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid); 
          if (_obj.DocumentKind.Equals(lnaGenActivitiesKind))
          {
            if (_obj.LeadingDocument != null && _obj.Orderanorsv != null)  
              anorsv.RelationModule.PublicFunctions.Module.CheckSpecialRelations(_obj.Id, _obj.Orderanorsv.Id);
          }
        } 
      }
      
    }

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      _obj.SignESsline = true;
    }
  }

}