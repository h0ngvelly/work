﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule;
using anorsv.DocflowModule.Structures.Module;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace anorsv.DocflowModule.Server
{
  public class ModuleFunctions
  {

    /// <summary>
    /// Проверка возможности изменения вида документа
    /// Для всех пользователей - кроме Приказа о введении в действие ЛНА
    /// Для Администраторов, Аудиторов - кроме Приказа о введении в действие ЛНА, созданного из ЛНА
    /// </summary>
    [Public, Remote(IsPure = true)]
    public bool CanChangeDocumentKind(sline.RSV.IOrder order)
    {
      var canChange = true;
      
      var orderImplLNAKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);      
      if (orderImplLNAKind != null && order.DocumentKind != null && order.DocumentKind.Equals(orderImplLNAKind))
      {
        // Приказ о введении в действие ЛНА
        if (!Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Administrators) && !Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Auditors))
        {
          // Для всех пользователей - запрет на изменение вида приказа
          canChange = false;
        }
        else
        {
          // Для Администраторов и Аудиторов - проверим наличие связи с ЛНА
          var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);          
          if (lnaGenActivitiesKind != null)
          {
            var allLNAGeneralActivity = anorsv.DocflowModule.LNAs.GetAll()
              .Where(l => order.Relations.GetRelatedFrom(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).Contains(l))
              .Where(l => l.DocumentKind.Equals(lnaGenActivitiesKind))
              .ToList();
            
            // Если есть связи Введен в действие с ЛНА по общей деятельности организации - запрет на изменение вида приказа
            if (allLNAGeneralActivity.Any())
              canChange = false;
          }
        }
      }
      
      return canChange;
    }
    
    [Public, Remote(PackResultEntityEagerly = true)]
    public IApprovalTaskAccessRightsRule CreateApprovalTaskAccessRightsRule(sline.RSV.IApprovalRule rule)
    {
      var rightRule = anorsv.DocflowModule.ApprovalTaskAccessRightsRules.Create();
      rightRule.Name = rule.Name;
      foreach (var documentKind in rule.DocumentKinds.Select(d => d.DocumentKind))
        rightRule.DocumentKinds.AddNew().DocumentKind = documentKind;
      rightRule.ApprovalRule = rule;
      rightRule.Save();
      return rightRule;
    }
    
    /// <summary>
    /// Получить правило выдачи прав на задачу
    /// </summary>
    [Public, Remote(PackResultEntityEagerly = true)]
    public IApprovalTaskAccessRightsRule GetApprovalTaskAccessRightsRule(sline.RSV.IApprovalRule rule)
    {
      return ApprovalTaskAccessRightsRules.GetAll().FirstOrDefault(r => Equals(r.ApprovalRule, rule) && rule.Status == Sungero.Docflow.ApprovalRule.Status.Active);
    }
    
    /// <summary>
    /// Удалить элементы очереди.
    /// </summary>
    /// <param name="itemsIds">Элементы на удаление.</param>
    [Public]
    public static void FastDeleteQueueItems(List<int> itemsIds)
    {
      using (var command = SQL.GetCurrentConnection().CreateCommand())
      {
        command.CommandText = string.Format("delete from {0} where Id in ({1})",
                                            anorsv.DocflowModule.TaskGrantRightsQueueItems.Info.DBTableName, string.Join(", ", itemsIds));
        command.ExecuteNonQuery();
      }
    }
    
    /// <summary>
    /// Получить Guid типа прав.
    /// </summary>
    /// <param name="rightType">Перечисление с типом прав.</param>
    /// <returns>Guid типа прав.</returns>
    [Public]
    public static Guid GetRightTypeGuid(Enumeration? rightType)
    {
      if (rightType == anorsv.DocflowModule.TaskAccessRightsRuleBaseMembers.RightType.Read)
        return DefaultAccessRightsTypes.Read;

      if (rightType == anorsv.DocflowModule.TaskAccessRightsRuleBaseMembers.RightType.Edit)
        return DefaultAccessRightsTypes.Change;

      if (rightType == anorsv.DocflowModule.TaskAccessRightsRuleBaseMembers.RightType.FullAccess)
        return DefaultAccessRightsTypes.FullAccess;
      
      return new Guid();
    }
    
    /// <summary>
    /// Сгенерируем zip архив
    /// </summary>
    /// <param name="fileList">anorsv.DocflowModule.Structures.Module.IZipModel</param>
    /// <param name="level">уровень сжатия от 0 до 9</param>
    /// <returns>Stream</returns>
    [Public]
    public virtual string CreateZipArchive(anorsv.DocflowModule.Structures.Module.IZipModel fileList, int level, string zipName)
    {
      string filename = zipName + ".zip";
      
      MemoryStream outputMemStream = new MemoryStream();
      ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);
      
      zipStream.SetLevel(level);
      if(fileList.Files.Count > 0)
      {
        foreach (var file in fileList.Files)
        {
          ZipEntry newEntry = new ZipEntry(file.FileName);
          newEntry.DateTime = file.DateTime;
          
          zipStream.PutNextEntry(newEntry);

          byte[] buffer = new byte[4096];
          using (MemoryStream streamReader = new MemoryStream(file.Body))
          {
            StreamUtils.Copy(streamReader, zipStream, buffer);
          }
          
          zipStream.CloseEntry();
        }
        zipStream.IsStreamOwner = false;
        zipStream.Finish();
        zipStream.Close();
        
        outputMemStream.Position = 0;
        var defaultTestZipFromDb = Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.PublicConstants.Module.TestZip);
        var defaultTestZip = defaultTestZipFromDb != null ? defaultTestZipFromDb.ToString() : "";
        if(!string.IsNullOrEmpty(defaultTestZip) && Boolean.Parse(defaultTestZip))
        {
          var defaultTestZipURL = Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.PublicConstants.Module.TestZipURL).ToString();
          using (var fileStream = new FileStream(Path.Combine(defaultTestZipURL, Calendar.Today.Date.ToString(), Guid.NewGuid().ToString(), "test.zip"), FileMode.Create))
          {
            outputMemStream.Seek(0, SeekOrigin.Begin);
            outputMemStream.CopyTo(fileStream);
          }
        }
      }
      
      return Convert.ToBase64String(outputMemStream.ToArray());
    }

  }
}
