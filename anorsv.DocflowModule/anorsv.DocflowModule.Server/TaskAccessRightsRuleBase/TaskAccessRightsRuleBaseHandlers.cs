﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.TaskAccessRightsRuleBase;

namespace anorsv.DocflowModule
{
  partial class TaskAccessRightsRuleBaseServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.ApplyConfidentialDocuments = false;
      _obj.IsAccess = false;
    }
  }

}