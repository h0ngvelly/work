﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.DocflowModule.Server
{
  public class ModuleAsyncHandlers
  {

    public virtual void AsyncUpdateConfidentiality(anorsv.DocflowModule.Server.AsyncHandlerInvokeArgs.AsyncUpdateConfidentialityInvokeArgs args)
    {
      bool docKindConfidentiality = args.DocKindConf;
      bool needRetry = false;
      anorsv.OfficialDocument.IOfficialDocument document = anorsv.OfficialDocument.OfficialDocuments.Null;
      
      Logger.DebugFormat("AsyncUpdateConfidentiality: start for dockind Id={0}, docKindConfidentiality={1}", args.DocKindId, docKindConfidentiality);
      
      // Список ИД документов указанного вида
      // у которых конфиденциальность отличается от установленной в виде
      var documentIdArray = anorsv.OfficialDocument.OfficialDocuments.GetAll()
        .Where(d => d.DocumentKind.Id == args.DocKindId)
        .Where(d => d.ConfidentialDocumentanorsv != docKindConfidentiality)
        .Select(d => d.Id)
        .ToArray();
      
      // Изменить конфиденциальность в документах     
      foreach (var documentId in documentIdArray)
      {
        try
        {
          document = anorsv.OfficialDocument.OfficialDocuments.Get(documentId);
          document.ConfidentialDocumentanorsv = docKindConfidentiality;
          document.Save();
        }
        catch (Exception ex)
        {
          Logger.DebugFormat("AsyncUpdateConfidentiality: can not change confidentiality in document ID {0}. {1}", documentId, ex.Message);
          needRetry = true;
        }
      }
      
      args.Retry = needRetry;
    }

  }
}