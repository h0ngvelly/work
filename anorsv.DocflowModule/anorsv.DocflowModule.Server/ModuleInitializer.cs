using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Sungero.Commons;
using Sungero.Company;
using Sungero.Content;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.CoreEntities.RelationType;
using Sungero.Docflow;
using Sungero.Docflow.ApprovalStage;
using Sungero.Docflow.DocumentKind;
using Sungero.Docflow.OfficialDocument;
using Sungero.Domain;
using Sungero.Domain.Initialization;
using Sungero.Domain.Shared;
using Sungero.Workflow;

namespace anorsv.DocflowModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      FillAllowForConfidentialDocumentsProperty();
      FillConfidentialDocKindProperty();
      
      CreateDocumentTypes();
      CreateDocumentKinds();
      UpdateDocumentKinds();
      CreateRelationTypes();
      CreateFederalProjectsCreatingRole();
      GrantAccessRightsForDocuments();
      
      LNADefaultParams();
      ChangeLNADocTypes();
      ConvertDocuments();
      FillNullableBool();
      GrantAccessRightsTopic();
      SyncProjectsInAllAddendums();
      FillImplOrderInLNA();
      CreateRoles();
    }
    
    /// <summary>
    /// Задать параметры доступности функционала по умолчанию
    /// </summary>
    public static void LNADefaultParams()
    {
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable") == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("OrderAttGroupAvailable", "false");
    }
    
    /// <summary>
    /// Заполняет значением по умолчанию свойство AllowForConfidentialDocumentsanorsvProperty в справочнике AccessRightsRule.
    /// </summary>
    static void FillAllowForConfidentialDocumentsProperty()
    {
      InitializationLogger.Debug("Init: Fill empty AllowForConfidentialDocumentsanorsv property in AccessRightsRule.");
      
      foreach (var rule in anorsv.AnorsvMainSolution.AccessRightsRules.GetAll(r => !r.AllowForConfidentialDocumentsanorsv.HasValue))
      {
        rule.AllowForConfidentialDocumentsanorsv = false;
        rule.Save();
      }
    }
    
    /// <summary>
    /// Заполняет значением по умолчанию свойство Confidentialanorsv в справочнике DocumentKind.
    /// </summary>
    static void FillConfidentialDocKindProperty()
    {
      InitializationLogger.Debug("Init: Fill empty Confidentialanorsv property in DocumentKind.");
      
      foreach (var kind in anorsv.AnorsvMainSolution.DocumentKinds.GetAll(k => !k.Confidentialanorsv.HasValue))
      {
        kind.Confidentialanorsv = false;
        kind.Save();
      }
    }
    
    /// <summary>
    /// Создать типы документов
    /// </summary>
    public static void CreateDocumentTypes()
    {
      InitializationLogger.Debug("DocflowModule Init: create Local normative act documents type.");
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Локальный нормативный акт", LNA.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Inner, true);
      
      InitializationLogger.Debug("DocflowModule Init: create Federal project documents type.");
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Федеральный проект", FederalProject.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Inner, true);
    }
    
    /// <summary>
    /// Создать виды документов
    /// </summary>
    public static void CreateDocumentKinds()
    {
      var substActions = new Sungero.Domain.Shared.IActionInfo[] {
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendActionItem,
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendForApproval,
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendForFreeApproval,
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendForAcquaintance };
      
      InitializationLogger.Debug("DocflowModule Init: create document kind Приказ о введении в действие ЛНА по общей деятельности организации.");
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          Resources.LNAImplOrderKindName,
          Resources.LNAImplOrderKindShortName,
          Sungero.Docflow.DocumentKind.NumberingType.Registrable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Inner, true, false, anorsv.AnorsvMainSolution.Module.Docflow.PublicConstants.Module.OrderTypeGuid,
          substActions,
          anorsv.DocflowModule.Constants.Module.DocKinds.OrderLNAImplKindGuid,
          false
         );
      
      var substActionsFedProjects = new Sungero.Domain.Shared.IActionInfo[] {
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendActionItem,
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendForAcquaintance };
      
      InitializationLogger.Debug("DocflowModule Init: create document kind «Социальные лифты для каждого» н.п. «Образование».");
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          Resources.FPSocialElevatorsKindName,
          Resources.FPSocialElevatorsKindShortName,
          Sungero.Docflow.DocumentKind.NumberingType.NotNumerable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Inner,
          true,
          false,
          anorsv.DocflowModule.PublicConstants.Module.DocTypes.FederalProjectTypeGuid,
          substActionsFedProjects,
          anorsv.DocflowModule.Constants.Module.DocKinds.FPSocialElevatorsKindGuid
         );
      
      InitializationLogger.Debug("DocflowModule Init: create document kind «Развитие системы поддержки молодежи» («Молодежь России») н.п. «Образование».");
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          Resources.FPYouthOfRussiaKindName,
          Resources.FPYouthOfRussiaKindShortName,
          Sungero.Docflow.DocumentKind.NumberingType.NotNumerable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Inner,
          true,
          false,
          anorsv.DocflowModule.PublicConstants.Module.DocTypes.FederalProjectTypeGuid,
          substActionsFedProjects,
          anorsv.DocflowModule.Constants.Module.DocKinds.FPYouthOfRussiaKindGuid
         );
      
      InitializationLogger.Debug("DocflowModule Init: create document kind «Искусственный интеллект» н.п. «Цифровая экономика Российской Федерации».");
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          Resources.FPArtificialIntelligenceKindName,
          Resources.FPArtificialIntelligenceKindShortName,
          Sungero.Docflow.DocumentKind.NumberingType.NotNumerable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Inner,
          true,
          false,
          anorsv.DocflowModule.PublicConstants.Module.DocTypes.FederalProjectTypeGuid,
          substActionsFedProjects,
          anorsv.DocflowModule.Constants.Module.DocKinds.FPArtificialIntelligenceKindGuid
         );
      
      InitializationLogger.Debug("DocflowModule Init: create document kind «Профессионалитет».");
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          Resources.FPProfessionalismKindName,
          Resources.FPProfessionalismKindShortName,
          Sungero.Docflow.DocumentKind.NumberingType.NotNumerable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Inner,
          true,
          false,
          anorsv.DocflowModule.PublicConstants.Module.DocTypes.FederalProjectTypeGuid,
          substActionsFedProjects,
          anorsv.DocflowModule.Constants.Module.DocKinds.FPProfessionalismKindGuid
         );
      
    }
    
    /// <summary>
    /// Присвоить GUID ранее созданным вручную видам документов, используемых в вычислениях
    /// </summary>
    public static void UpdateDocumentKinds()
    {
      InitializationLogger.Debug("DocflowModule Init: update LNA kinds: Локальный нормативный акт по общей деятельности.");
      var lnaGenActivitiesEntityId = anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid;
      var lnaGenActivitiesExternalLink = Sungero.Docflow.PublicFunctions.Module.GetExternalLink(anorsv.DocflowModule.PublicConstants.Module.DocKinds.DocKind, lnaGenActivitiesEntityId);
      if (lnaGenActivitiesExternalLink == null)
      {
        var docKindLNAGenActivities = Sungero.Docflow.DocumentKinds
          .GetAll(k => k.Name.Equals("Локальный нормативный акт по общей деятельности")).FirstOrDefault();
        Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(docKindLNAGenActivities, lnaGenActivitiesEntityId);
      }
      
    }
    
    /// <summary>
    /// Выдать права на типы документов
    /// </summary>
    public void GrantAccessRightsForDocuments()
    {
      InitializationLogger.Debug("DocflowModule Init: Grant access rights for documents.");
      var allUsers = Roles.AllUsers;
      if (allUsers != null)
      {
        // документы
        InitializationLogger.Debug("Init: Grant access rights for Локальный нормативный акт.");
        if (!LNAs.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Create, allUsers))
        {
          LNAs.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Create);
          LNAs.AccessRights.Save();
        }
        
        InitializationLogger.Debug("Init: Grant access rights for Федеральный проект.");
        if (!FederalProjects.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Read, allUsers))
        {
          FederalProjects.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
          FederalProjects.AccessRights.Save();
        }
      }
      
      InitializationLogger.Debug("Init: Grant access rights to create for Федеральный проект.");
      IRole fpCreatingUsers  = Roles.GetAll(r => r.Sid == anorsv.DocflowModule.PublicConstants.Module.Roles.FederalProjectsCreatingRole.RoleGuid).FirstOrDefault();
      if (fpCreatingUsers != null && !FederalProjects.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Create, fpCreatingUsers))
      {
        FederalProjects.AccessRights.Grant(fpCreatingUsers, DefaultAccessRightsTypes.Create);
        FederalProjects.AccessRights.Save();
      }
    }
    
    /// <summary>
    /// Создать роли.
    /// </summary>
    public void CreateFederalProjectsCreatingRole()
    {
      var role = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(
        Constants.Module.Roles.FederalProjectsCreatingRole.RoleName,
        Constants.Module.Roles.FederalProjectsCreatingRole.RoleDescription,
        Constants.Module.Roles.FederalProjectsCreatingRole.RoleGuid
       );
      
      role.IsSystem = true;
      role.Save();
    }
    
    
    public void FillNullableBool()
    {
      var accessRights = ApprovalTaskAccessRightsRules.GetAll().Where(a => a.IsAccess == null);
      
      foreach (var accessRight in accessRights)
      {
        if (accessRight.IsAccess == null)
        {
          accessRight.IsAccess = false;
          accessRight.Save();
        }
      }
    }
    
    public void FillConfidentialDocumentsNullableBool()
    {
      var documents = OfficialDocument.OfficialDocuments.GetAll().Where(d => d.ConfidentialDocumentanorsv == null);
      int totalDocumentsCount = documents.Count();
      int lastCompliteCount = 0;
      int completedDocCount = 0;
      InitializationLogger.DebugFormat("Found {0} documents with null privacy value.", totalDocumentsCount);
      foreach (var document in documents)
      {
        var locks = Locks.GetLockInfo(document);
        if(locks.IsLocked && !locks.IsLockedByMe)
          Locks.Unlock(document);
        document.ConfidentialDocumentanorsv = false;
        document.Save();
        completedDocCount ++;
      }
      
      if (completedDocCount == lastCompliteCount + 100)
      {
        InitializationLogger.DebugFormat("Init: Succesfull update official document for {0} of {1}.", completedDocCount, totalDocumentsCount);
        lastCompliteCount = completedDocCount;
      }
      
      if(completedDocCount > lastCompliteCount)
        InitializationLogger.DebugFormat("Init: Succesfull update official document", completedDocCount, totalDocumentsCount);
      
    }
    
    public void GrantAccessRightsTopic()
    {
      var allUsers = Roles.AllUsers;
      // Тематики
      DocflowModule.AnorsvGenericTopics.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      DocflowModule.AnorsvGenericTopics.AccessRights.Save();
      
      // Настройки связи между тематиками
      DocflowModule.DocKindAndTopicLinks.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      DocflowModule.DocKindAndTopicLinks.AccessRights.Save();
      
    }
    
    public void ChangeLNADocTypes()
    {
      InitializationLogger.Debug("DocflowModule Init: change doc type in Document kinds.");
      
      // Изменить тип документа в карточках вида
      var docKindLNAList = Sungero.Docflow.DocumentKinds
        .GetAll(k => k.Name.Contains("Локальный нормативный акт")).ToList();
      var docTypeLNA = Sungero.Docflow.DocumentTypes
        .GetAll(t => t.DocumentTypeGuid == LNA.ClassTypeGuid.ToString()).FirstOrDefault();
      
      foreach (var docKindLNA in docKindLNAList)
      {
        if (!docKindLNA.DocumentType.Equals(docTypeLNA))
        {
          try
          {
            docKindLNA.DocumentType = docTypeLNA;
            docKindLNA.Save();
            InitializationLogger.Debug("DocflowModule Init: '" + docKindLNA.Name + "' kind changed.");
          }
          catch(Exception ex)
          {
            InitializationLogger.Debug("DocflowModule Init EXCEPTION: error in type changing for document kind " + docKindLNA.Name + ": " + ex.Message);
          }
        }
      }
    }
    
    public static void ConvertDocuments()
    {
      // Конвертировать документы из типа Приказ в тип ЛНА, если их вид был успешно связан с типом ЛНА
      var docTypeLNA = Sungero.Docflow.DocumentTypes
        .GetAll(t => t.DocumentTypeGuid == LNA.ClassTypeGuid.ToString())
        .FirstOrDefault();
      var docKindLNAList = Sungero.Docflow.DocumentKinds
        .GetAll(k => k.Name.Contains("Локальный нормативный акт"))
        .Where(k => k.DocumentType.Equals(docTypeLNA))
        .ToList();
      
      var documentIds = sline.RSV.Orders
        .GetAll(d => docKindLNAList.Contains(d.DocumentKind))
        .Select(d => d.Id)
        .ToList();
      
      int currentProcessedDocumentsCount = 0;
      int previousStepCount = 0;
      int totalDocumentsCount = documentIds.Count;
      int debugCount = 25;
      
      InitializationLogger.DebugFormat("DocflowModule Init: Convert documents. Total docs count = {0}.", totalDocumentsCount);
      
      foreach (var documentId in documentIds)
      {
        var document = sline.RSV.Orders.Get(documentId);
        var docKindLNA = document.DocumentKind;
        var convertedDocument = anorsv.DocflowModule.LNAs.As(document.ConvertTo(anorsv.DocflowModule.LNAs.Info));
        convertedDocument.DocumentKind = docKindLNA;
        try
        {
          convertedDocument.Save();
        }
        catch (Exception ex)
        {
          InitializationLogger.DebugFormat("ERROR: in document {0} convertation - {1} ", documentId, ex.Message);
        }
        currentProcessedDocumentsCount++;
        
        if (currentProcessedDocumentsCount - previousStepCount == debugCount)
        {
          previousStepCount = currentProcessedDocumentsCount;
          InitializationLogger.DebugFormat("Successfully converted {0} of {1} documents.", currentProcessedDocumentsCount, totalDocumentsCount);
        }
      }
      
      if (totalDocumentsCount > 0 && previousStepCount < currentProcessedDocumentsCount)
      {
        InitializationLogger.DebugFormat("Successfully converted {0} of {1} documents.", currentProcessedDocumentsCount, totalDocumentsCount);
      }
      
    }
    
    /// <summary>
    /// Создать типы связей.
    /// </summary>
    public static void CreateRelationTypes()
    {
      InitializationLogger.Debug("DocflowModule Init: Create relation type PutIntoAction.");
      
      // Вводит в действие
      var putIntoAction = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRelationType(Constants.Module.PutIntoActionRelationName,
                                                                                                  Resources.RelationLNASourceTitle,
                                                                                                  Resources.RelationLNATargetTitle,
                                                                                                  Resources.RelationLNASourceDescription,
                                                                                                  Resources.RelationLNATargetDescription, false, false, true, true);
      putIntoAction.IsSystem = false;
      
      putIntoAction.Mapping.Clear();
      var putIntoActionRow = putIntoAction.Mapping.AddNew();
      putIntoActionRow.Target = Sungero.RecordManagement.Orders.Info;
      putIntoActionRow.Source = anorsv.DocflowModule.LNAs.Info;
      putIntoAction.Save();
      
      InitializationLogger.Debug("DocflowModule Init: Create relation type Amending.");
      
      // Вносит изменения
      var amending = Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateRelationType(anorsv.OfficialDocumentModule.PublicConstants.Module.AmendingRelationName,
                            Resources.RelationAmendingSourceTitle,
                            Resources.RelationAmendingTargetTitle,
                            Resources.RelationAmendingSourceDescription,
                            Resources.RelationAmendingTargetDescription, false, false, true, false);
      amending.Mapping.Clear();
      var amendingRow1 = amending.Mapping.AddNew();
      amendingRow1.Source = Sungero.RecordManagement.Orders.Info;
      amendingRow1.Target = Sungero.RecordManagement.Orders.Info;
      var amendingRow2 = amending.Mapping.AddNew();
      amendingRow2.Source = anorsv.DocflowModule.LNAs.Info;
      amendingRow2.Target = Sungero.RecordManagement.Orders.Info;
      var amendingRow3 = amending.Mapping.AddNew();
      amendingRow3.Source = anorsv.DocflowModule.LNAs.Info;
      amendingRow3.Target = anorsv.DocflowModule.LNAs.Info;
      amending.Save();
    }
    
    /// <summary>
    /// Синхронизация проектов приложений с ведущим документом.
    /// </summary>
    public static void SyncProjectsInAllAddendums()
    {
      int completedAddendumCount = 0;
      int lastCompletedAddendumCount = 0;
      int totalAddendumCount = 0;
      
      InitializationLogger.DebugFormat("Init: Try synchronize addendum projects with leading document.");

      var addendums = sline.RSV.Addendums
        .GetAll(d => d.LeadingDocument != null
                && (d.ProjectsCollectionanorsv.Count() != anorsv.OfficialDocument.OfficialDocuments.As(d.LeadingDocument).ProjectsCollectionanorsv.Count()
                    || (d.ProjectsCollectionanorsv.Count() > 0
                        && anorsv.OfficialDocument.OfficialDocuments.As(d.LeadingDocument).ProjectsCollectionanorsv.Count() > 0
                        && d.ProjectsCollectionanorsv.Sum(p => p.Project.Id) != anorsv.OfficialDocument.OfficialDocuments.As(d.LeadingDocument).ProjectsCollectionanorsv.Sum(p => p.Project.Id))
                   )
               );
      
      totalAddendumCount = addendums.Count();
      
      foreach (var addendum in addendums)
      {
        anorsv.OfficialDocument.IOfficialDocument leadingDocument = anorsv.OfficialDocument.OfficialDocuments.As(addendum.LeadingDocument);
        addendum.ProjectsCollectionanorsv.Clear();
        foreach (var leadingDocumentprojectRow in leadingDocument.ProjectsCollectionanorsv)
          if (leadingDocumentprojectRow.Project != null)
            addendum.ProjectsCollectionanorsv.AddNew().Project = leadingDocumentprojectRow.Project;
        addendum.Save();
        
        Logger.DebugFormat(Resources.SyncProjectForAddendumDebugMessage, addendum.Id);
        
        completedAddendumCount++;
        
        if (completedAddendumCount == lastCompletedAddendumCount + 100)
        {
          InitializationLogger.DebugFormat("Init: Succesfull synchronized projects for {0} of {1} addendums.", completedAddendumCount, totalAddendumCount);
          lastCompletedAddendumCount = completedAddendumCount;
        }
      }

      if (completedAddendumCount > lastCompletedAddendumCount)
        InitializationLogger.DebugFormat("Init: Succesfull synchronized projects for {0} of {1} addendums.", completedAddendumCount, totalAddendumCount);
    }
    
    /// <summary>
    /// Заполнить новый реквизит Приказ и Ведущий документ
    /// Для ЛНА по общ.деятельности, связанных с Приказом о введении в действие спец.связью, у которых реквизит еще не заполнен
    /// </summary>
    public static void FillImplOrderInLNA()
    {
      var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
      var orderImplLNAKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
      
      var docLNAList = anorsv.DocflowModule.LNAs.GetAll()
        .Where(d => d.DocumentKind.Equals(lnaGenActivitiesKind))
        .ToList();
      
      foreach (var docLNA in docLNAList)
      {
        var docOrderList = sline.RSV.Orders.GetAll()
          .Where(o => docLNA.Relations.GetRelated(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).Contains(o))
          .Where(o => o.DocumentKind.Equals(orderImplLNAKind))
          .ToList();
        
        if (docOrderList.Any())
        {
          var docOrder = docOrderList.FirstOrDefault();
          docLNA.LeadingDocument = docOrder;
          docLNA.Orderanorsv = docOrder;
          docLNA.Save();
        }
      }
    }
    
    /// <summary>
    /// Создание предопределенных ролей.
    /// </summary>
    public void CreateRoles()
    {
      InitializationLogger.Debug("Init: Create default roles.");
      var admin = Users.GetAll(r => r.Id == 62).FirstOrDefault();
      // ПЗГД - с одним участником
      var role = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(Constants.Module.Roles.PZGD.RoleName,
                                                                                 Constants.Module.Roles.PZGD.RoleDescription,
                                                                                 Constants.Module.Roles.PZGD.RoleGuid);
      if (role != null && role.IsSingleUser != true && !role.RecipientLinks.Select(s => s.Member).Any() && admin != null)
      {
        role.IsSingleUser = true;
        role.RecipientLinks.AddNew().Member = admin;
        role.Save();
      }
      // Руководитель ДПО - с одним участником
      var managerRole = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(Constants.Module.Roles.ContractLegalDepartmentManager.RoleName,
                                                                                        Constants.Module.Roles.ContractLegalDepartmentManager.RoleDescription,
                                                                                        Constants.Module.Roles.ContractLegalDepartmentManager.RoleGuid);
      if (managerRole != null && managerRole.IsSingleUser != true && !managerRole.RecipientLinks.Select(s => s.Member).Any() && admin != null)
      {
        managerRole.IsSingleUser = true;
        managerRole.RecipientLinks.AddNew().Member = admin;
        managerRole.Save();
      }
      // Ассистент ГД
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(Constants.Module.Roles.GeneralDirectorAssistant.RoleName,
                                                                      Constants.Module.Roles.GeneralDirectorAssistant.RoleDescription,
                                                                      Constants.Module.Roles.GeneralDirectorAssistant.RoleGuid);
      // Юристы
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(Constants.Module.Roles.Lawyers.RoleName,
                                                                      Constants.Module.Roles.Lawyers.RoleDescription,
                                                                      Constants.Module.Roles.Lawyers.RoleGuid);
    }

  }
}
