﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Docflow;

namespace anorsv.DocflowModule.Server
{
  public class ModuleJobs
  {
    /// <summary>
    /// Получить из списка правил подходящие для задачи.
    /// </summary>
    /// <param name="task">Задача.</param>
    /// <param name="rules">Правила.</param>
    /// <returns>Подходящие правила.</returns>
    public static List<anorsv.DocflowModule.IApprovalTaskAccessRightsRule> GetAvailableRules(Sungero.Docflow.IApprovalTask task, List<anorsv.DocflowModule.IApprovalTaskAccessRightsRule> rules)
    {
      return rules
        //.Where(s => s.Status == DocflowModule.ApprovalTaskAccessRightsRule.Status.Active)
        .Where(s => s.ApprovalRule.Id == task.ApprovalRule.Id).ToList();
    }
    
    /// <summary>
    /// Выдать права на задачу по правилу.
    /// </summary>
    /// <param name="task">задача.</param>
    /// <param name="rule">Правило.</param>
    /// <returns>Возвращает true, если права удалось выдать, false - если надо повторить позже.</returns>
    public static bool TryGrantAccessRightsToTaskByRule(Sungero.Docflow.IApprovalTask task, IApprovalTaskAccessRightsRule rule)
    {
      Logger.DebugFormat("TryGrantAccessRightsToTaskByRule: task {0}, rule {1}", task.Id, rule.Id);
      
      var isChanged = false;
      
      foreach (var member in rule.Members)
      {
        var accessRights = anorsv.DocflowModule.PublicFunctions.Module.GetRightTypeGuid(member.RightType);
        if (!task.AccessRights.IsGrantedDirectly(accessRights, member.Recipient))
        {
          if (Locks.GetLockInfo(task).IsLockedByOther)
            return false;
          
          task.AccessRights.Grant(member.Recipient, accessRights);
          task.AccessRights.Save();
          Logger.Debug(anorsv.DocflowModule.Resources.RightsTaskFormat(task.Id, rule.Id));
        }
        
        foreach (var attachment in task.AllAttachments)
        {
          if(!attachment.AccessRights.IsGrantedDirectly(accessRights, member.Recipient))
          {
            var document = anorsv.OfficialDocument.OfficialDocuments.As(attachment);
            if(document != null)
            {
              if(document.ConfidentialDocumentanorsv != null && Equals(document.ConfidentialDocumentanorsv, rule.ApplyConfidentialDocuments.Value) && rule.DocumentKinds.Select(k => k.DocumentKind).Contains(document.DocumentKind))
              {
                attachment.AccessRights.Grant(member.Recipient, accessRights);
                attachment.AccessRights.Save();
                Logger.Debug(anorsv.DocflowModule.Resources.RightsDocumentFormat(attachment.Id, rule.Id));
              }
            }
          }
        }
        
        isChanged = true;
      }
      
      if (isChanged)
      {
        ((Sungero.Domain.Shared.IExtendedEntity)task).Params[Constants.Module.DontUpdateModified] = true;
      }
      
      return true;
    }
    
    /// <summary>
    /// Выдать права на задачу.
    /// </summary>
    /// <param name="queueItem">Элемент очереди.</param>
    /// <param name="allRules">Действующие правила выдачи прав.</param>
    /// <returns>True, если элемент очереди был успешно обработан.</returns>
    protected virtual bool GrantRightsToTaskByRules(ITaskGrantRightsQueueItem queueItem, List<IApprovalTaskAccessRightsRule> allRules)
    {
      var task = Sungero.Docflow.ApprovalTasks.GetAll(d => d.Id == queueItem.TaskId.Value).FirstOrDefault();
      if (task == null)
        return true;
      var rule = queueItem.AccessRightsRule;
      
      // Права на задачу.
      if (!TryGrantAccessRightsToTaskByRule(task, rule))
        return false;


      return true;
    }
    
    /// <summary>
    /// Создать элемент очереди выдачи прав.
    /// </summary>
    /// <param name="documentId">ИД задачи.</param>
    /// <param name="rule">Правило.</param>
    /// <param name="rightType">Тип элемента.</param>
    /// <returns>Структура для сохранения в таблицу очереди выдачи прав.</returns>
    private static Structures.TaskGrantRightsQueueItem.ProxyQueueItem CreateAccessRightsQueueItem(int taskId, IApprovalTaskAccessRightsRule accessRights)
    {
      Logger.DebugFormat("CreateAccessRightsQueueItem: task {0}, accessRights {1}", taskId, accessRights);
      var queueItem = Structures.TaskGrantRightsQueueItem.ProxyQueueItem.Create();
      queueItem.TaskId = taskId;
      queueItem.AccessRightsRu = accessRights.Id;
      return queueItem;
    }
    
    /// <summary>
    /// Пакетная запись структур в таблицу.
    /// </summary>
    /// <param name="table">Название таблицы.</param>
    /// <param name="structures">Структуры. Только простые структуры без сущностей.</param>
    [Public]
    public static void WriteStructuresToTable(string table, System.Collections.Generic.IEnumerable<anorsv.DocflowModule.Structures.TaskGrantRightsQueueItem.ProxyQueueItem> structures)
    {
      var list = structures.ToList();
      if (!list.Any())
        return;
      
      SQL.CreateBulkCopy().Write(table, list);
    }
    
    /// <summary>
    /// 
    /// </summary>
    public virtual void GrantAccessRightsApprovalTask()
    {
      var startDate = Calendar.Now;
      var lastStartDate = Sungero.Docflow.PublicFunctions.Module.GetLastAgentRunDate(Constants.Module.LastAccessRightsApprovalTasksUpdateDate);

      var allRules = ApprovalTaskAccessRightsRules.GetAll(s => s.Status == anorsv.DocflowModule.ApprovalTaskAccessRightsRule.Status.Active).ToList();
      if (!allRules.Any())
      {
        Sungero.Docflow.PublicFunctions.Module.UpdateLastAgentRunDate(Constants.Module.LastAccessRightsApprovalTasksUpdateDate, startDate);
        return;
      }
      
      // Измененные задачи.
      var queue = new List<anorsv.DocflowModule.Structures.TaskGrantRightsQueueItem.ProxyQueueItem>();
      var allRuleVer = new List<Sungero.Docflow.IApprovalRuleBase>();
      foreach (var rule in allRules)
      {
        if(rule.IsAccess.Value)
        {
          allRuleVer.Add(rule.ApprovalRule);
          var oldRule = rule.ApprovalRule.ParentRule;
          
          while (oldRule != null)
          {
            allRuleVer.Add(oldRule);
            oldRule = oldRule.ParentRule;
          }
          
          foreach (var ruleVer in allRuleVer)
          {
            var taskList = Sungero.Docflow.ApprovalTasks.GetAll(t => t.ApprovalRule == ruleVer);
            Logger.Debug(anorsv.DocflowModule.Resources.FoundTasksByRuleFormat(taskList.Count(), ruleVer.Id));
            foreach (var task in taskList)
            {
              queue.Add(CreateAccessRightsQueueItem(task.Id, rule));
            }
          }
        }
      }
      
      var table = TaskGrantRightsQueueItems.Info.DBTableName;
      var ids = Sungero.Domain.IdentifierGenerator.GenerateIdentifiers(table, queue.Count).ToList();
      for (int i = 0; i < queue.Count; i++)
        queue[i].Id = ids[i];
      Sungero.Docflow.PublicFunctions.Module.WriteStructuresToTable(table, queue);
      Logger.DebugFormat("GrantAccessRightsToTasks: Added to queue {0} task.", queue.Count);
      
      // Обновить дату запуска агента в базе.
      Sungero.Docflow.PublicFunctions.Module.UpdateLastAgentRunDate(Constants.Module.LastAccessRightsApprovalTasksUpdateDate, startDate);
      
      // Выдать права на pflfxb.
      var step = 5;
      var error = 0;
      var isEmpty = false;
      for (int i = 0; i < 10000; i = i + step)
      {
        // Если элементов больше нет - заканчиваем.
        if (isEmpty)
          break;
        
        var result = Transactions.Execute(
          () =>
          {
            Logger.DebugFormat("GrantAccessRightsToTask: Start process queue from {0}.", i);

            // Т.к. в конце транзакции элементы удаляются, в Take берем просто N новых элементов.
            var queueItemPart = TaskGrantRightsQueueItems.GetAll().Skip(error).Take(step).ToList();
            if (!queueItemPart.Any())
            {
              // Завершаем транзакцию, если больше нечего обрабатывать.
              isEmpty = true;
              return;
            }

            var accessRightsGranted = queueItemPart
              .Where(q => this.GrantRightsToTaskByRules(q, allRules))
              .ToList();
            if (accessRightsGranted.Any())
              Functions.Module.FastDeleteQueueItems(accessRightsGranted.Select(a => a.Id).ToList());
            error += queueItemPart.Count() - accessRightsGranted.Count();
          });
        if (!result)
          error += step;
      }
    }
  }
}
