﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.FederalProject;

namespace anorsv.DocflowModule
{
  partial class FederalProjectClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      _obj.State.Properties.Project.IsEnabled = true;
      _obj.State.Properties.Project.IsVisible = true;
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      _obj.State.Properties.Project.IsEnabled = true;
      _obj.State.Properties.Project.IsVisible = true;
    }

  }
}