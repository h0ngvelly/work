using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.FederalProject;

namespace anorsv.DocflowModule.Client
{

  partial class FederalProjectActions
  {
    public override void CreateFromTemplate(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.CreateFromTemplate(e);
    }

    public override bool CanCreateFromTemplate(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      //return base.CanCreateFromTemplate(e);
      return false;
    }

    public override void SendToCounterparty(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.SendToCounterparty(e);
    }

    public override bool CanSendToCounterparty(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      //return base.CanSendToCounterparty(e);
      return false;
    }

    public override void SendForReview(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.SendForReview(e);
    }

    public override bool CanSendForReview(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      //return base.CanSendForReview(e);
      return false;
    }

  }

}