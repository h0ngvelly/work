using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.AnorsvGenericTopic;

namespace anorsv.DocflowModule.Client
{
  partial class AnorsvGenericTopicFunctions
  {

    /// <summary>
    /// Выбор шаблона с фильтрацией по Тематике и признаку "Типовой".
    /// </summary>
    /// <param name="topic">Тематика.</param>
    /// <param name="isStandard">Типовой.</param>
    [Public]
    public static void SelectDocumentTemplate(anorsv.OfficialDocument.IOfficialDocument document, anorsv.DocflowModule.IAnorsvGenericTopic topic, bool? isStandard)
    {
      var templateList = new List<Sungero.Docflow.IDocumentTemplate>();
      
      if (isStandard  == true)
      {
        templateList = topic.Templates.Select(s => s.Template).ToList();
        if (!templateList.Any())
        {
          Dialogs.NotifyMessage(AnorsvGenericTopics.Resources.WarningTopicStandardTemplatesNotFound);
          return;
        }
      }
      else
      {
        templateList = topic.NonStandardTemplates.Select(s => s.NonStandardTemplates).ToList();
        if (!templateList.Any())
          {
          Dialogs.NotifyMessage(AnorsvGenericTopics.Resources.WarningTopicNonStandardTemplatesNotFound);
          return;
        }
      }
      templateList = templateList.Where(t => t.Status == Sungero.Docflow.DocumentTemplate.Status.Active).ToList();
      
      var dialog = Dialogs.CreateInputDialog(sline.RSV.Memos.Resources.ChooseTemplateDialogTitle);
      dialog.Text = sline.RSV.Memos.Resources.ChooseTemplateDialogText;
      
      var template = dialog.AddSelect(AnorsvGenericTopics.Resources.DialogDocumentTemplateLabel, true, Sungero.Docflow.DocumentTemplates.Null).From(templateList);
      
      if (templateList.Count() == 1)
        template.Value = templateList.FirstOrDefault();
      
      if (dialog.Show() == DialogButtons.Ok)
      {
        var chosedTemplate = Sungero.Content.ElectronicDocumentTemplates.As(template.Value);
        using (var body = chosedTemplate.LastVersion.Body.Read())
        {
          var newVersion = document.CreateVersionFrom(body, chosedTemplate.AssociatedApplication.Extension);
          var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)document;
          internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = chosedTemplate.Id;
          document.Save();
        }
      }
      else
        return;
    }
  }
}