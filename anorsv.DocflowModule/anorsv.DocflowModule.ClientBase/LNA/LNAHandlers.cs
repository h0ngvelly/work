﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.LNA;

namespace anorsv.DocflowModule
{
  partial class LNAClientHandlers
  {

    public virtual void DesiredDateRegslineanorsvValueInput(Sungero.Presentation.DateTimeValueInputEventArgs e)
    {
      
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      // Для ЛНА по общ.деятельности Направление деятельности обязательно для заполнения
      var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
      if (lnaGenActivitiesKind != null && _obj.DocumentKind != null)
      {
        bool canChangeORD = (_obj.DocumentKind.Equals(lnaGenActivitiesKind));
        _obj.State.Properties.ORDsline.IsRequired = canChangeORD;
        _obj.State.Properties.ORDsline.IsVisible = canChangeORD;
        _obj.State.Properties.ORDsline.IsEnabled = canChangeORD;
        
        // Предупреждение о невозможности изменения связей с Закупкой из карточки СЗ
        if (canChangeORD)
          e.AddInformation(anorsv.RelationModule.Resources.RelationProhibitionForImplOrder);
      }
      // Если Направление деятельности скрыли, то очищаем значение
      //if (_obj.State.Properties.ORDsline.IsVisible == false && _obj.ORDsline != null)
      //  _obj.ORDsline = null;
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      // Для ЛНА по общ.деятельности Направление деятельности обязательно для заполнения
      var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
      if (lnaGenActivitiesKind != null)
      {
        bool canChangeORD = (_obj.DocumentKind.Equals(lnaGenActivitiesKind));
        _obj.State.Properties.ORDsline.IsRequired = canChangeORD;
        _obj.State.Properties.ORDsline.IsVisible = canChangeORD;
        _obj.State.Properties.ORDsline.IsEnabled = canChangeORD;
        
        // Предупреждение о невозможности изменения связей с Закупкой из карточки СЗ
        if (canChangeORD)
          e.AddInformation(anorsv.RelationModule.Resources.RelationProhibitionForImplOrder);
      }
    }

  }
}