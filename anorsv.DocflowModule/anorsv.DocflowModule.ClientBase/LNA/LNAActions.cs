﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.DocflowModule.LNA;
using docRelation = anorsv.RelationModule;

namespace anorsv.DocflowModule.Client
{
  partial class LNAActions
  {
    public override void DeleteRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Только для ЛНА по общей деятельности
      var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
      if (lnaGenActivitiesKind != null && _obj.DocumentKind.Equals(lnaGenActivitiesKind))
      {
        var availableTypeList = RelationTypes.GetAll()
          .Where(r => r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName)
          .ToList();
        docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(_obj, availableTypeList);
      }
      else
      {
        base.DeleteRelationanorsv(e);
      }
      
    }

    public override bool CanDeleteRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanDeleteRelationanorsv(e);
    }

    public virtual void CreateOrder(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Принудительно сохранить документ для установления связи с приказом
      if (_obj.State.IsChanged)
        _obj.Save();
      
      // Создать приказ
      var order = sline.RSV.Orders.Create();
      order.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
      order.BusinessUnit = _obj.BusinessUnit;
      order.OurSignatory = _obj.OurSignatory;
      order.Project = _obj.Project;
      order.SignESanorsv = _obj.SignESanorsv;
      order.Subject = _obj.Subject;
      // Связать приказ с лна
      //order.Relations.AddFrom(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName, _obj);
      
      order.ShowModal();
      
      // Если Приказ сохранили
      if (sline.RSV.Orders.GetAll(d => d.Id == order.Id).Any())
      {
        _obj.LeadingDocument = order;
        _obj.Orderanorsv = order;
        _obj.Relations.Add(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, order);
        _obj.Save();
        
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(_obj.Id, order.Id);
      }
    }

    public virtual bool CanCreateOrder(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      var canCreateOrder = false;
      
      // Только для ЛНА по общей деятельности
      var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
      if (lnaGenActivitiesKind != null && _obj.DocumentKind.Equals(lnaGenActivitiesKind))
      {
        // Приказ о введении в действие ЛНА по общей деятельности организации, связанный с ЛНА связью Вводит в действие
        var orderImplLNAKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
        if (orderImplLNAKind != null)
        {
          var allOrderGeneralActivity = sline.RSV.Orders.GetAll()
            .Where(o => _obj.Relations.GetRelated(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).Contains(o))
            .Where(o => o.DocumentKind.Equals(orderImplLNAKind))
            .ToList();
          
          // Если нет связи Введен в действие с Приказом о введении в действие ЛНА по общей деятельности организации, можно создать новый
          if (!allOrderGeneralActivity.Any())
            canCreateOrder = true;
        }
      }
      
      return canCreateOrder;
    }

    public override void CreateManyAddendum(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.CreateManyAddendum(e);
    }

    public override bool CanCreateManyAddendum(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanCreateManyAddendum(e);
    }

    public override void SendForAcquaintance(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetAcqTasks(_obj);
      
      if (createdTasks)
      {
        e.AddError(anorsv.DocflowModule.Resources.AcquaintanceTaskExists);
        return;
      }
      else
      {
        base.SendForAcquaintance(e);
      }
    }

    public override bool CanSendForAcquaintance(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForAcquaintance(e);
    }

    public override void SendForReview(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetReviewTasks(_obj);
      
      if (createdTasks)
      {
        e.AddError(anorsv.DocflowModule.Resources.ReviewTaskExists);
        return;
      }
      else
      {
        base.SendForReview(e);
      }
    }

    public override bool CanSendForReview(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForReview(e);
    }

    public override void SendForFreeApproval(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetFreeTasks(_obj);
      
      if (createdTasks)
      {
        e.AddError(anorsv.DocflowModule.Resources.FreeApprovalTaskExists);
        return;
      }
      else
      {
        base.SendForFreeApproval(e);
      }
    }

    public override bool CanSendForFreeApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForFreeApproval(e);
    }

    public override void SendActionItem(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetActionTasks(_obj);
      
      if (createdTasks)
      {
        e.AddError(anorsv.DocflowModule.Resources.ActionTaskExists);
        return;
      }
      else
      {
        base.SendActionItem(e);
      }
    }

    public override bool CanSendActionItem(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendActionItem(e);
    }

    public override void SendForApproval(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetApprovalTasks(_obj);
      
      if (createdTasks)
      {
        e.AddError(anorsv.DocflowModule.Resources.ApprovalTaskExists);
        return;
      }
      else
      {
        // Принудительно сохранить документ
        _obj.Save();
      
        // Локальный нормативный акт по общей деятельности
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);        
        if (lnaGenActivitiesKind != null && _obj.DocumentKind.Equals(lnaGenActivitiesKind))
        {
          // Приказ о введении в действие ЛНА по общей деятельности организации, связанный с ЛНА связью Вводит в действие
          var orderImplLNAKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
          if (orderImplLNAKind != null)
          {
            var allOrderGeneralActivity = sline.RSV.Orders.GetAll()
              .Where(o => _obj.Relations.GetRelated(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).Contains(o))
              .Where(o => o.DocumentKind.Equals(orderImplLNAKind))
              .ToList();
            
            // Если нет связи Введен в действие с Приказом о введении в действие ЛНА по общей деятельности организации, не отправлять на согласование ЛНА
            if (!allOrderGeneralActivity.Any())
            {
              //e.AddError(anorsv.DocflowModule.LNAs.Resources.LNAWithoutOrderText);
              //return;
              
              // Заменить вывод сообщения об отсутствии Приказа на диалог создания Приказа
              var dialogCreateOrder = Dialogs.CreateConfirmDialog(anorsv.DocflowModule.LNAs.Resources.LNAWithoutOrderDialogText).Show();
              
              if (dialogCreateOrder)
              {
                // Создать приказ
                var order = sline.RSV.Orders.Create();
                order.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
                order.BusinessUnit = _obj.BusinessUnit;
                order.OurSignatory = _obj.OurSignatory;
                order.Project = _obj.Project;
                order.SignESanorsv = _obj.SignESanorsv;
                order.Subject = _obj.Subject;
                
                order.ShowModal();
                
                // Если Приказ сохранили
                if (sline.RSV.Orders.GetAll(d => d.Id == order.Id).Any())
                {
                  _obj.LeadingDocument = order;
                  _obj.Orderanorsv = order;
                  _obj.Relations.Add(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, order);
                  _obj.Save();
                  
                  anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(_obj.Id, order.Id);
                }
                else
                {
                  return;
                }
              }
              else
              {
                return;
              }
              
            }
          }
        }
        
        base.SendForApproval(e);
        
      }
    }

    public override bool CanSendForApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForApproval(e);
    }

  }

}