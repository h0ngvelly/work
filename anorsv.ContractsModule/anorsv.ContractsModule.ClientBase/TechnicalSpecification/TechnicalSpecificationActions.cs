﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ContractsModule.TechnicalSpecification;

namespace anorsv.ContractsModule.Client
{
  partial class TechnicalSpecificationActions
  {
    public override void CreateFromTemplate(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (_obj.GeneralTopic != null)
      {
        if (_obj.IsStandard == true)
          anorsv.DocflowModule.PublicFunctions.AnorsvGenericTopic.SelectDocumentTemplate(_obj, _obj.GeneralTopic, true);
        else
          anorsv.DocflowModule.PublicFunctions.AnorsvGenericTopic.SelectDocumentTemplate(_obj, _obj.GeneralTopic, false);
      }
      else
        base.CreateFromTemplate(e);
    }

    public override bool CanCreateFromTemplate(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanCreateFromTemplate(e);
    }

  }

}