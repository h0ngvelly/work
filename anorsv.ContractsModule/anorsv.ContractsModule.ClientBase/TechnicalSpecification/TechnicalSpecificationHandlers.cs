﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ContractsModule.TechnicalSpecification;

namespace anorsv.ContractsModule
{
  partial class TechnicalSpecificationClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      _obj.State.Properties.DocumentGroup.IsVisible = false;
      
      var techSpecKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
      if (_obj.DocumentKind != null && _obj.DocumentKind == techSpecKind)
      {
        _obj.State.Properties.Purchaseanorsv.IsVisible = true;
        // Предупреждение о невозможности изменения связей с Закупкой из карточки ТЗ
        if (_obj.Purchaseanorsv == null)
          e.AddInformation(anorsv.RelationModule.Resources.RelationProhibitionForContractPurchase);
      }
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      _obj.State.Properties.DocumentGroup.IsVisible = false;
      
      var techSpecKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
      if (_obj.DocumentKind != null && _obj.DocumentKind == techSpecKind)
      {
        _obj.State.Properties.Purchaseanorsv.IsVisible = true;
        // Предупреждение о невозможности изменения связей с Закупкой из карточки ТЗ
        if (_obj.Purchaseanorsv == null)
          e.AddInformation(anorsv.RelationModule.Resources.RelationProhibitionForContractPurchase);
      }
    }

  }
}