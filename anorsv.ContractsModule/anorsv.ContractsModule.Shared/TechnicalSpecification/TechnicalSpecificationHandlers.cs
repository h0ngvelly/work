﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ContractsModule.TechnicalSpecification;

namespace anorsv.ContractsModule
{
  partial class TechnicalSpecificationSharedHandlers
  {

    public virtual void SubjectTSChanged(Sungero.Domain.Shared.StringPropertyChangedEventArgs e)
    {
      FillName();
    }

  }
}