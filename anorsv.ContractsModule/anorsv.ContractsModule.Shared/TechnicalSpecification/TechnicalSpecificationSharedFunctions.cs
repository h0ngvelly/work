﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ContractsModule.TechnicalSpecification;

namespace anorsv.ContractsModule.Shared
{
  partial class TechnicalSpecificationFunctions
  {

    public override void ChangeDocumentPropertiesAccess(bool isEnabled, bool isRepeatRegister)
    {
      base.ChangeDocumentPropertiesAccess(isEnabled, isRepeatRegister);
      
      //Если ТЗ по закупке
      var dockind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
      if (_obj.DocumentKind == dockind)
      {
        //сделать признак Типовой недостпуным для редактирования
        _obj.State.Properties.IsStandard.IsEnabled = false;
      }
    }
    
    /// <summary>
    /// Заполнить имя.
    /// </summary>
    public override void FillName()
    {
      if (_obj.DocumentKind != null)
      {
        // Формат имени ТЗ должен строиться: “Техническое задание ПО [“Предмет ТЗ“] “
        var technicalSpecificationKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
        if(_obj.DocumentKind == technicalSpecificationKind)
        {
          _obj.Name = anorsv.ContractsModule.TechnicalSpecifications.Resources.TSpurchase + _obj.SubjectTS;
        }
        else
        {
          base.FillName();
        }
      }
      
    }
  }
}