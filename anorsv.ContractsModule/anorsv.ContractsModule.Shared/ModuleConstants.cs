﻿using System;
using Sungero.Core;

namespace anorsv.ContractsModule.Constants
{
  public static class Module
  {
    public static class Initialize
    {
      [Sungero.Core.Public]
      public static readonly Guid AgreementKind = Guid.Parse("D7331C3D-5EE5-4743-AFCE-1066117AB59E");
      [Sungero.Core.Public]
      public static readonly Guid RoadmapKind = Guid.Parse("B711B221-CF90-4FE7-B8EE-39C55962E638");
      [Sungero.Core.Public]
      public static readonly Guid SupAgreementType = Guid.Parse("265f2c57-6a8a-4a15-833b-ca00e8047fa5");
      [Sungero.Core.Public]
      public static readonly Guid TechnicalSpecificationKind = Guid.Parse("3090F1B8-07D2-4B2F-9625-CAC56B98DB8E");

    }

  }
}