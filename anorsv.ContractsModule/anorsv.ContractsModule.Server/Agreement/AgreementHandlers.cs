﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ContractsModule.Agreement;

namespace anorsv.ContractsModule
{
  partial class AgreementAuthorPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> AuthorFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.AuthorFiltering(query, e);
      // Перенесено из перекрытия sline
      var users = Substitutions.ActiveSubstitutedUsers.ToList();
      if (!Users.Current.IncludedIn(Roles.Administrators) && users.Any())
      {
        users.Add(Users.Current);
        query = query.Where(x => users.Contains(Sungero.CoreEntities.Users.As(x)));
      }
      return query;
    }
  }

}