﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ContractsModule.TechnicalSpecification;

namespace anorsv.ContractsModule
{
  partial class TechnicalSpecificationGeneralTopicPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> GeneralTopicFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      if (_obj.DocumentKind != null)
        query = query.Where(q => q.DocumentKind == _obj.DocumentKind)
          .Where(q => q.Status == anorsv.DocflowModule.AnorsvGenericTopic.Status.Active);
      
      return query;
    }
  }

  partial class TechnicalSpecificationServerHandlers
  {

    public override void BeforeSaveHistory(Sungero.Content.DocumentHistoryEventArgs e)
    {
      base.BeforeSaveHistory(e);
      
      var isChangeRelationAction = e.Action == Sungero.CoreEntities.History.Action.ChangeRelation;
      if (isChangeRelationAction)
      {
        if (e.OperationDetailed.ToString().Equals(anorsv.RelationModule.PublicConstants.Module.SystemOperations.RemoveRelation))
        {
          // При изменении связи проверить для ТЗ наличие обязательной связи
          var techSpecKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
          if (_obj.DocumentKind.Equals(techSpecKind))
          {
            if (_obj.LeadingDocument != null && _obj.Purchaseanorsv != null)
              anorsv.RelationModule.PublicFunctions.Module.CheckSpecialRelations(_obj.Purchaseanorsv.Id, _obj.Id);
          }
        }
      }
      
    }
  }

}