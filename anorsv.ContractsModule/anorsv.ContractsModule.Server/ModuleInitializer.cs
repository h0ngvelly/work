﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Content;
using Sungero.Core;
using Sungero.Docflow;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;
using Sungero.Domain.Shared;
using Init = anorsv.ContractsModule.Constants.Module.Initialize;

namespace anorsv.ContractsModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      ContractDefaultParams();
      CreateDocumentTypes();
      CreateDocumentKinds();
      GrantAccessRights();
    }
    
    /// <summary>
    /// Задать параметры доступности функционала по умолчанию
    /// </summary>
    public static void ContractDefaultParams()
    {
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable") == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("ContractAttGroupAvailable", "false");
    }
    
    /// <summary>
    /// Создать типы документов
    /// </summary>
    public static void CreateDocumentTypes()
    {
      InitializationLogger.Debug("ContractsModule Init: create Contracts types.");
      
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentType(Resources.AgreementTypeName, Agreement.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Contracts, true);
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentType(anorsv.ContractsModule.Resources.TechnicalSpecification, TechnicalSpecification.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Contracts, true);
    }
    
    /// <summary>
    /// Создать виды документов
    /// </summary>
    public static void CreateDocumentKinds()
    {
      var substActions = new Sungero.Domain.Shared.IActionInfo[] {
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendActionItem,
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendForApproval,
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendForAcquaintance };
      
      InitializationLogger.Debug("ContractsModule Init: create Соглашение о сотрудничестве kind.");
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          Resources.AgreementKindName,
          Resources.AgreementKindShortName,
          Sungero.Docflow.DocumentKind.NumberingType.Registrable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Contracts, true, false, Agreement.ClassTypeGuid,
          substActions,
          Init.AgreementKind
         );
      
      InitializationLogger.Debug("ContractsModule Init: create Дорожная карта взаимодействия с регионами/Губернаторами kind.");
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          Resources.RoadmapKindName,
          Resources.RoadmapKindShortName,
          Sungero.Docflow.DocumentKind.NumberingType.Registrable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Contracts, true, false, Init.SupAgreementType, 
          substActions,
          Init.RoadmapKind
         );
      InitializationLogger.Debug("ContractsModule Init: create Техническое задание по закупкеkind.");
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          anorsv.ContractsModule.Resources.TechSpetification,
          anorsv.ContractsModule.Resources.TechSpetification,
          Sungero.Docflow.DocumentKind.NumberingType.Registrable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Contracts, true, false, TechnicalSpecification.ClassTypeGuid,
          substActions,
          Init.TechnicalSpecificationKind
         );
    }
    
    public static void GrantAccessRights()
    {
      var allUsers = Roles.AllUsers;
      if (allUsers != null)
      {
        if (!anorsv.Contracts.SupAgreements.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Create, allUsers))
        {
          InitializationLogger.Debug("ContractsModule Init: Grant access rights for SupAgreement to all users.");
          anorsv.Contracts.SupAgreements.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
          anorsv.Contracts.SupAgreements.AccessRights.Save();
        }

        if (!anorsv.ContractsModule.Agreements.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Create, allUsers))
        {
          InitializationLogger.Debug("ContractsModule Init: Grant access rights for Agreement  to all users.");
          anorsv.ContractsModule.Agreements.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
          anorsv.ContractsModule.Agreements.AccessRights.Save();
        }
        if (!anorsv.ContractsModule.TechnicalSpecifications.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Create, allUsers))
        {
          InitializationLogger.Debug("ContractsModule Init: Grant access rights for TechnicalSpecification to all users.");
          anorsv.ContractsModule.Agreements.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
          anorsv.ContractsModule.Agreements.AccessRights.Save();
        }
      }
      
      var contractsResponsible = Roles.GetAll().Where(n => n.Sid == Sungero.Docflow.PublicConstants.Module.RoleGuid.ContractsResponsible).FirstOrDefault();
      if (contractsResponsible != null)
      { 
        InitializationLogger.Debug("ContractsModule Init: Grant rights on document to responsible managers.");
        anorsv.ContractsModule.Agreements.AccessRights.Grant(contractsResponsible, DefaultAccessRightsTypes.Create);
        anorsv.ContractsModule.Agreements.AccessRights.Save();
        anorsv.Contracts.SupAgreements.AccessRights.Grant(contractsResponsible, DefaultAccessRightsTypes.Create);    
        anorsv.Contracts.SupAgreements.AccessRights.Save();
      }
    }
    
  }
}
