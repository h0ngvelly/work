using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Content;
using Sungero.Domain.Shared;

namespace anorsv.ServiceLibrary.Shared
{
  public class ModuleFunctions
  {
    /// <summary>
    /// Добавляет или удаляем префикс сигнализирующий о наличии подзадач у задания, уведомления или задаяния на приёмку.
    /// </summary>
    /// <param name="assignment">Задание, уведомление или задание на приёмку</param>
    [Public]
    public static void MarkSubtaskExistInAssignmentSubject(Sungero.Workflow.IAssignmentBase assignment)
    {
      string prefix = sline.CustomModule.Settings.GetAll().FirstOrDefault().SubtasksPrefix;
      string subject = String.Empty;
      bool needUpdateSubject = false;
      
      if (assignment.Subtasks.Any())
      {
        if (!assignment.Subject.StartsWith(prefix))
        {
          if ((prefix.Length + assignment.Subject.Length) > 245)
          {
            string newSubject = prefix + assignment.Subject.Remove(assignment.Subject.Length - prefix.Length - 1);
            
            if (!assignment.Subject.Equals(newSubject))
            {
              needUpdateSubject = true;
            }
          }
          else
          {
            string newSubject = prefix + assignment.Subject;
            
            if (!assignment.Subject.Equals(newSubject))
            {
              needUpdateSubject = true;
            }
          }
        }
      }
      else
      {
        if (assignment.Subject.StartsWith(prefix))
        {
          needUpdateSubject = true;
        }
      }
      
      if (needUpdateSubject)
      {
        var updateAssigment = anorsv.TaskModule.AsyncHandlers.UpdateAssigmentSubject.Create();
        updateAssigment.assigmentId = assignment.Id;
        updateAssigment.ExecuteAsync();
      }
    }
    
    /// <summary>
    /// Добавить перенос в конец строки, если она не пуста.
    /// </summary>
    /// <param name="row">Строка.</param>
    /// <returns>Результирующая строка.</returns>
    [Public]
    public static string AddEndOfLine(string row)
    {
      return string.IsNullOrWhiteSpace(row) ? string.Empty : row + Environment.NewLine;
    }
    
    [Public]
    public static void SetAccessRightForAllUsersOnDocument(Sungero.Content.IElectronicDocument document)
    {
      
    }

    /// <summary>
    /// Получить локализованное имя результата согласования по подписи.
    /// </summary>
    /// <param name="signature">Подпись.</param>
    /// <param name="emptyIfNotValid">Вернуть пустую строку, если подпись не валидна.</param>
    /// <returns>Локализованный результат подписания.</returns>
    [Public]
    public static string GetEndorsingResultFromSignature(Sungero.Domain.Shared.ISignature signature, bool emptyIfNotValid)
    {
      var result = string.Empty;
      
      if (emptyIfNotValid && !signature.IsValid)
        return result;
      
      switch (signature.SignatureType)
      {
        case SignatureType.Approval:
          result = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormApproved;
          break;
        case SignatureType.Endorsing:
          result = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormEndorsed;
          break;
        case SignatureType.NotEndorsing:
          result = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormNotEndorsed;
          break;
      }
      
      return result;
    }

    /// <summary>
    /// Получить ключ для подписи.
    /// </summary>
    /// <param name="signature">Подпись.</param>
    /// <param name="versionNumber">Номер версии.</param>
    /// <returns>Ключ для подписи.</returns>
    [Public]
    public static string GetSignatureKey(Sungero.Domain.Shared.ISignature signature, int versionNumber)
    {
      // Если подпись не "несогласующая", она должна схлапываться вне версий.
      if (signature.SignatureType != SignatureType.NotEndorsing)
        versionNumber = 0;
      
      if (signature.Signatory != null)
      {
        if (signature.SubstitutedUser != null && !signature.SubstitutedUser.Equals(signature.Signatory))
          return string.Format("{0} - {1}:{2}:{3}", signature.Signatory.Id, signature.SubstitutedUser.Id, signature.SignatureType == SignatureType.Approval, versionNumber);
        else
          return string.Format("{0}:{1}:{2}", signature.Signatory.Id, signature.SignatureType == SignatureType.Approval, versionNumber);
      }
      else
        return string.Format("{0}:{1}:{2}", signature.SignatoryFullName, signature.SignatureType == SignatureType.Approval, versionNumber);
    }
    
    #region Работа со строками
    
    /// <summary>
    /// Привести строку к формату "{Ф}{ормат}"
    /// </summary>
    /// <param name="label">Исходная строка которую нужно приветси к формату "{Ф}{ормат}"</param>
    /// <returns>Результирующая строка</returns>
    [Public]
    public static string FormatPrintableName(string label)
    {
      string result;
      if (string.IsNullOrWhiteSpace(label))
      {
        result =  string.Empty;
      }
      else
      {
        result = string.Format(
          "{0}{1}"
          , char.ToUpper(label[0])
          , label.Length > 1 ? label.Substring(1).ToLower() : string.Empty);
      }
      
      return result;
    }
    
    #endregion

    #region Упаковка и распаковка в строку словаря
    
    /// <summary>
    /// Упаковка словаря в строку для передачи через Remote функции.
    /// </summary>
    /// <param name="source">Словарь.</param>
    /// <returns>Строка с упакованным словарем.</returns>
    [Public]
    public static string BoxToString(System.Collections.Generic.Dictionary<string, int> source)
    {
      string result = string.Empty;
      var valueDelimiter = "-";
      var rowDelimiter = "|";
      
      result = string.Join(rowDelimiter, source.Select(d => string.Format("{0}{2}{1}", d.Key, d.Value, valueDelimiter)));

      return result;
    }
    
    /// <summary>
    /// Распаковка словаря из строки.
    /// </summary>
    /// <param name="source">Строка с упакованным словарем.</param>
    /// <returns>Словарь.</returns>
    [Public]
    public static System.Collections.Generic.Dictionary<string, int> UnboxDictionary(string source)
    {
      var valueDelimiter = '-';
      var rowDelimiter = '|';
      
      System.Collections.Generic.Dictionary<string, int> dictionary = new System.Collections.Generic.Dictionary<string, int>();
      foreach (var row in source.Split(rowDelimiter))
      {
        var key = row.Split(valueDelimiter)[0];
        var value = int.Parse(row.Split(valueDelimiter)[1]);
        dictionary.Add(key, value);
      }
      
      return dictionary;
    }
    
    #endregion
  }
}