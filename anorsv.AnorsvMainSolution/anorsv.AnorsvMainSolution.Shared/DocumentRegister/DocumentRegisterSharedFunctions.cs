﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DocumentRegister;

namespace anorsv.AnorsvMainSolution.Shared
{
  partial class DocumentRegisterFunctions
  {

    public new static string GetDepartmentValidationError(Sungero.Docflow.IDocumentRegister documentRegister, Sungero.Docflow.IOfficialDocument document)
    {
      // Проверить необходимость кода подразделения.
      if (documentRegister == null || !documentRegister.NumberFormatItems.Any(n => n.Element == Sungero.Docflow.DocumentRegisterNumberFormatItems.Element.DepartmentCode))
        return string.Empty;
      
      // Проверить наличие подразделения.
      if (document.Department == null)
        return anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.CreateValidationError(documentRegister, Sungero.Docflow.Resources.FillDepartment);
      
      // Проверить наличие кода у подразделения.
      if (string.IsNullOrWhiteSpace(document.Department.Code))
        return anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.CreateValidationError(documentRegister, Sungero.Docflow.Resources.FillDepartmentCode);
      
      return string.Empty;
    }
    
    public new static string GetBusinessUnitValidationError(Sungero.Docflow.IDocumentRegister documentRegister,  Sungero.Docflow.IOfficialDocument document)
    {
      // Проверить необходимость кода НОР.
      if (documentRegister == null || !documentRegister.NumberFormatItems.Any(n => n.Element == Sungero.Docflow.DocumentRegisterNumberFormatItems.Element.BUCode))
        return string.Empty;
      
      // Проверить наличие НОР.
      if (document.BusinessUnit == null)
        return anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.CreateValidationError(documentRegister, Sungero.Docflow.Resources.FillBusinessUnit);
      
      // Проверить наличие кода у НОР.
      if (string.IsNullOrWhiteSpace(document.BusinessUnit.Code))
        return anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.CreateValidationError(documentRegister, Sungero.Docflow.Resources.FillBusinessUnitCode);
      
      return string.Empty;
    }
    
    public new static string GetDocumentKindValidationError(Sungero.Docflow.IDocumentRegister documentRegister,  Sungero.Docflow.IOfficialDocument document)
    {
      // Проверить необходимость кода вида документа.
      if (documentRegister == null || !documentRegister.NumberFormatItems.Any(n => n.Element == Sungero.Docflow.DocumentRegisterNumberFormatItems.Element.DocKindCode))
        return string.Empty;
      
      // Проверить наличие вида документа.
      if (document.DocumentKind == null)
        return anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.CreateValidationError(documentRegister, Sungero.Docflow.Resources.FillDocumentKind);
      
      // Проверить наличие кода у вида документа.
      if (string.IsNullOrWhiteSpace(document.DocumentKind.Code))
        return anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.CreateValidationError(documentRegister, Sungero.Docflow.Resources.FillDocumentKindCode);
      
      return string.Empty;
    }

    [Public]
    public override string CheckRegistrationNumberFormat(Nullable<DateTime> registrationDate, string registrationNumber, string departmentCode, string businessUnitCode, string caseFileIndex, string docKindCode, string counterpartyCode, string leadDocNumber, bool searchCorrectingPostfix)
    {
      return base.CheckRegistrationNumberFormat(registrationDate, registrationNumber, departmentCode, businessUnitCode, caseFileIndex, docKindCode, counterpartyCode, leadDocNumber, searchCorrectingPostfix);
    }
    
    [Public]
    public override string CheckDocumentRegisterSections(Sungero.Docflow.IOfficialDocument document)
    {
      //return base.CheckDocumentRegisterSections(document);
      
      var departmentValidationErrors = anorsv.AnorsvMainSolution.Functions.DocumentRegister.GetDepartmentValidationError(_obj, document);
      if (!Equals(departmentValidationErrors, string.Empty))
        return departmentValidationErrors;
      
      var businessUnitValidationErrors = anorsv.AnorsvMainSolution.Functions.DocumentRegister.GetBusinessUnitValidationError(_obj, document);
      if (!Equals(businessUnitValidationErrors, string.Empty))
        return businessUnitValidationErrors;
      
      var docKindCodeValidationErrors = anorsv.AnorsvMainSolution.Functions.DocumentRegister.GetDocumentKindValidationError(_obj, document);
      if (!Equals(docKindCodeValidationErrors, string.Empty))
        return docKindCodeValidationErrors;
      
      return string.Empty;
    
    }
    
    /// <summary>
    /// Сформировать ошибку валидации.
    /// </summary>
    /// <param name="documentRegister">Журнал регистрации.</param>
    /// <param name="errorModel">Шаблон ошибки.</param>
    /// <returns>Ошибка валидации.</returns>
    [Public]
    public new static string CreateValidationError(Sungero.Docflow.IDocumentRegister documentRegister, string errorModel)
    {
      if (documentRegister.RegisterType == Sungero.Docflow.DocumentRegister.RegisterType.Numbering)
      {
        return string.Format(errorModel, Sungero.Docflow.Resources.numberWord);
      }
      else
      {
        return string.Format(errorModel, Sungero.Docflow.Resources.registerWord);
      }
    }

    /// <summary>
    /// Получить журнал по умолчанию для документа.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="filteredDocRegisters">Список доступных журналов.</param>
    /// <param name="settingType">Тип настройки.</param>
    /// <returns>Журнал.</returns>
    [Public]
    public new static IDocumentRegister GetDefaultDocRegister(Sungero.Docflow.IOfficialDocument document, List<Sungero.Docflow.IDocumentRegister> filteredDocRegisters, Enumeration? settingType)
    {
      var defaultDocRegister = DocumentRegisters.Null;

      if (document == null)
        return defaultDocRegister;

      var registrationSetting = Sungero.Docflow.PublicFunctions.RegistrationSetting.GetSettingByDocument(document, settingType);
      if (registrationSetting != null && filteredDocRegisters.Contains(registrationSetting.DocumentRegister))
        return anorsv.AnorsvMainSolution.DocumentRegisters.As(registrationSetting.DocumentRegister);
      
      var personalSettings = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(null);
      if (personalSettings != null)
      {
        var documentKind = document.DocumentKind;

        if (documentKind.DocumentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Incoming)
          defaultDocRegister = anorsv.AnorsvMainSolution.DocumentRegisters.As(personalSettings.IncomingDocRegister);
        if (documentKind.DocumentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Outgoing)
          defaultDocRegister = anorsv.AnorsvMainSolution.DocumentRegisters.As(personalSettings.OutgoingDocRegister);
        if (documentKind.DocumentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Inner)
          defaultDocRegister = anorsv.AnorsvMainSolution.DocumentRegisters.As(personalSettings.InnerDocRegister);
        if (documentKind.DocumentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Contracts)
          defaultDocRegister = anorsv.AnorsvMainSolution.DocumentRegisters.As(personalSettings.ContractDocRegister);
      }

      if (defaultDocRegister == null || !filteredDocRegisters.Contains(defaultDocRegister) || defaultDocRegister.Status != Sungero.CoreEntities.DatabookEntry.Status.Active)
      {
        defaultDocRegister = filteredDocRegisters.Count() > 1 ? DocumentRegisters.Null : DocumentRegisters.As(filteredDocRegisters.FirstOrDefault());
      }
      
      return defaultDocRegister;
    }
  }
}