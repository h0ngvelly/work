﻿using System;
using Sungero.Core;

namespace anorsv.AnorsvMainSolution.Module.Docflow.Constants
{
  public static class Module
  {
    // Уведомления о завершении срока исполнения поручения.
    public const string ExecutionOrderLastNotificationKey = "ExecutionOrderLastNotificationKey";
    public const string ExecutionOrderTableName = "Sungero_Docflow_ExecuteOrder";
    
    #region Типы документов
    [Sungero.Core.Public]
    public static readonly Guid OrderTypeGuid = Guid.Parse("9570e517-7ab7-4f23-a959-3652715efad3");
    
    #endregion
    
  }
}