﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ApprovalRuleBase;

namespace anorsv.AnorsvMainSolution.Shared
{
  partial class ApprovalRuleBaseFunctions
  {

    [Public]
    public int? SLGetNextStageNumber(Sungero.Docflow.IOfficialDocument document, Nullable<int> currentStageNumber, Sungero.Docflow.IApprovalTask task)
    {
      var number = this.GetNextStageNumber(document, currentStageNumber, task).Number;
      return number;
    }

  }
}