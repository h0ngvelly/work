﻿using System;
using Sungero.Core;

namespace anorsv.AnorsvMainSolution.Module.ProjectPlanner.Constants
{
  public static class Module
  {
    public static class Initialize
    {
      [Sungero.Core.Public]
      public static readonly Guid CustomerRequirementsKind = Guid.Parse("FC5B2F85-548D-4DE0-B1E9-66C873932111");
    }
  }
}