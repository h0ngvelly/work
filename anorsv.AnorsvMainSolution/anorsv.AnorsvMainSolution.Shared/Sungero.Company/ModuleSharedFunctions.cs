﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.Company.Shared
{
  partial class ModuleFunctions
  {
    /// <summary>
    /// Пользователям отобразить только заявки на замещения, где они замещающие/замещаемые.
    /// Администраторам отобразить все заявки на замещения.
    /// </summary>
    [Public]
    public virtual System.Linq.IQueryable<anorsv.AnorsvSubstitution.ISubstitutionRequestBase> GetVisibleSubstitutions(Sungero.Company.IEmployee employee = null)
    {
      if (Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Administrators))
        return anorsv.AnorsvSubstitution.SubstitutionRequestBases.GetAll();
      
      if (employee == null)
        employee = Sungero.Company.Employees.Current;
      
      // Для пользователя без сотрудника не отображаем
      if (employee == null)
        return null;
      
      return anorsv.AnorsvSubstitution.SubstitutionRequestBases.GetAll().Where(q => q.Substitute.Equals(employee) || q.Employee.Equals(employee));
    }

  }
}