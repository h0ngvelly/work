﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.AcquaintanceTask;

namespace anorsv.AnorsvMainSolution.Shared
{
  partial class AcquaintanceTaskFunctions
  {

    /// <summary>
    /// Добавить в Кроме роль "Исключенные из общей рассылки на ознакомление"
    /// </summary>       
    public void AddExcludedPerformersRole()
    {
       var excludedRole = Sungero.CoreEntities.Recipients.GetAll().Where(r => r.Name.Equals("Исключенные из общей рассылки на ознакомление")).FirstOrDefault();
       if (excludedRole != null && !_obj.ExcludedPerformers.Any(p => p.ExcludedPerformer.Equals(excludedRole)))
       {
        var addExcludedRole = _obj.ExcludedPerformers.AddNew();
        addExcludedRole.ExcludedPerformer = excludedRole;
       }
    }

  }
}