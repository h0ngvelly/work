﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.AcquaintanceTask;

namespace anorsv.AnorsvMainSolution
{
  partial class AcquaintanceTaskSharedHandlers
  {

    public override void DocumentGroupAdded(Sungero.Workflow.Interfaces.AttachmentAddedEventArgs e)
    {
      base.DocumentGroupAdded(e);
      
      //Если задача на ознакомления Приказа о введнии в действие ЛНА, автоматически добавлять во вложения связанный с приказом ЛНА по общей деятельности
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      var orderKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
      if (document.Relations.GetRelatedFrom(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).Any())
      {
        var docLNA = document.Relations.GetRelatedFrom(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).FirstOrDefault();
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        if (anorsv.OfficialDocument.OfficialDocuments.As(docLNA).DocumentKind == lnaGenActivitiesKind)
          _obj.OtherGroup.All.Add(docLNA);
      }
    }
  }

  partial class AcquaintanceTaskPerformersSharedHandlers
  {

    public override void PerformersPerformerChanged(Sungero.RecordManagement.Shared.AcquaintanceTaskPerformersPerformerChangedEventArgs e)
    {
      base.PerformersPerformerChanged(e);
      
      if (e.NewValue != null && e.NewValue.Equals(Roles.AllUsers))
        anorsv.AnorsvMainSolution.Functions.AcquaintanceTask.AddExcludedPerformersRole(anorsv.AnorsvMainSolution.AcquaintanceTasks.As(_obj.AcquaintanceTask));
    }
  }

}