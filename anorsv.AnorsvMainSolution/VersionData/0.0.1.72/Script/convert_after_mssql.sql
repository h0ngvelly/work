﻿declare @fk_name sysname = (select [CONSTRAINT_NAME] from [INFORMATION_SCHEMA].[KEY_COLUMN_USAGE] where [TABLE_NAME] like N'Sungero_WF_Task' and [COLUMN_NAME] like N'RedirectToslin_RSV_sline');

declare @sql_delete_fk_key nvarchar(4000);

if @fk_name is not null
begin
  set @sql_delete_fk_key = N'alter table dbo.Sungero_WF_Task drop constraint ' + @fk_name + ';';
  execute (@sql_delete_fk_key);
end;

set @fk_name = (select [CONSTRAINT_NAME] from [INFORMATION_SCHEMA].[KEY_COLUMN_USAGE] where [TABLE_NAME] like N'Sungero_WF_Assignment' and [COLUMN_NAME] like N'Redirectsline_RSV_sline');

if @fk_name is not null
begin
  set @sql_delete_fk_key = N'alter table dbo.Sungero_WF_Assignment drop constraint ' + @fk_name + ';';
  execute (@sql_delete_fk_key);
end;

if exists (select * from [INFORMATION_SCHEMA].[COLUMNS] where [TABLE_NAME] = N'Sungero_WF_Task' and [COLUMN_NAME] = N'RedirectToanor_AnorsvM_anorsv')
  and exists (select * from [INFORMATION_SCHEMA].[COLUMNS] where [TABLE_NAME] = N'Sungero_WF_Task' and [COLUMN_NAME] = N'RedirectToslin_RSV_sline')
begin
  update
    [Sungero_WF_Task]
  set
    [RedirectToanor_AnorsvM_anorsv] = [RedirectToslin_RSV_sline];

  alter table [Sungero_WF_Task] drop column [RedirectToslin_RSV_sline];
end;

if exists (select * from [INFORMATION_SCHEMA].[COLUMNS] where [TABLE_NAME] = N'Sungero_WF_Assignment' and [COLUMN_NAME] = N'Redirectsline_RSV_sline')
  and exists (select * from [INFORMATION_SCHEMA].[COLUMNS] where [TABLE_NAME] = N'Sungero_WF_Assignment' and [COLUMN_NAME] = N'RedirectToanor_AnorsvM_anorsv')
begin
  update
    [Sungero_WF_Assignment]
  set
    [RedirectToanor_AnorsvM_anorsv] = [Redirectsline_RSV_sline];

  alter table [Sungero_WF_Assignment] drop column [Redirectsline_RSV_sline];
end;

