﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DocumentReviewTask;

namespace anorsv.AnorsvMainSolution
{
  partial class DocumentReviewTaskServerHandlers
  {

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      base.Saving(e);
      
      // Почистим группы вложений от дублей
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.AddendaGroup);
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.OtherGroup);
    }
  }


}