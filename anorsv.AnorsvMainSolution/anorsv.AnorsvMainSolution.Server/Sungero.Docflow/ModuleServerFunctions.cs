using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV;

namespace anorsv.AnorsvMainSolution.Module.Docflow.Server
{
  partial class ModuleFunctions
  {   
    
    #region Рассылка уведомлений
    
    /// <summary>
    /// Получить параметры по умолчанию для рассылки уведомлений по документам.
    /// </summary>
    /// <param name="lastNotificationParamName">Имя параметра в Sungero_Docflow_Params с датой последнего уведомления.</param>
    /// <param name="noticesTableName">Имя таблицы, в которой содержится информация об уведомлениях.</param>
    /// <returns>Параметры для рассылки уведомлений по документам.</returns>
    //    [Public]
    //    public static IExpiringDocsNotificationParams GetDefaultExpiringTasksNotificationParams(string lastNotificationParamName,
    //                                                                                           string noticesTableName)
    //    {
    //      var param = ExpiringDocsNotificationParams.Create();
    //      param.LastNotification = PublicFunctions.Module.GetLastNotificationDate(lastNotificationParamName, null);
    //      param.LastNotificationReserve = param.LastNotification.AddDays(-2);
    //      param.Today = Calendar.Today;
    //      param.TodayReserve = param.Today.AddDays(2);
    //      param.BatchCount = 100;
    //      param.ExpiringDocTableName = noticesTableName;
    //      param.LastNotificationParamName = lastNotificationParamName;
    //      param.TaskParams = ExpiringNotificationTaskParams.Create();
//
    //      return param;
    //    }
    
    /// <summary>
    /// Добавить в таблицу для отправки задачу, с указанием документа.
    /// </summary>
    /// <param name="expiringDocsTableName">Имя таблицы для отправки уведомлений.</param>
    /// <param name="document">Документ.</param>
    /// <param name="task">Задача, которая была запущена.</param>
    [Public]
    public static void AddTaskToExpiringTasksTable(string expiringDocsTableName, int executionTask, int task)
    {
      if (string.IsNullOrWhiteSpace(expiringDocsTableName))
        return;
      var command = string.Format(Queries.Module.AddTaskExpiringTaskTable, expiringDocsTableName, executionTask, task);
      Sungero.Docflow.PublicFunctions.Module.ExecuteSQLCommand(command);
      Logger.DebugFormat("Task {0} for task {1} started and marked in db.", task, executionTask);
    }
    
    /// <summary>
    /// Получить задачи, по которым уже отправлены уведомления.
    /// </summary>
    /// <param name="expiringDocumentTableName">Имя таблицы, в которой хранятся Id задач для завершения.</param>
    /// <returns>Список Id задач, по которым задачи уже отправлены.</returns>
    [Public]
    public static List<int> GetTasksWithSendedTask(string expiringDocumentTableName)
    {
      var result = new List<int>();
      var commandText = string.Format(Queries.Module.SelectTaskWithSendedTask, expiringDocumentTableName);
      using (var command = SQL.GetCurrentConnection().CreateCommand())
      {
        try
        {
          command.CommandText = commandText;
          using (var rdr = command.ExecuteReader())
          {
            while (rdr.Read())
              result.Add(rdr.GetInt32(0));
          }
          return result;
        }
        catch (Exception ex)
        {
          Logger.Error("Error while getting array of docs with sent tasks", ex);
          return result;
        }
      }
    }
    
    /// <summary>
    /// Убрать из таблицы для отправки Id задач.
    /// </summary>
    /// <param name="expiringDocsTableName">Имя таблицы для отправки уведомлений.</param>
    /// <param name="ids">Id задач.</param>
    [Public]
    public static void ClearIdsFromExpiringTasksTable(string expiringDocsTableName, List<int> ids)
    {
      if (string.IsNullOrWhiteSpace(expiringDocsTableName))
        return;
      if (!ids.Any())
        return;
      string command = string.Empty;
      command = string.Format(Queries.Module.DeleteTaskIdsWithoutTask, expiringDocsTableName, string.Join(", ", ids));
      Sungero.Docflow.PublicFunctions.Module.ExecuteSQLCommand(command);
    }
    
    /// <summary>
    /// Очистить таблицу для отправки уведомлений.
    /// </summary>
    /// <param name="expiringDocsTableName">Имя таблицы для отправки уведомлений.</param>
    /// <param name="taskIsNull">True - очистить записи с неотправленными задачами,
    /// False - очистить записи с отправленными задачами.</param>
    [Public]
    public static void ClearExpiringTasksTable(string expiringDocsTableName, bool taskIsNull)
    {
      string command = string.Empty;
      
      if (taskIsNull)
        command = string.Format(Queries.Module.ClearExpiringTableWithoutTasks, expiringDocsTableName);
      else
        command = string.Format(Queries.Module.ClearExpiringTableWithTasks, expiringDocsTableName);
      
      Sungero.Docflow.PublicFunctions.Module.ExecuteSQLCommand(command);
    }
    
    /// <summary>
    /// Записать Id задач в таблицу для отправки.
    /// </summary>
    /// <param name="expiringDocsTableName">Имя таблицы для отправки уведомлений.</param>
    /// <param name="ids">Id документов.</param>
    [Public]
    public static void AddExpiringTasksToTable(string expiringDocsTableName, List<int> ids)
    {
      if (string.IsNullOrWhiteSpace(expiringDocsTableName))
        return;
      if (!ids.Any())
        return;
      var command = string.Format(Queries.Module.AddExpiringTasksToTable, expiringDocsTableName, string.Join("), (", ids));
      Sungero.Docflow.PublicFunctions.Module.ExecuteSQLCommand(command);
    }
    #endregion
    
    /// <summary>
    /// Сравнить два типа прав.
    /// </summary>
    /// <param name="type1">Тип 1.</param>
    /// <param name="type2">Тип 2.</param>
    /// <returns>1 - тип 1 больше типа 2. 0 - типы равны. -1 - тип 1 меньше типа 2.</returns>
    [Public]
    public override int CompareInstanceAccessRightsTypes(System.Guid type1, System.Guid type2)
    {
      // Если передали тип прав, характерный для типа в целом, а не для экземпляра - падать.
      if (type1 == DefaultAccessRightsTypes.Create)
        throw AppliedCodeException.Create(Sungero.RecordManagement.Resources.UnsupportedAccessRightsTypeMessageFormat(nameof(type1), "Create"));
      if (type2 == DefaultAccessRightsTypes.Create)
        throw AppliedCodeException.Create(Sungero.RecordManagement.Resources.UnsupportedAccessRightsTypeMessageFormat(nameof(type2), "Create"));
      if (type1 == DefaultAccessRightsTypes.Approve)
        throw AppliedCodeException.Create(Sungero.RecordManagement.Resources.UnsupportedAccessRightsTypeMessageFormat(nameof(type1), "Approve"));
      if (type2 == DefaultAccessRightsTypes.Approve)
        throw AppliedCodeException.Create(Sungero.RecordManagement.Resources.UnsupportedAccessRightsTypeMessageFormat(nameof(type2), "Approve"));

      // Равенство.
      if (type1 == type2)
        return 0;

      // Доступ запрещен - больше любых других прав.
      if (type1 == DefaultAccessRightsTypes.Forbidden && type2 != DefaultAccessRightsTypes.Forbidden)
        return 1;
      if (type2 == DefaultAccessRightsTypes.Forbidden && type1 != DefaultAccessRightsTypes.Forbidden)
        return -1;

      // Полные права больше всех остальных. Доступ запрещен проверен выше.
      if (type1 == DefaultAccessRightsTypes.FullAccess && type2 != DefaultAccessRightsTypes.FullAccess)
        return 1;
      if (type2 == DefaultAccessRightsTypes.FullAccess && type1 != DefaultAccessRightsTypes.FullAccess)
        return -1;

      // Права на изменение следующие по важности после полного доступа.
      if (type1 == DefaultAccessRightsTypes.Change && type2 != DefaultAccessRightsTypes.Change)
        return 1;
      if (type2 == DefaultAccessRightsTypes.Change && type1 != DefaultAccessRightsTypes.Change)
        return -1;

      // Права на чтение ниже всех остальных. Если вдруг добрались сюда, то смотрим по type1.
      return type1 == DefaultAccessRightsTypes.Read ? -1 : 1;
    }
    
    /// <summary>
    /// Выдать права на документ по правилу назначения прав.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="ruleIds">ИД правил назначения прав.</param>
    /// <param name="grantAccessRightsToRelatedDocuments">Выдавать права связанным документам.</param>
    /// <returns>True, если права были успешно выданы.</returns>
    public override bool GrantAccessRightsToDocumentByRule(Sungero.Docflow.IOfficialDocument document, List<int> ruleIds, bool grantAccessRightsToRelatedDocuments)
    {
      var logMessagePrefix = string.Format("GrantAccessRightsToDocumentsByRule. Document(ID={0}).", document.Id);
      if (document == null)
      {
        Logger.DebugFormat("{0} No document with this id found", logMessagePrefix);
        return true;
      }
      
      var documentRules = Sungero.Docflow.AccessRightsRules.GetAll(r => ruleIds.Contains(r.Id)).ToList();
      if (!documentRules.Any())
      {
        Logger.DebugFormat("{0} No suitable rules found", logMessagePrefix);
        return true;
      }
      
      var needSave = false;
      var recipientsAccessRights = this.GetRecipientsAccessRights(document.Id, documentRules);
      foreach (var recipientAccessRights in recipientsAccessRights)
      {
        var recipientAccessRightsGuids = recipientAccessRights.Value.Select(x => Sungero.Docflow.PublicFunctions.Module.GetRightTypeGuid(x));
        var highestAccessRights = this.GetHighestInstanceAccessRights(recipientAccessRightsGuids);
        if (this.GrantAccessRightsOnEntity(document, recipientAccessRights.Key, highestAccessRights.Value))
          needSave = true;
      }
      
      if (needSave)
      {
        try
        {
          document.AccessRights.Save();
          Logger.DebugFormat("{0} Rights to the document granted successfully", logMessagePrefix);
        }
        catch (Exception ex)
        {
          // В случае возникновения исключения документ всегда уходит на повторную обработку в рамках АО.
          // Ничего критичного не произойдет и поэтому в лог пишется сообщение типа Debug,
          // чтобы оно не светилось красным и не нервировало лишний раз администратора.
          Logger.DebugFormat("{0} Cannot grant rights to document", ex, logMessagePrefix);
          return false;
        }
      }
      
      // Права на дочерние документы от ведущего.
      if (grantAccessRightsToRelatedDocuments)
        this.CreateGrantAccessRightsToChildDocumentAsyncHandler(document, documentRules);
      
      return true;
    }

    /// <summary>
    /// Получить из списка правил подходящие для документа.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="rules">Правила.</param>
    /// <returns>Подходящие правила.</returns>
    public new static List<Sungero.Docflow.IAccessRightsRule> GetAvailableRules(Sungero.Docflow.IOfficialDocument document, List<Sungero.Docflow.IAccessRightsRule> rules)
    {
      var documentGroup = Sungero.Docflow.PublicFunctions.OfficialDocument.GetDocumentGroup(document);
      
      return rules
        .Where(s => s.Status == Sungero.Docflow.AccessRightsRule.Status.Active)
        .Where(s => !s.DocumentKinds.Any() || s.DocumentKinds.Any(k => Equals(k.DocumentKind, document.DocumentKind)))
        .Where(s => !s.BusinessUnits.Any() || s.BusinessUnits.Any(u => Equals(u.BusinessUnit, document.BusinessUnit)))
        .Where(s => !s.Departments.Any() || s.Departments.Any(k => Equals(k.Department, document.Department)))
        .Where(s => !s.DocumentGroups.Any() || s.DocumentGroups.Any(k => Equals(k.DocumentGroup, documentGroup)))
        .Where(s => anorsv.AnorsvMainSolution.AccessRightsRules.As(s).AllowForConfidentialDocumentsanorsv == anorsv.OfficialDocument.OfficialDocuments.As(document).ConfidentialDocumentanorsv)
        .ToList();
    }
    
    /// <summary>
    /// Получить из списка правил подходящие для документа.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <returns>Подходящие правила.</returns>
    public override IQueryable<Sungero.Docflow.IAccessRightsRule> GetAvailableRules(Sungero.Docflow.IOfficialDocument document)
    {
      var documentGroup = Sungero.Docflow.PublicFunctions.OfficialDocument.GetDocumentGroup(document);
      
      return Sungero.Docflow.AccessRightsRules.GetAll(s => s.Status == Sungero.Docflow.AccessRightsRule.Status.Active)
        .Where(s => !s.DocumentKinds.Any() || s.DocumentKinds.Any(k => Equals(k.DocumentKind, document.DocumentKind)))
        .Where(s => !s.BusinessUnits.Any() || s.BusinessUnits.Any(u => Equals(u.BusinessUnit, document.BusinessUnit)))
        .Where(s => !s.Departments.Any() || s.Departments.Any(k => Equals(k.Department, document.Department)))
        .Where(s => !s.DocumentGroups.Any() || s.DocumentGroups.Any(k => Equals(k.DocumentGroup, documentGroup)))
        .Where(s => anorsv.AnorsvMainSolution.AccessRightsRules.As(s).AllowForConfidentialDocumentsanorsv == anorsv.OfficialDocument.OfficialDocuments.As(document).ConfidentialDocumentanorsv);
    }
    
    /// <summary>
    /// Получить все дочерние документы по иерархии, у которых документ из списка указан в качестве LeadingDocument.
    /// </summary>
    /// <param name="documentIds">Список Ид ведущих документов.</param>
    /// <returns>Список Ид документов.</returns>
    public override List<int> GetAllChildDocuments(List<int> documentIds)
    {
      var documents = new List<int>(documentIds);
      var allChildDocuments = new List<int>();
      var childDocuments = this.GetChildDocuments(documents);
      while (childDocuments.Any())
      {
        documents.AddRange(childDocuments);
        allChildDocuments.AddRange(childDocuments);
        childDocuments = this.GetChildDocuments(documents);
      }
      
      return allChildDocuments;
    }
    
    /// <summary>
    /// Получить количество документов в пакете для массовой выдачи прав.
    /// </summary>
    /// <returns>Количество документов в пакете.</returns>
    public override int GetAccessRightsBulkProcessingBatchSize()
    {
      var batchSize = (int)Sungero.Docflow.PublicFunctions.Module.Remote.GetDocflowParamsNumbericValue(Sungero.Docflow.Constants.Module.AccessRightsBulkProcessing.BatchSizeParamName);
      if (batchSize <= 0)
        batchSize = Sungero.Docflow.Constants.Module.AccessRightsBulkProcessing.BatchSize;
      return batchSize;
    }
    
    /// <summary>
    /// Получить список ИД подходящих для документа правил.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="ruleIds">Список ИД правил, которые нужно проверить.</param>
    /// <returns>Список ИД правил.</returns>
    public override List<int> GetAvailableRuleIds(Sungero.Docflow.IOfficialDocument document, List<int> ruleIds)
    {
      var documentRuleIds = GetAvailableRules(document).Select(r => r.Id).ToList();
      var leadindDocumentsRuleIds = Docflow.Functions.Module.GetLeadindDocumentsRules(document);
      documentRuleIds.AddRange(leadindDocumentsRuleIds);
      
      if (ruleIds.Any())
        return ruleIds.Where(r => documentRuleIds.Contains(r)).ToList();
      else
        return documentRuleIds;
    }

    /// <summary>
    /// Получить правила назначения прав по ведущим документам.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <returns>Список правил.</returns>
    public override List<int> GetLeadindDocumentsRules(Sungero.Docflow.IOfficialDocument document)
    {
      var leadDocumentRuleIds = new List<int>();
      if (document == null)
        return leadDocumentRuleIds;
      
      var leadingDocumentIds = Docflow.Functions.Module.GetLeadingDocuments(document);
      foreach (var leadingDocumentId in leadingDocumentIds)
      {
        var leadingDocument = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == leadingDocumentId).FirstOrDefault();
        var availableRuleIds = GetAvailableRules(leadingDocument)
          .Where(s => s.GrantRightsOnLeadingDocument == true)
          .Select(s => s.Id)
          .ToList();
        leadDocumentRuleIds.AddRange(availableRuleIds);
      }
      
      return leadDocumentRuleIds;
    }
    
    /// <summary>
    /// Получить документы, у которых документ указан в качестве LeadingDocument.
    /// </summary>
    /// <param name="document">Ведущий документ.</param>
    /// <returns>Документы, у которых документ указан в качестве LeadingDocument.</returns>
    [Public]
    public static List<int> GetDocumentsByLeadingDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var documents = new List<int>() { document.Id };
      var allChildDocuments = new List<int>();
      var childDocuments = Sungero.Docflow.OfficialDocuments.GetAll(d => d.LeadingDocument != null && documents.Contains(d.LeadingDocument.Id) && !documents.Contains(d.Id))
        .Select(d => d.Id)
        .ToList();
      while (childDocuments.Any())
      {
        documents.AddRange(childDocuments);
        allChildDocuments.AddRange(childDocuments);
        childDocuments = Sungero.Docflow.OfficialDocuments.GetAll(d => d.LeadingDocument != null && documents.Contains(d.LeadingDocument.Id) && !documents.Contains(d.Id))
          .Select(d => d.Id)
          .ToList();
      }
      return allChildDocuments;
    }

    /// <summary>
    /// Получить ведущие документы.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <returns>Ведущие документы.</returns>
    [Public]
    public new static List<int> GetLeadingDocuments(Sungero.Docflow.IOfficialDocument document)
    {
      var documents = new List<int>() { document.Id };
      var leadingDocuments = new List<int>();
      while (document.LeadingDocument != null && !documents.Contains(document.LeadingDocument.Id))
      {
        documents.Add(document.LeadingDocument.Id);
        leadingDocuments.Add(document.LeadingDocument.Id);
        document = document.LeadingDocument;
      }
      return leadingDocuments;
    }
    
    [Public]
    public static string GeUserNameInGenitive(string userName)
    {
      var result = GetFormattedUserNameInGenitive(userName);
      return result;
    }
    
  }
}