﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.AnorsvMainSolution.Module.Docflow.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      base.Initializing(e);
      UpdateRelationTypes();

      /*Рахимова. Если параметр "За сколько дней до окончания срока исполнения поручений " не задан - присваиваем по умолчанию 2
        *         Если параметр "Срок поручениия " не задан - присваиваем по умолчанию 5
        *         Если параметр "Период замещения " не задан - присваиваем по умолчанию 30*/
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("DaysForEndExecutionOrders") == null)
      {
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("DaysForEndExecutionOrders", "2");
      }
      
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderTermCVD") == null)
      {
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("OrderTermCVD", "5");
      }
      
      /*if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("SubstitutePeriod") == null)
      {
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("SubstitutePeriod", "30");
      }*/
      
      var rules = sline.RSV.ApprovalRules.GetAll().ToList();
      foreach (var rule in rules)
      {
        if (rule.ShowSubstitutesanorsv == null)
        {
          rule.ShowSubstitutesanorsv = true;
          rule.Save();
        }
      }
    
    }
    
    public static void UpdateRelationTypes()
    {
      // Основание
      var basis = RelationTypes.GetAll(r => r.Name == Sungero.Docflow.Constants.Module.BasisRelationName).FirstOrDefault();
      
      if (basis == null)
        InitializationLogger.Debug("Init: No relation type named Basis.");

      if (basis != null)
      {
        basis.UseTarget = true;
        
        // Изменение описания по запросу Калининой С.
        basis.SourceDescription = anorsv.AnorsvMainSolution.Module.Docflow.Resources.RelationBasisSourceDescription;
        basis.TargetDescription = anorsv.AnorsvMainSolution.Module.Docflow.Resources.RelationBasisTargetDescription;
      
        basis.Save();
        InitializationLogger.Debug("Init: Update relation type named Basis.");
      }
    }

  }

}
