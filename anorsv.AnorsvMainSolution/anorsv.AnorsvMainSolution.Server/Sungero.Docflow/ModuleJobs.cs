﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.Docflow.Server
{
  partial class ModuleJobs
  {

    /// <summary>
    /// 
    /// </summary>
    public virtual void SentNotificationsForOrderExecutionanorsv()
    {
      var createTableCommand = Queries.Module.CreateTableForExecuteOrder;
      Sungero.Docflow.PublicFunctions.Module.ExecuteSQLCommand(createTableCommand);
      
      var notifyParams = Sungero.Docflow.PublicFunctions.Module.GetDefaultExpiringDocsNotificationParams(Constants.Module.ExecutionOrderLastNotificationKey,
                                                                                                         Constants.Module.ExecutionOrderTableName);
      
      var alreadySentTasks = PublicFunctions.Module.GetTasksWithSendedTask(notifyParams.ExpiringDocTableName);
      
      var str1 = Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("DaysForEndExecutionOrders");      
      var daysForEndExecutionOrders = Convert.ToInt32(str1);
      
      var str2 = Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderTermCVD");
      var orderTerm = Convert.ToInt32(str2);
      
      var executionTasks = anorsv.AnorsvMainSolution.ActionItemExecutionTasks
        .GetAll()
        .Where(p => !alreadySentTasks.Contains(p.Id))
        .Where(p => p.Deadline.Value.Date == Calendar.UserToday.AddDays(daysForEndExecutionOrders).Date)
        .Where(p=>p.Started.Value.AddDays(orderTerm).BeginningOfDay() <= p.Deadline.Value) // берем только те поручения у которых срок исполнения больше 5 дней
        .Where(p=>p.ExecutionState == Sungero.RecordManagement.ActionItemExecutionTask.ExecutionState.OnControl ||
               p.ExecutionState == Sungero.RecordManagement.ActionItemExecutionTask.ExecutionState.OnExecution ||
               p.ExecutionState == Sungero.RecordManagement.ActionItemExecutionTask.ExecutionState.OnRework)
        .ToList();

      Logger.DebugFormat("Execution assignment to send notification count = {0}.", executionTasks.Count());
      
      for (int i = 0; i < executionTasks.Count(); i = i + notifyParams.BatchCount)
      {
        var result = Transactions.Execute(
          () =>
          {
            var executionTasksPart = executionTasks.Skip(i).Take(notifyParams.BatchCount).ToList();
            
            if (!executionTasksPart.Any())
              return;
            
            PublicFunctions.Module.ClearIdsFromExpiringTasksTable(notifyParams.ExpiringDocTableName, executionTasksPart.Select(x => x.Id).ToList());
            PublicFunctions.Module.AddExpiringTasksToTable(notifyParams.ExpiringDocTableName, executionTasksPart.Select(x => x.Id).ToList());
            
            foreach (var executionTask in executionTasksPart)
            {
              var subject = Sungero.Docflow.PublicFunctions.Module.TrimQuotes(anorsv.AnorsvMainSolution.Module.Docflow.Resources.ExecutingOrderSubjectFormat(executionTask.DisplayValue, executionTask.Id.ToString()));
              if (subject.Length > Sungero.Workflow.SimpleTasks.Info.Properties.Subject.Length)
                subject = subject.Substring(0, Sungero.Workflow.SimpleTasks.Info.Properties.Subject.Length);
              
              var activeText = Sungero.Docflow.PublicFunctions.Module.TrimQuotes(anorsv.AnorsvMainSolution.Module.Docflow.Resources.ExecutingOrderTextFormat(executionTask.Deadline.Value.ToShortDateString(),
                                                                                                                                             executionTask.DisplayValue, executionTask.Id.ToString()));
              // перебираем все действующие задания по задаче, вычисляем исполнителей
              var assigments = Sungero.RecordManagement.ActionItemExecutionAssignments
                .GetAll(a => a.Task.Id.Equals(executionTask.Id)
                        && a.Completed == null
                        && a.Status == Sungero.RecordManagement.ActionItemExecutionAssignment.Status.InProcess);
              
              
              /*все исполнителям поручений высылаем уведомления с вложением с заданием*/
              foreach (var assignment in assigments)
              {
                var newTask = Sungero.Workflow.SimpleTasks.CreateWithNotices(subject, assignment.Performer);
                newTask.NeedsReview = false;
                newTask.ActiveText = anorsv.AnorsvMainSolution.Module.Docflow.Resources.NotificationMessage + assignment.DisplayValue;
                newTask.Attachments.Add(assignment);
                newTask.Start();
                Logger.DebugFormat("Notice with Id '{0}' has been started", newTask.Id);
                PublicFunctions.Module.AddTaskToExpiringTasksTable(notifyParams.ExpiringDocTableName, executionTask.Id, newTask.Id);
              };
              
              /*Контроллерам высылаем простое информативное уведомление*/
              if(executionTask.Supervisor!=null)
              {
                var newTaskSupervisor = Sungero.Workflow.SimpleTasks.CreateWithNotices(subject, executionTask.Supervisor);
                newTaskSupervisor.ActiveText = anorsv.AnorsvMainSolution.Module.Docflow.Resources.NotificationMessage + executionTask.DisplayValue;
                newTaskSupervisor.Attachments.Add(executionTask);
                newTaskSupervisor.Start();
                Logger.DebugFormat("Notice with Id '{0}' has been started", newTaskSupervisor.Id);
                PublicFunctions.Module.AddTaskToExpiringTasksTable(notifyParams.ExpiringDocTableName, executionTask.Id, newTaskSupervisor.Id);
              }
              
            }
          });
      }
    }
  }
}
