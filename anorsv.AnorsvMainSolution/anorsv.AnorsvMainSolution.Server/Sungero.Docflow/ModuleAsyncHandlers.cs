using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.Docflow.Server
{
  partial class ModuleAsyncHandlers
  {

    /// <summary>
    /// Асинхронная выдача прав на документы от правила.
    /// </summary>
    /// <param name="args">Параметры вызова асинхронного обработчика.</param>
    public override void GrantAccessRightsToDocumentsByRule(Sungero.Docflow.Server.AsyncHandlerInvokeArgs.GrantAccessRightsToDocumentsByRuleInvokeArgs args)
    {
      int ruleId = args.RuleId;
      var logMessagePrefix = string.Format("GrantAccessRightsToDocumentsByRuleAsync. Rule(ID={0}).", ruleId);
      Logger.DebugFormat("{0} Start creating documents queue", logMessagePrefix);
      
      var rule = AccessRightsRules.GetAll(r => r.Id == ruleId).FirstOrDefault();
      if (rule == null)
      {
        Logger.DebugFormat("{0} No rule with this id found", logMessagePrefix);
        return;
      }
      
      var documents = GetDocumentsByRule(rule).ToList();
      
      if (documents.Any())
      {
        if (rule.GrantRightsOnLeadingDocument.GetValueOrDefault())
          documents.AddRange(Docflow.Functions.Module.GetAllChildDocuments(documents));
        
        var batchSize = Docflow.Functions.Module.GetAccessRightsBulkProcessingBatchSize();
        Logger.DebugFormat("{0} Documents for processing: {1}. Max batch size: {2}", logMessagePrefix, documents.Count, batchSize);
        
        var docsProcessed = 0;
        while (docsProcessed < documents.Count)
        {
          var batch = documents.Skip(docsProcessed).Take(batchSize).ToList();
          Sungero.Docflow.PublicFunctions.Module.CreateGrantAccessRightsToDocumentAsyncHandlerBulk(rule.Id);//, batch);
          docsProcessed += batch.Count;
        }
        
        Logger.DebugFormat("{0} Documents queue created successfully", logMessagePrefix);
      }
      else
        Logger.DebugFormat("{0} No documents found", logMessagePrefix);
    }
    
    /// <summary>
    /// Асинхронная выдача прав на документ.
    /// </summary>
    /// <param name="args">Параметры вызова асинхронного обработчика.</param>
    public override void GrantAccessRightsToDocument(Sungero.Docflow.Server.AsyncHandlerInvokeArgs.GrantAccessRightsToDocumentInvokeArgs args)
    {
      int documentId = args.DocumentId;
      string stringRuleIds = args.RuleIds;
      
      var document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentId).FirstOrDefault();
      if (document == null)
      {
        Logger.DebugFormat("GrantAccessRightsToDocument: no document with id {0}", documentId);
        return;
      }

      if (Locks.GetLockInfo(document).IsLockedByOther)
      {
        Logger.DebugFormat("GrantAccessRightsToDocument: document with id {0} is blocked", documentId);
        args.Retry = true;
        return;
      }

      var ruleIds = new List<int>();
      if (!string.IsNullOrWhiteSpace(stringRuleIds))
      {
        try
        {
          ruleIds = stringRuleIds.Split(new string[] { Sungero.Docflow.Constants.AccessRightsRule.DocumentIdsSeparator }, StringSplitOptions.None)
            .Select(r => int.Parse(r)).ToList();
        }
        catch
        {
          Logger.DebugFormat("GrantAccessRightsToDocument: incorrect right ids for document {0}", documentId);
          return;
        }
      }
      
      var availableRuleIds = Docflow.Functions.Module.GetAvailableRuleIds(document, ruleIds);
      if (!availableRuleIds.Any())
      {
        Logger.DebugFormat("GrantAccessRightsToDocument: no suitable rules for document {0}", documentId);
        return;
      }
      
      Logger.DebugFormat("GrantAccessRightsToDocument: start grant rights for document {0}", documentId);
      
      var isGranted = Docflow.Functions.Module.GrantAccessRightsToDocumentByRule(document, availableRuleIds, args.GrantRightToChildDocuments);
      if (!isGranted)
      {
        Logger.DebugFormat("GrantAccessRightsToDocument: cannot grant rights for document {0}", documentId);
        args.Retry = true;
      }
      else
        Logger.DebugFormat("GrantAccessRightsToDocument: done for document {0}", documentId);
    }
    
    /// <summary>
    /// Фильтр для категорий договоров.
    /// </summary>
    /// <param name="rule">Правило.</param>
    /// <param name="query">Ленивый запрос документов.</param>
    /// <returns>Относительно ленивый запрос с категориями.</returns>
    private static IEnumerable<int> FilterDocumentsByGroups(anorsv.AnorsvMainSolution.IAccessRightsRule rule, IQueryable<Sungero.Docflow.IOfficialDocument> query)
    {
      foreach (var document in query)
      {
        var documentGroup = Sungero.Docflow.PublicFunctions.OfficialDocument.GetDocumentGroup(document);
        if (rule.DocumentGroups.Any(k => Equals(k.DocumentGroup, documentGroup)))
          yield return document.Id;
      }
    }

    /// <summary>
    /// Получить документы по правилу.
    /// </summary>
    /// <param name="rule">Правило.</param>
    /// <returns>Документы по правилу.</returns>
    public static IEnumerable<int> GetDocumentsByRule(anorsv.AnorsvMainSolution.IAccessRightsRule rule)
    {
      var documentKinds = rule.DocumentKinds.Select(t => t.DocumentKind).ToList();
      var businessUnits = rule.BusinessUnits.Select(t => t.BusinessUnit).ToList();
      var departments = rule.Departments.Select(t => t.Department).ToList();
      
      var documents = Sungero.Docflow.OfficialDocuments.GetAll()
        .Where(d => !documentKinds.Any() || documentKinds.Contains(d.DocumentKind))
        .Where(d => !businessUnits.Any() || businessUnits.Contains(d.BusinessUnit))
        .Where(d => !departments.Any() || departments.Contains(d.Department))
        .Where(d => anorsv.OfficialDocument.OfficialDocuments.As(d).ConfidentialDocumentanorsv == rule.AllowForConfidentialDocumentsanorsv);
      
      if (rule.DocumentGroups.Any())
        return FilterDocumentsByGroups(rule, documents);
      else
        return documents.Select(d => d.Id);
    }
  }
}