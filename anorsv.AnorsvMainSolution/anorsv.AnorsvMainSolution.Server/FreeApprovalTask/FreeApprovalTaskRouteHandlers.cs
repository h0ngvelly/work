﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.AnorsvMainSolution.FreeApprovalTask;

namespace anorsv.AnorsvMainSolution.Server
{
  partial class FreeApprovalTaskRouteHandlers
  {

    public override void CompleteAssignment8(Sungero.Docflow.IFreeApprovalFinishAssignment assignment, Sungero.Docflow.Server.FreeApprovalFinishAssignmentArguments e)
    {
      base.CompleteAssignment8(assignment, e);
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment3(Sungero.Docflow.IFreeApprovalReworkAssignment assignment, Sungero.Docflow.Server.FreeApprovalReworkAssignmentArguments e)
    {
      base.CompleteAssignment3(assignment, e);
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment2(Sungero.Docflow.IFreeApprovalAssignment assignment, Sungero.Docflow.Server.FreeApprovalAssignmentArguments e)
    {
      base.CompleteAssignment2(assignment, e);
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

  }
}