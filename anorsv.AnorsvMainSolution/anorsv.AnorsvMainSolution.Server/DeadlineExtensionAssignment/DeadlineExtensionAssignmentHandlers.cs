﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DeadlineExtensionAssignment;

namespace anorsv.AnorsvMainSolution
{
  partial class DeadlineExtensionAssignmentServerHandlers
  {

    public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
    {
      if (_obj.Task.ParentAssignment.Task.ParentAssignment != null)
      {
        if (_obj.Task.ParentAssignment.Task.ParentAssignment.Deadline < _obj.NewDeadline
            && Equals(_obj.Result, anorsv.AnorsvMainSolution.DeadlineExtensionAssignment.Result.Accept))
        {
          var dialog = anorsv.AnorsvMainSolution.PublicFunctions.DeadlineExtensionAssignment.SubtaskDeadlineNotChanged(_obj);
          
          if (!dialog)
          {
            e.AddError(anorsv.AnorsvMainSolution.DeadlineExtensionAssignments.Resources.AssignmentNotCompleted);
            return;
          }
        }
      }
      
      base.BeforeComplete(e);
    }
  }

}
