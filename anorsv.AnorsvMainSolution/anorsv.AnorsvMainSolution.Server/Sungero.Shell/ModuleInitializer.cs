﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.AnorsvMainSolution.Module.Shell.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      base.Initializing(e);
      GrantRightOnNewFolders(); 
    }
    
    /// <summary>
    /// Выдать права на папки потока модуля.
    /// </summary>
    /// <param name="allUsers">Группа "Все пользователи".</param>
    public static void GrantRightOnNewFolders()
    {
      var allUsers = Roles.AllUsers;
      var onCheckingFolder = SpecialFolders.OnChekinganorsv;
      var formalizedSubtaskFolder = anorsv.FormalizedSubTask.SpecialFolders.FormalizedSubTaskAssignment;
      
      if (allUsers != null)
      {
        InitializationLogger.Debug("Init: Grant right on OnCheking special folders to all users.");
        if (onCheckingFolder != null 
            && !onCheckingFolder.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Read, allUsers))
        {
          onCheckingFolder.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
          onCheckingFolder.AccessRights.Save();
        }

        if (formalizedSubtaskFolder != null 
            && !formalizedSubtaskFolder.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Read, allUsers))
        {
          InitializationLogger.Debug("Init: Grant right on formalized subtask special folders to all users."); 
          formalizedSubtaskFolder.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);       
          formalizedSubtaskFolder.AccessRights.Save();
        }
      }
      
      
    }
  }
}
