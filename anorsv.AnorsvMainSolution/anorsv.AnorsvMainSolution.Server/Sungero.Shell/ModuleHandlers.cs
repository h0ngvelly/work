﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.Shell.Server
{

  partial class OnChekinganorsvFolderHandlers
  {

    public virtual IQueryable<Sungero.Workflow.IAssignmentBase> OnChekinganorsvDataQuery(IQueryable<Sungero.Workflow.IAssignmentBase> query)
    {
      // Фильтрация по типу заданий и уведомлений
      var result = query
        .Where(a =>
               Sungero.RecordManagement.ActionItemSupervisorAssignments.Is(a) ||
               Sungero.RecordManagement.AcquaintanceFinishAssignments.Is(a) ||
               Sungero.Docflow.FreeApprovalFinishAssignments.Is(a) ||
               Sungero.Docflow.DeadlineExtensionAssignments.Is(a) ||
               anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignments.Is(a) ||
               anorsv.FormalizedSubTask.FormalizedSubtaskReviewAssignments.Is(a) ||
               Sungero.RecordManagement.ReportRequestCheckAssignments.Is(a) ||
               Sungero.Workflow.ReviewAssignments.Is(a) ||
               Sungero.Docflow.CheckReturnCheckAssignments.Is(a));
      
      // Запрос непрочитанных без фильтра.
      if (_filter == null)
        return Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result);
      
      // Фильтры по статусу, замещению и периоду.
      result = Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(
        result
        , _filter.InProcessanorsv
        , _filter.Last30Daysanorsv
        , _filter.Last90Daysanorsv
        , _filter.Last180Daysanorsv
        , false
       );
      
      return result;
    }
  }

  partial class OnRegisterFolderHandlers
  {

    public override bool IsOnRegisterVisible()
    {
      return Sungero.Docflow.PublicFunctions.Module.Remote.IncludedInClerksRole() && !Sungero.Docflow.PublicFunctions.Module.IncludedInBusinessUnitHeadsRole();
    }
  }

  partial class OnDocumentProcessingFolderHandlers
  {

    public override bool IsOnDocumentProcessingVisible()
    {
      return true;
    }
  }




  partial class ApprovalFolderHandlers
  {

    public override bool IsApprovalVisible()
    {
      return base.IsApprovalVisible();
    }
  }

  partial class OnSigningFolderHandlers
  {

    public override bool IsOnSigningVisible()
    {
      //return base.IsOnSigningVisible();
      return true;
    }
  }

  partial class OnReviewFolderHandlers
  {

    public override bool IsOnReviewVisible()
    {
      //return base.IsOnReviewVisible();
      return true;
    }
  }

  partial class OnApprovalFolderHandlers
  {

    public override bool IsOnApprovalVisible()
    {
      //return base.IsOnApprovalVisible();
      return true;
    }
  }

  partial class OnReworkFolderHandlers
  {

    public override bool IsOnReworkVisible()
    {
      //return base.IsOnReworkVisible();
      return true;
    }
  }

  partial class OnChekingFolderHandlers
  {

    public override bool IsOnChekingVisible()
    {
      return false;
    }
  }

  partial class ShellHandlers
  {
  }
}