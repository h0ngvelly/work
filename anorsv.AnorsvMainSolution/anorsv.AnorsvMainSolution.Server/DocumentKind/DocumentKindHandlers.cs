﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DocumentKind;

namespace anorsv.AnorsvMainSolution
{
  partial class DocumentKindServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      // По умолчанию не конфиденциальный
      _obj.Confidentialanorsv = false;
    }
  }

  partial class DocumentKindDocKindSignatoryanorsvPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> DocKindSignatoryanorsvFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      // Возможность выбрать только из действующих записей
      return query.Where(f => f.Status.Value == Sungero.Company.Employee.Status.Active);
    }

  }

}