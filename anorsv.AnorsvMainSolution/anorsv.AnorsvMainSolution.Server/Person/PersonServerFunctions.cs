﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.Person;

namespace anorsv.AnorsvMainSolution.Server
{
  partial class PersonFunctions
  {
    /// <summary>
    /// Проверка наличия Работников, в которых используется данная персона
    /// </summary>
    [Remote(IsPure = true)]
    public virtual bool CheckEmployeeForPerson()
    {
      return Sungero.Company.Employees.GetAll().Where(r => r.Person.Id == _obj.Id).Any();
    }
  }
}