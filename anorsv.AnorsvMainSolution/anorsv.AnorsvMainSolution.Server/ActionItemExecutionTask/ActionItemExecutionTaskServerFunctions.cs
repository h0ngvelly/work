﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ActionItemExecutionTask;

namespace anorsv.AnorsvMainSolution.Server
{
    partial class ActionItemExecutionTaskFunctions
    {
        /// <summary>
        /// Получить незавершенные подчиненные поручения.
        /// </summary>
        /// <param name="entity"> Поручение, для которого требуется получить незавершенные.</param>
        /// <returns>Список незавершенных подчиненных поручений.</returns>
        [Remote(IsPure = true)]
        public static List<IActionItemExecutionTask> SL_GetSubActionItemExecutions(IActionItemExecutionAssignment entity)
        {
            // TODO Котегов: есть бага платформы 19850.
            return ActionItemExecutionTasks.GetAll()
                .Where(t => t.ParentAssignment == entity)
                .Where(t => t.ActionItemType == Sungero.RecordManagement.ActionItemExecutionTask.ActionItemType.Additional ||
                       t.ActionItemType == Sungero.RecordManagement.ActionItemExecutionTask.ActionItemType.Main)
                .Where(t => t.Status.Value == Sungero.Workflow.Task.Status.InProcess)
                .ToList();
        }
        
        /// <summary>
        /// Получить поручения по фед.проектам
        /// </summary>
        [Public]
        public static List<IActionItemExecutionTask> GetFedProjectsActionItemExecitionTasks()
        {
          return ActionItemExecutionTasks.GetAll()
            .Where(t => t.AttachmentDetails.Any(a => anorsv.DocflowModule.FederalProjects.GetAll().Any(d => d.Id == a.AttachmentId)))
            .ToList();
        }
    
    }
}