﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.AnorsvMainSolution.ActionItemExecutionTask;

namespace anorsv.AnorsvMainSolution.Server
{
    partial class ActionItemExecutionTaskRouteHandlers
    {

        public override void CompleteAssignment6(Sungero.RecordManagement.IActionItemSupervisorAssignment assignment, Sungero.RecordManagement.Server.ActionItemSupervisorAssignmentArguments e)
        {
            if (assignment.Result == Sungero.RecordManagement.ActionItemSupervisorAssignment.Result.Agree)
            {
                var doc = _obj.DocumentsGroup.OfficialDocuments.FirstOrDefault();
                var myDoc = sline.RSV.IncomingLetters.As(doc);
                if (myDoc != null)
                {
                    myDoc.ExecutionDatesline = assignment.Completed;
                    myDoc.Save();
                }
            }
            base.CompleteAssignment6(assignment, e);
        }

        public override bool Decision5Result()
        {
            return base.Decision5Result();
        }

        public override void Script10Execute()
        {
            base.Script10Execute();
        }

        public override void StartBlock4(Sungero.RecordManagement.Server.ActionItemExecutionAssignmentArguments e)
        {
            if (_obj.RedirectToanorsv != null)
            {
                _obj.Assignee = _obj.RedirectToanorsv;
                _obj.RedirectToanorsv = null;
            }
            base.StartBlock4(e);
        }

        public override void CompleteAssignment4(Sungero.RecordManagement.IActionItemExecutionAssignment assignment, Sungero.RecordManagement.Server.ActionItemExecutionAssignmentArguments e)
        {
            if (assignment.Result == anorsv.AnorsvMainSolution.ActionItemExecutionAssignment.Result.Redirect)
            {
                var myAssignment = anorsv.AnorsvMainSolution.ActionItemExecutionAssignments.As(assignment);
                _obj.RedirectToanorsv = myAssignment.RedirectToanorsv;
                var doc = _obj.DocumentsGroup.OfficialDocuments.FirstOrDefault();
                doc.Assignee =  _obj.RedirectToanorsv;
                doc.Save();
            }
            if (assignment.Result == anorsv.AnorsvMainSolution.ActionItemExecutionAssignment.Result.Done)
            {
                if (_obj.Supervisor == null || _obj.Supervisor == _obj.Assignee)
                {
                    var doc = _obj.DocumentsGroup.OfficialDocuments.FirstOrDefault();
                    var myDoc = sline.RSV.IncomingLetters.As(doc);
                    if (myDoc != null)
                    {
                        myDoc.ExecutionDatesline = assignment.Completed;
                        myDoc.Save();
                    }
                }
            }
            base.CompleteAssignment4(assignment, e);
        }

    }
}