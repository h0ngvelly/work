﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ActionItemExecutionTask;

namespace anorsv.AnorsvMainSolution
{
  partial class ActionItemExecutionTaskFilteringServerHandler<T>
  {

    public virtual IQueryable<Sungero.Docflow.IDocumentType> DocumentTypeFiltering(IQueryable<Sungero.Docflow.IDocumentType> query, Sungero.Domain.FilteringEventArgs e)
    {
      if (_filter.DocumentKind != null)
        query = query.Where(t => Equals(t.DocumentTypeGuid, _filter.DocumentKind.DocumentType.DocumentTypeGuid));
      return query;
    }

    public virtual IQueryable<Sungero.Docflow.IDocumentKind> DocumentKindFiltering(IQueryable<Sungero.Docflow.IDocumentKind> query, Sungero.Domain.FilteringEventArgs e)
    {
      if (_filter.DocumentType != null)
        query = query.Where(k => Equals(k.DocumentType, _filter.DocumentType));
      return query;
    }

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      query = base.Filtering(query, e);      
      
      // Вернуть нефильтрованный список, если нет фильтра. Он будет использоваться во всех Get() и GetAll().
      var filter = _filter;
      if (filter != null)
      {
        // Наложить фильтр по типу/виду основного документа
        if (filter.DocumentType != null)
          query = query.Where(c => c.AttachmentDetails.Any(a => anorsv.OfficialDocument.OfficialDocuments
          .GetAll()
          .Any(d => d.Id == a.AttachmentId && Equals(a.AttachmentTypeGuid.ToString(), filter.DocumentType.DocumentTypeGuid))));
        if (filter.DocumentKind != null)
          query = query.Where(c => c.AttachmentDetails.Any(a => anorsv.OfficialDocument.OfficialDocuments
          .GetAll()
          .Any(d => d.Id == a.AttachmentId && Equals(d.DocumentKind, filter.DocumentKind))));
      }

      
      return query;
    }
  }

    partial class ActionItemExecutionTaskServerHandlers
    {

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      base.Saving(e);
      
      // Почистим группы вложений от дублей
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.AddendaGroup);
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.OtherGroup);
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.ResultGroup);
    }

        public override void BeforeStart(Sungero.Workflow.Server.BeforeStartEventArgs e)
        {
            base.BeforeStart(e);
            
            if (_obj.ActionItemType == ActionItemType.Component)
            {
                _obj.ThreadSubject += " для " +_obj.Assignee;
            }
        }
    }


}