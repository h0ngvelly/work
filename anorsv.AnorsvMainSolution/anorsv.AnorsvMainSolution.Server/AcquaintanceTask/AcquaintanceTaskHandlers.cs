﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.AcquaintanceTask;

namespace anorsv.AnorsvMainSolution
{
  partial class AcquaintanceTaskServerHandlers
  {

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      base.Saving(e);
      
      // Почистим группы вложений от дублей
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.AddendaGroup);
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.OtherGroup);
    }
  }

  partial class AcquaintanceTaskObserversObserverPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> ObserversObserverFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.ObserversObserverFiltering(query, e);
      return query;
    }
  }


}