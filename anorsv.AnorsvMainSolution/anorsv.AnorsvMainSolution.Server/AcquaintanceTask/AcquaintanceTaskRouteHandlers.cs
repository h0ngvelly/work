﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.AnorsvMainSolution.AcquaintanceTask;

namespace anorsv.AnorsvMainSolution.Server
{
  partial class AcquaintanceTaskRouteHandlers
  {

    public override void CompleteAssignment4(Sungero.RecordManagement.IAcquaintanceFinishAssignment assignment, Sungero.RecordManagement.Server.AcquaintanceFinishAssignmentArguments e)
    {
      base.CompleteAssignment4(assignment, e);
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment3(Sungero.RecordManagement.IAcquaintanceAssignment assignment, Sungero.RecordManagement.Server.AcquaintanceAssignmentArguments e)
    {
      base.CompleteAssignment3(assignment, e);
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

  }
}