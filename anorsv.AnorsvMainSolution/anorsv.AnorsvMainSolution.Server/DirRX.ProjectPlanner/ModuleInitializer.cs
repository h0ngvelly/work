﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.AnorsvMainSolution.Module.ProjectPlanner.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      UpgradeDocumentKinds();
    }
    
    /// <summary>
    /// Вид документа Техническое задание проекта
    /// </summary>
    public void UpgradeDocumentKinds()
    {
      InitializationLogger.Debug("ContractsModule Init: upgrade CustomerRequirements document kind.");
      
      var docKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.Initialize.CustomerRequirementsKind);
      docKind.Name = ProjectPlanner.Resources.ProjectTSName;
      docKind.ShortName = ProjectPlanner.Resources.ProjectTSShortName;
      docKind.Save();
    }
  }
}
