﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DocumentRegister;

namespace anorsv.AnorsvMainSolution.Server
{
  partial class DocumentRegisterFunctions
  {

    [Public, Remote(IsPure = true)]
    public override string GetNextNumber(DateTime date, int leadDocumentId, Sungero.Docflow.IOfficialDocument document, string leadingDocumentNumber, int departmentId, int businessUnitId, string caseFileIndex, string docKindCode, string indexLeadingSymbol)
    {
      return base.GetNextNumber(date, leadDocumentId, document, leadingDocumentNumber, departmentId, businessUnitId, caseFileIndex, docKindCode, indexLeadingSymbol);
    }

  }
}