﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.CaseFile;

namespace anorsv.AnorsvMainSolution
{
  partial class CaseFileServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);
      
      // Если Дело отмечено основным для видов документов,
      // Проверить, что нет других основных Дел для выбранного вида документа
      /*var isMainForKind = _obj.DocKindCollectionanorsv.Where(k => k.MainForDocKind == true).ToList();
      if (isMainForKind.Any())
      {
        foreach (var row in isMainForKind)
        {
          var docKind = row.DocKind;
          var mainCaseFile = CaseFiles.GetAll().Where(c => !c.Id.Equals(_obj.Id) && 
                                                      c.DocKindCollectionanorsv.Any(d => d.MainForDocKind == true && 
                                                                                    d.DocKind.Equals(docKind))).FirstOrDefault();
          if (mainCaseFile != null)
            e.AddError(anorsv.AnorsvMainSolution.Resources.MoreThanOneMainCaseFileForDocTypeFormat(mainCaseFile.Name));  
        }
      }*/
      var isMainForKind = _obj.DocKindCollectionanorsv.ToList();
      foreach (var row in isMainForKind)
      {
        var docKind = row.DocKind;
        var mainCaseFile = CaseFiles.GetAll().Where(c => !c.Id.Equals(_obj.Id) && 
                                                      c.DocKindCollectionanorsv.Any(d => d.DocKind.Equals(docKind))).FirstOrDefault();
        if (mainCaseFile != null)
          e.AddError(anorsv.AnorsvMainSolution.Resources.MoreThanOneMainCaseFileForDocTypeFormat(docKind.Name, mainCaseFile.Name));  
      }
      
      
      /*var isMainCaseFile = _obj.DocKindCollectionanorsv.Any(a => a.MainForDocKind == true);
      if (isMainCaseFile == true)
      {
        var mainCaseFiles = CaseFiles.GetAll().Where(c => !c.Id.Equals(_obj.Id) && c.DocKindCollectionanorsv.Any(d => d.MainForDocKind == true)).ToList();
        
        if (mainCaseFiles.Any())
        {
          var mainCaseForKind = mainCaseFiles.Any(f => );
        }
        //var mainCaseFile = CaseFiles.GetAll().Where(a => a.DocKindCollectionanorsv.Any(c => c.MainForDocKind == true && _obj.DocKindCollectionanorsv.Any(d => d.MainForDocKind == true && d.DocKind == c.DocKind)));
        //if (mainCaseFile.Any())
        //  e.AddError(anorsv.AnorsvMainSolution.Resources.MoreThanOneMainCaseFileForDocTypeFormat(mainCaseFile.First().Name));
      }*/
    }

  }

  partial class CaseFileDocKindCollectionanorsvDocKindPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> DocKindCollectionanorsvDocKindFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      // Возможность выбрать только из действующих записей нумеруемых/регистрируемых видов
      return query.Where(f => (f.Status.Value == DocumentKind.Status.Active) && 
        ((f.NumberingType == DocumentKind.NumberingType.Numerable) || (f.NumberingType == DocumentKind.NumberingType.Registrable)) );
    }

  }

}