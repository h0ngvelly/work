﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.AccessRightsRule;

namespace anorsv.AnorsvMainSolution
{
  partial class AccessRightsRuleServerHandlers
  {

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      base.Saving(e);
      
      if (_obj.AllowForConfidentialDocumentsanorsv == null)
      {
        _obj.AllowForConfidentialDocumentsanorsv = false;
      }
    }
  }

}