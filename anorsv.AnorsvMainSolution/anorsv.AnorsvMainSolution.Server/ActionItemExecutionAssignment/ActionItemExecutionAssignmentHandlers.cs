﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ActionItemExecutionAssignment;

namespace anorsv.AnorsvMainSolution
{
  partial class ActionItemExecutionAssignmentServerHandlers
  {

    public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
    {
      string lockedDocumentMessage = anorsv.TaskModule.PublicFunctions.Module
        .AttachedDocumentIsLocked(_obj.AllAttachments.ToList());
      
      if (!string.IsNullOrEmpty(lockedDocumentMessage))
      {
        e.AddError(lockedDocumentMessage);
        return;
      }
      
      base.BeforeComplete(e);

      if (_obj.Result == anorsv.AnorsvMainSolution.ActionItemExecutionAssignment.Result.Redirect)
      {
        e.Result = anorsv.AnorsvMainSolution.ActionItemExecutionAssignments.Resources
          .RedirectedFormat(
            Sungero.Company.PublicFunctions.Employee.GetShortName(_obj.RedirectToanorsv, DeclensionCase.Dative, true));
      }
    }
  }
}
