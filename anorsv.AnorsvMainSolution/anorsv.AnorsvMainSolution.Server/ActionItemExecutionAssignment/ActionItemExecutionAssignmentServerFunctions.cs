﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ActionItemExecutionAssignment;

namespace anorsv.AnorsvMainSolution.Server
{
    partial class ActionItemExecutionAssignmentFunctions
    {
        /// <summary>
        /// Проверка, все ли задания соисполнителям созданы.
        /// </summary>
        /// <returns>True, если можно выполнять задание.</returns>
        [Remote(IsPure = true)]
        public bool SL_IsCoAssigneeAssignamentCreated()
        {
            var task = ActionItemExecutionTasks.As(_obj.Task);
            var taskAssignees = task.CoAssignees.Select(c => c.Assignee).Distinct().ToList();
            var asgAssignees = ActionItemExecutionAssignments
                .GetAll(j => j.Task.ParentAssignment != null &&
                        Equals(task, j.Task.ParentAssignment.Task) &&
                        Equals(task.StartId, j.Task.ParentAssignment.TaskStartId) &&
                        Equals(ActionItemExecutionTasks.As(j.Task).ActionItemType, Sungero.RecordManagement.ActionItemExecutionTask.ActionItemType.Additional))
                .Select(c => c.Performer).Distinct().ToList();
            return taskAssignees.Count <= asgAssignees.Count;
        }
    }
}