﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.AnorsvMainSolution.Module.RecordManagementUI.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      base.Initializing(e);
      
      // Выдача прав на папки
      GrantAccessRightOnFolders();
    }
    
    private void GrantAccessRightOnFolders()
    {
      var allUsers = Roles.AllUsers;
      
      if (allUsers != null)
      {
        // Выдача прав на папку "Служебные записки"
        if (SpecialFolders.MemosSpecialFolderanorsv != null)
        {
          if (!SpecialFolders.MemosSpecialFolderanorsv.AccessRights.CanRead(allUsers))
          {
            SpecialFolders.MemosSpecialFolderanorsv.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
            SpecialFolders.MemosSpecialFolderanorsv.AccessRights.Save();
            InitializationLogger.Debug(Resources.InitSpecialFolderAccessRightsGrantedSuccefullFormat("Memos"));
          }
        }
        else
        {
          throw Sungero.Core.AppliedCodeException.Create(Resources.ErrorMemosSpecialFolderNotFoundOnInitialization);
        }
        
        // Выдача прав на папку "Внутренние документы"
        if (SpecialFolders.AllInternalDocumentsSpecialFolderanorsv != null)
        {
          if (!SpecialFolders.AllInternalDocumentsSpecialFolderanorsv.AccessRights.CanRead(allUsers))
          {
            SpecialFolders.AllInternalDocumentsSpecialFolderanorsv.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
            SpecialFolders.AllInternalDocumentsSpecialFolderanorsv.AccessRights.Save();
            InitializationLogger.Debug(Resources.InitSpecialFolderAccessRightsGrantedSuccefullFormat("Internal documents"));
          }
        }
        else
        {
          throw Sungero.Core.AppliedCodeException.Create(Resources.ErrorInternalDocumentsSpecialFolderNotFoundOnInitialization);
        }
      }
    }
    
  }
}
