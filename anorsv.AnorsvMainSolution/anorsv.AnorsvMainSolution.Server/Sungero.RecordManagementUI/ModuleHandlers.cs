using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.RecordManagementUI.Server
{
  partial class MemosSpecialFolderanorsvFolderHandlers
  {

    public virtual IQueryable<Sungero.Docflow.IDocumentKind> MemosSpecialFolderanorsvDocumentKindFiltering(IQueryable<Sungero.Docflow.IDocumentKind> query)
    {
      var kinds = Sungero.Docflow.PublicFunctions.DocumentKind.GetAvailableDocumentKinds(typeof(sline.RSV.IMemo));
      return query.Where(d => d.Status == Sungero.CoreEntities.DatabookEntry.Status.Active &&
                         d.DocumentType.DocumentFlow == Sungero.Docflow.DocumentType.DocumentFlow.Inner && kinds.Contains(d));
    }

    public virtual IQueryable<Sungero.Docflow.IDocumentRegister> MemosSpecialFolderanorsvDocumentRegisterFiltering(IQueryable<Sungero.Docflow.IDocumentRegister> query)
    {
      return query.Where(q => q.DocumentFlow == Sungero.Docflow.DocumentRegister.DocumentFlow.Inner);
    }

    public virtual IQueryable<sline.RSV.IMemo> MemosSpecialFolderanorsvDataQuery(IQueryable<sline.RSV.IMemo> query)
    {
      if (_filter == null)
        return query;
      
      // Не выводим Не нумеруемые документы.
      query = query.Where(d => d.DocumentKind.NumberingType != DocumentKind.NumberingType.NotNumerable);
      
      // Убираем приложения.
      var guid = Sungero.Docflow.Server.Addendum.ClassTypeGuid.ToString();
      query = query.Where(d => d.DocumentKind.DocumentType.DocumentTypeGuid != guid);
      
      // Не выводим документы по контрагенту.
      query = query.Where(d => !Sungero.Docflow.CounterpartyDocuments.Is(d));
      
      // Фильтр по журналу регистрации.
      if (_filter.DocumentRegister != null)
        query = query.Where(d => Equals(d.DocumentRegister, _filter.DocumentRegister));
      
      // Фильтр по виду документа.
      if (_filter.DocumentKind != null)
        query = query.Where(d => Equals(d.DocumentKind, _filter.DocumentKind));
      
      // Фильтр по статусу. Если все галочки включены, то нет смысла добавлять фильтр.
      if ((_filter.Registered || _filter.Reserved || _filter.NotRegistered) &&
          !(_filter.Registered && _filter.Reserved && _filter.NotRegistered))
        query = query.Where(l => _filter.Registered && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Registered ||
                            _filter.Reserved && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Reserved ||
                            _filter.NotRegistered && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.NotRegistered);
      
      // Фильтр по нашей организации.
      if (_filter.BusinessUnit != null)
        query = query.Where(d => Equals(d.BusinessUnit, _filter.BusinessUnit));
      
      // Фильтр "Подразделение".
      if (_filter.Department != null)
        query = query.Where(c => Equals(c.Department, _filter.Department));

      // Фильтр по интервалу времени
      var periodBegin = Calendar.UserToday.AddDays(-7);
      var periodEnd = Calendar.UserToday.EndOfDay();
      
      if (_filter.LastWeek)
        periodBegin = Calendar.UserToday.AddDays(-7);
      
      if (_filter.LastMonth)
        periodBegin = Calendar.UserToday.AddDays(-30);
      
      if (_filter.Last90Days)
        periodBegin = Calendar.UserToday.AddDays(-90);
      
      if (_filter.ManualPeriod)
      {
        periodBegin = _filter.DateRangeFrom ?? Calendar.SqlMinValue;
        periodEnd = _filter.DateRangeTo ?? Calendar.SqlMaxValue;
      }
      
      var serverPeriodBegin = Equals(Calendar.SqlMinValue, periodBegin) ? periodBegin : Sungero.Docflow.PublicFunctions.Module.Remote.GetTenantDateTimeFromUserDay(periodBegin);
      var serverPeriodEnd = Equals(Calendar.SqlMaxValue, periodEnd) ? periodEnd : periodEnd.EndOfDay().FromUserTime();
      var clientPeriodEnd = !Equals(Calendar.SqlMaxValue, periodEnd) ? periodEnd.AddDays(1) : Calendar.SqlMaxValue;
      query = query.Where(j => (j.DocumentDate.Between(serverPeriodBegin, serverPeriodEnd) ||
                                j.DocumentDate == periodBegin) && j.DocumentDate != clientPeriodEnd);
      
      // Фильтр по сотруднику из Объекта СЗ
      if (_filter.Employee != null)
        query = query.Where(c => Equals(c.Employeeanorsv, _filter.Employee));
      
      // Фильтр по подразделению из Объекта СЗ
      if (_filter.Subdivision != null)
        query = query.Where(c => Equals(c.Subdivisionanorsv, _filter.Subdivision));
      
      return query;
    }
  }

  partial class AllInternalDocumentsSpecialFolderanorsvFolderHandlers
  {

    public virtual IQueryable<Sungero.Docflow.IDocumentRegister> AllInternalDocumentsSpecialFolderanorsvDocumentRegisterFiltering(IQueryable<Sungero.Docflow.IDocumentRegister> query)
    {
      return query.Where(q => q.DocumentFlow == Sungero.Docflow.DocumentRegister.DocumentFlow.Inner);
    }

    public virtual IQueryable<Sungero.Docflow.IDocumentKind> AllInternalDocumentsSpecialFolderanorsvDocumentKindFiltering(IQueryable<Sungero.Docflow.IDocumentKind> query)
    {
      // При вызове во внутренних документах, не выводим Не нумеруемые виды документов.
      query = query.Where(d => d.NumberingType != Sungero.Docflow.DocumentKind.NumberingType.NotNumerable && d.DocumentType.IsRegistrationAllowed == true);
      
      // TODO Zamerov: явно убираем приложения.
      var guid = Sungero.Docflow.Server.Addendum.ClassTypeGuid.ToString();
      query = query.Where(d => d.DocumentType.DocumentTypeGuid != guid);
      
      // Убираем документы контрагента.
      var counterpartyDocumentTypeGuid = Sungero.Docflow.Server.CounterpartyDocument.ClassTypeGuid.ToString();
      query = query.Where(d => d.DocumentType.DocumentTypeGuid != counterpartyDocumentTypeGuid);
      
      var kinds = Sungero.Docflow.PublicFunctions.DocumentKind.GetAvailableDocumentKinds(typeof(Sungero.Docflow.IInternalDocumentBase));
      return query.Where(d => d.Status == Sungero.CoreEntities.DatabookEntry.Status.Active &&
                         d.DocumentType.DocumentFlow == Sungero.Docflow.DocumentType.DocumentFlow.Inner && kinds.Contains(d));
    }

    public virtual IQueryable<anorsv.InternalDocumentBase.IInternalDocumentBase> AllInternalDocumentsSpecialFolderanorsvDataQuery(IQueryable<anorsv.InternalDocumentBase.IInternalDocumentBase> query)
    {
      if (_filter == null)
        return query;
      
      // Не выводим Не нумеруемые документы.
      query = query.Where(d => d.DocumentKind.NumberingType != DocumentKind.NumberingType.NotNumerable);
      
      // Убираем приложения.
      var guid = Sungero.Docflow.Server.Addendum.ClassTypeGuid.ToString();
      query = query.Where(d => d.DocumentKind.DocumentType.DocumentTypeGuid != guid);
      
      // Не выводим документы по контрагенту.
      query = query.Where(d => !Sungero.Docflow.CounterpartyDocuments.Is(d));
      
      // Фильтр по журналу регистрации.
      if (_filter.DocumentRegister != null)
        query = query.Where(d => Equals(d.DocumentRegister, _filter.DocumentRegister));
      
      // Фильтр по виду документа.
      if (_filter.DocumentKind != null)
        query = query.Where(d => Equals(d.DocumentKind, _filter.DocumentKind));
      
      // Фильтр по статусу. Если все галочки включены, то нет смысла добавлять фильтр.
      if ((_filter.Registered || _filter.Reserved || _filter.NotRegistered) &&
          !(_filter.Registered && _filter.Reserved && _filter.NotRegistered))
        query = query.Where(l => _filter.Registered && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Registered ||
                            _filter.Reserved && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Reserved ||
                            _filter.NotRegistered && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.NotRegistered);
      
      // Фильтр по нашей организации.
      if (_filter.BusinessUnit != null)
        query = query.Where(d => Equals(d.BusinessUnit, _filter.BusinessUnit));
      
      // Фильтр "Подразделение".
      if (_filter.Department != null)
        query = query.Where(c => Equals(c.Department, _filter.Department));

      // Фильтр по интервалу времени
      var periodBegin = Calendar.UserToday.AddDays(-7);
      var periodEnd = Calendar.UserToday.EndOfDay();
      
      if (_filter.LastWeek)
        periodBegin = Calendar.UserToday.AddDays(-7);
      
      if (_filter.LastMonth)
        periodBegin = Calendar.UserToday.AddDays(-30);
      
      if (_filter.Last90Days)
        periodBegin = Calendar.UserToday.AddDays(-90);
      
      if (_filter.ManualPeriod)
      {
        periodBegin = _filter.DateRangeFrom ?? Calendar.SqlMinValue;
        periodEnd = _filter.DateRangeTo ?? Calendar.SqlMaxValue;
      }
      
      var serverPeriodBegin = Equals(Calendar.SqlMinValue, periodBegin) ? periodBegin : Sungero.Docflow.PublicFunctions.Module.Remote.GetTenantDateTimeFromUserDay(periodBegin);
      var serverPeriodEnd = Equals(Calendar.SqlMaxValue, periodEnd) ? periodEnd : periodEnd.EndOfDay().FromUserTime();
      var clientPeriodEnd = !Equals(Calendar.SqlMaxValue, periodEnd) ? periodEnd.AddDays(1) : Calendar.SqlMaxValue;
      query = query.Where(j => (j.DocumentDate.Between(serverPeriodBegin, serverPeriodEnd) ||
                                j.DocumentDate == periodBegin) && j.DocumentDate != clientPeriodEnd);
      
      return query;
    }
  }

  partial class RecordManagementUIHandlers
  {
  }
}
