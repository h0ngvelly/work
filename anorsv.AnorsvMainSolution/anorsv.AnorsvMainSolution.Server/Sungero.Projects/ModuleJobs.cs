﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.Projects.Server
{
  partial class ModuleJobs
  {

    /// <summary>
    /// Выдача прав на проекты и проектные папки.
    /// </summary>
    public override void GrantAccessRightsToProjectDocuments()
    {
      DateTime startDate = Calendar.Now;
      DateTime lastStartDate = Sungero.Docflow.PublicFunctions.Module.GetLastAgentRunDate(Sungero.Projects.Constants.Module.LastProjectDocumentRightsUpdateDate);
      
      // Измененные документы.
      Logger.DebugFormat("GrantAccessRightsToProjectDocuments: Start get changed projects documents.");
      var queue = new List<Sungero.Projects.Structures.ProjectDocumentRightsQueueItem.ProxyQueueItem>();
      var changedDocumentIds = anorsv
        .OfficialDocument
        .OfficialDocuments
        .GetAll(d => d.Modified >= lastStartDate
                && d.Modified <= startDate
                && (d.Project != null
                    || d.ProjectsCollectionanorsv.Count() > 0
                    || (Sungero.Docflow.Addendums.Is(d)
                        && (anorsv.OfficialDocument.OfficialDocuments.As(d.LeadingDocument).Project != null
                            || anorsv.OfficialDocument.OfficialDocuments.As(d.LeadingDocument).ProjectsCollectionanorsv.Count() > 0)
                       )
                   )
               )
        .Select(d => d.Id);
      
      foreach (var documentId in changedDocumentIds)
      {
        Sungero.Docflow.IOfficialDocument document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentId).FirstOrDefault();
        if (document == null)
          continue;
        
        var projects = anorsv.OfficialDocument.PublicFunctions.Module.GetProjects(document);
        foreach(Sungero.Docflow.IProjectBase project in projects)
          queue.AddRange(Functions.Module.CreateAccessRightsProjectDocumentQueueItemWithAddendum(documentId, project.Id));
        
      }
      
      // Измененные проекты.
      Logger.DebugFormat("GrantAccessRightsToProjectDocuments: Start get changed projects members documents.");
      var changedProjectsQueueItems = Sungero.Projects.ProjectMemberRightsQueueItems.GetAll().ToList();
      foreach (var memberQueueItem in changedProjectsQueueItems)
      {
        var project = Sungero.Projects.Projects.GetAll(p => p.Id == memberQueueItem.ProjectId).FirstOrDefault();
        if (project == null)
          continue;
        
        List<Sungero.Docflow.IProjectBase> allProjects = ModuleFunctions.GetProjectAndSubProjects(project).Select(p => Sungero.Docflow.ProjectBases.As(p)).ToList();
        
        var changedMembersDocuments = Sungero.Docflow.OfficialDocuments
          .GetAll(d => allProjects.Contains(d.Project)
                  || (anorsv.OfficialDocument.OfficialDocuments.Is(d)
                      && anorsv.OfficialDocument.OfficialDocuments.As(d)
                      .ProjectsCollectionanorsv.Any(p => allProjects.Contains(p.Project))
                     )
                 );
        
        foreach (var document in changedMembersDocuments)
          queue.AddRange(Functions.Module.CreateAccessRightsProjectDocumentQueueItemWithAddendum(document.Id, project.Id));
      }
      
      var table = Sungero.Projects.ProjectDocumentRightsQueueItems.Info.DBTableName;
      var ids = Sungero.Domain.IdentifierGenerator.GenerateIdentifiers(table, queue.Count).ToList();
      for (int i = 0; i < queue.Count; i++)
        queue[i].Id = ids[i];
      Sungero.Docflow.PublicFunctions.Module.WriteStructuresToTable(table, queue);
      Logger.DebugFormat("GrantAccessRightsToProjectDocuments: Added to queue {0} documents.", queue.Count);
      
      if (changedProjectsQueueItems.Any())
        Sungero.Docflow.PublicFunctions.Module.FastDeleteQueueItems(changedProjectsQueueItems.Select(q => q.Id).ToList());
      
      // Обновить дату запуска агента в базе.
      Sungero.Docflow.PublicFunctions.Module.UpdateLastAgentRunDate(Sungero.Projects.Constants.Module.LastProjectDocumentRightsUpdateDate, startDate);
      
      // Выдать права на документы.
      var step = 5;
      var error = 0;
      var isEmpty = false;
      for (int i = 0; i < 10000; i = i + step)
      {
        // Если элементов больше нет - заканчиваем.
        if (isEmpty)
          break;
        
        var result = Transactions.Execute(
          () =>
          {
            Logger.DebugFormat("GrantAccessRightsToProjectDocuments: Start process queue from {0}.", i);

            // Т.к. в конце транзакции элементы удаляются, в Take берем просто N новых элементов.
            var queueItemPart = Sungero.Projects.ProjectDocumentRightsQueueItems.GetAll().Skip(error).Take(step).ToList();
            if (!queueItemPart.Any())
            {
              // Завершаем транзакцию, если больше нечего обрабатывать.
              isEmpty = true;
              return;
            }

            var accessRightsGranted = queueItemPart
              .Where(q => Functions.Module.AddDocumentToFolder(q) && Functions.Module.GrantRightsToProjectDocuments(q))
              .ToList();
            if (accessRightsGranted.Any())
              Sungero.Docflow.PublicFunctions.Module.FastDeleteQueueItems(accessRightsGranted.Select(a => a.Id).ToList());
            error += queueItemPart.Count - accessRightsGranted.Count;
          });
        if (!result)
          error += step;
      }
    }

  }
}