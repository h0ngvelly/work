using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Shared;

namespace anorsv.AnorsvMainSolution.Module.Projects.Server
{
  partial class ModuleFunctions
  {
    #region Выдача прав на проектные документы

    /// <summary>
    /// Создать элементы очереди выдачи прав на документы по проектам, включая приложения к документу.
    /// </summary>
    /// <param name="documentId">ИД документа.</param>
    /// <param name="projectId">ИД проекта.</param>
    /// <returns>Список структур для сохранения в таблицу очереди выдачи прав.</returns>
    public new static List<Sungero.Projects.Structures.ProjectDocumentRightsQueueItem.ProxyQueueItem> CreateAccessRightsProjectDocumentQueueItemWithAddendum(int documentId, int projectId)
    {
      var queue = new List<Sungero.Projects.Structures.ProjectDocumentRightsQueueItem.ProxyQueueItem>();
      var document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentId).FirstOrDefault();
      if (document == null)
        return queue;

      queue.Add(CreateAccessRightsProjectDocumentQueueItem(documentId, projectId));
      
      // Приложения по документу тоже собрать.
      var addenda = GetAddendums(document);
      foreach (var addendum in addenda)
        queue.Add(CreateAccessRightsProjectDocumentQueueItem(addendum.Id, projectId));
      return queue;
    }
    
    private static List<Sungero.Docflow.IOfficialDocument> GetAddendums(Sungero.Docflow.IOfficialDocument document)
    {
      var addendaList = new List<Sungero.Docflow.IOfficialDocument>() { };
      var leadingDocuments = new List<Sungero.Docflow.IOfficialDocument>() { document };
      while (Sungero.Docflow.Addendums.GetAll(a => leadingDocuments.Contains(a.LeadingDocument)).Any())
      {
        leadingDocuments = Sungero.Docflow.OfficialDocuments.GetAll(a => Sungero.Docflow.Addendums.Is(a) && leadingDocuments.Contains(a.LeadingDocument)).ToList();
        addendaList.AddRange(leadingDocuments);
      }
      
      return addendaList;
    }
    
    /// <summary>
    /// Создать элемент очереди выдачи прав на документы по проектам.
    /// </summary>
    /// <param name="documentId">ИД документа.</param>
    /// <param name="projectId">ИД проекта.</param>
    /// <returns>Структура для сохранения в таблицу очереди выдачи прав.</returns>
    public new static Sungero.Projects.Structures.ProjectDocumentRightsQueueItem.ProxyQueueItem CreateAccessRightsProjectDocumentQueueItem(int documentId, int projectId)
    {
      Logger.DebugFormat("CreateProjectDocumentRightsQueueItem: document {0}, project {1}", documentId, projectId);
      var queueItem = Sungero.Projects.Structures.ProjectDocumentRightsQueueItem.ProxyQueueItem.Create();
      queueItem.Discriminator = Sungero.Projects.Server.ProjectDocumentRightsQueueItem.ClassTypeGuid;
      queueItem.DocumentId_Project_Sungero = documentId;
      queueItem.ProjectId_Project_Sungero = projectId;
      return queueItem;
    }
    
    /// <summary>
    /// Получить права участников проекта.
    /// </summary>
    /// <param name="queueItem">Элемент очереди.</param>
    /// <param name="document">Документ.</param>
    /// <param name="project">Проект.</param>
    /// <returns>Список прав в виде реципиент-тип прав.</returns>
    internal List<Sungero.Projects.Structures.Module.RecipientRights>
      GetProjectRecipientRights(Sungero.Projects.IProjectDocumentRightsQueueItem queueItem
                                , Sungero.Docflow.IOfficialDocument document
                                , Sungero.Docflow.IProjectBase project)
    {
      var result = new List<Sungero.Projects.Structures.Module.RecipientRights>();
      // Выдать права на сам документ.
      this.AddRecipientRightsForProject(document, project, document.DocumentKind.GrantRightsToProject.Value, result);
      this.GrantRightsOnDocumentToLeadingProgect(document, result);

      return result;
    }
    
    /// <summary>
    /// Выдать права на документ вышестоящим проектам.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="result">Результат.</param>
    private void GrantRightsOnDocumentToLeadingProgect(Sungero.Docflow.IOfficialDocument document
                                                       , List<Sungero.Projects.Structures.Module.RecipientRights> result)
    {
      var grantRightsDocument = document;
      while (document.LeadingDocument != null)
      {
        var leadingProjectBase = document.LeadingDocument.Project;
        var leadingProject = leadingProjectBase != null ? Sungero.Projects.Projects.GetAll(p => p.Id == leadingProjectBase.Id).FirstOrDefault() : Sungero.Projects.Projects.Null;
        this.AddRecipientRightsForProject(grantRightsDocument, leadingProject, document.LeadingDocument.DocumentKind.GrantRightsToProject.Value, result);
        foreach (var projectBase in anorsv.OfficialDocument.OfficialDocuments.As(document.LeadingDocument).ProjectsCollectionanorsv.Where(p => p.Project != null).Select(p => p.Project))
        {
          leadingProject = projectBase != null ? Sungero.Projects.Projects.GetAll(p => p.Id == projectBase.Id).FirstOrDefault() : Sungero.Projects.Projects.Null;
          this.AddRecipientRightsForProject(grantRightsDocument, leadingProject, document.LeadingDocument.DocumentKind.GrantRightsToProject.Value, result);
        }
        
        document = document.LeadingDocument;
      }
    }
    
    /// <summary>
    /// Выдать права на документы.
    /// </summary>
    /// <param name="queueItem">Элемент очереди.</param>
    /// <returns>Признак успешности выдачи прав.</returns>
    public new bool GrantRightsToProjectDocuments(Sungero.Projects.IProjectDocumentRightsQueueItem queueItem)
    {
      var document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == queueItem.DocumentId.Value).FirstOrDefault();
      if (document == null)
        return true;

      var isChanged = false;
      var accessRights = this.GetProjectRecipientRights(queueItem, document, document.Project);
      foreach (var accessRight in accessRights)
      {
        var accessRightsType = Sungero.Docflow.PublicFunctions.Module.GetRightTypeGuid(new Enumeration(accessRight.AccessRights));
        if (!document.AccessRights.IsGrantedDirectly(accessRightsType, accessRight.Recipient))
        {
          if (Locks.GetLockInfo(document).IsLockedByOther)
            return false;
          
          document.AccessRights.Grant(accessRight.Recipient, accessRightsType);
          isChanged = true;
        }
      }

      if (isChanged)
      {
        ((Sungero.Domain.Shared.IExtendedEntity)document).Params[Sungero.Docflow.PublicConstants.OfficialDocument.DontUpdateModified] = true;
        document.Save();
      }
      
      return true;
    }

    /// <summary>
    /// Заполнить список прав на проект.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="projectBase">Проект.</param>
    /// <param name="grantRightsToProject">True, если выдавать права участникам проектов. Иначе - false.</param>
    /// <param name="result">Список прав в виде реципиент-тип прав.</param>
    public override void AddRecipientRightsForProject(Sungero.Docflow.IOfficialDocument document
                                                      , Sungero.Docflow.IProjectBase projectBase
                                                      , bool grantRightsToProject
                                                      , List<Sungero.Projects.Structures.Module.RecipientRights> result)
    {
      if (projectBase == null)
        return;
      
      if (Sungero.Projects.Projects.Is(projectBase))
        return;
      
      var project = Sungero.Projects.Projects.As(projectBase);
      var readType = Sungero.Projects.Constants.Module.AccessRightsReadTypeName;
      var editType = Sungero.Projects.Constants.Module.AccessRightsEditTypeName;

      if (IsProjectDocument(document))
        result.Add(Sungero.Projects.Structures.Module.RecipientRights.Create(project.Manager, editType));

      if (grantRightsToProject == true)
      {
        if (project.Administrator != null)
          result.Add(Sungero.Projects.Structures.Module.RecipientRights.Create(project.Administrator, editType));
        
        if (project.InternalCustomer != null)
          result.Add(Sungero.Projects.Structures.Module.RecipientRights.Create(project.InternalCustomer, readType));

        foreach (var teamMember in project.TeamMembers)
        {
          result.Add(teamMember.Group == Sungero.Projects.ProjectTeamMembers.Group.Management
                     ? Sungero.Projects.Structures.Module.RecipientRights.Create(teamMember.Member, editType)
                     : Sungero.Projects.Structures.Module.RecipientRights.Create(teamMember.Member, readType));
        }
      }
      
      if (project.LeadingProject != null)
        this.AddRecipientRightsForProject(document, project.LeadingProject, grantRightsToProject, result);
    }

    /// <summary>
    /// Запустить фоновый процесс "Проекты. Автоматическое назначение прав на документы".
    /// </summary>
    [Public, Remote(IsPure = false)]
    public new static void RequeueProjectDocumentRightsSync()
    {
      Sungero.Projects.Jobs.GrantAccessRightsToProjectDocuments.Enqueue();
    }

    /// <summary>
    /// Вложить документ в папку проекта.
    /// </summary>
    /// <param name="queueItem">Элемент очереди.</param>
    /// <returns>True, если элемент очереди обработан.</returns>
    public override bool AddDocumentToFolder(Sungero.Projects.IProjectDocumentRightsQueueItem queueItem)
    {
      var document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == queueItem.DocumentId.Value).FirstOrDefault();
      if (document == null)
        return true;

      var project = Sungero.Projects.Projects.GetAll(d => d.Id == queueItem.ProjectId.Value).FirstOrDefault();
      if (project == null)
        return true;

      if (!IsDocumentBelongProject(document, project))
        return true;

      Sungero.Projects.PublicFunctions.ProjectCore.AddDocumentToFolder(project, document);
      
      return true;
    }

    /// <summary>
    /// Проверка, относится ли документ к проекту.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="project">Проект.</param>
    /// <returns>Если документ относится к проекту, то true, иначе - false.</returns>
    public new static bool IsDocumentBelongProject(Sungero.Docflow.IOfficialDocument document, Sungero.Projects.IProjectCore project)
    {
      List<Sungero.Projects.IProjectCore> documentProjects = anorsv.OfficialDocument.PublicFunctions.Module
        .GetProjects(document)
        .ToList()
        .Select(p => Sungero.Projects.ProjectCores.As(p)).ToList();
        
      List<Sungero.Projects.IProjectCore> projects = new List<Sungero.Projects.IProjectCore>();
      foreach(var documentProject in documentProjects)
      {
        projects = projects.Union(GetProjectAndSubProjects(documentProject)).ToList();
      }
      return documentProjects.Count() > 0 ? documentProjects.Any(dp => projects.Contains(dp)) : false;
    }
    
    [Public]
    public static bool IsProjectDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var analyzedDocument = anorsv.OfficialDocument.OfficialDocuments.As(document);
      if (analyzedDocument != null)
        return (analyzedDocument.Project != null || analyzedDocument.ProjectsCollectionanorsv.Any(p => p.Project != null) && analyzedDocument.DocumentKind.ProjectsAccounting.Value);
      else
        return document.Project != null;
    }
    
    #endregion

    #region Информация о проектах
    
    /// <summary>
    /// Получить проект с его подпроектами.
    /// </summary>
    /// <param name="project">Проект.</param>
    /// <returns>Список проектов.</returns>
    [Public]
    public new static List<Sungero.Projects.IProjectCore> GetProjectAndSubProjects(Sungero.Projects.IProject project)
    {
      List<Sungero.Projects.IProjectCore> allProjects = new List<Sungero.Projects.IProjectCore>() { project};
      List<Sungero.Projects.IProjectCore> leadingProjects = new List<Sungero.Projects.IProjectCore>() { project };
      
      while (Sungero.Projects.ProjectCores.GetAll().Any(p => leadingProjects.Contains(p.LeadingProject)))
      {
        leadingProjects = Sungero.Projects.ProjectCores.GetAll(p => leadingProjects.Contains(p.LeadingProject)).ToList();
        allProjects.AddRange(leadingProjects);
      }
      
      return allProjects;
    }
    
    #endregion
  }
}