﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.AnorsvMainSolution.Module.Projects.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      base.Initializing(e);
      RevokeRightsOnBaseSpecialFolders();
    }
    
    public static void RevokeRightsOnBaseSpecialFolders()
    {
      var allUsers = Roles.AllUsers;
      if (anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectDocuments != null)
      {
        anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectDocuments.AccessRights.RevokeAll(allUsers);
        anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectDocuments.AccessRights.Save();
      }
      
      if (anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectsPlansDirRX != null)
      {
        anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectsPlansDirRX.AccessRights.RevokeAll(allUsers);
        anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectsPlansDirRX.AccessRights.Save();
      }
      
      if (anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectTasksSungero != null)
      {
        anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectTasksSungero.AccessRights.RevokeAll(allUsers);
        anorsv.AnorsvMainSolution.Module.Projects.SpecialFolders.ProjectTasksSungero.AccessRights.Save();
      }
      
      InitializationLogger.Debug(Resources.InitilizeRevokeAccessRightsOnProjetcsExecutionSpecialFolder);
    }
  }

}
