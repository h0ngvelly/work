﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.AcquaintanceTask;
using docRelation = anorsv.RelationModule;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class AcquaintanceTaskActions
  {
    public virtual void ShowSubstitutedMessageanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var message = "";
      e.Params.TryGetValue("ShowSubstituteMessage", out message);
      Dialogs.ShowMessage(message, MessageType.Warning);
    }

    public virtual bool CanShowSubstitutedMessageanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }
    
    public override void Start(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Диалог выбора связанных документов для добавления во вложения
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document == null)
        return;
      docRelation.PublicFunctions.Module.AddOtherGroupAttachmentsDialog(Sungero.RecordManagement.AcquaintanceTasks.As(_obj), document);
            
      base.Start(e);
    }

    public override bool CanStart(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanStart(e);
    }

  }

}
