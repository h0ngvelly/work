﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.AcquaintanceTask;
using Sungero.Company;

namespace anorsv.AnorsvMainSolution
{

  partial class AcquaintanceTaskClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
//      /*Рахимова. Вывод сообщения о замещаемых согласующих*/
      var approvers = new List <Sungero.Company.IEmployee> ();
      
      // Кого ознакомить и Наблюдатели
      approvers.AddRange(_obj.Performers.Select(a=>Sungero.Company.Employees.As(a.Performer)).ToList());
      if(_obj.Observers.Any())
      approvers.AddRange(_obj.Observers.Select(a=>Sungero.Company.Employees.As(a.Observer)).ToList());
      
      var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, _obj.MaxDeadline);
      var substituteCount = messageList.Count;
      //message = message.Split('%') [1];
      
      if (messageList.Any())
      {
        if (substituteCount < 4)
          e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
        else
        {
          e.Params.AddOrUpdate("ShowSubstituteMessage", string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
          e.AddInformation(anorsv.ApprovalTask.ApprovalTasks.Resources.ShowSubstitutionsByTaskPerformers, _obj.Info.Actions.ShowSubstitutedMessageanorsv);
        }
      }
    }

  }
}