﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DocumentKind;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class DocumentKindActions
  {
    public virtual void ChangeConfidentialityanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Принудительно сохранить запись
      _obj.Save();
      // Запустить асинхронный обработчик изменения конфиденциальности в документах
      var asyncUpdateConfidentiality = anorsv.DocflowModule.AsyncHandlers.AsyncUpdateConfidentiality.Create();
      asyncUpdateConfidentiality.DocKindId = _obj.Id;
      asyncUpdateConfidentiality.DocKindConf = (_obj.Confidentialanorsv == true) ? true : false;
      asyncUpdateConfidentiality.ExecuteAsync();
    }

    public virtual bool CanChangeConfidentialityanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return Users.Current.IncludedIn(Roles.Administrators);
    }
  
  }

}