﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DocumentKind;

namespace anorsv.AnorsvMainSolution
{
  partial class DocumentKindClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      _obj.State.Properties.Confidentialanorsv.IsEnabled = Users.Current.IncludedIn(Roles.Administrators);
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      _obj.State.Properties.Confidentialanorsv.IsEnabled = Users.Current.IncludedIn(Roles.Administrators);
    }

  }
}