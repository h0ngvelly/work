﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.FreeApprovalReworkAssignment;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class FreeApprovalReworkAssignmentActions
  {
    public override void Reworked(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      
      if (!success)
      {
        e.Cancel();
      }

      base.Reworked(e);
    }

    public override bool CanReworked(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanReworked(e);
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote
        .CreateFormalizedSubTask(_obj);
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess 
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

    public virtual void CreateManyAddendumsline(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.ForApprovalGroup.ElectronicDocuments.FirstOrDefault();
      if (document != null)
      {
        anorsv.OfficialDocumentModule.PublicFunctions.Module
          .AddManyAddendumDialog(Sungero.Docflow.OfficialDocuments.As(document));
        Sungero.Docflow.PublicFunctions.Module
          .SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, Sungero.Docflow.OfficialDocuments.As(document));
        Sungero.Docflow.PublicFunctions.OfficialDocument
          .AddRelatedDocumentsToAttachmentGroup(Sungero.Docflow.OfficialDocuments.As(document), _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumsline(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}