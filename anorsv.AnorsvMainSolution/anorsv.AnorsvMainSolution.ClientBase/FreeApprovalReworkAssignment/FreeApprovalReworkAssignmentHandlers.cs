using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.FreeApprovalReworkAssignment;

namespace anorsv.AnorsvMainSolution
{
  partial class FreeApprovalReworkAssignmentClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      if (_obj.Approvers.Any())
      {
        var approvers = new List <Sungero.Company.IEmployee> ();
        approvers.AddRange(_obj.Approvers.Select(a => a.Approver).ToList());
        var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, _obj.Deadline);
        
        if (messageList.Any())
        {
          e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
        }
      }
    }

    public override void Closing(Sungero.Presentation.FormClosingEventArgs e)
    {
      base.Closing(e);
      
      anorsv.ServiceLibrary.PublicFunctions.Module.MarkSubtaskExistInAssignmentSubject(_obj);
    }

  }
}