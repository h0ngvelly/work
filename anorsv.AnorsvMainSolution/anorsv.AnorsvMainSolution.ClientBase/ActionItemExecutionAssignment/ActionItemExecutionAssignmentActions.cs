﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ActionItemExecutionAssignment;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class ActionItemExecutionAssignmentActions
  {
    public virtual void CreateManyAddendumsline(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentsGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumsline(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public override void Done(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      if (string.IsNullOrWhiteSpace(_obj.ActiveText))
      {
        e.AddError(ActionItemExecutionAssignments.Resources.ReportIsNotFilled);
        return;
      }
      
      if (!Functions.ActionItemExecutionAssignment.Remote.SL_IsCoAssigneeAssignamentCreated(_obj))
      {
        Dialogs.NotifyMessage(ActionItemExecutionAssignments.Resources.AssignmentsNotCreated);
        e.Cancel();
      }
      
      var giveRights = Sungero.Docflow.PublicFunctions.Module
        .ShowDialogGrantAccessRights(_obj, _obj.ResultGroup.All.Concat(_obj.OtherGroup.All).ToList());
      if (giveRights == false)
        e.Cancel();
      
      // Проверить наличие подчиненных поручений.
      var subActionItemExecutions = Functions.ActionItemExecutionTask.Remote.SL_GetSubActionItemExecutions(_obj);
      if (!subActionItemExecutions.Any())
      {
        if (giveRights == null)
        {
          // Замена стандартного диалога подтверждения выполнения действия.
          if (!Sungero.Docflow.PublicFunctions.Module
            .ShowConfirmationDialog(
              e.Action.ConfirmationMessage, null, null,
              "bd3560c8-dee0-4fce-a17f-005b170e7835"))
            e.Cancel();
        }
      }
      else
      {
        var confirmationDialog = Dialogs.CreateTaskDialog(
          ActionItemExecutionTasks.Resources.StopAdditionalActionItemExecutions,
          ActionItemExecutionTasks.Resources.StopAdditionalActionItemExecutionsDescription,
          MessageType.Question);
        var abort = confirmationDialog.Buttons.AddCustom(ActionItemExecutionAssignments.Resources.Abort);
        confirmationDialog.Buttons.Default = abort;
        var notAbort = confirmationDialog.Buttons.AddCustom(ActionItemExecutionAssignments.Resources.NotAbort);
        confirmationDialog.Buttons.AddCancel();
        var result = confirmationDialog.Show();
        
        // Необходимость прекращения подчиненных поручений.
        if (result == abort)
          _obj.NeedAbortChildActionItems = true;
        
        // Отменить выполнения задания.
        if (result == DialogButtons.Cancel)
          e.Cancel();
      }
    }

    public override bool CanDone(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanDone(e);
    }

    public virtual void Redirect(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      if (_obj.RedirectToanorsv == null)
      {
        e.AddError("Выберите сотрудника для переадресации поручения!");
        return;
      }
      
      var giveRights = Sungero.Docflow.PublicFunctions.Module.ShowDialogGrantAccessRights(_obj, _obj.ResultGroup.All.Concat(_obj.OtherGroup.All).ToList());
      if (giveRights == false)
        e.Cancel();
    }

    public virtual bool CanRedirect(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return true;
    }

  }



}