﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ActionItemExecutionTask;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class ActionItemExecutionTaskActions
  {
    public virtual void ShowSubstitutedMessageanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var message = "";
      e.Params.TryGetValue("ShowSubstituteMessage", out message);
      Dialogs.ShowMessage(message, MessageType.Warning);
    }

    public virtual bool CanShowSubstitutedMessageanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public virtual void CreateManyAddendumsline(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentsGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
    }

    public virtual bool CanCreateManyAddendumsline(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}