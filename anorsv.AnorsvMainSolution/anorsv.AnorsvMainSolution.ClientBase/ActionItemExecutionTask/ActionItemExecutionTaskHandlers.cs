using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ActionItemExecutionTask;

namespace anorsv.AnorsvMainSolution
{

  partial class ActionItemExecutionTaskClientHandlers
  {
    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      /*Рахимова. Вывод сообщения о замещаемых согласующих*/
      var approvers = new List <Sungero.Company.IEmployee> ();
      
      // Исполнитель и соисполнители
      approvers.Add(_obj.Assignee);
      if (_obj.CoAssignees.Any())
        approvers.AddRange(_obj.CoAssignees.Select(a=>a.Assignee).ToList());
      
      var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, _obj.Deadline);
      int substituteCount = messageList.Count;

      if (messageList.Any())
      {
        if (substituteCount < 4)
          e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
        else
        {
          e.Params.AddOrUpdate("ShowSubstituteMessage", string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
          e.AddInformation(anorsv.ApprovalTask.ApprovalTasks.Resources.ShowSubstitutionsByTaskPerformers, _obj.Info.Actions.ShowSubstitutedMessageanorsv);
        }
      }
    }

  }
}