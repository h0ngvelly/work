﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ActionItemSupervisorAssignment;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class ActionItemSupervisorAssignmentActions
  {
    public override void Agree(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      base.Agree(e);
    }

    public override bool CanAgree(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanAgree(e);
    }


    public virtual void CreateManyAddendumsline(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentsGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumsline(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}