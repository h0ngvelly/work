using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.PowerOfAttorneyBase;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class PowerOfAttorneyBaseActions
  {
    public virtual void RevokePowerOfAttorneyanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      
    }

    public virtual bool CanRevokePowerOfAttorneyanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}