﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.FreeApprovalTask;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class FreeApprovalTaskActions
  {
    public virtual void ShowSubstitutedMessageanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var message = "";
      e.Params.TryGetValue("ShowSubstituteMessage", out message);
      Dialogs.ShowMessage(message, MessageType.Warning);
    }

    public virtual bool CanShowSubstitutedMessageanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}