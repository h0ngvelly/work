﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.FreeApprovalTask;

namespace anorsv.AnorsvMainSolution
{
  partial class FreeApprovalTaskClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      /*Рахимова. Вывод сообщения о замещаемых согласующих*/
      var approvers = new List <Sungero.Company.IEmployee> ();
      
      // Обязательные согласующие.
      var recipients = _obj.Approvers.Select(a => a.Approver).ToList();
      recipients.AddRange(_obj.Observers.Select(o => o.Observer).ToList());
      
      approvers.AddRange(Sungero.Docflow.PublicFunctions.Module.GetEmployeesFromRecipients(recipients));
            
      var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, _obj.MaxDeadline);
      var substituteCount = messageList.Count;
      
      if (messageList.Any())
      {
        if (substituteCount < 4)
          e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
        else
        {
          e.Params.AddOrUpdate("ShowSubstituteMessage", string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
          e.AddInformation(anorsv.ApprovalTask.ApprovalTasks.Resources.ShowSubstitutionsByTaskPerformers, _obj.Info.Actions.ShowSubstitutedMessageanorsv);
        }
      }
    }

  }
}