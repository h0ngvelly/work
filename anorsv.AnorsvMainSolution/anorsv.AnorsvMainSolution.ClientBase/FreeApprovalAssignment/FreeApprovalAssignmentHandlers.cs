using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.FreeApprovalAssignment;
using Sungero.Domain.Shared;

namespace anorsv.AnorsvMainSolution
{
  partial class FreeApprovalAssignmentClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      //base.Showing(e);
      e.Entity.State.IsEnabled = false;
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      if (_obj.Addressee != null)
      {
        var approvers = new List <Sungero.Company.IEmployee> ();
        approvers.Add(_obj.Addressee);
        var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, _obj.Deadline);
        
        if (messageList.Any())
        {
          e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
        }
      }
    }

    public override void Closing(Sungero.Presentation.FormClosingEventArgs e)
    {
      base.Closing(e);
      
      anorsv.ServiceLibrary.PublicFunctions.Module.MarkSubtaskExistInAssignmentSubject(_obj);
    }

  }
}