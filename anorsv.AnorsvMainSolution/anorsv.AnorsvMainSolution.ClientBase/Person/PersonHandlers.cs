﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.Person;

namespace anorsv.AnorsvMainSolution
{
  partial class PersonClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      // Если с персоной связан работник, блокировать карточку от изменения для всех, кроме администраторов
      var canModifyPerson = !Functions.Person.Remote.CheckEmployeeForPerson(_obj) || 
        Users.Current.IncludedIn(Roles.Administrators);
      e.Params.AddOrUpdate("AnoRsvCanModifyPerson", canModifyPerson);
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      bool canModifyPerson = true;
      if (e.Params.Contains("AnoRsvCanModifyPerson"))
        if (e.Params.TryGetValue("AnoRsvCanModifyPerson", out canModifyPerson) && !canModifyPerson)
        {
          foreach (var prop in _obj.State.Properties)
            prop.IsEnabled = false;
        }
    }

  }
}