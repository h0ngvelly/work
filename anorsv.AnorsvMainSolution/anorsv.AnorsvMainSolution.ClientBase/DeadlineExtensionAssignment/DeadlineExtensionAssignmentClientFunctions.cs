﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DeadlineExtensionAssignment;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class DeadlineExtensionAssignmentFunctions
  {
    /// <summary>
    /// 
    /// </summary> 
    [Public]
    public bool SubtaskDeadlineNotChanged()
    {
      var dialog = Dialogs.CreateTaskDialog(anorsv.AnorsvMainSolution.DeadlineExtensionAssignments.Resources.DeadlineWarningMessage, anorsv.AnorsvMainSolution.DeadlineExtensionAssignments.Resources.DeadlineWarningMessageSecond, MessageType.Question, "Предупреждение");
      
      var pressButton = dialog.Buttons.AddCustom("Принять");
      dialog.Buttons.AddCancel();
      
      var result = dialog.Show();
      
      if (result == DialogButtons.Cancel)
        return false;
      else 
        return true;
    }

  }
}