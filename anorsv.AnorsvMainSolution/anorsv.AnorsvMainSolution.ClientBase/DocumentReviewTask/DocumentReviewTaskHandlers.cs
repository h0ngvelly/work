﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DocumentReviewTask;

namespace anorsv.AnorsvMainSolution
{
  partial class DocumentReviewTaskClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      // Рахимова. Вывод сообщения о замещаемых согласующих
      var approvers = new List <Sungero.Company.IEmployee> ();
      
      // Кого ознакомить и Наблюдатели
      if(_obj.Addressees.Any())
        approvers.AddRange(_obj.Addressees.Select(a=>Sungero.Company.Employees.As(a.Addressee)).ToList());
      approvers.Add(_obj.Addressee);
      if (_obj.ResolutionObservers.Any())
        approvers.AddRange(_obj.ResolutionObservers.Select(a=>Sungero.Company.Employees.As(a.Observer)).ToList());
      
      var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, _obj.Deadline);
      int substituteCount = messageList.Count;
      
      if (messageList.Any())
      {
        if (substituteCount < 4)
          e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
        else
        {
          e.Params.AddOrUpdate("ShowSubstituteMessage", string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
          e.AddInformation(anorsv.ApprovalTask.ApprovalTasks.Resources.ShowSubstitutionsByTaskPerformers, _obj.Info.Actions.ShowSubstitutedMessageanorsv);
        }
      }
    }

  }
}