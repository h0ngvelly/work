﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.DocumentReviewTask;

namespace anorsv.AnorsvMainSolution.Client
{
    partial class DocumentReviewTaskActions
    {
        public virtual void ShowSubstitutedMessageanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            var message = "";
            e.Params.TryGetValue("ShowSubstituteMessage", out message);
            Dialogs.ShowMessage(message, MessageType.Warning);
        }
    
        public virtual bool CanShowSubstitutedMessageanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return true;
        }

        
        public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            base.CreateSubtask(e);
        }
    
        public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return false;
        }
        
        public override void Start(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            base.Start(e);
        }
    
        public override bool CanStart(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanStart(e);
        }
    }
}