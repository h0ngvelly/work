﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.RecordManagementUI.Client
{
  partial class ModuleFunctions
  {
    /// <summary>
    /// Настроить визуальную доступность для пользователей разработанного функционала после переноса
    /// </summary>
    public virtual void CustomFunctionalityAvailableParameter()
    {
      if (Users.Current.IncludedIn(Roles.Administrators))
      {
        var dialog = Dialogs.CreateInputDialog("");
        var contractAttGroupAvailableOldValue = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
        var orderAttGroupAvailableOldValue = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
        var contractAttGroupAvailable = dialog.AddBoolean(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.ContractAttGroup, contractAttGroupAvailableOldValue);        
        var orderAttGroupAvailable = dialog.AddBoolean(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.OrderAttGroup, orderAttGroupAvailableOldValue);
        
        if (dialog.Show() == DialogButtons.Ok)
        {
          Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("ContractAttGroupAvailable", contractAttGroupAvailable.Value.ToString());
          Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("OrderAttGroupAvailable", orderAttGroupAvailable.Value.ToString());
        }
      }
    }
    
    /// <summary>
    /// Получить/задать параметры БЧП.
    /// </summary>
    [Public]
    public virtual void GetSetBCHPParams()
    {
      var bCHPParams = anorsv.RecordManagement.Module.RecordManagement.PublicFunctions.Module.Remote.GetBCHPParams();
      
      var employee = sline.RSV.Employees.Null;
      var employeeId = bCHPParams.DefaultBCHPOperatorId;
      if (employeeId != 0)
        employee = sline.RSV.Employees.Get(employeeId);
      var dialog = Dialogs.CreateInputDialog(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.ParametersTitle);
      
      var defaultOperator = dialog.AddSelect(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.BCHPDefaultOperator, true, employee);
      var testZip = dialog.AddBoolean(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.IsArchive, bCHPParams.TestZip);
      var testZipURL = dialog.AddString(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.ZipPath, false, bCHPParams.TestZipURL);
      testZipURL.IsEnabled = false;
      testZip.SetOnValueChanged(x =>
                                {
                                  if(testZip.Value.Value)
                                    testZipURL.IsEnabled = true;
                                  else
                                    testZipURL.IsEnabled = false;
                                }
                               );
      
      var mailServer = dialog.AddString(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.MailServerAddress, true, bCHPParams.MailServer);
      var mailServerPort = dialog.AddInteger(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.MailServerPort, true, bCHPParams.MailServerPort);
      var mailTo = dialog.AddString(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.EmailAddress, true, bCHPParams.MailTo);
      var mailToPass = dialog.AddPasswordString(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.Password, true, bCHPParams.MailToPass);
      var MailToDisplayName = dialog.AddString(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.MailSignature, true, bCHPParams.MailToDisplayName);
      
      if (dialog.Show() == DialogButtons.Ok)
      {
        if (bCHPParams.DefaultBCHPOperatorId != defaultOperator.Value.Id ||
            bCHPParams.TestZip != testZip.Value.Value ||
            bCHPParams.TestZipURL != testZipURL.Value ||
            bCHPParams.MailServer != mailServer.Value ||
            bCHPParams.MailServerPort != mailServerPort.Value ||
            bCHPParams.MailTo != mailTo.Value ||
            bCHPParams.MailToPass != mailToPass.Value ||
            bCHPParams.MailToDisplayName != MailToDisplayName.Value)
        {
          bCHPParams.DefaultBCHPOperatorId = defaultOperator.Value.Id;
          bCHPParams.TestZip = testZip.Value.Value;
          bCHPParams.TestZipURL = testZipURL.Value;
          bCHPParams.MailServer = mailServer.Value;
          bCHPParams.MailServerPort = mailServerPort.Value.Value;
          bCHPParams.MailTo = mailTo.Value;
          bCHPParams.MailToPass = mailToPass.Value;
          bCHPParams.MailToDisplayName = MailToDisplayName.Value;
          anorsv.RecordManagement.Module.RecordManagement.PublicFunctions.Module.Remote.SetBCHPParams(bCHPParams);
          Dialogs.NotifyMessage(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.ParametersSaved);
        }
      }
    }
    
    /// <summary>
    /// 
    /// </summary>
    public virtual void SubstitutePeriodParametr()
    {
      var dialog = Dialogs.CreateInputDialog("");
      int oldDaysCount = 30;
      oldDaysCount = Convert.ToInt32(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("SubstitutePeriod"));
      
      var newDaysCount = dialog.AddInteger("Период замещения, дни", true, oldDaysCount);
      
      if (dialog.Show() == DialogButtons.Ok)
      {
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("SubstitutePeriod", newDaysCount.Value.ToString());
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void DaysForEndExecutionOrders()
    {
      var dialog = Dialogs.CreateInputDialog("");
      int oldDaysCountForSentNotification = 2;
      int oldOrderTerm = 5;
      oldDaysCountForSentNotification = Convert.ToInt32(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("DaysForEndExecutionOrders"));
      oldOrderTerm = Convert.ToInt32(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderTermCVD"));
      
      var newDaysCountForSentNotification = dialog.AddInteger(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.DaysNotify, true, oldDaysCountForSentNotification);
      var newOrderTerm = dialog.AddInteger(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.TermOrderExceeds, true, oldOrderTerm);
      
      if (dialog.Show() == DialogButtons.Ok)
      {
        if (newDaysCountForSentNotification.Value > newOrderTerm.Value)
          Dialogs.ShowMessage(anorsv.AnorsvMainSolution.Module.RecordManagementUI.Resources.DaysForEndExecutionWarningMessage, MessageType.Error);
        else
        {
          Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("DaysForEndExecutionOrders", newDaysCountForSentNotification.Value.ToString());
          Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam("OrderTermCVD", newOrderTerm.Value.ToString());
          
        }
      }
    }

  }
}