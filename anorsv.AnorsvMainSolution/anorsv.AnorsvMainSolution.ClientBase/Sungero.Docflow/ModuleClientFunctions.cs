﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.Docflow.Client
{
  partial class ModuleFunctions
  {

    /// <summary>
    /// Список задач на исполнение поручений по фед.проектам
    /// </summary>
    [Public]
    public virtual void ShowFedProjectsActionItemExecitionTasks()
    {
      var taskList = anorsv.AnorsvMainSolution.PublicFunctions.ActionItemExecutionTask.GetFedProjectsActionItemExecitionTasks();
      taskList.Show();
    }
    
    [Public]
    public override void AddManyAddendumDialog(Sungero.Docflow.IOfficialDocument document)
    {
      base.AddManyAddendumDialog(document);
    }
  }
}