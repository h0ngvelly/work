﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvMainSolution.Module.Company.Client
{
  partial class ModuleFunctions
  {

    /// <summary>
    /// Создать документ с диалогом выбора типа документа.
    /// </summary>
    public virtual void CreateSubstitution()
    {        
      //anorsv.AnorsvSubstitution.VacationSubstitutionRequests.CreateDocumentWithCreationDialog(anorsv.AnorsvSubstitution.VacationSubstitutionRequests.Info);
      var doc = anorsv.AnorsvSubstitution.VacationSubstitutionRequests.Create();
      doc.Show();
    }
    
    /// <summary>
    /// Показать замещения только в качестве замещаемого/сотрудника
    /// </summary>
    public virtual void ShowSubstitutions()
    {
      var empSubstitutions = anorsv.AnorsvMainSolution.Module.Company.PublicFunctions.Module.GetVisibleSubstitutions(null);
      
      if (empSubstitutions != null)
        empSubstitutions.Show();
    }
  }
}