﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.ReviewManagerAssignment;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class ReviewManagerAssignmentActions
  {
    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.CreateSubtask(e);
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return false;
    }

  }

}