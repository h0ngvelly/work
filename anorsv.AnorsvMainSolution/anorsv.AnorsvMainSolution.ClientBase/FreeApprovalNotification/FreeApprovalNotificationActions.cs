﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution.FreeApprovalNotification;

namespace anorsv.AnorsvMainSolution.Client
{
  partial class FreeApprovalNotificationActions
  {
    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote
        .CreateFormalizedSubTask(_obj);
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Task.Status == Sungero.Workflow.Task.Status.InProcess 
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}