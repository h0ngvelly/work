﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Shared;
using ServiceFunctions = anorsv.ServiceLibrary.PublicFunctions;

namespace anorsv.Reports.Server
{
  public class ModuleFunctions
  {

    /// <summary>
    /// Заполнить SQL таблицу для формирования отчета "Лист согласования".
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="reportSessionId">Идентификатор отчета.</param>
    [Public]
    public static void UpdateApprovalSheetReportTable(Sungero.Docflow.IOfficialDocument document, string reportSessionId)
    {
      var filteredSignatures = new Dictionary<string, ISignature>();
      
      var setting = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(null);
      var showNotApproveSign = setting != null ? setting.ShowNotApproveSign == true : false;
      
      foreach (var version in document.Versions.OrderByDescending(v => v.Created))
      {
        var versionSignatures = Signatures.Get(version).Where(s => (showNotApproveSign || s.SignatureType != SignatureType.NotEndorsing)
                                                              && s.IsExternal != true
                                                              && !filteredSignatures.ContainsKey(ServiceFunctions.Module.GetSignatureKey(s, version.Number.Value)));
        var lastSignaturesInVersion = versionSignatures
          .GroupBy(v => ServiceFunctions.Module.GetSignatureKey(v, version.Number.Value))
          .Select(grouping => grouping.Where(s => s.SigningDate == grouping.Max(last => last.SigningDate)).First());
        
        foreach (ISignature signature in lastSignaturesInVersion)
        {
          filteredSignatures.Add(ServiceFunctions.Module.GetSignatureKey(signature, version.Number.Value), signature);
          if (!signature.IsValid)
            foreach (var error in signature.ValidationErrors)
              Logger.DebugFormat("UpdateApprovalSheetReportTable: reportSessionId {0}, document {1}, version {2}, signatory {3}, substituted user {7}, signature {4}, with error {5} - {6}",
                                 reportSessionId, document.Id, version.Number,
                                 signature.Signatory != null ? signature.Signatory.Name : signature.SignatoryFullName, signature.Id, error.ErrorType, error.Message,
                                 signature.SubstitutedUser != null ? signature.SubstitutedUser.Name : string.Empty);
          
          // Dmitriev_IA: signature.AdditionalInfo формируется в Employee в действии "Получение информации о подписавшем".
          //              Может содержать лишние пробелы в должности сотрудника. US 89747.
          var employeeName = string.Empty;
          var additionalInfos = (signature.AdditionalInfo ?? string.Empty)
            .Split(new char[] { '|' }, StringSplitOptions.None)
            .Select(x => x.Trim())
            .ToList();
          if (signature.SubstitutedUser == null)
          {
            var additionalInfo = additionalInfos.FirstOrDefault();
            employeeName = string.Format("<b>{1}</b>{0}", signature.SignatoryFullName, ServiceFunctions.Module.AddEndOfLine(additionalInfo)).Trim();
          }
          else
          {
            if (additionalInfos.Count() == 3)
            {
              // Замещающий.
              var signatoryAdditionalInfo = additionalInfos[0];
              if (!string.IsNullOrEmpty(signatoryAdditionalInfo))
                signatoryAdditionalInfo = ServiceFunctions.Module.AddEndOfLine(string.Format("<b>{0}</b>", signatoryAdditionalInfo));
              var signatoryText = ServiceFunctions.Module.AddEndOfLine(string.Format("{0}{1}", signatoryAdditionalInfo, signature.SignatoryFullName));
              
              // Замещаемый.
              var substitutedUserAdditionalInfo = additionalInfos[1];
              if (!string.IsNullOrEmpty(substitutedUserAdditionalInfo))
                substitutedUserAdditionalInfo = ServiceFunctions.Module.AddEndOfLine(string.Format("<b>{0}</b>", substitutedUserAdditionalInfo));
              var substitutedUserText = string.Format("{0}{1}", substitutedUserAdditionalInfo, signature.SubstitutedUserFullName);
              
              // Замещающий за замещаемого.
              var onBehalfOfText = ServiceFunctions.Module.AddEndOfLine(Sungero.Docflow.ApprovalTasks.Resources.OnBehalfOf);
              employeeName = string.Format("{0}{1}{2}", signatoryText, onBehalfOfText, substitutedUserText);
            }
            else if (additionalInfos.Count() == 2)
            {
              // Замещающий / Система.
              var signatoryText = ServiceFunctions.Module.AddEndOfLine(signature.SignatoryFullName);
              
              // Замещаемый.
              var substitutedUserAdditionalInfo = additionalInfos[0];
              if (!string.IsNullOrEmpty(substitutedUserAdditionalInfo))
                substitutedUserAdditionalInfo = ServiceFunctions.Module.AddEndOfLine(string.Format("<b>{0}</b>", substitutedUserAdditionalInfo));
              var substitutedUserText = string.Format("{0}{1}", substitutedUserAdditionalInfo, signature.SubstitutedUserFullName);
              
              // Система за замещаемого.
              var onBehalfOfText = ServiceFunctions.Module.AddEndOfLine(Sungero.Docflow.ApprovalTasks.Resources.OnBehalfOf);
              employeeName = string.Format("{0}{1}{2}", signatoryText, onBehalfOfText, substitutedUserText);
            }
          }
          
          var commandText = Queries.Module.InsertIntoApprovalSheetReportTable;
            using (var command = SQL.GetCurrentConnection().CreateCommand())
          {
            var separator = ", ";
            var errorString = Sungero.Docflow.PublicFunctions.Module.GetSignatureValidationErrorsAsString(signature, separator);
            var signErrors = string.IsNullOrWhiteSpace(errorString)
              ? Sungero.Docflow.Reports.Resources.ApprovalSheetReport.SignStatusActive
              : Sungero.Docflow.PublicFunctions.Module.ReplaceFirstSymbolToUpperCase(errorString.ToLower());
            var resultString = anorsv.ServiceLibrary.PublicFunctions.Module.GetEndorsingResultFromSignature(signature, false);
            command.CommandText = commandText;
            SQL.AddParameter(command, "@reportSessionId",  reportSessionId, System.Data.DbType.String);
            SQL.AddParameter(command, "@employeeName",  employeeName, System.Data.DbType.String);
            SQL.AddParameter(command, "@resultString",  resultString, System.Data.DbType.String);
            SQL.AddParameter(command, "@comment",  signature.Comment, System.Data.DbType.String);
            SQL.AddParameter(command, "@signErrors",  signErrors, System.Data.DbType.String);
            SQL.AddParameter(command, "@versionNumber",  version.Number, System.Data.DbType.Int32);
            SQL.AddParameter(command, "@SignatureDate",  signature.SigningDate.FromUtcTime(), System.Data.DbType.DateTime);
            
            command.ExecuteNonQuery();
          }
        }
      }
    }

  }
}