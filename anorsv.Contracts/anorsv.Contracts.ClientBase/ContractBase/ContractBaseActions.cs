﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.ContractBase;

namespace anorsv.Contracts.Client
{
  partial class ContractBaseVersionsActions
  {
    public override void ImportVersion(Sungero.Domain.Client.ExecuteChildCollectionActionArgs e)
    {
      base.ImportVersion(e);
    }

    public override bool CanImportVersion(Sungero.Domain.Client.CanExecuteChildCollectionActionArgs e)
    {
      var contract = anorsv.Contracts.ContractBases.As(_obj.ElectronicDocument);
      var IsStandard = contract.IsStandard.GetValueOrDefault(false);
      var leadingDocumentIsPurchaseRequest = 
        contract.LeadingDocument != null && sline.CustomModule.PurchaseRequests.Is(contract.LeadingDocument);
      var contractForPurchaseRelation = _obj.ElectronicDocument.Relations
        .GetRelatedFrom(sline.CustomModule.Resources.ContractForPurchaseRelationName).Any();
      var tsForPurchaseRelation = _obj.ElectronicDocument.Relations
        .GetRelatedFrom(sline.CustomModule.Resources.TSForPurchaseRelationName).Any();
      var allForPurchaseRelationSimple = _obj.ElectronicDocument.Relations
        .GetRelatedFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName).Any();
      
      if (IsStandard 
          && leadingDocumentIsPurchaseRequest 
          && (contractForPurchaseRelation || tsForPurchaseRelation || allForPurchaseRelationSimple))
        return false;
      else
        return base.CanImportVersion(e);
    }
  }


  partial class ContractBaseActions
  {


    public override void ImportInLastVersion(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ImportInLastVersion(e);
    }

    public override bool CanImportInLastVersion(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      var contract = anorsv.Contracts.ContractBases.As(_obj);
      var IsStandard = contract.IsStandard.GetValueOrDefault(false);
      var leadingDocumentIsPurchaseRequest = 
        contract.LeadingDocument != null && sline.CustomModule.PurchaseRequests.Is(contract.LeadingDocument);
      var contractForPurchaseRelation = _obj.Relations
        .GetRelatedFrom(sline.CustomModule.Resources.ContractForPurchaseRelationName).Any();
      var tsForPurchaseRelation = _obj.Relations
        .GetRelatedFrom(sline.CustomModule.Resources.TSForPurchaseRelationName).Any();
      var allForPurchaseRelationSimple = _obj.Relations
        .GetRelatedFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName).Any();
      
      if (IsStandard 
          && leadingDocumentIsPurchaseRequest 
          && (contractForPurchaseRelation || tsForPurchaseRelation || allForPurchaseRelationSimple))
        return false;
      else
        return base.CanImportInLastVersion(e);
    }

    public override void ImportInNewVersion(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ImportInNewVersion(e);
    }

    public override bool CanImportInNewVersion(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      var contract = anorsv.Contracts.ContractBases.As(_obj);
      var IsStandard = contract.IsStandard.GetValueOrDefault(false);
      var leadingDocumentIsPurchaseRequest = 
        contract.LeadingDocument != null && sline.CustomModule.PurchaseRequests.Is(contract.LeadingDocument);
      var contractForPurchaseRelation = _obj.Relations
        .GetRelatedFrom(sline.CustomModule.Resources.ContractForPurchaseRelationName).Any();
      var tsForPurchaseRelation = _obj.Relations
        .GetRelatedFrom(sline.CustomModule.Resources.TSForPurchaseRelationName).Any();
      var allForPurchaseRelationSimple = _obj.Relations
        .GetRelatedFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName).Any();
      
      if (IsStandard 
          && leadingDocumentIsPurchaseRequest 
          && (contractForPurchaseRelation || tsForPurchaseRelation || allForPurchaseRelationSimple))
        return false;
      else
        return base.CanImportInNewVersion(e);
    }

  }

}