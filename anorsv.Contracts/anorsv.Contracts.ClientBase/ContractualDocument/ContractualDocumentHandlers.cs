﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.ContractualDocument;

namespace anorsv.Contracts
{
  partial class ContractualDocumentClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      Functions.ContractualDocument.ChangeEnableVisiblePropertiesState(_obj);
    }

  }
}