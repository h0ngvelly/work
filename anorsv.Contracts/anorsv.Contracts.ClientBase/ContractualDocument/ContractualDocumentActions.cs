﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.ContractualDocument;

namespace anorsv.Contracts.Client
{
  partial class ContractualDocumentActions
  {
    public override void Register(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.Register(e);
      
      Functions.ContractualDocument.ChangeEnableVisiblePropertiesState(_obj);
    }

    public override bool CanRegister(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanRegister(e);
    }

    public override void SendForFreeApproval(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Перенесено из перекрытия sline
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetFreeTasks(_obj);      
      if (createdTasks)
      {
        e.AddError(anorsv.Contracts.Resources.FreeApprovalTaskExists); //"Документ уже отправлен на свободное согласование!"
        return;
      }
      else
      {
        base.SendForFreeApproval(e);
      }
    }

    public override bool CanSendForFreeApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForFreeApproval(e);
    }

    public override void SendForApproval(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Перенесено из перекрытия sline
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetApprovalTasks(_obj);      
      if (createdTasks)
      {
        e.AddError(anorsv.Contracts.Resources.ApprovalTaskExists); //"Документ уже отправлен на согласование по регламенту!"
        return;
      }
      else
      {
        base.SendForApproval(e);
      }
    }

    public override bool CanSendForApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForApproval(e);
    }

    public override void SendActionItem(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Перенесено из перекрытия sline
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetActionTasks(_obj);
      if (createdTasks)
      {
        e.AddError(anorsv.Contracts.Resources.ActionTaskExists); //"Документ уже отправлен на исполнение!"
        return;
      }
      else
      {
        base.SendActionItem(e);
      }
    }

    public override bool CanSendActionItem(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendActionItem(e);
    }

    public override void SendForReview(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Перенесено из перекрытия sline
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetReviewTasks(_obj);      
      if (createdTasks)
      {
        e.AddError(anorsv.Contracts.Resources.ReviewTaskExists); //"Документ уже отправлен на рассмотрение!"
        return;
      }
      else
      {
        base.SendForReview(e);
      }
    }

    public override bool CanSendForReview(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForReview(e);
    }

    public override void SendForAcquaintance(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Перенесено из перекрытия sline
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetAcqTasks(_obj);      
      if (createdTasks)
      {
        e.AddError(anorsv.Contracts.Resources.AcquaintanceTaskExists); //"Документ уже отправлен на ознакомление!"
        return;
      }
      else
      {
        base.SendForAcquaintance(e);
      }
    }

    public override bool CanSendForAcquaintance(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForAcquaintance(e);
    }

  }


}