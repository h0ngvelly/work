﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.Contract;

namespace anorsv.Contracts.Client
{
  partial class ContractActions
  {
    public override void CreateFromTemplate(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (_obj.GeneralTopicanorsv != null)
      {
        if (_obj.IsStandard == true)
          anorsv.DocflowModule.PublicFunctions.AnorsvGenericTopic.SelectDocumentTemplate(_obj, _obj.GeneralTopicanorsv, true);
        else
          anorsv.DocflowModule.PublicFunctions.AnorsvGenericTopic.SelectDocumentTemplate(_obj, _obj.GeneralTopicanorsv, false);
      }
      else
        base.CreateFromTemplate(e);
    }

    public override bool CanCreateFromTemplate(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanCreateFromTemplate(e);
    }

  }


}