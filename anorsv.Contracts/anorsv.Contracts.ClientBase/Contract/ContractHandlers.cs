using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.Contract;

namespace anorsv.Contracts
{
  partial class ContractClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      // если договор расходный скрывать категории
      var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
      if (_obj.DocumentKind != null && _obj.DocumentKind == expendableContractKind)
      {
        _obj.State.Properties.DocumentGroup.IsVisible = false;
        _obj.State.Properties.Purchaseanorsv.IsVisible = true;
        // Предупреждение о невозможности изменения связей с Закупкой из карточки Договора
        if (_obj.Purchaseanorsv == null)
          e.AddInformation(anorsv.RelationModule.Resources.RelationProhibitionForContractPurchase);
      }
      else
      {
        _obj.State.Properties.DocumentGroup.IsVisible = true;
      }
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      // если договор расходный скрывать категории
      var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
      if (_obj.DocumentKind != null && _obj.DocumentKind == expendableContractKind)
      {
        _obj.State.Properties.DocumentGroup.IsVisible = false;
        _obj.State.Properties.Purchaseanorsv.IsVisible = true;
        // Предупреждение о невозможности изменения связей с Закупкой из карточки Договора
        if (_obj.Purchaseanorsv == null)
          e.AddInformation(anorsv.RelationModule.Resources.RelationProhibitionForContractPurchase);
      }
      else
      {
        _obj.State.Properties.DocumentGroup.IsVisible = true;
      }      
    }
  }
}