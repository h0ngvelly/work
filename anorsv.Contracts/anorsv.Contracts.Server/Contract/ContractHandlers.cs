﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.Contract;

namespace anorsv.Contracts
{
  partial class ContractGeneralTopicanorsvPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> GeneralTopicanorsvFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      if (_obj.DocumentKind != null)
        query = query.Where(d => Equals(d.DocumentKind, _obj.DocumentKind));
      
      return query;
    }
  }

  partial class ContractServerHandlers
  {

    public override void BeforeSaveHistory(Sungero.Content.DocumentHistoryEventArgs e)
    {
      base.BeforeSaveHistory(e);
      
      var isChangeRelationAction = e.Action == Sungero.CoreEntities.History.Action.ChangeRelation;
      if (isChangeRelationAction)
      {
        if (e.OperationDetailed.ToString().Equals(anorsv.RelationModule.PublicConstants.Module.SystemOperations.RemoveRelation))
        {
          // При изменении связи проверить для Проекта договора наличие обязательной связи
          var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
          if (_obj.DocumentKind.Equals(expendableContractKind))
          {
            if (_obj.LeadingDocument != null && _obj.Purchaseanorsv != null)  
              anorsv.RelationModule.PublicFunctions.Module.CheckSpecialRelations(_obj.Purchaseanorsv.Id, _obj.Id);
          }
        } 
      }
      
    }
  }



}