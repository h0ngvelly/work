﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.Contract;

namespace anorsv.Contracts.Shared
{
  partial class ContractFunctions
  {
    
    /// <summary>
    /// Заполнить имя.
    /// </summary>
    public override void FillName()
    {
      if (_obj.DocumentKind != null)
      {
        // если договор расходный формат имени договора должен строиться: “Договор о [“Предмет договора”] с [“Контрагент“] ”.
        var dockind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
        if (_obj.DocumentKind.Id == dockind.Id)
        {
          _obj.Name = anorsv.Contracts.Contracts.Resources.Contract + _obj.SubjectContractanorsv + anorsv.Contracts.Contracts.Resources.With + _obj.Counterparty;
        }
        else
        {
          base.FillName();
        }
      }
    }
    
    public override void ChangeDocumentPropertiesAccess(bool isEnabled, bool isRepeatRegister)
    {
      base.ChangeDocumentPropertiesAccess(isEnabled, isRepeatRegister);
      
      // если договор расходный
      var dockind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
      if (_obj.DocumentKind == dockind)
      {
        //признак Типовой недостпуно для редактирования
        _obj.State.Properties.IsStandard.IsEnabled = false;
      }
    }
  }
}