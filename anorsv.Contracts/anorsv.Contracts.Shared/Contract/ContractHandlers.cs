using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.Contract;

namespace anorsv.Contracts
{
  partial class ContractSharedHandlers
  {

    public override void CounterpartyChanged(Sungero.Docflow.Shared.ContractualDocumentBaseCounterpartyChangedEventArgs e)
    {
      base.CounterpartyChanged(e);
      FillName();
    }

    public virtual void SubjectContractanorsvChanged(Sungero.Domain.Shared.StringPropertyChangedEventArgs e)
    {
      FillName();
    }

    public override void DocumentKindChanged(Sungero.Docflow.Shared.OfficialDocumentDocumentKindChangedEventArgs e)
    {
      base.DocumentKindChanged(e);
      FillName();
      // добавляем признак по виду документа "Договоры расходные"
      var ExpendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
      if (_obj.DocumentKind == ExpendableContractKind)
      {
        _obj.State.Properties.DocumentGroup.IsVisible = false;
        _obj.State.Properties.GeneralTopicanorsv.IsVisible = true;
      }
      else
      {
        _obj.State.Properties.DocumentGroup.IsVisible = true;
        _obj.State.Properties.GeneralTopicanorsv.IsVisible = false;
      }
    }

  }
}