﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.Contracts.ContractualDocument;

namespace anorsv.Contracts.Shared
{
  partial class ContractualDocumentFunctions
  {
    /// <summary>
    /// Управляем доступностью и видимостью свойств.
    /// </summary>
    public void ChangeEnableVisiblePropertiesState()
    {
      var clerks = Sungero.Docflow.PublicFunctions.DocumentRegister.Remote.GetClerks();
      
      _obj.State.Properties.Author.IsEnabled = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());

      // № вед.документа и ссылка на вед.докмент 1С доступны Делопроизводителям всегда
      if (Users.Current.IncludedIn(clerks))
      {
        _obj.State.Properties.LeadingDocNumberanorsv.IsEnabled = true;
        _obj.State.Properties.LeadingDocLinkanorsv.IsEnabled = true;
      }
      // Рег.№ и Дата рег. контрагентом доступны Делопроизводителям, если документ не зарегистрирован
      _obj.State.Properties.CRegistrationNumberanorsv.IsEnabled = (Users.Current.IncludedIn(clerks) && (!_obj.RegistrationState.Equals(RegistrationState.Registered)));
      _obj.State.Properties.CRegistrationDateanorsv.IsEnabled = (Users.Current.IncludedIn(clerks) && (!_obj.RegistrationState.Equals(RegistrationState.Registered)));
      
      // Рег.№ и Дата рег. контрагентом отображаются только с остальными регистрационными данными
      _obj.State.Properties.CRegistrationNumberanorsv.IsVisible = _obj.State.Properties.RegistrationNumber.IsVisible;
      _obj.State.Properties.CRegistrationDateanorsv.IsVisible = _obj.State.Properties.RegistrationDate.IsVisible;
    }
  }
}