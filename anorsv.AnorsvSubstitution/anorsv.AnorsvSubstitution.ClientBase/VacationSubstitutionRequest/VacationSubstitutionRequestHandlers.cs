﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvSubstitution.VacationSubstitutionRequest;

namespace anorsv.AnorsvSubstitution
{
  partial class VacationSubstitutionRequestClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      _obj.State.Properties.Substitution.IsVisible = (_obj.Substitution != null);
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      _obj.State.Properties.Substitution.IsVisible = (_obj.Substitution != null);
    }

  }
}