﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvSubstitution;

namespace anorsv.AnorsvSubstitution.Server
{
  public class ModuleAsyncHandlers
  {

    public virtual void CreateSubstitutionByRequest(anorsv.AnorsvSubstitution.Server.AsyncHandlerInvokeArgs.CreateSubstitutionByRequestInvokeArgs args)
    {
      int substitutionRequestId = args.SubstitutionRequestId;
      ISubstitutionRequestBase substitutionRequest = SubstitutionRequestBases.GetAll(sr => sr.Id == substitutionRequestId).FirstOrDefault();
      bool needRetry = true;
      
      if (substitutionRequest != null)
      {
        needRetry = !Functions.SubstitutionRequestBase.SynchronizeSubstitution(substitutionRequest, false);
      }
      else
      {
        needRetry = false;
      }
      
      args.Retry = needRetry;
    }

  }
}