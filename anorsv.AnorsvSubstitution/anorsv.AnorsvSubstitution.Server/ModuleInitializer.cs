﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;
using Sungero.Docflow;

namespace anorsv.AnorsvSubstitution.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      CreateDocumentTypes();
      CreateDocumentKinds();
      GrantAccessRights();
    }
    
    /// <summary>
    /// Создать типы документов
    /// </summary>
    public static void CreateDocumentTypes()
    {
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentType(Resources.VacationSubstitutionTypeName, VacationSubstitutionRequest.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Inner, true);
    }
    
    /// <summary>
    /// Создать виды документов
    /// </summary>
    public static void CreateDocumentKinds()
    {
      var substActions = new Sungero.Domain.Shared.IActionInfo[] {
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendActionItem,
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendForApproval,
        Sungero.Docflow.OfficialDocuments.Info.Actions.SendForAcquaintance };
      
      Sungero.Docflow.PublicInitializationFunctions.Module
        .CreateDocumentKind(
          Resources.VacationSubstitutionKindName,
          Resources.VacationSubstitutionKindShortName,
          Sungero.Docflow.DocumentKind.NumberingType.NotNumerable,
          Sungero.Docflow.DocumentKind.DocumentFlow.Inner, false, false, VacationSubstitutionRequest.ClassTypeGuid,
          substActions,
          Constants.Module.VacationSubstitutionKind
         );
    }
    
    
    public static void GrantAccessRights()
    {
      InitializationLogger.Debug("Init: Grant access rights on SubstitutionRequest to all users.");
      
      var allUser = Roles.AllUsers;
      
      SubstitutionRequestBases.AccessRights.Grant(allUser, DefaultAccessRightsTypes.Create);
      SubstitutionRequestBases.AccessRights.Save();
    }
  }
}
