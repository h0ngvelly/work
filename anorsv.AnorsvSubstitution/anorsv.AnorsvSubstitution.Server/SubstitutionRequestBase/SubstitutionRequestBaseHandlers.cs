﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvSubstitution.SubstitutionRequestBase;

namespace anorsv.AnorsvSubstitution
{
  partial class SubstitutionRequestBaseServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      if (Sungero.Company.Employees.Current != null)
        _obj.Employee = Sungero.Company.Employees.Current;
      _obj.ConfidentialDocumentanorsv = false;
    }

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      base.Saving(e);
      
      if (_obj.InternalApprovalState == InternalApprovalState.Approved && _obj.Substitution == null)
      {
        Functions.SubstitutionRequestBase.SynchronizeSubstitution(_obj, true);
      }
    }

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      // Проверить равенство замещающего и сотрудника
      if (_obj.Substitute != null && _obj.Employee != null)
        if (Equals(_obj.Substitute, _obj.Employee))
          e.AddError(anorsv.UserSubstitution.Resources.SameSubstitutionAndEmployee);
      
      // Проверить валидность дат
      if (_obj.State.Properties.StartDate.IsChanged && _obj.StartDate != null && _obj.StartDate < Calendar.Today)
        e.AddError(anorsv.UserSubstitution.Resources.StartDateLessTodayDate);
      if (_obj.State.Properties.EndDate.IsChanged && _obj.EndDate != null && _obj.EndDate < Calendar.Today)
        e.AddError(anorsv.UserSubstitution.Resources.EndDateLessTodayDate);
      if ((_obj.StartDate != null) && (_obj.EndDate != null) && (_obj.StartDate > _obj.EndDate))
        e.AddError(anorsv.UserSubstitution.Resources.StartDateLessEndDate);
      
      base.BeforeSave(e);
    }
  }

  partial class SubstitutionRequestBaseEmployeePropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> EmployeeFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(q => q.Status.Equals(Sungero.Company.Employee.Status.Active));
      return query;
    }
  }

  partial class SubstitutionRequestBaseSubstitutePropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> SubstituteFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(q => q.Status.Equals(Sungero.Company.Employee.Status.Active));
      return query;
    }
  }

}