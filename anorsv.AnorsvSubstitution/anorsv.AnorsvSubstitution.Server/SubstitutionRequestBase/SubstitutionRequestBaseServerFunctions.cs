﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvSubstitution.SubstitutionRequestBase;

namespace anorsv.AnorsvSubstitution.Server
{
  partial class SubstitutionRequestBaseFunctions
  {
    /// <summary>
    /// Программно создать замещение.
    /// </summary>
    [Public]
    public virtual void CreateRealSubstitution()
    {
      var substitution = Substitutions.Create();
      substitution.User = _obj.Employee;
      substitution.Substitute = _obj.Substitute;
      if (_obj.StartDate != null)
        substitution.StartDate = _obj.StartDate;
      if (_obj.EndDate != null)
        substitution.EndDate = _obj.EndDate;
      substitution.IsSystem = false;
      substitution.Comment = _obj.Note;
      substitution.Save();
      
      _obj.Substitution = substitution;
    }
    
    [Public]
    public virtual bool SynchronizeSubstitution(bool possibleAsyncSynchronozation)
    {
      bool successfull = true;
      
      // Проверим наличие подходящего замещения, если не найдено создадим.
      if (_obj.Substitution == null)
      {
        var substitution =
          Substitutions
          .GetAll(
            s => s.User.Equals(_obj.Employee)
            && s.Substitute.Equals(_obj.Substitute)
            && s.StartDate == _obj.StartDate
            && s.EndDate == _obj.EndDate
            && (!s.IsSystem.HasValue || s.IsSystem.HasValue && s.IsSystem.Value)
           )
          .FirstOrDefault();
        
        if (substitution == null)
        {
          if (Substitutions.AccessRights.CanCreate())
          {
            Functions.SubstitutionRequestBase.CreateRealSubstitution(_obj);
            successfull = true;
          }
          else
          {
            if (possibleAsyncSynchronozation)
            {
              var asyncCreateSubstitutionHandler = AsyncHandlers.CreateSubstitutionByRequest.Create();
              asyncCreateSubstitutionHandler.SubstitutionRequestId = _obj.Id;
              asyncCreateSubstitutionHandler.ExecuteAsync();
              successfull = true;
            }
          }
        }
        else
        {
          _obj.Substitution = substitution;
          successfull = true;
        }
      }
      
      return successfull;
    }
    
  }
}