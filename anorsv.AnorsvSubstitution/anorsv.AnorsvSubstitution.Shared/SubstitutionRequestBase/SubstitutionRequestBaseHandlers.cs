using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvSubstitution.SubstitutionRequestBase;

namespace anorsv.AnorsvSubstitution
{
  partial class SubstitutionRequestBaseSharedHandlers
  {

    public virtual void EndDateChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      anorsv.AnorsvSubstitution.Functions.SubstitutionRequestBase.FillName(_obj);
    }

    public virtual void StartDateChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      anorsv.AnorsvSubstitution.Functions.SubstitutionRequestBase.FillName(_obj);
    }

    public virtual void EmployeeChanged(anorsv.AnorsvSubstitution.Shared.SubstitutionRequestBaseEmployeeChangedEventArgs e)
    {
      anorsv.AnorsvSubstitution.Functions.SubstitutionRequestBase.FillName(_obj);
    }

    public virtual void SubstituteChanged(anorsv.AnorsvSubstitution.Shared.SubstitutionRequestBaseSubstituteChangedEventArgs e)
    {
      anorsv.AnorsvSubstitution.Functions.SubstitutionRequestBase.FillName(_obj);
    }

  }
}