﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvSubstitution.SubstitutionRequestBase;

namespace anorsv.AnorsvSubstitution.Shared
{
  partial class SubstitutionRequestBaseFunctions
  {
    /// <summary>
    /// Сформировать имя документа
    /// </summary>       
    public override void FillName()
    {
      // Сформировать имя
      _obj.Name = "Замещение";
      if (_obj.Substitute != null && _obj.Employee != null)
      {
        _obj.Name += string.Format(" {0} - {1}", _obj.Substitute.Name, _obj.Employee.Name);
        if (_obj.StartDate != null)
          _obj.Name += string.Format(" с {0}", _obj.StartDate.Value.Date.ToString("d"));
        if (_obj.EndDate != null)
          _obj.Name += string.Format(" по {0}", _obj.EndDate.Value.Date.ToString("d"));
      }
    }
    
  }
}