﻿using System;
using Sungero.Core;

namespace anorsv.AnorsvSubstitution.Constants
{
  public static class Module
  {

    /// <summary>
    /// Вид документа Заявка на замещение
    /// </summary>
    [Sungero.Core.Public]
    public static readonly Guid VacationSubstitutionKind = Guid.Parse("DB7E8733-A821-4B90-9557-F5095BA6B871");

  }
}