﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalCheckingAssignment;
using docRelation = anorsv.RelationModule;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalCheckingAssignmentActions
  {
    public virtual void CreateManyAddendumanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        //RSV.Module.Docflow.Functions.Module.AddManyAddendumDialog(document);
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public virtual void CreateMemoanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (!_obj.MemoGroupanorsv.Memos.Any())
      {
        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
        if (document != null)
        {
          //anorsv.ApprovalTaskModule.PublicFunctions.Module.MemoDialog(Sungero.Docflow.ApprovalTasks.As(_obj.Task), _obj.Id, document);
          anorsv.ApprovalTaskModule.PublicFunctions.Module.CreateMemoWithoutDialog(Sungero.Docflow.ApprovalTasks.As(_obj.Task), _obj.Id, document);
          _obj.Save();
        }
      }
      else
        Dialogs.ShowMessage(anorsv.ApprovalTaskModule.Resources.DocAlreadyCreatedMemoText, MessageType.Error);
    }

    public virtual bool CanCreateMemoanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var needMemo = contractAttGroupAvailable 
        && (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
        && (_obj.Stageanorsv == null || anorsv.ApprovalTaskModule.PublicFunctions.Module.NeedMemoForPurchaseByStageSetting(_obj.Stageanorsv))
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
      return needMemo;
    }

    public virtual void CreateTSanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (!_obj.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Any())
      {
        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
        if (document != null)
        {
          anorsv.ApprovalTaskModule.PublicFunctions.Module.TSDialog(_obj, document);
          _obj.Save();
        }
      }
      else
        Dialogs.ShowMessage(anorsv.ApprovalTaskModule.Resources.DocAlreadyCreatedTSText, MessageType.Error);
    }

    public virtual bool CanCreateTSanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      return (contractAttGroupAvailable && _obj.ApprovalPurhaseanorsv.GetValueOrDefault());
      // TODO MED: скрыть проверку по реквизиту РСВ, оставить по реквизиту ЦВД
      /*var needTS = (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
        && (_obj.Stageanorsv == null || anorsv.ApprovalTaskModule.PublicFunctions.Module.NeedContractDraftByStageSetting(_obj.Stageanorsv))
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
      return needTS;*/
    }

    public virtual void CreateDraftContractanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (!_obj.ContractGroupanorsv.Contracts.Any())
      {
        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
        if (document != null)
        {
          anorsv.ApprovalTaskModule.PublicFunctions.Module.DraftContractDialog(_obj, document);
          _obj.Save();
        }
      }
      else
        Dialogs.ShowMessage(anorsv.ApprovalTaskModule.Resources.DocAlreadyCreatedContractText, MessageType.Error);
    }

    public virtual bool CanCreateDraftContractanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      return (contractAttGroupAvailable && _obj.ApprovalPurhaseanorsv.GetValueOrDefault());
      // TODO MED: скрыть проверку по реквизиту РСВ, оставить по реквизиту ЦВД
      /*var needContractDraft = (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
        && (_obj.Stageanorsv == null || anorsv.ApprovalTaskModule.PublicFunctions.Module.NeedContractDraftByStageSetting(_obj.Stageanorsv))
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
      return needContractDraft;*/
    }

    public virtual void AddDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.AddDocumentLinkDialog(document, _obj.Id);
    }

    public virtual bool CanAddDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      //bool canModifyRelation = false;
      //if (_obj.DocumentGroup.OfficialDocuments.Any() && e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
      //  if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
      //    return canModifyRelation;
      bool canModifyRelation = (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess &&
                                _obj.AccessRights.CanUpdate() &&
                                _obj.DocumentGroup.OfficialDocuments.Any());
      
      return canModifyRelation;
    }

    public virtual void DeleteDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      bool canModifyPurchaseRelations = false;
      if (e.Params.Contains("AnoRsvCanModifyPurchaseRelations"))
        e.Params.TryGetValue("AnoRsvCanModifyPurchaseRelations", out canModifyPurchaseRelations);
          
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        if (canModifyPurchaseRelations)
        {
          var availableTypeList = new List<IRelationType>();
          if (_obj.ApprovalPurhaseanorsv == true)
          {
            var contractTypeList = RelationTypes.GetAll()
              .Where(r => r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName ||
                     r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName)
              .ToList();
            availableTypeList.AddRange(contractTypeList);
          }
          if (_obj.Stageanorsv.NeedAttachMemoanorsv == true)
          {
            var memoForPurchaseTypeList = RelationTypes.GetAll()
              .Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName)
              .ToList();
            availableTypeList.AddRange(memoForPurchaseTypeList);
          }
          docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(document, availableTypeList);
        }
        else
        {
          docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(document);
        }
      }
    }

    public virtual bool CanDeleteDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
          return canModifyRelation;
      
      return canModifyRelation;
    }

    public virtual void ChangeDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
//      bool canModifyPurchaseRelations = false;
//      if (e.Params.Contains("AnoRsvCanModifyPurchaseRelations"))
//        if (e.Params.TryGetValue("AnoRsvCanModifyPurchaseRelations", out canModifyPurchaseRelations));
          
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
//        if (canModifyPurchaseRelations)
//        {
//          var availableTypeList = new List<IRelationType>();
//          if (_obj.ApprovalPurhaseanorsv == true)
//          {
//            var contractTypeList = RelationTypes.GetAll()
//              .Where(r => r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName ||
//                     r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName)
//              .ToList();
//            availableTypeList.AddRange(contractTypeList);
//          }
//          if (_obj.Stageanorsv.NeedAttachMemoanorsv == true)
//          {
//            var memoForPurchaseTypeList = RelationTypes.GetAll()
//              .Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName)
//              .ToList();
//            availableTypeList.AddRange(memoForPurchaseTypeList);
//          }
//          docRelation.PublicFunctions.Module.ChangeDocumentLinkDialog(document, availableTypeList);
//        }
//        else
//        {
          docRelation.PublicFunctions.Module.ChangeDocumentLinkDialog(document);
//        }
      }
    }

    public virtual bool CanChangeDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
          return canModifyRelation;
      
      return canModifyRelation;
    }

    public virtual void CreateDocumentLinkanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.AddDocumentLinkDialog(document, _obj.Id);
    }

    public virtual bool CanCreateDocumentLinkanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess && _obj.AccessRights.CanUpdate() && _obj.DocumentGroup.OfficialDocuments.Any());
    }

    public override void ForRework(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.ForRework(e);
    }

    public override bool CanForRework(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanForRework(e);
    }

    public override void Accept(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      // Проверка наличия обязательных вложений в задании
      string needAttachmentMessage = anorsv.ApprovalTaskModule.PublicFunctions.Module.CheckAttachedDocuments(Sungero.Workflow.Assignments.As(_obj));
      if (!string.IsNullOrEmpty(needAttachmentMessage))
      {
        e.AddError(needAttachmentMessage);
        e.Cancel();
      }
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Accept(e);
    }

    public override bool CanAccept(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanAccept(e);
    }



    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = null;
      
      if (_obj.Stageanorsv != null)
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj, _obj.Stageanorsv);
      }
      else
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      }
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && (_obj.Stageanorsv == null || anorsv.TaskModule.PublicFunctions.Module.SubtaskIsAllowedByStageSetting(_obj.Stageanorsv))
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}