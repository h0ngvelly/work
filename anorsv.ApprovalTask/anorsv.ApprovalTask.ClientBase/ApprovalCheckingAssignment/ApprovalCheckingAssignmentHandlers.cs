using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalCheckingAssignment;
using docRelation = anorsv.RelationModule;

namespace anorsv.ApprovalTask
{
  partial class ApprovalCheckingAssignmentClientHandlers
  {

    public override void Closing(Sungero.Presentation.FormClosingEventArgs e)
    {
      base.Closing(e);
      
      anorsv.ServiceLibrary.PublicFunctions.Module.MarkSubtaskExistInAssignmentSubject(_obj);
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      if (_obj.ApprovalPurhaseanorsv == true && contractAttGroupAvailable == true && _obj.Completed == null)
        e.AddInformation(anorsv.ApprovalTask.ApprovalTasks.Resources.ApprovalTaskHintInformationForPurchaseRequest);
      
      base.Refresh(e);
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        // Проверить возможность изменения связей
        bool canModifyRelations = docRelation.PublicFunctions.Module.Remote.CanChangeDocumentRelations(document, Sungero.Workflow.AssignmentBases.As(_obj));
        e.Params.AddOrUpdate("AnoRsvCanModifyRelationsFromTask", canModifyRelations);
        
        // Проверить возможность изменений связей с Закупкой
        bool canModifyPurchaseRelations = sline.CustomModule.PurchaseRequests.Is(document) &&
          ((_obj.ApprovalPurhaseanorsv == true) || (_obj.Stageanorsv != null && _obj.Stageanorsv.NeedAttachMemoanorsv == true));
        e.Params.AddOrUpdate("AnoRsvCanModifyPurchaseRelations", canModifyPurchaseRelations);
      }
      
      //видимость вложений в зависимости от вида документ "ЛНА по общ.деятельности"
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      _obj.State.Attachments.OrderGroupanorsv.IsVisible = false;
      if (orderAttGroupAvailable && anorsv.DocflowModule.LNAs.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault()))
      {
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        if (lnaGenActivitiesKind != null && _obj.DocumentGroup.OfficialDocuments.FirstOrDefault().DocumentKind.Equals(lnaGenActivitiesKind))
          _obj.State.Attachments.OrderGroupanorsv.IsVisible = true;
      }
      
      //видимость вложений в зависимости от типа документа "Заявка на закупку"
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var isPurchaseRequest = contractAttGroupAvailable && sline.CustomModule.PurchaseRequests.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.MemoGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.ContractGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.TechnicalSpecificationGroupanorsv.IsVisible = isPurchaseRequest;
    }

  }
}
