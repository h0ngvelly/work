﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalManagerAssignment;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalManagerAssignmentActions
  {
    public virtual void CreateManyAddendumanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        //RSV.Module.Docflow.Functions.Module.AddManyAddendumDialog(document);
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public override void ForRevision(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.ForRevision(e);
    }

    public override bool CanForRevision(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanForRevision(e);
    }

    public override void Approved(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Approved(e);
    }

    public override bool CanApproved(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanApproved(e);
    }

    public virtual void ApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var report = anorsv.OfficialDocument.Reports.GetApprovalSheetByProcessReport();
      report.Document = _obj.DocumentGroup.OfficialDocuments.Single();;
      report.Open();
    }

    public virtual bool CanApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return !approvalSheetBySignsIsValid;
    }

    public override void ApprovalForm(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ApprovalForm(e);
    }

    public override bool CanApprovalForm(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return approvalSheetBySignsIsValid;
    }


    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = null;
      
      if (_obj.Stage != null)
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj, sline.RSV.ApprovalStages.As(_obj.Stage));
      }
      else
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      }
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.SubtaskIsAllowedByStageSetting(_obj.Stage)
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}