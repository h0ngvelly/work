﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalCheckReturnAssignment;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalCheckReturnAssignmentActions
  {
    public virtual void CreateManyAddendumanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        //RSV.Module.Docflow.Functions.Module.AddManyAddendumDialog(document);
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public override void NotSigned(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.NotSigned(e);
    }

    public override bool CanNotSigned(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanNotSigned(e);
    }

    public override void Signed(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Signed(e);
    }

    public override bool CanSigned(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanSigned(e);
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = null;
      
      if (_obj.Stageanorsv != null)
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj, _obj.Stageanorsv);
      }
      else
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      }
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && (_obj.Stageanorsv == null || anorsv.TaskModule.PublicFunctions.Module.SubtaskIsAllowedByStageSetting(_obj.Stageanorsv))
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}