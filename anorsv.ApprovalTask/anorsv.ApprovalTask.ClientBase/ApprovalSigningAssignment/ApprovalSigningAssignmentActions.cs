﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalSigningAssignment;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalSigningAssignmentActions
  {
    public virtual void CreateManyAddendumanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        //RSV.Module.Docflow.Functions.Module.AddManyAddendumDialog(document);
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public virtual void ChangeSigningTypeanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        return;
      }
      
      _obj.SigningTypeChangedanorsv = true;
      _obj.Complete(ApprovalSigningAssignment.Result.ForRevision);
      e.CloseFormAfterAction = true;
    }

    public virtual bool CanChangeSigningTypeanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool assignmnentInProcess = _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess;
      bool isPerformerOrSubstitute = false;
      bool inSigningTypeChangeRole = false;
      
      if (e.Params.Contains("RsvIsPerformerOrSubstitute"))
      {
        e.Params.TryGetValue("RsvIsPerformerOrSubstitute", out isPerformerOrSubstitute);
      }
      else
      {
        isPerformerOrSubstitute = anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
        e.Params.AddOrUpdate("RsvIsPerformerOrSubstitute", isPerformerOrSubstitute);
      }
      
      if (e.Params.Contains("RsvInSigningTypeChangeRole"))
      {
        e.Params.TryGetValue("RsvInSigningTypeChangeRole", out inSigningTypeChangeRole);
      }
      else
      {
        inSigningTypeChangeRole = anorsv.ApprovalTaskModule.PublicFunctions.Module.InSiginingTypeChangeRole(Sungero.Company.Employees.Current);
        e.Params.AddOrUpdate("RsvInSigningTypeChangeRole", inSigningTypeChangeRole);
      }
      
      return assignmnentInProcess && isPerformerOrSubstitute && inSigningTypeChangeRole;
    }

    public override void Sign(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }
      
      // Отключено как непубличный функционал
      // Регистрация
//      var outgoingLetterDocKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.RecordManagement.Module.RecordManagement.PublicConstants.Module.DocKind.BCHPOutgoingLetterKind);
//      if (sline.RSV.OutgoingLetters.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault()))
//      {
//        var eDoc = sline.RSV.OutgoingLetters.As(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
//        if (Equals(eDoc.DocumentKind, outgoingLetterDocKind) && eDoc.BCHPDocParamsanorsv != null)
//        {
//          var operation = Sungero.Docflow.RegistrationSetting.SettingType.Registration;
//          var registers = Sungero.Docflow.PublicFunctions.OfficialDocument.GetDocumentRegistersByDocument(eDoc, operation);
//          var defaultDocumentRegister = anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.GetDefaultDocRegister(eDoc, registers, operation);
//          var nextNumber = String.Empty;
//          if (defaultDocumentRegister != null)
//          {
//            var regDate = Calendar.Today;
//            Sungero.Docflow.PublicFunctions.OfficialDocument.RegisterDocument(eDoc, defaultDocumentRegister, regDate, nextNumber, false, true);
//          }
//          
//          //Сформировать тело документа по шаблону
//          var template = Sungero.Content.ElectronicDocumentTemplates.As(Sungero.Docflow.DocumentTemplates.GetAll().FirstOrDefault(t => t.DocumentKinds.Any(k => Equals(k.DocumentKind, outgoingLetterDocKind))
//                                                                                                                  && t.Status == Sungero.Docflow.DocumentTemplate.Status.Active));
//          using (var body = template.LastVersion.Body.Read())
//          {
//            //Обновим тело последней версии документа
//            eDoc.LastVersion.Body.Write(body);
//            var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)eDoc;
//            internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
//            eDoc.Save();
//            
//            //Создадим новую версию документа для ее подписания и перевода в pdf c отметкой
//            eDoc.CreateVersionFrom(body, template.AssociatedApplication.Extension);
//            internalEntity = (Sungero.Domain.Shared.IExtendedEntity)eDoc;
//            internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
//            eDoc.Save();
//          }
//        }
//      }
      
      base.Sign(e);
    }

    public override bool CanSign(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanSign(e);
    }

    public override void ConfirmSign(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.ConfirmSign(e);
    }

    public override bool CanConfirmSign(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanConfirmSign(e);
    }

    public override void Abort(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Abort(e);
    }

    public override bool CanAbort(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanAbort(e);
    }

    public override void ForRevision(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.ForRevision(e);
    }

    public override bool CanForRevision(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanForRevision(e);
    }

    public virtual void ApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var report = anorsv.OfficialDocument.Reports.GetApprovalSheetByProcessReport();
      report.Document = _obj.DocumentGroup.OfficialDocuments.Single();;
      report.Open();
    }

    public virtual bool CanApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return !approvalSheetBySignsIsValid;
    }

    public override void ApprovalForm(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ApprovalForm(e);
    }

    public override bool CanApprovalForm(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return approvalSheetBySignsIsValid;
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = null;
      
      if (_obj.Stage != null)
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj, sline.RSV.ApprovalStages.As(_obj.Stage));
      }
      else
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      }
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.SubtaskIsAllowedByStageSetting(_obj.Stage)
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}