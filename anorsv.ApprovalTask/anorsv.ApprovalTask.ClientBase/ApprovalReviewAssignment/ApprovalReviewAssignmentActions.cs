using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalReviewAssignment;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalReviewAssignmentActions
  {
    public virtual void CreateManyAddendumanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public override void Abort(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Abort(e);
    }

    public override bool CanAbort(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanAbort(e);
    }

    public override void ForRework(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.ForRework(e);
    }

    public override bool CanForRework(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanForRework(e);
    }

    public override void Informed(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Informed(e);
    }

    public override bool CanInformed(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanInformed(e);
    }

    public override void AddResolution(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.AddResolution(e);
    }

    public override bool CanAddResolution(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanAddResolution(e);
    }

    public override void AddActionItem(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      var documents = _obj.DocumentGroup.All.Concat(_obj.AddendaGroup.All);
      Dictionary<int, bool> forcedLocks = new Dictionary<int, bool>();
      Dictionary<int, Sungero.Domain.Shared.LockInfo> locksInfo = new Dictionary<int, Sungero.Domain.Shared.LockInfo>();
      
      
      foreach (var document in documents)
      {
        bool isForceLocked = false;
        var lockInfo = Locks.GetLockInfo(document);
        
        if (!lockInfo.IsLockedByOther && !lockInfo.IsLockedByMe)
        {
          isForceLocked = Locks.TryLock(document);
        }
        
        forcedLocks.Add(document.Id, isForceLocked);
        locksInfo.Add(document.Id, lockInfo);
      }
      
      var notLocked = forcedLocks.Any(fc => fc.Value == false && locksInfo[fc.Key].IsLockedByMe == false);
      
      if (!notLocked)
      {
        base.AddActionItem(e);
      }
      
      foreach (var forcedLock in forcedLocks)
      {
        if (forcedLock.Value)
        {
          Locks.Unlock(documents.Where(d => d.Id == forcedLock.Key).Single());
        }
      }
      
      if (notLocked)
      {
        e.AddError("Not locked document");
        e.Cancel();
      }

    }

    public override bool CanAddActionItem(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanAddActionItem(e);
    }

    public override void ApprovalForm(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ApprovalForm(e);
    }

    public override bool CanApprovalForm(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return approvalSheetBySignsIsValid;
    }

    public virtual void ApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var report = anorsv.OfficialDocument.Reports.GetApprovalSheetByProcessReport();
      report.Document = _obj.DocumentGroup.OfficialDocuments.Single();;
      report.Open();
    }

    public virtual bool CanApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return !approvalSheetBySignsIsValid;
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = null;
      
      if (_obj.Stage != null)
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj, sline.RSV.ApprovalStages.As(_obj.Stage));
      }
      else
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      }
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.SubtaskIsAllowedByStageSetting(_obj.Stage)
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}