﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalTask;
using Sungero.Company;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalTaskFunctions
  {

    /// <summary>
    /// Формирует сообщение о наличии замещения для согласующих и ответственных.
    /// </summary>
    /// <param name="approvers">Список проверяемых работников.</param>
    /// <param name="maxDeadline">Ограничительная дата до которой анализируется наличие замещений.</param>
    /// <returns>Список сообщений о гайденых замещениях.</returns>
    [Public]
    public static List<anorsv.ApprovalTaskModule.Structures.Module.ISubstitutionsMessage> SubstituteMessage(List<Sungero.Company.IEmployee> approvers, DateTime? maxDeadline)
    {
      var messageList = new List<anorsv.ApprovalTaskModule.Structures.Module.ISubstitutionsMessage>();
      
      if (!approvers.Any())
        return messageList;
      
      foreach (var approver in approvers.Distinct())
      {
        var substitutes = Sungero.CoreEntities.Substitutions
          .GetAll(s => s.User.Equals(Users.As(approver))
                  && s.StartDate != null
                  && s.EndDate != null && Calendar.Today <= s.EndDate)
          .ToList();
        
        foreach (var substitute in substitutes)
        {
          var approverFullName = CommonLibrary.PersonFullName
            .Create(approver.Person.LastName,
                    approver.Person.FirstName,
                    approver.Person.MiddleName);
          
          var substituteFullName = CommonLibrary.PersonFullName
            .Create(Employees.As(substitute.Substitute).Person.LastName,
                    Employees.As(substitute.Substitute).Person.FirstName,
                    Employees.As(substitute.Substitute).Person.MiddleName);
          
          if (substitute.StartDate <= Calendar.Today)
            messageList.Add(
              anorsv.ApprovalTaskModule.Structures.Module.SubstitutionsMessage
              .Create(
                anorsv.ApprovalTask.ApprovalTasks.Resources.EmployeeReplacesFormat(
                  approverFullName
                  , CaseConverter.ConvertPersonFullNameToTargetDeclension(substituteFullName, DeclensionCase.Ablative)
                  , substitute.StartDate.Value.ToString("d"), substitute.EndDate.Value.ToString("d"))));
          
          if (substitute.StartDate > Calendar.Today
              && ((substitute.StartDate < maxDeadline && maxDeadline != null) || (maxDeadline == null)))
            messageList.Add(
              anorsv.ApprovalTaskModule.Structures.Module.SubstitutionsMessage
              .Create(
                anorsv.ApprovalTask.ApprovalTasks.Resources.EmployeeWillReplaceFormat(
                  approverFullName
                  , CaseConverter.ConvertPersonFullNameToTargetDeclension(substituteFullName, DeclensionCase.Ablative)
                  , substitute.StartDate.Value.ToString("d"), substitute.EndDate.Value.ToString("d"))));
        }
      }
      
      return messageList;
    }

    [Public]
    public string CreateApprovalSheet()
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      var hasSignatures = Functions.ApprovalTask.Remote.SLHasSignatureForApprovalSheetReport(_obj);
      if (!hasSignatures)
      {
        Dialogs.NotifyMessage(Sungero.Docflow.OfficialDocuments.Resources.DocumentIsNotSigned);
        return string.Empty;
      }
      else
      {
        var path = Path.GetTempPath() + Guid.NewGuid().ToString() + ".pdf";
        var report = Sungero.Docflow.Reports.GetApprovalSheetReport();
        report.Document = document;
        return Path.GetTempPath() + report.ExportToFile(path);
      }
    }
  }
}
