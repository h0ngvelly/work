﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalTask;
using Sungero.Company;

namespace anorsv.ApprovalTask
{
  partial class ApprovalTaskClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      //видимость вложений в зависимости от типа документа "Заявка на закупку"
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var isPurchaseRequest = contractAttGroupAvailable && sline.CustomModule.PurchaseRequests.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.MemoGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.ContractGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.TechnicalSpecificationGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Properties.Typicalanorsv.IsVisible = isPurchaseRequest;
      
      //видимость вложений в зависимости от типа и вида документа "ЛНА"
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      var isLNA = orderAttGroupAvailable && anorsv.DocflowModule.LNAs.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.OrderGroupanorsv.IsVisible = false;
      if (isLNA == true)
      {
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        _obj.State.Attachments.OrderGroupanorsv.IsVisible = (lnaGenActivitiesKind != null && _obj.DocumentGroup.OfficialDocuments.FirstOrDefault().DocumentKind.Equals(lnaGenActivitiesKind));
      }
      
      var contractApprovalRule = Sungero.Contracts.ContractsApprovalRules.As(_obj.ApprovalRule);
      var canEditPreApprovers = false;
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      // Для СЗ - Предварительно согласующие по Тематикам СЗ с настроенным предварительным согласованием
      var memo = sline.RSV.Memos.As(document);
      var doc = anorsv.OfficialDocument.OfficialDocuments.As(document);
      if (memo != null)
      {
        var memoTopic = memo.MemoTopicanorsv;
        canEditPreApprovers = canEditPreApprovers || (memoTopic != null && memoTopic.PreAgreement == true); // && !memoTopic.PreApprovers.Any()
      }
      
      // Для всех - Предварительно согласующие по Тематикам общим с настроенным предварительным согласованием
      if (doc != null)
      {
        var baseTopic = doc.Topicanorsv;
        canEditPreApprovers = canEditPreApprovers || (baseTopic != null && baseTopic.PreAgreement == true); // && !baseTopic.PreApprovers.Any()
      }
      
      var approvalRule = sline.RSV.ApprovalRules.As(_obj.ApprovalRule);
      if (approvalRule != null)
      {
        // Исправлено неверное сравнение типа этапа с типом
        var stages = approvalRule.Stages.Where(a => a.StageType == Sungero.Docflow.ApprovalRuleStages.StageType.Approvers);
        _obj.State.Properties.PreApproversAnorsvanorsv.IsEnabled = true;
        var isStage = false;
        foreach (var stage in stages)
        {
          var rsvStage = sline.RSV.ApprovalStages.As(stage.Stage);
          if(rsvStage != null)
          {
            if(rsvStage.PreliminaryApprovalBlockanorsv.Value)
              isStage = rsvStage.PreliminaryApprovalBlockanorsv.Value;
          }
        }
        
        if (!isStage)
        {
          // Предварительно согласующие не доступны для заполнения, если нет блока предварительного согласования
          _obj.State.Properties.PreApproversAnorsvanorsv.IsEnabled = false;
          
          //если есть согласующие, но нет блока предварительное согласование
          if(_obj.PreApproversAnorsvanorsv.Any() || canEditPreApprovers)
            e.AddInformation(anorsv.ApprovalTask.ApprovalTasks.Resources.NoPreApprovalStage);
        }
        //если нет согласующих, но есть блок предварительное согласование
        else
        {
          // Предварительно согласующие не доступны для заполнения, если в тематике запрещено предварительное согласование
          if (!canEditPreApprovers)
            _obj.State.Properties.PreApproversAnorsvanorsv.IsEnabled = false;
          
          // TODO MED: перенос вывода сообщения об отсутствии предсогласующих
          //if(!_obj.PreApproversAnorsvanorsv.Any())
          //  e.AddWarning(anorsv.ApprovalTask.ApprovalTasks.Resources.NoApprovalsInPreApprovalBlocks);
        }
        //если нет согласующих, но есть блок предварительное согласование
//        else if(!_obj.PreApproversAnorsvanorsv.Any())
//          e.AddWarning(anorsv.ApprovalTask.ApprovalTasks.Resources.NoApprovalsInPreApprovalBlocks);
      }
      
    }
    
    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      if (_obj.Status != anorsv.ApprovalTask.ApprovalTask.Status.InProcess)
        Functions.ApprovalTask.UpdateReglamentApprovers(_obj);
      
      base.Refresh(e);
      
      //видимость вложений в зависимости от типа документа "Заявка на закупку"
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var isPurchaseRequest = contractAttGroupAvailable && sline.CustomModule.PurchaseRequests.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.MemoGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.ContractGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.TechnicalSpecificationGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Properties.Typicalanorsv.IsVisible = isPurchaseRequest;
      
      //видимость вложений в зависимости от типа и вида документа "ЛНА"
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      var isLNA = orderAttGroupAvailable && anorsv.DocflowModule.LNAs.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.OrderGroupanorsv.IsVisible = false;
      if (isLNA == true)
      {
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        _obj.State.Attachments.OrderGroupanorsv.IsVisible = (lnaGenActivitiesKind != null && _obj.DocumentGroup.OfficialDocuments.FirstOrDefault().DocumentKind.Equals(lnaGenActivitiesKind));
      }
      
      /*Рахимова. Вывод сообщения о замещаемых согласующих*/
      var approvers = new List <Sungero.Company.IEmployee> ();
      
      // Обязательные согласующие.
      var recipients = _obj.ReqApprovers.Select(a => a.Approver).ToList();
      
      // Доп.согласующие.
      var additionalApprovers = _obj.AddApprovers.Select(a => a.Approver).ToList();
      additionalApprovers.AddRange(_obj.AddApproversExpanded.Select(a => a.Approver).ToList());
      //Предварительные
      additionalApprovers.AddRange(_obj.PreApproversAnorsvanorsv.Select(a => a.Approver).ToList());
      if (additionalApprovers.Any())
        recipients.AddRange(additionalApprovers);
      approvers.AddRange(Sungero.Docflow.PublicFunctions.Module.GetEmployeesFromRecipients(recipients));
      //Подписывающий и адресаты
      approvers.Add(_obj.Signatory);
      approvers.Add(_obj.Addressee);
      
      approvers.AddRange(_obj.Addressees.Select(a=>a.Addressee).ToList());
      var approvalRule = sline.RSV.ApprovalRules.As(_obj.ApprovalRule);
      if (approvalRule != null)
      {
        var isShowSubstitutes = sline.RSV.ApprovalRules.GetAll(a => a.Equals(_obj.ApprovalRule)).FirstOrDefault().ShowSubstitutesanorsv;
        var deadline = Calendar.Today; //PublicFunctions.ApprovalTask.GetDeadlineFromTask(_obj);
        
        var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, deadline);
        int substituteCount = messageList.Count;

        if (messageList.Any())// && isShowSubstitutes.Value == true)
        {
          if (substituteCount < 4)
            e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
          else
          {
            e.Params.AddOrUpdate("ShowSubstituteMessage", string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
            e.AddInformation(anorsv.ApprovalTask.ApprovalTasks.Resources.ShowSubstitutionsByTaskPerformers, _obj.Info.Actions.ShowSubstitutedMessageanorsv);
          }
        }
        
        // Вывод сообщения об отсутствии предварительно согласующих
        var stages = approvalRule.Stages.Where(a => a.StageType == Sungero.Docflow.ApprovalRuleStages.StageType.Approvers);
        bool isStage = false;
        foreach (var stage in stages)
        {
          var rsvStage = sline.RSV.ApprovalStages.As(stage.Stage);
          if(rsvStage != null)
          {
            if(rsvStage.PreliminaryApprovalBlockanorsv.Value)
              isStage = rsvStage.PreliminaryApprovalBlockanorsv.Value;
          }
        }
        
        if (isStage && !_obj.PreApproversAnorsvanorsv.Any())
          e.AddWarning(anorsv.ApprovalTask.ApprovalTasks.Resources.NoApprovalsInPreApprovalBlocks);
      }
      
      

    }
  }
}
