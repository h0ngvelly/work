using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalTask;
using docRelation = anorsv.RelationModule;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalTaskActions
  {






    public virtual void ShowSubstitutedMessageanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var message = "";
      e.Params.TryGetValue("ShowSubstituteMessage", out message);
      Dialogs.ShowMessage(message, MessageType.Warning);
      
    }

    public virtual bool CanShowSubstitutedMessageanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public override void Start(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Диалог выбора связанных документов для добавления во вложения
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document == null)
        return;
      docRelation.PublicFunctions.Module.AddOtherGroupAttachmentsDialog(Sungero.Docflow.ApprovalTasks.As(_obj), document);
      
      base.Start(e);
    }

    public override bool CanStart(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanStart(e);
    }

    public virtual void ApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var report = anorsv.OfficialDocument.Reports.GetApprovalSheetByProcessReport();
      report.Document = _obj.DocumentGroup.OfficialDocuments.Single();;
      report.Open();
    }

    public virtual bool CanApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return !approvalSheetBySignsIsValid;
    }

    public override void ApprovalForm(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ApprovalForm(e);
    }

    public override bool CanApprovalForm(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return approvalSheetBySignsIsValid;
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.CreateSubtask(e);
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return false;
    }

  }

}