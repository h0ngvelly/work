﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalAssignment;
using docRelation = anorsv.RelationModule;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalAssignmentActions
  {
    public virtual void CreateManyAddendumsanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        //RSV.Module.Docflow.Functions.Module.AddManyAddendumDialog(document);
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumsanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    private void CreateSubTask()
    {
      var task = Sungero.Workflow.SimpleTasks.CreateAsSubtask(_obj);
      task.Author = Users.Current;
      
      var step1 = task.RouteSteps.AddNew();
      step1.AssignmentType = Sungero.Workflow.SimpleTask.AssignmentType.Notice;
      var role1 = sline.CustomModule.Settings.GetAll().FirstOrDefault().RoleNoticeZGD;
      step1.Performer = role1;
      step1.Deadline = _obj.Deadline;
      
      
      var step2 = task.RouteSteps.AddNew();
      step2.AssignmentType = Sungero.Workflow.SimpleTask.AssignmentType.Assignment;
      var role2 = sline.CustomModule.Settings.GetAll().FirstOrDefault().RoleAssigmentZGD;
      step2.Performer = role2;
      step2.Deadline = _obj.Deadline;
      
      task.NeedsReview = true;
      task.Deadline = _obj.Deadline;
      task.Subject = " >>> Согласование закупочной документации";
      var prefix = sline.CustomModule.Settings.GetAll().FirstOrDefault().SubtasksPrefix;
      if (task.Subject.Contains(prefix))
        task.Subject.Replace(prefix, string.Empty);
      
      task.ActiveText = "Согласование закупочной документации";
      task.Save();
      task.Start();
      
      _obj.SubtaskZGDanorsv = task;
      _obj.Save();
      
      Dialogs.NotifyMessage("Подзадача подчиненным отправлена!");
    }
    
    public virtual void CreateSubtaskanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (_obj.SubtaskZGDanorsv == null)
      {
        this.CreateSubTask();
      }
      else
      {
        var dialog = Dialogs.CreateTaskDialog("Вы уже отправляли подзадачу своим подчиненным. Какое действие выполнить?");
        var openButton = dialog.Buttons.AddCustom("Открыть отправленную подзадачу");
        var newButton = dialog.Buttons.AddCustom("Отправить новую подзадачу");
        var cancel = dialog.Buttons.AddCancel();
        dialog.Buttons.Default = openButton;
        var result = dialog.Show();

        if (result == openButton)
        {
          _obj.SubtaskZGDanorsv.ShowModal();
        }
        if (result == newButton)
        {
          var dialog2 = Dialogs.CreateTaskDialog("При старте новой подзадачи, старая подзадача будет прекращена. Продолжить?");
          dialog2.Buttons.AddYesNo();
          var result2 = dialog2.Show();
          if (result2 == DialogButtons.Yes)
          {
            var task = _obj.SubtaskZGDanorsv;
            task.Abort();
            
            this.CreateSubTask();
          }
        }
        
      }
    }

    public virtual bool CanCreateSubtaskanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      var stage = _obj.Stage;
      var myStage = sline.RSV.ApprovalStages.As(stage);
      if (myStage.ZGDsline == true)
        return true;
      else
        return false;
    }

    public virtual void AgreeWithoutFinalVerificationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var action = new Sungero.Workflow.Client.ExecuteResultActionArgs(e.FormType, e.Entity, e.Action);
      base.Approved(action);
      _obj.SkipStageWithoutFinalVerificationanorsv = true;
      
      // Закрываем карточку задания после выполнения.
      e.CloseFormAfterAction = true;
      // Выполняем задание.
      _obj.Complete(Result.Approved);
    }

    public virtual bool CanAgreeWithoutFinalVerificationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      var clerk = Sungero.Docflow.PublicFunctions.DocumentRegister.Remote.GetClerks();
      
      return _obj.Stage.AgreeWithoutFinalVerificationanorsv.Value && Substitutions.SubstitutedUsers.Where(r => r.Equals(_obj.Performer)).Any() &&
        _obj.Status.Value == Sungero.Workflow.Assignment.Status.InProcess && Users.Current.IncludedIn(clerk) && _obj.Performer.IncludedIn(clerk) &&
        Locks.GetLockInfo(_obj).IsLockedByOther != true;
    }

    public virtual void AddDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.AddDocumentLinkDialog(document, _obj.Id);
    }

    public virtual bool CanAddDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      //bool canModifyRelation = false;
      //if (_obj.DocumentGroup.OfficialDocuments.Any() && e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
      //  if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
      //    return canModifyRelation;
      bool canModifyRelation = (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess &&
                                _obj.AccessRights.CanUpdate() &&
                                _obj.DocumentGroup.OfficialDocuments.Any());
      
      return canModifyRelation;
    }

    public virtual void DeleteDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(document);
    }

    public virtual bool CanDeleteDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
          return canModifyRelation;
      
      return canModifyRelation;
    }

    public virtual void ChangeDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.ChangeDocumentLinkDialog(document);
    }

    public virtual bool CanChangeDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
          return canModifyRelation;
      
      return canModifyRelation;
    }

    public virtual void CreateDocumentLinkanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.AddDocumentLinkDialog(document, _obj.Id);
    }

    public virtual bool CanCreateDocumentLinkanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess && _obj.AccessRights.CanUpdate() && _obj.DocumentGroup.OfficialDocuments.Any());
    }

    public override void Forward(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Forward(e);
    }

    public override bool CanForward(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanForward(e);
    }

    public override void ForRevision(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.ForRevision(e);
    }

    public override bool CanForRevision(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanForRevision(e);
    }

    public override void Approved(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Approved(e);
    }

    public override bool CanApproved(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanApproved(e);
    }

    public virtual void ApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var report = anorsv.OfficialDocument.Reports.GetApprovalSheetByProcessReport();
      report.Document = _obj.DocumentGroup.OfficialDocuments.Single();;
      report.Open();
    }

    public virtual bool CanApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return !approvalSheetBySignsIsValid;
    }

    public override void ApprovalForm(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ApprovalForm(e);
    }

    public override bool CanApprovalForm(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Если нет основного документа, то отчёт "Лист согласования не доступен"
      if (document == null)
      {
        return false;
      }
      
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(document);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return approvalSheetBySignsIsValid;
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = null;
      
      if (_obj.Stage != null)
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj, _obj.Stage);
      }
      else
      {
        subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      }
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.SubtaskIsAllowedByStageSetting(_obj.Stage)
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}