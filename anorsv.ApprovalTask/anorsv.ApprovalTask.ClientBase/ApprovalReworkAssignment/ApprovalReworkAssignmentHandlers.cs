using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalReworkAssignment;
using docRelation = anorsv.RelationModule;

namespace anorsv.ApprovalTask
{
  partial class ApprovalReworkAssignmentClientHandlers
  {

    public override void Closing(Sungero.Presentation.FormClosingEventArgs e)
    {
      base.Closing(e);
      
      anorsv.ServiceLibrary.PublicFunctions.Module.MarkSubtaskExistInAssignmentSubject(_obj);
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      var rule = sline.RSV.ApprovalRules.GetAll(a => a.Equals(ApprovalTasks.As(_obj.Task))).FirstOrDefault();
      var isShowSubstitutes = true;
      if (rule != null)
        isShowSubstitutes = rule.ShowSubstitutesanorsv.HasValue ? rule.ShowSubstitutesanorsv.Value : true;
      
      if (_obj.Addressee != null && isShowSubstitutes == true)
      {
        var approvers = new List <Sungero.Company.IEmployee> ();
        approvers.Add(_obj.ForwardPerformer);
        approvers.Add(_obj.Signatory);
        var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, _obj.Deadline);
        
        if (messageList.Any())
        {
          e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
        }
      }
      
      _obj.State.Properties.PreApproversanorsv.IsEnabled = anorsv.ApprovalTaskModule.PublicFunctions.Module.PreApproversEnabled(anorsv.ApprovalTask.ApprovalTasks.As(_obj.Task));
      
      base.Refresh(e);
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        // Проверить возможность изменения связей
        bool canModifyRelations = docRelation.PublicFunctions.Module.Remote.CanChangeDocumentRelations(document, Sungero.Workflow.AssignmentBases.As(_obj));
        e.Params.AddOrUpdate("AnoRsvCanModifyRelationsFromTask", canModifyRelations);
        
        // Проверить возможность изменений связей с Закупкой
        bool canModifyPurchaseRelations = sline.CustomModule.PurchaseRequests.Is(document);
        e.Params.AddOrUpdate("AnoRsvCanModifyPurchaseRelations", canModifyPurchaseRelations);
        
        // Проверить возможность изменений связей с ЛНА
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        bool canModifyLNARelations = (anorsv.DocflowModule.LNAs.Is(document) &&
                                      lnaGenActivitiesKind != null &&
                                      document.DocumentKind.Equals(lnaGenActivitiesKind));
        e.Params.AddOrUpdate("AnoRsvCanModifyLNARelations", canModifyLNARelations);
      }
      
      _obj.State.Properties.PreApproversanorsv.IsEnabled = anorsv.ApprovalTaskModule.PublicFunctions.Module.PreApproversEnabled(anorsv.ApprovalTask.ApprovalTasks.As(_obj.Task));
      
      //видимость вложений в зависимости от типа документа "Заявка на закупку"
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var isPurchaseRequest = contractAttGroupAvailable && sline.CustomModule.PurchaseRequests.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.MemoGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.ContractGroupanorsv.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.TechnicalSpecificationGroupanorsv.IsVisible = isPurchaseRequest;
      
      //видимость вложений в зависимости от вида документ "ЛНА по общ.деятельности"
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      _obj.State.Attachments.OrderGroupanorsv.IsVisible = false;
      if (orderAttGroupAvailable && anorsv.DocflowModule.LNAs.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault()))
      {
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        _obj.State.Attachments.OrderGroupanorsv.IsVisible = (lnaGenActivitiesKind != null && _obj.DocumentGroup.OfficialDocuments.FirstOrDefault().DocumentKind.Equals(lnaGenActivitiesKind));
      }
    }
  }
}
