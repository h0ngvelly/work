﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalReworkAssignment;
using docRelation = anorsv.RelationModule;

namespace anorsv.ApprovalTask.Client
{
  partial class ApprovalReworkAssignmentActions
  {
    public virtual void CreateManyAddendumanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        //RSV.Module.Docflow.Functions.Module.AddManyAddendumDialog(document);
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        _obj.Save();
      }
    }

    public virtual bool CanCreateManyAddendumanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public virtual void CreateMemoanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (!_obj.MemoGroupanorsv.Memos.Any())
      {
        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
        if (document != null)
        {
          anorsv.ApprovalTaskModule.PublicFunctions.Module.CreateMemoWithoutDialog(Sungero.Docflow.ApprovalTasks.As(_obj.Task), _obj.Id, document);
          _obj.Save();
        }
      }
      else
        Dialogs.ShowMessage(anorsv.ApprovalTaskModule.Resources.DocAlreadyCreatedMemoText, MessageType.Error);
    }

    public virtual bool CanCreateMemoanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var needMemo = contractAttGroupAvailable 
        && (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
        && (_obj.Stageanorsv == null || anorsv.ApprovalTaskModule.PublicFunctions.Module.NeedMemoForPurchaseByStageSetting(_obj.Stageanorsv))
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
      return needMemo;
    }

    public virtual void CreateTSanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (!_obj.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Any())
      {
        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
        if (document != null)
        {
          anorsv.ApprovalTaskModule.PublicFunctions.Module.TSDialog(_obj, document);
          _obj.Save();
        }
      }
      else
        Dialogs.ShowMessage(anorsv.ApprovalTaskModule.Resources.DocAlreadyCreatedTSText, MessageType.Error);
    }

    public virtual bool CanCreateTSanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      return (contractAttGroupAvailable && _obj.ApprovalPurhaseanorsv.GetValueOrDefault());
      //return true;
    }

    public virtual void CreateDraftContractanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (!_obj.ContractGroupanorsv.Contracts.Any())
      {
        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
        if (document != null)
        {
          anorsv.ApprovalTaskModule.PublicFunctions.Module.DraftContractDialog(_obj, document);
          _obj.Save();
        }
      }
      else
        Dialogs.ShowMessage(anorsv.ApprovalTaskModule.Resources.DocAlreadyCreatedContractText, MessageType.Error);
    }

    public virtual bool CanCreateDraftContractanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      return (contractAttGroupAvailable && _obj.ApprovalPurhaseanorsv.GetValueOrDefault());
      //return true;
    }

    public virtual void CreateImpOrderanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
//      if (!_obj.OrderGroupanorsv.Orders.Any())
//      {
//        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
//        if (document != null)
//        {
//          anorsv.ApprovalTaskModule.PublicFunctions.Module.CreateOrderWithoutDialog(Sungero.Docflow.ApprovalTasks.As(_obj.Task), _obj.Id, document);
//          _obj.Save();
//        }
//      }  
//      else
//        Dialogs.ShowMessage(anorsv.ApprovalTaskModule.Resources.DocAlreadyCreatedOrderText, MessageType.Error);
    }

    public virtual bool CanCreateImpOrderanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {      
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
      var needOrder = orderAttGroupAvailable 
        && (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
        && (document != null && lnaGenActivitiesKind != null && document.DocumentKind.Equals(lnaGenActivitiesKind))
        && (anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current)));
      return needOrder;
    }

    public virtual void AddDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.AddDocumentLinkDialog(document, _obj.Id);
    }

    public virtual bool CanAddDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      //bool canModifyRelation = false;
      //if (_obj.DocumentGroup.OfficialDocuments.Any() && e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
      //  if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
      //    return canModifyRelation;
      bool canModifyRelation = (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess &&
                                _obj.AccessRights.CanUpdate() &&
                                _obj.DocumentGroup.OfficialDocuments.Any());
      
      return canModifyRelation;
    }

    public virtual void DeleteDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      bool canModifyPurchaseRelations = false;
      if (e.Params.Contains("AnoRsvCanModifyPurchaseRelations"))
        e.Params.TryGetValue("AnoRsvCanModifyPurchaseRelations", out canModifyPurchaseRelations);
      
      bool canModifyLNARelations = false;
      if (e.Params.Contains("AnoRsvCanModifyLNARelations"))
        e.Params.TryGetValue("AnoRsvCanModifyLNARelations", out canModifyLNARelations);
          
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        if (canModifyPurchaseRelations)
        {
          var availableTypeList = RelationTypes.GetAll()
          .Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName ||
                      r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName || 
                      r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName)
          .ToList();
          docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(document, availableTypeList);
        }
        else if (canModifyLNARelations)
        {
          var availableTypeList = RelationTypes.GetAll()
          .Where(r => r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName)
          .ToList();
          docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(document, availableTypeList);
        }
        else
        {
          docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(document);
        }
      }
    }

    public virtual bool CanDeleteDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
          return canModifyRelation;
      
      return canModifyRelation;
    }

    public virtual void ChangeDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
//      bool canModifyPurchaseRelations = false;
//      if (e.Params.Contains("AnoRsvCanModifyPurchaseRelations"))
//        if (e.Params.TryGetValue("AnoRsvCanModifyPurchaseRelations", out canModifyPurchaseRelations));
      
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
//        if (canModifyPurchaseRelations)
//        {
//          var availableTypeList = RelationTypes.GetAll()
//          .Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName ||
//                      r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName || 
//                      r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName)
//          .ToList();
//          docRelation.PublicFunctions.Module.ChangeDocumentLinkDialog(document, availableTypeList);
//        }
//        else
//        {        
          docRelation.PublicFunctions.Module.ChangeDocumentLinkDialog(document);
//        }
      }
    }

    public virtual bool CanChangeDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
          return canModifyRelation;
      
      return canModifyRelation;
    }

    public virtual void CreateDocumentLinkanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.AddDocumentLinkDialog(_obj.DocumentGroup.OfficialDocuments.First(), _obj.Id);
    }

    public virtual bool CanCreateDocumentLinkanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return false;
      //return (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess && _obj.AccessRights.CanUpdate() && _obj.DocumentGroup.OfficialDocuments.Any());
    }


    public override void Forward(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      base.Forward(e);
    }

    public override bool CanForward(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanForward(e);
    }

    public override void ForReapproving(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }

      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      var preApproversList = _obj.PreApproversanorsv.Select(a => a.Approver).ToList();
      
      // Заполнение Предсогласующих из Тематики СЗ
      var memo = sline.RSV.Memos.As(document);
      if (memo != null && memo.MemoTopicanorsv != null)
        preApproversList.AddRange(memo.MemoTopicanorsv.PreApprovers.Select(a => a.Approver));
      
      // Заполнение Предсогласующих из Тематики общей
      var officialDocument = anorsv.OfficialDocument.OfficialDocuments.As(document);
      if (officialDocument != null && officialDocument.Topicanorsv != null)
        preApproversList.AddRange(officialDocument.Topicanorsv.PreApprovers.Select(a => a.Approver));
      
      var task = anorsv.ApprovalTask.ApprovalTasks.As(_obj.Task);
      task.PreApproversAnorsvanorsv.Clear();
      _obj.PreApproversanorsv.Clear();
      foreach (var approver in preApproversList.Distinct())
      {
        task.PreApproversAnorsvanorsv.AddNew().Approver = approver;
        _obj.PreApproversanorsv.AddNew().Approver = approver;
      }
      
      // Проверка наличия обязательных вложений в задании
      string needAttachmentMessage = anorsv.ApprovalTaskModule.PublicFunctions.Module.CheckAttachedDocuments(Sungero.Workflow.Assignments.As(_obj));
      if (!string.IsNullOrEmpty(needAttachmentMessage))
      {
        e.AddError(needAttachmentMessage);
        e.Cancel();
      }
      

      base.ForReapproving(e);
    }

    public override bool CanForReapproving(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return base.CanForReapproving(e);
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      subtask.ShowModal();
      _obj.Save();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}
