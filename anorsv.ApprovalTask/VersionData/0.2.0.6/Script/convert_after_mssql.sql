﻿if exists (select * from information_schema.COLUMNS where table_name = 'Sungero_WF_Assignment' and COLUMN_NAME = 'SubtaskZGDslin_RSV_sline')
begin
EXEC sp_rename 'dbo.Sungero_WF_Assignment.SubtaskZGDslin_RSV_sline', 'SubtaskZGDanor_Approv1_anorsv', 'COLUMN';
end

if exists (select * from information_schema.COLUMNS where table_name = 'Sungero_WF_Task' and COLUMN_NAME = 'StageNumSkipsl_RSV_sline')
begin
EXEC sp_rename 'dbo.Sungero_WF_Task.StageNumSkipsl_RSV_sline', 'StageNumSkipsa_Approv1_anorsv', 'COLUMN';
end