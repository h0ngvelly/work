﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalSigningAssignment;

namespace anorsv.ApprovalTask
{
  partial class ApprovalSigningAssignmentServerHandlers
  {

    public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
    {
      string lockedDocumentMessage = anorsv.TaskModule.PublicFunctions.Module.AttachedDocumentIsLocked(_obj.AllAttachments.ToList());
      if (!string.IsNullOrEmpty(lockedDocumentMessage))
      {
        e.AddError(lockedDocumentMessage);
        return;
      }

      base.BeforeComplete(e);
      
      if (_obj.Result == Result.ForRevision && _obj.SigningTypeChangedanorsv == true)
      {
        e.Result = Resources.SigningAssignmentResultSigningTypeChange;
      }
    }
  }

}
