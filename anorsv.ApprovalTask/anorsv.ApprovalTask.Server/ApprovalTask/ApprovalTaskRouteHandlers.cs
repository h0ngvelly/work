using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.ApprovalTask.ApprovalTask;

namespace anorsv.ApprovalTask.Server
{
  partial class ApprovalTaskRouteHandlers
  {

    public override void EndBlock6(Sungero.Docflow.Server.ApprovalAssignmentEndBlockEventArguments e)
    {
      base.EndBlock6(e);
      if (e.CreatedAssignments.Cast< anorsv.ApprovalTask.IApprovalAssignment>().Any(a => a.SkipStageWithoutFinalVerificationanorsv == true))
        _obj.SkipStageFinalVerificationanorsv = true;
    }

    public override void Script26Execute()
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      anorsv.TaskModule.PublicFunctions.Module.RefreshContractGroup(document, _obj.ContractGroupanorsv);
      anorsv.TaskModule.PublicFunctions.Module.RefreshMemoGroup(document, _obj.MemoGroupanorsv);
      anorsv.TaskModule.PublicFunctions.Module.RefreshOrderGroup(document, _obj.OrderGroupanorsv);
      anorsv.TaskModule.PublicFunctions.Module.RefreshTechnicalSpecificationGroup(document, _obj.TechnicalSpecificationGroupanorsv);
      Functions.ApprovalTask.SaveTaskInitiatorAccessRights(_obj);
      
      base.Script26Execute();
    }

    public override void StartAssignment9(Sungero.Docflow.IApprovalSigningAssignment assignment, Sungero.Docflow.Server.ApprovalSigningAssignmentArguments e)
    {
      base.StartAssignment9(assignment, e);
      
      var mainDocument = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      var addendum = Sungero.Docflow.Addendums.GetAll()
        .Where(x => x.LeadingDocument == mainDocument)
        .OrderByDescending(x => x.Id)
        .FirstOrDefault();
      if (addendum != null)
      {
        if (!assignment.AddendaGroup.All.Any(a => Equals(a, addendum)))
          assignment.AddendaGroup.All.Add(addendum);
      }
    }
    
    #region Functions
    // часто используемые функции
    public void CreateApprovalSheet()
    {
      try
      {
        var defaultStage = _obj.ApprovalRule.Stages.FirstOrDefault(s => s.Number == _obj.StageNumber);
        if (defaultStage != null)
        {
          var currentStage = sline.RSV.ApprovalStages.GetAll(x => x.Id == defaultStage.Stage.Id).FirstOrDefault();
          if (currentStage != null && currentStage.CreateApprovalSheetsline == true)
          {
            var mainDocument = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
            
            var hasSignatures = Functions.ApprovalTask.SLHasSignatureForApprovalSheetReport(_obj);
            if (!hasSignatures)
            {
              Logger.ErrorFormat(" >>> SOFTLINE >>> CreateApprovalForm >>> documentId='{0}', message: {1}", mainDocument.Id.ToString(), Sungero.Docflow.OfficialDocuments.Resources.DocumentIsNotSigned);
            }
            else
            {
              var report = Sungero.Docflow.Reports.GetApprovalSheetReport();
              report.Document = mainDocument;
              
              var relatedDocs = mainDocument.Relations.GetRelated("Addendum");
              if (relatedDocs.Any())
              {
                var addendum = Sungero.Docflow.Addendums.Null;
                foreach(var realtedDoc in relatedDocs)
                {
                  var mDoc = Sungero.Docflow.OfficialDocuments.As(realtedDoc);
                  if (mDoc != null)
                  {
                    if (mDoc.DocumentKind.Name.ToLower().Contains("ист согласов"))
                    {
                      addendum = Sungero.Docflow.Addendums.As(mDoc);
                      break;
                    }
                  }
                }
                
                if (addendum == null)
                {
                  Logger.DebugFormat(" >>> SOFTLINE >>> CreateApprovalForm >>> Has related, and Approval sheet create for documentID='{0}'.", mainDocument.Id.ToString());
                  addendum = Sungero.Docflow.Addendums.Create();
                  addendum.LeadingDocument = mainDocument;
                  if (addendum.State.Properties.Name.IsEnabled)
                    addendum.Name = "Лист согласования от " + Calendar.Today.Date.ToShortDateString();
                  else
                    addendum.Subject = "Лист согласования от " + Calendar.Today.Date.ToShortDateString();
                  addendum.DocumentKind = Sungero.Docflow.DocumentKinds.GetAll().FirstOrDefault(x => x.Name.ToLower().Contains("ист согласов"));
                }
                else
                {
                  Logger.DebugFormat(" >>> SOFTLINE >>> CreateApprovalForm >>> Approval sheet for documentID='{0}' already exist. Create new version.", mainDocument.Id.ToString());
                }
                
                addendum.CreateVersion();
                addendum.LastVersion.Body.Write(report.Export());
                addendum.LastVersion.AssociatedApplication = Sungero.Content.AssociatedApplications.GetByExtension("pdf");
                addendum.Save();
              }
              else
              {
                Logger.DebugFormat(" >>> SOFTLINE >>> CreateApprovalForm >>> No related, Approval sheet create for documentID='{0}'.", mainDocument.Id.ToString());
                var addendum = Sungero.Docflow.Addendums.Create();
                addendum.LeadingDocument = mainDocument;
                if (addendum.State.Properties.Name.IsEnabled)
                  addendum.Name = "Лист согласования от " + Calendar.Today.Date.ToShortDateString();
                else
                  addendum.Subject = "Лист согласования от " + Calendar.Today.Date.ToShortDateString();
                addendum.DocumentKind = Sungero.Docflow.DocumentKinds.GetAll().FirstOrDefault(x => x.Name.ToLower().Contains("ист согласов"));
                
                addendum.CreateVersion();
                addendum.LastVersion.Body.Write(report.Export());
                addendum.LastVersion.AssociatedApplication = Sungero.Content.AssociatedApplications.GetByExtension("pdf");
                addendum.Save();
              }
              
              Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, mainDocument);
            }
          }
        }
        else
          Logger.ErrorFormat(" >>> SOFTLINE >>> CreateApprovalForm >>> ERROR >>> defaultStage is NULL for taskId='{0}'", _obj.Id.ToString());
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> CreateApprovalForm >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }
    
    public void SetSkipStage()
    {
      var defaultStage = _obj.ApprovalRule.Stages.FirstOrDefault(s => s.Number == _obj.StageNumber);
      var currentStage = sline.RSV.ApprovalStages.GetAll(x => x.Id == defaultStage.Stage.Id).FirstOrDefault();
      
      if (currentStage != null && currentStage.IgnoreStagessline == true)
        _obj.StageNumSkipsanorsv = _obj.StageNumber;
      else
        _obj.StageNumSkipsanorsv = 0;
      
    }
    
    public void CheckNextStage()
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      var nextStageNumber = anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRuleBase.SLGetNextStageNumber(anorsv.AnorsvMainSolution.ApprovalRuleBases.As(_obj.ApprovalRule), document, _obj.StageNumber, _obj);
      if (nextStageNumber > 0)
      {
        var stage = _obj.ApprovalRule.Stages
          .Where(s => s.Number == nextStageNumber)
          .Where(s => s.StageType == Sungero.Docflow.ApprovalRuleBaseStages.StageType.Sign)
          .FirstOrDefault();
        
        if (stage != null)
        {
          CreateApprovalSheet();
        }
      }
    }
    
    public bool SkipStage()
    {
      var res = false;
      if (_obj.StageNumSkipsanorsv > 0)
      {
        if (_obj.StageNumSkipsanorsv != _obj.StageNumber)
          res = true;
      }
      return res;
    }
    
    public bool SkipForSecApproval()
    {
      var res = false;
      if (_obj.IterationId > 1)
      {
        var defaultStage = _obj.ApprovalRule.Stages.FirstOrDefault(s => s.Number == _obj.StageNumber);
        var currentStage = sline.RSV.ApprovalStages.GetAll(x => x.Id == defaultStage.Stage.Id).FirstOrDefault();
        if (currentStage != null && currentStage.NoSecondApprovesline == true)
          res = true;
      }
      return res;
    }
    
    public List<sline.RSV.IOutgoingLetterAnyApproverssline> AddApprovers()
    {
      List<sline.RSV.IOutgoingLetterAnyApproverssline> result = new List<sline.RSV.IOutgoingLetterAnyApproverssline>();
      
      var doc = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (Sungero.RecordManagement.OutgoingLetters.Is(doc) || sline.RSV.OutgoingLetters.Is(doc))
      {
        var mDoc = sline.RSV.OutgoingLetters.As(doc);
        if (mDoc != null)
        {
          if (mDoc.AnyApproverssline.Any())
          {
            var defaultStage = _obj.ApprovalRule.Stages.FirstOrDefault(s => s.Number == _obj.StageNumber);
            if (defaultStage != null)
            {
              var currentStage = sline.RSV.ApprovalStages.GetAll(x => x.Id == defaultStage.Stage.Id).FirstOrDefault();
              
              if (mDoc.AnyApproverssline.Any(x => x.Stage == currentStage))
              {
                result.AddRange(mDoc.AnyApproverssline.Where(x => x.Stage == currentStage));
              }
            }
          }
        }
      }
      return result;
    }
    #endregion
    
    public override void StartAssignment6(Sungero.Docflow.IApprovalAssignment assignment, Sungero.Docflow.Server.ApprovalAssignmentArguments e)
    {
      base.StartAssignment6(assignment, e);
      
      var assignmentAnorsv = anorsv.ApprovalTask.ApprovalAssignments.As(assignment);
      
      var contract = assignmentAnorsv.ContractGroupanorsv.Contracts.FirstOrDefault();
      var technicalSpecification = assignmentAnorsv.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.FirstOrDefault();
      
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      if (contractAttGroupAvailable && contract != null && technicalSpecification != null)
        assignmentAnorsv.Typicalanorsv = contract.IsStandard.Value && technicalSpecification.IsStandard.Value;
      
      if (contractAttGroupAvailable && contract != null && technicalSpecification == null)
        assignmentAnorsv.Typicalanorsv = contract.IsStandard.Value;
    }

    public override void StartBlock42(Sungero.Docflow.Server.ApprovalNotificationArguments e)
    {
      base.StartBlock42(e);
    }

    public override void StartBlock31(Sungero.Docflow.Server.ApprovalCheckingAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.SimpleAgr);

      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.SimpleAgr);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || currentStage == null || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }
      
      // Отключено как непубличный функционал
      //            if (currentStage.StageType == sline.RSV.ApprovalStage.StageType.SimpleAgr
      //                && sline.RSV.ApprovalStages.As(currentStage.Stage).TaskTextanorsv != null)
      //              e.Block.Text = sline.RSV.ApprovalStages.As(currentStage.Stage).TaskTextanorsv;
      
      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }

      base.StartBlock31(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock30(Sungero.Docflow.Server.ApprovalSimpleAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.SimpleAgr);

      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.SimpleAgr);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || currentStage == null || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }
      
      
      // Отключено как непубличный функционал
      //      if (currentStage.StageType == sline.RSV.ApprovalStage.StageType.SimpleAgr
      //          && sline.RSV.ApprovalStages.As(currentStage.Stage).TaskTextanorsv != null)
      //        e.Block.Text = sline.RSV.ApprovalStages.As(currentStage.Stage).TaskTextanorsv;

      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock30(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock27(Sungero.Docflow.Server.ApprovalCheckReturnAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.CheckReturn);
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.CheckReturn);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }

      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock27(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock28(Sungero.Docflow.Server.ApprovalSendingAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.Sending);
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.Sending);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }

      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock28(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock37(Sungero.Docflow.Server.ApprovalExecutionAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.Execution);
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.Execution);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }

      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock37(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock36(Sungero.Docflow.Server.ApprovalReviewAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.Review);
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.Review);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }

      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock36(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock20(Sungero.Docflow.Server.ApprovalPrintingAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.Print);
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.Print);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }

      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock20(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock23(Sungero.Docflow.Server.ApprovalRegistrationAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.Register);
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.Register);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }

      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock23(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock6(Sungero.Docflow.Server.ApprovalAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.Approvers);
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.Approvers);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }
      
      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock6(e);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock39(Sungero.Docflow.Server.ApprovalNotificationArguments e)
    {
      var needSkip = _obj.SkipStageanorsv ?? false;
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.Notice);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }
      
      base.StartBlock39(e);
    }

    public override void EndBlock5(Sungero.Docflow.Server.ApprovalReworkAssignmentEndBlockEventArguments e)
    {
      base.EndBlock5(e);
    }

    public override void StartBlock5(Sungero.Docflow.Server.ApprovalReworkAssignmentArguments e)
    {
      // Сбросить номер очередного этапа.
      _obj.StageNumber = null;
      
      // Апнуть Iteration.
      _obj.Iteration++;

      if (_obj.SigningTypeChangedanorsv ?? false)
      {
        return;
      }

      base.StartBlock5(e);
    }

    public override void StartBlock3(Sungero.Docflow.Server.ApprovalManagerAssignmentArguments e)
    {
      var needSkip = anorsv.ApprovalTaskModule.PublicFunctions.Module.SkipStage(_obj, sline.RSV.ApprovalStage.StageType.Manager);
      
      var currentStage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(_obj, sline.RSV.ApprovalStage.StageType.Manager);
      var skipStageWithoutFinal = currentStage != null ? sline.RSV.ApprovalStages.As(currentStage.Stage).SkipStageWithoutFinalVerificationanorsv.Value : false;
      
      if (needSkip || skipStageWithoutFinal == _obj.SkipStageFinalVerificationanorsv.GetValueOrDefault() && skipStageWithoutFinal)
      {
        return;
      }
      
      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock3(e);
      
      try
      {
        var result = AddApprovers();
        
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
    }

    public override void StartBlock9(Sungero.Docflow.Server.ApprovalSigningAssignmentArguments e)
    {
      if (SkipStage()) { return; }
      
      if (SkipForSecApproval()) { return; }
      
      base.StartBlock9(e);
      
      // создание листа согласования
      CreateApprovalSheet();
      
      var mainDocument = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, mainDocument);
      Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(mainDocument, _obj.OtherGroup);
      
      try
      {
        var result = AddApprovers();
        foreach (var performer in result)
        {
          if (!e.Block.Performers.Contains(performer.Approver))
            e.Block.Performers.Add(performer.Approver);
        }
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> AddApprovers >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
      }
      
      if (e.Block.Performers.Any(p => p != null))
      {
        _obj.SigningTypeChangedanorsv = false;
        _obj.SkipStageanorsv = false;
      }
    }

    public override void EndBlock9(Sungero.Docflow.Server.ApprovalSigningAssignmentEndBlockEventArguments e)
    {
      base.EndBlock9(e);
    }

    public override void StartAssignment5(Sungero.Docflow.IApprovalReworkAssignment assignment, Sungero.Docflow.Server.ApprovalReworkAssignmentArguments e)
    {
      var rsvAssignment = anorsv.ApprovalTask.ApprovalReworkAssignments.As(assignment);
      
      if (_obj.PreApproversAnorsvanorsv.Any())
      {
        var preApproversList = _obj.PreApproversAnorsvanorsv.Select(a => a.Approver).ToList();
        foreach (var approver in preApproversList)
        {
          rsvAssignment.PreApproversanorsv.AddNew().Approver = approver;
        }
      }
      
      base.StartAssignment5(assignment, e);

      var task = anorsv.ApprovalTask.ApprovalTasks.As(_obj);
      var officialDocuments = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      var isPurchaseRequest = sline.CustomModule.PurchaseRequests.Is(officialDocuments);
      if (isPurchaseRequest)
      {
        var assignmentPurchaise = anorsv.ApprovalTask.ApprovalReworkAssignments.As(assignment);
        assignmentPurchaise.ApprovalPurhaseanorsv = isPurchaseRequest;
      }
    }

    public override void StartAssignment28(Sungero.Docflow.IApprovalSendingAssignment assignment, Sungero.Docflow.Server.ApprovalSendingAssignmentArguments e)
    {
      anorsv.ApprovalTask.IApprovalSendingAssignment currentAssignment = ApprovalSendingAssignments.As(assignment);
      
      base.StartAssignment28(assignment, e);

      var ruleStage = anorsv.TaskModule.PublicFunctions.Module.ApprovalTaskGetStage(_obj, Sungero.Docflow.ApprovalStage.StageType.Sending);
      if (ruleStage != null)
      {
        currentAssignment.Stageanorsv = ruleStage.Stage;
        currentAssignment.StageNumber = ruleStage.Number;
      }
    }

    public override void StartAssignment30(Sungero.Docflow.IApprovalSimpleAssignment assignment, Sungero.Docflow.Server.ApprovalSimpleAssignmentArguments e)
    {
      anorsv.ApprovalTask.IApprovalSimpleAssignment currentAssignment = ApprovalSimpleAssignments.As(assignment);
      
      base.StartAssignment30(assignment, e);

      var ruleStage = anorsv.TaskModule.PublicFunctions.Module.ApprovalTaskGetStage(_obj, Sungero.Docflow.ApprovalStage.StageType.SimpleAgr);
      if (ruleStage != null)
      {
        currentAssignment.Stageanorsv = ruleStage.Stage;
        currentAssignment.StageNumber = ruleStage.Number;
      }
      var stage = _obj.ApprovalRule.Stages
        .Where(s => s.Stage != null)
        .Where(s => s.Stage.StageType == Sungero.Docflow.ApprovalStage.StageType.SimpleAgr)
        .FirstOrDefault(s => s.Number == _obj.StageNumber);
      if (stage != null)
      {
        var approvalStage = sline.RSV.ApprovalStages.As(stage.Stage);
        if (approvalStage != null)
        {
          var assignmentPurchaise = anorsv.ApprovalTask.ApprovalSimpleAssignments.As(assignment);
          assignmentPurchaise.ApprovalPurhaseanorsv = approvalStage.ApprovalPurhaseanorsv;
          
        }
      }
    }

    public override void StartAssignment23(Sungero.Docflow.IApprovalRegistrationAssignment assignment, Sungero.Docflow.Server.ApprovalRegistrationAssignmentArguments e)
    {
      anorsv.ApprovalTask.IApprovalRegistrationAssignment currentAssignment = ApprovalRegistrationAssignments.As(assignment);
      
      base.StartAssignment23(assignment, e);

      var ruleStage = anorsv.TaskModule.PublicFunctions.Module.ApprovalTaskGetStage(_obj, Sungero.Docflow.ApprovalStage.StageType.Register);
      if (ruleStage != null)
      {
        currentAssignment.Stageanorsv = ruleStage.Stage;
        currentAssignment.StageNumber = ruleStage.Number;
      }
    }

    public override void StartAssignment20(Sungero.Docflow.IApprovalPrintingAssignment assignment, Sungero.Docflow.Server.ApprovalPrintingAssignmentArguments e)
    {
      anorsv.ApprovalTask.IApprovalPrintingAssignment currentAssignment = ApprovalPrintingAssignments.As(assignment);
      
      base.StartAssignment20(assignment, e);

      var ruleStage = anorsv.TaskModule.PublicFunctions.Module.ApprovalTaskGetStage(_obj, Sungero.Docflow.ApprovalStage.StageType.Print);
      if (ruleStage != null)
      {
        currentAssignment.Stageanorsv = ruleStage.Stage;
        currentAssignment.StageNumber = ruleStage.Number;
      }
    }

    public override void StartAssignment27(Sungero.Docflow.IApprovalCheckReturnAssignment assignment, Sungero.Docflow.Server.ApprovalCheckReturnAssignmentArguments e)
    {
      anorsv.ApprovalTask.IApprovalCheckReturnAssignment currentAssignment = ApprovalCheckReturnAssignments.As(assignment);
      
      base.StartAssignment27(assignment, e);

      var ruleStage = anorsv.TaskModule.PublicFunctions.Module.ApprovalTaskGetStage(_obj, Sungero.Docflow.ApprovalStage.StageType.SimpleAgr);
      if (ruleStage != null)
      {
        currentAssignment.Stageanorsv = ruleStage.Stage;
        currentAssignment.StageNumber = ruleStage.Number;
      }
      
    }

    public override void StartAssignment31(Sungero.Docflow.IApprovalCheckingAssignment assignment, Sungero.Docflow.Server.ApprovalCheckingAssignmentArguments e)
    {
      anorsv.ApprovalTask.IApprovalCheckingAssignment currentAssignment = ApprovalCheckingAssignments.As(assignment);
      
      base.StartAssignment31(assignment, e);

      var ruleStage = anorsv.TaskModule.PublicFunctions.Module.ApprovalTaskGetStage(_obj, Sungero.Docflow.ApprovalStage.StageType.SimpleAgr);
      
      if (ruleStage != null)
      {
        currentAssignment.Stageanorsv = ruleStage.Stage;
        currentAssignment.StageNumber = ruleStage.Number;
      }
      var stage = _obj.ApprovalRule.Stages
        .Where(s => s.Stage != null)
        .Where(s => s.Stage.StageType == Sungero.Docflow.ApprovalStage.StageType.SimpleAgr)
        .FirstOrDefault(s => s.Number == _obj.StageNumber);
      if (stage != null)
      {
        var approvalStage = sline.RSV.ApprovalStages.As(stage.Stage);
        if (approvalStage != null)
        {
          var assignmentPurchaise = anorsv.ApprovalTask.ApprovalCheckingAssignments.As(assignment);
          assignmentPurchaise.ApprovalPurhaseanorsv = approvalStage.ApprovalPurhaseanorsv;
          
        }
      }
    }

    public override void StartNotice33(Sungero.Docflow.IApprovalSimpleNotification notice, Sungero.Docflow.Server.ApprovalSimpleNotificationArguments e)
    {
      var currentNotice = ApprovalSimpleNotifications.As(notice);
      
      base.StartNotice33(notice, e);
      
      var ruleStage = anorsv.TaskModule.PublicFunctions.Module.ApprovalTaskGetStage(_obj, Sungero.Docflow.ApprovalStage.StageType.Notice);
      if (ruleStage != null)
      {
        currentNotice.Stageanorsv = ruleStage.Stage;
        currentNotice.StageNumberanorsv = ruleStage.Number;
      }
      
      //anorsv.TaskModule.PublicFunctions.Module.BCHPSendMail(_obj);
    }

    public override void Script11Execute()
    {
      bool decisionResult = base.Decision32Result();
      
      base.Script11Execute();
      
      if (decisionResult)
      {
        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();

        if (anorsv.AnorsvSubstitution.SubstitutionRequestBases.Is(document))
        {
          if (document.InternalApprovalState != anorsv.AnorsvSubstitution.SubstitutionRequestBase.InternalApprovalState.Approved &&
              document.InternalApprovalState != anorsv.AnorsvSubstitution.SubstitutionRequestBase.InternalApprovalState.Signed)
            document.InternalApprovalState = anorsv.AnorsvSubstitution.SubstitutionRequestBase.InternalApprovalState.Approved;
        }
        
        if (sline.RSV.Orders.Is(document))
        {
          if (document.InternalApprovalState != anorsv.AnorsvSubstitution.SubstitutionRequestBase.InternalApprovalState.Approved &&
              document.InternalApprovalState != anorsv.AnorsvSubstitution.SubstitutionRequestBase.InternalApprovalState.Signed)
            document.InternalApprovalState = anorsv.AnorsvSubstitution.SubstitutionRequestBase.InternalApprovalState.Approved;
          if (document.LifeCycleState != Sungero.Docflow.OfficialDocument.LifeCycleState.Active)
            document.LifeCycleState = Sungero.Docflow.OfficialDocument.LifeCycleState.Active;
        }
      }
    }
    
    public override bool Decision32Result()
    {
      
      bool res = base.Decision32Result();
      
      if (res)
      {
        try
        {
          var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var currentState = document.InternalApprovalState;

          // Не меняем статус подписанного документа.
          if (currentState == Sungero.Docflow.OfficialDocument.InternalApprovalState.Signed)
          {
            Logger.DebugFormat(" >>> SOFTLINE >>> UpdateApprovalState: Task {0}, document {1} already signed.", _obj.Id, document.Id);
            return res;
          }
          
          // Не меняем статус рассмотренного документа.
          if (currentState == Sungero.Docflow.Memo.InternalApprovalState.Reviewed)
          {
            Logger.DebugFormat(" >>> SOFTLINE >>> UpdateApprovalState: Task {0}, document {1} already reviewed.", _obj.Id, document.Id);
            return res;
          }

          Logger.DebugFormat(" >>> SOFTLINE >>> UpdateApprovalState: Task {3}, document {0}, {1} -> {2}", document.Id, currentState, "Approved", _obj.Id);
          
          if (sline.CustomModule.PlanNeedsLines.Is(document))
          {
            if (document.InternalApprovalState != sline.CustomModule.PlanNeedsLine.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.CustomModule.PlanNeedsLine.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.CustomModule.PlanNeedsLine.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.CustomModule.MinutesPurchases.Is(document))
          {
            if (document.InternalApprovalState != sline.CustomModule.MinutesPurchase.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.CustomModule.MinutesPurchase.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.CustomModule.MinutesPurchase.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.CustomModule.ContractChanges.Is(document))
          {
            if (document.InternalApprovalState != sline.CustomModule.ContractChange.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.CustomModule.ContractChange.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.CustomModule.ContractChange.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.CustomModule.PurchaseRequests.Is(document))
          {
            if (document.InternalApprovalState != sline.CustomModule.PurchaseRequest.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.CustomModule.PurchaseRequest.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.CustomModule.PurchaseRequest.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.Addendums.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.Addendum.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.Addendum.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.Addendum.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.CompanyDirectives.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.CompanyDirective.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.CompanyDirective.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.CompanyDirective.InternalApprovalState.Approved;
            
            return res;
          }
          if (anorsv.Contracts.Contracts.Is(document))
          {
            if (document.InternalApprovalState != anorsv.Contracts.Contract.InternalApprovalState.Approved &&
                document.InternalApprovalState != anorsv.Contracts.Contract.InternalApprovalState.Signed)
              document.InternalApprovalState = anorsv.Contracts.Contract.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.ContractStatements.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.ContractStatement.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.ContractStatement.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.ContractStatement.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.CounterpartyDocuments.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.CounterpartyDocument.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.CounterpartyDocument.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.CounterpartyDocument.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.IncomingInvoices.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.IncomingInvoice.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.IncomingInvoice.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.IncomingInvoice.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.IncomingLetters.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.IncomingLetter.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.IncomingLetter.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.IncomingLetter.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.IncomingTaxInvoices.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.IncomingTaxInvoice.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.IncomingTaxInvoice.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.IncomingTaxInvoice.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.Memos.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.Memo.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.Memo.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.Memo.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.Orders.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.Order.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.Order.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.Order.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.OutgoingLetters.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.OutgoingLetter.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.OutgoingLetter.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.OutgoingLetter.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.OutgoingTaxInvoices.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.OutgoingTaxInvoice.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.OutgoingTaxInvoice.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.OutgoingTaxInvoice.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.PowerOfAttorneys.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.PowerOfAttorney.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.PowerOfAttorney.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.PowerOfAttorney.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.SimpleDocuments.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.SimpleDocument.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.SimpleDocument.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.SimpleDocument.InternalApprovalState.Approved;
            
            return res;
          }
          //if (sline.RSV.SupAgreements.Is(document))
          //{
          //    if (document.InternalApprovalState != sline.RSV.SupAgreement.InternalApprovalState.Approved &&
          //        document.InternalApprovalState != sline.RSV.SupAgreement.InternalApprovalState.Signed)
          //        document.InternalApprovalState = sline.RSV.SupAgreement.InternalApprovalState.Approved;
          if (anorsv.Contracts.SupAgreements.Is(document))
          {
            if (document.InternalApprovalState != anorsv.Contracts.SupAgreement.InternalApprovalState.Approved &&
                document.InternalApprovalState != anorsv.Contracts.SupAgreement.InternalApprovalState.Signed)
              document.InternalApprovalState = anorsv.Contracts.SupAgreement.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.UniversalTransferDocuments.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.UniversalTransferDocument.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.UniversalTransferDocument.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.UniversalTransferDocument.InternalApprovalState.Approved;
            
            return res;
          }
          if (sline.RSV.Waybills.Is(document))
          {
            if (document.InternalApprovalState != sline.RSV.Waybill.InternalApprovalState.Approved &&
                document.InternalApprovalState != sline.RSV.Waybill.InternalApprovalState.Signed)
              document.InternalApprovalState = sline.RSV.Waybill.InternalApprovalState.Approved;
            
            return res;
          }
          Logger.DebugFormat(" >>> SOFTLINE >>> UpdateApprovalState >>> No change! >>> Document type is {0} ", document.GetType().Name);
        }
        catch(Exception exc)
        {
          Logger.ErrorFormat(" >>> SOFTLINE >>> UpdateApprovalState >>> Error {0} {1} {2}", exc.Message, Environment.NewLine, exc.StackTrace);
        }
        
      }
      
      return res;
    }

    public override void CompleteAssignment27(Sungero.Docflow.IApprovalCheckReturnAssignment assignment, Sungero.Docflow.Server.ApprovalCheckReturnAssignmentArguments e)
    {
      base.CompleteAssignment27(assignment, e);
      
      CheckNextStage();
      
      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment31(Sungero.Docflow.IApprovalCheckingAssignment assignment, Sungero.Docflow.Server.ApprovalCheckingAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalCheckingAssignment.Result.ForRework)
      {
        SetSkipStage();
      }
      
      if (assignment.Result == Sungero.Docflow.ApprovalCheckingAssignment.Result.Accept)
      {
        _obj.StageNumSkipsanorsv = 0;
        
        var completedBy = sline.RSV.Employees.As(assignment.CompletedBy);
        var performer = sline.RSV.Employees.As(assignment.Performer);
        if (completedBy != null && performer != null && !string.IsNullOrWhiteSpace(performer.PersonnelNumber) && performer.PersonnelNumber.ToLower() == "dzd")
        {
          var doc = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var purchaseRequest = sline.CustomModule.PurchaseRequests.As(doc);
          if (purchaseRequest != null)
          {
            purchaseRequest.EmployeeDZD = completedBy;
            purchaseRequest.Save();
          }
        }
        
        CheckNextStage();
      }
      
      base.CompleteAssignment31(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
      
      var contract = _obj.ContractGroupanorsv.Contracts.FirstOrDefault();
      var technicalSpecification = _obj.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.FirstOrDefault();
      
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      if (contractAttGroupAvailable && contract != null && technicalSpecification != null)
        _obj.Typicalanorsv = contract.IsStandard.Value && technicalSpecification.IsStandard.Value;
      
      if (contractAttGroupAvailable && contract != null && technicalSpecification == null)
        _obj.Typicalanorsv = contract.IsStandard.Value;
    }

    public override void CompleteAssignment30(Sungero.Docflow.IApprovalSimpleAssignment assignment, Sungero.Docflow.Server.ApprovalSimpleAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalSimpleAssignment.Result.Complete)
      {
        _obj.StageNumSkipsanorsv = 0;
        
        var completedBy = sline.RSV.Employees.As(assignment.CompletedBy);
        var performer = sline.RSV.Employees.As(assignment.Performer);
        if (completedBy != null && performer != null && !string.IsNullOrWhiteSpace(performer.PersonnelNumber) && performer.PersonnelNumber.ToLower() == "dzd")
        {
          var doc = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var purchaseRequest = sline.CustomModule.PurchaseRequests.As(doc);
          if (purchaseRequest != null)
          {
            purchaseRequest.EmployeeDZD = completedBy;
            purchaseRequest.Save();
          }
        }
        
        CheckNextStage();
      }
      
      base.CompleteAssignment30(assignment, e);
      
      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment28(Sungero.Docflow.IApprovalSendingAssignment assignment, Sungero.Docflow.Server.ApprovalSendingAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalExecutionAssignment.Result.ForRevision)
      {
        SetSkipStage();
      }
      else
      {
        _obj.StageNumSkipsanorsv = 0;
        CheckNextStage();
      }
      
      base.CompleteAssignment28(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment37(Sungero.Docflow.IApprovalExecutionAssignment assignment, Sungero.Docflow.Server.ApprovalExecutionAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalExecutionAssignment.Result.ForRevision)
      {
        SetSkipStage();
      }
      else
      {
        _obj.StageNumSkipsanorsv = 0;
        CheckNextStage();
      }
      
      base.CompleteAssignment37(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      //anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);

    }

    public override void CompleteAssignment36(Sungero.Docflow.IApprovalReviewAssignment assignment, Sungero.Docflow.Server.ApprovalReviewAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalReviewAssignment.Result.ForRework)
      {
        SetSkipStage();
      }
      else
      {
        _obj.StageNumSkipsanorsv = 0;
        CheckNextStage();
      }
      
      base.CompleteAssignment36(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      //anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment9(Sungero.Docflow.IApprovalSigningAssignment assignment, Sungero.Docflow.Server.ApprovalSigningAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalSigningAssignment.Result.ForRevision)
      {
        SetSkipStage();
      }
      else
      {
        if (assignment.Result == Sungero.Docflow.ApprovalSigningAssignment.Result.Sign)
        {
          _obj.StageNumSkipsanorsv = 0;
          
          try
          {
            var doc = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
            Functions.ApprovalTask.ConvertToPdf(_obj, doc);
          }
          catch(Exception exc)
          {
            Logger.ErrorFormat(" >>> SOFTLINE >>> ConvertToPdf >>> ERROR >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
          }
        }
      }
      
      base.CompleteAssignment9(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
      
      _obj.SigningTypeChangedanorsv = ApprovalSigningAssignments.As(assignment).SigningTypeChangedanorsv ?? false;
      _obj.SkipStageanorsv = _obj.SigningTypeChangedanorsv;
      
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      if (_obj.SigningTypeChangedanorsv.HasValue && _obj.SigningTypeChangedanorsv.Value)
      {
        if (sline.RSV.Orders.Is(document))
        {
          var order = sline.RSV.Orders.As(document);
          order.SignESanorsv = sline.RSV.Order.SignESanorsv.SignOnPaper;
          order.SignESsline = false;
          order.Save();
        }
        else if (sline.RSV.CompanyDirectives.Is(document))
        {
          var companyDirective = sline.RSV.CompanyDirectives.As(document);
          companyDirective.SignESanorsv = sline.RSV.CompanyDirective.SignESanorsv.SignOnPaper;
          companyDirective.SignESsline = false;
          companyDirective.Save();
        }
        else if (sline.RSV.OutgoingLetters.Is(document))
        {
          var outgoingLetter = sline.RSV.OutgoingLetters.As(document);
          outgoingLetter.SignESanorsv = sline.RSV.OutgoingLetter.SignESanorsv.SignOnPaper;
          outgoingLetter.SignESsline = false;
          outgoingLetter.Save();
        }
      }
    }

    public override void CompleteAssignment20(Sungero.Docflow.IApprovalPrintingAssignment assignment, Sungero.Docflow.Server.ApprovalPrintingAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalPrintingAssignment.Result.ForRevision)
      {
        SetSkipStage();
      }
      else
      {
        CheckNextStage();
        _obj.StageNumSkipsanorsv = 0;
      }
      
      base.CompleteAssignment20(assignment, e);
      
      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment23(Sungero.Docflow.IApprovalRegistrationAssignment assignment, Sungero.Docflow.Server.ApprovalRegistrationAssignmentArguments e)
    {
      base.CompleteAssignment23(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment6(Sungero.Docflow.IApprovalAssignment assignment, Sungero.Docflow.Server.ApprovalAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalRegistrationAssignment.Result.ForRevision)
      {
        SetSkipStage();
      }
      else
      {
        CheckNextStage();
        _obj.StageNumSkipsanorsv = 0;
      }
      
      base.CompleteAssignment6(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public override void CompleteAssignment5(Sungero.Docflow.IApprovalReworkAssignment assignment, Sungero.Docflow.Server.ApprovalReworkAssignmentArguments e)
    {
      base.CompleteAssignment5(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
      
      var contract = _obj.ContractGroupanorsv.Contracts.FirstOrDefault();
      var technicalSpecification = _obj.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.FirstOrDefault();
      
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      if (contractAttGroupAvailable && contract != null && technicalSpecification != null)
        _obj.Typicalanorsv = contract.IsStandard.Value && technicalSpecification.IsStandard.Value;
      
      if (contractAttGroupAvailable && contract != null && technicalSpecification == null)
        _obj.Typicalanorsv = contract.IsStandard.Value;
    }

    public override void CompleteAssignment3(Sungero.Docflow.IApprovalManagerAssignment assignment, Sungero.Docflow.Server.ApprovalManagerAssignmentArguments e)
    {
      if (assignment.Result == Sungero.Docflow.ApprovalManagerAssignment.Result.ForRevision)
      {
        SetSkipStage();
      }
      else
      {
        CheckNextStage();
        _obj.StageNumSkipsanorsv = 0;
      }
      
      base.CompleteAssignment3(assignment, e);

      // Дополнительно прекратим все дочерние подзадачи
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }
  }

}