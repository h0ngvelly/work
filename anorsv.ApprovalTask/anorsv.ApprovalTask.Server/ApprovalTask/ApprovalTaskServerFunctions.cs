﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalTask;
using anorsv.ApprovalTask;
using Sungero.Workflow;
using Sungero.Docflow;
using Sungero.Docflow.ApprovalStage;
using ATRightsType = Sungero.Docflow.ApprovalTaskRevokedDocumentsRights;
using System.Reflection;

namespace anorsv.ApprovalTask.Server
{
  partial class ApprovalTaskFunctions
  {

    /// <summary>
    /// Проверить для согласуемого документа обязательные связанные документы,
    /// Если обязательные связанные документы есть - вложить их в задачу,
    /// Если обязательные связанные документы отсутствуют - вернуть сообщение об ошибке
    /// </summary>
    [Public]
    public string CheckRequiredRelatedDocuments()
    {
      var message = string.Empty;
      var approvalDocument = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      
      // Обязательные связанные документы для ЛНА
      if (approvalDocument != null && anorsv.DocflowModule.LNAs.Is(approvalDocument))
      {
        // ЛНА по общей деятельности - обязателен Приказ о введении в действие ЛНА, связанный как Вводит в действие
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        if (lnaGenActivitiesKind != null && approvalDocument.DocumentKind.Equals(lnaGenActivitiesKind))
        {
          var requiredDocs = anorsv.ApprovalTask.Functions.ApprovalTask.GetRequiredRelatedForLNAGenActivities(_obj);
          if (!requiredDocs.Any())
          {
            message = anorsv.ApprovalTask.ApprovalTasks.Resources.ApprovalTaskLNAWithoutOrderError;
          }
          else
          {
            foreach (var requiredDoc in requiredDocs)
            {
              if (!_obj.OrderGroupanorsv.All.Contains(requiredDoc))
                _obj.OrderGroupanorsv.All.Add(requiredDoc);
            }
          }
        }
      }
      
      return message;
    }
    
    /// <summary>
    /// Список Приказов о введении в действие ЛНА по общей деятельности организации
    /// Связанных с ЛНА связью Вводит в действие
    /// </summary>
    public List<sline.RSV.IOrder> GetRequiredRelatedForLNAGenActivities()
    {
      var orderList = new List<sline.RSV.IOrder>();
      var approvalDocument = _obj.DocumentGroup.OfficialDocuments.First();
      var lna = anorsv.DocflowModule.LNAs.As(approvalDocument);
      
      // Приказ о введении в действие ЛНА по общей деятельности организации cвязанный с ЛНА связью Вводит в действие
      var orderImplLNAKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);

      if (orderImplLNAKind != null)
      {
        var allOrderGeneralActivity = sline.RSV.Orders.GetAll()
          .Where(o => lna.Relations.GetRelated(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).Contains(o))
          .Where(o => o.DocumentKind.Equals(orderImplLNAKind))
          .ToList();
        orderList.AddRange(allOrderGeneralActivity);
      }
      
      return orderList;
    }

    /// <summary>
    /// Получить всех обязательных сотрудников процесса согласования.
    /// </summary>
    /// <param name="stages">Список этапов согласования.</param>
    /// <returns>Обязательные сотрудники.</returns>
    public override List<Sungero.Company.IEmployee> GetAllRequiredApprovers(List<Sungero.Docflow.Structures.Module.DefinedApprovalBaseStageLite> stages)
    {
      var approversStages = stages
        .Where(s => s.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers)
        .Where(s => ApprovalStages.Is(s.StageBase))
        .Select(s => ApprovalStages.As(s.StageBase))
        .ToList();
      
      var recipients = new List<Sungero.CoreEntities.IRecipient>();
      foreach (var stage in approversStages)
      {
        // Сотрудники/группы.
        if (stage.Recipients.Any())
          recipients.AddRange(stage.Recipients
                              .Where(rec => rec.Recipient != null)
                              .Select(rec => rec.Recipient)
                              .ToList());
        
        // Роли согласования.
        if (stage.ApprovalRoles.Any())
          recipients.AddRange(stage.ApprovalRoles
                              .Where(r => r.ApprovalRole != null && r.ApprovalRole.Type != Sungero.Docflow.ApprovalRoleBase.Type.Approvers)
                              .Select(r => anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRoleBase.GetRolePerformer(anorsv.AnorsvMainSolution.ApprovalRoleBases.As(r.ApprovalRole), _obj))
                              .Where(r => r != null)
                              .ToList());
      }

      var performers = Sungero.Company.PublicFunctions.Module.GetEmployeesFromRecipients(recipients).Distinct().ToList();
      var assignments = ApprovalAssignments.GetAll()
        .Where(a => Equals(a.Task, _obj) && Equals(a.TaskStartId, _obj.StartId))
        .ToList();
      
      // Поиск обязательных.
      foreach (var assignment in assignments)
      {
        if (!_obj.AddApproversExpanded.Any(x => Equals(x.Approver, assignment.Performer)))
        {
          performers.Add(Sungero.Company.Employees.As(assignment.Performer));
          performers = performers.Distinct().ToList();
        }
      }

      var start = assignments.Count + 1;
      while (start > assignments.Count)
      {
        start = assignments.Count;
        var delete = new List<IAssignment>();
        foreach (var assignment in assignments)
        {
          if (assignment.ForwardedTo == null)
            continue;
          
          if (performers.Contains(assignment.Performer))
          {
            performers.AddRange(assignment.ForwardedTo.Select(u => Sungero.Company.Employees.As(u)));
            performers = performers.Distinct().ToList();
            delete.Add(assignment);
          }
        }
        assignments.RemoveAll(a => delete.Contains(a));
      }

      return performers;
    }
    
    /// <summary>
    /// 
    /// </summary>
    [Public]
    public DateTime? GetDeadlineFromTask()
    {
      var stages = Sungero.Docflow.Server.ApprovalTaskFunctions.GetBaseStages(_obj).BaseStages;
      return base.GetExpectedDate(null, stages);
    }
    
    #region Управление вложениями
    
    /// <summary>
    /// Синхронизация вложений с учётом связей основного документа.
    /// </summary>
    /// <param name="task"></param>
    [Public, Remote(IsPure=false)]
    public void RefreshAttachments()
    {
      RefreshAttachmentsInternal();
    }
    
    [Public]
    public void RefreshAttachmentsInternal()
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshMemoGroup(document, _obj.MemoGroupanorsv);
        anorsv.TaskModule.PublicFunctions.Module.RefreshContractGroup(document, _obj.ContractGroupanorsv);
        anorsv.TaskModule.PublicFunctions.Module.RefreshTechnicalSpecificationGroup(document, _obj.TechnicalSpecificationGroupanorsv);
        anorsv.TaskModule.PublicFunctions.Module.RefreshOrderGroup(document, _obj.OrderGroupanorsv);
      }
    }

    #endregion
    
    #region Права

    /// Установить права на вложения.
    /// </summary>
    /// <param name="recipient">Получатель прав.</param>
    /// <param name="accessRightsType">Тип прав.</param>
    /// <param name="withRestrict">Удалить предыдущие права.</param>
    public override void SetAccessRightsForAttachments(IRecipient recipient, Guid accessRightsType, bool withRestrict)
    {
      if (withRestrict && Equals(recipient, _obj.Author))
        Functions.ApprovalTask.SaveTaskInitiatorAccessRights(_obj);
      
      var documents = new List<Sungero.Domain.Shared.IEntity>();
      documents.AddRange(_obj.DocumentGroup.All);
      documents.AddRange(_obj.AddendaGroup.All);
      documents.AddRange(_obj.ContractGroupanorsv.All);
      documents.AddRange(_obj.TechnicalSpecificationGroupanorsv.All);
      documents.AddRange(_obj.MemoGroupanorsv.All);
      documents.AddRange(_obj.OrderGroupanorsv.All);
      
      foreach (var document in documents)
      {
        if (withRestrict)
          document.AccessRights.RevokeAll(recipient);
        
        base.GrantAccessRightsOnDocument(Sungero.Docflow.OfficialDocuments.As(document), recipient, accessRightsType);
      }
    }
    
    /// <summary>
    /// Вернуть права инициатору после прекращения задачи.
    /// </summary>
    public override void GrantAccessRightsForAttachmentsToInitiatorOnAbort()
    {
      Logger.Debug("Start GrantAccessRightsForAttachmentsToInitiatorOnAbort");
      
      var stageRightsType = DefaultAccessRightsTypes.Change;

      var documents = new List<Sungero.Domain.Shared.IEntity>();
      documents.AddRange(_obj.DocumentGroup.All);
      documents.AddRange(_obj.AddendaGroup.All);
      documents.AddRange(_obj.ContractGroupanorsv.All);
      documents.AddRange(_obj.TechnicalSpecificationGroupanorsv.All);
      documents.AddRange(_obj.MemoGroupanorsv.All);
      documents.AddRange(_obj.OrderGroupanorsv.All);

      foreach (var document in documents)
      {
        var rightsType = document.AccessRights.CanUpdate(_obj.Author)
          || _obj.RevokedDocumentsRights
          .Any(r => r.DocumentId == document.Id
               && r.RightType != Sungero.Docflow.ApprovalTaskRevokedDocumentsRights.RightType.Read)
          ? stageRightsType
          : DefaultAccessRightsTypes.Read;
        GrantAccessRightsOnDocument(Sungero.Docflow.OfficialDocuments.As(document), _obj.Author, rightsType);
        document.Save();
      }
      
      
      Logger.Debug("Done GrantAccessRightsForAttachmentsToInitiatorOnAbort");
    }
    
    /// <summary>
    /// Ограничить права инициатора на вложения при старте.
    /// </summary>
    public override void RestrictAccessRightsForAttachmentsToInitiatorOnStart()
    {
      if (_obj.ApprovalRule.NeedRestrictInitiatorRights == true)
        Functions.ApprovalTask.SetAccessRightsForAttachments(_obj, _obj.Author, DefaultAccessRightsTypes.Read, true);
    }
    
    /// <summary>
    /// Ограничить права на вложения ответственного за доработку.
    /// </summary>
    /// <param name="performer">Ответственный за доработку.</param>
    public override void RestrictAccessRightsForAttachmentsToReworkPerformer(IRecipient performer)
    {
      if (_obj.ApprovalRule.NeedRestrictInitiatorRights == true)
        Functions.ApprovalTask.SetAccessRightsForAttachments(_obj, performer, DefaultAccessRightsTypes.Read, true);
    }
    
    /// <summary>
    /// Сохранить права инициатора задачи перед отбором.
    /// </summary>
    public override void SaveTaskInitiatorAccessRights()
    {
      var documents = new List<Sungero.Domain.Shared.IEntity>();
      documents.AddRange(_obj.DocumentGroup.All);
      documents.AddRange(_obj.AddendaGroup.All);
      documents.AddRange(_obj.ContractGroupanorsv.All);
      documents.AddRange(_obj.TechnicalSpecificationGroupanorsv.All);
      documents.AddRange(_obj.MemoGroupanorsv.All);
      documents.AddRange(_obj.OrderGroupanorsv.All);
      
      foreach (var document in documents)
      {
        Enumeration? rightsType = null;
        if (document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.FullAccess, _obj.Author))
          rightsType = Sungero.Docflow.ApprovalTaskRevokedDocumentsRights.RightType.FullAccess;
        else if (document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Change, _obj.Author))
          rightsType = Sungero.Docflow.ApprovalTaskRevokedDocumentsRights.RightType.Edit;
        else if (document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Read, _obj.Author))
          rightsType = Sungero.Docflow.ApprovalTaskRevokedDocumentsRights.RightType.Read;
        
        if (rightsType != null)
        {
          Logger.DebugFormat("SaveTaskInitiatorAccessRights: Check Access Rights DocumentId = {0} , RightType: {1}", document.Id, rightsType.ToString());

          var revokedRights = _obj.RevokedDocumentsRights.SingleOrDefault(r => r.DocumentId == document.Id);
          
          if (revokedRights != null &&
              (revokedRights.RightType == rightsType ||
               revokedRights.RightType == Sungero.Docflow.ApprovalTaskRevokedDocumentsRights.RightType.FullAccess ||
               revokedRights.RightType == Sungero.Docflow.ApprovalTaskRevokedDocumentsRights.RightType.Edit &&
               rightsType == Sungero.Docflow.ApprovalTaskRevokedDocumentsRights.RightType.Read))
          {
            Logger.DebugFormat("SaveTaskInitiatorAccessRights: Already Have Access Rights DocumentId = {0} , RightType: {1}", document.Id, revokedRights.RightType.ToString());
            continue;
          }

          Logger.DebugFormat("SaveTaskInitiatorAccessRights: Save Access Rights DocumentId = {0} , RightType: {1}", document.Id, rightsType.ToString());
          if (revokedRights == null)
            revokedRights = _obj.RevokedDocumentsRights.AddNew();
          
          revokedRights.DocumentId = document.Id;
          revokedRights.RightType = rightsType;
        }
      }
    }
    
    /// <summary>
    /// Ограничить права на документы у исполнителя задания.
    /// </summary>
    /// <param name="assignment">Задание.</param>
    public override void RestrictAccessRightsForAssignmentPerformer(IAssignment assignment)
    {
      Functions.ApprovalTask.SaveTaskInitiatorAccessRights(_obj);
      base.RestrictAccessRightsForAssignmentPerformer(assignment);
    }

    /// <summary>
    /// Выдать права на вложения, не выше прав инициатора задачи.
    /// </summary>
    /// <param name="recipients">Исполнители.</param>
    public override void GrantAccessRightsForAttachments(List<IRecipient> recipients)
    {
      Logger.Debug("Start GrantAccessRightsForAttachments");
      var stage = _obj.ApprovalRule.Stages
        .FirstOrDefault(s => s.Number == _obj.StageNumber);
      
      var stageRightsType = DefaultAccessRightsTypes.Change;
      
      if (stage != null)
      {
        var stageLite = Sungero.Docflow.Structures.Module.DefinedApprovalStageLite.Create(stage.Stage, stage.Number, stage.StageType);
        var collapsedStages = GetCollapsedStages(_obj, stageLite);
        var namesCollapsedStages = string.Join(", ", collapsedStages.Select(s => s.StageType.ToString()).ToList());
        
        Logger.DebugFormat("GetCollapsedStages: {0}", namesCollapsedStages);
        
        if (collapsedStages.All(s => s.Stage.RightType == Sungero.Docflow.ApprovalStage.RightType.Read))
        {
          stageRightsType = DefaultAccessRightsTypes.Read;
          Logger.Debug("StageRightsType = Read");
        }
        
        if (collapsedStages.Any(s => s.Stage.RightType == Sungero.Docflow.ApprovalStage.RightType.FullAccess))
        {
          stageRightsType = DefaultAccessRightsTypes.FullAccess;
          Logger.Debug("StageRightsType = FullAccess");
        }
      }
      
      Logger.DebugFormat("StageRightsTypeGuid = {0}", stageRightsType.ToString());
      
      foreach (var recipient in recipients)
      {
        Functions.ApprovalTask.SetAccessRightsForAttachments(_obj, recipient, stageRightsType, true);
      }
      
      Logger.Debug("Done GrantAccessRightsForAttachments");
    }

    #endregion
    
    public void ConvertToPdf(Sungero.Docflow.IOfficialDocument document)
    {
      var signHtml = Sungero.Docflow.PublicFunctions.Module.GetSignatureMarkAsHtml(document, document.LastVersion.Id);
      GeneratePublicBodyWithSignatureMark(document, document.LastVersion.Id, signHtml);
    }
    
    public void GeneratePublicBodyWithSignatureMark(Sungero.Docflow.IOfficialDocument document, int versionId, string signatureMark)
    {
      var version = document.Versions.SingleOrDefault(v => v.Id == versionId);
      if (version == null)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> ERROR in GeneratePublicBodyWithSignatureMark >>> Document Id - '{0}' has no versions with versionId = '{1}'.", document.Id.ToString(), versionId.ToString());
        return;
      }
      
      Logger.DebugFormat(" >>> SOFTLINE >>> Start generate public body: document application - {0}, version application - {1}.", document.AssociatedApplication, version.BodyAssociatedApplication);
      
      System.IO.Stream pdfDocumentStream = null;
      using (var inputStream = new System.IO.MemoryStream())
      {
        version.Body.Read().CopyTo(inputStream);
        try
        {
          var pdfConverter = new Sungero.AsposeExtensions.Converter();
          var extension = version.BodyAssociatedApplication.Extension;
          pdfDocumentStream = pdfConverter.GeneratePdf(inputStream, extension);
          var htmlStampString = signatureMark;
          pdfDocumentStream = pdfConverter.AddSignatureMark(pdfDocumentStream, extension, htmlStampString, Sungero.Docflow.Resources.SignatureMarkAnchorSymbol,
                                                            Sungero.Docflow.Constants.Module.SearchablePagesLimit);
        }
        catch (Exception e)
        {
          if (e is Sungero.AsposeExtensions.PdfConvertException)
            Logger.Error(" >>> SOFTLINE >>> ERROR in GeneratePublicBodyWithSignatureMark >>> " + Sungero.Docflow.Resources.PdfConvertErrorFormat(document.Id), e.InnerException);
          else
            Logger.Error(string.Format(" >>> SOFTLINE >>> ERROR in GeneratePublicBodyWithSignatureMark >>> {0} {1}", Sungero.Docflow.Resources.PdfConvertErrorFormat(document.Id), e.Message));
          
          Logger.ErrorFormat(" >>> SOFTLINE >>> ERROR in GeneratePublicBodyWithSignatureMark: {0}.", Sungero.Docflow.Resources.DocumentBodyNeedsRepair);
          return;
        }
      }

      version.PublicBody.Write(pdfDocumentStream);
      version.AssociatedApplication = Sungero.Content.AssociatedApplications.GetByExtension("pdf");
      pdfDocumentStream.Close();
      
      Logger.DebugFormat(" >>> SOFTLINE >>> GeneratePublicBodyWithSignatureMark >>> Generate completed. Saving...");
      
      try
      {
        document.Save();
      }
      catch (Sungero.Domain.Shared.Exceptions.RepeatedLockException e)
      {
        Logger.Error(" >>> SOFTLINE >>> ERROR in GeneratePublicBodyWithSignatureMark >>> " + e.Message);
      }
      catch (Exception e)
      {
        Logger.Error(" >>> SOFTLINE >>> ERROR in GeneratePublicBodyWithSignatureMark >>> " + e.Message);
      }

      Logger.Debug(" >>> SOFTLINE >>> GeneratePublicBodyWithSignatureMark >>> Completed!");
    }
    
    [Remote(IsPure = true)]
    public bool SLHasSignatureForApprovalSheetReport()
    {
      var setting = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(null);
      var showNotApproveSign = setting != null ? setting.ShowNotApproveSign == true : false;
      
      return Signatures.Get(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault()).Any(s => (showNotApproveSign || s.SignatureType != SignatureType.NotEndorsing) && s.IsExternal != true);
    }
  }
}
