using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalTask;

namespace anorsv.ApprovalTask
{
  partial class ApprovalTaskPreApproversAnorsvanorsvApproverPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PreApproversAnorsvanorsvApproverFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(c => c.Status == Sungero.CoreEntities.DatabookEntry.Status.Active);
      
      // Отфильтровать всех пользователей.
      query = query.Where(x => x.Sid != Sungero.Domain.Shared.SystemRoleSid.AllUsers);
      
      // Отфильтровать служебные роли.
      return (IQueryable<T>)Sungero.RecordManagement.PublicFunctions.Module.ObserversFiltering(query);
    }
  }

  partial class ApprovalTaskAddApproversApproverSearchPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> AddApproversApproverSearchDialogFiltering(IQueryable<T> query, Sungero.Domain.PropertySearchDialogFilteringEventArgs e)
    {
      query = base.AddApproversApproverSearchDialogFiltering(query, e);
      return query;
    }
  }

  partial class ApprovalTaskAddApproversApproverPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> AddApproversApproverFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.AddApproversApproverFiltering(query, e);
      return query;
    }
  }

  partial class ApprovalTaskServerHandlers
  {

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      base.Saving(e);
      
      // Почистим группы вложений от дублей
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.AddendaGroup);
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.OtherGroup);
    }

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);
      
      var preApproversList = _obj.PreApproversAnorsvanorsv.Select(a => a.Approver).ToList();
      
      // Заполнение Предсогласующих из Тематики СЗ
      var memo = sline.RSV.Memos.As(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      if (memo != null && memo.MemoTopicanorsv != null)
        preApproversList.AddRange(memo.MemoTopicanorsv.PreApprovers.Select(a => a.Approver));
      
      // Заполнение Предсогласующих из Тематики общей
      var officialDocument = anorsv.OfficialDocument.OfficialDocuments.As(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      if (officialDocument != null && officialDocument.Topicanorsv != null)
        preApproversList.AddRange(officialDocument.Topicanorsv.PreApprovers.Select(a => a.Approver));
      
      _obj.PreApproversAnorsvanorsv.Clear();
      foreach (var preApprover in preApproversList.Distinct())
        _obj.PreApproversAnorsvanorsv.AddNew().Approver = preApprover;
      
      bool contractAttGroupAvailable = Convert.ToBoolean(
        Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var contract = _obj.ContractGroupanorsv.Contracts.FirstOrDefault();
      var technicalSpecification = _obj.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.FirstOrDefault();
      
      if (contractAttGroupAvailable && contract != null && technicalSpecification != null)
        _obj.Typicalanorsv = contract.IsStandard.Value && technicalSpecification.IsStandard.Value;
      
      if (contractAttGroupAvailable && contract != null && technicalSpecification == null)
        _obj.Typicalanorsv = contract.IsStandard.Value;
    }

    public override void BeforeAbort(Sungero.Workflow.Server.BeforeAbortEventArgs e)
    {
      base.BeforeAbort(e);
      
      _obj.SkipStageanorsv = false;
      _obj.SigningTypeChangedanorsv = false;
      _obj.AgreeWithoutFinalVerificationanorsv = false;
      _obj.SkipStageFinalVerificationanorsv = false;
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      _obj.StageNumSkipsanorsv = 0;
      _obj.CreateApprovalSheetsanorsv = false;
      _obj.SkipStageanorsv = false;
      _obj.SigningTypeChangedanorsv = false;
      _obj.AgreeWithoutFinalVerificationanorsv = false;
      _obj.SkipStageFinalVerificationanorsv = false;
      _obj.Typicalanorsv = false;
      
      // Если задача создана из карточки ЛНА по общей деятельности, в поле примечание писать автотекст
      // "Прошу согласовать проект ЛНА по общей деятельности и проект Приказа о введении его в действие"
      if(CallContext.CalledDirectlyFrom(DocflowModule.LNAs.Info))
      {
        var idLNA = CallContext.GetCallerEntityId(DocflowModule.LNAs.Info);
        var LNA = DocflowModule.LNAs.Get(idLNA);
        var LNAGeneralActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        if (LNA.DocumentKind.Equals(LNAGeneralActivitiesKind))
        {
          _obj.ActiveText = anorsv.ApprovalTask.ApprovalTasks.Resources.ActiveText_for_LNA;
          // Вложить Приказ, вводящий в действие ЛНА
          var order = LNA.Orderanorsv;
          if (order != null)
          {
            bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
            if (orderAttGroupAvailable == true)
              _obj.OrderGroupanorsv.Orders.Add(order);
            else
              _obj.OtherGroup.All.Add(order);
          }
        }
      }
    }
    

    public override void BeforeRestart(Sungero.Workflow.Server.BeforeRestartEventArgs e)
    {
      _obj.StageNumSkipsanorsv = 0;
      _obj.CreateApprovalSheetsanorsv = false;
      
      base.BeforeRestart(e);
    }

    public override void BeforeStart(Sungero.Workflow.Server.BeforeStartEventArgs e)
    {
      // Стартовать, если вложены обязательные связанные документы
      var requiredRelatedDocumentsMessage = anorsv.ApprovalTask.Functions.ApprovalTask.CheckRequiredRelatedDocuments(_obj);
      
      if (!string.IsNullOrEmpty(requiredRelatedDocumentsMessage))
      {
        e.AddError(requiredRelatedDocumentsMessage);
        return;
      }
      
      _obj.StageNumSkipsanorsv = 0;
      _obj.CreateApprovalSheetsanorsv = false;
      
      base.BeforeStart(e);
    }
    
  }
  
  partial class ApprovalTaskObserversObserverPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> ObserversObserverFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.ObserversObserverFiltering(query, e);
      return query;
    }
  }
  
}