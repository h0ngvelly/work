﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalSendingAssignment;

namespace anorsv.ApprovalTask
{
  partial class ApprovalSendingAssignmentServerHandlers
  {

    public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
    {
      string lockedDocumentMessage = anorsv.TaskModule.PublicFunctions.Module.AttachedDocumentIsLocked(_obj.AllAttachments.ToList());
      if (!string.IsNullOrEmpty(lockedDocumentMessage))
      {
        e.AddError(lockedDocumentMessage);
        return;
      }
      
      base.BeforeComplete(e);
    }
  }

}
