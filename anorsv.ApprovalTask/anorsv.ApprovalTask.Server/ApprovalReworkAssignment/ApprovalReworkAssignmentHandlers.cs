﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalReworkAssignment;

namespace anorsv.ApprovalTask
{
  partial class ApprovalReworkAssignmentServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);      
    }

    public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
    {
      /*var preApproversList = _obj.PreApproversanorsv.Select(a => a.Approver).ToList();      
      // Заполнение Предсогласующих из Тематики СЗ
      var memo = sline.RSV.Memos.As(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      if (memo != null && memo.MemoTopicanorsv != null)
        preApproversList.AddRange(memo.MemoTopicanorsv.PreApprovers.Select(a => a.Approver));
      // Заполнение Предсогласующих из Тематики общей
      var officialDocument = anorsv.OfficialDocument.OfficialDocuments.As(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      if (officialDocument != null && officialDocument.Topicanorsv != null)
        preApproversList.AddRange(officialDocument.Topicanorsv.PreApprovers.Select(a => a.Approver));
      
      _obj.PreApproversanorsv.Clear();
      foreach (var preApprover in preApproversList.Distinct())
        _obj.PreApproversanorsv.AddNew().Approver = preApprover;*/
      
      string lockedDocumentMessage = anorsv.TaskModule.PublicFunctions.Module.AttachedDocumentIsLocked(_obj.AllAttachments.ToList());
      if (!string.IsNullOrEmpty(lockedDocumentMessage))
      {
        e.AddError(lockedDocumentMessage);
        return;
      }
      else
        base.BeforeComplete(e);
      
    }
  }

}
