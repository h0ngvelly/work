﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalAssignment;

namespace anorsv.ApprovalTask
{
  partial class ApprovalAssignmentServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      _obj.SkipStageWithoutFinalVerificationanorsv = false;
    }

    public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
    {
      string lockedDocumentMessage = anorsv.TaskModule.PublicFunctions.Module.AttachedDocumentIsLocked(_obj.AllAttachments.ToList());
      if (!string.IsNullOrEmpty(lockedDocumentMessage))
      {
        e.AddError(lockedDocumentMessage);
        return;
      }
      
      base.BeforeComplete(e);
    }
  }

}
