using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalManagerAssignment;

namespace anorsv.ApprovalTask
{

  partial class ApprovalManagerAssignmentSharedHandlers
  {

    public override void AddApproversChanged(Sungero.Domain.Shared.CollectionPropertyChangedEventArgs e)
    {
      base.AddApproversChanged(e);
      _obj.State.Controls.Control.Refresh();
    }
  }

}