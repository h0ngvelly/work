using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalTask;

namespace anorsv.ApprovalTask
{


  partial class ApprovalTaskSharedHandlers
  {

    public virtual void PreApproversAnorsvanorsvChanged(Sungero.Domain.Shared.CollectionPropertyChangedEventArgs e)
    {
      _obj.State.Controls.Control.Refresh();
    }    

    public override void ApprovalRuleChanged(Sungero.Docflow.Shared.ApprovalTaskApprovalRuleChangedEventArgs e)
    {
      base.ApprovalRuleChanged(e);
      
      // Добавить во вложения обязательные связанные документы
      var requiredRelatedDocumentsMessage = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.CheckRequiredRelatedDocuments(_obj);
      
    }
    
  }

}