﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalTask;

namespace anorsv.ApprovalTask.Shared
{
  partial class ApprovalTaskFunctions
  {

    //    /// <summary>
    //    /// Определить номер следующего этапа.
    //    /// </summary>
    //    /// <param name="task">Задача.</param>
    //    /// <returns>Номер следующего этапа.</returns>
    //    [Public]
    //    public static int? GetNextStageNumber(IApprovalTask task)
    //    {
    //      return Functions.ApprovalTask.GetNextStageNumber(task);
    //    }
    
    public void UpdateReglamentApprovers()
    {
      
      var result = _obj.ReqApprovers.Select(x => x.Approver).ToList();
      var preAgreementRes = new List<Sungero.CoreEntities.IRecipient>();
      var preAgreementResTask = _obj.PreApproversAnorsvanorsv.Select(p => p.Approver).ToList();
      
      try
      {
        Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentApprovers >>> start");
        var stages = _obj.ApprovalRule.Stages;
        
        #region Предсогласующие
        
        var stage0 = stages.Where(x => x.Stage != null
                                  && x.Stage.ApprovalRoles.Any()
                                  && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.PreApprovers))
          .FirstOrDefault();
        
        if (stage0 != null)
        {
          Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentPreApprovers >>> stage0 != null");
          var roles0 = stage0.Stage.ApprovalRoles;
          var bRole0 = roles0.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.PreApprovers).FirstOrDefault().ApprovalRole;
          var role0 = sline.CustomModule.MyApprovalRoleses.As(bRole0);
          
          if (role0 != null && !_obj.State.Properties.PreApproversAnorsvanorsv.IsChanged)
          {
            Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentPreApprovers >>> role0 != null");
            preAgreementRes.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role0, _obj));
            
            _obj.PreApproversAnorsvanorsv.Clear();
            foreach (var res in preAgreementRes.Distinct())
            {
              var m = _obj.PreApproversAnorsvanorsv.AddNew();
              m.Approver = res;
              Logger.Debug("PreApprovers: " + res.Name);
            }
            
            
          }
        }
        #endregion
        
        var stage = stages.Where(x => x.Stage != null
                                 && x.Stage.ApprovalRoles.Any()
                                 && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagers))
          .FirstOrDefault();
        
        if (stage != null)
        {
          Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentApprovers >>> stage != null");
          var roles = stage.Stage.ApprovalRoles;
          var bRole = roles.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagers).FirstOrDefault().ApprovalRole;
          var role = sline.CustomModule.MyApprovalRoleses.As(bRole);
          
          if (role != null)
          {
            Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentApprovers >>> role != null");
            result.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, _obj));
            
            _obj.ReqApprovers.Clear();
            foreach (var res in result.Distinct())
            {
              var n = _obj.ReqApprovers.AddNew();
              n.Approver = res;
              Logger.Debug("AppManagers: " + res.Name);
            }
          }
        }
        
        // ------
        var stage2 = stages.Where(x => x.Stage != null
                                  && x.Stage.ApprovalRoles.Any()
                                  && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.ManagLessFirst))
          .FirstOrDefault();
        
        if (stage2 != null)
        {
          Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentApproversLessFirst >>> stage2 != null");
          var roles2 = stage2.Stage.ApprovalRoles;
          var bRole2 = roles2.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.ManagLessFirst).FirstOrDefault().ApprovalRole;
          var role2 = sline.CustomModule.MyApprovalRoleses.As(bRole2);
          
          if (role2 != null)
          {
            Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentApproversLessFirst >>> role2 != null");
            result.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role2, _obj));
            
            _obj.ReqApprovers.Clear();
            foreach (var res in result.Distinct())
            {
              var n = _obj.ReqApprovers.AddNew();
              n.Approver = res;
              Logger.Debug("ManagLessFirst: " + res.Name);
            }
          }
        }
        
        // ------
        var stage3 = stages.Where(x => x.Stage != null
                                  && x.Stage.ApprovalRoles.Any()
                                  && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.SubstApprov))
          .FirstOrDefault();
        
        if (stage3 != null)
        {
          Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentSubstApprovers >>> stage3 != null");
          var roles3 = stage3.Stage.ApprovalRoles;
          var bRole3 = roles3.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.SubstApprov).FirstOrDefault().ApprovalRole;
          var role3 = sline.CustomModule.MyApprovalRoleses.As(bRole3);
          
          if (role3 != null)
          {
            Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentSubstApprovers >>> role3 != null");
            result.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role3, _obj));
            
            _obj.ReqApprovers.Clear();
            foreach (var res in result.Distinct())
            {
              var n = _obj.ReqApprovers.AddNew();
              n.Approver = res;
              Logger.Debug("SubstApprov: " + res.Name);
            }
          }
        }
        // ------
        var stage4 = stages.Where(x => x.Stage != null
                                  && x.Stage.ApprovalRoles.Any()
                                  && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.GDDeputy))
          .FirstOrDefault();
        
        if (stage4 != null)
        {
          Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentGDDeputy >>> stage4 != null");
          var roles4 = stage4.Stage.ApprovalRoles;
          var bRole4 = roles4.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.GDDeputy).FirstOrDefault().ApprovalRole;
          var role4 = sline.CustomModule.MyApprovalRoleses.As(bRole4);
          
          if (role4 != null)
          {
            Logger.Debug(" >>> SOFTLINE >>> UpdateReglamentGDDeputy >>> role4 != null");
            result.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role4, _obj));
            
            _obj.ReqApprovers.Clear();
            foreach (var res in result.Distinct())
            {
              var n = _obj.ReqApprovers.AddNew();
              n.Approver = res;
              Logger.Debug("GDDeputy: " + res.Name);
            }
          }
        }
        
        #region Автор документа
        
        var stageDocAuthor = stages.Where(x => x.Stage != null
                                          && x.Stage.ApprovalRoles.Any()
                                          && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.DocAuthor))
          .FirstOrDefault();
        
        if (stageDocAuthor != null)
        {
          var roles = stageDocAuthor.Stage.ApprovalRoles;
          var bRole = roles.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.DocAuthor).FirstOrDefault().ApprovalRole;
          var role = sline.CustomModule.MyApprovalRoleses.As(bRole);
          
          if (role != null)
          {
            result.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, _obj));
            _obj.ReqApprovers.Clear();
            foreach (var res in result.Distinct())
            {
              var n = _obj.ReqApprovers.AddNew();
              n.Approver = res;
              Logger.Debug("DocAuthor: " + res.Name);
            }
          }
        }
        #endregion
        
        #region Ответственный за ЦФО
        
        var stageResponsibleFRC = stages.Where(x => x.Stage != null
                                               && x.Stage.ApprovalRoles.Any()
                                               && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.ResponsibleFRC))
          .FirstOrDefault();
        
        if (stageResponsibleFRC != null)
        {
          var roles = stageResponsibleFRC.Stage.ApprovalRoles;
          var bRole = roles.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.ResponsibleFRC).FirstOrDefault().ApprovalRole;
          var role = sline.CustomModule.MyApprovalRoleses.As(bRole);
          
          if (role != null)
          {
            result.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, _obj));
            _obj.ReqApprovers.Clear();
            foreach (var res in result.Distinct())
            {
              var n = _obj.ReqApprovers.AddNew();
              n.Approver = res;
              Logger.Debug("ResponsibleFRC: " + res.Name);
            }
          }
        }
        
        #endregion

        #region Непосредственный руководитель по иерархии
        
        var stageAppManager = stages.Where(x => x.Stage != null
                                           && x.Stage.ApprovalRoles.Any()
                                           && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManager))
          .FirstOrDefault();
        
        if (stageAppManager != null)
        {
          var roles = stageAppManager.Stage.ApprovalRoles;
          var bRole = roles.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManager).FirstOrDefault().ApprovalRole;
          var role = sline.CustomModule.MyApprovalRoleses.As(bRole);
          
          if (role != null)
          {
            result.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, _obj));
            _obj.ReqApprovers.Clear();
            foreach (var res in result.Distinct())
            {
              var n = _obj.ReqApprovers.AddNew();
              n.Approver = res;
              Logger.Debug("AppManager: " + res.Name);
            }
          }
        }
        
        #endregion
        
        #region Непосредственный руководитель по иерархии до ЗГД
        
        var stageAppManagerLessGDDeputy = stages.Where(x => x.Stage != null
                                                       && x.Stage.ApprovalRoles.Any()
                                                       && x.Stage.ApprovalRoles.Any(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagerGDDep))
          .FirstOrDefault();
        
        if (stageAppManagerLessGDDeputy != null)
        {
          var roles = stageAppManager.Stage.ApprovalRoles;
          var bRole = roles.Where(c => c.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagerGDDep).FirstOrDefault().ApprovalRole;
          var role = sline.CustomModule.MyApprovalRoleses.As(bRole);
          
          if (role != null)
          {
            result.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, _obj));
            _obj.ReqApprovers.Clear();
            foreach (var res in result.Distinct())
            {
              var n = _obj.ReqApprovers.AddNew();
              n.Approver = res;
              Logger.Debug("AppManagerGDDep: " + res.Name);
            }
          }
        }
        
        if (preAgreementRes.Any())
        {
          foreach (var preAgreement in preAgreementRes)
            result.Remove(preAgreement);//
        }
        
        #endregion
        
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> ERROR on UpdateReglamentApprovers >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
        if (!_obj.ReqApprovers.Select(x => x.Approver).ToList().SequenceEqual(result))
        {
          _obj.ReqApprovers.Clear();
          foreach (var res in result.Distinct())
          {
            var n = _obj.ReqApprovers.AddNew();
            n.Approver = res;
          }
        }
      }
    }
  }
}