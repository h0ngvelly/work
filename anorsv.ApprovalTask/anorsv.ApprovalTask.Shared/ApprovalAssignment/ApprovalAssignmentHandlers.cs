﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalAssignment;

namespace anorsv.ApprovalTask
{
  partial class ApprovalAssignmentSharedHandlers
  {

    public override void AddresseeChanged(Sungero.Docflow.Shared.ApprovalAssignmentAddresseeChangedEventArgs e)
    {
      base.AddresseeChanged(e);
      
      _obj.State.Controls.Control.Refresh();
    }
  }


}