﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask.ApprovalReworkAssignment;

namespace anorsv.ApprovalTask
{
  partial class ApprovalReworkAssignmentSharedHandlers
  {

    public override void SignatoryChanged(Sungero.Docflow.Shared.ApprovalReworkAssignmentSignatoryChangedEventArgs e)
    {
      base.SignatoryChanged(e);
      _obj.State.Controls.Control.Refresh();
    }

    public override void ForwardPerformerChanged(Sungero.Docflow.Shared.ApprovalReworkAssignmentForwardPerformerChangedEventArgs e)
    {
      base.ForwardPerformerChanged(e);
      _obj.State.Controls.Control.Refresh();
    }

  }
}