﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.TaskModule.Structures.Module
{

  /// <summary>
  /// Информация о состоянии подзадач у задания.
  /// </summary>
  [Public]
  partial class AssignmentSubtasksSummary
  {
    /// <summary>
    /// Количество подзадач в фазе исполнения.
    /// </summary>
    public int InWork { get; set; }
    
    /// <summary>
    /// Количество позадач в фазе приёмки.
    /// </summary>
    public int InAccept { get; set; }
    
    /// <summary>
    /// Общее количество подазадач.
    /// </summary>
    public int totalCount { get; set; }
  }
  
  /// <summary>
  /// Информация об этапе
  /// </summary>
  [Public]
  partial class DefinedApprovalStageLite
  {
    public sline.RSV.IApprovalStage Stage { get; set; }
    
    public int? Number { get; set; }
    
    public Sungero.Core.Enumeration? StageType { get; set; }
  }
}