﻿using System;
using Sungero.Core;

namespace anorsv.TaskModule.Constants
{
  public static class Module
  {

    [Public]
    public static class LeadingAssignmentDialog
    {
      [Public]
      public static class LeadingAssigmnentDialogResult
      {
        [Public]
        public static readonly Guid AcceptAndComplete = Guid.Parse("5FBC5FBD-32D3-4E61-92A1-33AF69730ACD");
        
        [Public]
        public static readonly Guid Accept = Guid.Parse("4476E7DB-2ABF-412B-817E-13DB73C3AA0D");
        
        [Public]
        public static readonly Guid Open = Guid.Parse("7EA068E7-A7B7-4F3F-878E-88E1293CDD43");
        
        [Public]
        public static readonly Guid Cancel = Guid.Parse("14D14692-EA37-4710-8C28-28129BE8202A");
      }
    }
    
    [Public]
    public static class AssignmentCompleteDialog
    {
      [Public]
      public static class AssignmentCompleteDialogResult
      {
        [Public]
        public static readonly Guid Show = Guid.Parse("653C39FD-76C2-4F7A-9139-BAE25757684F");
        
        [Public]
        public static readonly Guid Abort = Guid.Parse("9E93DC3A-F83D-4235-8330-60EE4D8B28BB");

        [Public]
        public static readonly Guid StayInWork = Guid.Parse("3802F0B4-680C-4C18-AA73-DD16A3086848");

        [Public]
        public static readonly Guid Cancel = Guid.Parse("14D14692-EA37-4710-8C28-28129BE8202A");
        
        [Public]
        public static readonly Guid AcceptSubtasksAndAbortSubtasks = Guid.Parse("1E100CD2-F12F-4F6A-A712-694C528B57D3");
        
        [Public]
        public static readonly Guid AcceptSubtasks = Guid.Parse("52977FAE-786D-439A-B179-98164D046BF4");
        
        [Public]
        public static readonly Guid NotNeeded = Guid.Parse("71EAFBB7-54BD-498B-8D62-9586637E9FD6");
      }
    }
    
    [Public]
    public static class TaskMainGroup
    {
      [Public]
      public static readonly Guid FreeApprovalTask = Guid.Parse("cd77936e-884e-44bb-b869-9a60f9f5f2b4");
      
      [Public]
      public static readonly Guid AcquaintanceTask = Guid.Parse("19c1e8c9-e896-4d93-a1e8-4e22b932c1ce");

    }
  }
}