﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.ApprovalTask;

namespace anorsv.TaskModule.Shared
{
  public class ModuleFunctions
  {
    [Public]
    public static string AttachedDocumentIsLocked(System.Collections.Generic.IList<Sungero.Domain.Shared.IEntity> attachments)
    {
      var documents = new List<Sungero.Content.IElectronicDocument>();
      var lockedByOtherDocNames = new List<string>();
      var lockedByOwnerDocNames = new List<string>();
      var currentOwnerName = string.Empty;
      var docNames = string.Empty;
      var resultMessage = string.Empty;
      
      foreach (var attachment in attachments)
      {
        if (Sungero.Content.ElectronicDocuments.Is(attachment))
        {
          var attachmentDoc = Sungero.Content.ElectronicDocuments.As(attachment);
          if (!documents.Contains(attachmentDoc))
            documents.Add(attachmentDoc);
        }
      }
      
      // Проверить на блокировку каждую версию
      foreach (var document in documents)
      {
        var documentName = document.Name;
        foreach (var documentVersion in document.Versions)
        {
          if (Locks.GetLockInfo(documentVersion.Body).IsLocked)
          {
            if (Locks.GetLockInfo(documentVersion.Body).OwnerName == Users.Current.Name)
            {
              lockedByOwnerDocNames.Add(documentName); // documentVersion.DisplayValue
              if (string.IsNullOrEmpty(currentOwnerName))
                currentOwnerName = Locks.GetLockInfo(documentVersion.Body).OwnerName;
            }
            else
            {
              lockedByOtherDocNames.Add(string.Format(anorsv.TaskModule.Resources.AttachedDocumentsLockedByUserTemplate, documentName, Locks.GetLockInfo(documentVersion.Body).OwnerName)); //documentVersion.DisplayValue
            }
          }
        }
      }
      
      if (lockedByOwnerDocNames.Any() && !lockedByOtherDocNames.Any())
      {
        // Если есть только заблокированные текущим пользователем документы
        foreach (var docName in lockedByOwnerDocNames)
        {
          if (string.IsNullOrEmpty(docNames))
            docNames = docName;
          else
            docNames = docNames + Environment.NewLine + docName;
        }
        
        if (lockedByOwnerDocNames.Count() > 1)
          resultMessage = anorsv.TaskModule.Resources.AttachedDocumentsLockedMessage + Environment.NewLine + docNames;
        else
          resultMessage = string.Format(anorsv.TaskModule.Resources.AttachedDocumentIsLockedMessage, docNames);
        
      }
      else if (!lockedByOwnerDocNames.Any() && lockedByOtherDocNames.Any())
      {
        // Если есть только заблокированные другими пользователями документы
        foreach (var docName in lockedByOtherDocNames)
        {
          if (string.IsNullOrEmpty(docNames))
            docNames = docName;
          else
            docNames = docNames + Environment.NewLine + docName;
        }
        
        if (lockedByOtherDocNames.Count() > 1)
          resultMessage = anorsv.TaskModule.Resources.AttachedDocumentsLockedByUserMessage + Environment.NewLine + docNames;
        else
          resultMessage = string.Format(anorsv.TaskModule.Resources.AttachedDocumentIsLockedByUserMessage, docNames, currentOwnerName);
        
      }
      else if (lockedByOwnerDocNames.Any() && lockedByOtherDocNames.Any())
      {
        // Если есть заблокированные и текущим, и другими пользователями документы
        foreach (var docName in lockedByOwnerDocNames)
        {
          var docNameOwner = string.Format(anorsv.TaskModule.Resources.AttachedDocumentsLockedByUserTemplate, docName, currentOwnerName);
          if (string.IsNullOrEmpty(docNames))
            docNames = docNameOwner;
          else
            docNames = docNames + Environment.NewLine + docNameOwner;
        }
        foreach (var docName in lockedByOtherDocNames)
        {
          if (string.IsNullOrEmpty(docNames))
            docNames = docName;
          else
            docNames = docNames + Environment.NewLine + docName;
        }
        
        resultMessage = anorsv.TaskModule.Resources.AttachedDocumentsLockedByUserMessage + Environment.NewLine + docNames;
      }
      
      return resultMessage;
    }
    
    [Public]
    public static bool IsApprovalTaskAssignment(Sungero.Workflow.IAssignmentBase assignment)
    {
      return Sungero.Docflow.ApprovalAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalCheckingAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalCheckReturnAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalExecutionAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalManagerAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalNotifications.Is(assignment)
        || Sungero.Docflow.ApprovalPrintingAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalRegistrationAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalReviewAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalReworkAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalSendingAssignments.Is(assignment)
        || Sungero.Docflow.ApprovalSimpleAssignments.Is(assignment);
    }
    
    /// <summary>
    /// Определяет является ли переданный пользователь исполнителем по заданию/уведомлению или его замещающим.
    /// </summary>
    /// <param name="assignment"></param>
    /// <param name="user"></param>
    /// <returns></returns>
    [Public]
    public static bool IsPerformerOrSubstitute(IAssignmentBase assignment, IUser user)
    {
      return assignment.Performer.Equals(user) || Substitutions.UsersWhoSubstitute(assignment.Performer).Contains(user);
    }
    
    [Public]
    public static bool SubtaskIsAllowedByStageSetting(Sungero.Docflow.IApprovalStage stage)
    {
      sline.RSV.IApprovalStage currentStage = sline.RSV.ApprovalStages.As(stage);
      
      return SubtaskIsAllowedByStageSetting(currentStage);
    }
    
    [Public]
    public static bool SubtaskIsAllowedByStageSetting(sline.RSV.IApprovalStage stage)
    {
      return (stage.SubtasksIsAllowedanorsv.HasValue && stage.SubtasksIsAllowedanorsv.Value) || !stage.SubtasksIsAllowedanorsv.HasValue;
    }
    
    #region Работа с вложениями
    
    /// <summary>
    /// Заменяет содержимое группы вложений в задаче.
    /// </summary>
    /// <param name="group">Группа вложений, в которой нужно сделать изменения.</param>
    /// <param name="entity">Сущность, которую нужно поместить в группу вложений.</param>
    [Public]
    public static void SynchronizeAttachmentsInGroup(Sungero.Workflow.Interfaces.IWorkflowEntityAttachmentGroup group, Sungero.Domain.Shared.IEntity entity)
    {
      if (entity == null)
      {
        if (group.All.Any())
        {
          group.All.Clear();
        }
        
        return;
      }

      if (group.All.Contains(entity))
      {
        return;
      }
      
      if (group.All.Count > 0)
      {
        group.All.Clear();
      }
      
      group.All.Add(entity);
    }
    
    /// <summary>
    /// Синхронизация группы вложений "Приказы".
    /// </summary>
    /// <param name="document">Основной документ.</param>
    /// <param name="group">Группа для которой происходит синхронизация приказа.</param>
    [Public]
    public static void RefreshOrderGroup(Sungero.Docflow.IOfficialDocument document, Sungero.Workflow.Interfaces.IWorkflowEntityAttachmentGroup group)
    {
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      var orderPutIntoAction = document.Relations.GetRelated(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).FirstOrDefault();   
      if (orderAttGroupAvailable && orderPutIntoAction != null)
        anorsv.TaskModule.PublicFunctions.Module.SynchronizeAttachmentsInGroup(group, orderPutIntoAction);
    }
    
    /// <summary>
    /// Синхронизация группы вложений "Служебная записка".
    /// </summary>
    /// <param name="document">Основной документ.</param>
    /// <param name="group">Группа для которой происходит синхронизация служебной записки.</param>
    [Public]
    public static void RefreshMemoGroup(Sungero.Docflow.IOfficialDocument document, Sungero.Workflow.Interfaces.IWorkflowEntityAttachmentGroup group)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));    
      var memo = document.Relations.GetRelated(sline.CustomModule.Resources.MemoForPurchaseRelationName).FirstOrDefault();
      if (contractAttGroupAvailable && memo != null)
        anorsv.TaskModule.PublicFunctions.Module.SynchronizeAttachmentsInGroup(group, memo);
    }
    
    /// <summary>
    /// Синхронизация группы вложений "Проект договора"
    /// </summary>
    /// <param name="document">Основной документ.</param>
    /// <param name="group">Группа для которой происходит синхронизация проекта договора.</param>
    [Public]
    public static void RefreshContractGroup(Sungero.Docflow.IOfficialDocument document, Sungero.Workflow.Interfaces.IWorkflowEntityAttachmentGroup group)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));      
      var contract = document.Relations.GetRelated(sline.CustomModule.Resources.ContractForPurchaseRelationName).FirstOrDefault();
      if (contractAttGroupAvailable && contract != null)
        anorsv.TaskModule.PublicFunctions.Module.SynchronizeAttachmentsInGroup(group, contract);
    }
    
    /// <summary>
    /// Синхронизация группы вложений "Техническое задание"
    /// </summary>
    /// <param name="document">Основной документ.</param>
    /// <param name="group">Группа для которой происходит синхронизация технического задания.</param>
    [Public]
    public static void RefreshTechnicalSpecificationGroup(Sungero.Docflow.IOfficialDocument document, Sungero.Workflow.Interfaces.IWorkflowEntityAttachmentGroup group)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));      
      var technicalSpecification = document.Relations.GetRelated(sline.CustomModule.Resources.TSForPurchaseRelationName).FirstOrDefault();
      if (contractAttGroupAvailable && technicalSpecification != null)
        anorsv.TaskModule.PublicFunctions.Module.SynchronizeAttachmentsInGroup(group, technicalSpecification);
    }
    
    [Public]
    public static void OptimizeAttachementDocumentGroup(Sungero.Workflow.Interfaces.IWorkflowEntityAttachmentGroup group)
    {
      var allDocuments = group.All.Where(e => Sungero.Content.ElectronicDocuments.Is(e));
      var uniqueDocuments = allDocuments.Distinct();
      if (allDocuments.Count() > uniqueDocuments.Count())
      {
        foreach (var document in uniqueDocuments)
        {
          var countInAll = allDocuments.Count(d => d.Equals(document));
          if (countInAll > 1)
          {
            var removeCount = countInAll - 1;
            while (removeCount > 0)
            {
              group.All.Remove(document);
              removeCount--;
            }
          }
        }
      }
    }

    #endregion
    
   }
}