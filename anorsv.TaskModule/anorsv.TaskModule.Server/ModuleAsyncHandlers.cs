using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.FormalizedSubTask;

namespace anorsv.TaskModule.Server
{
  public class ModuleAsyncHandlers
  {

    public virtual void UpdateAssigmentSubject(anorsv.TaskModule.Server.AsyncHandlerInvokeArgs.UpdateAssigmentSubjectInvokeArgs args)
    {
      var assignment = Sungero.Workflow.Assignments.GetAll().Where(asg => asg.Id == args.assigmentId).FirstOrDefault();
      if (assignment != null)
      {
        var assignmentLockInfo = Locks.GetLockInfo(assignment);
        var isForcedLocked = false;
        
        if (!assignmentLockInfo.IsLockedByOther && !assignmentLockInfo.IsLockedByMe)
          isForcedLocked = Locks.TryLock(assignment);
        
        if (isForcedLocked || assignmentLockInfo.IsLockedByMe)
        {
          string prefix = sline.CustomModule.Settings.GetAll().FirstOrDefault().SubtasksPrefix;
          string subject = String.Empty;
          
          if (assignment.Subtasks.Any())
          {
            if (!assignment.Subject.StartsWith(prefix))
            {
              if ((prefix.Length + assignment.Subject.Length) > 245)
              {
                string newSubject = prefix + assignment.Subject.Remove(assignment.Subject.Length - prefix.Length - 1);
                
                if (!assignment.Subject.Equals(newSubject))
                {
                  subject = newSubject;
                  assignment.Subject = newSubject;
                  assignment.Save();
                }
              }
              else
              {
                string newSubject = prefix + assignment.Subject;
                
                if (!assignment.Subject.Equals(newSubject))
                {
                  subject = prefix + assignment.Subject;
                  assignment.Subject = prefix + assignment.Subject;
                  assignment.Save();
                }
              }
            }
          }
          else
          {
            if (assignment.Subject.StartsWith(prefix))
            {
              subject = assignment.Subject.Replace(prefix, string.Empty);
              assignment.Subject = assignment.Subject.Replace(prefix, string.Empty);
              assignment.Save();
            }
          }
          
          if (isForcedLocked)
          {
            Locks.Unlock(assignment);
          }
        }
        else
        {
          args.Retry = true;
          var retryTime = Calendar.Now.AddSeconds(60 + 10 * args.RetryIteration);
          args.NextRetryTime = retryTime;
        }
      }
    }

    public virtual void UpdatingSubtaskNoticeSubject(anorsv.TaskModule.Server.AsyncHandlerInvokeArgs.UpdatingSubtaskNoticeSubjectInvokeArgs args)
    {
      // Формируем тему уведомлений по новому шаблону
      var noticeList = Sungero.Workflow.Notices.GetAll().Where(n => n.Task.Id == args.TaskId).ToList();
      foreach (var notice in noticeList)
      {
        try
        {
          notice.Subject = Sungero.Workflow.Tasks.Get(args.TaskId).Subject;
          notice.Save();
        }
        catch(Exception ex)
        {
          Logger.DebugFormat("UpdatingSubtaskNoticeSubject: notice is locked. TaskID-{0}. {1}", args.TaskId, ex.Message);
          args.Retry = true;
          return;
        }
      }
    }

    public virtual void UpdatingSubtaskJobNumber(anorsv.TaskModule.Server.AsyncHandlerInvokeArgs.UpdatingSubtaskJobNumberInvokeArgs args)
    {
      ITaskExtraParam subtaskExtraParam = anorsv.TaskModule.TaskExtraParams.Get(args.SubtaskParamId);
      if (subtaskExtraParam != null)
      {
        // Если запись есть, блокируем ее и увеличиваем счетчик подзадач
        if (Locks.TryLock(subtaskExtraParam))
        {
          // Формируем имя задания по новому шаблону
          var job = Sungero.Workflow.AssignmentBases.Get(args.JobId);
          if (job != null)
            anorsv.TaskModule.PublicFunctions.Module.UpdateSubtaskJobSubject(job, subtaskExtraParam);
          else
          {
            Logger.DebugFormat("UpdatingSubtaskJobNumber: job was deleted. Job id - {0}.", args.JobId);
            return;
          }
        }
        else
        {
          Logger.DebugFormat("UpdatingSubtaskJobNumber: subtask's TaskExtraParam is locked. Record id - {0}.", args.SubtaskParamId);
          args.Retry = true;
          return;
        }
      }
    }

    public virtual void UpdatingSubtaskNumber(anorsv.TaskModule.Server.AsyncHandlerInvokeArgs.UpdatingSubtaskNumberInvokeArgs args)
    {
      ITaskExtraParam parentTaskExtraParam = anorsv.TaskModule.TaskExtraParams.Get(args.ParentTaskParamId);
      if (parentTaskExtraParam != null)
      {
        // Если запись есть, блокируем ее и увеличиваем счетчик подзадач
        if (Locks.TryLock(parentTaskExtraParam))
        {
          // Формируем имя подзадачи по новому шаблону
          anorsv.FormalizedSubTask.IFormalizedSubTask subtask = anorsv.FormalizedSubTask.FormalizedSubTasks.Get(args.TaskId);
          if (subtask != null)
            anorsv.TaskModule.PublicFunctions.Module.UpdateSubtaskSubject(subtask, parentTaskExtraParam);
          else
          {
            Logger.DebugFormat("UpdatingSubtaskNumber: subtask was deleted. Subtask id - {0}.", args.TaskId);
            return;
          }
        }
        else
        {
          Logger.DebugFormat("UpdatingSubtaskNumber: parent's TaskExtraParam is locked. Record id - {0}.", args.ParentTaskParamId);
          args.Retry = true;
          return;
        }
      }
    }

    public virtual void UpdatingSubtaskJobSubject(anorsv.TaskModule.Server.AsyncHandlerInvokeArgs.UpdatingSubtaskJobSubjectInvokeArgs args)
    {
      
      // Формируем тему задания по новому шаблону
      var job = Sungero.Workflow.AssignmentBases.Get(args.JobId);
      if (job != null)
        try
      {
        job.Subject  = args.JobSubject;
        job.Save();
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("UpdatingSubtaskSubject: job is locked. JobID-{0}. {1}", args.JobId, ex.Message);
        args.Retry = true;
        return;
      }
      else
      {
        Logger.DebugFormat("UpdatingSubtaskJobSubject: job was deleted. JobID-{0}.", args.JobId);
        return;
      }
      
    }

    public virtual void UpdatingSubtaskSubject(anorsv.TaskModule.Server.AsyncHandlerInvokeArgs.UpdatingSubtaskSubjectInvokeArgs args)
    {
      
      // Формируем тему подзадачи по новому шаблону
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = anorsv.FormalizedSubTask.FormalizedSubTasks.Get(args.TaskId);
      if (subtask != null)
        try
      {
        subtask.Subject = args.TaskSubject;
        subtask.Save();
        
        // Изменить тему всех созданных уведомлений
        anorsv.TaskModule.PublicFunctions.Module.UpdateSubtaskNoticeSubject(subtask.Id);
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("UpdatingSubtaskSubject: subtask is locked. SubtaskID-{0}. {1}", args.TaskId, ex.Message);
        args.Retry = true;
        return;
      }
      else
      {
        Logger.DebugFormat("UpdatingSubtaskSubject: subtask was deleted. SubtaskID-{0}.", args.TaskId);
        return;
      }
    }

    public virtual void TaskModuleAcceptingTask(anorsv.TaskModule.Server.AsyncHandlerInvokeArgs.TaskModuleAcceptingTaskInvokeArgs args)
    {
      int taskId = args.TaskId;
      bool succesfull = false;
      ITask task = Tasks.GetAll(t => t.Id == taskId).SingleOrDefault();
      
      if (task != null && task.Status.HasValue && task.Status.Value == Sungero.Workflow.Task.Status.InProcess)
      {
        IFormalizedSubTask formalizedSubtask = FormalizedSubTasks.As(task);
        ISimpleTask simpleTask = SimpleTasks.As(task);
        
        if (formalizedSubtask != null && Functions.Module.FormalizedSubtaskUnderReview(formalizedSubtask))
        {
          IFormalizedSubtaskControlAssignment reviewAssignment =
            FormalizedSubtaskControlAssignments.GetAll(a => a.Task.Equals(task) && a.Status == Sungero.Workflow.ReviewAssignment.Status.InProcess).SingleOrDefault();
          
          if (reviewAssignment != null)
          {
            if (Functions.Module.AcceptingFormalizedSubtask(reviewAssignment))
            {
              succesfull = true;
            }
            else
            {
              args.Retry = true;
            }
          }
          else
          {
            succesfull = true;
          }
          
        }
      }

      if (succesfull)
      {
        Logger.DebugFormat("TaskModule.TaskModuleAcceptTask: Succesfull accepted task with Id = {0}.", taskId);
      }
    }

    public virtual void TaskModuleAbortingTask(anorsv.TaskModule.Server.AsyncHandlerInvokeArgs.TaskModuleAbortingTaskInvokeArgs args)
    {
      int taskId = args.TaskId;
      bool succesfull = false;
      List<Enumeration> taskStatuses = new List<Enumeration> {Sungero.Workflow.Task.Status.InProcess
          , Sungero.Workflow.Task.Status.Suspended
          , Sungero.Workflow.Task.Status.UnderReview};
      Sungero.Workflow.ITask task = Sungero.Workflow.Tasks.GetAll(t => t.Id == taskId).SingleOrDefault();
      
      if (task != null && task.Status.HasValue && taskStatuses.Contains(task.Status.Value))
      {
        if (!Functions.Module.AbortingTask(task))
        {
          args.Retry = true;
        }
      }
      
      if (succesfull)
      {
        Logger.DebugFormat("TaskModule.TaskModuleAbortTask: Succesfull aborted task with Id = {0}.", taskId);
      }
    }

  }
}