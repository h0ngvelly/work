using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.TaskModule.TaskExtraParam;

namespace anorsv.TaskModule
{
  partial class TaskExtraParamServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      _obj.Name = _obj.TaskId.ToString();
      if (_obj.JobId != null)
        _obj.Name = string.Format("{0}-{1}", _obj.Name, _obj.JobId.ToString());
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.SubtaskNumber = 0;
      _obj.JobNumber = 0;
    }
  }

}