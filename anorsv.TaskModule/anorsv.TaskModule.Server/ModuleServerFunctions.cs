﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.FormalizedSubTask;

namespace anorsv.TaskModule.Server
{
  public class ModuleFunctions
  {
    
    #region Формирование наименований задач и заданий с учётом порядка их формирования
    
    /// <summary>
    /// Создать запись для родительского задания
    /// </summary>
    [Public]
    public void CreateTaskExtraParams(Sungero.Workflow.IAssignmentBase parentAssignment)
    {
      anorsv.TaskModule.ITaskExtraParam taskExtraParam = anorsv.TaskModule.TaskExtraParams.Null;
      var findTaskExtraParam = anorsv.TaskModule.TaskExtraParams
        .GetAll()
        .Where(t => t.JobId == parentAssignment.Id && t.TaskId == parentAssignment.Task.Id)
        .FirstOrDefault();
      
      if (findTaskExtraParam == null)
      {
        do
        {
          taskExtraParam = CreateTaskExtraParam(
            parentAssignment.Task.Id,
            parentAssignment.Id,
            Resources.TaskExtraParamsRecordNameTemplateFormat(parentAssignment.Task.Id, parentAssignment.Id));
        }
        while (taskExtraParam == null);
      }
      else
      {
        Logger.DebugFormat("CreateTaskExtraParams: parent job TaskExtraParam is already assist. JobID-{0}, TaskID-{1}.", parentAssignment.Id, parentAssignment.Task.Id);
      }
    }
    
    /// <summary>
    /// Создать запись для формализованной подзадачи
    /// </summary>
    [Public]
    public void CreateTaskExtraParams(anorsv.FormalizedSubTask.IFormalizedSubTask subtask)
    {
      anorsv.TaskModule.ITaskExtraParam taskExtraParam = anorsv.TaskModule.TaskExtraParams.Null;
      var findTaskExtraParam = anorsv.TaskModule.TaskExtraParams.GetAll().Where(t => t.TaskId == subtask.Id).FirstOrDefault();
      if (findTaskExtraParam == null)
      {
        do
        {
          taskExtraParam = CreateTaskExtraParam(subtask.Id, null, subtask.Id.ToString());
        }
        while (taskExtraParam == null);
      }
      else
      {
        Logger.DebugFormat(Resources.TaskExtraParamsAlreadyExistFormat(subtask.Id));
      }
    }
    
    public anorsv.TaskModule.ITaskExtraParam CreateTaskExtraParam(int? taskId, int? jobId, string name)
    {
      anorsv.TaskModule.ITaskExtraParam taskExtraParam = anorsv.TaskModule.TaskExtraParams.Null;
      try
      {
        // Если запись для форм.подзадачи еще нет - создадим новую
        taskExtraParam = anorsv.TaskModule.TaskExtraParams.Create();
        taskExtraParam.TaskId = taskId;
        if (jobId != null)
          taskExtraParam.JobId = jobId;
        taskExtraParam.Name = name;
        taskExtraParam.Save();
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("CreateTaskExtraParams: can not save TaskExtraParam record. TaskID-{0}, JobId-{1}. ExMessage-{2}", taskId, jobId, ex.Message);
        return null;
      }
      return taskExtraParam;
    }
    
    [Public]
    public void UpdateSubtaskSubject(anorsv.FormalizedSubTask.IFormalizedSubTask subtask, ITaskExtraParam parentTaskExtraParam)
    {
      parentTaskExtraParam.SubtaskNumber = parentTaskExtraParam.SubtaskNumber + 1;
      parentTaskExtraParam.Save();
      var subtaskNumber = parentTaskExtraParam.SubtaskNumber;
      Locks.Unlock(parentTaskExtraParam);
      
      // Изменить тему подзадачи
      var subtaskAuthor = anorsv.AnorsvMainSolution.Module.Docflow.PublicFunctions.Module.GeUserNameInGenitive(subtask.ParentAssignment.Performer.Name);
      var subject = Resources.FormalizedSubTaskSubjectTemplateFormat(parentTaskExtraParam.SubtaskNumber, subtaskAuthor);
      try
      {
        subtask.Subject  = subject;
        subtask.Save();
        
        // Изменить тему всех созданных уведомлений
        anorsv.TaskModule.PublicFunctions.Module.UpdateSubtaskNoticeSubject(subtask.Id);
      }
      catch(Exception ex)
      {
        // Если подзадача заблокирована - повторяем попытку в асинхр.обработчике
        Logger.DebugFormat("UpdateSubtaskSubject: subtask is locked. SubtaskID-{0}. {1}", subtask.Id, ex.Message);
        var asyncUpdateSubtaskSubject = AsyncHandlers.UpdatingSubtaskSubject.Create();
        asyncUpdateSubtaskSubject.TaskId = subtask.Id;
        asyncUpdateSubtaskSubject.TaskSubject = subject;
        asyncUpdateSubtaskSubject.ExecuteAsync();
      }
    }
    
    [Public]
    public void UpdateSubtaskNoticeSubject(int taskId)
    {
      var asyncUpdateSubtaskNoticeSubject = AsyncHandlers.UpdatingSubtaskNoticeSubject.Create();
      asyncUpdateSubtaskNoticeSubject.TaskId = taskId;
      asyncUpdateSubtaskNoticeSubject.ExecuteAsync();
    }
    
    [Public]
    public void UpdateSubtaskJobSubject(Sungero.Workflow.IAssignmentBase assignment, ITaskExtraParam parentTaskExtraParam)
    {
      parentTaskExtraParam.JobNumber = parentTaskExtraParam.JobNumber + 1;
      parentTaskExtraParam.Save();
      var assignmentNumber = parentTaskExtraParam.JobNumber;
      Locks.Unlock(parentTaskExtraParam);
      
      // Изменить тему подзадачи
      var subject = string.Empty;
      var subtask = anorsv.FormalizedSubTask.FormalizedSubTasks.Null;
      var mainDoc = Sungero.Docflow.OfficialDocuments.Null;
      var taskAuthorName = anorsv.AnorsvMainSolution.Module.Docflow.PublicFunctions.Module.GeUserNameInGenitive(assignment.Task.Author.Name);
      var taskPerformerName = string.Empty;
      
      var subtaskAssignment = anorsv.FormalizedSubTask.FormalizedSubtaskAssignments.As(assignment);
      var subtaskControlAssignment = anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignments.As(assignment);
      if (subtaskAssignment != null)
      {
        subtask = anorsv.FormalizedSubTask.FormalizedSubTasks.Get(assignment.Task.Id);
        mainDoc = subtask.DocumentGroup.OfficialDocuments.FirstOrDefault();
        subject = Resources.FormalizedSubtaskJobSubjectTemplateFormat(assignmentNumber, taskAuthorName);
      }
      if (subtaskControlAssignment != null)
      {
        subtask = anorsv.FormalizedSubTask.FormalizedSubTasks.Get(assignment.Task.Id);
        mainDoc = subtask.DocumentGroup.OfficialDocuments.FirstOrDefault();
        foreach (var performer in subtask.Performers)
        {
          var performerName = anorsv.AnorsvMainSolution.Module.Docflow.PublicFunctions.Module.GeUserNameInGenitive(performer.Perfomer.Name);
          if (string.IsNullOrEmpty(taskPerformerName))
            taskPerformerName = performerName;
          else
            taskPerformerName = string.Format("{0}, {1}", taskPerformerName, performerName);
        }
        subject = Resources.FormalizedSubtaskControlAssignmentSubjectTemplateFormat(assignmentNumber, taskPerformerName);
      }
      
      // Добавим имя основного документа, если он есть
      if (mainDoc != null)
        subject = string.Format("{0}: {1}", subject, mainDoc.Name);
      
      // Обрежем тему, если превышает 250 симмолов
      if (subject.Length > 250)
        subject = string.Format("{0}...", subject.Substring(0, 240));
      
      try
      {
        assignment.Subject  = subject;
        assignment.Save();
      }
      catch(Exception ex)
      {
        // Если подзадача заблокирована - повторяем попытку в асинхр.обработчике
        Logger.DebugFormat("UpdateSubtaskJobSubject: job is locked. JobID-{0}. {1}", assignment.Id, ex.Message);
        var asyncUpdateSubtaskJobSubject = AsyncHandlers.UpdatingSubtaskJobSubject.Create();
        asyncUpdateSubtaskJobSubject.JobId = assignment.Id;
        asyncUpdateSubtaskJobSubject.JobSubject = subject;
        asyncUpdateSubtaskJobSubject.ExecuteAsync();
      }
    }
    
    #endregion
    
    #region Поиск и анализ состояния задач и заданий
    
    /// <summary>
    /// Определяет наличие активной задачи на рассмотрение.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи на рассмотрение в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public, Remote(IsPure = true)]
    public static bool ActiveAcquaintanceTaskExist(Sungero.Docflow.IOfficialDocument document)
    {
      return ActiveAcquaintanceTaskExistInternal(document);
    }

    /// <summary>
    /// Определяет наличие активной задачи на рассмотрение.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи на рассмотрение в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public]
    public static bool ActiveAcquaintanceTaskExistInternal(Sungero.Docflow.IOfficialDocument document)
    {
      var documentGroupGuid = Constants.Module.TaskMainGroup.AcquaintanceTask;
      var activeTaskExist = false;
      
      Sungero.Core.AccessRights.AllowRead(
        () =>
        {
          activeTaskExist = Sungero.RecordManagement.AcquaintanceTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(
              t => t.AttachmentDetails
              .Any(att => att.AttachmentId == document.Id &&
                   att.GroupId == documentGroupGuid))
            .Any();
        });
      
      return activeTaskExist;
    }
    
    /// <summary>
    /// Определяет наличие активной задачи на рассмотрение.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи на рассмотрение в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public, Remote(IsPure = true)]
    public static bool ActiveReviewTaskExist(Sungero.Docflow.IOfficialDocument document)
    {
      return ActiveReviewTaskExistInternal(document);
    }

    /// <summary>
    /// Определяет наличие активной задачи на рассмотрение.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи на рассмотрение в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public]
    public static bool ActiveReviewTaskExistInternal(Sungero.Docflow.IOfficialDocument document)
    {
      var documentGroupGuid = Sungero.Docflow.PublicConstants.Module.TaskMainGroup.DocumentReviewTask;
      var activeTaskExist = false;
      
      Sungero.Core.AccessRights.AllowRead(
        () =>
        {
          activeTaskExist = Sungero.RecordManagement.DocumentReviewTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(
              t => t.AttachmentDetails
              .Any(att => att.AttachmentId == document.Id &&
                   att.GroupId == documentGroupGuid))
            .Any();
        });
      
      return activeTaskExist;
    }
    
    /// <summary>
    /// Определяет наличие активной задачи по свободному согласованию.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи по свободному согласованию в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public, Remote(IsPure = true)]
    public static bool ActiveFreeApprovalTaskExist(Sungero.Docflow.IOfficialDocument document)
    {
      return ActiveFreeApprovalTaskExistInternal(document);
    }

    /// <summary>
    /// Определяет наличие активной задачи по свободному согласованию.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи по свободному согласованию в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public]
    public static bool ActiveFreeApprovalTaskExistInternal(Sungero.Docflow.IOfficialDocument document)
    {
      var documentGroupGuid = Constants.Module.TaskMainGroup.FreeApprovalTask;
      var activeTaskExist = false;
      
      Sungero.Core.AccessRights.AllowRead(
        () =>
        {
          activeTaskExist = Sungero.Docflow.FreeApprovalTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(
              t => t.AttachmentDetails
              .Any(att => att.AttachmentId == document.Id &&
                   att.GroupId == documentGroupGuid))
            .Any();
        });
      
      return activeTaskExist;
    }
    
    /// <summary>
    /// Определяет наличие активной задачи исполнения поручения.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи исполнения порчений в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public, Remote(IsPure = true)]
    public static bool ActiveActionItemExecutionTaskExist(Sungero.Docflow.IOfficialDocument document)
    {
      return ActiveActionItemExecutionTaskExistInternal(document);
    }
    
    /// <summary>
    /// Определяет наличие активной задачи исполнения поручения.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи исполнения порчений в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public]
    public static bool ActiveActionItemExecutionTaskExistInternal(Sungero.Docflow.IOfficialDocument document)
    {
      var documentGroupGuid = Sungero.Docflow.PublicConstants.Module.TaskMainGroup.ActionItemExecutionTask;
      var activeTaskExist = false;
      
      Sungero.Core.AccessRights.AllowRead(
        () =>
        {
          activeTaskExist = Sungero.RecordManagement.ActionItemExecutionTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(
              t => t.AttachmentDetails
              .Any(att => att.AttachmentId == document.Id &&
                   att.GroupId == documentGroupGuid))
            .Any();
        });
      
      return activeTaskExist;
    }
    
    /// <summary>
    /// Определяет наличие активной задачи согласования по регламенту для документа.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи по согласованию по регламенту в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public, Remote(IsPure = true)]
    public static bool ActiveApprovalTaskExist(Sungero.Docflow.IOfficialDocument document)
    {
      return ActiveApprovalTaskExistInternal(document);
    }
    
    /// <summary>
    /// Определяет наличие активной задачи согласования по регламенту для документа.
    /// </summary>
    /// <param name="document">Документ, для которого осуществляется поиск активных задач.</param>
    /// <returns>Возвращает True, если есть задачи по согласованию по регламенту в статусе "В работе" или "Приостановлено", иначе  возвращает False.</returns>
    [Public]
    public static bool ActiveApprovalTaskExistInternal(Sungero.Docflow.IOfficialDocument document)
    {
      var documentGroupGuid = Sungero.Docflow.PublicConstants.Module.TaskMainGroup.ApprovalTask;
      var activeTaskExist = false;
      
      Sungero.Core.AccessRights.AllowRead(
        () =>
        {
          activeTaskExist = Sungero.Docflow.ApprovalTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(
              t => t.AttachmentDetails
              .Any(att => att.AttachmentId == document.Id &&
                   att.GroupId == documentGroupGuid))
            .Any();
        });
      
      return activeTaskExist;
    }

    [Public, Remote(IsPure = true)]
    public static Enumeration? GetAssignmentStatus(int assignmentId)
    {
      return Sungero.Workflow.AssignmentBases.Get(assignmentId).Status.Value;
    }
    
    [Public, Remote(IsPure = true)]
    public static bool ExistAssignmentSubtaskInWork(int assignmentId)
    {
      return Sungero.Workflow.Tasks.GetAll(t => t.ParentAssignment != null && t.ParentAssignment.Id == assignmentId && t.Status == Sungero.Workflow.Task.Status.InProcess).Any();
    }
    
    [Public, Remote(IsPure = true)]
    public static IQueryable<Sungero.Workflow.ITask> GetAssignmentSubtasksInWork(int assignmentId)
    {
      return Sungero.Workflow.Tasks.GetAll(t => t.ParentAssignment != null && t.ParentAssignment.Id == assignmentId && t.Status == Sungero.Workflow.Task.Status.InProcess);
    }
    
    [Public, Remote(IsPure = true)]
    public static IQueryable<Sungero.Workflow.ITask> GetAssignmentActiveSubtasks(int assignmentId)
    {
      List<Enumeration> allowedSubtaskStatues = new List<Enumeration>() {Sungero.Workflow.Task.Status.InProcess, Sungero.Workflow.Task.Status.UnderReview};
      
      return Sungero
        .Workflow
        .Tasks
        .GetAll(t => t.ParentAssignment.Id == assignmentId && t.Status.HasValue && allowedSubtaskStatues.Contains(t.Status.Value));
    }
    
    [Public, Remote(IsPure = true)]
    public static IQueryable<Sungero.Workflow.ITask> GetAssignmentSubtask(int assignmentId)
    {
      return Sungero.Workflow.Tasks.GetAll(t => t.ParentAssignment != null && t.ParentAssignment.Id == assignmentId);
    }
    
    [Public, Remote(IsPure = true)]
    public static IQueryable<Sungero.Workflow.ITask> GetTaskSubtasks(int taskId, bool withAssignmentsSubtasks, bool withoutCheckingPermissions)
    {
      IQueryable<Sungero.Workflow.ITask> searchedSubtasks = null;
      
      if (withoutCheckingPermissions)
      {
        Sungero.Core.AccessRights.AllowRead(
          () => { searchedSubtasks = GetTaskSubtasks(taskId, withAssignmentsSubtasks); }
         );
      }
      else
      {
        searchedSubtasks = GetTaskSubtasks(taskId, withAssignmentsSubtasks);
      }
      
      return searchedSubtasks;
    }
    
    private static IQueryable<Sungero.Workflow.ITask> GetTaskSubtasks(int taskId, bool withAssignmentsSubtasks)
    {
      List<int> searchedSubtasksList = Sungero.Workflow.Tasks.GetAll(t => t.ParentTask.Id == taskId).Select(t => t.Id).ToList();
      
      if (withAssignmentsSubtasks)
      {
        List<int> searchedAssignmentsList = Sungero.Workflow.Assignments.GetAll(a => a.Task.Id == taskId).Select(a => a.Id).ToList();
        List<int> searchedAssignmentsSubtasksList = Sungero.Workflow.Tasks.GetAll(t => searchedAssignmentsList.Contains(t.ParentAssignment.Id) && !searchedSubtasksList.Contains(t.Id)).Select(t => t.Id).ToList();
        searchedSubtasksList.AddRange(searchedAssignmentsSubtasksList);
      }
      
      return Sungero.Workflow.Tasks.GetAll(t => searchedSubtasksList.Contains(t.Id));
    }
    
    #endregion
    
    #region Диалоги выполнения заданий
    
    [Public, Remote(IsPure = true)]
    public static Guid GetAssignmentCompleteDialogType(int assignmentId)
    {
      Guid result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.NotNeeded;
      
      Sungero.Core.AccessRights.AllowRead(
        () => {
          int subtasksInWork = 0;
          int subtasksUnderReview = 0;
          IQueryable<Sungero.Workflow.ITask> subtasks = GetAssignmentActiveSubtasks(assignmentId);
          
          foreach (Sungero.Workflow.ITask subtask in subtasks)
          {
            if (TaskInWork(subtask))
            {
              subtasksInWork++;
            }
            else if (TaskUnderReview(subtask))
            {
              subtasksUnderReview++;
            }
            else
            {
              subtasksInWork++;
            }
          }
          
          if (subtasksInWork == 0 && subtasksUnderReview > 0)
          {
            result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.AcceptSubtasks;
          }
          else if (subtasksInWork > 0 && subtasksUnderReview == 0)
          {
            result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Abort;
          }
          else if (subtasksInWork > 0 && subtasksUnderReview > 0)
          {
            result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.AcceptSubtasksAndAbortSubtasks;
          }
        }
       );
      
      return result;
    }
    
    [Public, Remote(IsPure=false)]
    public static void AssignmentSubtasksAcceptAndAbort(int parentAssignmentId, bool possibleAsyncOperaton)
    {
      AssignmentSubtasksAcceptAndAbortInternal(parentAssignmentId, possibleAsyncOperaton);
    }
    
    [Public]
    public static void AssignmentSubtasksAcceptAndAbortInternal(int parentAssignmentId, bool possibleAsyncOperaton)
    {
      Logger.DebugFormat("TaskModule.Server.AssignmentChildTasksAcceptAndAbortInternal: AssigmentId = {0}.", parentAssignmentId);
      
      // Найдём все подзадачи
      List<Enumeration> taskStatuses = new List<Enumeration> {Sungero.Workflow.Task.Status.InProcess
          , Sungero.Workflow.Task.Status.Suspended
          , Sungero.Workflow.Task.Status.UnderReview};
      List<ITask> subtasks = Tasks.GetAll(task => task.ParentAssignment.Id == parentAssignmentId && taskStatuses.Contains(task.Status.Value)).ToList();
      List<ITask> nextLevelsubtasks = new List<ITask>();

      // Найдём все подзадачи с глубиной вложенности 2 и более
      do
      {
        nextLevelsubtasks.Clear();
        foreach(ITask parentTask in subtasks)
          foreach(IAssignment assignment in Assignments.GetAll(a => a.Task.Equals(parentTask)).ToList())
            foreach(ITask subtask in assignment.Subtasks)
              if (taskStatuses.Contains(subtask.Status.Value) && !subtasks.Contains(subtask))
                nextLevelsubtasks.Add(subtask);
        subtasks.AddRange(nextLevelsubtasks);
      } while(nextLevelsubtasks.Count() > 0);
      
      // Перевернём список задач, что-бы обабатывать задачи в порядке от нибольшей глубины вложенности к наименьшей.
      subtasks.Reverse();
      
      foreach (ITask subtask in subtasks)
      {
        bool isSimpleTask = SimpleTasks.Is(subtask);
        bool isFormalizedSubtask = FormalizedSubTasks.Is(subtask);
        
        if (isSimpleTask)
        {
          FinalizingSimpleTask(SimpleTasks.As(subtask), possibleAsyncOperaton);
        }
        else if (isFormalizedSubtask)
        {
          FinalizingFormalizedSubtask(FormalizedSubTasks.As(subtask), possibleAsyncOperaton);
        }
        else
        {
          AbortingTask(subtask);
        }
      }
    }
    
    [Public, Remote(IsPure = true)]
    public static bool TaskInWork(Sungero.Workflow.ITask task)
    {
      if (Sungero.Workflow.SimpleTasks.Is(task))
      {
        return SimpleTaskInWork(Sungero.Workflow.SimpleTasks.As(task));
      }
      else if (anorsv.FormalizedSubTask.FormalizedSubTasks.Is(task))
      {
        return FormalizedSubtaskInWork(anorsv.FormalizedSubTask.FormalizedSubTasks.As(task));
      }
      else
      {
        return task.Status == Sungero.Workflow.Task.Status.InProcess;
      }
    }
    
    [Public, Remote(IsPure = true)]
    public static bool SimpleTaskInWork(Sungero.Workflow.ISimpleTask task)
    {
      return task.Status == Sungero.Workflow.SimpleTask.Status.InProcess;
    }
    
    [Public]
    public static bool SimpleUnderReview(Sungero.Workflow.ISimpleTask task)
    {
      return task.Status == Sungero.Workflow.SimpleTask.Status.UnderReview;
    }

    [Public]
    public static bool AcceptingSimpleTask(IReviewAssignment assignment)
    {
      bool succesfull = false;
      var lockInfo = Locks.GetLockInfo(assignment);
      bool isLocked = lockInfo.IsLocked;
      bool isLockedByMe = lockInfo.IsLockedByMe;
      
      if (!isLocked || (isLocked && isLockedByMe))
      {
        assignment.Complete(Sungero.Workflow.ReviewAssignment.Result.Accepted);
        succesfull = true;
      }
      
      return succesfull;
    }
    
    [Public]
    public static bool AcceptingFormalizedSubtask(IFormalizedSubtaskControlAssignment assignment)
    {
      bool succesfull = false;
      var lockInfo = Locks.GetLockInfo(assignment);
      bool isLocked = lockInfo.IsLocked;
      bool isLockedByMe = lockInfo.IsLockedByMe;
      
      if (!isLocked || (isLocked && isLockedByMe))
      {
        assignment.Complete(anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment.Result.Complete);
        succesfull = true;
      }
      
      return succesfull;
    }
    
    [Public]
    public static bool AbortingTask(ITask task)
    {
      bool succesfull = false;
      var lockInfo = Locks.GetLockInfo(task);
      bool isLocked = lockInfo.IsLocked;
      bool isLockedByMe = lockInfo.IsLockedByMe;
      
      if (!isLocked || (isLocked && isLockedByMe))
      {
        task.Abort();
        succesfull = true;
      }
      
      return succesfull;
    }

    [Public]
    public static bool FormalizedSubtaskInWork(anorsv.FormalizedSubTask.IFormalizedSubTask task)
    {
      return task.Status == anorsv.FormalizedSubTask.FormalizedSubTask.Status.InProcess
        && !anorsv
        .FormalizedSubTask
        .FormalizedSubtaskControlAssignments
        .GetAll(a => a.Task.Id == task.Id && a.Status == anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment.Status.InProcess)
        .Any();
    }
    
    [Public]
    public static bool TaskUnderReview(Sungero.Workflow.ITask task)
    {
      if (Sungero.Workflow.SimpleTasks.Is(task))
      {
        return SimpleUnderReview(Sungero.Workflow.SimpleTasks.As(task));
      }
      else if (anorsv.FormalizedSubTask.FormalizedSubTasks.Is(task))
      {
        return FormalizedSubtaskUnderReview(anorsv.FormalizedSubTask.FormalizedSubTasks.As(task));
      }
      else
      {
        return task.Status == Sungero.Workflow.Task.Status.UnderReview;
      }
    }
    
    [Public]
    public static bool FormalizedSubtaskUnderReview(anorsv.FormalizedSubTask.IFormalizedSubTask task)
    {
      return task.Status == anorsv.FormalizedSubTask.FormalizedSubTask.Status.InProcess
        && anorsv
        .FormalizedSubTask
        .FormalizedSubtaskControlAssignments
        .GetAll(a => a.Task.Id == task.Id && a.Status == anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment.Status.InProcess)
        .Any();
    }
    
    [Public, Remote(IsPure = false)]
    public static void CreateAsyncTaskModuleAcceptingTask(int taskId)
    {
      var asyncTaskModuleAcceptingTask = AsyncHandlers.TaskModuleAcceptingTask.Create();
      asyncTaskModuleAcceptingTask.TaskId = taskId;
      asyncTaskModuleAcceptingTask.ExecuteAsync();
    }
    
    [Public, Remote(IsPure = false)]
    public static void CreateAsyncTaskModuleAbortingTask(int taskId)
    {
      var asyncTaskModuleAbortingTask = AsyncHandlers.TaskModuleAbortingTask.Create();
      asyncTaskModuleAbortingTask.TaskId = taskId;
      asyncTaskModuleAbortingTask.ExecuteAsync();
    }

    [Public]
    public static bool FinalizingSimpleTask(ISimpleTask task, bool possibleAsyncOperaton)
    {
      bool succesfull = false;
      
      if (SimpleUnderReview(task))
      {
        IReviewAssignment reviewAssignment =
          ReviewAssignments.GetAll(a => a.Task.Equals(task) && a.Status == Sungero.Workflow.ReviewAssignment.Status.InProcess).SingleOrDefault();
        
        if (reviewAssignment != null)
        {
          if (!AcceptingSimpleTask(reviewAssignment))
          {
            if (possibleAsyncOperaton)
            {
              CreateAsyncTaskModuleAcceptingTask(task.Id);
              succesfull = true;
            }
          }
          else
          {
            succesfull = true;
          }
        }
        else
        {
          succesfull = true;
        }
      }
      else
      {
        if (!AbortingTask(task))
        {
          if (possibleAsyncOperaton)
          {
            CreateAsyncTaskModuleAbortingTask(task.Id);
            succesfull = true;
          }
        }
        else
        {
          succesfull = true;
        }
      }
      
      return succesfull;
    }
    
    [Public]
    public static bool FinalizingFormalizedSubtask(IFormalizedSubTask task, bool possibleAsyncOperaton)
    {
      bool succesfull = false;
      
      if (FormalizedSubtaskUnderReview(task))
      {
        IFormalizedSubtaskControlAssignment reviewAssignment =
          FormalizedSubtaskControlAssignments.GetAll(a => a.Task.Equals(task) && a.Status == Sungero.Workflow.ReviewAssignment.Status.InProcess).SingleOrDefault();
        
        if (reviewAssignment != null)
        {
          if (!AcceptingFormalizedSubtask(reviewAssignment))
          {
            if (possibleAsyncOperaton)
            {
              CreateAsyncTaskModuleAcceptingTask(task.Id);
              succesfull = true;
            }
          }
          else
          {
            succesfull = true;
          }
        }
        else
        {
          succesfull = true;
        }
      }
      else
      {
        var lastAssignment =
          Assignments
          .GetAll(a => a.Task.Equals(task) && (FormalizedSubtaskAssignments.Is(a) || FormalizedSubtaskControlAssignments.Is(a)))
          .OrderByDescending(a => a.Created)
          .FirstOrDefault();
        
        bool awaitingTaskComplete =
          FormalizedSubtaskControlAssignments.Is(lastAssignment)
          && lastAssignment.Status == anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment.Status.Completed;
        
        if (!awaitingTaskComplete)
        {
          if (!AbortingTask(task))
          {
            if (possibleAsyncOperaton)
            {
              CreateAsyncTaskModuleAbortingTask(task.Id);
              succesfull = true;
            }
          }
          else
          {
            succesfull = true;
          }
        }
        else
        {
          succesfull = true;
        }
      }
      
      return succesfull;
    }
    
    #endregion
    
    #region Работа с регламентами
    
    /// <summary>
    /// Определить текущий этап.
    /// </summary>
    /// <param name="task">Задача.</param>
    /// <param name="stageType">Тип этапа.</param>
    /// <returns>Текущий этап, либо null, если этапа нет (или это не тот этап).</returns>
    [Public]
    public static Structures.Module.IDefinedApprovalStageLite ApprovalTaskGetStage(Sungero.Docflow.IApprovalTask task, Enumeration stageType)
    {
      var stage = task.ApprovalRule.Stages
        .Where(s => s.Stage.StageType == stageType)
        .FirstOrDefault(s => s.Number == task.StageNumber);
      
      if (stage != null)
        return Structures.Module.DefinedApprovalStageLite.Create(sline.RSV.ApprovalStages.As(stage.Stage), stage.Number, stage.StageType);
      
      return null;
    }
    
    # endregion
    
    [Public]
    public static void BCHPSendMail(Sungero.Docflow.IApprovalTask task)
    {
      try
      {
        var approvalRules = sline.RSV.ApprovalStages
          .GetAll()
          .FirstOrDefault(r => r.StageType == Sungero.Docflow.ApprovalStage.StageType.Notice && r.BCHPanorsv.Value == true);
        var stage = task.ApprovalRule. Stages.Where(s => s.Stage == approvalRules).FirstOrDefault();
        if (stage != null)
        {
          var docKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.RecordManagement.Module.RecordManagement.PublicConstants.Module.DocKind.BCHPOutgoingLetterKind);
          var eDoc = sline.RSV.OutgoingLetters.As(task.DocumentGroup.OfficialDocuments.FirstOrDefault());
          if (Equals(eDoc.DocumentKind, docKind) && eDoc.BCHPDocParamsanorsv != null)
            anorsv.RecordManagement.Module.RecordManagement.PublicFunctions.Module.BCHPMailSend(eDoc);
        }
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
      }
    }
    
  }
}
