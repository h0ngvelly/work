﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.TaskModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      GrantAccessRightsForDatabook();
      FillTaskExtraParams();
    }
    
    public void GrantAccessRightsForDatabook()
    {
      InitializationLogger.Debug("Init: grant access rights for databooks.");
      var allUsers = Roles.AllUsers;

      // справочники
      InitializationLogger.Debug("Init: Grant access rights for TaskExtraParam.");
      if (!anorsv.TaskModule.TaskExtraParams.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Read, allUsers))
      {
        anorsv.TaskModule.TaskExtraParams.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
        anorsv.TaskModule.TaskExtraParams.AccessRights.Save();
      }
    }
    
    public void FillTaskExtraParams()
    {
      InitializationLogger.Debug("Init: fill params for assignments with subtasks in TaskExtraParam");
      
      var parentAssignments = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll()
        .Where(s => !anorsv.TaskModule.TaskExtraParams.GetAll()
               .Where(t => t.JobId == s.ParentAssignment.Id).Any())
        .Where(s => Sungero.Workflow.AssignmentBases.GetAll()
               .Where(a => a.Id == s.ParentAssignment.Id && a.Status.Equals(Sungero.Workflow.AssignmentBase.Status.InProcess)).Any())
        .Select(s => s.ParentAssignment.Id)
        .Distinct()
        .ToArray();
      
      var subtaskCount = 0;
      anorsv.TaskModule.ITaskExtraParam taskExtraParam = anorsv.TaskModule.TaskExtraParams.Null;
      foreach (int parentAssignmentId in parentAssignments)
      {
        subtaskCount = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll()
          .Where(s => s.ParentAssignment.Id == parentAssignmentId)
          .Count();
        
        if (subtaskCount > 0)
        {
          InitializationLogger.Debug("Init INFO: TaskID-" + Sungero.Workflow.AssignmentBases.Get(parentAssignmentId).Task.Id + ", JobID-" + parentAssignmentId + ", SubtNumber-" +  subtaskCount);
          try
          {
            taskExtraParam = anorsv.TaskModule.TaskExtraParams.Create();
            taskExtraParam.JobId = parentAssignmentId;
            taskExtraParam.TaskId = Sungero.Workflow.AssignmentBases.Get(parentAssignmentId).Task.Id;
            taskExtraParam.Name = string.Format("{0}-{1}", taskExtraParam.TaskId, taskExtraParam.JobId);
            taskExtraParam.SubtaskNumber = subtaskCount;
            taskExtraParam.Save();
          }
          catch (Exception ex)
          {
            InitializationLogger.Debug("Init EXCEPTION: error in assignment params fiiling for assignment " + parentAssignmentId+ ": " + ex.Message);
          }
        }
        
      }
    }
  }
}
