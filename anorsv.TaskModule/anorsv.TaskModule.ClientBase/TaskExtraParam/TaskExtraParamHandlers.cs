﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.TaskModule.TaskExtraParam;

namespace anorsv.TaskModule
{
  partial class TaskExtraParamClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      _obj.State.Properties.SubtaskNumber.IsEnabled = Users.Current.IncludedIn(Roles.Administrators);
      _obj.State.Properties.JobNumber.IsEnabled = Users.Current.IncludedIn(Roles.Administrators);
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      _obj.State.Properties.SubtaskNumber.IsEnabled = Users.Current.IncludedIn(Roles.Administrators);
      _obj.State.Properties.JobNumber.IsEnabled = Users.Current.IncludedIn(Roles.Administrators);
       
    }

  }
}