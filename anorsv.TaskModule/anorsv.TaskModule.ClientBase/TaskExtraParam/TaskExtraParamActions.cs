﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.TaskModule.TaskExtraParam;

namespace anorsv.TaskModule.Client
{
  partial class TaskExtraParamActions
  {
    public override void DeleteEntity(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.DeleteEntity(e);
    }

    public override bool CanDeleteEntity(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      //return base.CanDeleteEntity(e);
      if (Users.Current.IncludedIn(Roles.Administrators))
        return base.CanDeleteEntity(e);
      else
        return false;
    }

  }

}