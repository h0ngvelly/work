﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;

namespace anorsv.TaskModule.Client
{
  public class ModuleFunctions
  {

    #region Диалог приёмки подазадчи, если есть ведущее задание в работе
    
    [Public]
    public static Guid LeadingAssignmentDialog(Sungero.Workflow.IAssignmentBase assignment)
    {
      Guid result = Constants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.Cancel;
      var parentFormalizedSubtaskAssignment = anorsv.FormalizedSubTask.FormalizedSubtaskAssignments.As(assignment);
      var parentSimpleAssignment = Sungero.Workflow.SimpleAssignments.As(assignment);
      
      var dialog = Dialogs.CreateTaskDialog(
        Resources.ReviewAssignmentCompleteDialogText
        , Resources.ReviewAssignmentCompleteDialogDescription
        , MessageType.Question
        , Resources.ReviewAssignmentCompleteDialogTitle
       );
      
      if (parentFormalizedSubtaskAssignment != null || parentSimpleAssignment != null)
      {
        dialog.Buttons.AddCustom(Resources.ReviewAssignmentCompleteDialogAcceptCompleteActionName);
      }
      dialog.Buttons.AddCustom(Resources.ReviewAssignmentCompleteDialogAcceptActionName);
      dialog.Buttons.AddCustom(Resources.ReviewAssignmentCompleteDialogOpenActionName);
      dialog.Buttons.AddCancel();
      
      return GetLeadingAssignmentDialogResult(dialog.Show().Name);
    }
    
    private static Guid GetLeadingAssignmentDialogResult(string buttonName)
    {
      Guid result = Constants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.Cancel;
      
      if (buttonName == Resources.ReviewAssignmentCompleteDialogAcceptActionName)
      {
        result = Constants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.Accept;
      }
      else if (buttonName == Resources.ReviewAssignmentCompleteDialogAcceptCompleteActionName)
      {
        result = Constants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.AcceptAndComplete;
      }
      else if (buttonName == Resources.ReviewAssignmentCompleteDialogOpenActionName)
      {
        result = Constants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.Open;
      }
      
      return result;
    }
    
    #endregion
    
    #region Диалог приёмки подазадчи при ведущем задании в работе
    
    [Public]
    public static Guid AssignmentCompleteDialog(Guid dialogType)
    {
      List<Guid> ListAllowedDialogTypes = new List<Guid>() {
        anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Abort
          , anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.AcceptSubtasks
          , anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.AcceptSubtasksAndAbortSubtasks
          , anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.NotNeeded
      };
      
      if (!ListAllowedDialogTypes.Contains(dialogType))
      {
        throw AppliedCodeException.Create(Resources.AssignmentCompleteDialogTypeNotAllowedExceptionFormat(dialogType));
      }
      
      if (dialogType == anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.NotNeeded)
      {
        return dialogType;
      }
      
      Guid result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Cancel;
      string dialogDescription = string.Empty;
      string dialogSubTaskButtoName = string.Empty;
      
      if (dialogType.Equals(anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Abort))
      {
        dialogSubTaskButtoName = Resources.AssignmentCompleteDialogAbortActionName;
        dialogDescription = Resources.AssignmentCompleteDialogDescriptionAbortAction;
      }
      else if (dialogType.Equals(anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.AcceptSubtasksAndAbortSubtasks))
      {
        dialogSubTaskButtoName = Resources.AssignmentCompleteDialogAcceptAndAbortActionName;
        dialogDescription = Resources.AssignmentCompleteDialogDescriptionAcceptAndAbortAction;
      }
      else if (dialogType.Equals(anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.AcceptSubtasks))
      {
        dialogSubTaskButtoName = Resources.AssignmentCompleteDialogAcceptActionName;
        dialogDescription = Resources.AssignmentCompleteDialogDescriptionAcceptAction;
      }

      var dialog = Dialogs.CreateTaskDialog(
        Resources.AssignmentCompleteDialogText
        , dialogDescription
        , MessageType.Question
        , Resources.AssignmentCompleteDialogTitle
       );
      
      dialog.Buttons.AddCustom(dialogSubTaskButtoName);
      dialog.Buttons.AddCustom(Resources.AssignmentCompleteDialogShowActionName);
      
      
      dialog.Buttons.AddCancel();
      
      return GetAssignmentCompleteDialogResult(dialog.Show().Name);
    }
    
    private static Guid GetAssignmentCompleteDialogResult(string buttonName)
    {
      Guid result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Cancel;
      
      if (buttonName == Resources.AssignmentCompleteDialogShowActionName)
      {
        result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Show;
      }
      else if (buttonName == Resources.AssignmentCompleteDialogAbortActionName)
      {
        result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Abort;
      }
      else if (buttonName == Resources.AssignmentCompleteDialogAcceptActionName)
      {
        result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.AcceptSubtasks;
      }
      else if (buttonName == Resources.AssignmentCompleteDialogAcceptAndAbortActionName)
      {
        result = Constants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.AcceptSubtasksAndAbortSubtasks;
      }
      
      return result;
    }

    [Public]
    public static bool AssignmentComplete(int assignmentId)
    {
      while (true)
      {
        Guid dialogType = anorsv.TaskModule.PublicFunctions.Module.Remote.GetAssignmentCompleteDialogType(assignmentId);
        Guid choice = anorsv.TaskModule.PublicFunctions.Module.AssignmentCompleteDialog(dialogType);
        if (choice == anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Show)
        {
          IQueryable<Sungero.Workflow.ITask> activeSubtasks = anorsv.TaskModule.PublicFunctions.Module.Remote.GetAssignmentSubtasksInWork(assignmentId);
          anorsv.TaskModule.PublicFunctions.Module.Remote.GetAssignmentActiveSubtasks(assignmentId).ShowModal();
        }
        else if (choice == anorsv.TaskModule.PublicConstants.Module.AssignmentCompleteDialog.AssignmentCompleteDialogResult.Cancel)
        {
          return false;
        }
        else
        {
          return true;
        }
      }
    }
    
    #endregion
  }
  
}