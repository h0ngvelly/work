﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.ApprovalTaskModule.Server
{
  public class ModuleAsyncHandlers
  {

    public virtual void RelatedDocAdd(anorsv.ApprovalTaskModule.Server.AsyncHandlerInvokeArgs.RelatedDocAddInvokeArgs args)
    {
      // Добавляем вложение в задачу
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(args.DocId);
      if (relatedDocument == null)
      {
        Logger.DebugFormat("RelatedDocAdd: document is not found. DocID-{0}.", args.DocId);
        args.Retry = false;
        return;
      }
      
      var task = Sungero.Docflow.ApprovalTasks.Get(args.TaskId);
      if (task == null)
      {
        Logger.DebugFormat("RelatedDocAdd: task is not found. TaskID-{0}.", args.TaskId);
        args.Retry = false;
        return;
      }
      
      try
      {
        if (args.RelationName == Sungero.Docflow.PublicConstants.Module.AddendumRelationName)
          task.AddendaGroup.All.Add(relatedDocument);
        else
          task.OtherGroup.All.Add(relatedDocument);
        task.Save();
        args.Retry = false;
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("RelatedDocAdd: task is locked. TaskID-{0}, DocID-{1}. {2}", args.TaskId, args.DocId, ex.Message);
        args.Retry = true;
      }
    }

    public virtual void RelatedDocChange(anorsv.ApprovalTaskModule.Server.AsyncHandlerInvokeArgs.RelatedDocChangeInvokeArgs args)
    {
      // Изменяем группу для вложения задачи
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(args.DocId);
      if (relatedDocument == null)
      {
        Logger.DebugFormat("RelatedDocChange: document is not found. DocID-{0}.", args.DocId);
        args.Retry = false;
        return;
      }
      
      var task = Sungero.Docflow.ApprovalTasks.Get(args.TaskId);
      if (task == null)
      {
        Logger.DebugFormat("RelatedDocChange: task is not found. TaskID-{0}.", args.TaskId);
        args.Retry = false;
        return;
      }
      
      try
      {
        if (task.AddendaGroup.All.Contains(relatedDocument))
          task.AddendaGroup.All.Remove(relatedDocument);
        else if (task.OtherGroup.All.Contains(relatedDocument))
          task.OtherGroup.All.Remove(relatedDocument);
        
        if (args.RelationName == Sungero.Docflow.PublicConstants.Module.AddendumRelationName)
          task.AddendaGroup.All.Add(relatedDocument);
        else
          task.OtherGroup.All.Add(relatedDocument);
        task.Save();
        args.Retry = false;
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("RelatedDocChange: task is locked. TaskID-{0}, DocID-{1}. {2}", args.TaskId, args.DocId, ex.Message);
        args.Retry = true;
      }
      
    }

    public virtual void RelatedDocDelete(anorsv.ApprovalTaskModule.Server.AsyncHandlerInvokeArgs.RelatedDocDeleteInvokeArgs args)
    {
      // Удаляем вложение из задачи
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(args.DocId);
      if (relatedDocument == null)
      {
        Logger.DebugFormat("RelatedDocDelete: document is not found. DocID-{0}.", args.DocId);
        args.Retry = false;
        return;
      }
      
      var task = Sungero.Docflow.ApprovalTasks.Get(args.TaskId);
      if (task == null)
      {
        Logger.DebugFormat("RelatedDocDelete: task is not found. TaskID-{0}.", args.TaskId);
        args.Retry = false;
        return;
      }
      
      try
      {
        if (args.RelationName == Sungero.Docflow.PublicConstants.Module.AddendumRelationName)
          task.AddendaGroup.All.Remove(relatedDocument);
        else
          task.OtherGroup.All.Remove(relatedDocument);
        task.Save();
        args.Retry = false;
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("RelatedDocDelete: task is locked. TaskID-{0}, DocID-{1}. {2}", args.TaskId, args.DocId, ex.Message);
        args.Retry = true;
      }
    }

  }
}