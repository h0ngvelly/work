using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.ApprovalTask;
using docRelation = anorsv.RelationModule;

namespace anorsv.ApprovalTaskModule.Server
{
  public class ModuleFunctions
  {
    
    /// <summary>
    /// Получить список шаблонов в тематике СЗ
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static List<Sungero.Docflow.IDocumentTemplate> GetListTemplatesForMemo(int topicId)
    {
      var templateList = new List<Sungero.Docflow.IDocumentTemplate>();
      var topic = sline.CustomModule.TopicMemos.GetAll().Where(s => s.Id == topicId).FirstOrDefault(); // TODO MED: Потом заменить тематику СЗ на общую по мере их объединения
      if (topic != null)
      {
        var topicTemplateList = topic.Templates.Where(t => t.Template != null).Select(t => t.Template).ToList();
        templateList = Sungero.Docflow.DocumentTemplates.GetAll().Where(t => t.Status == Sungero.Docflow.DocumentTemplate.Status.Active && topicTemplateList.Contains(t)).ToList();
      }
      
      return templateList;
    }
    
    /// <summary>
    /// получить шаблон
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static Sungero.Docflow.IDocumentTemplate GetTemplate()
    {
      return Sungero.Docflow.DocumentTemplates.GetAll().Where(t => t.Status == Sungero.Docflow.DocumentTemplate.Status.Active).FirstOrDefault();
    }
    
    /// <summary>
    /// создать договор
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static anorsv.Contracts.IContract CreateContract(Sungero.Docflow.IOfficialDocument document, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectContract, bool IsStandard)
    {
      var docPurchase = sline.CustomModule.PurchaseRequests.As(document);
      var contract = anorsv.Contracts.Contracts.Create();
      contract.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.ExpendableContractKind);
      contract.Subject = docPurchase.Name;
      contract.Project = docPurchase.Project;
      contract.PurchaseMethodanorsv = docPurchase.PurchaseMethod;
      // Способ закупки
      var PurchaseMethod = docPurchase.PurchaseMethod;
      //вид документа - "Закупка у единственного поставщика"
      var singlePurchaseKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.SinglePurchaseKind);
      if (PurchaseMethod.DocumentKind == singlePurchaseKind)
      {
        //Единственный поставщик
        contract.Counterparty =  anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetCounterpartySingleSupplier();
      }
      else
      {
        //Контрагент не определен
        contract.Counterparty =  anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetCounterpartyNotDefinedCounterparty();
      }
      
      var docKind = anorsv.AnorsvMainSolution.DocumentKinds.As(singlePurchaseKind);
      if (docKind.SignESanorsv == anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignESanorsv)
        contract.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES;
      else
        contract.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignOnPaper;
      
      contract.Frcanorsv = docPurchase.Frc;
      contract.SubjectPurchaseanorsv = docPurchase.Subject;
      contract.SummFSanorsv = docPurchase.SumIF;
      contract.ExpectedResultanorsv = docPurchase.Result;
      contract.EmployeeDZDanorsv = docPurchase.EmployeeDZD;
      contract.IdRPanorsv = docPurchase.IdentityPP;
      foreach (var SourceFinancing in docPurchase.SourcesFinancing)
      {
        contract.SourcesFinancinganorsv.AddNew().SourceFinancing = SourceFinancing.SourceFinancing;
      }
      
      contract.Note = docPurchase.Note;
      contract.IsStandard = IsStandard;
      contract.GeneralTopicanorsv = topic;
      contract.SubjectContractanorsv = subjectContract;
      contract.Purchaseanorsv = docPurchase;
      contract.LeadingDocument = docPurchase;
      contract.Save();
      return contract;
      
    }
    
    /// <summary>
    /// создать карточку документа ТЗ
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static anorsv.ContractsModule.ITechnicalSpecification CreateCardTS(Sungero.Docflow.IOfficialDocument document, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectTS, bool IsStandard)
    {
      var docPurchase = sline.CustomModule.PurchaseRequests.As(document);
      var docTS = anorsv.ContractsModule.TechnicalSpecifications.Create();
      docTS.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
      docTS.Subject = subjectTS;
      docTS.Project = docPurchase.Project;
      docTS.PurchaseMethodanorsv = docPurchase.PurchaseMethod;
      docTS.SignESanorsv = docPurchase.SignESanorsv;
      // Способ закупки
      var PurchaseMethod = docPurchase.PurchaseMethod;
      //вид документа - "Закупка у единственного поставщика"
      var singlePurchaseKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.SinglePurchaseKind);
      if (PurchaseMethod.DocumentKind == singlePurchaseKind)
      {
        //Единственный поставщик
        docTS.Counterparty =  anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetCounterpartySingleSupplier();
      }
      else
      {
        //Контрагент не определен
        docTS.Counterparty =  anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetCounterpartyNotDefinedCounterparty();
      }
      
      var docKind = anorsv.AnorsvMainSolution.DocumentKinds.As(singlePurchaseKind);
      if (docKind.SignESanorsv == anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignESanorsv)
        docTS.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES;
      else
        docTS.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignOnPaper;
      
      docTS.Frc = docPurchase.Frc;
      docTS.SubjectPurchase = docPurchase.Subject;
      docTS.SummFSanorsv = docPurchase.SumIF;
      docTS.ExpectedResultanorsv = docPurchase.Result;
      docTS.EmployeeDZDanorsv = docPurchase.EmployeeDZD;
      docTS.IdRPanorsv = docPurchase.IdentityPP;
      foreach (var SourceFinancing in docPurchase.SourcesFinancing)
      {
        docTS.SourcesFinancing.AddNew().SourceFinancing = SourceFinancing.SourceFinancing;
      }
      
      docTS.Note = docPurchase.Note;
      docTS.IsStandard = IsStandard;
      docTS.GeneralTopic = topic;
      docTS.SubjectTS = subjectTS;
      docTS.Purchaseanorsv = docPurchase;
      docTS.LeadingDocument = docPurchase;
      docTS.Save();
      return docTS;
      
    }
    
    /// <summary>
    /// создать СЗ по закупке
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static sline.RSV.IMemo CreateMemo(Sungero.Docflow.IOfficialDocument document, sline.CustomModule.ITopicMemo topic)
    {
      var docPurchase = sline.CustomModule.PurchaseRequests.As(document);
      var memo = sline.RSV.Memos.Create();
      memo.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.ProcurementActivitiesKind);
      memo.MemoTopicanorsv = topic; // TODO MED: Потом заменить тематику СЗ на общую по мере их объединения
      memo.Purchaseanorsv = docPurchase;
      memo.LeadingDocument = docPurchase;
      memo.Save();
      
      return memo;
    }
    
    /// <summary>
    /// получить контрагента-заглушку - Единственный поставщик
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static Sungero.Parties.ICounterparty GetCounterpartySingleSupplier()
    {
      var supplier = Sungero.Parties.Counterparties.GetAll().Where(t => t.Name == anorsv.ApprovalTaskModule.Resources.SingleSupplier);
      if (supplier.Any())
      {
        return supplier.FirstOrDefault();
      }
      else
      {
        var newsupplier = Sungero.Parties.Companies.Create();
        newsupplier.Name = anorsv.ApprovalTaskModule.Resources.SingleSupplier;
        newsupplier.Save();
        return  newsupplier;
      }
    }
    
    /// <summary>
    /// получить контрагента-заглушку - Контрагент не определен
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static Sungero.Parties.ICounterparty GetCounterpartyNotDefinedCounterparty()
    {
      var supplier = Sungero.Parties.Counterparties.GetAll().Where(t => t.Name == anorsv.ApprovalTaskModule.Resources.NotDefinedCounterparty);
      if (supplier.Any())
      {
        return supplier.FirstOrDefault();
      }
      else
      {
        var newsupplier = Sungero.Parties.Companies.Create();
        newsupplier.Name = anorsv.ApprovalTaskModule.Resources.NotDefinedCounterparty;
        newsupplier.Save();
        return  newsupplier;
      }
    }
    
    /// <summary>
    /// Получить список шаблонов в тематике
    /// </summary>
    [Remote(IsPure = true)]
    public static List<Sungero.Docflow.IDocumentTemplate> GetListTemplates(int topicId)
    {
      // ИСПРАВЛЕНИЕ УСЛОВИЯ:
      //return anorsv.DocflowModule.AnorsvGenericTopics.GetAll().
      //  Where(s => s.Id == topicId).FirstOrDefault().Templates.Where(t => t.Template != null).Select(t => t.Template).ToList();
      
      var templateList = new List<Sungero.Docflow.IDocumentTemplate>();
      var topic = anorsv.DocflowModule.AnorsvGenericTopics.GetAll().Where(s => s.Id == topicId).FirstOrDefault();
      if (topic != null)
        templateList = topic.Templates.Where(t => t.Template != null).Select(t => t.Template).ToList();
      
      return templateList;
    }

    /// <summary>
    /// Редактирование Предсогласующих доступно, если:
    /// 1. Есть блок предварительного согласования И
    /// 2. В Тематике общей или Тематике СЗ настроено предварительное согласование
    /// </summary>
    [Public]
    public static bool PreApproversEnabled(anorsv.ApprovalTask.IApprovalTask task)
    {
      var canEditPreApprovers = false;
      var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      // Для СЗ - Предварительно согласующие по Тематикам СЗ с настроенным предварительным согласованием
      var memo = sline.RSV.Memos.As(document);
      var doc = anorsv.OfficialDocument.OfficialDocuments.As(document);
      if (memo != null)
      {
        var memoTopic = memo.MemoTopicanorsv;
        canEditPreApprovers = canEditPreApprovers || (memoTopic != null && memoTopic.PreAgreement == true);
      }
      // Для всех - Предварительно согласующие по Тематикам общим с настроенным предварительным согласованием
      if (doc != null)
      {
        var baseTopic = doc.Topicanorsv;
        canEditPreApprovers = canEditPreApprovers || (baseTopic != null && baseTopic.PreAgreement == true);
      }
      
      var approvalRule = sline.RSV.ApprovalRules.As(task.ApprovalRule);
      var stages = approvalRule.Stages.Where(a => a.StageType == Sungero.Docflow.ApprovalRuleStages.StageType.Approvers);
      var isStage = false;
      foreach (var stage in stages)
      {
        var rsvStage = sline.RSV.ApprovalStages.As(stage.Stage);
        if(rsvStage != null)
        {
          if(rsvStage.PreliminaryApprovalBlockanorsv.Value)
            isStage = rsvStage.PreliminaryApprovalBlockanorsv.Value;
        }
      }
      
      if (!isStage)
      {
        // Предварительно согласующие не доступны для заполнения, если нет блока предварительного согласования
        return false;
      }
      else
      {
        // Предварительно согласующие не доступны для заполнения, если в тематике запрещено предварительное согласование
        if (!canEditPreApprovers)
          return false;
      }
      
      return true;
    }
    
    [Public]
    public static void BCHPSendMail(Sungero.Docflow.IApprovalTask task)
    {
      try
      {
        var approvalRules = sline.RSV.ApprovalStages.GetAll().FirstOrDefault(r => r.StageType == Sungero.Docflow.ApprovalStage.StageType.Notice && r.BCHPanorsv.Value == true);
        var stage = task.ApprovalRule. Stages.Where(s => s.Stage == approvalRules).FirstOrDefault();
        if (stage != null)
        {
          var docKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.RecordManagement.Module.RecordManagement.PublicConstants.Module.DocKind.BCHPOutgoingLetterKind);
          var eDoc = sline.RSV.OutgoingLetters.As(task.DocumentGroup.OfficialDocuments.FirstOrDefault());
          if (Equals(eDoc.DocumentKind, docKind) && eDoc.BCHPDocParamsanorsv != null)
            anorsv.RecordManagement.Module.RecordManagement.PublicFunctions.Module.BCHPMailSend(eDoc);
        }
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
      }
    }
    
    #region Работа со схемой регламента
    
    /// <summary>
    /// Пропуск этапа в случае:
    /// 1. Если меняли тип подписания с ЭП на подписание на бумаге.
    /// Сбрасывает признак пропуска этапа при смене типа подписания, если блок имеет в схеме условие "Подписать ЭП?".
    /// 2. Если блок типа Согласование с руководителем и Руководитель не указан
    /// </summary>
    /// <param name="task">Задача для которой делается проверка пропуска этапа.</param>
    /// <param name="stageType">Тип этапа для которого делается проверка пропуска.</param>
    /// <returns>Возвращает true, если нужно пропустить этап, иначе false.</returns>
    [Public]
    public static bool SkipStage(anorsv.ApprovalTask.IApprovalTask task, Enumeration stageType)
    {
      var needSkip = task.SkipStageanorsv ?? false;
      var stage = anorsv.ApprovalTaskModule.PublicFunctions.Module.GetStage(task, stageType);
      
      if (stage != null && needSkip)
      {
        var previusConditionExist = anorsv.ApprovalTaskModule.PublicFunctions.Module
          .ConditionInPreviousBlockExist(task, task.StageNumber, sline.RSV.Condition.ConditionType.SingES);
        
        if (previusConditionExist)
        {
          task.SkipStageanorsv = false;
          needSkip = false;
        }
      }
      
      if (stage != null && stageType == sline.RSV.ApprovalStage.StageType.Manager && needSkip == false)
      {
        var manager = Sungero.Company.Employees.Null;
        var anorsvRole = anorsv.AnorsvMainSolution.ApprovalRoles.As(stage.Stage.ApprovalRole);
        var slineRole = sline.CustomModule.MyApprovalRoleses.As(stage.Stage.ApprovalRole);
        
        if (stage.Stage.AssigneeType == Sungero.Docflow.ApprovalStage.AssigneeType.Role)
        {
          if (anorsvRole != null)
          {
            manager = anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRole.GetRolePerformer(anorsvRole, task);
          }
          else if (slineRole != null)
          {
            manager = sline.CustomModule.PublicFunctions.MyApprovalRoles.GetRolePerformer(slineRole, task);
          }
        }
        else
        {
          manager = Sungero.Docflow.PublicFunctions.ApprovalRuleBase.Remote.GetEmployeeByAssignee(stage.Stage.Assignee); //
        }
        
        if (manager == null)
          needSkip = true;
      }
      
      return needSkip;
    }
    
    /// <summary>
    /// Определяет наличие блока условия с определённым типом в схеме регламента.
    /// </summary>
    /// <param name="task">Задача, в регламенте которой, осуществляется поиск.</param>
    /// <param name="stageNumber">Номер этапа для котрого делается проверка.</param>
    /// <param name="conditionType">Тип условия для которого делается проверка.</param>
    /// <returns>Возвращает true если ранее в регламенте для текущего этапа встречается блок условия с нужным типом,иначе false.</returns>
    [Public]
    public static bool ConditionInPreviousBlockExist(anorsv.ApprovalTask.IApprovalTask task, int? stageNumber, Enumeration conditionType)
    {
      var conditionExist = false;
      var rule = task.ApprovalRule;
      var currentBlockNumber = stageNumber ?? task.StageNumber;
      var previousBlockNumber = currentBlockNumber;
      
      if (currentBlockNumber != null)
      {
        while (!conditionExist && previousBlockNumber != null)
        {
          previousBlockNumber = task.ApprovalRule.Transitions.Where(t => t.TargetStage == currentBlockNumber).Select(t => t.SourceStage).FirstOrDefault();
          conditionExist = task.ApprovalRule.Conditions.Any(c => c.Number == previousBlockNumber && c.Condition.ConditionType == conditionType);
          currentBlockNumber = previousBlockNumber;
        }
      }
      
      return conditionExist;
    }
    
    /// <summary>
    /// Определяет наличие блока условия с определённым типом в схеме регламента.
    /// </summary>
    /// <param name="task">Задача, в регламенте которой, осуществляется поиск.</param>
    /// <param name="conditionType">Тип условия для которого делается проверка.</param>
    /// <returns>Возвращает true если ранее в регламенте для текущего этапа встречается блок условия с нужным типом,иначе false.</returns>
    [Public]
    public static bool ConditionInPreviousBlockExist(anorsv.ApprovalTask.IApprovalTask task, Enumeration conditionType)
    {
      var conditionExist = ConditionInPreviousBlockExist(task, task.StageNumber, conditionType);
      
      return conditionExist;
    }
    
    /// <summary>
    /// Определить текущий этап.
    /// </summary>
    /// <param name="task">Задача.</param>
    /// <param name="stageType">Тип этапа.</param>
    /// <returns>Текущий этап, либо null, если этапа нет (или это не тот этап).</returns>
    [Public]
    public static Structures.Module.IDefinedApprovalStageLite GetStage(anorsv.ApprovalTask.IApprovalTask task, Enumeration stageType)
    {
      var stage = task.ApprovalRule.Stages
        .Where(s => s.Stage.StageType == stageType)
        .FirstOrDefault(s => s.Number == task.StageNumber);
      
      if (stage != null)
        return Structures.Module.DefinedApprovalStageLite.Create(stage.Stage, stage.Number, stage.StageType);
      
      return null;
    }

    #endregion
    
    #region Изменение вложения связанного документа в задачах
    
    /// <summary>
    /// Добавление связанного документа в область вложений в задаче по регламенту
    /// </summary>
    [Public]
    public static void AddRelatedDocInAttachments(Sungero.Docflow.IApprovalTask task, Sungero.Content.IElectronicDocument relatedDocument, int? assignmentId)
    {
      bool changeInTask = true;
      var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        // Если возможно, изменяем вложения напрямую в задании
        Sungero.Workflow.IAssignmentBase assignment = Sungero.Workflow.AssignmentBases.Null;
        if (assignmentId != null)
        {
          assignment = Sungero.Workflow.AssignmentBases.GetAll(a => a.Id == assignmentId).FirstOrDefault();
          if (assignment != null && assignment.Task.Id == task.Id)
          {
            changeInTask = false;
            // Для каждого типа задания отдельная фукнция, чтобы вложения сразу визуально отобразились
            if (Sungero.Docflow.ApprovalReworkAssignments.Is(assignment))
              anorsv.ApprovalTaskModule.Functions.Module.AddRelatedDocInReworkAssignment(Sungero.Docflow.ApprovalReworkAssignments.As(assignment), relatedDocument);
            else if (Sungero.Docflow.ApprovalCheckingAssignments.Is(assignment))
              anorsv.ApprovalTaskModule.Functions.Module.AddRelatedDocInCheckingAssignment(Sungero.Docflow.ApprovalCheckingAssignments.As(assignment), relatedDocument);
            else if (Sungero.Docflow.ApprovalAssignments.Is(assignment))
              anorsv.ApprovalTaskModule.Functions.Module.AddRelatedDocInApprovalAssignment(Sungero.Docflow.ApprovalAssignments.As(assignment), relatedDocument);
            else if (Sungero.Docflow.ApprovalSimpleAssignments.Is(assignment))
              anorsv.ApprovalTaskModule.Functions.Module.AddRelatedDocInSimpleAssignment(Sungero.Docflow.ApprovalSimpleAssignments.As(assignment), relatedDocument);
            else
              changeInTask = true;
          }
        }
        
        if (changeInTask == true)
        {
          // Если не возможно изменить вложения напрямую в задании, меняем через задачу
          var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
          if (documentAddenda.Contains(relatedDocument))
          {
            // Если связанный документ - Приложение, вложить в область Приложения
            if (!task.AddendaGroup.All.Contains(relatedDocument))
              task.AddendaGroup.All.Add(relatedDocument);
          }
          else
          {
            // Если связанный документ - не Приложение, вложить в область Дополнительно
            if (!task.OtherGroup.All.Contains(relatedDocument))
              task.OtherGroup.All.Add(relatedDocument);
          }
          task.Save();
        }
      }
    }
    
    /// <summary>
    /// Добавление связанного документа в область вложений в задание Доработки
    /// </summary>
    public static void AddRelatedDocInReworkAssignment(Sungero.Docflow.IApprovalReworkAssignment assignment, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = assignment.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (documentAddenda.Contains(relatedDocument))
        {
          // Если связанный документ - Приложение, вложить в область Приложения
          if (!assignment.AddendaGroup.All.Contains(relatedDocument))
            assignment.AddendaGroup.All.Add(relatedDocument);
        }
        else
        {
          // Если связанный документ - не Приложение, вложить в область Дополнительно
          if (!assignment.OtherGroup.All.Contains(relatedDocument))
            assignment.OtherGroup.All.Add(relatedDocument);
        }
        assignment.Save();
      }
    }
    
    /// <summary>
    /// Добавление связанного документа в область вложений в задание Проверки
    /// </summary>
    public static void AddRelatedDocInCheckingAssignment(Sungero.Docflow.IApprovalCheckingAssignment assignment, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = assignment.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (documentAddenda.Contains(relatedDocument))
        {
          // Если связанный документ - Приложение, вложить в область Приложения
          if (!assignment.AddendaGroup.All.Contains(relatedDocument))
            assignment.AddendaGroup.All.Add(relatedDocument);
        }
        else
        {
          // Если связанный документ - не Приложение, вложить в область Дополнительно
          if (!assignment.OtherGroup.All.Contains(relatedDocument))
            assignment.OtherGroup.All.Add(relatedDocument);
        }
        assignment.Save();
      }
    }
    
    /// <summary>
    /// Добавление связанного документа в область вложений в задание Согласования
    /// </summary>
    public static void AddRelatedDocInApprovalAssignment(Sungero.Docflow.IApprovalAssignment assignment, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = assignment.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (documentAddenda.Contains(relatedDocument))
        {
          // Если связанный документ - Приложение, вложить в область Приложения
          if (!assignment.AddendaGroup.All.Contains(relatedDocument))
            assignment.AddendaGroup.All.Add(relatedDocument);
        }
        else
        {
          // Если связанный документ - не Приложение, вложить в область Дополнительно
          if (!assignment.OtherGroup.All.Contains(relatedDocument))
            assignment.OtherGroup.All.Add(relatedDocument);
        }
        assignment.Save();
      }
    }
    
    /// <summary>
    /// Добавление связанного документа в область вложений в задание
    /// </summary>
    public static void AddRelatedDocInSimpleAssignment(Sungero.Docflow.IApprovalSimpleAssignment assignment, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = assignment.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (documentAddenda.Contains(relatedDocument))
        {
          // Если связанный документ - Приложение, вложить в область Приложения
          if (!assignment.AddendaGroup.All.Contains(relatedDocument))
            assignment.AddendaGroup.All.Add(relatedDocument);
        }
        else
        {
          // Если связанный документ - не Приложение, вложить в область Дополнительно
          if (!assignment.OtherGroup.All.Contains(relatedDocument))
            assignment.OtherGroup.All.Add(relatedDocument);
        }
        assignment.Save();
      }
    }
    
    /// <summary>
    /// Удаление связанного документа из области вложений в задаче по регламенту
    /// </summary>
    [Public]
    public static void DeleteRelatedDocInAttachments(anorsv.ApprovalTask.IApprovalTask task, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        if (task.AddendaGroup.All.Contains(relatedDocument))
        {
          // Удалить документ из области Приложения
          task.AddendaGroup.All.Remove(relatedDocument);
        }
        else if (task.OrderGroupanorsv.All.Contains(relatedDocument))
        {
          // Удалить документ из области Приказ
          task.OrderGroupanorsv.All.Remove(relatedDocument);
        }
        else if (task.ContractGroupanorsv.All.Contains(relatedDocument))
        {
          // Удалить документ из области Проект договора
          task.ContractGroupanorsv.All.Remove(relatedDocument);
        }
        else if (task.TechnicalSpecificationGroupanorsv.All.Contains(relatedDocument))
        {
          // Удалить документ из области Техническое задание
          task.TechnicalSpecificationGroupanorsv.All.Remove(relatedDocument);
        }
        else if (task.MemoGroupanorsv.All.Contains(relatedDocument))
        {
          // Удалить документ из области Служебная записка
          task.MemoGroupanorsv.All.Remove(relatedDocument);
        }
        else if (task.OtherGroup.All.Contains(relatedDocument))
        {
          // Удалить документ из области Дополнительно
          task.OtherGroup.All.Remove(relatedDocument);
        }
        
        task.Save();
      }
    }
    
    /// <summary>
    /// Изменение области вложения связанного документа в задаче по регламенту
    /// </summary>
    [Public]
    public static void ChangeRelatedDocInAttachments(anorsv.ApprovalTask.IApprovalTask task, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null  && task.AllAttachments.Contains(relatedDocument))
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (documentAddenda.Contains(relatedDocument))
        {
          // Если связанный документ - Приложение
          if (task.OtherGroup.All.Contains(relatedDocument))
          {
            // Удалить документ из области Дополнительно
            task.OtherGroup.All.Remove(relatedDocument);
          }
          else if (task.OrderGroupanorsv.All.Contains(relatedDocument))
          {
            // Удалить документ из области Приказ
            task.OrderGroupanorsv.All.Remove(relatedDocument);
          }
          else if (task.ContractGroupanorsv.All.Contains(relatedDocument))
          {
            // Удалить документ из области Проект договора
            task.ContractGroupanorsv.All.Remove(relatedDocument);
          }
          else if (task.TechnicalSpecificationGroupanorsv.All.Contains(relatedDocument))
          {
            // Удалить документ из области Техническое задание
            task.TechnicalSpecificationGroupanorsv.All.Remove(relatedDocument);
          }
          else if (task.MemoGroupanorsv.All.Contains(relatedDocument))
          {
            // Удалить документ из области Служебная записка
            task.MemoGroupanorsv.All.Remove(relatedDocument);
          }
          
          if (!task.AddendaGroup.All.Contains(relatedDocument))
          {
            // Вложить документ в область Приложения
            task.AddendaGroup.All.Add(relatedDocument);
          }
        }
        else
        {
          // Если связанный документ - не Приложение
          if (task.AddendaGroup.All.Contains(relatedDocument))
          {
            // Удалить документ из области Приложения
            task.AddendaGroup.All.Remove(relatedDocument);
          }
          else if (task.OrderGroupanorsv.All.Contains(relatedDocument))
          {
            // Удалить документ из области Приказ
            task.OrderGroupanorsv.All.Remove(relatedDocument);
          }
          else if (task.ContractGroupanorsv.All.Contains(relatedDocument))
          {
            // Удалить документ из области Проект договора
            task.ContractGroupanorsv.All.Remove(relatedDocument);
          }
          else if (task.TechnicalSpecificationGroupanorsv.All.Contains(relatedDocument))
          {
            // Удалить документ из области Техническое задание
            task.TechnicalSpecificationGroupanorsv.All.Remove(relatedDocument);
          }
          else if (task.MemoGroupanorsv.All.Contains(relatedDocument))
          {
            // Удалить документ из области Служебная записка
            task.MemoGroupanorsv.All.Remove(relatedDocument);
          }
          
          if (!task.OtherGroup.All.Contains(relatedDocument))
          {
            // Вложить документ в область Дополнительно
            task.OtherGroup.All.Add(relatedDocument);
          }
        }
        
        task.Save();
      }
    }
    
    #endregion
    
    #region Проверка задач/заданий по связанному документу
    
    /// <summary>
    /// Проверка наличия задач по регламенту, в которые вложен документ
    /// </summary>
    [Public]
    public static bool IsAnyTaskInProcess(Sungero.Docflow.IOfficialDocument document)
    {
      var taskIds = string.Empty;
      var command = string.Format(Queries.Module.SelectTaskData, document.Id);
      var commandExecutionResult = Sungero.Docflow.PublicFunctions.Module.ExecuteScalarSQLCommand(command);
      if (!(commandExecutionResult is DBNull) && commandExecutionResult != null)
        taskIds = commandExecutionResult.ToString();
      
      return !string.IsNullOrWhiteSpace(taskIds);
    }
    
    /// <summary>
    /// Проверка наличия заданий в задачах по регламенту, в которые вложен документ
    /// и пользователь является исполнителем или замещающим исполнителя
    /// </summary>
    [Public]
    public static bool IsAnyJobInProcess(Sungero.Docflow.IOfficialDocument document, Sungero.CoreEntities.IUser user)
    {
      var jobIds = string.Empty;
      var substUsers = Sungero.CoreEntities.Substitutions.ActiveSubstitutedUsersWithoutSystem.ToList();
      string performersId = user.Id.ToString();//Users.Current.Id.ToString();
      if (substUsers.Any())
        performersId = performersId + "," + string.Join(",", substUsers.Select(s => s.Id));
      
      var command = string.Format(Queries.Module.SelectJobData, document.Id, performersId);
      var commandExecutionResult = Sungero.Docflow.PublicFunctions.Module.ExecuteScalarSQLCommand(command);
      if (!(commandExecutionResult is DBNull) && commandExecutionResult != null)
        jobIds = commandExecutionResult.ToString();
      
      return !string.IsNullOrWhiteSpace(jobIds);
    }
    
    /// <summary>
    /// Список задач, в которые вложен документ
    /// и пользователь является исполнителем или замещающим исполнителя задания в работе
    /// </summary>
    [Public, Remote]
    public static List<Sungero.Workflow.ITask> GetTaskInProcess(int documentId, Sungero.CoreEntities.IUser user)
    {
      var taskIdList = new List<int>();
      
      if (Users.Current.IncludedIn(Roles.Administrators))
      {
        taskIdList = Sungero.Workflow.AssignmentBases.GetAll()
          .Where(j => j.Status.Equals(Sungero.Workflow.AssignmentBase.Status.InProcess) || j.Status.Equals(Sungero.Workflow.AssignmentBase.Status.Suspended))
          .Where(j => j.Task.AttachmentDetails.Any(att => att.AttachmentId == documentId))
          .Select(j => j.Task.Id)
          .ToList();
      }
      else
      {
        var substUsers = Sungero.CoreEntities.Substitutions.ActiveSubstitutedUsersWithoutSystem.ToList();
        substUsers.Add(Users.Current);
        taskIdList = Sungero.Workflow.AssignmentBases.GetAll()
          .Where(j => j.Status.Equals(Sungero.Workflow.AssignmentBase.Status.InProcess) || j.Status.Equals(Sungero.Workflow.AssignmentBase.Status.Suspended))
          .Where(j => substUsers.Contains(j.Performer))
          .Where(j => j.Task.AttachmentDetails.Any(att => att.AttachmentId == documentId))
          .Select(j => j.Task.Id)
          .ToList();
      }
      var taskList = Sungero.Workflow.Tasks.GetAll()
        .Where(t => taskIdList.Contains(t.Id))
        .Where(t => t.Status.Equals(Sungero.Workflow.Task.Status.InProcess) || t.Status.Equals(Sungero.Workflow.Task.Status.Suspended))
        .ToList();
      
      return taskList;
    }
    
    /// <summary>
    /// Список заданий, в которые вложен документ
    /// и пользователь является исполнителем или замещающим исполнителя задания в работе
    /// </summary>
    [Public, Remote]
    public static List<Sungero.Workflow.IAssignmentBase> GetJobInProcess(int documentId)
    {
      var jobList = new List<Sungero.Workflow.IAssignmentBase>();
      
      if (Users.Current.IncludedIn(Roles.Administrators))
      {
        jobList = Sungero.Workflow.AssignmentBases.GetAll()
          .Where(j => j.Status.Equals(Sungero.Workflow.AssignmentBase.Status.InProcess) || j.Status.Equals(Sungero.Workflow.AssignmentBase.Status.Suspended))
          .Where(j => j.Task.AttachmentDetails.Any(att => att.AttachmentId == documentId))
          .ToList();
      }
      else
      {
        var substUsers = Sungero.CoreEntities.Substitutions.ActiveSubstitutedUsersWithoutSystem.ToList();
        substUsers.Add(Users.Current);
        jobList = Sungero.Workflow.AssignmentBases.GetAll()
          .Where(j => j.Status.Equals(Sungero.Workflow.AssignmentBase.Status.InProcess) || j.Status.Equals(Sungero.Workflow.AssignmentBase.Status.Suspended))
          .Where(j => substUsers.Contains(j.Performer))
          .Where(j => j.Task.AttachmentDetails.Any(att => att.AttachmentId == documentId))
          .ToList();
      }
      
      return jobList;
    }
    
    #endregion

  }
}
