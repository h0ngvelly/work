﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.ApprovalTaskModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      CreateRoles();
    }
    
    public static void CreateRoles()
    {
      string roleName = Resources.RoleNameSigningTypeChange;;
      string roleDescription = Resources.DescriptionSigningTypeChangeRole;
      Guid roleGuid = Constants.Module.RoleGuid.SingingTypeChangeRole;
      
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(roleName, roleDescription, roleGuid);
    }
  }

}
