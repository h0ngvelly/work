﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.ApprovalTaskModule.Structures.Module
{

  /// <summary>
  /// 
  /// </summary>
  [Public]
  partial class SubstitutionsMessage
  {
    public string Message {get; set;}
  }

  [Public]
  partial class DefinedApprovalStageLite
  {
    public Sungero.Docflow.IApprovalStage Stage { get; set; }
    
    public int? Number { get; set; }
    
    public Sungero.Core.Enumeration? StageType { get; set; }
  }

}