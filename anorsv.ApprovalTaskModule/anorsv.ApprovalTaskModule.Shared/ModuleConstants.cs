﻿using System;
using Sungero.Core;

namespace anorsv.ApprovalTaskModule.Constants
{
  public static class Module
  {
    // Максимальный размер файла для добавления из диалога.
    [Public]
    public const int DocDialogMaxFileSize = 2147483646; // 2Гб
    
    [Public]
    public static class RoleGuid
    {
      /// <summary>
      /// Guid роли "Изменяющие вид подписания в согласованиях по регламенту"
      /// </summary>
      public static readonly Guid SingingTypeChangeRole = Guid.Parse("7E39D571-A1BA-4358-AED8-C438052FACD8");
    }
    public static class DocKind
    {
      /// <summary>
      /// Договор расходный
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid ExpendableContractKind = Guid.Parse("E0D61AB9-ECBD-42EE-BF44-0DF4B08863F8");
      /// <summary>
      /// Служебные записки по закупочной деятельности
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid ProcurementActivitiesKind = Guid.Parse("CAEB4E82-4C1A-4DE3-A53B-A3314B878194");
    }
  }
}