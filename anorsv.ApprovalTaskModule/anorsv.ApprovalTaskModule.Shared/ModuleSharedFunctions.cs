using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.ApprovalTaskModule.Shared
{
  public class ModuleFunctions
  {
    #region Проверки на необходимость вложения при выполнении задания
    [Public]
    public static string CheckAttachedDocuments(Sungero.Workflow.IAssignment assignment)
    {
      var resultMessage = string.Empty;
      
      if (anorsv.ApprovalTask.ApprovalCheckingAssignments.Is(assignment))
      {
        resultMessage = anorsv.ApprovalTaskModule.Functions.Module.CheckApprovalCheckingAssignmentAttachedDocuments(anorsv.ApprovalTask.ApprovalCheckingAssignments.As(assignment));
      }
      else if (anorsv.ApprovalTask.ApprovalSimpleAssignments.Is(assignment))
      {
        resultMessage = anorsv.ApprovalTaskModule.Functions.Module.CheckApprovalSimpleAssignmentAttachedDocuments(anorsv.ApprovalTask.ApprovalSimpleAssignments.As(assignment));
      }
      else if (anorsv.ApprovalTask.ApprovalReworkAssignments.Is(assignment))
      {
        resultMessage = anorsv.ApprovalTaskModule.Functions.Module.CheckApprovalReworkAssignmentAttachedDocuments(anorsv.ApprovalTask.ApprovalReworkAssignments.As(assignment));
      }
      
      return resultMessage;
    }
    
    public static string CheckApprovalCheckingAssignmentAttachedDocuments(anorsv.ApprovalTask.IApprovalCheckingAssignment assignment)
    {
      var resultMessage = new List<string>();
      var stage = assignment.Stageanorsv;
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      
      bool needContractDraft = contractAttGroupAvailable
        && assignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && (assignment.Stageanorsv == null || NeedContractDraftByStageSetting(assignment.Stageanorsv))
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(assignment, Users.As(Sungero.Company.Employees.Current));
      bool contractExist = assignment.ContractGroupanorsv.Contracts.Any();
      bool tsExists = assignment.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Any();
      
      if (needContractDraft && !contractExist)
        resultMessage.Add(anorsv.ApprovalTaskModule.Resources.NoContractForPurchaseMessage);
      
      if (needContractDraft && contractExist && assignment.ContractGroupanorsv.Contracts.FirstOrDefault().IsStandard == true && !tsExists)
        resultMessage.Add(anorsv.ApprovalTaskModule.Resources.NoTsForPurchaseMessage);
      
      var needMemo = contractAttGroupAvailable
        && (assignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
        && (assignment.Stageanorsv == null
            || NeedMemoForPurchaseByStageSetting(assignment.Stageanorsv));
      
      if (needMemo && !assignment.MemoGroupanorsv.Memos.Any())
      {
        resultMessage.Add(anorsv.ApprovalTaskModule.Resources.NoMemoForPurchaseMessage);
      }
      
      return resultMessage.Count > 0 ? string.Join(Environment.NewLine, resultMessage) : string.Empty;
    }
    
    public static string CheckApprovalSimpleAssignmentAttachedDocuments(anorsv.ApprovalTask.IApprovalSimpleAssignment assignment)
    {
      var resultMessage = new List<string>();
      var stage = assignment.Stageanorsv;
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      
      bool needContractDraft = contractAttGroupAvailable
        && assignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && (assignment.Stageanorsv == null || NeedContractDraftByStageSetting(assignment.Stageanorsv))
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(assignment, Users.As(Sungero.Company.Employees.Current));
      bool contractExist = assignment.ContractGroupanorsv.Contracts.Any();
      bool tsExists = assignment.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Any();
      
      if (needContractDraft && !contractExist)
        resultMessage.Add(anorsv.ApprovalTaskModule.Resources.NoContractForPurchaseMessage);
      
      if (needContractDraft && contractExist && assignment.ContractGroupanorsv.Contracts.FirstOrDefault().IsStandard == true && !tsExists)
        resultMessage.Add(anorsv.ApprovalTaskModule.Resources.NoTsForPurchaseMessage);
      
      var needMemo = contractAttGroupAvailable
        && (assignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
        && (assignment.Stageanorsv == null
            || anorsv.ApprovalTaskModule.PublicFunctions.Module.NeedMemoForPurchaseByStageSetting(assignment.Stageanorsv));
      
      if (needMemo && !assignment.MemoGroupanorsv.Memos.Any())
      {
        resultMessage.Add(anorsv.ApprovalTaskModule.Resources.NoMemoForPurchaseMessage);
      }
      
      return resultMessage.Count > 0 ? string.Join(Environment.NewLine, resultMessage) : string.Empty;
    }
    
    public static string CheckApprovalReworkAssignmentAttachedDocuments(anorsv.ApprovalTask.IApprovalReworkAssignment assignment)
    {
      var resultMessage = new List<string>();

      // Приказ о введении в действие для ЛНА
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
      var needOrder = (assignment.Status == Sungero.Workflow.AssignmentBase.Status.InProcess)
        && (anorsv.DocflowModule.LNAs.Is(assignment.DocumentGroup.OfficialDocuments.FirstOrDefault()))
        && (lnaGenActivitiesKind != null && assignment.DocumentGroup.OfficialDocuments.FirstOrDefault().DocumentKind.Equals(lnaGenActivitiesKind));
      if (needOrder)
      {
        if (orderAttGroupAvailable == true && !assignment.OrderGroupanorsv.Orders.Any())
        {
          resultMessage.Add(anorsv.ApprovalTaskModule.Resources.NoOrderImplLNAMessage);
        }
        else if (orderAttGroupAvailable == false)
        {
          var lna = anorsv.DocflowModule.LNAs.As(assignment.DocumentGroup.OfficialDocuments.FirstOrDefault());
          var orderImplLNAKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
          if (orderImplLNAKind != null)
          {
            var allOrderGeneralActivity = sline.RSV.Orders.GetAll()
              .Where(o => lna.Relations.GetRelated(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).Contains(o))
              .Where(o => o.DocumentKind.Equals(orderImplLNAKind))
              .ToList();
            
            // Если нет связи Введен в действие с Приказом о введении в действие ЛНА по общей деятельности организации, не отправлять на согласование ЛНА
            if (!allOrderGeneralActivity.Any() || (allOrderGeneralActivity.Any() && !assignment.OtherGroup.All.Contains(allOrderGeneralActivity.FirstOrDefault())))
            {
              resultMessage.Add(anorsv.ApprovalTaskModule.Resources.NoOrderImplLNAMessage);
            }
          }
        }
      }

      return resultMessage.Count > 0 ? string.Join(Environment.NewLine, resultMessage) : string.Empty;
    }
    
    /// <summary>
    /// Определяет необходимость вложения на этапе Проекта договора или ТЗ.
    /// </summary>
    /// <param name="stage"></param>
    /// <returns>bool</returns>
    [Public]
    public static bool NeedContractDraftByStageSetting(sline.RSV.IApprovalStage stage)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      return (contractAttGroupAvailable && stage.ApprovalPurhaseanorsv.HasValue && stage.ApprovalPurhaseanorsv.Value == true);
    }
    
    /// <summary>
    /// Определяет необходимость вложения на этапе СЗ по закупке.
    /// </summary>
    /// <param name="stage"></param>
    /// <returns>bool</returns>
    [Public]
    public static bool NeedMemoForPurchaseByStageSetting(sline.RSV.IApprovalStage stage)
    {
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      return (contractAttGroupAvailable && stage.NeedAttachMemoanorsv.HasValue && stage.NeedAttachMemoanorsv.Value == true);
    }
    
    #endregion

    #region Проверки на вхождения в роли
    
    /// <summary>
    /// 
    /// </summary>
    [Public]
    public static bool InSiginingTypeChangeRole(IRecipient recipient)
    {
      Guid roleGuid = Constants.Module.RoleGuid.SingingTypeChangeRole;
      bool inRole = recipient.IncludedIn(roleGuid);
      
      return inRole;
    }
    
    [Public]
    public virtual bool UserInTestRole(IRecipient recipient)
    {
      bool result = false;
      IRole testRole  = Roles.GetAll(r => r.Name == "Тестирующие согласование по регламентам").FirstOrDefault();
      
      if (testRole != null)
      {
        result = recipient.IncludedIn(testRole);
      }
      
      return result;
    }
    
    #endregion
    
  }
}