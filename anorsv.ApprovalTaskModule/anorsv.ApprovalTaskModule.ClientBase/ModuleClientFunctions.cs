using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Shared;
using Sungero.Workflow;

namespace anorsv.ApprovalTaskModule.Client
{
  public class ModuleFunctions
  {
    /// <summary>
    /// Диалог по добавлению СЗ по закупке.
    /// </summary>
    /// <param name="document">Документ, для которого создается СЗ.</param>
    [Public]
    public virtual void MemoDialog(Sungero.Docflow.IApprovalTask currentTask, int assignmentId, Sungero.Docflow.IOfficialDocument document)
    {
      var procurementActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ProcurementActivitiesKind);
      var topicList = sline.CustomModule.TopicMemos.GetAll(t => t.DocumentKind.Equals(procurementActivitiesKind)).ToList(); // TODO MED: Потом заменить тематику СЗ на общую по мере их объединения
      var templateMemoList = new List<Sungero.Docflow.IDocumentTemplate>();
      var template = Sungero.Docflow.DocumentTemplates.Null;
      
      // Создать диалог ввода.
      var dialog = Dialogs.CreateInputDialog(anorsv.ApprovalTaskModule.Resources.ChooseMemoTemplate);
      dialog.Width = 600;
      
      // Добавить поле для выбора тематики. TODO MED: Потом заменить тематику СЗ на общую по мере их объединения
      //var topic = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.TopicDialog, true, anorsv.DocflowModule.AnorsvGenericTopics.Null);
      var topic = dialog
        .AddSelect(anorsv.ApprovalTaskModule.Resources.TopicDialog, true, sline.CustomModule.TopicMemos.Null)
        .From(topicList.ToArray());
      var templateDoc = dialog
        .AddSelect(anorsv.ApprovalTaskModule.Resources.MemoTemplate, true, templateMemoList.ToArray().FirstOrDefault())
        .From(templateMemoList.ToArray());
      
      topic.SetOnValueChanged(
        (e) =>
        {
          if (e.NewValue == null)
          {
            templateDoc.Value = null;
            templateDoc.IsEnabled = false;
          }
          else
          {
            templateDoc.IsEnabled = true;
            templateMemoList = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetListTemplatesForMemo(topic.Value.Id);
            templateDoc.From(templateMemoList.ToArray());
            templateDoc.Value = templateMemoList.ToArray().FirstOrDefault();
          }
        });
      
      if (dialog.Show() == DialogButtons.Ok)
      {
        // Создать СЗ из шаблона
        template = templateDoc.Value;
        try
        {
          CreateMemo(currentTask, assignmentId, document, template, topic.Value);
        }
        catch(Exception ex)
        {
          Logger.ErrorFormat("ERROR: Ошибка создания СЗ по закупочной деятельности из карточки задания - " + ex.Message);
        }
      }
      
    }
    
    /// <summary>
    /// Диалог по добавлению технического задания в задание с доработкой.
    /// </summary>
    /// <param name="document">Документ, для которого создаются приложения.</param>
    [Public]
    public virtual void TSDialog(anorsv.ApprovalTask.IApprovalCheckingAssignment currentAssignment, Sungero.Docflow.IOfficialDocument document)
    {
      // Создать диалог ввода.
      var dialog = Dialogs.CreateInputDialog(anorsv.ApprovalTaskModule.Resources.ChooseFormTS);
      // вид "Техническое задание"
      var TSKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
      // список тематик
      var TopicList = anorsv.DocflowModule.AnorsvGenericTopics
        .GetAll()
        .Where(k => k.DocumentKind == TSKind &&
               k.Status == anorsv.DocflowModule.AnorsvGenericTopic.Status.Active)
        .ToList();
      // Добавить поле для выбора тематики.
      var topic = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.TopicDialog, true, anorsv.DocflowModule.AnorsvGenericTopics.Null).From(TopicList);
      // Добавить поле "Предмет ТЗ"
      var subjectTS = dialog.AddString(anorsv.ApprovalTaskModule.Resources.SubjectTS, true).MaxLength(250);
      // Добавить поле для ввода типовой.
      var typical = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.Typical, true).From("Да", "Нет");
      var fileSelector = dialog.AddFileSelect(anorsv.ApprovalTaskModule.Resources.File, false);
      fileSelector.MaxFileSize(Constants.Module.DocDialogMaxFileSize);
      fileSelector.IsVisible  = false;
      var templateTS = Sungero.Docflow.DocumentTemplates.Null;
      var templateDoc = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.ChooseTemplateTS, true, templateTS);
      templateDoc.IsVisible = false;
      var importButton = dialog.Buttons.AddCustom(anorsv.ApprovalTaskModule.Resources.Create);
      var cancelButton = dialog.Buttons.AddCustom(Sungero.Docflow.Resources.Dialog_Close);
      var isImportingTemplates = false;
      var isImportingFile = false;
      var resultDialog = false;
      var viewTemplate = false;
      dialog.Width = 650;
      
      topic.SetOnValueChanged(
        (x) =>
        {
          subjectTS.Value = null;
          typical.Value = null;
          fileSelector.Value = null;
          templateDoc.Value = null;
        });
      
      dialog.SetOnRefresh(
        er =>
        {if (resultDialog == false)
          {
            if (typical.Value == "Да")
            {
              if (topic.Value != null)
              {
                //получить список шаблонов в соответствующей тематике
                var templates = Functions.Module.Remote.GetListTemplates(topic.Value.Id);
                templateDoc.From(templates);
                templateDoc.IsVisible = true;
                //если шаблонов несколько - дать права выбора
                if (templates.Count > 1)
                {
                  if(viewTemplate == false)
                  {
                    // Добавить поле для выбора шаблона.
                    templateDoc.IsVisible = true;
                    isImportingTemplates = true;
                    isImportingFile = false;
                    fileSelector.IsVisible  = false;
                    viewTemplate = true;
                  }
                  
                  fileSelector.IsVisible  = false;
                  fileSelector.IsRequired  = false;
                  templateDoc.IsVisible = viewTemplate;
                }
                else
                {
                  fileSelector.IsVisible  = false;
                  isImportingTemplates = true;
                  isImportingFile = false;
                  templateTS = templates.FirstOrDefault();
                  templateDoc.Value =  templateTS;
                  templateDoc.IsVisible = templates.Count > 0;
                }
              }
            }
            if (typical.Value == "Нет")
            {
              viewTemplate = true;
              templateDoc.IsVisible = false;
              isImportingTemplates = false;
              isImportingFile = true;
              fileSelector.IsVisible  = true;
              fileSelector.IsRequired = true;
              templateDoc.Value = null;
            }
            if (typical.Value == null)
            {
              viewTemplate = false;
              templateDoc.IsVisible = false;
              isImportingTemplates = false;
              isImportingFile = false;
              fileSelector.IsVisible  = false;
              fileSelector.IsRequired = false;
              templateDoc.Value = null;
            }
          }});
      
      
      dialog.SetOnButtonClick(
        b =>
        {
          resultDialog = true;
          if (b.Button == cancelButton)
          {
            topic.IsRequired = false;
            subjectTS.IsRequired = false;
            typical.IsRequired = false;
            fileSelector.IsRequired = false;
            templateDoc.IsRequired = false;
            return;
          }
          if (b.Button == importButton && b.IsValid)
          {
            if (!b.IsValid)
              return;
            try
            {
              if (isImportingTemplates)
              {
                CreateTS(currentAssignment, document, templateTS, topic.Value, subjectTS.Value);
              }
            }
            catch(Exception ex)
            {
              Logger.ErrorFormat(ex.Message);
            }
            try
            {
              if (isImportingFile)
              {
                CreateTSFromFile(currentAssignment, fileSelector.Value.Name, document, fileSelector.Value.Content, topic.Value, subjectTS.Value);
              }
            }
            catch(Exception ex)
            {
              Logger.ErrorFormat(ex.Message);
            }
          }
        });
      
      dialog.Show();
      
      
    }
    /// <summary>
    /// Диалог по добавлению проекта договора.
    /// </summary>
    /// <param name="document">Документ, для которого создаются приложения.</param>
    [Public]
    public virtual void DraftContractDialog(anorsv.ApprovalTask.IApprovalCheckingAssignment currentAssignment, Sungero.Docflow.IOfficialDocument document)
    {
      // Создать диалог ввода.
      var dialog = Dialogs.CreateInputDialog(anorsv.ApprovalTaskModule.Resources.ChoozeFromContract);
      // вид "Расходные договоры"
      var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
      // список тематик
      var TopicList = anorsv.DocflowModule.AnorsvGenericTopics
        .GetAll()
        .Where(k => k.DocumentKind == expendableContractKind &&
               k.Status == anorsv.DocflowModule.AnorsvGenericTopic.Status.Active)
        .ToList();
      // Добавить поле для выбора тематики.
      var topic = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.TopicDialog, true, anorsv.DocflowModule.AnorsvGenericTopics.Null).From(TopicList);
      // Добавить поле "Предмет договора"
      var subjectContract = dialog.AddString(anorsv.ApprovalTaskModule.Resources.SubjectContract, true).MaxLength(250);
      // Добавить поле для ввода типовой.
      var typical = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.Typical, true).From("Да", "Нет");
      var fileSelector = dialog.AddFileSelect(anorsv.ApprovalTaskModule.Resources.File, false);
      fileSelector.MaxFileSize(Constants.Module.DocDialogMaxFileSize);
      fileSelector.IsVisible  = false;
      var templateContract = Sungero.Docflow.DocumentTemplates.Null;
      var templateDoc = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.ChooseTemplate, true, templateContract);
      templateDoc.IsVisible = false;
      var importButton = dialog.Buttons.AddCustom(anorsv.ApprovalTaskModule.Resources.Create);
      var cancelButton = dialog.Buttons.AddCustom(Sungero.Docflow.Resources.Dialog_Close);
      var isImportingTemplates = false;
      var isImportingFile = false;
      var resultDialog = false;
      var viewTemplate = false;
      dialog.Width = 600;
      
      
      topic.SetOnValueChanged(
        (x) =>
        {
          subjectContract.Value = null;
          typical.Value = null;
          fileSelector.Value = null;
          templateDoc.Value = null;
        });
      
      dialog.SetOnRefresh(
        er =>
        {if (resultDialog == false)
          {
            if (typical.Value == "Да")
            {
              
              if (topic.Value != null)
              {
                //получить список шаблонов в соответствующей тематике
                var templates = Functions.Module.Remote.GetListTemplates(topic.Value.Id);
                templateDoc.From(templates);
                
                //если шаблонов несколько - дать права выбора
                if (templates.Count > 1)
                {
                  if(viewTemplate == false)
                  {
                    // Добавить поле для выбора шаблона.
                    templateDoc.IsVisible = true;
                    isImportingTemplates = true;
                    isImportingFile = false;
                    templateDoc.SetOnValueChanged(
                      (x) =>
                      {
                        if (x.NewValue == null)
                        {
                          templateContract = null;
                        }
                        else
                        {
                          templateContract = templateDoc.Value;
                        }
                      });
                    
                    fileSelector.IsVisible  = false;
                    viewTemplate = true;
                  }
                  
                  fileSelector.IsVisible  = false;
                  fileSelector.IsRequired  = false;
                  templateDoc.IsVisible = viewTemplate;
                }
                else
                {
                  fileSelector.IsVisible  = false;
                  isImportingTemplates = true;
                  isImportingFile = false;
                  templateContract = templates.FirstOrDefault();
                  templateDoc.Value = templateContract;
                  templateDoc.IsVisible = templates.Any();
                }
              }
            }
            if (typical.Value == "Нет")
            {
              viewTemplate = false;
              templateDoc.IsVisible = false;
              isImportingTemplates = false;
              isImportingFile = true;
              fileSelector.IsVisible  = true;
              fileSelector.IsRequired = true;
              templateDoc.Value = null;
            }
            if (typical.Value == null)
            {
              viewTemplate = false;
              templateDoc.IsVisible = false;
              isImportingTemplates = false;
              isImportingFile = false;
              fileSelector.IsVisible  = false;
              fileSelector.IsRequired = false;
              templateDoc.Value = null;
            }
            
          }});
      
      
      dialog.SetOnButtonClick(
        b =>
        {
          resultDialog = true;
          if (b.Button == cancelButton)
          {
            topic.IsRequired = false;
            subjectContract.IsRequired = false;
            typical.IsRequired = false;
            fileSelector.IsRequired = false;
            templateDoc.IsRequired = false;
            return;
          }
          if (b.Button == importButton && b.IsValid)
          {
            if (!b.IsValid)
              return;
            try
            {
              if (isImportingTemplates)
              {
                CreateContract(currentAssignment, document, templateContract, topic.Value, subjectContract.Value);
              }
            }
            catch(Exception ex)
            {
              Logger.ErrorFormat(ex.Message);
            }
            try
            {
              if (isImportingFile)
              {
                CreateContractFromFile(currentAssignment, fileSelector.Value.Name, document, fileSelector.Value.Content, topic.Value, subjectContract.Value);
              }
            }
            catch(Exception ex)
            {
              Logger.ErrorFormat(ex.Message);
            }
          }
          
        });
      
      dialog.Show();
    }
    
    /// <summary>
    /// Создать договор из файла в задании с доработкой.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="addendumName">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="addendumContent">Тело документа.</param>
    public virtual void CreateContractFromFile(anorsv.ApprovalTask.IApprovalCheckingAssignment currentAssignment, string contractFileName, Sungero.Docflow.IOfficialDocument leadingDocument, byte[] contractFileContent, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectContract)
    {
      var IsStandard = false;
      var contractFile = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateContract(leadingDocument, topic, subjectContract, IsStandard);
      var error = false;
      try
      {
        currentAssignment.ContractGroupanorsv.Contracts.Add(contractFile);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
        error = true;
      }
      if (error == false)
      {
        contractFile.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        contractFile.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, contractFile.Id);

        using (var fileStream = new System.IO.MemoryStream(contractFileContent))
        {
          contractFile.CreateVersionFrom(fileStream, System.IO.Path.GetExtension(contractFileName));
          contractFile.Save();
          Logger.DebugFormat("contractFile: After creating contractFile contractFileid = {1}",  contractFile.Id);
        }
      }
    }

    /// <summary>
    /// Создать проект контракта по шаблону.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="template">Шаблон.</param>
    /// <param name="topic">Тематика.</param>
    /// <param name="subjectContract">Предмет.</param>
    public virtual void  CreateContract(anorsv.ApprovalTask.IApprovalCheckingAssignment currentAssignment, Sungero.Docflow.IOfficialDocument leadingDocument, Sungero.Docflow.IDocumentTemplate template, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectContract)
    {
      var IsStandard = true;
      var contract = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateContract(leadingDocument, topic, subjectContract, IsStandard);
      try
      {
        currentAssignment.ContractGroupanorsv.Contracts.Add(contract);
        //contract.Relations.AddFrom(sline.CustomModule.Resources.ContractForPurchaseRelationName, leadingDocument);
        contract.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        contract.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, contract.Id);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
      }
      
      using (var body = template.LastVersion.Body.Read())
      {
        var newVersion = contract.CreateVersionFrom(body, template.AssociatedApplication.Extension);
        var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)contract;
        internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
        contract.Save();
      }
    }
    
    /// <summary>
    /// Создать ТЗ из файла.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="fileNameTS">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="fileContentTS">Тело документа.</param>
    /// <param name="topic">Тематика.</param>
    /// <param name="string subjectTS">Предмет ТЗ.</param>
    public virtual void CreateTSFromFile(anorsv.ApprovalTask.IApprovalCheckingAssignment currentAssignment, string fileNameTS, Sungero.Docflow.IOfficialDocument leadingDocument, byte[] fileContentTS, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectTS)
    {
      var IsStandard = false;
      var docTSFile = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateCardTS(leadingDocument, topic, subjectTS, IsStandard);
      var error = false;
      try
      {
        currentAssignment.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTSFile);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
        error = true;
      }
      if (error == false)
      {
        docTSFile.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        docTSFile.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, docTSFile.Id);
        using (var fileStream = new System.IO.MemoryStream(fileContentTS))
        {
          docTSFile.CreateVersionFrom(fileStream, System.IO.Path.GetExtension(fileNameTS));
          docTSFile.Save();
          Logger.DebugFormat("docTSFile: After creating docTSFile docTSFilid = {1}",  docTSFile.Id);
        }
        
      }
      
    }

    /// <summary>
    /// Создать ТЗ по шаблону.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="template">Шаблон.</param>
    /// <param name="topic">Тематика.</param>
    /// <param name="subjectTS">Предмет ТЗ.</param>
    public virtual void  CreateTS(anorsv.ApprovalTask.IApprovalCheckingAssignment currentAssignment, Sungero.Docflow.IOfficialDocument leadingDocument, Sungero.Docflow.IDocumentTemplate template, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectTS)
    {
      var IsStandard = true;
      var docTS = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateCardTS(leadingDocument, topic, subjectTS, IsStandard);
      try
      {
        currentAssignment.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTS);
        docTS.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        docTS.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, docTS.Id);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
      }
      using (var body = template.LastVersion.Body.Read())
      {
        var newVersion = docTS.CreateVersionFrom(body, template.AssociatedApplication.Extension);
        var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)docTS;
        internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
        docTS.Save();
      }
      
    }

    /// <summary>
    /// Диалог по добавлению технического задания в простое задание.
    /// </summary>
    /// <param name="document">Документ, для которого создаются приложения.</param>
    [Public]
    public virtual bool TSDialogForSimple(anorsv.ApprovalTask.IApprovalSimpleAssignment currentAssignment, Sungero.Docflow.IOfficialDocument document)
    {
      // Создать диалог ввода.
      var dialog = Dialogs.CreateInputDialog(anorsv.ApprovalTaskModule.Resources.ChooseFormTS);
      // вид "Техническое задание"
      var TSKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
      // список тематик
      var TopicList = anorsv.DocflowModule.AnorsvGenericTopics.GetAll().Where(k => k.DocumentKind == TSKind &&
                                                                              k.Status == anorsv.DocflowModule.AnorsvGenericTopic.Status.Active).ToList();
      // Добавить поле для выбора тематики.
      var topic = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.TopicDialog, true, anorsv.DocflowModule.AnorsvGenericTopics.Null).From(TopicList);
      // Добавить поле "Предмет ТЗ"
      var subjectTS = dialog.AddString(anorsv.ApprovalTaskModule.Resources.SubjectTS, true).MaxLength(250);
      // Добавить поле для ввода типовой.
      var typical = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.Typical, true).From("Да", "Нет");
      var fileSelector = dialog.AddFileSelect(anorsv.ApprovalTaskModule.Resources.File, false);
      fileSelector.MaxFileSize(Constants.Module.DocDialogMaxFileSize);
      fileSelector.IsVisible  = false;
      var templateTS = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTemplate();
      var templateDoc = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.ChooseTemplateTS, true, templateTS);
      templateDoc.IsVisible = false;
      var importButton = dialog.Buttons.AddCustom(anorsv.ApprovalTaskModule.Resources.Create);
      var cancelButton = dialog.Buttons.AddCustom(Sungero.Docflow.Resources.Dialog_Close);
      var isImportingTemplates = false;
      var isImportingFile = false;
      var resultDialog = false;
      var viewTemplate = false;
      dialog.Width = 650;
      
      topic.SetOnValueChanged((x) =>
                              {
                                subjectTS.Value = null;
                                typical.Value = null;
                                fileSelector.Value = null;
                                templateDoc.Value = null;
                              });
      
      dialog.SetOnRefresh(er =>
                          {if (resultDialog == false)
                            {
                              if (typical.Value == "Да")
                              {
                                if (topic.Value != null)
                                {
                                  //получить список шаблонов в соответствующей тематике
                                  var templates = Functions.Module.Remote.GetListTemplates(topic.Value.Id);
                                  //если шаблонов несколько - дать права выбора
                                  if (templates.Count > 1)
                                  {
                                    if(viewTemplate == false)
                                    {
                                      // Добавить поле для выбора шаблона.
                                      templateDoc = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.ChooseTemplateTS, true, Sungero.Docflow.DocumentTemplates.Null).From(templates.ToList());
                                      isImportingTemplates = true;
                                      isImportingFile = false;
                                      templateDoc.SetOnValueChanged((x) =>
                                                                    {
                                                                      if (x.NewValue == null)
                                                                      {
                                                                        templateTS = null;
                                                                      }
                                                                      else
                                                                      {
                                                                        templateTS = templateDoc.Value;
                                                                      }
                                                                    });
                                      
                                      
                                      
                                      fileSelector.IsVisible  = false;
                                      viewTemplate = true;
                                    }
                                    
                                    fileSelector.IsVisible  = false;
                                    fileSelector.IsRequired  = false;
                                    templateDoc.IsVisible = viewTemplate;
                                    
                                  }
                                  else
                                  {
                                    templateDoc.IsVisible = false;
                                    fileSelector.IsVisible  = false;
                                    isImportingTemplates = true;
                                    isImportingFile = false;
                                    templateTS = templates.FirstOrDefault();
                                  }
                                }
                                else
                                {
                                  templateDoc.IsVisible = false;
                                  fileSelector.IsVisible  = false;
                                  fileSelector.IsRequired  = false;
                                  isImportingTemplates = true;
                                  isImportingFile = false;
                                }
                              }
                              if (typical.Value == "Нет")
                              {
                                viewTemplate = true;
                                templateDoc.IsVisible = false;
                                isImportingTemplates = false;
                                isImportingFile = true;
                                fileSelector.IsVisible  = true;
                                fileSelector.IsRequired = true;
                              }
                              if (typical.Value == null)
                              {
                                viewTemplate = false;
                                templateDoc.IsVisible = false;
                                isImportingTemplates = false;
                                isImportingFile = false;
                                fileSelector.IsVisible  = false;
                                fileSelector.IsRequired = false;
                              }
                            }});
      
      
      dialog.SetOnButtonClick(b =>
                              {
                                resultDialog = true;
                                if (b.Button == cancelButton)
                                {
                                  topic.IsRequired = false;
                                  subjectTS.IsRequired = false;
                                  typical.IsRequired = false;
                                  fileSelector.IsRequired = false;
                                  templateDoc.IsRequired = false;
                                  return;
                                }
                                if (b.Button == importButton && b.IsValid)
                                {
                                  if (!b.IsValid)
                                    return;
                                  try
                                  {
                                    if (isImportingTemplates)
                                    {
                                      CreateTSSimple(currentAssignment, document, templateTS, topic.Value, subjectTS.Value);
                                    }
                                  }
                                  catch(Exception ex)
                                  {
                                    Logger.ErrorFormat(ex.Message);
                                  }
                                  try
                                  {
                                    if (isImportingFile)
                                    {
                                      CreateTSFromFileSimple(currentAssignment, fileSelector.Value.Name, document, fileSelector.Value.Content, topic.Value, subjectTS.Value);
                                    }
                                  }
                                  catch(Exception ex)
                                  {
                                    Logger.ErrorFormat(ex.Message);
                                  }
                                }
                              });
      
      if (dialog.Show() == importButton)
        return true;
      else
        return false;
    }
    
    /// <summary>
    /// Диалог по добавлению проекта договора в простом задании.
    /// </summary>
    /// <param name="document">Документ, для которого создаются приложения.</param>
    [Public]
    public virtual bool DraftContractDialogForSimple(anorsv.ApprovalTask.IApprovalSimpleAssignment currentAssignment, Sungero.Docflow.IOfficialDocument document)
    {
      // Создать диалог ввода.
      var dialog = Dialogs.CreateInputDialog(anorsv.ApprovalTaskModule.Resources.ChoozeFromContract);
      // вид "Расходные договоры"
      var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
      // список тематик
      var TopicList = anorsv.DocflowModule.AnorsvGenericTopics.GetAll().Where(k => k.DocumentKind == expendableContractKind &&
                                                                              k.Status == anorsv.DocflowModule.AnorsvGenericTopic.Status.Active).ToList();
      // Добавить поле для выбора тематики.
      var topic = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.TopicDialog, true, anorsv.DocflowModule.AnorsvGenericTopics.Null).From(TopicList);
      // Добавить поле "Предмет договора"
      var subjectContract = dialog.AddString(anorsv.ApprovalTaskModule.Resources.SubjectContract, true).MaxLength(250);
      // Добавить поле для ввода типовой.
      var typical = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.Typical, true).From("Да", "Нет");
      var fileSelector = dialog.AddFileSelect(anorsv.ApprovalTaskModule.Resources.File, false);
      fileSelector.MaxFileSize(Constants.Module.DocDialogMaxFileSize);
      fileSelector.IsVisible  = false;
      var templateContract = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTemplate();
      var templateDoc = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.ChooseTemplate, true, templateContract);
      templateDoc.IsVisible = false;
      var importButton = dialog.Buttons.AddCustom(anorsv.ApprovalTaskModule.Resources.Create);
      var cancelButton = dialog.Buttons.AddCustom(Sungero.Docflow.Resources.Dialog_Close);
      var isImportingTemplates = false;
      var isImportingFile = false;
      var resultDialog = false;
      var viewTemplate = false;
      dialog.Width = 600;
      
      topic.SetOnValueChanged((x) =>
                              {
                                subjectContract.Value = null;
                                typical.Value = null;
                                fileSelector.Value = null;
                                templateDoc.Value = null;
                              });
      
      dialog.SetOnRefresh(er =>
                          {
                            if (resultDialog == false)
                            {
                              if (typical.Value == "Да")
                              {
                                if (topic.Value != null)
                                {
                                  //получить список шаблонов в соответствующей тематике
                                  var templates = Functions.Module.Remote.GetListTemplates(topic.Value.Id);
                                  //если шаблонов несколько - дать права выбора
                                  if (templates.Count > 1)
                                  {
                                    if(viewTemplate == false)
                                    {
                                      // Добавить поле для выбора шаблона.
                                      templateDoc = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.ChooseTemplate, true, Sungero.Docflow.DocumentTemplates.Null).From(templates.ToList());
                                      isImportingTemplates = true;
                                      isImportingFile = false;
                                      templateDoc.SetOnValueChanged((x) =>
                                                                    {
                                                                      if (x.NewValue == null)
                                                                      {
                                                                        templateContract = null;
                                                                      }
                                                                      else
                                                                      {
                                                                        templateContract = templateDoc.Value;
                                                                      }
                                                                    });
                                      
                                      fileSelector.IsVisible  = false;
                                      viewTemplate = true;
                                    }
                                    
                                    fileSelector.IsVisible  = false;
                                    fileSelector.IsRequired  = false;
                                    templateDoc.IsVisible = viewTemplate;
                                    
                                  }
                                  else
                                  {
                                    templateDoc.IsVisible = false;
                                    fileSelector.IsVisible  = false;
                                    fileSelector.IsRequired  = false;
                                    isImportingTemplates = true;
                                    isImportingFile = false;
                                    templateContract = templates.FirstOrDefault();
                                  }
                                }
                                else
                                {
                                  templateDoc.IsVisible = false;
                                  fileSelector.IsVisible  = false;
                                  fileSelector.IsRequired  = false;
                                  isImportingTemplates = true;
                                  isImportingFile = false;
                                }
                              }
                              if (typical.Value == "Нет")
                              {
                                viewTemplate = true;
                                templateDoc.IsVisible = false;
                                isImportingTemplates = false;
                                isImportingFile = true;
                                fileSelector.IsVisible  = true;
                                fileSelector.IsRequired = true;
                              }
                              if (typical.Value == null)
                              {
                                viewTemplate = false;
                                templateDoc.IsVisible = false;
                                isImportingTemplates = false;
                                isImportingFile = false;
                                fileSelector.IsVisible  = false;
                                fileSelector.IsRequired = false;
                              }
                            }});
      
      
      dialog.SetOnButtonClick(b =>
                              {
                                resultDialog = true;
                                if (b.Button == cancelButton)
                                {
                                  topic.IsRequired = false;
                                  subjectContract.IsRequired = false;
                                  typical.IsRequired = false;
                                  fileSelector.IsRequired = false;
                                  templateDoc.IsRequired = false;
                                  return;
                                }
                                if (b.Button == importButton && b.IsValid)
                                {
                                  if (!b.IsValid)
                                    return;
                                  try
                                  {
                                    if (isImportingTemplates)
                                    {
                                      CreateContractSimple(currentAssignment, document, templateContract, topic.Value, subjectContract.Value);
                                    }
                                  }
                                  catch(Exception ex)
                                  {
                                    Logger.ErrorFormat(ex.Message);
                                  }
                                  try
                                  {
                                    if (isImportingFile)
                                    {
                                      CreateContractFromFileSimple(currentAssignment, fileSelector.Value.Name, document, fileSelector.Value.Content, topic.Value, subjectContract.Value);
                                    }
                                  }
                                  catch(Exception ex)
                                  {
                                    Logger.ErrorFormat(ex.Message);
                                  }
                                }
                              });
      
      if (dialog.Show() == importButton)
        return true;
      else
        return false;
    }
    
    /// <summary>
    /// Создать договор из файла.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="addendumName">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="addendumContent">Тело документа.</param>
    public virtual void CreateContractFromFileSimple(anorsv.ApprovalTask.IApprovalSimpleAssignment currentAssignment, string contractFileName, Sungero.Docflow.IOfficialDocument leadingDocument, byte[] contractFileContent, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectContract)
    {
      var IsStandard = false;
      var contractFile = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateContract(leadingDocument, topic, subjectContract, IsStandard);
      var error = false;
      try
      {
        currentAssignment.ContractGroupanorsv.Contracts.Add(contractFile);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
        error = true;
      }
      if (error == false)
      {
        //contractFile.Relations.AddFrom(sline.CustomModule.Resources.ContractForPurchaseRelationName, leadingDocument);
        contractFile.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        contractFile.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, contractFile.Id);

        using (var fileStream = new System.IO.MemoryStream(contractFileContent))
        {
          contractFile.CreateVersionFrom(fileStream, System.IO.Path.GetExtension(contractFileName));
          contractFile.Save();
          Logger.DebugFormat("contractFile: After creating contractFile contractFileid = {1}",  contractFile.Id);
        }
      }
      
    }
    
    /// <summary>
    /// Создать проект контракта по шаблону.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="template">Шаблон.</param>
    /// <param name="subjectContract">Предмет.</param>
    /// /// <param name="topic">Тематика.</param>
    public virtual void  CreateContractSimple(anorsv.ApprovalTask.IApprovalSimpleAssignment currentAssignment, Sungero.Docflow.IOfficialDocument leadingDocument, Sungero.Docflow.IDocumentTemplate template, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectContract)
    {
      var IsStandard = true;
      var contract = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateContract(leadingDocument, topic, subjectContract, IsStandard);
      try
      {
        currentAssignment.ContractGroupanorsv.Contracts.Add(contract);
        //contract.Relations.AddFrom(sline.CustomModule.Resources.ContractForPurchaseRelationName, leadingDocument);
        contract.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        contract.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, contract.Id);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
      }
      
      using (var body = template.LastVersion.Body.Read())
      {
        var newVersion = contract.CreateVersionFrom(body, template.AssociatedApplication.Extension);
        var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)contract;
        internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
        contract.Save();
      }
      
    }
    
    /// <summary>
    /// Создать ТЗ из файла.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="addendumName">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="addendumContent">Тело документа.</param>
    public virtual void CreateTSFromFileSimple(anorsv.ApprovalTask.IApprovalSimpleAssignment currentAssignment, string contractFileName, Sungero.Docflow.IOfficialDocument leadingDocument, byte[] contractFileContent, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectTS)
    {
      var IsStandard = false;
      var docTSFile = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateCardTS(leadingDocument, topic, subjectTS, IsStandard);
      var error = false;
      try
      {
        currentAssignment.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTSFile);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
        error = true;
      }
      if (error == false)
      {
        docTSFile.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        docTSFile.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, docTSFile.Id);
        using (var fileStream = new System.IO.MemoryStream(contractFileContent))
        {
          docTSFile.CreateVersionFrom(fileStream, System.IO.Path.GetExtension(contractFileName));
          docTSFile.Save();
          Logger.DebugFormat("docTSFile: After creating docTSFile docTSFilid = {1}",  docTSFile.Id);
        }
      }
      
    }

    /// <summary>
    /// Создать ТЗ по шаблону.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="addendumName">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="addendumContent">Тело документа.</param>
    public virtual void  CreateTSSimple(anorsv.ApprovalTask.IApprovalSimpleAssignment currentAssignment, Sungero.Docflow.IOfficialDocument leadingDocument, Sungero.Docflow.IDocumentTemplate template, anorsv.DocflowModule.IAnorsvGenericTopic topic, string subjectTS)
    {
      var IsStandard = true;
      var docTS = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateCardTS(leadingDocument, topic, subjectTS, IsStandard);
      try
      {
        currentAssignment.TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTS);
        //docTS.Relations.AddFrom(sline.CustomModule.Resources.TSForPurchaseRelationName, leadingDocument);
        docTS.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        docTS.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, docTS.Id);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
      }
      using (var body = template.LastVersion.Body.Read())
      {
        var newVersion = docTS.CreateVersionFrom(body, template.AssociatedApplication.Extension);
        var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)docTS;
        internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
        docTS.Save();
      }
      
    }

    /// <summary>
    /// Создать СЗ по закупке по шаблону.
    /// </summary>
    /// <param name="currentTask">Задача согласования ведущего документа.</param>
    /// <param name="assignmentId">ИД текущего задания.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="template">Шаблон.</param>
    /// <param name="topic">Тематика СЗ.</param>
    public virtual void CreateMemo(Sungero.Docflow.IApprovalTask currentTask, int assignmentId, Sungero.Docflow.IOfficialDocument leadingDocument, Sungero.Docflow.IDocumentTemplate template, sline.CustomModule.ITopicMemo topic)
    {
      // TODO MED: Потом заменить тематику СЗ на общую по мере их объединения
      //var memo = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateMemo(leadingDocument, topic);
      var docPurchase = sline.CustomModule.PurchaseRequests.As(leadingDocument);
      var memo = sline.RSV.Memos.Create();
      memo.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.ProcurementActivitiesKind);
      memo.MemoTopicanorsv = topic; // TODO MED: Потом заменить тематику СЗ на общую по мере их объединения
      if (docPurchase != null)
      {
        memo.Purchaseanorsv = docPurchase;
        memo.LeadingDocument = docPurchase;
      }
      
      // Создадим тело документа из шаблона
      using (var body = template.LastVersion.Body.Read())
      {
        var newVersion = memo.CreateVersionFrom(body, template.AssociatedApplication.Extension);
        var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)memo;
        internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
      }
      
      memo.ShowModal();
      
      // Если СЗ сохранили
      if (memo != null)
      {
        // Установим связь с Закупкой
        memo.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        memo.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, memo.Id);
        
        // Вложим СЗ в задачу по закупке
        try
        {
          var appCheckingAssignment = anorsv.ApprovalTask.ApprovalCheckingAssignments
            .GetAll(a => a.Id == assignmentId).FirstOrDefault();
          var appSimpleAssignment = anorsv.ApprovalTask.ApprovalSimpleAssignments
            .GetAll(a => a.Id == assignmentId).FirstOrDefault();
          var approvalTask = anorsv.ApprovalTask.ApprovalTasks.As(currentTask);
          
          if (appCheckingAssignment != null)
          {
            appCheckingAssignment.MemoGroupanorsv.Memos.Add(memo);
            appCheckingAssignment.Save();
          }
          else if (appSimpleAssignment != null)
          {
            appSimpleAssignment.MemoGroupanorsv.Memos.Add(memo);
            appSimpleAssignment.Save();
          }
          else if (approvalTask != null)
          {
            approvalTask.MemoGroupanorsv.Memos.Add(memo);
            approvalTask.Save();
          }
        }
        catch(Exception ex)
        {
          Logger.ErrorFormat(ex.Message);
        }
      }
    }

    /// <summary>
    /// Создать СЗ по закупке по шаблону без диалога.
    /// </summary>
    /// <param name="currentTask">Задача согласования ведущего документа.</param>
    /// <param name="assignmentId">ИД текущего задания.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    [Public]
    public virtual void CreateMemoWithoutDialog(Sungero.Docflow.IApprovalTask currentTask, int assignmentId, Sungero.Docflow.IOfficialDocument leadingDocument)
    {
      var docPurchase = sline.CustomModule.PurchaseRequests.As(leadingDocument);
      if (docPurchase != null)
      {
        var memoType = Sungero.Docflow.DocumentTypes.GetAll(t => t.DocumentTypeGuid == anorsv.DocflowModule.PublicConstants.Module.DocTypes.MemoTypeGUID.ToString()).FirstOrDefault();
        var memoKind = anorsv.AnorsvMainSolution.DocumentKinds.Null;
        var baseTopic = anorsv.AnorsvCoreModule.AnorsvTopicBases.Null;
        var memoTopic = sline.CustomModule.TopicMemos.Null;
        var memoTemplate = Sungero.Docflow.DocumentTemplates.Null;
        
        // Найдем доступные для создания из задач по согласовнию закупок СЗ
        var purchaseKindLinkList = anorsv.DocflowModule.DocKindAndTopicLinks
          .GetAll(l => l.MainDocKind.Equals(docPurchase.DocumentKind)
                  && l.CreatedDocKind.DocumentType.Equals(memoType)
                  && l.FromAppTask == true).ToList();
        
        if (purchaseKindLinkList.Count() > 1)
          purchaseKindLinkList = purchaseKindLinkList.Where(l => l.MainDocTopic != null).ToList();

        if (purchaseKindLinkList.Count() == 0)
        {
          Dialogs.ShowMessage(ApprovalTaskModule.Resources.NoDocKindAndTopicLinkText, MessageType.Error);
          return;
        }
        else
        {
          var purchaseKindLink = purchaseKindLinkList.FirstOrDefault();
          memoKind = purchaseKindLink.CreatedDocKind;
          baseTopic = purchaseKindLink.CreatedDocTopic;
          var generalTopic = anorsv.DocflowModule.AnorsvGenericTopics.As(baseTopic);
          if (generalTopic != null)
          {
            // TODO MED: Потом заменить тематику СЗ на общую по мере их объединения
            // memoTemplate =  Functions.Module.Remote.GetListTemplates(generalTopic.Id).FirstOrDefault();
            memoTopic = generalTopic.TopicMemo;
            if (memoTopic != null)
            {
              memoTemplate = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetListTemplatesForMemo(memoTopic.Id).FirstOrDefault();
              if (memoTemplate == null)
              {
                Dialogs.ShowMessage(ApprovalTaskModule.Resources.NoTemplateForCreatingText, MessageType.Error);
                return;
              }
            }
            else
            {
              Dialogs.ShowMessage(ApprovalTaskModule.Resources.NoMemoTopicForCreatingText, MessageType.Error);
              return;
            }
          }
          else
          {
            Dialogs.ShowMessage(ApprovalTaskModule.Resources.NoGeneralTopicForCreatingText, MessageType.Error);
            return;
          }
        }
        
        if (memoKind != null && memoTopic != null && memoTemplate != null)
        {
          var memo = sline.RSV.Memos.Create();
          memo.DocumentKind = memoKind; // Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.ProcurementActivitiesKind);
          memo.MemoTopicanorsv = memoTopic; // TODO MED: Потом заменить тематику СЗ на общую по мере их объединения
          memo.Purchaseanorsv = docPurchase;
          memo.LeadingDocument = docPurchase;
          
          // Создадим тело документа из шаблона
          using (var body = memoTemplate.LastVersion.Body.Read())
          {
            var newVersion = memo.CreateVersionFrom(body, memoTemplate.AssociatedApplication.Extension);
            var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)memo;
            internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = memoTemplate.Id;
          }
          
          memo.ShowModal();
          
          // Если СЗ сохранили
          if (memo != null)
          {
            // Установим связь с Закупкой
            // Создание Простой связи с последующей программной заменой на закрытую спец.связь
            memo.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
            memo.Save();
            anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, memo.Id);
            
            // Вложим СЗ в задачу по закупке
            try
            {
              var appCheckingAssignment = anorsv.ApprovalTask.ApprovalCheckingAssignments.GetAll(a => a.Id == assignmentId).FirstOrDefault();
              var appSimpleAssignment = anorsv.ApprovalTask.ApprovalSimpleAssignments.GetAll(a => a.Id == assignmentId).FirstOrDefault();
              var approvalTask = anorsv.ApprovalTask.ApprovalTasks.As(currentTask);
              
              if (appCheckingAssignment != null)
              {
                appCheckingAssignment.MemoGroupanorsv.Memos.Add(memo);
                appCheckingAssignment.Save();
              }
              else if (appSimpleAssignment != null)
              {
                appSimpleAssignment.MemoGroupanorsv.Memos.Add(memo);
                appSimpleAssignment.Save();
              }
              else if (approvalTask != null)
              {
                approvalTask.MemoGroupanorsv.Memos.Add(memo);
                approvalTask.Save();
              }
            }
            catch(Exception ex)
            {
              Logger.ErrorFormat(ex.Message);
            }
          }
        }
      }
      else
      {
        // В остальных случаях нет заданного шаблона
        
      }
    }
    
    /// <summary>
    /// Создать Приказ к ЛНА без диалога.
    /// </summary>
    /// <param name="currentTask">Задача согласования ведущего документа.</param>
    /// <param name="assignmentId">ИД текущего задания.</param>
    /// <param name="leadingDocument">Согласуемый документ.</param>
    [Public]
    public virtual void CreateOrderWithoutDialog(Sungero.Docflow.IApprovalTask currentTask, int assignmentId, Sungero.Docflow.IOfficialDocument approvingDocument)
    {
      var docLNA = anorsv.DocflowModule.LNAs.As(approvingDocument);
      if (docLNA != null)
      {
        // Создать приказ
        var order = sline.RSV.Orders.Create();
        order.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
        order.BusinessUnit = docLNA.BusinessUnit;
        order.OurSignatory = docLNA.OurSignatory;
        order.Project = docLNA.Project;
        order.SignESanorsv = docLNA.SignESanorsv;
        order.Subject = docLNA.Subject;
        // Связать приказ с лна
        //order.Relations.AddFrom(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName, _obj);
        
        order.ShowModal();
        
        // Если Приказ сохранили
        if (order != null)
        {
          // В ЛНА укажем созданный Приказ как ведущий документ
          docLNA.LeadingDocument = order;
          docLNA.Orderanorsv = order;
          docLNA.Save();
          // Свяжем ЛНА и Приказ спец.связью
          docLNA.Relations.Add(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, order);
          docLNA.Relations.Save();
          anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(docLNA.Id, order.Id);
          
          // Вложим Приказ в задачу по ЛНА
          try
          {
            bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
            var appReworkAssignment = anorsv.ApprovalTask.ApprovalReworkAssignments.GetAll(a => a.Id == assignmentId).FirstOrDefault();
            var approvalTask = anorsv.ApprovalTask.ApprovalTasks.As(currentTask);
            
            if (appReworkAssignment != null)
            {
              if (orderAttGroupAvailable == true)
                appReworkAssignment.OrderGroupanorsv.Orders.Add(order);
              else
                appReworkAssignment.OtherGroup.All.Add(order);
              appReworkAssignment.Save();
            }
            else if (approvalTask != null)
            {
              if (orderAttGroupAvailable == true)
                approvalTask.OrderGroupanorsv.Orders.Add(order);
              else
                approvalTask.OtherGroup.All.Add(order);
              approvalTask.Save();
            }
          }
          catch(Exception ex)
          {
            Logger.ErrorFormat(ex.Message);
          }
        }
      }
    }
    
    #region Универсальный вариант диалогов
    
    #region Диалог создания Договора
    /// <summary>
    /// Диалог по добавлению проекта договора в простом задании.
    /// </summary>
    /// <param name="document">Документ, для которого создаются приложения.</param>
    [Public]
    public virtual bool DraftContractDialog(Sungero.Workflow.IAssignmentBase assignment, Sungero.Docflow.IOfficialDocument document)
    {
      // Создать диалог ввода.
      var dialog = Dialogs.CreateInputDialog(anorsv.ApprovalTaskModule.Resources.ChoozeFromContract);
      // вид "Расходные договоры"
      var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
      // список тематик
      var TopicList = anorsv.DocflowModule.AnorsvGenericTopics.GetAll().Where(k => k.DocumentKind == expendableContractKind &&
                                                                              k.Status == anorsv.DocflowModule.AnorsvGenericTopic.Status.Active).ToList();
      // Добавить поле для выбора тематики.
      var topic = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.TopicDialog, true, anorsv.DocflowModule.AnorsvGenericTopics.Null).From(TopicList);
      // Добавить поле "Предмет договора"
      var subjectContract = dialog.AddString(anorsv.ApprovalTaskModule.Resources.SubjectContract, true).MaxLength(250);
      // Добавить поле для ввода типовой.
      var typical = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.Typical, true).From("Да", "Нет");
      var fileSelector = dialog.AddFileSelect(anorsv.ApprovalTaskModule.Resources.File, false);
      fileSelector.MaxFileSize(Constants.Module.DocDialogMaxFileSize);
      fileSelector.IsVisible  = false;
      var templateContract = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTemplate();
      var templateDoc = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.ChooseTemplate, true, templateContract);
      templateDoc.IsVisible = false;
      var importButton = dialog.Buttons.AddCustom(anorsv.ApprovalTaskModule.Resources.Create);
      var cancelButton = dialog.Buttons.AddCustom(Sungero.Docflow.Resources.Dialog_Close);
      var isImportingTemplates = false;
      var isImportingFile = false;
      var resultDialog = false;
      var viewTemplate = false;
      dialog.Width = 600;
      
      topic.SetOnValueChanged((x) =>
                              {
                                subjectContract.Value = null;
                                typical.Value = null;
                                fileSelector.Value = null;
                                templateDoc.Value = null;
                              });
      
      dialog.SetOnRefresh(
        er =>
        {
          if (resultDialog == false)
          {
            if (typical.Value == "Да")
            {
              if (topic.Value != null)
              {
                //получить список шаблонов в соответствующей тематике
                var templates = Functions.Module.Remote.GetListTemplates(topic.Value.Id);
                //если шаблонов несколько - дать права выбора
                if (templates.Count > 1)
                {
                  if(viewTemplate == false)
                  {
                    // Добавить поле для выбора шаблона.
                    templateDoc.From(templates.ToList());
                    isImportingTemplates = true;
                    isImportingFile = false;
                    templateDoc.SetOnValueChanged(
                      (x) =>
                      {
                        if (x.NewValue == null)
                        {
                          templateContract = null;
                        }
                        else
                        {
                          templateContract = templateDoc.Value;
                        }
                      });
                    
                    fileSelector.IsVisible  = false;
                    viewTemplate = true;
                  }
                  
                  fileSelector.IsVisible  = false;
                  fileSelector.IsRequired  = false;
                  templateDoc.IsVisible = viewTemplate;
                }
                else
                {
                  templateDoc.IsVisible = false;
                  fileSelector.IsVisible  = false;
                  fileSelector.IsRequired  = false;
                  isImportingTemplates = true;
                  isImportingFile = false;
                  templateContract = templates.FirstOrDefault();
                }
              }
              else
              {
                templateDoc.IsVisible = false;
                fileSelector.IsVisible  = false;
                fileSelector.IsRequired  = false;
                isImportingTemplates = true;
                isImportingFile = false;
              }
            }
            if (typical.Value == "Нет")
            {
              viewTemplate = true;
              templateDoc.IsVisible = false;
              isImportingTemplates = false;
              isImportingFile = true;
              fileSelector.IsVisible  = true;
              fileSelector.IsRequired = true;
            }
            if (typical.Value == null)
            {
              viewTemplate = false;
              templateDoc.IsVisible = false;
              isImportingTemplates = false;
              isImportingFile = false;
              fileSelector.IsVisible  = false;
              fileSelector.IsRequired = false;
            }
          }});
      
      
      dialog.SetOnButtonClick(
        b =>
        {
          resultDialog = true;
          
          if (b.Button == cancelButton)
          {
            topic.IsRequired = false;
            subjectContract.IsRequired = false;
            typical.IsRequired = false;
            fileSelector.IsRequired = false;
            templateDoc.IsRequired = false;
            return;
          }
          
          if (b.Button == importButton && b.IsValid)
          {
            if (!b.IsValid)
              return;
            try
            {
              if (isImportingTemplates)
              {
                CreateContract(assignment, document, templateContract, topic.Value, subjectContract.Value);
              }
              else if (isImportingFile)
              {
                CreateContractFromFile2(assignment, fileSelector.Value.Name, document, fileSelector.Value.Content, topic.Value, subjectContract.Value);
              }
            }
            catch(Exception ex)
            {
              Logger.ErrorFormat(ex.Message);
            }
          }
        });
      
      if (dialog.Show() == importButton)
        return true;
      else
        return false;
    }
    
    /// <summary>
    /// Создать проект контракта по шаблону.
    /// </summary>
    /// <param name="assignment">Задание в котром создаётся черновик договора по шаблону.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="template">Шаблон договора.</param>
    /// <param name="topic">Тематика для договора.</param>
    /// <param name="subjectContract">Предмет договора.</param>
    public virtual void CreateContract(
      Sungero.Workflow.IAssignmentBase assignment
      , Sungero.Docflow.IOfficialDocument leadingDocument
      , Sungero.Docflow.IDocumentTemplate template
      , anorsv.DocflowModule.IAnorsvGenericTopic topic
      , string subjectContract)
    {
      var IsStandard = true;
      var contract = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateContract(leadingDocument, topic, subjectContract, IsStandard);
      try
      {
        if (anorsv.ApprovalTask.ApprovalSimpleAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalSimpleAssignments.As(assignment).ContractGroupanorsv.Contracts.Add(contract);
        if (anorsv.ApprovalTask.ApprovalCheckingAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalCheckingAssignments.As(assignment).ContractGroupanorsv.Contracts.Add(contract);
        if (anorsv.ApprovalTask.ApprovalReworkAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalReworkAssignments.As(assignment).ContractGroupanorsv.Contracts.Add(contract);
        
        contract.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        contract.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, contract.Id);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
      }
      
      using (var body = template.LastVersion.Body.Read())
      {
        var newVersion = contract.CreateVersionFrom(body, template.AssociatedApplication.Extension);
        var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)contract;
        internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
        contract.Save();
      }
    }
    
    /// <summary>
    /// Создать договор из файла.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="addendumName">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="addendumContent">Тело документа.</param>
    public virtual void CreateContractFromFile2(
      Sungero.Workflow.IAssignmentBase assignment
      , string contractFileName, Sungero.Docflow.IOfficialDocument leadingDocument
      , byte[] contractFileContent
      , anorsv.DocflowModule.IAnorsvGenericTopic topic
      , string subjectContract)
    {
      var IsStandard = false;
      var contractFile = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateContract(leadingDocument, topic, subjectContract, IsStandard);
      var error = false;
      try
      {
        if (anorsv.ApprovalTask.ApprovalSimpleAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalSimpleAssignments.As(assignment).ContractGroupanorsv.Contracts.Add(contractFile);
        if (anorsv.ApprovalTask.ApprovalCheckingAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalCheckingAssignments.As(assignment).ContractGroupanorsv.Contracts.Add(contractFile);
        if (anorsv.ApprovalTask.ApprovalReworkAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalReworkAssignments.As(assignment).ContractGroupanorsv.Contracts.Add(contractFile);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
        error = true;
      }
      if (error == false)
      {
        contractFile.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        contractFile.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, contractFile.Id);

        using (var fileStream = new System.IO.MemoryStream(contractFileContent))
        {
          contractFile.CreateVersionFrom(fileStream, System.IO.Path.GetExtension(contractFileName));
          contractFile.Save();
          Logger.DebugFormat("contractFile: After creating contractFile contractFileid = {1}",  contractFile.Id);
        }
      }
    }
    #endregion
    
    #region Диалог создания ТЗ
    /// <summary>
    /// Диалог по добавлению технического задания в простое задание.
    /// </summary>
    /// <param name="document">Документ, для которого создаются приложения.</param>
    [Public]
    public virtual bool TSDialog(Sungero.Workflow.IAssignmentBase assignment, Sungero.Docflow.IOfficialDocument document)
    {
      // Создать диалог ввода.
      var dialog = Dialogs.CreateInputDialog(anorsv.ApprovalTaskModule.Resources.ChooseFormTS);
      // вид "Техническое задание"
      var TSKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.ContractsModule.PublicConstants.Module.Initialize.TechnicalSpecificationKind);
      // список тематик
      var TopicList = anorsv.DocflowModule.AnorsvGenericTopics.GetAll().Where(k => k.DocumentKind == TSKind &&
                                                                              k.Status == anorsv.DocflowModule.AnorsvGenericTopic.Status.Active).ToList();
      // Добавить поле для выбора тематики.
      var topic = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.TopicDialog, true, anorsv.DocflowModule.AnorsvGenericTopics.Null).From(TopicList);
      // Добавить поле "Предмет ТЗ"
      var subjectTS = dialog.AddString(anorsv.ApprovalTaskModule.Resources.SubjectTS, true).MaxLength(250);
      // Добавить поле для ввода типовой.
      var typical = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.Typical, true).From("Да", "Нет");
      var fileSelector = dialog.AddFileSelect(anorsv.ApprovalTaskModule.Resources.File, false);
      fileSelector.MaxFileSize(Constants.Module.DocDialogMaxFileSize);
      fileSelector.IsVisible  = false;
      var templateTS = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTemplate();
      var templateDoc = dialog.AddSelect(anorsv.ApprovalTaskModule.Resources.ChooseTemplateTS, true, templateTS);
      templateDoc.IsVisible = false;
      var importButton = dialog.Buttons.AddCustom(anorsv.ApprovalTaskModule.Resources.Create);
      var cancelButton = dialog.Buttons.AddCustom(Sungero.Docflow.Resources.Dialog_Close);
      var isImportingTemplates = false;
      var isImportingFile = false;
      var resultDialog = false;
      var viewTemplate = false;
      dialog.Width = 650;
      
      topic.SetOnValueChanged(
        (x) =>
        {
          subjectTS.Value = null;
          typical.Value = null;
          fileSelector.Value = null;
          templateDoc.Value = null;
        });
      
      dialog.SetOnRefresh(
        er =>
        {if (resultDialog == false)
          {
            if (typical.Value == "Да")
            {
              if (topic.Value != null)
              {
                //получить список шаблонов в соответствующей тематике
                var templates = Functions.Module.Remote.GetListTemplates(topic.Value.Id);
                //если шаблонов несколько - дать права выбора
                if (templates.Count > 1)
                {
                  if(viewTemplate == false)
                  {
                    // Добавить поле для выбора шаблона.
                    templateDoc.From(templates.ToList());
                    isImportingTemplates = true;
                    isImportingFile = false;
                    
                    templateDoc.SetOnValueChanged(
                      (x) =>
                      {
                        if (x.NewValue == null)
                        {
                          templateTS = null;
                        }
                        else
                        {
                          templateTS = templateDoc.Value;
                        }
                      });
                    
                    fileSelector.IsVisible  = false;
                    viewTemplate = true;
                  }
                  
                  fileSelector.IsVisible  = false;
                  fileSelector.IsRequired  = false;
                  templateDoc.IsVisible = viewTemplate;
                }
                else
                {
                  templateDoc.IsVisible = false;
                  fileSelector.IsVisible  = false;
                  isImportingTemplates = true;
                  isImportingFile = false;
                  templateTS = templates.FirstOrDefault();
                }
              }
              else
              {
                templateDoc.IsVisible = false;
                fileSelector.IsVisible  = false;
                fileSelector.IsRequired  = false;
                isImportingTemplates = true;
                isImportingFile = false;
              }
            }
            if (typical.Value == "Нет")
            {
              viewTemplate = true;
              templateDoc.IsVisible = false;
              isImportingTemplates = false;
              isImportingFile = true;
              fileSelector.IsVisible  = true;
              fileSelector.IsRequired = true;
            }
            if (typical.Value == null)
            {
              viewTemplate = false;
              templateDoc.IsVisible = false;
              isImportingTemplates = false;
              isImportingFile = false;
              fileSelector.IsVisible  = false;
              fileSelector.IsRequired = false;
            }
          }});
      
      
      dialog.SetOnButtonClick(
        b =>
        {
          resultDialog = true;
          if (b.Button == cancelButton)
          {
            topic.IsRequired = false;
            subjectTS.IsRequired = false;
            typical.IsRequired = false;
            fileSelector.IsRequired = false;
            templateDoc.IsRequired = false;
            return;
          }
          if (b.Button == importButton && b.IsValid)
          {
            if (!b.IsValid)
              return;
            try
            {
              if (isImportingTemplates)
              {
                CreateTS(assignment, document, templateTS, topic.Value, subjectTS.Value);
              }
            }
            catch(Exception ex)
            {
              Logger.ErrorFormat(ex.Message);
            }
            try
            {
              if (isImportingFile)
              {
                CreateTSFromFile(assignment, fileSelector.Value.Name, document, fileSelector.Value.Content, topic.Value, subjectTS.Value);
              }
            }
            catch(Exception ex)
            {
              Logger.ErrorFormat(ex.Message);
            }
          }
        });
      
      if (dialog.Show() == importButton)
        return true;
      else
        return false;
    }
    
    /// <summary>
    /// Создать ТЗ из файла.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="addendumName">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="addendumContent">Тело документа.</param>
    public virtual void CreateTSFromFile(
      Sungero.Workflow.IAssignmentBase assignment
      , string contractFileName, Sungero.Docflow.IOfficialDocument leadingDocument
      , byte[] contractFileContent
      , anorsv.DocflowModule.IAnorsvGenericTopic topic
      , string subjectTS)
    {
      var IsStandard = false;
      var docTSFile = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateCardTS(leadingDocument, topic, subjectTS, IsStandard);
      var error = false;
      
      try
      {
        if (anorsv.ApprovalTask.ApprovalSimpleAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalSimpleAssignments.As(assignment).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTSFile);
        if (anorsv.ApprovalTask.ApprovalCheckingAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalCheckingAssignments.As(assignment).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTSFile);
        if (anorsv.ApprovalTask.ApprovalReworkAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalReworkAssignments.As(assignment).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTSFile);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
        error = true;
      }

      if (error == false)
      {
        docTSFile.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        docTSFile.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, docTSFile.Id);
        using (var fileStream = new System.IO.MemoryStream(contractFileContent))
        {
          docTSFile.CreateVersionFrom(fileStream, System.IO.Path.GetExtension(contractFileName));
          docTSFile.Save();
          Logger.DebugFormat("docTSFile: After creating docTSFile docTSFilid = {1}",  docTSFile.Id);
        }
      }
      
    }

    /// <summary>
    /// Создать ТЗ по шаблону.
    /// </summary>
    /// <param name="currentAssignment">Текущее задание.</param>
    /// <param name="addendumName">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="addendumContent">Тело документа.</param>
    public virtual void  CreateTS(
      Sungero.Workflow.IAssignmentBase assignment
      , Sungero.Docflow.IOfficialDocument leadingDocument
      , Sungero.Docflow.IDocumentTemplate template, anorsv.DocflowModule.IAnorsvGenericTopic topic
      , string subjectTS)
    {
      var IsStandard = true;
      var docTS = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.CreateCardTS(leadingDocument, topic, subjectTS, IsStandard);
      
      try
      {
        if (anorsv.ApprovalTask.ApprovalSimpleAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalSimpleAssignments.As(assignment).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTS);
        if (anorsv.ApprovalTask.ApprovalCheckingAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalCheckingAssignments.As(assignment).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTS);
        if (anorsv.ApprovalTask.ApprovalReworkAssignments.Is(assignment))
          anorsv.ApprovalTask.ApprovalReworkAssignments.As(assignment).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.Add(docTS);
        docTS.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, leadingDocument);
        leadingDocument.Save();
        docTS.Save();
        anorsv.RelationModule.PublicFunctions.Module.UpdateSpecialRelations(leadingDocument.Id, docTS.Id);
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(ex.Message);
      }
      
      using (var body = template.LastVersion.Body.Read())
      {
        var newVersion = docTS.CreateVersionFrom(body, template.AssociatedApplication.Extension);
        var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)docTS;
        internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
        docTS.Save();
      }
    }
    
    #endregion
    
    #endregion
  }
}

