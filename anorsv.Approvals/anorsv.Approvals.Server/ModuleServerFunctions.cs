﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;

namespace anorsv.Approvals.Server
{
  public class ModuleFunctions
  {
    [Public, Remote(IsPure=false)]
    public static void AssignmentChildTasksAbort(Sungero.Workflow.IAssignment parentAssignment)
    {
      AssignmentChildTasksAbortInternal(parentAssignment);
    }
    
    [Public]
    public static void AssignmentChildTasksAbortInternal(Sungero.Workflow.IAssignment parentAssignment)
    {
      Logger.DebugFormat("Approvals.Server.AssignmentChildTasksAbort: AssigmentId = {0}.", parentAssignment.Id);
      
      // Прекратим все дочерние подзадачи
      List<Enumeration> taskStatuses = new List<Enumeration> {Sungero.Workflow.Task.Status.InProcess
          , Sungero.Workflow.Task.Status.Suspended
          , Sungero.Workflow.Task.Status.UnderReview};
      List<ITask> childTasks = Tasks.GetAll(task => task.ParentAssignment.Equals(parentAssignment) && taskStatuses.Contains(task.Status.Value)).ToList();
      List<ITask> deeperChildTasks = new List<ITask>();

      do
      {
        deeperChildTasks.Clear();
        foreach(ITask parentTask in childTasks)
          foreach(IAssignment assignment in Assignments.GetAll(a => a.Task.Equals(parentTask)).ToList())
            foreach(ITask childTask in assignment.Subtasks)
              if (taskStatuses.Contains(childTask.Status.Value) && !childTasks.Contains(childTask))
                deeperChildTasks.Add(childTask);
        childTasks.AddRange(deeperChildTasks);
      } while(deeperChildTasks.Count() > 0);
      
      foreach (ITask childTask in childTasks)
      {
        Logger.DebugFormat("Approvals.Server.AssignmentChildTasksAbort: Abort task with ParentTaskId = {0}, ParentAssigmentId = {1}, TaskId = {2}.", childTask.ParentAssignment.Task.Id, childTask.ParentAssignment.Id, childTask.Id);
        
        childTask.Abort();
      }
    }

    [Public, Remote(IsPure=false)]
    public static void RefreshAttachments(anorsv.FormalizedSubTask.IFormalizedSubTask task)
    {
      RefreshAttachmentsInternal(task);
    }
    
    [Public]
    public static void RefreshAttachmentsInternal(anorsv.FormalizedSubTask.IFormalizedSubTask task)
    {
      var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(task.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, task.OtherGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshMemoGroup(document, task.MemoGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshContractGroup(document, task.ContractGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshTechnicalSpecificationGroup(document, task.TechnicalSpecificationGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshOrderGroup(document, task.OrderGroup);
      }
    }
    
  }
}