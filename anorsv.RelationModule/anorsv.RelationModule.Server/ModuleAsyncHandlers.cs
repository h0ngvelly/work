﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.RelationModule.Server
{
  public class ModuleAsyncHandlers
  {

    public virtual void RelationSynchronization(anorsv.RelationModule.Server.AsyncHandlerInvokeArgs.RelationSynchronizationInvokeArgs args)
    {
      var relationTypeCurrent = RelationTypes.GetAll().Where(r => r.Name == Sungero.Docflow.PublicConstants.Module.SimpleRelationName).FirstOrDefault();
      IRelationType relationType = RelationTypes.Null;
      
      var sourceDoc = anorsv.OfficialDocument.OfficialDocuments.GetAll().Where(s => s.Id == args.SourceId).FirstOrDefault();
      var targetDoc = anorsv.OfficialDocument.OfficialDocuments.GetAll().Where(t => t.Id == args.TargetId).FirstOrDefault();
      
      // Необходимые типы связей для типов/видов документов
      if (sline.RSV.Memos.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
      {
        // СЗ по закупке
        relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName).FirstOrDefault();
      }
      else if (anorsv.Contracts.Contracts.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
      {
        // Проект договора
        relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName).FirstOrDefault();
      }
      else if (anorsv.ContractsModule.TechnicalSpecifications.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
      {
        // Техническое задание
        relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName).FirstOrDefault();
      }
      else if (sline.RSV.Orders.Is(targetDoc) && anorsv.DocflowModule.LNAs.Is(sourceDoc))
      {
        // Приказ
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid); 
        if (lnaGenActivitiesKind != null && sourceDoc.DocumentKind.Equals(lnaGenActivitiesKind))
          relationType = RelationTypes.GetAll().Where(r => r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).FirstOrDefault();
      }
      
      if (relationTypeCurrent != null && relationType != null)
      {
        var targetDocRelation = targetDoc.Relations.GetRelatedFrom(relationType.Name).ToList();
        if (!targetDocRelation.Contains(sourceDoc))
        {
          try
          {
            targetDoc.Relations.AddFrom(Sungero.Docflow.PublicConstants.Module.SimpleRelationName, sourceDoc);
            targetDoc.Save();
            var command = string.Format(Queries.Module.UpdateSpecialRelationPurchaseMemo, relationType.Id, relationTypeCurrent.Id, args.SourceId, args.TargetId);
            Sungero.Docflow.PublicFunctions.Module.ExecuteSQLCommand(command);
            args.Retry = false;
          }
          catch(Exception ex)
          {
            Logger.DebugFormat("RelationSynchronization: doc relation error. SourceId-{0}, TargetId-{1}. {2}", args.SourceId, args.TargetId, ex.Message);
            args.Retry = true;
          }
        }
        else
        {
          args.Retry = false;
          return;
        }
      }
      else
      {
        args.Retry = false;
        return;
      }
    }

    public virtual void RelatedDocAdd(anorsv.RelationModule.Server.AsyncHandlerInvokeArgs.RelatedDocAddInvokeArgs args)
    {
      // Добавление вложения в задачи
      var document = Sungero.Content.ElectronicDocuments.Get(args.MainDocId);
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(args.DocId);
      var approvalTask = Sungero.Docflow.ApprovalTasks.GetAll(d => d.Id == args.TaskId).FirstOrDefault();
      var formalizedTask = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll(d => d.Id == args.TaskId).FirstOrDefault();
      
      if (relatedDocument == null)
      {
        Logger.DebugFormat("RelatedDocAdd: document is not found. DocID-{0}.", args.DocId);
        args.Retry = false;
        return;
      }
      
      if (approvalTask == null && formalizedTask == null)
      {
        Logger.DebugFormat("RelatedDocAdd: task is not found. TaskID-{0}.", args.TaskId);
        args.Retry = false;
        return;
      }
      
      try
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (approvalTask != null)
        {
          // Если связанный документ Приложение, добавим в Приложения задачи согласования
          // Если связанный документ не Приложение, добавим в Дополнительно задачи согласования
          if (documentAddenda.Contains(relatedDocument))
          {
            if (!approvalTask.AddendaGroup.All.Contains(relatedDocument))
              approvalTask.AddendaGroup.All.Add(relatedDocument);
          }
          else
          {
            if (!approvalTask.OtherGroup.All.Contains(relatedDocument))
              approvalTask.OtherGroup.All.Add(relatedDocument);
          }
          approvalTask.Save();
          args.Retry = false;
        }
        else if (formalizedTask != null)
        {
          // Если связанный документ Приложение, добавим в Приложения формализованной подзадачи
          // Если связанный документ не Приложение, добавим в Дополнительно формализованной подзадачи
          if (documentAddenda.Contains(relatedDocument))
          {
            if (!formalizedTask.AddendaGroup.All.Contains(relatedDocument))
              formalizedTask.AddendaGroup.All.Add(relatedDocument);
          }
          else
          {
            if (!formalizedTask.OtherGroup.All.Contains(relatedDocument))
              formalizedTask.OtherGroup.All.Add(relatedDocument);
          }
          formalizedTask.Save();
          args.Retry = false;
        }
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("RelatedDocAdd: task is locked. TaskID-{0}, DocID-{1}. {2}", args.TaskId, args.DocId, ex.Message);
        args.Retry = true;
      }
      
    }

    public virtual void RelatedDocChange(anorsv.RelationModule.Server.AsyncHandlerInvokeArgs.RelatedDocChangeInvokeArgs args)
    {
      // Изменяем группу для вложения задачи
      var document = Sungero.Content.ElectronicDocuments.Get(args.MainDocId);
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(args.DocId);
      var approvalTask = Sungero.Docflow.ApprovalTasks.GetAll(d => d.Id == args.TaskId).FirstOrDefault();
      var formalizedTask = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll(d => d.Id == args.TaskId).FirstOrDefault();
      
      if (relatedDocument == null)
      {
        Logger.DebugFormat("RelatedDocChange: document is not found. DocID-{0}.", args.DocId);
        return;
      }
      
      if (approvalTask == null && formalizedTask == null)
      {
        Logger.DebugFormat("RelatedDocChange: task is not found. TaskID-{0}.", args.TaskId);
        args.Retry = false;
        return;
      }
      
      try
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (approvalTask != null)
        {
          // Если связанный документ Приложение, удалим из Дополнительно и вложим в Приложения задачи согласования
          // Если связанный документ не Приложение, удалим из Приложения и вложим в Дополнительно задачи согласования
          if (documentAddenda.Contains(relatedDocument))
          {
            if (approvalTask.OtherGroup.All.Contains(relatedDocument))
              approvalTask.OtherGroup.All.Remove(relatedDocument);
            if (!approvalTask.AddendaGroup.All.Contains(relatedDocument))
              approvalTask.AddendaGroup.All.Add(relatedDocument);
          }
          else
          {
            if (approvalTask.AddendaGroup.All.Contains(relatedDocument))
              approvalTask.AddendaGroup.All.Remove(relatedDocument);
            if (!approvalTask.OtherGroup.All.Contains(relatedDocument))
              approvalTask.OtherGroup.All.Add(relatedDocument);
          }
          approvalTask.Save();
          args.Retry = false;
        }
        else if (formalizedTask != null)
        {
          // Если связанный документ Приложение, удалим из Дополнительно и вложим в Приложения формализованной подзадачи
          // Если связанный документ не Приложение, удалим из Приложения и вложим в Дополнительно формализованной подзадачи
          if (documentAddenda.Contains(relatedDocument))
          {
            if (formalizedTask.OtherGroup.All.Contains(relatedDocument))
              formalizedTask.OtherGroup.All.Remove(relatedDocument);
            if (!formalizedTask.AddendaGroup.All.Contains(relatedDocument))
              formalizedTask.AddendaGroup.All.Add(relatedDocument);
          }
          else
          {
            if (formalizedTask.AddendaGroup.All.Contains(relatedDocument))
              formalizedTask.AddendaGroup.All.Remove(relatedDocument);
            if (!formalizedTask.OtherGroup.All.Contains(relatedDocument))
              formalizedTask.OtherGroup.All.Add(relatedDocument);
          }
          formalizedTask.Save();
          args.Retry = false;
        }
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("RelatedDocChange: task is locked. TaskID-{0}, DocID-{1}. {2}", args.TaskId, args.DocId, ex.Message);
        args.Retry = true;
      }
      
    }

    public virtual void RelatedDocDelete(anorsv.RelationModule.Server.AsyncHandlerInvokeArgs.RelatedDocDeleteInvokeArgs args)
    {
      // Удаляем вложение из задачи
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(args.DocId);
      var approvalTask = Sungero.Docflow.ApprovalTasks.GetAll(d => d.Id == args.TaskId).FirstOrDefault();
      var formalizedTask = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll(d => d.Id == args.TaskId).FirstOrDefault();
      
      if (relatedDocument == null)
      {
        Logger.DebugFormat("RelatedDocDelete: document is not found. DocID-{0}.", args.DocId);
        return;
      }
      
      if (approvalTask == null && formalizedTask == null)
      {
        Logger.DebugFormat("RelatedDocDelete: task is not found. TaskID-{0}.", args.TaskId);
        args.Retry = false;
      }
      
      try
      {
        if (approvalTask != null)
        {
          // Удалить документ из области Приложения или Дополнительно задачи согласования
          if (approvalTask.AddendaGroup.All.Contains(relatedDocument))
            approvalTask.AddendaGroup.All.Remove(relatedDocument);
          else if (approvalTask.OtherGroup.All.Contains(relatedDocument))
            approvalTask.OtherGroup.All.Remove(relatedDocument);
          approvalTask.Save();
          args.Retry = false;
        }
        else if (formalizedTask != null)
        {
          // Удалить документ из области Приложения или Дополнительно формализованной подзадачи
          if (formalizedTask.AddendaGroup.All.Contains(relatedDocument))
            formalizedTask.AddendaGroup.All.Remove(relatedDocument);
          else if (formalizedTask.OtherGroup.All.Contains(relatedDocument))
            formalizedTask.OtherGroup.All.Remove(relatedDocument);
          formalizedTask.Save();
          args.Retry = false;
        }
      }
      catch(Exception ex)
      {
        Logger.DebugFormat("RelatedDocDelete: task is locked. TaskID-{0}, DocID-{1}. {2}", args.TaskId, args.DocId, ex.Message);
        args.Retry = true;
      }
      
    }

  }
}