﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using docRelation = anorsv.RelationModule;

namespace anorsv.RelationModule.Server
{
  public class ModuleFunctions
  {
    
    #region Добавление, удаление, изменение связи документов
    
    /// <summary>
    /// Проверить наличие связи для документов определенных видов
    /// </summary>
    [Public]
    public static void CheckSpecialRelations(int sourceId, int targetId)
    {
      var relationTypeCurrent = RelationTypes.GetAll().Where(r => r.Name == Sungero.Docflow.PublicConstants.Module.SimpleRelationName).FirstOrDefault();
      IRelationType relationType = RelationTypes.Null;
      
      var sourceDoc = anorsv.OfficialDocument.OfficialDocuments.GetAll(d => d.Id == sourceId).FirstOrDefault();
      var targetDoc = anorsv.OfficialDocument.OfficialDocuments.GetAll(d => d.Id == targetId).FirstOrDefault();
      
      if (targetDoc != null && sourceDoc != null)
      {
        // Необходимые типы связей для типов/видов документов
        if (sline.RSV.Memos.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
        {
          // СЗ по закупке
          relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName).FirstOrDefault();
        }
        else if (anorsv.Contracts.Contracts.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
        {
          // Проект договора
          relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName).FirstOrDefault();
        }
        else if (anorsv.ContractsModule.TechnicalSpecifications.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
        {
          // Техническое задание
          relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName).FirstOrDefault();
        }
        else if (sline.RSV.Orders.Is(targetDoc) && anorsv.DocflowModule.LNAs.Is(sourceDoc))
        {
          // Приказ
          var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
          if (lnaGenActivitiesKind != null && sourceDoc.DocumentKind.Equals(lnaGenActivitiesKind))
            relationType = RelationTypes.GetAll().Where(r => r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).FirstOrDefault();
        }
        
        if (relationTypeCurrent != null && relationType != null)
        {
          var targetDocRelation = targetDoc.Relations.GetRelatedFrom(relationType.Name).ToList();
          if (!targetDocRelation.Contains(sourceDoc))
          {
            // Создание Простой связи с последующей программной заменой на закрытую спец.связь
            var asyncRelationSynchronization = anorsv.RelationModule.AsyncHandlers.RelationSynchronization.Create();
            asyncRelationSynchronization.SourceId = sourceId;
            asyncRelationSynchronization.TargetId = targetId;
            asyncRelationSynchronization.ExecuteAsync();
          }
          
        }
      }
    }
    
    /// <summary>
    /// Программное создание связи для документов определенных видов
    /// </summary>
    [Public]
    public static void UpdateSpecialRelations(int sourceId, int targetId)
    {
      var relationTypeCurrent = RelationTypes.GetAll().Where(r => r.Name == Sungero.Docflow.PublicConstants.Module.SimpleRelationName).FirstOrDefault();
      IRelationType relationType = RelationTypes.Null;
      
      var sourceDoc = anorsv.OfficialDocument.OfficialDocuments.Get(sourceId);
      var targetDoc = anorsv.OfficialDocument.OfficialDocuments.Get(targetId);
      
      // Необходимые типы связей для типов документов
      if (sline.RSV.Memos.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
      {
        // СЗ по закупке
        relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName).FirstOrDefault();
      }
      else if (anorsv.Contracts.Contracts.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
      {
        // Проект договора
        relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName).FirstOrDefault();
      }
      else if (anorsv.ContractsModule.TechnicalSpecifications.Is(targetDoc) && sline.CustomModule.PurchaseRequests.Is(sourceDoc))
      {
        // Техническое задание
        relationType = RelationTypes.GetAll().Where(r => r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName).FirstOrDefault();
      }
      else if (sline.RSV.Orders.Is(targetDoc) && anorsv.DocflowModule.LNAs.Is(sourceDoc))
      {
        // Приказ
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        if (lnaGenActivitiesKind != null && sourceDoc.DocumentKind.Equals(lnaGenActivitiesKind))
          relationType = RelationTypes.GetAll().Where(r => r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).FirstOrDefault();
      }
      
      if (relationTypeCurrent != null && relationType != null)
      {
        // Изменить тип связи с Простой на специальную предусмотренную для типа
        var command = string.Format(Queries.Module.UpdateSpecialRelationPurchaseMemo, relationType.Id, relationTypeCurrent.Id, sourceId, targetId);
        Sungero.Docflow.PublicFunctions.Module.ExecuteSQLCommand(command);
      }
    }
    
    /// <summary>
    /// Проверка возможности изменения связи с документом (из документа)
    /// Для администраторов, делопроизводителей, автором при отсутствии задач по регламенту или при наличии задания в работе
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static bool CanAddDocumentRelations(Sungero.Docflow.IOfficialDocument document)
    {
      var canChange = false;

      if (Users.Current.IncludedIn(Roles.Administrators) || Users.Current.IncludedIn(Sungero.Docflow.PublicFunctions.DocumentRegister.Remote.GetClerks()))
      {
        canChange = true;
      }
      else
      {
        var documentList = document.Relations.GetRelated().Union(document.Relations.GetRelatedFrom());
        var substUsers = Sungero.CoreEntities.Substitutions.ActiveSubstitutedUsersWithoutSystem.ToList();
        substUsers.Add(Users.Current);
        
        // Для автора - разрешить при отсутствии регистрации, задач по регламенту или при наличии задания в работе
        bool haveAuthorPermission = substUsers.Contains(document.PreparedBy);
        canChange = haveAuthorPermission;
      }
      return canChange;
    }
    
    /// <summary>
    /// Проверка возможности изменения связи с документом (из документа)
    /// Для администраторов, делопроизводителей, автором при отсутствии задач по регламенту или при наличии задания в работе
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static bool CanChangeDocumentRelations(Sungero.Docflow.IOfficialDocument document)
    {
      var canChange = false;

      if (Users.Current.IncludedIn(Roles.Administrators) || Users.Current.IncludedIn(Sungero.Docflow.PublicFunctions.DocumentRegister.Remote.GetClerks()))
      {
        canChange = true;
      }
      else
      {
        var documentList = document.Relations.GetRelated().Union(document.Relations.GetRelatedFrom());
        var substUsers = Sungero.CoreEntities.Substitutions.ActiveSubstitutedUsersWithoutSystem.ToList();
        substUsers.Add(Users.Current);
        
        // Для автора - разрешить при отсутствии регистрации, задач по регламенту или при наличии задания в работе
        bool haveAuthorPermission = substUsers.Contains(document.PreparedBy);
        bool haveRegistration = (document.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Registered &&
                                 document.DocumentKind.NumberingType != Sungero.Docflow.DocumentKind.NumberingType.Numerable) ||
          (document.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Reserved);
        bool haveAnyTask = anorsv.ApprovalTaskModule.PublicFunctions.Module.IsAnyTaskInProcess(document);
        bool haveAnyJob = anorsv.ApprovalTaskModule.PublicFunctions.Module.IsAnyJobInProcess(document, Users.Current);
        canChange = ((!haveAnyTask) || (haveAnyTask && haveAnyJob)) && haveAuthorPermission && !haveRegistration;
      }
      return canChange;
    }
    
    /// <summary>
    /// Проверка возможности изменения связи с документом (из задания)
    /// Для администраторов, делопроизводителей, автором при отсутствии задач по регламенту или при наличии задания в работе
    /// </summary>
    [Public, Remote(IsPure = true)]
    public static bool CanChangeDocumentRelations(Sungero.Docflow.IOfficialDocument document, Sungero.Workflow.IAssignmentBase assignment)
    {
      var canChange = false;
      
      if (Users.Current.IncludedIn(Roles.Administrators) || Users.Current.IncludedIn(Sungero.Docflow.PublicFunctions.DocumentRegister.Remote.GetClerks()))
      {
        canChange = true;
      }
      else
      {
        var substUsers = Sungero.CoreEntities.Substitutions.ActiveSubstitutedUsersWithoutSystem.ToList();
        substUsers.Add(Users.Current);
        
        // Для автора - разрешить при отсутствии регистрации, задач по регламенту или при наличии задания в работе
        bool haveAuthorPermission = substUsers.Contains(document.PreparedBy);
        bool haveRegistration = (document.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Registered &&
                                 document.DocumentKind.NumberingType != Sungero.Docflow.DocumentKind.NumberingType.Numerable) ||
          (document.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Reserved);
        bool haveAnyJob = (assignment.Task.Status.Equals(Sungero.Docflow.ApprovalTask.Status.InProcess) || assignment.Task.Status.Equals(Sungero.Docflow.ApprovalTask.Status.Suspended)) &&
          (assignment.Status.Equals(Sungero.Workflow.AssignmentBase.Status.InProcess) || assignment.Status.Equals(Sungero.Workflow.AssignmentBase.Status.Suspended)) &&
          (substUsers.Contains(assignment.Performer));
        //anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(assignment, Users.Current);
        //(assignment.AccessRights.CanUpdate(Users.Current));
        canChange = haveAnyJob && haveAuthorPermission && !haveRegistration;
      }
      return canChange;
    }
    
    /// <summary>
    /// Разрыв текущей связи с ведущим документом
    /// </summary>
    [Remote]
    public static void DeleteLeadingDocRelation(int documentId, int leadingDocumentId, string selectedLinkType)
    {
      var document = anorsv.OfficialDocument.OfficialDocuments.GetAll(d => d.Id == documentId).FirstOrDefault();
      var leadingDocument = anorsv.OfficialDocument.OfficialDocuments.GetAll(d => d.Id == leadingDocumentId).FirstOrDefault();
      
      if (document != null && leadingDocument != null)
      {
        if (sline.RSV.Memos.Is(document) && sline.CustomModule.PurchaseRequests.Is(leadingDocument) && selectedLinkType.Equals(sline.CustomModule.Resources.MemoForPurchaseRelationName))
        {
          // СЗ по закупке (удаление из Закупки)
          var memo = sline.RSV.Memos.As(document);
          memo.LeadingDocument = null;
          memo.Purchaseanorsv = null;
          memo.Save();
          memo.Relations.RemoveFrom(selectedLinkType, leadingDocument);
          memo.Relations.Save();
        }
        else if (anorsv.Contracts.Contracts.Is(document) && sline.CustomModule.PurchaseRequests.Is(leadingDocument) && selectedLinkType.Equals(sline.CustomModule.Resources.ContractForPurchaseRelationName))
        {
          // Проект договора (удаление из Закупки)
          var conractDraft = anorsv.Contracts.Contracts.As(document);
          conractDraft.LeadingDocument = null;
          conractDraft.Purchaseanorsv = null;
          conractDraft.Save();
          conractDraft.Relations.RemoveFrom(selectedLinkType, leadingDocument);
          conractDraft.Relations.Save();
        }
        else if (anorsv.ContractsModule.TechnicalSpecifications.Is(document) && sline.CustomModule.PurchaseRequests.Is(leadingDocument) && selectedLinkType.Equals(sline.CustomModule.Resources.TSForPurchaseRelationName))
        {
          // Техническое задание (удаление из Закупки)
          var techSpes = anorsv.ContractsModule.TechnicalSpecifications.As(document);
          techSpes.LeadingDocument = null;
          techSpes.Purchaseanorsv = null;
          techSpes.Save();
          techSpes.Relations.RemoveFrom(selectedLinkType, leadingDocument);
          techSpes.Relations.Save();
        }
        else if (anorsv.DocflowModule.LNAs.Is(document) && sline.RSV.Orders.Is(leadingDocument) && selectedLinkType.Equals(anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName))
        {
          // ЛНА (удаление из Приказа)
          var lna = anorsv.DocflowModule.LNAs.As(document);
          lna.LeadingDocument = null;
          lna.Orderanorsv = null;
          lna.Save();
          lna.Relations.RemoveFrom(selectedLinkType, leadingDocument);
          lna.Relations.Save();
        }
        else
        {
          document.LeadingDocument = null;
          document.Save();
          document.Relations.Remove(selectedLinkType, leadingDocument);
          document.Relations.Save();
        }
        
      }
    }
    
    /// <summary>
    /// Разрыв текущей связи и создание новой
    /// </summary>
    [Remote]
    public static void UpdateRelation(int documentId, int relatedDocumentId, string currentLinkTypeName, string selectedLinkTypeName, bool currentUseTarget, bool selectedUseTarget)
    {
      // Удалить выбранную связь и добавить новую
      var document = Sungero.Content.ElectronicDocuments.Get(documentId);
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(relatedDocumentId);
      
      // Удалить ссылку на основной документ в качестве ведущего
      var relatedDocOfficial = anorsv.OfficialDocument.OfficialDocuments.As(relatedDocument);
      var docOfficial = anorsv.OfficialDocument.OfficialDocuments.As(document);
      if (relatedDocOfficial.LeadingDocument != null && relatedDocOfficial.LeadingDocument.Equals(docOfficial))
      {
        relatedDocOfficial.LeadingDocument = null;
        relatedDocOfficial.Save();
      }
      if (docOfficial.LeadingDocument != null && docOfficial.LeadingDocument.Equals(relatedDocOfficial))
      {
        docOfficial.LeadingDocument = null;
        docOfficial.Save();
      }
      
      if (currentUseTarget)
        document.Relations.Remove(currentLinkTypeName, relatedDocument);
      else
        document.Relations.RemoveFrom(currentLinkTypeName, relatedDocument);
      if (selectedUseTarget)
        document.Relations.Add(selectedLinkTypeName, relatedDocument);
      else
        document.Relations.AddFrom(selectedLinkTypeName, relatedDocument);
      document.Relations.Save();
      
      // Изменить расположение документа в области вложений задачи
      var taskList = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTaskInProcess(document.Id, Users.Current);
      foreach (var task in taskList)
      {
        ChangeRelatedDocInAttachments(task.Id, relatedDocument.Id);
      }
    }
    
    /// <summary>
    /// Добавление связанного документа в область вложений
    /// </summary>
    [Public, Remote]
    public static void AddRelatedDocInAttachments(int taskId, int relatedDocumentId, int? assignmentId)
    {
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(relatedDocumentId);
      var approvalTask = Sungero.Docflow.ApprovalTasks.GetAll(t => t.Id == taskId).FirstOrDefault();
      var formalizedSubtask = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll(t => t.Id == taskId).FirstOrDefault();
      
      if (approvalTask != null)
        anorsv.ApprovalTaskModule.PublicFunctions.Module.AddRelatedDocInAttachments(approvalTask, relatedDocument, assignmentId);
      else if (formalizedSubtask != null)
        anorsv.FormalizedSubTask.PublicFunctions.Module.AddRelatedDocInAttachments(formalizedSubtask, relatedDocument, assignmentId);
    }
    
    /// <summary>
    /// Удаление связанного документа из области вложений
    /// </summary>
    [Public, Remote]
    public static void DeleteRelatedDocInAttachments(int taskId, int relatedDocumentId)
    {
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(relatedDocumentId);
      var approvalTask = anorsv.ApprovalTask.ApprovalTasks.GetAll(t => t.Id == taskId).FirstOrDefault();  //Sungero.Docflow.ApprovalTasks.GetAll(t => t.Id == taskId).FirstOrDefault();
      var formalizedSubtask = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll(t => t.Id == taskId).FirstOrDefault();
      
      if (approvalTask != null)
        anorsv.ApprovalTaskModule.PublicFunctions.Module.DeleteRelatedDocInAttachments(approvalTask, relatedDocument);
      else if (formalizedSubtask != null)
        anorsv.FormalizedSubTask.PublicFunctions.Module.DeleteRelatedDocInAttachments(formalizedSubtask, relatedDocument);
    }
    
    /// <summary>
    /// Изменение области вложения связанного документа
    /// </summary>
    [Public, Remote]
    public static void ChangeRelatedDocInAttachments(int taskId, int relatedDocumentId)
    {
      var relatedDocument = Sungero.Content.ElectronicDocuments.Get(relatedDocumentId);
      var approvalTask = anorsv.ApprovalTask.ApprovalTasks.GetAll(t => t.Id == taskId).FirstOrDefault();  //Sungero.Docflow.ApprovalTasks.GetAll(t => t.Id == taskId).FirstOrDefault();
      var formalizedSubtask = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll(t => t.Id == taskId).FirstOrDefault();
      
      if (approvalTask != null)
        anorsv.ApprovalTaskModule.PublicFunctions.Module.ChangeRelatedDocInAttachments(approvalTask, relatedDocument);
      else if (formalizedSubtask != null)
        anorsv.FormalizedSubTask.PublicFunctions.Module.ChangeRelatedDocInAttachments(formalizedSubtask, relatedDocument);
    }
    
    #endregion
    
  }
}