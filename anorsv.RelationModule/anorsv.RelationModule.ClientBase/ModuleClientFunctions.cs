﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using docRelation = anorsv.RelationModule;

namespace anorsv.RelationModule.Client
{
  public class ModuleFunctions
  {

    #region Добавление, удаление, изменение связи документов
    
    /// <summary>
    /// Диалог добавления связанных документов во вложения при старте задачи
    /// </summary>
    [Public]
    public void AddOtherGroupAttachmentsDialog(Sungero.Docflow.IApprovalTask currentTask, Sungero.Docflow.IOfficialDocument document)
    {
      var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
      var documentList = document.Relations.GetRelated().Union(document.Relations.GetRelatedFrom())
        .Where(d => !currentTask.AllAttachments.Contains(d))
        .OrderBy(d => d.Name)
        .ToList();
      //.Where(d => !documentAddenda.Contains(d))
      //.Where(d => !currentTask.OtherGroup.All.Contains(d))
      
      if (documentList.Any())
      {
        // Создать диалог выбора вкладываемых связанных документов
        var dialog = Dialogs.CreateInputDialog(docRelation.Resources.AddOtherAttachmentDialogTitle);
        var selectedDocuments = dialog
          .AddSelectMany(docRelation.Resources.AddOtherAttachmentDialogDocsText, false, Sungero.Content.ElectronicDocuments.Null)
          .From(documentList.ToArray());
        
        if (ClientApplication.ApplicationType == ApplicationType.Web)
          dialog.Width = 850;
        dialog.Buttons.AddOkCancel();
        dialog.Buttons.Default = DialogButtons.Ok;
        
        if (dialog.Show() == DialogButtons.Ok)
        {
          //Добавить документ в область вложений
          foreach (var selectedDocument in selectedDocuments.Value)
          {
            currentTask.OtherGroup.All.Add(selectedDocument);
          }
        }
        
      }
      else
      {
        // Не отображаем диалог при отсутствии связанных документов
        return;
      }
    }
    
    /// <summary>
    /// Диалог добавления связанных документов во вложения при старте задачи согласования
    /// </summary>
    [Public]
    public void AddOtherGroupAttachmentsDialog(Sungero.RecordManagement.IAcquaintanceTask currentTask, Sungero.Docflow.IOfficialDocument document)
    {
      var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
      var documentList = document.Relations.GetRelated().Union(document.Relations.GetRelatedFrom())
        .Where(d => !documentAddenda.Contains(d))
        .Where(d => !currentTask.OtherGroup.All.Contains(d))
        .OrderBy(d => d.Name)
        .ToList();
      
      if (documentList.Any())
      {
        // Создать диалог выбора вкладываемых связанных документов
        var dialog = Dialogs.CreateInputDialog(docRelation.Resources.AddOtherAttachmentDialogTitle);
        var selectedDocuments = dialog
          .AddSelectMany(docRelation.Resources.AddOtherAttachmentDialogDocsText, false, Sungero.Content.ElectronicDocuments.Null)
          .From(documentList.ToArray());
        
        if (ClientApplication.ApplicationType == ApplicationType.Web)
          dialog.Width = 850;
        dialog.Buttons.AddOkCancel();
        dialog.Buttons.Default = DialogButtons.Ok;
        
        if (dialog.Show() == DialogButtons.Ok)
        {
          //Добавить документ в область вложений
          foreach (var selectedDocument in selectedDocuments.Value)
          {
            currentTask.OtherGroup.All.Add(selectedDocument);
          }
        }
        
      }
      else
      {
        // Не отображаем диалог при отсутствии связанных документов
        return;
      }
    }
    
    /// <summary>
    /// Диалог добавление связи с документом
    /// </summary>
    [Public]
    public void AddDocumentLinkDialog(Sungero.Docflow.IOfficialDocument document, int? assignmentId)
    {
      //Проверим что управление связями не запрещено
      if (document.State.IsInserted)
      {
        return;
      }

      // Создать диалог поиска документов по критиерям
      var dialogDocumentSearch = Dialogs.CreateSearchDialog<Sungero.Docflow.IOfficialDocument>(docRelation.Resources.DocumentSearchDialogTitle);
      
      if (dialogDocumentSearch.Show())
      {
        var documentList = dialogDocumentSearch.GetQuery();
        
        // Значения по умолчанию
        var mainDocumentList = new List<Sungero.Docflow.IOfficialDocument>();
        mainDocumentList.Add(document);
        // Если не найдено документов, прервать
        if (documentList.Count() == 0)
        {
          Dialogs.NotifyMessage(docRelation.Resources.DocumentSearchNoResultText);
          return;
        }
        
        var selectedLinkType = RelationTypes.Null;
        var banForPurchaseTypeList = RelationTypes.GetAll(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName ||
                                                          r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName ||
                                                          r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName ||
                                                          r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName).ToList();
        var relationTypeList = new List<IRelationType>();
        var relationNameList = new List<string>();
        var sourceNameList = new List<string>();
        var targetNameList = new List<string>();
        var inTaskList = new List<Sungero.Workflow.ITask>();
        
        // Создать диалог настройки связи
        var dialog = Dialogs.CreateInputDialog(docRelation.Resources.AddDocumentLinkDialogTitle);
        var mainDocument = dialog
          .AddSelect(docRelation.Resources.AddDocumentLinkDialogMainDocText, true, document)
          .From(mainDocumentList);
        mainDocument.IsEnabled = false;
        var selectedDocument = dialog
          .AddSelect(docRelation.Resources.AddDocumentLinkDialogDocText, true, Sungero.Content.ElectronicDocuments.Null)
          .From(documentList);
        var selectedLinkTypeName = dialog
          .AddSelect(docRelation.Resources.AddDocumentLinkDialogLinkText, true, string.Empty)
          .From(relationNameList.ToArray());
        //.From(relationTypeList.OrderBy(r => r.TargetDescription).Select((r => r.TargetDescription)).ToArray());   //31-10-22
        selectedLinkTypeName.IsEnabled = false;
        var addInTaskAttachment = dialog
          .AddSelectMany(docRelation.Resources.AddDocumentLinkDialogAttachText, false, Sungero.Workflow.Tasks.Null)
          .From(inTaskList.ToArray());
        addInTaskAttachment.IsEnabled = false;
        
        // Если найден 1 документ, указать для него значения по умолчанию
        if (documentList.Count() == 1)
        {
          selectedDocument.Value = documentList.First();
          selectedLinkTypeName.IsEnabled = true;
          var mainDocType = document.DocumentKind.DocumentType;
          var mainDocTypeGuid = mainDocType.DocumentTypeGuid.ToString();
          var relatedDocTypeGuid = string.Empty;
          if (Sungero.Docflow.OfficialDocuments.Is(selectedDocument.Value))
            relatedDocTypeGuid = Sungero.Docflow.OfficialDocuments.As(selectedDocument.Value).DocumentKind.DocumentType.DocumentTypeGuid.ToString();
          
          relationTypeList = RelationTypes.GetAllCached()
            .Where(r => r.Status.Value.ToString() == "Active")
            .Where(r => !r.Mapping.Any() ||
                   (r.UseTarget == true && r.Mapping.Where(m => (m.SourceType.ToString() == mainDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info) &&
                                                           (m.TargetType.ToString() == relatedDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info)).Any()) ||
                   (r.UseSource == true && r.Mapping.Where(m => (m.TargetType.ToString() == mainDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info) &&
                                                           (m.SourceType.ToString() == relatedDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info)).Any()))
            .Where(r => !banForPurchaseTypeList.Contains(r))
            .ToList();
          
          targetNameList = relationTypeList
            .Where(r => r.UseTarget == true)
            .Where(r => !r.Mapping.Any() || r.Mapping.Where(m => (m.SourceType.ToString() == mainDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info) &&
                                                            (m.TargetType.ToString() == relatedDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info)).Any())
            .Select(r => r.TargetTitle)
            .ToList();
          sourceNameList = relationTypeList
            .Where(r => r.UseSource == true)
            .Where(r => !r.Mapping.Any() || r.Mapping.Where(m => (m.TargetType.ToString() == mainDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info) &&
                                                            (m.SourceType.ToString() == relatedDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info)).Any())
            .Select(r => r.SourceTitle)
            .ToList();
          relationNameList = targetNameList.Union(sourceNameList).ToList();
          selectedLinkTypeName.From(relationNameList.ToArray());
          selectedLinkTypeName.Value = relationNameList.ToArray().FirstOrDefault();
          //selectedLinkTypeName.From(relationTypeList.Select((r => r.TargetDescription)).ToArray()); //31-10-22
          //if (relationTypeList.Count() == 1)
          //    selectedLinkTypeName.Value = relationTypeList.Select((r => r.TargetDescription)).ToArray().First(); //31-10-22
          
          addInTaskAttachment.IsEnabled = true;
          inTaskList = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTaskInProcess(document.Id, Users.Current);
          addInTaskAttachment.From(inTaskList.ToArray());
          if (inTaskList.Count() == 1)
            addInTaskAttachment.Value = inTaskList.ToArray();
        }
        
        selectedDocument.SetOnValueChanged(
          (e) =>
          {
            if (e.NewValue == null)
            {
              selectedLinkTypeName.IsEnabled = false;
              selectedLinkTypeName.Value = null;
              relationTypeList.Clear();
              
              addInTaskAttachment.IsEnabled = false;
              addInTaskAttachment.Value = null;
              inTaskList.Clear();
              
              return;
            }
            else
            {
              selectedLinkTypeName.IsEnabled = true;
              var mainDocType = document.DocumentKind.DocumentType;
              var mainDocTypeGuid = mainDocType.DocumentTypeGuid.ToString();
              var relatedDocTypeGuid = string.Empty;
              if (Sungero.Docflow.OfficialDocuments.Is(e.NewValue))
                relatedDocTypeGuid = Sungero.Docflow.OfficialDocuments.As(e.NewValue).DocumentKind.DocumentType.DocumentTypeGuid.ToString();
              
              // Доступные для связываемого документа типы связей
              relationTypeList = RelationTypes.GetAllCached()
                .Where(r => r.Status.Value.ToString() == "Active")
                .Where(r => !r.Mapping.Any() ||
                       (r.UseTarget == true && r.Mapping.Where(m => (m.SourceType.ToString() == mainDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info) &&
                                                               (m.TargetType.ToString() == relatedDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info)).Any()) ||
                       (r.UseSource == true && r.Mapping.Where(m => (m.TargetType.ToString() == mainDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info) &&
                                                               (m.SourceType.ToString() == relatedDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info)).Any()))
                .Where(r => !banForPurchaseTypeList.Contains(r))
                .ToList();
              targetNameList = relationTypeList
                .Where(r => r.UseTarget == true)
                .Where(r => !r.Mapping.Any() || r.Mapping.Where(m => (m.SourceType.ToString() == mainDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info) &&
                                                                (m.TargetType.ToString() == relatedDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info)).Any())
                .Select(r => r.TargetTitle)
                .ToList();
              sourceNameList = relationTypeList
                .Where(r => r.UseSource == true)
                .Where(r => !r.Mapping.Any() || r.Mapping.Where(m => (m.TargetType.ToString() == mainDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info) &&
                                                                (m.SourceType.ToString() == relatedDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info)).Any())
                .Select(r => r.SourceTitle)
                .ToList();
              relationNameList = targetNameList.Union(sourceNameList).ToList();
              
              //selectedLinkTypeName.From(relationTypeList.OrderBy(r => r.TargetDescription).Select((r => r.TargetDescription)).ToArray());
              //if (relationTypeList.Count() == 1)
              //  selectedLinkTypeName.Value = relationTypeList.Select((r => r.TargetDescription)).ToArray().First();
              selectedLinkTypeName.From(relationNameList.ToArray());
              selectedLinkTypeName.Value = relationNameList.ToArray().FirstOrDefault();
              
              // Существующие для связываемого документа задачи
              addInTaskAttachment.IsEnabled = true;
              inTaskList = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTaskInProcess(document.Id, Users.Current);
              addInTaskAttachment.From(inTaskList.ToArray());
              if (inTaskList.Count() == 1)
                addInTaskAttachment.Value = inTaskList.ToArray();
            }
          });
        
        if (ClientApplication.ApplicationType == ApplicationType.Web)
          dialog.Width = 850;
        
        if (dialog.Show() == DialogButtons.Ok)
        {
          // Выбранный тип связи
          selectedLinkType = RelationTypes.GetAll()
            .Where(r => (r.TargetTitle.Equals(selectedLinkTypeName.Value) && targetNameList.Contains(selectedLinkTypeName.Value)) ||
                   (r.SourceTitle.Equals(selectedLinkTypeName.Value) && sourceNameList.Contains(selectedLinkTypeName.Value)))
            .FirstOrDefault();
          //selectedLinkType = RelationTypes.GetAll().Where(r => r.TargetDescription.Equals(selectedLinkTypeName.Value)).FirstOrDefault();  //31-10-22
          
          // Добавить связь с основным документом
          //document.Relations.Add(selectedLinkType.Name, selectedDocument.Value);  //31-10-22
          if (targetNameList.Contains(selectedLinkTypeName.Value))
            document.Relations.Add(selectedLinkType.Name, selectedDocument.Value);
          else
            document.Relations.AddFrom(selectedLinkType.Name, selectedDocument.Value);
          document.Relations.Save();
          
          // Добавить документ в область вложений задачи
          foreach (var task in addInTaskAttachment.Value)
          {
            docRelation.PublicFunctions.Module.Remote.AddRelatedDocInAttachments(task.Id, selectedDocument.Value.Id, assignmentId);
          }
          
          Dialogs.NotifyMessage(string.Format(docRelation.Resources.RelationAddedText, selectedDocument.Value));
        }
      }
      
    }
    
    /// <summary>
    /// Диалог удаления связи с документом
    /// </summary>
    [Public]
    public void DeleteDocumentLinkDialog(Sungero.Docflow.IOfficialDocument document)
    {
      var availableTypeList = new List<IRelationType>();
      var banTypeList = RelationTypes.GetAll()
        .Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName ||
               r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName ||
               r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName ||
               r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName)
        .ToList();
      DeleteDocumentLinkDialog(document, availableTypeList, banTypeList);
    }
    
    [Public]
    public void DeleteDocumentLinkDialog(Sungero.Docflow.IOfficialDocument document, List<IRelationType> availableTypeList)
    {
      var banTypeList = RelationTypes.GetAll()
        .Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName ||
               r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName ||
               r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName ||
               r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName)
        .ToList();
      DeleteDocumentLinkDialog(document, availableTypeList, banTypeList);
    }
    
    [Public]
    public void DeleteDocumentLinkDialog(Sungero.Docflow.IOfficialDocument document, List<IRelationType> availableTypeList, List<IRelationType> banTypeList)
    {
      //Проверим что управление связями не запрещено
      if (document.State.IsInserted)
      {
        return;
      }
      
      // Значения по умолчанию
      var mainDocumentList = new List<Sungero.Docflow.IOfficialDocument>();
      mainDocumentList.Add(document);
      var documentList = document.Relations.GetRelated().Union(document.Relations.GetRelatedFrom());
      var selectedLinkType = RelationTypes.Null;
      // Скорректируем список запрещенных к выбору типов связи, если есть прямое разрешение
      if (availableTypeList.Any())
        banTypeList = banTypeList.Where(r => !availableTypeList.Contains(r)).ToList();
      var relationTypeList = new List<IRelationType>();
      var relationNameList = new List<string>();
      var sourceNameList = new List<string>();
      var targetNameList = new List<string>();
      var inTaskList = new List<Sungero.Workflow.ITask>();

      if (documentList.Any())
      {
        // Создать диалог удаления связи
        var dialog = Dialogs.CreateInputDialog(docRelation.Resources.DeleteDocumentLinkDialogTitle);
        var mainDocument = dialog
          .AddSelect(docRelation.Resources.AddDocumentLinkDialogMainDocText, true, document)
          .From(mainDocumentList);
        mainDocument.IsEnabled = false;
        var selectedDocument = dialog
          .AddSelect(docRelation.Resources.DeleteDocumentLinkDialogDocText, true, Sungero.Content.ElectronicDocuments.Null)
          .From(documentList);
        var selectedLinkTypeName = dialog
          .AddSelect(docRelation.Resources.DeleteDocumentLinkDialogLinkText, true, string.Empty)
          .From(relationNameList.ToArray());
        //  .From(relationTypeList.OrderBy(r => r.TargetDescription).Select((r => r.TargetDescription)).ToArray()); //31-10-22
        selectedLinkTypeName.IsEnabled = false;
        var deleteInTaskAttachment = dialog
          .AddSelectMany(docRelation.Resources.DeleteDocumentLinkDialogAttachText, false, Sungero.Workflow.Tasks.Null)
          .From(inTaskList.ToArray());
        deleteInTaskAttachment.IsEnabled = false;
        
        // Если найден 1 связанный документ, указать для него значения по умолчанию
        if (documentList.Count() == 1)
        {
          selectedDocument.Value = documentList.First();
          selectedLinkTypeName.IsEnabled = true;
          relationTypeList = RelationTypes.GetAllCached()
            .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value) || document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
            .ToList();
          targetNameList = RelationTypes.GetAllCached()
            .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value))
            .Where(r => r.UseTarget == true)
            .Select(r => r.TargetTitle)
            .ToList();
          sourceNameList = RelationTypes.GetAllCached()
            .Where(r => document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
            .Where(r => r.UseSource == true)
            .Select(r => r.SourceTitle)
            .ToList();
          relationNameList = targetNameList.Union(sourceNameList).ToList();
          selectedLinkTypeName.From(relationNameList.ToArray());
          selectedLinkTypeName.Value = relationNameList.ToArray().FirstOrDefault();

          //selectedLinkTypeName.From(relationTypeList.OrderBy(r => r.TargetDescription).Select((r => r.TargetDescription)).ToArray());
          //if (relationTypeList.Count() == 1)
          //  selectedLinkTypeName.Value = relationTypeList.Select((r => r.TargetDescription)).ToArray().First();   //31-10-22
          
          deleteInTaskAttachment.IsEnabled = true;
          inTaskList = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTaskInProcess(selectedDocument.Value.Id, Users.Current);
          deleteInTaskAttachment.From(inTaskList.ToArray());
          if (inTaskList.Count() == 1)
            deleteInTaskAttachment.Value = inTaskList.ToArray();
        }
        
        selectedDocument.SetOnValueChanged(
          (e) =>
          {
            if (e.NewValue == null)
            {
              selectedLinkTypeName.IsEnabled = false;
              selectedLinkTypeName.Value = null;
              relationTypeList.Clear();
              
              deleteInTaskAttachment.IsEnabled = false;
              deleteInTaskAttachment.Value = null;
              inTaskList.Clear();
              
              return;
            }
            else
            {
              // Существующие для связанного документа типы связей
              selectedLinkTypeName.IsEnabled = true;
              relationTypeList = RelationTypes.GetAllCached()
                .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value) || document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
                .ToList();
              targetNameList = RelationTypes.GetAllCached()
                .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value))
                .Where(r => r.UseTarget == true)
                .Select(r => r.TargetTitle)
                .ToList();
              sourceNameList = RelationTypes.GetAllCached()
                .Where(r => document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
                .Where(r => r.UseSource == true)
                .Select(r => r.SourceTitle)
                .ToList();
              relationNameList = targetNameList.Union(sourceNameList).ToList();
              selectedLinkTypeName.From(relationNameList.ToArray());
              selectedLinkTypeName.Value = relationNameList.ToArray().FirstOrDefault();
              //selectedLinkTypeName.From(relationTypeList.OrderBy(r => r.TargetDescription).Select((r => r.TargetDescription)).ToArray());
              //if (relationTypeList.Count() == 1)
              //  selectedLinkTypeName.Value = relationTypeList.Select((r => r.TargetDescription)).ToArray().First();   //31-10-22
              
              // Существующие для связанного документа задачи
              deleteInTaskAttachment.IsEnabled = true;
              inTaskList = anorsv.ApprovalTaskModule.PublicFunctions.Module.Remote.GetTaskInProcess(selectedDocument.Value.Id, Users.Current);
              deleteInTaskAttachment.From(inTaskList.ToArray());
              if (inTaskList.Count() == 1)
                deleteInTaskAttachment.Value = inTaskList.ToArray();
            }
          });
        
        
        if (ClientApplication.ApplicationType == ApplicationType.Web)
          dialog.Width = 850;
        dialog.Buttons.AddOkCancel();
        dialog.Buttons.Default = DialogButtons.Ok;
        
        if (dialog.Show() == DialogButtons.Ok)
        {
          // Вычислим тип связи
          //selectedLinkType = RelationTypes.GetAll()
          //  .Where(r => r.TargetTitle.Equals(selectedLinkTypeName.Value) ||
          //         r.SourceTitle.Equals(selectedLinkTypeName.Value))
          //  .FirstOrDefault();
          if (targetNameList.Contains(selectedLinkTypeName.Value))
          {
            selectedLinkType = RelationTypes.GetAllCached()
              .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value))
              .Where(r => r.TargetTitle.Equals(selectedLinkTypeName.Value))
              .FirstOrDefault();
          }
          if (sourceNameList.Contains(selectedLinkTypeName.Value) && selectedLinkType == null)
          {
            selectedLinkType = RelationTypes.GetAllCached()
              .Where(r => document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
              .Where(r => r.SourceTitle.Equals(selectedLinkTypeName.Value))
              .FirstOrDefault();
          }
          
          if (banTypeList.Contains(selectedLinkType))
          {
            // Сообщение о запрете удаления связи
            if (anorsv.DocflowModule.LNAs.Is(document) || sline.RSV.Orders.Is(document))
              Dialogs.ShowMessage(string.Format(docRelation.Resources.CantDeleteRelationImpOrderText, selectedDocument.Value), MessageType.Error);
            else  
              Dialogs.ShowMessage(string.Format(docRelation.Resources.CantDeleteRelationText, selectedDocument.Value), MessageType.Error);
            return;
          }
          else
          {
            // Удалить ссылку на основной документ в качестве ведущего
            var relatedDoc = anorsv.OfficialDocument.OfficialDocuments.As(selectedDocument.Value);
            if (relatedDoc.LeadingDocument != null && relatedDoc.LeadingDocument.Equals(document))
            {
              //relatedDoc.LeadingDocument = null;
              //relatedDoc.Save();
              Functions.Module.Remote.DeleteLeadingDocRelation(relatedDoc.Id, document.Id, selectedLinkType.Name);
            }
            else if (document.LeadingDocument != null && document.LeadingDocument.Equals(relatedDoc))
            {
              //document.LeadingDocument = null;
              //document.Save();
              Functions.Module.Remote.DeleteLeadingDocRelation(document.Id, relatedDoc.Id, selectedLinkType.Name);
            }
            else
            {
              document.Relations.Remove(selectedLinkType.Name, selectedDocument.Value);
              document.Relations.Save();
            }
            
            // Удалить связь с основным документом
            //document.Relations.Remove(selectedLinkType.Name, selectedDocument.Value);
            //document.Relations.Save();
            //document.Save();
            Dialogs.NotifyMessage(string.Format(docRelation.Resources.RelationDeletedText, selectedDocument.Value));
            
            // Удалить документ из области вложений задачи
            var taskIdList = new List<int>();
            foreach (var task in deleteInTaskAttachment.Value)
            {
              taskIdList.Add(task.Id);
              docRelation.PublicFunctions.Module.Remote.DeleteRelatedDocInAttachments(task.Id, selectedDocument.Value.Id);
            }
            
            // Отправить уведомления об удалении связи
            if (taskIdList.Any())
              anorsv.AuditModule.PublicFunctions.Module.Remote.CreateDeletedRelationNotificationTask(document.Id, selectedDocument.Value.Id, taskIdList, selectedLinkTypeName.Value);
            //anorsv.AuditModule.PublicFunctions.Module.Remote.CreateDeletedRelationNotificationTask(document.Id, selectedDocument.Value.Id, taskIdList, selectedLinkType.TargetTitle); //31-10-22
          }
        }
        
      }
      else
      {
        // Сообщить об отсутствии связанных документов
        Dialogs.NotifyMessage(docRelation.Resources.NoRelatedDocumentsText);
        return;
      }
    }
    
    /// <summary>
    /// Диалог изменения связи с документом
    /// </summary>
    [Public]
    public void ChangeDocumentLinkDialog(Sungero.Docflow.IOfficialDocument document)
    {
      var availableTypeList = new List<IRelationType>();
      var banTypeList = RelationTypes.GetAll()
        .Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName ||
               r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName ||
               r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName ||
               r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName)
        .ToList();
      ChangeDocumentLinkDialog(document, availableTypeList, banTypeList);
    }
    
    [Public]
    public void ChangeDocumentLinkDialog(Sungero.Docflow.IOfficialDocument document, List<IRelationType> availableTypeList)
    {
      var banTypeList = RelationTypes.GetAll()
        .Where(r => r.Name == sline.CustomModule.Resources.MemoForPurchaseRelationName ||
               r.Name == sline.CustomModule.Resources.ContractForPurchaseRelationName ||
               r.Name == sline.CustomModule.Resources.TSForPurchaseRelationName ||
               r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName)
        .ToList();
      ChangeDocumentLinkDialog(document, availableTypeList, banTypeList);
    }
    
    [Public]
    public void ChangeDocumentLinkDialog(Sungero.Docflow.IOfficialDocument document, List<IRelationType> availableTypeList, List<IRelationType> banTypeList)
    {
      //Проверим что управление связями не запрещено
      if (document.State.IsInserted)
      {
        return;
      }

      // Значения по умолчанию
      var mainDocumentList = new List<Sungero.Docflow.IOfficialDocument>();
      mainDocumentList.Add(document);
      var documentList = document.Relations.GetRelated().Union(document.Relations.GetRelatedFrom());
      var mainDocType = document.DocumentKind.DocumentType;
      var mainDocTypeGuid = mainDocType.DocumentTypeGuid.ToString();
      var relatedDocType = Sungero.Docflow.DocumentTypes.Null;
      var relatedDocTypeGuid = string.Empty;
      var selectedLinkType = RelationTypes.Null;
      var currentLinkType = RelationTypes.Null;
      var relationTypeList = new List<IRelationType>();
      var relationNameList = new List<string>();
      var sourceNameList = new List<string>();
      var targetNameList = new List<string>();
      var currentRelationTypeList = new List<IRelationType>();
      var currentRelationNameList = new List<string>();
      var currentSourceNameList = new List<string>();
      var currentTargetNameList = new List<string>();
      
      if (documentList.Any())
      {
        // Создать диалог изменения связи
        var dialog = Dialogs.CreateInputDialog(docRelation.Resources.ChangeDocumentLinkDialogTitle);
        var mainDocument = dialog
          .AddSelect(docRelation.Resources.AddDocumentLinkDialogMainDocText, true, document)
          .From(mainDocumentList);
        mainDocument.IsEnabled = false;
        var selectedDocument = dialog
          .AddSelect(docRelation.Resources.AddDocumentLinkDialogDocText, true, Sungero.Content.ElectronicDocuments.Null)
          .From(documentList);
        var currentLinkTypeName = dialog
          .AddSelect(docRelation.Resources.DeleteDocumentLinkDialogLinkText, true, string.Empty)
          .From(currentRelationNameList.ToArray());
        //  .From(currentRelationTypeList.OrderBy(r => r.TargetDescription).Select((r => r.TargetDescription)).ToArray());  //31-10-22
        currentLinkTypeName.IsEnabled = false;
        var selectedLinkTypeName = dialog
          .AddSelect(docRelation.Resources.AddDocumentLinkDialogLinkText, true, string.Empty)
          .From(relationNameList.ToArray());
        //  .From(relationTypeList.Where(r => !currentRelationTypeList.Contains(r)).OrderBy(r => r.TargetDescription).Select((r => r.TargetDescription)).ToArray());  //31-10-22
        selectedLinkTypeName.IsEnabled = false;
        
        // Если найден 1 связанный документ, указать для него значения по умолчанию
        if (documentList.Count() == 1)
        {
          selectedDocument.Value = documentList.First();
          currentLinkTypeName.IsEnabled = true;
          currentRelationTypeList = RelationTypes.GetAllCached()
            .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value) || document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
            .ToList();
          currentTargetNameList = RelationTypes.GetAllCached()
            .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value))
            .Where(r => r.UseTarget == true)
            .Select(r => r.TargetTitle)
            .ToList();
          currentSourceNameList = RelationTypes.GetAllCached()
            .Where(r => document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
            .Where(r => r.UseSource == true)
            .Select(r => r.SourceTitle)
            .ToList();
          currentRelationNameList = currentTargetNameList.Union(currentSourceNameList).ToList();
          
          currentLinkTypeName.From(currentRelationNameList.ToArray());
          currentLinkTypeName.Value = currentRelationNameList.ToArray().FirstOrDefault();
          //currentLinkTypeName.From(currentRelationTypeList.OrderBy(r => r.TargetDescription).Select((r => r.TargetDescription)).ToArray());
          //if (currentRelationTypeList.Count() == 1)
          //  currentLinkTypeName.Value = currentRelationTypeList.Select((r => r.TargetDescription)).ToArray().First();   //31-10-22
          selectedLinkTypeName.IsEnabled = true;
          
          if (Sungero.Docflow.OfficialDocuments.Is(selectedDocument.Value))
          {
            relatedDocType = Sungero.Docflow.OfficialDocuments.As(selectedDocument.Value).DocumentKind.DocumentType;
            relatedDocTypeGuid = relatedDocType.DocumentTypeGuid.ToString();
          }
          relationTypeList = RelationTypes.GetAllCached()
            .Where(r => r.Status.Value.ToString() == "Active")
            .Where(r => !r.Mapping.Any() ||
                   (r.UseTarget == true && r.Mapping.Where(m => (m.SourceType.ToString() == mainDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info)).Any()) ||
                   (r.UseSource == true && r.Mapping.Where(m => (m.TargetType.ToString() == mainDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info)).Any()))
            .Where(r => !currentRelationTypeList.Contains(r))
            .ToList();
          targetNameList = relationTypeList
            .Where(r => r.UseTarget == true)
            .Where(r => !r.Mapping.Any() || r.Mapping.Where(m => (m.SourceType.ToString() == mainDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info)).Any())
            .Select(r => r.TargetTitle)
            .ToList();
          sourceNameList = relationTypeList
            .Where(r => r.UseSource == true)
            .Where(r => !r.Mapping.Any() || r.Mapping.Where(m => (m.TargetType.ToString() == mainDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info)).Any())
            .Select(r => r.SourceTitle)
            .ToList();
          relationNameList = targetNameList.Union(sourceNameList).ToList();
          
          selectedLinkTypeName.From(relationNameList.ToArray());
          selectedLinkTypeName.Value = relationNameList.ToArray().FirstOrDefault();
          //selectedLinkTypeName.From(relationTypeList.OrderBy(r => r.TargetDescription).Select(r => r.TargetDescription).ToArray());
          //if (relationTypeList.Count() == 1)
          //  selectedLinkTypeName.Value = relationTypeList.Select(r => r.TargetDescription).ToArray().First();   //31-10-22
        }
        
        selectedDocument.SetOnValueChanged(
          (e) =>
          {
            if (e.NewValue == null)
            {
              selectedLinkTypeName.IsEnabled = false;
              selectedLinkTypeName.Value = null;
              relationTypeList.Clear();
              
              currentLinkTypeName.IsEnabled = false;
              currentLinkTypeName.Value = null;
              currentRelationTypeList.Clear();
              
              return;
            }
            else
            {
              // Существующие для связанного документа типы связей
              currentLinkTypeName.IsEnabled = true;
              currentRelationTypeList = RelationTypes.GetAllCached()
                .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value) || document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
                .ToList();
              currentTargetNameList = RelationTypes.GetAllCached()
                .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value))
                .Where(r => r.UseTarget == true)
                .Select(r => r.TargetTitle)
                .ToList();
              currentSourceNameList = RelationTypes.GetAllCached()
                .Where(r => document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
                .Where(r => r.UseSource == true)
                .Select(r => r.SourceTitle)
                .ToList();
              currentRelationNameList = currentTargetNameList.Union(currentSourceNameList).ToList();
              
              currentLinkTypeName.From(currentRelationNameList.ToArray());
              currentLinkTypeName.Value = currentRelationNameList.ToArray().FirstOrDefault();
              //currentLinkTypeName.From(currentRelationTypeList.OrderBy(r => r.TargetDescription).Select(r => r.TargetDescription).ToArray());
              //if (currentRelationTypeList.Count() == 1)
              //  currentLinkTypeName.Value = currentRelationTypeList.Select(r => r.TargetDescription).ToArray().First();  //31-10-22
              
              selectedLinkTypeName.IsEnabled = true;
              if (Sungero.Docflow.OfficialDocuments.Is(e.NewValue))
              {
                relatedDocType = Sungero.Docflow.OfficialDocuments.As(e.NewValue).DocumentKind.DocumentType;
                relatedDocTypeGuid = relatedDocType.DocumentTypeGuid.ToString();
              }
              
              // Доступные для связанного документа типы связей
              relationTypeList = RelationTypes.GetAllCached()
                .Where(r => r.Status.Value.ToString() == "Active")
                .Where(r => !r.Mapping.Any() ||
                       (r.UseTarget == true && r.Mapping.Where(m => (m.SourceType.ToString() == mainDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info)).Any()) ||
                       (r.UseSource == true && r.Mapping.Where(m => (m.TargetType.ToString() == mainDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info)).Any()))
                .Where(r => !currentRelationTypeList.Contains(r))
                .ToList();
              targetNameList = relationTypeList
                .Where(r => r.UseTarget == true)
                .Where(r => !r.Mapping.Any() || r.Mapping.Where(m => (m.SourceType.ToString() == mainDocTypeGuid || m.Source == Sungero.Content.ElectronicDocuments.Info)).Any())
                .Select(r => r.TargetTitle)
                .ToList();
              sourceNameList = relationTypeList
                .Where(r => r.UseSource == true)
                .Where(r => !r.Mapping.Any() || r.Mapping.Where(m => (m.TargetType.ToString() == mainDocTypeGuid || m.Target == Sungero.Content.ElectronicDocuments.Info)).Any())
                .Select(r => r.SourceTitle)
                .ToList();
              relationNameList = targetNameList.Union(sourceNameList).ToList();
              
              selectedLinkTypeName.From(relationNameList.ToArray());
              selectedLinkTypeName.Value = relationNameList.ToArray().FirstOrDefault();
              
            }
          });
        
        if (ClientApplication.ApplicationType == ApplicationType.Web)
          dialog.Width = 850;
        dialog.Buttons.AddOkCancel();
        dialog.Buttons.Default = DialogButtons.Ok;
        
        if (dialog.Show() == DialogButtons.Ok)
        {
          //currentLinkType = RelationTypes.GetAll()
          //  .Where(r => r.TargetTitle.Equals(currentLinkTypeName.Value) ||
          //         r.SourceTitle.Equals(currentLinkTypeName.Value))
          //  .FirstOrDefault();
          if (currentTargetNameList.Contains(currentLinkTypeName.Value))
          {
            currentLinkType = RelationTypes.GetAllCached()
              .Where(r => document.Relations.GetRelated(r.Name).Contains(selectedDocument.Value))
              .Where(r => r.TargetTitle.Equals(currentLinkTypeName.Value))
              .FirstOrDefault();
          }
          if (currentSourceNameList.Contains(currentLinkTypeName.Value) && currentLinkType == null)
          {
            currentLinkType = RelationTypes.GetAllCached()
              .Where(r => document.Relations.GetRelatedFrom(r.Name).Contains(selectedDocument.Value))
              .Where(r => r.SourceTitle.Equals(currentLinkTypeName.Value))
              .FirstOrDefault();
          }
          var currentUseTarget = (currentTargetNameList.Contains(currentLinkTypeName.Value));
          
          // Выбранный тип связи
          selectedLinkType = RelationTypes.GetAll()
            .Where(r => (r.TargetTitle.Equals(selectedLinkTypeName.Value) && targetNameList.Contains(selectedLinkTypeName.Value)) ||
                   (r.SourceTitle.Equals(selectedLinkTypeName.Value) && sourceNameList.Contains(selectedLinkTypeName.Value)))
            .FirstOrDefault();
          var selectedUseTarget = (targetNameList.Contains(selectedLinkTypeName.Value));

          var relatedDocument = selectedDocument.Value;
          
          if (banTypeList.Contains(selectedLinkType) || banTypeList.Where(r => !availableTypeList.Contains(r)).Contains(currentLinkType))
          {
            // Сообщение о запрете изменения связи на связь с закупкой
            if (anorsv.DocflowModule.LNAs.Is(document) || sline.RSV.Orders.Is(document))
              Dialogs.ShowMessage(string.Format(docRelation.Resources.CantChangeRelationImplOrderText, selectedDocument.Value), MessageType.Error);
            else  
              Dialogs.ShowMessage(string.Format(docRelation.Resources.CantChangeRelationText, selectedDocument.Value), MessageType.Error);
            return;
          }
          else
          {
            // Определить типы документов для выбранной связи
            var targetGuidList = new List<string>();
            var targetInfoList = new List<Sungero.Domain.Shared.IEntityInfo>();
            
            foreach (var typeMapping in selectedLinkType.Mapping)
            {
              var targetGuid = typeMapping.TargetType.ToString();
              if (!targetGuidList.Contains(targetGuid))
              {
                targetGuidList.Add(targetGuid);
                targetInfoList.Add(typeMapping.Target);
              }
            }
            
            // Если невозможно связать документ текущего типа, то изменить тип связываемого документа
            if (!targetGuidList.Contains(relatedDocTypeGuid) && !targetGuidList.Contains(Sungero.Docflow.PublicConstants.DocumentTemplate.AllDocumentTypeGuid)) //targetTypeGuid.ToString()
            {
              // Типы документов, доступные для конвертации
              var convertationAllowedTo = anorsv.AnorsvMainSolution.DocumentTypes.As(relatedDocType).ConvertationAllowedCollectionanorsv
                .Select(c => c.ConvertationType.DocumentTypeGuid.ToString())
                .ToList();
              // Типы документов для выбранной связи, ограниченные по доступу для конвертации
              targetGuidList = targetGuidList.Where(t => convertationAllowedTo.Contains(t)).ToList();
              if (targetGuidList.Any())
                Functions.Module.ChangeDocTypeAndRelate(document, relatedDocument, targetGuidList, currentLinkType, selectedLinkType, currentUseTarget, selectedUseTarget);
              else
                Dialogs.NotifyMessage(docRelation.Resources.NoAllowedTypesText);
              return;
            }
            else
              docRelation.Functions.Module.Remote.UpdateRelation(document.Id, relatedDocument.Id, currentLinkType.Name, selectedLinkType.Name, currentUseTarget, selectedUseTarget);
            return;
          }
        }
      }
      else
      {
        Dialogs.NotifyMessage(docRelation.Resources.NoRelatedDocumentsText);
        return;
      }
    }
    
    /// <summary>
    /// Диалог изменения типа документа
    /// </summary>
    public virtual void ChangeDocTypeAndRelate(Sungero.Docflow.IOfficialDocument document, Sungero.Content.IElectronicDocument relatedDocument, List<string> typesGuid, Sungero.CoreEntities.IRelationType currentLinkType, Sungero.CoreEntities.IRelationType linkType, bool currentUseTarget, bool selectedUseTarget)
    {
      var convertedDoc = Sungero.Docflow.OfficialDocuments.Null;
      var selectedType = Sungero.Docflow.DocumentTypes.Null;
      var typeNameList = Sungero.Docflow.DocumentTypes.GetAll()
        .Where(t => t.Status.Equals(Sungero.Docflow.DocumentType.Status.Active))
        .Where(t => typesGuid.Contains(t.DocumentTypeGuid.ToString()))
        .OrderBy(t => t.Name)
        .Select(t => t.Name)
        .ToArray();
      
      // Создать диалог изменения связи
      var dialogChangeType = Dialogs.CreateInputDialog(docRelation.Resources.ChangeDocumentTypeTitle);
      var text = dialogChangeType.AddMultilineString(docRelation.Resources.AttentionTitle, false, docRelation.Resources.AttentionText);
      text.RowsCount(2);
      text.IsEnabled = false;
      var selectedTypeName = dialogChangeType
        .AddSelect(docRelation.Resources.ChangeDocumentTypeText, true)
        .From(typeNameList);
      if (typeNameList.Count() == 1)
        selectedTypeName.Value = typeNameList.FirstOrDefault();
      
      if (ClientApplication.ApplicationType == ApplicationType.Web)
        dialogChangeType.Width = 850;
      dialogChangeType.Buttons.AddOkCancel();
      dialogChangeType.Buttons.Default = DialogButtons.Ok;
      
      if (dialogChangeType.Show() == DialogButtons.Ok)
      {
        // Изменить тип документа на новый
        selectedType = Sungero.Docflow.DocumentTypes.GetAll()
          .Where(t => t.Name.Equals(selectedTypeName.Value))
          .FirstOrDefault();
        var selectedTypeInfo = linkType.Mapping
          .Where(m => m.TargetType.ToString() == selectedType.DocumentTypeGuid.ToString())
          .FirstOrDefault()
          .Target;
        
        convertedDoc = anorsv.OfficialDocument.OfficialDocuments.As(relatedDocument.ConvertTo(selectedTypeInfo));
        convertedDoc.ShowModal();
      }
      
      if (convertedDoc != null)
        if (convertedDoc.DocumentKind != null && convertedDoc.DocumentKind.DocumentType.Name == selectedTypeName.Value)
          docRelation.Functions.Module.Remote.UpdateRelation(document.Id, convertedDoc.Id, currentLinkType.Name, linkType.Name, currentUseTarget, selectedUseTarget);
    }
    
    #endregion
    
  }
}
