using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvIntergrationModule.AnorsvIntegrationSession;

namespace anorsv.AnorsvIntergrationModule
{
  partial class AnorsvIntegrationSessionSharedHandlers
  {

    public virtual void RespondingSystemChanged(anorsv.AnorsvIntergrationModule.Shared.AnorsvIntegrationSessionRespondingSystemChangedEventArgs e)
    {
      if (!e.NewValue.Equals(e.OldValue))
      {
        _obj.ExternalEntityType = null;
      }
    }

  }
}