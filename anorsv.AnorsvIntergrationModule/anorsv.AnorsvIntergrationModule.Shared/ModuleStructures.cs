﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvIntergrationModule;

namespace anorsv.AnorsvIntergrationModule.Structures.Module
{

  /// <summary>
  /// Структура описывающая статус элемента в очереди
  /// </summary>
  [Public]
  partial class ElementProcessingStatus
  {
    public int ElementId { get; set; }
    
    public int SessionId { get; set; }
    
    public Sungero.Core.Enumeration ProcessingStatus { get; set; }
    
    public Sungero.Core.Enumeration ProcessingResult { get; set; }
    
    public Sungero.Core.Enumeration ProcessingOperation { get; set; }
  }

  /// <summary>
  /// Структура предо
  /// </summary>
  [Public]
  partial class ImportingOperationResult
  {
    public int Total { get; set; }
    
    public int AwaitingProcessing { get; set; }
    
    public int Processed { get; set; }
    
    public int Successed { get; set; }
    
    public int Unsuccessed { get; set; }
    
    public int Created { get; set; }
    
    public int Updated { get; set; }
    
    public int Linked { get; set; }
  }
  
  /// <summary>
  /// Структура описывающая сотрудника привязанного к физическому лицу полученному из 1С ЗуП.
  /// </summary>
  [Public]
  partial class EmployementInformation
  {
      public string employeeGUID { get; set; }
      
      public string personnelNumber { get; set; }
      
      public string divisionGUID { get; set; }
      
      public string division { get; set; }
      
      public string positionGUID { get; set; }
      
      public string position { get; set; }
      
      public DateTime? startDate { get; set; }
      
      public bool isHeadEmployee { get; set; }
  }
  
  /// <summary>
  /// Структура описывающая физическое лицо полученное из 1С ЗуП.
  /// </summary>
  [Public]
  partial class PersonFrom1cZup
  {
    public Sungero.Core.Enumeration IngoingIdentificatorType { get; set; }
    
    public string IngoingIdentificator { get; set; }
    
    public Sungero.Core.Enumeration ProcessingStatus { get; set; }
    
    public DateTime UpToDate { get; set; }
    
    public DateTime? ProcessingEndDate { get; set; }
    
    public IAnorsvIntegrationSession IntegrationSession { get; set; }
    
    public bool Deleted { get; set; }
    
    public bool DeletionMark { get; set; }
    
    public string Code { get; set; }
    
    public string Hyperlink { get; set; }
    
    public string IngoingElementName { get; set; }
    
    public DateTime? BirthDate { get; set; }
    
    public Sungero.Core.Enumeration Sex { get; set; }
    
    public string FullName { get; set; }
    
    public string Surname { get; set; }
    
    public string Name { get; set; }
    
    public string Patronymic { get; set; }
    
    public List<anorsv.AnorsvIntergrationModule.Structures.Module.IEmployementInformation> EmployementsInformation { get; set; }
  }
  
  /// <summary>
  /// Структура описывающая подразделение полученное из 1С ЗуП.
  /// </summary>
  [Public]
  partial class DepartamentFrom1cZup
  {
    public Sungero.Core.Enumeration IngoingIdentificatorType { get; set; }
    
    public string IngoingIdentificator { get; set; }
    
    public Sungero.Core.Enumeration ProcessingStatus { get; set; }
    
    public DateTime UpToDate { get; set; }
    
    public DateTime? ProcessingEndDate { get; set; }
    
    public IAnorsvIntegrationSession IntegrationSession { get; set; }
    
    public bool Deleted { get; set; }
    
    public bool DeletionMark { get; set; }
    
    public string Code { get; set; }
    
    public string Hyperlink { get; set; }
    
    public string IngoingElementName { get; set; }
    
    public string HeadOffice { get; set; }
    
    public bool Formed { get; set; }
    
    public DateTime? CreationDate { get; set; }
    
    public bool Disbanded { get; set; }
    
    public DateTime? DissolutionDate { get; set; }
  }
  
}