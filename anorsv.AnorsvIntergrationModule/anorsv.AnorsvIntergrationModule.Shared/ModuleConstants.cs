﻿using System;
using Sungero.Core;

namespace anorsv.AnorsvIntergrationModule.Constants
{
  public static class Module
  {
    [Public]
    public static class InternalEntiyType
    {
      [Public]
      public static class Person
      {
        public  const string Status = "Active";
        
        public const string EntityTypeId = "8d3d3a18-f287-4b15-ba10-94c674c99f5b";
        
        public const string HumanReadableName = "Персона - Person";
        
        public const string InternalName = "Person";
      }

      [Public]
      public static class Deprtamenrt
      {
        public  const string Status = "Active";
        
        public const string EntityTypeId = "30f8074f-2ffa-4e2a-918c-83f494fa28b4";
        
        public const string HumanReadableName = "Подразделение - Departament";
        
        public const string InternalName = "Departament";
      }

      [Public]
      public static class Position
      {
        public  const string Status = "Active";
        
        public const string EntityTypeId = "070f5766-3075-4a85-a3c3-8b3e1ffa7c24";
        
        public const string HumanReadableName = "Должность - Position";
        
        public const string InternalName = "Position";
      }

      [Public]
      public static class Employee
      {
        public  const string Status = "Active";
        
        public const string EntityTypeId = "985b8518-ab5c-4481-93ff-138efb38259a";
        
        public const string HumanReadableName = "Работник - Employee";
        
        public const string InternalName = "Employee";
      }
    }
  }
}