﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvIntergrationModule.AnorsvDirectumSystemBase;

namespace anorsv.AnorsvIntergrationModule
{
  partial class AnorsvDirectumSystemBaseServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      _obj.CurrentSystem = false;
    }
  }

}