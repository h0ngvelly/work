using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvIntergrationModule.AnorsvIntegrationSession;

namespace anorsv.AnorsvIntergrationModule
{
  partial class AnorsvIntegrationSessionExternalEntityTypePropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ExternalEntityTypeFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = 
        query
        .Where(
          entityType =>
          _obj.RespondingSystem != null 
          && entityType.System.Equals(_obj.RespondingSystem)
         );
      
      return query;
    }
  }

  partial class AnorsvIntegrationSessionServerHandlers
  {

    public override void Deleting(Sungero.Domain.DeletingEventArgs e)
    {
      var elements = 
        anorsv.AnorsvIntergrationModule.AnorsvExternalIntergrationElements
        .GetAll(element => element.IntegrationSession.Equals(_obj))
        .Select(element => element.Id);
      
      foreach (var elementId in elements)
      {
        var element = anorsv.AnorsvIntergrationModule.AnorsvExternalIntergrationElements.Get(elementId);
        anorsv.AnorsvIntergrationModule.AnorsvExternalIntergrationElements.Delete(element);
      }
      
      base.Deleting(e);
    }

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      string requestingSystem = _obj.RequestingSystem != null ? _obj.RequestingSystem.DisplayValue : "";
      string respondingSystem = _obj.RespondingSystem != null ? _obj.RespondingSystem.DisplayValue : "";
      string id = _obj.Id.ToString();
      string sessionId = string.IsNullOrWhiteSpace(_obj.SessionId) ? "" : _obj.SessionId;
      
      _obj.Name = string.Format(
        "{0} => {1} (ID = {2}, SessionId = {3})"
        , requestingSystem
        , respondingSystem
        , id
        , sessionId
       );
    }
  }

}