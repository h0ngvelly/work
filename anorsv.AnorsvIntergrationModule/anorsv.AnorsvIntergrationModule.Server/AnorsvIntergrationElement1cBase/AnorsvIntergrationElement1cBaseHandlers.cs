﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvIntergrationModule.AnorsvIntergrationElement1cBase;

namespace anorsv.AnorsvIntergrationModule
{
  partial class AnorsvIntergrationElement1cBaseServerHandlers
  {

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      string id = _obj.Id.ToString();
      string ingoingIdentificator = string.IsNullOrWhiteSpace(_obj.IngoingIdentificator) ? "" : _obj.IngoingIdentificator;
      string ingoingName = _obj.IngoingElementName;
      string name = string.Format("{0} (Id = {1} ElementId = {2})", ingoingName, id, ingoingIdentificator);
      
      if (_obj.Name != name)
      {
        _obj.Name = name;
      }
    }
  }

}