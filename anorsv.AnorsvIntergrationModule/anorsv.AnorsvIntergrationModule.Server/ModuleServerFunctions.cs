﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain;
using Sungero.Domain.Shared;
using Sungero.Metadata;
using anorsv.AnorsvIntergrationModule;
using AnorsvIntegrationLibrary.PersonsFrom1cZup;
using AnorsvIntegrationLibrary.DepartamentsFrom1cZup;
using AnorsvIntegrationLibrary.PositionsFrom1cZup;
using anorsv.AnorsvIntergrationModule.AnorsvIntegrationSession1c;
using anorsv.AnorsvIntergrationModule.AnorsvIntergration1cZupPerson;
using anorsv.AnorsvIntergrationModule.AnorsvIntegration1cZupDepartament;
using AnorsvIntegrationLibrary.WebServiceClient;
using anorsv.AnorsvCoreModule;
using ServiceFunctions = anorsv.ServiceLibrary.PublicFunctions;
using Element = anorsv.AnorsvIntergrationModule.AnorsvExternalIntergrationElement;

namespace anorsv.AnorsvIntergrationModule.Server
{
  public class ModuleFunctions
  {
    #region Диалог первичной синхронизации
    
    [Remote(IsPure = true)]
    public static IQueryable<IAnorsvExternalEntityType> GetEntityTypesInSynchronizationDialog(int systemId)
    {
      var entityTypes = AnorsvExternalEntityTypes
        .GetAll(entityType => entityType.System.Id == systemId);
      
      return entityTypes;
    }
    
    [Remote(IsPure = true)]
    public static IQueryable<IAnorsvIntegrationSession> GetSessionsInSynchronizationDialog(int systemId, int ExternalEntityTypeId)
    {
      var sessions = AnorsvIntegrationSessions
        .GetAll(session => session.RespondingSystem.Id == systemId && session.ExternalEntityType.Id == ExternalEntityTypeId)
        .Where(
          session =>
          AnorsvExternalIntergrationElements
          .GetAll()
          .Where(
            element => element.IntegrationSession.Equals(session) && element.ProcessingStatus == Element.ProcessingStatus.AwaitProcessing)
          .Count() > 0);
      
      return sessions;
    }
    
    [Remote(IsPure = true)]
    public static string GetElementsProcessingStatusInSession(int sessionId)
    {
      string result = string.Empty;
      Dictionary<string, int> counts = new Dictionary<string, int>();
      List<Structures.Module.IElementProcessingStatus> elements =
        anorsv.AnorsvIntergrationModule.AnorsvExternalIntergrationElements
        .GetAll(element => element.IntegrationSession.Id == sessionId)
        .Select(
          element =>
          Structures.Module.ElementProcessingStatus.Create(
            element.Id
            , sessionId
            , element.ProcessingStatus.Value
            , element.ProcessingResult.HasValue ? element.ProcessingResult.Value : Element.ProcessingResult.Unknown
            , element.ProcessingOperation.HasValue ? element.ProcessingOperation.Value : Element.ProcessingOperation.Unknown
           )
         )
        .ToList();

      counts.Add("Total", elements.Count());
      counts.Add("AwaitingProcessing", elements.Count(element => element.ProcessingStatus.Equals(Element.ProcessingStatus.AwaitProcessing)));
      counts.Add("Processed", elements.Count(element => element.ProcessingStatus.Equals(Element.ProcessingStatus.Processed)));
      counts.Add("Successed", elements.Count(element => element.ProcessingResult.Equals(Element.ProcessingResult.Successful)));
      counts.Add("Unsuccessed", elements.Count(element => element.ProcessingResult.Equals(Element.ProcessingResult.Unseccessful)));
      counts.Add("Created", elements.Count(element => element.ProcessingOperation.Equals(Element.ProcessingOperation.Create)));
      counts.Add("Updated", elements.Count(element => element.ProcessingOperation.Equals(Element.ProcessingOperation.Update)));
      counts.Add("Linked", elements.Count(element => element.ProcessingOperation.Equals(Element.ProcessingOperation.Link)));
      
      result = ServiceFunctions.Module.BoxToString(counts);
      
      return result;
    }

    [Remote(IsPure = true)]
    public static List<int> GetElementsAwaitingProcessing(int sessionId)
    {
      List<int> elementIds =
        AnorsvExternalIntergrationElements
        .GetAll(element => element.IntegrationSession.Id == sessionId && element.ProcessingStatus == Element.ProcessingStatus.AwaitProcessing)
        .Select(element => element.Id)
        .ToList();
      
      return elementIds;
    }
    
    #endregion
    
    #region Работа с системами
    [Public]
    public static anorsv.AnorsvIntergrationModule.IAnorsvDirectumSystem GetMySystem()
    {
      return AnorsvDirectumSystems.GetAll(system => system.CurrentSystem.HasValue && system.CurrentSystem.Value).FirstOrDefault();
    }
    #endregion
    
    #region Универсальные поиски

    public static List<int> GetInternalEntityIdsByExternalId(Guid entityType, string externalSystemId, string externalEntityTypeId, Guid externalEntityId)
    {
      List<int> entityIds =
        GetExternalLinks(entityType, null, externalSystemId, externalEntityTypeId, externalEntityId)
        .Where(link => link.EntityId.HasValue)
        .Select(link => link.EntityId.Value)
        .ToList();
      
      return entityIds;
    }

    #endregion
    
    #region Физические лица
    
    public static bool NeedUpdatePersonByElementFrom1cZup(Sungero.Parties.IPerson person, IAnorsvIntergration1cZupPerson elementPerson, bool birthday)
    {
      bool result = false;
      
      result =
        person.LastName != elementPerson.Surname
        || person.FirstName != elementPerson.PersonName
        || person.MiddleName != elementPerson.Patronymic
        || (birthday && !person.DateOfBirth.Equals(elementPerson.BirthDay));
      
      return result;
    }
    
    public static Sungero.Parties.IPerson UpdatePersonByElementFrom1cZup(Sungero.Parties.IPerson person, IAnorsvIntergration1cZupPerson elementPerson)
    {
      if (person.LastName != elementPerson.Surname)
      {
        person.LastName = elementPerson.Surname;
      }
      
      if (person.FirstName != elementPerson.PersonName)
      {
        person.FirstName = elementPerson.PersonName;
      }

      if (person.MiddleName != elementPerson.Patronymic)
      {
        person.MiddleName = elementPerson.Patronymic;
      }

      if (!person.DateOfBirth.Equals(elementPerson.BirthDay))
      {
        person.DateOfBirth = elementPerson.BirthDay;
      }
      
      return person;
    }
    
    public static IQueryable<Sungero.Parties.IPerson> GetInternalPersonsByLinkFrom1cZup(IAnorsvIntergration1cZupPerson elementPerson)
    {
      Guid personTypeId = Guid.Parse("f5509cdc-ac0c-4507-a4d3-61d7a0a9b6f6");
      Guid elementExternalId = Guid.Parse(elementPerson.IngoingIdentificator);
      var personIds =
        GetExternalLinks(
          personTypeId
          , null
          , elementPerson.IntegrationSession.RespondingSystem.SystemId
          , "Person"
          , elementExternalId)
        .Select(link => link.EntityId);
      
      var persons = Sungero.Parties.People
        .GetAll(person => personIds.Contains(person.Id));
      
      return persons;
    }
    
    public static IQueryable<Sungero.Parties.IPerson> SearchInternalPersonsByElementFrom1cZup(IAnorsvIntergration1cZupPerson elementPerson, bool birthday, bool employee, bool strictSearchForEmployees)
    {
      List<int> personIds = new List<int>();
      
      List<string> employeesGuid = elementPerson.Employees
        .Where(emp => !string.IsNullOrEmpty(emp.EmployeeGuid))
        .Select(emp => emp.EmployeeGuid.ToLower())
        .ToList();
      
      var query = Sungero.Parties.People
        .GetAll(
          person => person.LastName == elementPerson.Surname
          && person.FirstName == elementPerson.PersonName
          && person.MiddleName == elementPerson.Patronymic
          && (
            (person.Sex == Sungero.Parties.Person.Sex.Male && elementPerson.Sex == Sex.Male)
            || (person.Sex == Sungero.Parties.Person.Sex.Female && elementPerson.Sex == Sex.Female)
           )
         );
      
      if (birthday)
      {
        query = query.Where(person => person.DateOfBirth == elementPerson.BirthDay);
      }
      
      if (employee)
      {
        if (strictSearchForEmployees && employeesGuid.Count > 0)
        {
          personIds = sline.RSV.Employees.GetAll(emp => employeesGuid.Contains(emp.GUIDsline.ToLower())).Select(emp => emp.Person.Id).ToList();
          query = query.Where(person => personIds.Contains(person.Id));
        }
        else
        {
          query.Where(person => sline.RSV.Employees.GetAll().Any(emp => emp.Person.Equals(person)));
        }
      }
      
      return query;
    }
    
    public static Sungero.Parties.IPerson CreatePersonFrom1cZup(IAnorsvIntergration1cZupPerson elementPerson)
    {
      Sungero.Parties.IPerson person = Sungero.Parties.People.Create();
      
      person.DateOfBirth = elementPerson.BirthDay;
      person.FirstName = elementPerson.PersonName;
      person.LastName = elementPerson.Surname;
      person.MiddleName = elementPerson.Patronymic;
      
      if (elementPerson.Sex == Sex.Male)
      {
        person.Sex = Sungero.Parties.Person.Sex.Male;
      }
      else if (elementPerson.Sex == Sex.Female)
      {
        person.Sex = Sungero.Parties.Person.Sex.Female;
      };
      
      person.Save();
      
      return person;
      
    }
    
    public static void SynchronyzePersonFrom1cZup(IAnorsvIntergration1cZupPerson element, bool needLinking, bool needCreating, bool needUpdating)
    {
      bool linked = false;
      bool created = false;
      bool updated = false;
      bool errorExist = false;
      List<Sungero.Parties.IPerson> linkedEntities = new List<Sungero.Parties.IPerson>();
      List<Sungero.Parties.IPerson> searchedEntities = new List<Sungero.Parties.IPerson>();
      List<Sungero.Parties.IPerson> newInSearchedEntities = new List<Sungero.Parties.IPerson>();
      
      if (element == null || !(needLinking || needCreating || needUpdating))
      {
        return;
      }
      
      if (element.ProcessingStatus != Element.ProcessingStatus.AwaitProcessing)
      {
        return;
      }
      
      if (Locks.TryLock(element))
      {
        element.ProcessingStatus = Element.ProcessingStatus.InProcessing;
        element.Save();
        
        linkedEntities = GetInternalPersonsByLinkFrom1cZup(element).ToList();
        
        if (needLinking)
        {
          searchedEntities = SearchInternalPersonsByElementFrom1cZup(element, false, true, false).ToList();
          newInSearchedEntities = searchedEntities.Where(entity => !linkedEntities.Contains(entity)).ToList();
          foreach(var entity in newInSearchedEntities)
          {
            CreateExternalLink(
              entity
              , element.IntegrationSession.RespondingSystem.SystemId
              , Guid.Parse(element.IngoingIdentificator)
              , element.IntegrationSession.ExternalEntityType.InternalEntityType.InternalName);
            linkedEntities.Add(entity);
          }
        }
        
        linked = needLinking && searchedEntities.Count > 0 && (searchedEntities.Count == linkedEntities.Count());
        
        if (needUpdating && linkedEntities.Count > 0)
        {
          foreach(var entity in linkedEntities)
          {
            bool entityDataDifferenced = NeedUpdatePersonByElementFrom1cZup(entity, element, true);
            updated = true;
            
            if (entityDataDifferenced)
            {
              if (Locks.TryLock(entity))
              {
                try
                {
                  UpdatePersonByElementFrom1cZup(entity, element).Save();
                }
                catch (Exception ex)
                {
                  updated = false;
                  errorExist = true;
                  
                }
              }
              else
              {
                updated = false;
                errorExist = true;
              }
            }
          }
        }
        
        if (needCreating && linkedEntities.Count == 0)
        {
          var entity = CreatePersonFrom1cZup(element);
          CreateExternalLink(
            entity
            , element.IntegrationSession.RespondingSystem.SystemId
            , Guid.Parse(element.IngoingIdentificator)
            , element.IntegrationSession.ExternalEntityType.InternalEntityType.InternalName);
          linkedEntities.Add(entity);
        }
        
        if (!errorExist && ((needLinking && linked) || (needCreating && created) || (needUpdating && updated) || (linkedEntities.Count > 0)))
        {
          element.ProcessingStatus = Element.ProcessingStatus.Processed;
          element.ProcessingResult = Element.ProcessingResult.Successful;
          element.ProcessingOperation = Element.ProcessingOperation.Unknown;
          if (created)
          {
            element.ProcessingOperation = Element.ProcessingOperation.Create;
          }
          else if (updated)
          {
            element.ProcessingOperation = Element.ProcessingOperation.Update;
          }
          else if (linked || linkedEntities.Count > 0)
          {
            element.ProcessingOperation = Element.ProcessingOperation.Link;
          }
        }
        else
        {
          element.ProcessingStatus = Element.ProcessingStatus.Processed;
          element.ProcessingResult = Element.ProcessingResult.Unseccessful;
          element.ProcessingOperation = Element.ProcessingOperation.Unknown;
        }
        
        if (element.State.IsChanged)
        {
          element.Save();
        }
        Locks.Unlock(element);
      }
    }
    
    [Remote(IsPure = false)]
    public static void SynchronyzePersonFrom1cZup(int elementPersonId, bool needLinking, bool needCreating, bool needUpdating)
    {
      IAnorsvIntergration1cZupPerson elementPerson = AnorsvIntergration1cZupPeople.Get(elementPersonId);
      SynchronyzePersonFrom1cZup(elementPerson, needLinking, needCreating, needUpdating);
    }
    
    /// <summary>
    /// Создаёт элемент персоны в очереди на обработку из структуры.
    /// </summary>
    /// <param name="source">Структура описывающая полученные данные из 1С ЗуП по персоне.</param>
    /// <returns>Созданная в очереди на обработку персона.</returns>
    public static IAnorsvIntergration1cZupPerson CreatePersonElement(Structures.Module.IPersonFrom1cZup source)
    {
      IAnorsvIntergration1cZupPerson person = AnorsvIntergration1cZupPeople.Create();
      
      // Базовые сведения для елементов из 1с
      person.Code = source.Code;
      person.Deleted = source.Deleted;
      person.DeletionMark = source.DeletionMark;
      person.Hyperlink = source.Hyperlink;
      person.IngoingElementName = source.IngoingElementName;
      person.IngoingIdentificator = source.IngoingIdentificator;
      person.IngoingIdentificatorType = source.IngoingIdentificatorType;
      person.IntegrationSession = source.IntegrationSession;
      person.ProcessingStatus = source.ProcessingStatus;
      person.ProcessingEndDate = source.ProcessingEndDate;
      person.UpToDate = source.UpToDate;

      // Сведенения специфичные для подразделения
      person.BirthDay = source.BirthDate;
      person.Patronymic = source.Patronymic;
      person.PersonName = source.Name;
      person.Sex = source.Sex;
      person.Surname = source.Surname;

      foreach (Structures.Module.IEmployementInformation employmentInformation in source.EmployementsInformation)
      {
        var employee = person.Employees.AddNew();
        employee.EmployeeGuid = employmentInformation.employeeGUID;
        employee.DivisionGuid = employmentInformation.divisionGUID;
        employee.DivisionName = employmentInformation.division;
        employee.IsHeadEmployee = employmentInformation.isHeadEmployee;
        employee.PersonelNumber = employmentInformation.personnelNumber;
        employee.PositionGuid = employmentInformation.positionGUID;
        employee.PositionName = employmentInformation.position;
        employee.StartDate = employmentInformation.startDate;
      }
      
      return person;
    }
    
    public static IAnorsvIntergration1cZupPerson CreatePersonElement(
      AnorsvIntegrationLibrary.PersonsFrom1cZup.PersonFrom1cZup personSourceData
      , IAnorsvIntegrationSession1c session
      , DateTime upToDate
     )
    {
      Structures.Module.IPersonFrom1cZup personSourceStructure = CreatePersonStructure(personSourceData, session, upToDate);
      
      bool personElementNotExistInQueue = true;
      var personElementsInQueue =
        AnorsvIntergration1cZupPeople
        .GetAll(
          element =>
          element.IngoingIdentificator == personSourceData.Guid
          && element.IntegrationSession.RespondingSystem.Equals(personSourceStructure.IntegrationSession.RespondingSystem)
          && element.ProcessingStatus != anorsv.AnorsvIntergrationModule.AnorsvIntergration1cZupPerson.ProcessingStatus.Processed)
        .Where(
          element =>
          element.BirthDay == personSourceStructure.BirthDate
          && element.Deleted == personSourceStructure.Deleted
          && element.DeletionMark == element.DeletionMark
          && element.Surname == personSourceStructure.Surname
          && element.PersonName == personSourceStructure.Name
          && element.Patronymic == personSourceStructure.Patronymic
          && element.Sex.Equals(personSourceStructure.Sex)
          && element.Employees.Count() == personSourceData.EmploymentInformation.Count)
        .ToList();
      
      if (personElementsInQueue.Count > 0)
      {
        foreach (var element in personElementsInQueue)
        {
          int countMatchedEmployee = 0;
          
          foreach(var employeeInformation in personSourceStructure.EmployementsInformation)
          {
            bool employeeMatched = element.Employees.Where(employee => employee.EmployeeGuid == employeeInformation.employeeGUID).Any();
            if (employeeMatched)
            {
              countMatchedEmployee++;
            }
          }
          if ((countMatchedEmployee == personSourceStructure.EmployementsInformation.Count && personSourceData.EmploymentInformation.Count > 0)
              || personSourceData.EmploymentInformation.Count == 0)
          {
            personElementNotExistInQueue = false;
            break;
          }
        }
      }
      
      if (personElementNotExistInQueue)
      {
        return CreatePersonElement(personSourceStructure);
      }
      
      return AnorsvIntergration1cZupPeople.Null;
    }
    
    /// <summary>
    /// Помещает полученные извне сведения о персонах в очередь для последующей обработки.
    /// </summary>
    /// <param name="persons">Список с данным персон.</param>
    /// <param name="session">Текущая сессия.</param>
    /// <param name="upToDate">Дата и время на которые актуальны полученные сведения.</param>
    /// <returns>Признак успешной операции размещения данных в очереди.</returns>
    public static bool PutPersonsInQueue(List<AnorsvIntegrationLibrary.PersonsFrom1cZup.PersonFrom1cZup> persons, IAnorsvIntegrationSession1c session, DateTime upToDate)
    {
      foreach (var personSourceData in persons.Where(element => element.IsFolder == false))
      {
        var person = CreatePersonElement(personSourceData, session, upToDate);
        if (person != null)
        {
          person.Save();
        }
      }
      
      return true;
    }
    
    public static Structures.Module.IPersonFrom1cZup CreatePersonStructure(
      AnorsvIntegrationLibrary.PersonsFrom1cZup.PersonFrom1cZup personSourceData
      , IAnorsvIntegrationSession1c session
      , DateTime upToDate
     )
    {
      Enumeration sex = personSourceData.Sex == "Мужской"
        ? anorsv.AnorsvIntergrationModule.AnorsvIntergration1cZupPerson.Sex.Male
        : anorsv.AnorsvIntergrationModule.AnorsvIntergration1cZupPerson.Sex.Female;
      
      DateTime parsedBirthDate;
      bool parseBirthDateSuccessfull = DateTime.TryParseExact(personSourceData.BirthDate, "s", null, DateTimeStyles.None, out parsedBirthDate);
      if (parseBirthDateSuccessfull && personSourceData.BirthDate == "0001-01-01T00:00:00")
      {
        parseBirthDateSuccessfull = false;
      }
      
      Structures.Module.IPersonFrom1cZup personSourceStructure = Structures.Module.PersonFrom1cZup.Create(
        anorsv.AnorsvIntergrationModule.AnorsvIntergration1cZupPerson.IngoingIdentificatorType.Guid
        , personSourceData.Guid
        , anorsv.AnorsvIntergrationModule.AnorsvIntergration1cZupPerson.ProcessingStatus.AwaitProcessing
        , upToDate
        , null
        , session
        , personSourceData.Deleted
        , personSourceData.DeletionMark
        , personSourceData.Code
        , personSourceData.Link
        , personSourceData.Description
        , parseBirthDateSuccessfull ? (DateTime?)parsedBirthDate : null
        , sex
        , personSourceData.Fio
        , personSourceData.Surname != "" ? anorsv.ServiceLibrary.PublicFunctions.Module.FormatPrintableName(personSourceData.Surname) : ""
        , personSourceData.Name != "" ? anorsv.ServiceLibrary.PublicFunctions.Module.FormatPrintableName(personSourceData.Name) : ""
        , personSourceData.Patronymic != "" ? anorsv.ServiceLibrary.PublicFunctions.Module.FormatPrintableName(personSourceData.Patronymic) : ""
        , new List<Structures.Module.IEmployementInformation>()
       );
      
      foreach (var employmentInformationSource in personSourceData.EmploymentInformation)
      {
        var employmentInformationStructure = CreateEmploymentInformationStructure(employmentInformationSource);
        
        if (employmentInformationStructure != null)
        {
          personSourceStructure.EmployementsInformation.Add(employmentInformationStructure);
        }
      }
      
      return personSourceStructure;
    }
    
    public static Structures.Module.IEmployementInformation CreateEmploymentInformationStructure(AnorsvIntegrationLibrary.PersonsFrom1cZup.EmploymentInformation employmentInformationSource)
    {
      Structures.Module.IEmployementInformation employmentInformation = null;
      DateTime parsedStartDate;
      bool parseStartDateSuccessfull = DateTime.TryParseExact(employmentInformationSource.startDate, "s", null, DateTimeStyles.None, out parsedStartDate);
      if (parseStartDateSuccessfull && employmentInformationSource.startDate == "0001-01-01T00:00:00")
      {
        parseStartDateSuccessfull = false;
      }
      bool sourceDataVilidated =
        !string.IsNullOrEmpty(employmentInformationSource.employeeGUID)
        && employmentInformationSource.employeeGUID != ""
        && !string.IsNullOrEmpty(employmentInformationSource.personnelNumber)
        && employmentInformationSource.personnelNumber != ""
        && !string.IsNullOrEmpty(employmentInformationSource.divisionGUID)
        && employmentInformationSource.divisionGUID != ""
        && !string.IsNullOrEmpty(employmentInformationSource.positionGUID)
        && employmentInformationSource.positionGUID != "";
      
      if (sourceDataVilidated)
      {
        employmentInformation = Structures.Module.EmployementInformation.Create(
          employmentInformationSource.employeeGUID
          , employmentInformationSource.personnelNumber != null ? employmentInformationSource.personnelNumber : ""
          , employmentInformationSource.divisionGUID  != null ? employmentInformationSource.divisionGUID : ""
          , employmentInformationSource.division != null ? employmentInformationSource.division : ""
          , employmentInformationSource.positionGUID != null ? employmentInformationSource.positionGUID : ""
          , employmentInformationSource.position != null ? employmentInformationSource.position : ""
          , parseStartDateSuccessfull ? (DateTime?) parsedStartDate : null
          , employmentInformationSource.isHeadEmployee
         );
      }
      
      return employmentInformation;
    }
    
    /// <summary>
    /// Обработка очереди персон из сессии.
    /// </summary>
    /// <param name="sessionId">Ид интеграционной сессии в рамках которой нужно обработать элементы персон.</param>
    /// <param name="settingsId">Ид конфигурации для типа внешней сущности персоны.</param>
    public static void ImportPersonsInQueue(int sessionId, int settingsId)
    {
      var session = AnorsvIntegrationSession1cs.GetAll(s => s.Id == sessionId).FirstOrDefault();
      var settings = AnorsvGenericIntegrationConfigurations.GetAll(s => s.Id == settingsId).FirstOrDefault();
      
      if (session == null)
      {
        var exception = AppliedCodeException.Create(
          Resources.ErrorNotFoundSession1cFormat(sessionId)
         );
        throw exception;
      }
      
      if (settings == null)
      {
        var exception = AppliedCodeException.Create(
          Resources.ErrorNotFoundSettingsFormat(settingsId)
         );
        throw exception;
      }
      
      ImportPersonsInQueue(session, settings);
    }

    /// <summary>
    /// Обработка очереди персон из сессии.
    /// </summary>
    /// <param name="session">Сессия в рамках которой нужно обработать элементы персон.</param>
    /// <param name="settings">Конфигурационные настройки для типа внешней сущности персоны.</param>
    public static void ImportPersonsInQueue(IAnorsvIntegrationSession1c session, IAnorsvIntegrationConfiguration settings)
    {
      bool needLink = settings.Link == true;
      bool needUpdate = settings.Update == true;
      bool needCreate = settings.Create == true;
      
      var awaitingElementsInQueue = AnorsvIntergration1cZupPeople
        .GetAll(
          element => element.IntegrationSession.Equals(session)
          && element.ProcessingStatus == Element.ProcessingStatus.AwaitProcessing);
      
      foreach (var element in awaitingElementsInQueue)
      {
        SynchronyzePersonFrom1cZup(element, needLink, needCreate, needUpdate);
      }
    }
    
    #endregion
    
    #region Подразделения
    
    public static bool DepartamentIsActive(IAnorsvIntegration1cZupDepartament element, DateTime date)
    {
      bool isActive =
        (element.Formed.HasValue && element.Formed.Value && (!element.CreationDate.HasValue || element.CreationDate.Value.CompareTo(date) <= 0))
        && (!element.Disbanded.HasValue || !element.Disbanded.Value || !element.DissolutionDate.HasValue || element.DissolutionDate.Value.CompareTo(date) >= 0);
      
      return isActive;
    }
    
    public static bool DepartamentIsActiveNow(IAnorsvIntegration1cZupDepartament element)
    {
      DateTime currentDate = Sungero.Core.Calendar.Today;
      bool isActive = DepartamentIsActive(element, currentDate);
      
      return isActive;
    }

    public static bool NeedUpdateDepartamentByElementFrom1cZup(sline.RSV.IDepartment destination, IAnorsvIntegration1cZupDepartament source, bool headOffice)
    {
      bool result = false;
      bool sourcePersonnelHeadOfficeIsEmpty = string.IsNullOrEmpty(source.PersonnelHeadOffice);
      bool personnelHeadOfficeIsDifferent = destination.PersonnelHeadOfficeanorsv == null && sourcePersonnelHeadOfficeIsEmpty;
      bool destinationIsActive = (destination.Status.HasValue && destination.Status.Value == sline.RSV.Department.Status.Active);
      bool sourceIsActive = DepartamentIsActiveNow(source);
      
      if (!sourcePersonnelHeadOfficeIsEmpty)
      {
        List<int> personnelHeadOfficeIds = GetInternalDepartamentIdsByExternalId(source.IntegrationSession.RespondingSystem.SystemId, source.PersonnelHeadOffice);
        
        personnelHeadOfficeIsDifferent =
          destination.PersonnelHeadOfficeanorsv == null
          || destination.PersonnelHeadOfficeanorsv != null && !personnelHeadOfficeIds.Contains(destination.PersonnelHeadOfficeanorsv.Id);
      }
      
      result =
        destination.Name != source.IngoingElementName
        || personnelHeadOfficeIsDifferent
        || destinationIsActive != sourceIsActive;
      
      return result;
    }
    
    public static List<int> GetInternalDepartamentIdsByExternalId(string externalSystemId, Guid externalEntityId)
    {
      Guid entityType = Guid.Parse("61b1c19f-26e2-49a5-b3d3-0d3618151e12");
      string externalEntityTypeId = "Departament";
      
      List<int> entityIds = GetInternalEntityIdsByExternalId(entityType, externalSystemId, externalEntityTypeId, externalEntityId);
      
      return entityIds;
    }
    
    public static List<int> GetInternalDepartamentIdsByExternalId(string externalSystemId, string externalEntityId)
    {
      Guid externalEntityIdGuid = Guid.Parse(externalEntityId);
      
      List<int> entityIds = GetInternalDepartamentIdsByExternalId(externalSystemId, externalEntityIdGuid);
      
      return entityIds;
    }

    public static sline.RSV.IDepartment UpdateDepartamentByElementFrom1cZup(sline.RSV.IDepartment destination, IAnorsvIntegration1cZupDepartament source)
    {
      bool sourcePersonnelHeadOfficeIsEmpty = string.IsNullOrEmpty(source.PersonnelHeadOffice);
      bool destinationIsActive = (destination.Status.HasValue && destination.Status.Value == sline.RSV.Department.Status.Active);
      bool sourceIsActive = DepartamentIsActiveNow(source);
      
      if (destination.Name != source.IngoingElementName)
      {
        destination.Name = source.IngoingElementName;
      }
      
      if (destination.PersonnelHeadOfficeanorsv != null && sourcePersonnelHeadOfficeIsEmpty)
      {
        destination = null;
      }
      else if (!sourcePersonnelHeadOfficeIsEmpty)
      {
        List<int> personnelHeadOfficeIds = GetInternalDepartamentIdsByExternalId(source.IntegrationSession.RespondingSystem.SystemId, source.PersonnelHeadOffice);
        
        if (personnelHeadOfficeIds.Count > 0)
        {
          if (destination.PersonnelHeadOfficeanorsv == null || !personnelHeadOfficeIds.Contains(destination.PersonnelHeadOfficeanorsv.Id))
          {
            var sourcePersonnelHeadDepartament =
              sline.RSV.Departments
              .GetAll(departament => personnelHeadOfficeIds.Contains(departament.Id))
              .FirstOrDefault();
            
            if (sourcePersonnelHeadDepartament != null)
            {
              destination.PersonnelHeadOfficeanorsv = sourcePersonnelHeadDepartament;
            }
          }
        }
      }
      
      if (destinationIsActive != sourceIsActive)
      {
        destination.Status = sourceIsActive ? sline.RSV.Department.Status.Active : sline.RSV.Department.Status.Closed;
      }
      
      return destination;
    }

    public static IQueryable<sline.RSV.IDepartment> GetInternalDepartamentsByLinkFrom1cZup(IAnorsvIntegration1cZupDepartament element)
    {
      var entityIds =  GetInternalDepartamentIdsByExternalId(element.IntegrationSession.RespondingSystem.SystemId, element.IngoingIdentificator);
      var entities = sline.RSV.Departments.GetAll(entity => entityIds.Contains(entity.Id));
      
      return entities;
    }

    public static IQueryable<sline.RSV.IDepartment> SearchInternalDepartamentsByElementFrom1cZup(IAnorsvIntegration1cZupDepartament element, bool personnelHeadOffice)
    {
      bool elementPersonnelHeadOfficeIsNotEmpty = !string.IsNullOrEmpty(element.PersonnelHeadOffice);
      List<int> personnelHeadOfficeIds = new List<int>();
      int personnelHeadOfficeId = -1;
      
      var query = sline.RSV.Departments.GetAll(departament => departament.Name == element.IngoingElementName);
      
      if (personnelHeadOffice && elementPersonnelHeadOfficeIsNotEmpty)
      {
        personnelHeadOfficeIds = GetInternalDepartamentIdsByExternalId(element.IntegrationSession.RespondingSystem.SystemId, element.PersonnelHeadOffice);
        if (personnelHeadOfficeIds.Count > 0)
        {
          personnelHeadOfficeId = personnelHeadOfficeIds.First();
          query = query.Where(departament => departament.PersonnelHeadOfficeanorsv != null && departament.PersonnelHeadOfficeanorsv.Id == personnelHeadOfficeId);
        }
      }
      
      return query;
    }

    public static sline.RSV.IDepartment CreateDepartamentFrom1cZup(IAnorsvIntegration1cZupDepartament element)
    {
      sline.RSV.IDepartment entity = sline.RSV.Departments.Create();
      List<int> personnelHeadOfficeIds = null;
      int personnelHeadOfficeId = -1;
      sline.RSV.IDepartment personnelHeadOffice = sline.RSV.Departments.Null;
      
      if (!string.IsNullOrEmpty(element.PersonnelHeadOffice))
      {
        personnelHeadOfficeIds = GetInternalDepartamentIdsByExternalId(element.IntegrationSession.RespondingSystem.SystemId, element.PersonnelHeadOffice);
        personnelHeadOfficeId = personnelHeadOfficeIds.FirstOrDefault();
        personnelHeadOffice = sline.RSV.Departments.Get(personnelHeadOfficeId);
      }
      
      entity.Name = element.IngoingElementName;
      entity.HeadOffice = personnelHeadOffice;
      entity.PersonnelHeadOfficeanorsv = personnelHeadOffice;
      
      entity.Save();
      
      return entity;
      
    }

    public static void SynchronyzeDepartamentFrom1cZup(IAnorsvIntegration1cZupDepartament element, bool needLinking, bool needCreating, bool needUpdating)
    {
      bool linked = false;
      bool created = false;
      bool updated = false;
      List<sline.RSV.IDepartment> linkedEntities = new List<sline.RSV.IDepartment>();
      List<sline.RSV.IDepartment> searchedEntities = new List<sline.RSV.IDepartment>();
      List<sline.RSV.IDepartment> newInSearchedEntities = new List<sline.RSV.IDepartment>();
      
      if (element == null || !(needLinking || needCreating || needUpdating))
      {
        return;
      }
      
      if (element.ProcessingStatus != Element.ProcessingStatus.AwaitProcessing)
      {
        return;
      }
      
      if (Locks.TryLock(element))
      {
        element.ProcessingStatus = Element.ProcessingStatus.InProcessing;
        element.Save();
        
        linkedEntities = GetInternalDepartamentsByLinkFrom1cZup(element).ToList();
        
        if (needLinking)
        {
          searchedEntities = SearchInternalDepartamentsByElementFrom1cZup(element, true).ToList();
          newInSearchedEntities = searchedEntities.Where(entity => !linkedEntities.Contains(entity)).ToList();
          foreach(var entity in newInSearchedEntities)
          {
            CreateExternalLink(entity, element.IntegrationSession.RespondingSystem.SystemId, Guid.Parse(element.IngoingIdentificator), "Departament");
            linkedEntities.Add(entity);
          }
        }
        
        linked = needLinking && searchedEntities.Count > 0 && (searchedEntities.Count == linkedEntities.Count());
        
        if (needUpdating && linkedEntities.Count > 0)
        {
          foreach(var entity in linkedEntities)
          {
            bool entityDataDifferenced = NeedUpdateDepartamentByElementFrom1cZup(entity, element, true);
            
            if (entityDataDifferenced)
            {
              UpdateDepartamentByElementFrom1cZup(entity, element).Save();
            }
          }
          
          updated = true;
        }
        
        if (needCreating && linkedEntities.Count == 0)
        {
          var entity = CreateDepartamentFrom1cZup(element);
          CreateExternalLink(entity, element.IntegrationSession.RespondingSystem.SystemId, Guid.Parse(element.IngoingIdentificator), "Departament");
          linkedEntities.Add(entity);
        }
        
        if ((needLinking && linked) || (needCreating && created) || (needUpdating && updated) || (linkedEntities.Count > 0))
        {
          element.ProcessingStatus = Element.ProcessingStatus.Processed;
          element.ProcessingResult = Element.ProcessingResult.Successful;
          element.ProcessingOperation = Element.ProcessingOperation.Unknown;
          if (created)
          {
            element.ProcessingOperation = Element.ProcessingOperation.Create;
          }
          else if (updated)
          {
            element.ProcessingOperation = Element.ProcessingOperation.Update;
          }
          else if (linked || linkedEntities.Count > 0)
          {
            element.ProcessingOperation = Element.ProcessingOperation.Link;
          }
        }
        else
        {
          element.ProcessingStatus = Element.ProcessingStatus.Processed;
          element.ProcessingResult = Element.ProcessingResult.Unseccessful;
          element.ProcessingOperation = Element.ProcessingOperation.Unknown;
        }
        
        if (element.State.IsChanged)
        {
          element.Save();
        }
        Locks.Unlock(element);
      }
    }

    [Remote(IsPure = false)]
    public static void SynchronyzeDepartamentFrom1cZup(int elementId, bool needLinking, bool needCreating, bool needUpdating)
    {
      IAnorsvIntegration1cZupDepartament elementPerson = AnorsvIntegration1cZupDepartaments.Get(elementId);
      SynchronyzeDepartamentFrom1cZup(elementPerson, needLinking, needCreating, needUpdating);
    }

    public static Structures.Module.IDepartamentFrom1cZup CreateDepartamentStructure(
      AnorsvIntegrationLibrary.DepartamentsFrom1cZup.DepartamentFrom1cZup sourceData
      , IAnorsvIntegrationSession1c session
      , DateTime upToDate
     )
    {
      
      DateTime parsedCreationDate;
      bool parsedCreationDateSuccessfull = DateTime.TryParseExact(sourceData.CreationDate, "s", null, DateTimeStyles.None, out parsedCreationDate);
      if (parsedCreationDateSuccessfull && sourceData.CreationDate == "0001-01-01T00:00:00")
      {
        parsedCreationDateSuccessfull = false;
      }

      DateTime parsedDissolutionDate;
      bool parsedDissolutionDateSuccessfull = DateTime.TryParseExact(sourceData.DissolutionDate, "s", null, DateTimeStyles.None, out parsedDissolutionDate);
      if (parsedDissolutionDateSuccessfull && sourceData.DissolutionDate == "0001-01-01T00:00:00")
      {
        parsedDissolutionDateSuccessfull = false;
      }
      
      Structures.Module.IDepartamentFrom1cZup sourceStructure = Structures.Module.DepartamentFrom1cZup.Create(
        anorsv.AnorsvIntergrationModule.AnorsvIntegration1cZupDepartament.IngoingIdentificatorType.Guid
        , sourceData.Guid
        , anorsv.AnorsvIntergrationModule.AnorsvIntegration1cZupDepartament.ProcessingStatus.AwaitProcessing
        , upToDate
        , null
        , session
        , sourceData.Deleted
        , sourceData.DeletionMark
        , sourceData.Code
        , sourceData.Link
        , sourceData.Description
        , sourceData.ParentGUID
        , sourceData.Formed
        , parsedCreationDateSuccessfull ? (DateTime?) parsedCreationDate : null
        , sourceData.Disbanded
        , parsedDissolutionDateSuccessfull ? (DateTime?) parsedDissolutionDate : null
       );
      
      return sourceStructure;
    }

    /// <summary>
    /// Создаёт элемент подразделения в очереди на обработку из структуры.
    /// </summary>
    /// <param name="source">Структура описывающая полученные данные из 1С ЗуП по подразделению.</param>
    /// <returns>Созданное в очереди на обработку подразделение.</returns>
    public static IAnorsvIntegration1cZupDepartament CreateDepartamentElement(Structures.Module.IDepartamentFrom1cZup source)
    {
      IAnorsvIntegration1cZupDepartament element = AnorsvIntegration1cZupDepartaments.Create();
      
      // Базовые сведения для елементов из 1с
      element.Code = source.Code;
      element.Deleted = source.Deleted;
      element.DeletionMark = source.DeletionMark;
      element.Hyperlink = source.Hyperlink;
      element.IngoingElementName = source.IngoingElementName;
      element.IngoingIdentificator = source.IngoingIdentificator;
      element.IngoingIdentificatorType = source.IngoingIdentificatorType;
      element.IntegrationSession = source.IntegrationSession;
      element.ProcessingStatus = source.ProcessingStatus;
      element.ProcessingEndDate = source.ProcessingEndDate;
      element.UpToDate = source.UpToDate;
      
      // Сведенения специфичные для подразделения
      element.PersonnelHeadOffice = source.HeadOffice.ToString();
      element.Formed = source.Formed;
      element.CreationDate = source.CreationDate;
      element.Disbanded = source.Disbanded;
      element.DissolutionDate = source.DissolutionDate;
      
      return element;
    }
    
    public static IAnorsvIntegration1cZupDepartament CreateDepartamentElement(
      AnorsvIntegrationLibrary.DepartamentsFrom1cZup.DepartamentFrom1cZup source
      , IAnorsvIntegrationSession1c session
      , DateTime upToDate
     )
    {
      Structures.Module.IDepartamentFrom1cZup sourceStructure = CreateDepartamentStructure(source, session, upToDate);
      
      bool elementNotExistInQueue =
        AnorsvIntegration1cZupDepartaments
        .GetAll(
          element =>
          element.IngoingIdentificator.Equals(source.Guid, StringComparison.OrdinalIgnoreCase)
          && element.IntegrationSession.RespondingSystem.Equals(sourceStructure.IntegrationSession.RespondingSystem)
          && element.ProcessingStatus != Element.ProcessingStatus.Processed)
        .Where(
          element =>
          element.IngoingElementName == sourceStructure.IngoingElementName
          && element.PersonnelHeadOffice.Equals(sourceStructure.HeadOffice, StringComparison.OrdinalIgnoreCase)
          && element.Formed == sourceStructure.Formed
          && element.CreationDate == sourceStructure.CreationDate
          && element.Disbanded == sourceStructure.Disbanded
          && element.DissolutionDate == sourceStructure.DissolutionDate)
        .Count() == 0;
      
      if (elementNotExistInQueue)
      {
        return CreateDepartamentElement(sourceStructure);
      }
      
      return AnorsvIntegration1cZupDepartaments.Null;
    }

    /// <summary>
    /// Помещает полученные извне сведения о подразделениях в очередь для последующей обработки.
    /// </summary>
    /// <param name="persons">Список с данным подразделений.</param>
    /// <param name="session">Текущая сессия.</param>
    /// <param name="upToDate">Дата и время на которые актуальны полученные сведения.</param>
    /// <returns>Признак успешной операции размещения данных в очереди.</returns>
    public static bool PutDepartamentsInQueue(List<AnorsvIntegrationLibrary.DepartamentsFrom1cZup.DepartamentFrom1cZup> source, IAnorsvIntegrationSession1c session, DateTime upToDate)
    {
      foreach (var sourceData in source)
      {
        var element = CreateDepartamentElement(sourceData, session, upToDate);
        if (element != null)
        {
          element.Save();
        }
      }
      
      return true;
    }

    #endregion
    
    #region Управление внешними ссылками
    
    /// <summary>
    /// Получить external links.
    /// </summary>
    /// <param name="entityType">Тип внутренней сущности.</param>
    /// <param name="entityId">ИД внутренней сущности.</param>
    /// <param name="externalSystemId">ИД внешней системы.</param>
    /// <param name="externalEntityTypeId">ИД типа внешней сущности.</param>
    /// <param name="externalEntityId">ИД внешней сущности.</param>
    /// <returns>Список external links.</returns>
    [Public]
    public static List<Sungero.Domain.Shared.IExternalLink> GetExternalLinks(Guid? entityType, int? entityId, string externalSystemId, string externalEntityTypeId, Guid externalEntityId)
    {
      return Sungero.Domain.ModuleFunctions
        .GetAllExternalLinks(
          l => (entityType == null || l.EntityTypeGuid == entityType)
          && (entityId == null || l.EntityId == entityId)
          && (string.IsNullOrEmpty(externalSystemId) || l.ExternalSystemId == externalSystemId)
          && (string.IsNullOrEmpty(externalEntityTypeId) || l.ExternalEntityTypeId == externalEntityTypeId)
          && (externalEntityId == null || l.ExternalEntityId.Equals(externalEntityId.ToString(), StringComparison.OrdinalIgnoreCase)))
        .ToList();
    }
    
    /// <summary>
    /// Создать external link.
    /// </summary>
    /// <param name="entity">Сущность.</param>
    /// <param name="externalSystemId">ИД внешней системы.</param>
    /// <param name="externalEntityId">ИД внешней сущности.</param>
    /// <param name="externalEntityTypeId">ИД типа внешней сущности.</param>
    /// <returns>External link.</returns>
    [Public]
    public static Sungero.Domain.Shared.IExternalLink CreateExternalLink(IEntity entity, string externalSystemId, Guid externalEntityId, string externalEntityTypeId)
    {
      var externalLink = Sungero.Domain.ModuleFunctions.CreateExternalLink();
      externalLink.EntityTypeGuid = entity.GetEntityMetadata().NameGuid;
      externalLink.ExternalEntityId = externalEntityId.ToString();
      externalLink.ExternalSystemId = externalSystemId;
      externalLink.ExternalEntityTypeId = externalEntityTypeId;
      externalLink.EntityId = entity.Id;
      externalLink.IsDeleted = false;
      externalLink.Save();
      
      return externalLink;
    }

    #endregion
    
    #region Работа с сессиями
    
    [Remote(IsPure=false)]
    public static void ClearEmptySessions(int systemId)
    {
      var sessions =
        AnorsvIntegrationSessions
        .GetAll(session => session.RespondingSystem.Id == systemId)
        .Where(session => AnorsvExternalIntergrationElements.GetAll().Where(element => element.IntegrationSession.Equals(session)).Count() == 0);
      
      foreach (var session in sessions)
      {
        AnorsvIntegrationSessions.Delete(session);
      }
    }
    
    #endregion
    
    #region Загрузка данных из внешних источников
    
    #region Загрузка персон

    [Public]
    public static bool GetPersonsFrom1c(IAnorsvGenericIntegrationConfiguration settings)
    {
      bool Successfull = false;
      var mySystem = Functions.Module.GetMySystem();
      var externalSystem = settings.ExternalEntityType.System;
      var externalEntityType = settings.ExternalEntityType;
      string serviceUrl = settings.Source;
      bool needAuthorization = settings.NeedAuthorization.HasValue ? settings.NeedAuthorization.Value : false;
      string userLogin = settings.AuthorizationLogin;
      string userPassword =
        string.IsNullOrEmpty(settings.AuthorizationPasswordValue)
        ? string.Empty
        : Encoding.UTF8.GetString(Convert.FromBase64String(settings.AuthorizationPasswordValue));
      
      System.IO.MemoryStream requestBodyStream = new System.IO.MemoryStream(
        UTF8Encoding.UTF8.GetBytes(Resources.External1cSystemIdFormat(mySystem.SystemId))
       );

      IAnorsvIntegrationSession1c session = AnorsvIntegrationSession1cs.Create();

      try
      {
        session.SessionId = Guid.NewGuid().ToString();
        session.SessionStatus = SessionStatus.Draft;
        session.RequestingSystem = Functions.Module.GetMySystem();
        session.RequestMethod = RequestMethod.HttpPut;
        session.RequestBodyContentType = RequestBodyContentType.Json;
        session.RequestBody.Write(requestBodyStream);
        session.RespondingSystem = externalSystem;
        session.Configuration = settings;
        session.ExternalEntityType = externalEntityType;
        session.ResponseBodyContentType = ResponseBodyContentType.Json;
        session.Save();
        
        session.StartDateTime = Sungero.Core.Calendar.Now;
        Client<PersonsResponse> client = new Client<PersonsResponse>();
        var response = client.GetResponse(serviceUrl, mySystem.SystemId, needAuthorization, userLogin, userPassword);
        var responseData = response.Data;
        
        var ExchangeSuccessful = Transactions.Execute(
          () => {
            Functions.Module.PutPersonsInQueue(responseData, session, response.ExchangePeriod);
          }
         );
        
        Logger.DebugFormat("GetPersonsFrom1cZup: Recived {0} persons.", responseData.Count());
        session.SessionStatus = SessionStatus.Successful;
        Successfull = true;
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat("GetPersonsFrom1cZup: {0}", ex.Message);
        session.SessionStatus = SessionStatus.Unsuccessful;
      }
      
      if (!session.State.IsInserted)
      {
        session.EndDateTime = Sungero.Core.Calendar.Now;
        session.Save();
      }

      return Successfull;
    }

    #endregion
    
    #region Работа с http сервисами 1с
    
    #endregion
    
    #endregion
  }
}