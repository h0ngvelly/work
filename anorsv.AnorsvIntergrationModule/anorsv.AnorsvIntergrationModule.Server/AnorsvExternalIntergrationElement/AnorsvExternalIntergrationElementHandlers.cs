using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvIntergrationModule.AnorsvExternalIntergrationElement;

namespace anorsv.AnorsvIntergrationModule
{
  partial class AnorsvExternalIntergrationElementServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      if (!_obj.State.IsCopied)
      {
        _obj.ProcessingStatus = ProcessingStatus.AwaitProcessing;
        _obj.ProcessingResult = ProcessingResult.Unknown;
        _obj.ProcessingOperation = ProcessingOperation.Unknown;
      }
    }
  }


}