﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.AnorsvIntergrationModule.Server
{
  partial class SystemsFolderHandlers
  {

    public virtual IQueryable<anorsv.AnorsvCoreModule.IAnorsvSystem> SystemsDataQuery(IQueryable<anorsv.AnorsvCoreModule.IAnorsvSystem> query)
    {
      if (_filter != null)
      {
        query = query.Where(
          s => _filter.AllTypesSystem
          || (_filter.System1c && Anorsv1cSystems.Is(s))
          || (_filter.SystemDirectum && AnorsvDirectumSystems.Is(s))
          || (_filter.SystemOther && AnorsvOtherSystems.Is(s))
         );
      }
      
      return query;
    }
  }

  partial class AnorsvIntergrationModuleHandlers
  {
  }
}