﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Sungero.Core;
using Sungero.CoreEntities;
using AnorsvIntegrationLibrary.PersonsFrom1cZup;
using AnorsvIntegrationLibrary.DepartamentsFrom1cZup;
using AnorsvIntegrationLibrary.PositionsFrom1cZup;
using AnorsvIntegrationLibrary.WebServiceClient;
using anorsv.AnorsvIntergrationModule.AnorsvIntegrationSession1c;
using anorsv.AnorsvCoreModule;

namespace anorsv.AnorsvIntergrationModule.Server
{
  public class ModuleAsyncHandlers
  {

    public virtual void GetDepatamentsFrom1cZup(anorsv.AnorsvIntergrationModule.Server.AsyncHandlerInvokeArgs.GetDepatamentsFrom1cZupInvokeArgs args)
    {
      System.IO.MemoryStream requestBodyStream = new System.IO.MemoryStream(UTF8Encoding.UTF8.GetBytes(string.Format("{{\"systemId\": \"{0}\"}}", args.SystemId)));

      IAnorsvIntegrationSession1c session = AnorsvIntegrationSession1cs.Create();

      try
      {
        session.SessionId = Guid.NewGuid().ToString();
        session.SessionStatus = SessionStatus.Draft;
        session.RequestingSystem = Functions.Module.GetMySystem();
        session.RequestMethod = RequestMethod.HttpPut;
        session.RequestBodyContentType = RequestBodyContentType.Json;
        session.RequestBody.Write(requestBodyStream);
        session.RespondingSystem = Anorsv1cSystems.GetAll(system => system.SystemId.Equals(args.SystemId, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        session.ExternalEntityType = 
          AnorsvExternalEntityTypes
          .GetAll(
            entityType => entityType.System.Equals(session.RespondingSystem) 
            && entityType.EntityTypeId.Equals(args.EntityTypeId, StringComparison.OrdinalIgnoreCase))
          .FirstOrDefault();
        session.ResponseBodyContentType = ResponseBodyContentType.Json;
        session.Save();
        
        session.StartDateTime = Calendar.Now;
        Client<DepartamentsResponse> client = new Client<DepartamentsResponse>();
        //var response = client.GetResponse(args.ServiceUrl, args.SystemId, args.NeedAuthorization, args.User, args.Password);
        var response = client.GetResponseFromFile(@"\\drctm-app-r02t.corp.rsv.ru\d$\Responses\Get1cZupDepartaments_nb154_dev.response.json");
        var responseData = response.Data;
        
        var ExchangeSuccessful = Transactions.Execute(
          () => {
            Functions.Module.PutDepartamentsInQueue(responseData, session, response.ExchangePeriod);
          }
         );
        
        Logger.DebugFormat("GetDepartamentsFrom1cZup: Recived {0} departaments.", response.Data.Count());
        session.SessionStatus = SessionStatus.Successful;
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat("GetDepartamentsFrom1cZup: {0}", ex.Message);
        session.SessionStatus = SessionStatus.Unsuccessful;
      }
      
      if (!session.State.IsInserted)
      {
        session.EndDateTime = Calendar.Now;
        session.Save();
      }
      args.Retry = false;
    }
    
    public virtual void GetPersonsFrom1cZup(anorsv.AnorsvIntergrationModule.Server.AsyncHandlerInvokeArgs.GetPersonsFrom1cZupInvokeArgs args)
    {
      var settings = AnorsvGenericIntegrationConfigurations
        .GetAll(s => s.Id == args.settingsId)
        .FirstOrDefault();
      
      if (settings != null)
      {
        Functions.Module.GetPersonsFrom1c(settings);
      }
    }
  }

}