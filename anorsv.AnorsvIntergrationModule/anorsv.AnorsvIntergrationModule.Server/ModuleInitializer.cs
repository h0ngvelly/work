﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;
using Docflow = Sungero.Docflow;

namespace anorsv.AnorsvIntergrationModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      var currentSystem = CreateCurrentSystem();
      CreateInternaEntities(currentSystem);
    }
    
    /// <summary>
    /// Создаёт системную запись о текущей системе в справочнике anorsv.AnorsvIntegrationModule.AnorsvDirectumSystem.
    /// </summary>
    /// <returns>Возвращает сущность текущей системы.</returns>
    private IAnorsvDirectumSystem CreateCurrentSystem()
    {
      var currentSystem = AnorsvDirectumSystems.GetAll(system => system.CurrentSystem == true).FirstOrDefault();
      string systemName = currentSystem != null ? currentSystem.Name : Dns.GetHostEntry(Dns.GetHostName()).HostName.ToLower();
      string systemId = currentSystem != null ? currentSystem.SystemId : Guid.NewGuid().ToString().ToLower();
      bool isCurrentSystem = true;
      
      if (currentSystem == null)
      {
        currentSystem = AnorsvDirectumSystems.Create();
        currentSystem.Name = systemName;
        currentSystem.SystemId = systemId;
        currentSystem.CurrentSystem = isCurrentSystem;
        currentSystem.Save();

        Docflow.PublicFunctions.Module.CreateExternalLink(currentSystem, Guid.Parse(systemId));

        InitializationLogger.DebugFormat(Resources.InitCreateNewCurrentSystemFormat(systemName, systemId));
      }
      else
      {
        var externalLink = Docflow.PublicFunctions.Module.GetExternalLink(currentSystem.GetType().GUID, Guid.Parse(systemId));
        
        if (externalLink == null)
        {
          Docflow.PublicFunctions.Module.CreateExternalLink(currentSystem, Guid.Parse(systemId));
        }
        
        InitializationLogger.DebugFormat(Resources.InitCurrentSystemExistFormat(systemName, systemId));
      }
      
      return currentSystem;
    }
    
    /// <summary>
    /// Создаёт поддерживаемы внутренние типы сущностей.
    /// </summary>
    /// <returns>Возвращает true если все сущности успешно созданы, иначе false.</returns>
    private bool CreateInternaEntities(IAnorsvDirectumSystem system)
    {
      bool errorExists = false;
      var currentSystem = system != null ? system : Functions.Module.GetMySystem();
      
      var entityType  = CreatePersonInternalEntityType(currentSystem);
      errorExists = errorExists || entityType == null;
      
      entityType  = CreateDepartamentInternalEntityType(currentSystem);
      errorExists = errorExists || entityType == null;
      
      entityType  = CreatePositionInternalEntityType(currentSystem);
      errorExists = errorExists || entityType == null;

      entityType  = CreateEmployeeInternalEntityType(currentSystem);
      errorExists = errorExists || entityType == null;
      
      return !errorExists;
    }
    
    /// <summary>
    /// Создаёт внутрений тип сущности в справочнике anorsv.AnorsvIntegrationModule.AnorsvInternalEntityType.
    /// </summary>
    /// <param name="humanReadableName">Человекочитаемое имя.</param>
    /// <param name="internalName">Машиночитаемое имя.</param>
    /// <param name="entityTypeId">Guid типа сущности.</param>
    /// <param name="status">Статус типа сущности.</param>
    /// <param name="system">Систеама для которой создаётся тип сущности.</param>
    /// <returns>Возращает созданную сущность.</returns>
    private IAnorsvInternalEntityType CreateInternalEntityType(string humanReadableName, string internalName, string entityTypeId, string status, IAnorsvDirectumSystem system)
    {
      var internalEntityType = AnorsvInternalEntityTypes
        .GetAll(et => et.EntityTypeId.Equals(entityTypeId, StringComparison.OrdinalIgnoreCase))
        .FirstOrDefault();
      
      var newStatus = AnorsvInternalEntityType.StatusItems.Items.FirstOrDefault(st => st.Value == status);
      
      if (internalEntityType == null)
      {
        internalEntityType = AnorsvInternalEntityTypes.Create();
        internalEntityType.HumanReadableName = humanReadableName;
        internalEntityType.InternalName = internalName;
        internalEntityType.EntityTypeId = entityTypeId;
        internalEntityType.Status = newStatus;
        internalEntityType.System = system;
        internalEntityType.Save();

        Docflow.PublicFunctions.Module.CreateExternalLink(internalEntityType, Guid.Parse(entityTypeId));

        InitializationLogger.Debug(
          Resources.InitCreateNewInternalEntityTypeFormat(humanReadableName, internalName, entityTypeId)
         );
      }
      else
      {
        var externalLink = Docflow.PublicFunctions.Module.GetExternalLink(internalEntityType.GetType().GUID, Guid.Parse(entityTypeId));
        
        if (externalLink == null)
        {
          Docflow.PublicFunctions.Module.CreateExternalLink(internalEntityType, Guid.Parse(entityTypeId));
        }
        
        if (internalEntityType.HumanReadableName != humanReadableName)
        {
          internalEntityType.HumanReadableName = humanReadableName;
        }
        
        if (internalEntityType.InternalName != internalName)
        {
          internalEntityType.InternalName = internalName;
        }
        
        if (internalEntityType.Status != newStatus)
        {
          internalEntityType.Status = newStatus;
        }
        
        if (internalEntityType.System != system)
        {
          internalEntityType.System = system;
        }
        
        if (internalEntityType.State.IsChanged)
        {
          internalEntityType.Save();
        }

        InitializationLogger.Debug(
          Resources.InitInternalEntityTypeExistFormat(humanReadableName, internalName, entityTypeId)
         );
      }
      
      return internalEntityType;
    }
    
    /// <summary>
    /// Создаёт новую запись для внутреннего типа сущности "Person"
    /// </summary>
    /// <returns>Возращает созданную сущность.</returns>
    private IAnorsvInternalEntityType CreatePersonInternalEntityType(IAnorsvDirectumSystem system)
    {
      string humanReadableName = Constants.Module.InternalEntiyType.Person.HumanReadableName;
      string internalName = Constants.Module.InternalEntiyType.Person.InternalName;
      string entityTypeId = Constants.Module.InternalEntiyType.Person.EntityTypeId;
      string status = Constants.Module.InternalEntiyType.Person.Status;
      
      var entityType = CreateInternalEntityType(humanReadableName, internalName, entityTypeId, status, system);
      
      return entityType;
    }

    /// <summary>
    /// Создаёт новую запись для внутреннего типа сущности "Departament"
    /// </summary>
    /// <returns>Возращает созданную сущность.</returns>
    private IAnorsvInternalEntityType CreateDepartamentInternalEntityType(IAnorsvDirectumSystem system)
    {
      string humanReadableName = Constants.Module.InternalEntiyType.Deprtamenrt.HumanReadableName;
      string internalName = Constants.Module.InternalEntiyType.Deprtamenrt.InternalName;
      string entityTypeId = Constants.Module.InternalEntiyType.Deprtamenrt.EntityTypeId;
      string status = Constants.Module.InternalEntiyType.Deprtamenrt.Status;
      
      var entityType = CreateInternalEntityType(humanReadableName, internalName, entityTypeId, status, system);
      
      return entityType;
    }

    /// <summary>
    /// Создаёт новую запись для внутреннего типа сущности "Position"
    /// </summary>
    /// <returns>Возращает созданную сущность.</returns>
    private IAnorsvInternalEntityType CreatePositionInternalEntityType(IAnorsvDirectumSystem system)
    {
      string humanReadableName = Constants.Module.InternalEntiyType.Position.HumanReadableName;
      string internalName = Constants.Module.InternalEntiyType.Position.InternalName;
      string entityTypeId = Constants.Module.InternalEntiyType.Position.EntityTypeId;
      string status = Constants.Module.InternalEntiyType.Position.Status;
      
      var entityType = CreateInternalEntityType(humanReadableName, internalName, entityTypeId, status, system);
      
      return entityType;
    }

    /// <summary>
    /// Создаёт новую запись для внутреннего типа сущности "Employee"
    /// </summary>
    /// <returns>Возращает созданную сущность.</returns>
    private IAnorsvInternalEntityType CreateEmployeeInternalEntityType(IAnorsvDirectumSystem system)
    {
      string humanReadableName = Constants.Module.InternalEntiyType.Employee.HumanReadableName;
      string internalName = Constants.Module.InternalEntiyType.Employee.InternalName;
      string entityTypeId = Constants.Module.InternalEntiyType.Employee.EntityTypeId;
      string status = Constants.Module.InternalEntiyType.Employee.Status;
      
      var entityType = CreateInternalEntityType(humanReadableName, internalName, entityTypeId, status, system);
      
      return entityType;
    }
  }
}
