﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvIntergrationModule.AnorsvDirectumSystem;

namespace anorsv.AnorsvIntergrationModule
{
  partial class AnorsvDirectumSystemServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      _obj.CurrentSystem = false;
    }

    public override void BeforeDelete(Sungero.Domain.BeforeDeleteEventArgs e)
    {
      base.BeforeDelete(e);
      
      if (_obj.CurrentSystem == true)
      {
        e.AddError(Resources.ErrorCannotDeleteDirectumCurentSystemRecord);
        return;
      }
    }
  }

}