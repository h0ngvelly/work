﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;

namespace anorsv.AnorsvIntergrationModule.Server
{
  public class ModuleJobs
  {

    /// <summary>
    /// 
    /// </summary>
    public virtual void ProcessingExternalPeople()
    {
      List<string> messages = new List<string>();
      string personEntityTypeId = Constants.Module.InternalEntiyType.Person.EntityTypeId;
      string personEntityTypeInternalName = Constants.Module.InternalEntiyType.Person.InternalName;
      string personEntityTypeHumanReadablelName = Constants.Module.InternalEntiyType.Person.HumanReadableName;
      var statusActive = anorsv.AnorsvIntergrationModule.AnorsvInternalEntityType.Status.Active;
      var sessionSuccessfull = anorsv.AnorsvIntergrationModule.AnorsvIntegrationSession.SessionStatus.Successful;
      var elementAwaitProcessing = anorsv.AnorsvIntergrationModule.AnorsvExternalIntergrationElement.ProcessingStatus.AwaitProcessing;
      
      var personInernalEntityType = AnorsvInternalEntityTypes
        .GetAll(
          et => et.Status == statusActive
          && et.EntityTypeId.Equals(personEntityTypeId, StringComparison.OrdinalIgnoreCase))
        .FirstOrDefault();

      if (personInernalEntityType != null)
      {
        var allSettings = AnorsvGenericIntegrationConfigurations
          .GetAll(
            s => s.ExternalEntityType.InternalEntityType.Equals(personInernalEntityType)
            && s.Status == statusActive
           );

        var allSession = AnorsvIntegrationSession1cs
          .GetAll(
            s => s.SessionStatus == sessionSuccessfull);
        allSession = allSession.Where(s=> allSettings.Contains(s.Configuration));
        allSession = allSession.Where(s => (AnorsvExternalIntergrationElements
                .GetAll()
                .Any(element => element.IntegrationSession.Equals(s) && element.ProcessingStatus == elementAwaitProcessing)))
          .OrderBy(s => s.StartDateTime);
        
        foreach (var session in allSession)
        {
          Functions.Module.ImportPersonsInQueue(session, session.Configuration);
        }
      }
      else
      {
        messages.Add(
          Resources.ErrorNotFindInternalEntityTypeFormat(personEntityTypeHumanReadablelName, personEntityTypeInternalName)
         );
      }

      // Формирование отчёта для ответственных за интеграцию
      if (messages.Count > 0)
      {
        var administratorsRole = Roles.Administrators;
        var taskText = string.Empty;
        
        var task = SimpleTasks.CreateWithNotices(
          "Background job report: ProcessingExternalPeople"
          , Roles.GetAllUsersInGroup(administratorsRole)
          , null
         );
        
        foreach (var messagePart in messages)
        {
          if (!string.IsNullOrEmpty(taskText))
          {
            taskText = string.Concat(taskText, Environment.NewLine, Environment.NewLine, messagePart);
          }
          else
          {
            taskText = messagePart;
          }
        }
        
        task.ActiveText = taskText;
        task.Start();
      }
    }

    /// <summary>
    /// Получает персоны из внешних источников согласно настройкам конфигурации.
    /// </summary>
    public virtual void GetExternalPeople()
    {
      List<string> messages = new List<string>();
      string personEntityTypeId = Constants.Module.InternalEntiyType.Person.EntityTypeId;
      string personEntityTypeInternalName = Constants.Module.InternalEntiyType.Person.InternalName;
      string personEntityTypeHumanReadablelName = Constants.Module.InternalEntiyType.Person.HumanReadableName;
      var statusActive = anorsv.AnorsvIntergrationModule.AnorsvInternalEntityType.Status.Active;
      
      // Создание новых сессий с данными
      var personInernalEntityType = AnorsvInternalEntityTypes
        .GetAll(
          et => et.Status == statusActive
          && et.EntityTypeId.Equals(personEntityTypeId, StringComparison.OrdinalIgnoreCase))
        .FirstOrDefault();
      
      if (personInernalEntityType != null)
      {
        var allSettings = AnorsvGenericIntegrationConfigurations
          .GetAll(
            s => s.ExternalEntityType.InternalEntityType.Equals(personInernalEntityType)
            && s.Status == statusActive
           );
        
        // Обработка данных из сессий с новыми данными
        foreach (var settings in allSettings)
        {
          var Successfull = Functions.Module.GetPersonsFrom1c(settings);
          if (!Successfull)
          {
            messages.Add(
              Resources.ErrorGettingDataUnsuccessfullFormat(settings.Id, settings.Name)
             );
          }
        }
      }
      else
      {
        messages.Add(
          Resources.ErrorNotFindInternalEntityTypeFormat(personEntityTypeHumanReadablelName, personEntityTypeInternalName)
         );
      }
      
      // Формирование отчёта для ответственных за интеграцию
      if (messages.Count > 0)
      {
        var administratorsRole = Roles.Administrators;
        var taskText = string.Empty;
        
        var task = SimpleTasks.CreateWithNotices(
          "Background job report: GetExternalPersons"
          , Roles.GetAllUsersInGroup(administratorsRole)
          , null
         );
        
        foreach (var messagePart in messages)
        {
          if (!string.IsNullOrEmpty(taskText))
          {
            taskText = string.Concat(taskText, Environment.NewLine, Environment.NewLine, messagePart);
          }
          else
          {
            taskText = messagePart;
          }
        }
        
        task.ActiveText = taskText;
        task.Start();
      }
    }

  }
}