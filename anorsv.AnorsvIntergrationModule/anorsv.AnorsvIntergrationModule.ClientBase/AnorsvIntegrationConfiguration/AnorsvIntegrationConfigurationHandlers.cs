﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvIntergrationModule.AnorsvIntegrationConfiguration;

namespace anorsv.AnorsvIntergrationModule
{
  partial class AnorsvIntegrationConfigurationClientHandlers
  {

    public virtual void NeedAuthorizationValueInput(Sungero.Presentation.BooleanValueInputEventArgs e)
    {
      bool needAutorization = _obj.NeedAuthorization == true;
      
      _obj.State.Properties.AuthorizationLogin.IsEnabled = needAutorization;
      _obj.State.Properties.AuthorizationLogin.IsRequired = needAutorization;
      _obj.State.Properties.AuthorizationPasswordVisible.IsEnabled = needAutorization;
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      bool needAutorization = _obj.NeedAuthorization == true;
      
      _obj.State.Properties.AuthorizationLogin.IsEnabled = needAutorization;
      _obj.State.Properties.AuthorizationLogin.IsRequired = needAutorization;
      _obj.State.Properties.AuthorizationPasswordVisible.IsEnabled = needAutorization;
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      bool needAutorization = _obj.NeedAuthorization == true;
      
      _obj.State.Properties.AuthorizationLogin.IsEnabled = needAutorization;
      _obj.State.Properties.AuthorizationLogin.IsRequired = needAutorization;
      _obj.State.Properties.AuthorizationPasswordVisible.IsEnabled = needAutorization;
   }
  }

}