﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonLibrary;
using Sungero.Core;
using Sungero.CoreEntities;
using ServiceFunctions = anorsv.ServiceLibrary.PublicFunctions;
using anorsv.AnorsvCoreModule;
using anorsv.AnorsvIntergrationModule;

namespace anorsv.AnorsvIntergrationModule.Client
{
  public class ModuleFunctions
  {

    /// <summary>
    /// 
    /// </summary>
    public virtual void SendRequestForDepartamentsFrom1cZup()
    {
      var asyncHandler = AsyncHandlers.GetDepatamentsFrom1cZup.Create();
      asyncHandler.ServiceUrl = "http://10.0.1.36/ZUP_COPY_FETISOVMOB/hs/rri/divisions";
      asyncHandler.SystemId = "b13f1b15-32ec-45af-9b4e-eaa56246546f";
      asyncHandler.EntityTypeId = "53018e85-150d-4cd8-90bb-d05d3c3927a0";
      asyncHandler.NeedAuthorization = true;
      asyncHandler.User = "ExchangeDirectum";
      asyncHandler.Password = "bf3411c2-59f7-465d-a778-d620a401aae4";
      asyncHandler.ExecuteAsync();
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void ClearEmptySessions()
    {
      var dialog = Dialogs.CreateInputDialog("Удаление пустых сессий");
      
      var systemInput = dialog.AddSelect("Система", true, anorsv.AnorsvCoreModule.AnorsvSystems.Null);
      systemInput.IsRequired = true;
      
      dialog.Buttons.AddOkCancel();
      
      if (dialog.Show() != DialogButtons.Ok)
      {
        return;
      }
      
      Functions.Module.Remote.ClearEmptySessions(systemInput.Value.Id);
      
      Dialogs.ShowMessage("Пустые сессии удалены");
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void FirstSync()
    {
      bool inProcessing = false;
      int top = 100;
      int step = 0;
      List<int> awaitingProcessingIds = new List<int>();
      
      var dialog = Dialogs.CreateInputDialog("Выберите данные");
      dialog.Width = 400;
      dialog.Height = 500;
      
      var systemInput = dialog.AddSelect("Система", true, AnorsvSystems.Null);
      systemInput.IsRequired = true;
      
      var entityTypeInput = dialog.AddSelect("Тип внешней сущности", true, AnorsvExternalEntityTypes.Null);
      entityTypeInput.IsRequired = true;
      entityTypeInput.IsEnabled = false;
      
      var sessionInput = dialog.AddSelect("Сессия", true, AnorsvIntegrationSessions.Null);
      sessionInput.IsRequired = true;
      sessionInput.IsEnabled = false;
      
      var onlyLinking = dialog.AddBoolean("Только связывать", true);
      var portionSize = dialog.AddInteger("Сколько обработать за раз", true, top);
      var total = dialog.AddInteger("Всего элементов", true, 0);
      var awaitingProcessing = dialog.AddInteger("Всего для обработки", true, 0);
      var processed = dialog.AddInteger("Всего Обработано", true, 0);
      var successed = dialog.AddInteger("Обработано успешно", true, 0);
      var unsuccessed = dialog.AddInteger("Обработано с ошибками", true, 0);
      var created = dialog.AddInteger("Соаздано новых", true, 0);
      var updated = dialog.AddInteger("Обновлено", true, 0);
      var linked = dialog.AddInteger("Связано", true, 0);
      
      var processingHyperlink = dialog.AddHyperlink("Обработать");
      
      dialog.Buttons.AddOkCancel();
      
      systemInput.SetOnValueChanged(
        system => {
          if (system.NewValue == null)
          {
            entityTypeInput.Value = null;
            entityTypeInput.IsEnabled = false;
          }
          else if (!system.NewValue.Equals(system.OldValue))
          {
            entityTypeInput.Value = null;
            entityTypeInput.From(Functions.Module.Remote.GetEntityTypesInSynchronizationDialog(system.NewValue.Id).ToList());
            entityTypeInput.IsEnabled = true;
          }
        });
      
      entityTypeInput.SetOnValueChanged(
        entityType => {
          if (entityType.NewValue == null)
          {
            sessionInput.Value = null;
            sessionInput.IsEnabled = false;
          }
          else if (!entityType.NewValue.Equals(entityType.OldValue))
          {
            sessionInput.Value = null;
            sessionInput.From(Functions.Module.Remote.GetSessionsInSynchronizationDialog(systemInput.Value.Id, entityType.NewValue.Id).ToList());
            sessionInput.IsEnabled = true;
          }
        });

      sessionInput.SetOnValueChanged(
        session => {
          total.Value = 0;
          awaitingProcessing.Value = 0 ;
          processed.Value = 0;
          successed.Value = 0;
          unsuccessed.Value = 0;
          created.Value = 0;
          updated.Value = 0;
          linked.Value = 0;
          
          if (session.NewValue == null)
          {
            awaitingProcessingIds.Clear();
          }
          else if (session.NewValue != session.OldValue)
          {
            inProcessing = false;
            step = 0;
            awaitingProcessingIds = Functions.Module.Remote.GetElementsAwaitingProcessing(sessionInput.Value.Id);
            onlyLinking.IsEnabled = session.NewValue != null;
          }
        }
       );
      
      portionSize.SetOnValueChanged(
        size => {
          if (size.NewValue != null && size.NewValue > 0)
          {
            top = size.NewValue.Value;
          }
          else
          {
            portionSize.Value = top;
          }
        }
       );
      
      dialog.SetOnRefresh(
        e => {
          portionSize.IsVisible = inProcessing;
          total.IsVisible = inProcessing;
          awaitingProcessing.IsVisible = inProcessing;
          processed.IsVisible = inProcessing;
          successed.IsVisible = inProcessing;
          unsuccessed.IsVisible = inProcessing;
          created.IsVisible = inProcessing;
          updated.IsVisible = inProcessing;
          linked.IsVisible = inProcessing;
          processingHyperlink.IsVisible = sessionInput.Value != null;
          onlyLinking.IsEnabled = sessionInput.Value != null;
        }
       );
      
      processingHyperlink.SetOnExecute(
        () => {
          if (inProcessing && step * top <= awaitingProcessingIds.Count)
          {
            Dialogs.NotifyMessage("Обработка очередной порции данных начата");
            var portion = awaitingProcessingIds.Skip(step * top).Take(top);
            foreach (int elementId in portion)
            {
              if (entityTypeInput.Value.InternalEntityType.InternalName == "Person")
              {
                Functions.Module.Remote.SynchronyzePersonFrom1cZup(elementId, true, false, !onlyLinking.Value.Value);
              }
              else if (entityTypeInput.Value.InternalEntityType.InternalName == "Departament")
              {
                Functions.Module.Remote.SynchronyzeDepartamentFrom1cZup(elementId, true, false, !onlyLinking.Value.Value);
              }
              else
              {
                
              }
            }
            step++;
            Dialogs.ShowMessage("Обработка очередной порции данных завершена");
          }
          else
          {
            inProcessing = true;
          }

          System.Collections.Generic.Dictionary<string, int> counts =
            ServiceFunctions.Module.UnboxDictionary(
              Functions.Module.Remote.GetElementsProcessingStatusInSession(sessionInput.Value.Id)
             );
          
          total.Value = counts["Total"];
          awaitingProcessing.Value = counts["AwaitingProcessing"];
          processed.Value = counts["Processed"];
          successed.Value = counts["Successed"];
          unsuccessed.Value = counts["Unsuccessed"];
          created.Value = counts["Created"];
          updated.Value = counts["Updated"];
          linked.Value = counts["Linked"];
        }
       );
      
      if (dialog.Show() != DialogButtons.Ok)
      {
        return;
      }
      
      
    }
    
    /// <summary>
    /// 
    /// </summary>
    public virtual void SendRequestForPersonsFrom1cZup()
    {
      var asyncHandler = AsyncHandlers.GetPersonsFrom1cZup.Create();
      string personInternalEntityTypeGuid = Constants.Module.InternalEntiyType.Person.EntityTypeId;
      var personInternalEntityTypeId = AnorsvInternalEntityTypes
        .GetAll(type => type.EntityTypeId.Equals(personInternalEntityTypeGuid, StringComparison.OrdinalIgnoreCase))
        .Select(type => type.Id)
        .FirstOrDefault();
      var availableSettings = AnorsvGenericIntegrationConfigurations
        .GetAll(
          settings => settings.Status == AnorsvGenericIntegrationConfiguration.Status.Active
          && settings.ExternalEntityType.InternalEntityType.Id == personInternalEntityTypeId
         )
        .ToArray();
      
      var settingsSelectDialog = Dialogs
        .CreateInputDialog(
          "Выбор сущности"
          , "Укажите тип внешней сущности для которой нужно провести сеанс получения данных."
         );
      settingsSelectDialog.Width = 600;
      
      var settingsInput = settingsSelectDialog
        .AddSelect<IAnorsvGenericIntegrationConfiguration>(
          "Настройки интеграции"
          , true
          , AnorsvGenericIntegrationConfigurations.Null
         );
      settingsInput.From(availableSettings);
      
      settingsSelectDialog.Buttons.AddOkCancel();
      
      if (settingsSelectDialog.Show() == DialogButtons.Ok)
      {
        asyncHandler.settingsId = settingsInput.Value.Id;
        asyncHandler.ExecuteAsync();
      }
    }

  }
}