﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.EntityModule.Server
{
  public class ModuleFunctions
  {
    #region Получение сведений о сущностях
    
    #region Получение сведений о сущностях для интеграции с 1с
    
    /// <summary>
    /// Определение Id сущности по значению ключевого для 1с атрибута.
    /// </summary>
    /// <param name="entityTypeName">Имя типа сущности.</param>
    /// <param name="keyValue">Значение ключевого атрибута.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    [Public(WebApiRequestType = RequestType.Post)]
    public virtual int GetEntityId1cIntegration(string entityTypeName, string keyValue)
    {
      int entityId = -1;
      List<string> allowedTypes = new List<string>() {
        "ProjectKind", "Employee", "CounterParty", "Project", "FRC", "IdentityPP", "PurchaseMethod", "JustificationPurchase",
        "SourcesFinancing", "PlanNeedsLine", "PurchaseRequest"
      };
      
      try
      {
        if (!allowedTypes.Contains(entityTypeName))
        {
          var printableAllowedTypes = string.Join(", ", allowedTypes.Select(s => string.Format("\"{0}\"", s)));
          var ex = AppliedCodeException.Create(
            string.Format(
              "Unexcepted entityTypeName = \"{0}\". Support only this entity types: {1}."
              , entityTypeName
              , printableAllowedTypes)
           );
          throw ex;
        }
        
        if (string.IsNullOrWhiteSpace(keyValue) || string.IsNullOrEmpty(keyValue))
        {
          throw AppliedCodeException.Create("Parametr keyValue can't be empty or contains only white spaces");
        }
        
        switch (entityTypeName)
        {
          case "ProjectKind":
            entityId = GetProjectKindIdByName(keyValue);
            break;
          case "Employee":
            entityId = GetEmployeeIdByGuid(keyValue);
            break;
          case "CounterParty":
            entityId = GetCounterpartyIdByName(keyValue);
            break;
          case "Project":
            entityId = GetProjectIdByGuid(keyValue);
            break;
          case "FRC":
            entityId = GetFrcIdByGuid(keyValue);
            break;
          case "IdentityPP":
            entityId = GetIdentityPpIdByGuid(keyValue);
            break;
          case "PurchaseMethod":
            entityId = GetPurchaseMethodIdByCode(keyValue);
            break;
          case "JustificationPurchase":
            entityId = GetJustificationPurchaseIdByGuid(keyValue);
            break;
          case "SourcesFinancing":
            entityId = GetSourcesFinancingIdByGuid(keyValue);
            break;
          case "PlanNeedsLine":
            entityId = GetPlanNeedsLineIdByGuid(keyValue);
            break;
          case "PurchaseRequest":
            entityId = GetPurchaseRequestIdByGuid(keyValue);
            break;
          default:
            entityId = -1;
            break;
        }
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat("{0}: {1}", ex.Message, ex.StackTrace);
        throw ex;
      }
      
      return entityId;
    }
    
    /// <summary>
    /// Поиск Id вида проекта по его наименованию.
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetProjectKindIdByName(string keyValue)
    {
      int entityId = Sungero.Projects.ProjectKinds
        .GetAll(e => e.Name.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id работника по свойству "GUIDsline".
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetEmployeeIdByGuid(string keyValue)
    {
      int entityId = sline.RSV.Employees
        .GetAll(e => e.GUIDsline.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id Контрагента по его наименованию.
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetCounterpartyIdByName(string keyValue)
    {
      int entityId = Sungero.Parties.Counterparties
        .GetAll(e => e.Name.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id проекта по значению свойства "GUIDsline".
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetProjectIdByGuid(string keyValue)
    {
      int entityId = sline.RSV.Projects
        .GetAll(e => e.GUIDsline.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id ЦФО по значению свойства "GUID".
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetFrcIdByGuid(string keyValue)
    {
      int entityId = sline.CustomModule.FRCs
        .GetAll(e => e.GUID.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id идентификатора ПП по значению свойства "GUID".
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetIdentityPpIdByGuid(string keyValue)
    {
      int entityId = sline.CustomModule.IdentityPPs
        .GetAll(e => e.GUID.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id способа закупки по значению свойства "Code".
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetPurchaseMethodIdByCode(string keyValue)
    {
      int entityId = sline.CustomModule.PurchaseMethods
        .GetAll(e => e.Code.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id обосноввания закупки по значению свойства "GUID".
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetJustificationPurchaseIdByGuid(string keyValue)
    {
      int entityId = sline.CustomModule.JustificationPurchases
        .GetAll(e => e.GUID.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id источника финансирования по значению свойства "GUID".
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetSourcesFinancingIdByGuid(string keyValue)
    {
      int entityId = sline.CustomModule.SourcesFinancings
        .GetAll(e => e.GUID.Equals(keyValue, StringComparison.OrdinalIgnoreCase)).Select(e => e.Id).FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }

    /// <summary>
    /// Поиск Id строки плана потребностей по GUID идентификатора ПП.
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetPlanNeedsLineIdByGuid(string keyValue)
    {
      int entityId = sline.CustomModule.PlanNeedsLines
        .GetAll(e => e.IdPP.GUID.Equals(keyValue, StringComparison.OrdinalIgnoreCase))
        .OrderByDescending(e => e.Created)
        .Select(e => e.Id)
        .FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }
    
    /// <summary>
    /// Поиск Id заявки на закупку по GUID идентификатора ПП.
    /// </summary>
    /// <param name="keyValue">Значение ключа по которому ведётся поиск.</param>
    /// <returns>Возвращает Id найденной сущности или -1.</returns>
    private int GetPurchaseRequestIdByGuid(string keyValue)
    {
      int entityId = sline.CustomModule.PurchaseRequests
        .GetAll(e => e.IdentityPP.GUID.Equals(keyValue, StringComparison.OrdinalIgnoreCase))
        .OrderByDescending(e => e.Created)
        .Select(e => e.Id)
        .FirstOrDefault();
      
      return entityId > 0 ? entityId : -1;
    }
    
    #endregion
    
    #endregion
  }
}