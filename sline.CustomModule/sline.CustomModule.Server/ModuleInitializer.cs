using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace sline.CustomModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      CreateSetting();
      CreateDocumentTypes();
      UpgradeDocumentKinds();
      CreateRelationTypes();
      GrantAccessRightsForDatabook();
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.PurchaseDZD, "Закупщик ДЗД");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.AppManagers, "Руководители инициатора");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.PurchRespons, "Ответственный за закупку");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.ApprORD, "Согласующие по тематикам ОРД");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.ManagLessFirst, "Руководители инициатора (без непосредственного руководителя)");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.PreApprovers, "Предварительно согласующие");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.SubstNotice, "Уведомляемые о замещении");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.SubstApprov, "Согласующие замещение");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.GDDeputy, "Согласующий Заместитель Генерального директора");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.DocAuthor, "Автор документа");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.ResponsibleFRC, "Ответственный за ЦФО");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.AppManager, "Непосредственный руководитель по иерархии");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.AppManagerGDDep, "Непосредственный руководитель по иерархии до ЗГД");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.ZGDWithoutFirst, "Заместитель ГД (кроме ПЗГД)");
      CreateApprovalRole(sline.CustomModule.MyApprovalRoles.Type.GDDirectSubord, "Руководители прямого подчинения ГД");
      FillNullableBool();
      SyncProjectsInAllPurchaseRequests();
      DeleteEmptyTxtVersions();
      FillNullTemplateJob();
      //SyncDesiredDateReg(); // Рахимова 18.10.2022 Поле ожидаемая дата регистрации скрыто из всех карточек документа и из все проверок
      CreateRoles();
      FillMemoTopics();
    }
    
    /// <summary>
    /// Обновление видов документов
    /// </summary>
    public static void UpgradeDocumentKinds()
    {
      // Договоры расходные
      var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.ExpendableContractKind);
      if (expendableContractKind == null)
      {
        expendableContractKind = Sungero.Docflow.DocumentKinds.GetAll().Where(k => k.Name == sline.CustomModule.Resources.ExpendableContract && k.Status == Sungero.Docflow.DocumentKind.Status.Active).FirstOrDefault();
        if (expendableContractKind != null)
          Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(expendableContractKind, Constants.Module.DocKind.ExpendableContractKind);
      }
      // Служебные записки по закупочной деятельности
      var procurementActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.ProcurementActivitiesKind);
      if (procurementActivitiesKind == null)
      {
        procurementActivitiesKind = Sungero.Docflow.DocumentKinds.GetAll().Where(k => k.Name == sline.CustomModule.Resources.ProcurementActivities).FirstOrDefault(); //&& k.Status == Sungero.Docflow.DocumentKind.Status.Active
        if (procurementActivitiesKind != null)
          Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(procurementActivitiesKind, Constants.Module.DocKind.ProcurementActivitiesKind);
      }
      // Закупка у единственного поставщика
      var singlePurchaseKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.SinglePurchaseKind);
      if (singlePurchaseKind == null)
      {
        singlePurchaseKind = Sungero.Docflow.DocumentKinds.GetAll().Where(k => k.Name == sline.CustomModule.Resources.SinglePurchase && k.Status == Sungero.Docflow.DocumentKind.Status.Active).FirstOrDefault();
        if (singlePurchaseKind != null)
          Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(singlePurchaseKind, Constants.Module.DocKind.SinglePurchaseKind);
      }
      // Конкурентная закупка
      var competitivePurchaseKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.CompetitivePurchaseKind);
      if (competitivePurchaseKind == null)
      {
        competitivePurchaseKind = Sungero.Docflow.DocumentKinds.GetAll().Where(k => k.Name == sline.CustomModule.Resources.CompetitivePurchase && k.Status == Sungero.Docflow.DocumentKind.Status.Active).FirstOrDefault();
        if (competitivePurchaseKind != null)
          Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(competitivePurchaseKind, Constants.Module.DocKind.CompetitivePurchaseKind);
      }
      // Служебные записки в ДИТ
      var memoDITKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.memoDITKind);
      if (memoDITKind == null)
      {
        memoDITKind = Sungero.Docflow.DocumentKinds.GetAll().Where(k => k.Name == sline.CustomModule.Resources.MemoDIT).FirstOrDefault();
        if (memoDITKind != null)
          Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(memoDITKind, Constants.Module.DocKind.memoDITKind);
      }
      // Закупка по счету
      var purchaseByInvoiceKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.PurchaseByInvoiceKind);
      if (purchaseByInvoiceKind == null)
      {
        purchaseByInvoiceKind = Sungero.Docflow.DocumentKinds.GetAll().Where(k => k.Name == sline.CustomModule.Resources.PurchaseByInvoice && k.Status == Sungero.Docflow.DocumentKind.Status.Active).FirstOrDefault();
        if (purchaseByInvoiceKind != null)
          Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(purchaseByInvoiceKind, Constants.Module.DocKind.PurchaseByInvoiceKind);
      }
      
    }
    
    public void CreateSetting()
    {
      InitializationLogger.Debug("Init: Create Setting");
      var settings = sline.CustomModule.Settings.GetAll();
      if (!settings.Any())
      {
        var setting = sline.CustomModule.Settings.Create();
        setting.TimeToExecution = 3;
        setting.SubtasksPrefix = "==>>";
        setting.Save();
      }
    }
    
    public static void CreateDocumentTypes()
    {
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Заявка на закупку", PurchaseRequest.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Inner, true);
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Сведения для изменения (расторжения) договора", ContractChange.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Contracts, true);
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Протокол комиссии по закупкам", MinutesPurchase.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Inner, true);
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Строка плана потребностей", PlanNeedsLine.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Inner, true);
    }
    
    public void GrantAccessRightsForDatabook()
    {
      InitializationLogger.Debug("Init: Grant access rights for databooks, documents.");
      var allUsers = Roles.AllUsers;

      // справочники
      InitializationLogger.Debug("Init: Grant access rights for ЦФО.");
      FRCs.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      FRCs.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Идентификатор ПП.");
      IdentityPPs.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      IdentityPPs.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Основание закупки.");
      JustificationPurchases.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      JustificationPurchases.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Способ закупки.");
      PurchaseMethods.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      PurchaseMethods.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Источник финансирования.");
      SourcesFinancings.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      SourcesFinancings.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Настройка.");
      Settings.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      Settings.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Тематика ОРД.");
      TopicOrds.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      TopicOrds.AccessRights.Save();
      
      InitializationLogger.Debug("Init: Grant access rights for Тематика СЗ.");
      TopicMemos.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      TopicMemos.AccessRights.Save();
      
      // документы
      InitializationLogger.Debug("Init: Grant access rights for Заявка на закупку.");
      PurchaseRequests.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Create);
      PurchaseRequests.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Протокол комиссии по закупкам.");
      MinutesPurchases.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Create);
      MinutesPurchases.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Сведения для изменения (расторжения) договора.");
      ContractChanges.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Create);
      ContractChanges.AccessRights.Save();

      InitializationLogger.Debug("Init: Grant access rights for Строка плана потребностей.");
      PlanNeedsLines.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Create);
      PlanNeedsLines.AccessRights.Save();
    }
    
    public static void CreateApprovalRole(Enumeration roleType, string description)
    {
      var role = MyApprovalRoleses.GetAll().Where(r => Equals(r.Type, roleType)).FirstOrDefault();

      // Проверяет наличие роли.
      if (role == null)
      {
        role = MyApprovalRoleses.Create();
        role.Type = roleType;
      }
      role.Name = description;
      role.Description = description;
      role.Save();
      
      InitializationLogger.DebugFormat("Init: Создана/обновлена роль '{0}'", description);
    }
    
    public void FillNullableBool()
    {
      InitializationLogger.Debug("Init: fill nullable boolean values");
      var titles = sline.RSV.JobTitles.GetAll().Where(x => x.Ignoresline == null);
      foreach (var title in titles)
      {
        title.Ignoresline = false;
        title.Save();
      }
      
      var stages = sline
        .RSV
        .ApprovalStages
        .GetAll()
        .Where(
          x => x.IgnoreStagessline == null
          || x.ZGDsline == null
          || x.CreateApprovalSheetsline == null
          || x.SubtasksIsAllowedanorsv == null
          || x.SubtasksIsAllowedanorsv == null
          || x.BCHPanorsv == null
          || x.PreliminaryApprovalBlockanorsv == null
          || x.AgreeWithoutFinalVerificationanorsv == null
          || x.SkipStageWithoutFinalVerificationanorsv == null
          //|| x.NoSecondApprovesline == null
         );
      
      foreach (var stage in stages)
      {
        if (stage.IgnoreStagessline == null)
          stage.IgnoreStagessline = false;
        if (stage.ZGDsline == null)
          stage.ZGDsline = false;
        if (stage.CreateApprovalSheetsline == null)
          stage.CreateApprovalSheetsline = false;
        if (stage.SubtasksIsAllowedanorsv == null)
          stage.SubtasksIsAllowedanorsv = true;
        if (stage.SubtaskInitiatorByDefaultanorsv == null)
          stage.SubtaskInitiatorByDefaultanorsv = sline.RSV.ApprovalStage.SubtaskInitiatorByDefaultanorsv.Performer;
        if (stage.BCHPanorsv == null)
          stage.BCHPanorsv = false;
        if (stage.PreliminaryApprovalBlockanorsv == null)
          stage.PreliminaryApprovalBlockanorsv = false;
        if (stage.AgreeWithoutFinalVerificationanorsv == null)
          stage.AgreeWithoutFinalVerificationanorsv = false;
        if (stage.SkipStageWithoutFinalVerificationanorsv == null)
          stage.SkipStageWithoutFinalVerificationanorsv = false;
        //        if (stage.NoSecondApprovesline == null)
        //          stage.NoSecondApprovesline = false;
        stage.Save();
      }
    }

    public void SyncProjectsInAllPurchaseRequests()
    {
      bool purchaseRequestsIsNotEmpty = true;
      
      while (purchaseRequestsIsNotEmpty)
      {
        var purchaseRequests = sline
          .CustomModule
          .PurchaseRequests
          .GetAll(d => d.ProjectCollection.Count() != d.ProjectsCollectionanorsv.Count())
          .Take(10).ToList();
        
        foreach(var purchaseRequest in purchaseRequests)
        {
          purchaseRequest.Name = purchaseRequest.Name;
          purchaseRequest.Save();
        }
        
        purchaseRequestsIsNotEmpty = purchaseRequests.Count() > 0;
      }
    }
    
    public void DeleteEmptyTxtVersions()
    {
      InitializationLogger.Debug("Init: Delete empty text versions in PurchaseRequests.");
      
      var documents  = PurchaseRequests.GetAll(d => d.HasVersions && d.Versions.Any(v => v.AssociatedApplication.Extension == "txt"));
      int currentProcessedDocumentsCount = 0;
      int previousProcessedDocumentsCount = 0;
      
      foreach (var document in documents)
      {
        var versions = document.Versions.Where(v => v.AssociatedApplication.Extension == "txt" && v.Body.Size < 3).ToList();
        
        if (versions.Count() > 0)
        {
          foreach (var version in versions)
          {
            document.DeleteVersion(version);
          }
          
          document.Save();
          currentProcessedDocumentsCount++;
        }
        
        if (currentProcessedDocumentsCount - previousProcessedDocumentsCount == 50)
        {
          InitializationLogger.DebugFormat("Processed {0} documents.", currentProcessedDocumentsCount);
          previousProcessedDocumentsCount = currentProcessedDocumentsCount;
        }
      }
      if (currentProcessedDocumentsCount != previousProcessedDocumentsCount)
      {
        InitializationLogger.DebugFormat("Processed {0} documents.", currentProcessedDocumentsCount);
      }
    }
    
    public void FillNullTemplateJob()
    {
      InitializationLogger.Debug("Init: fill nullable boolean values and job titles for template in employees");
      var emps = sline.RSV.Employees.GetAll().Where(x => x.NotUpdateAutomaticallyanorsv == null || x.Manuallyanorsv == null || x.TemplateJobanorsv == "" || x.TemplateJobanorsv == null);
      foreach (var emp in emps)
      {
        try
        {
          if (emp.NotUpdateAutomaticallyanorsv == null)
            emp.NotUpdateAutomaticallyanorsv = false;
          if (emp.Manuallyanorsv == null)
          {
            emp.Manuallyanorsv = false;
            sline.RSV.PublicFunctions.Employee.UpdateTemplateJob(emp);
          }
          if (emp.TemplateJobanorsv == "" || emp.TemplateJobanorsv == null)
            sline.RSV.PublicFunctions.Employee.UpdateTemplateJob(emp);
          
          emp.Save();
        }
        catch (Exception ex)
        {
          InitializationLogger.Debug("Init EXCEPTION: error in template filling for employee " + emp.Id + ": " + ex.Message);
        }
      }
    }
    
    public void SyncDesiredDateReg()
    {
      InitializationLogger.Debug("Init: fill desired registration date in CompanyDirectives");
      
      var documents  = sline.RSV.CompanyDirectives.GetAll(d => d.DesiredDateRegsline == null && d.PlacedToCaseFileDate != null);
      foreach (var document in documents)
      {
        try
        {
          document.DesiredDateRegsline = document.PlacedToCaseFileDate;
          document.Save();
        }
        catch (Exception ex)
        {
          InitializationLogger.Debug("Init EXCEPTION: error in desired registration date fiiling for document " + document.Id + ": " + ex.Message);
        }
      }
    }
    
    public void CreateRoles()
    {
      var role = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(
        Constants.Module.Roles.DateChangingRole.RoleName
        , Constants.Module.Roles.DateChangingRole.RoleDescription
        , Constants.Module.Roles.DateChangingRole.RoleGuid
       );
      
      role.IsSystem = true;
      role.Save();
    }
    
    public void FillMemoTopics()
    {
      InitializationLogger.Debug("Init: fill topic in Memos");
      
      var documents  = sline.RSV.Memos.GetAll(d => d.MemoTopicanorsv == null);
      foreach (var document in documents)
      {
        try
        {
          document.MemoTopicanorsv = sline.CustomModule.TopicMemos.GetAll(t => t.Name == "Тематика не обозначена" && t.DocumentKind.Equals(document.DocumentKind)).FirstOrDefault();
          if (document.MemoTopicanorsv != null)
            document.Save();
        }
        catch (Exception ex)
        {
          InitializationLogger.Debug("Init EXCEPTION: error in memo topic fiiling for document " + document.Id + ": " + ex.Message);
        }
      }
    }
    
    /// <summary>
    /// Создать типы связей.
    /// </summary>
    public static void CreateRelationTypes()
    {
      InitializationLogger.Debug("Init: Create relation type PurchaseMemo.");
      
      // СЗ по закупке - Закупка
      var memoForPurchase = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRelationType(Resources.MemoForPurchaseRelationName,
                                                                                                    Resources.RelationMemoForPurchaseSourceTitle,
                                                                                                    Resources.RelationMemoForPurchaseTargetTitle,
                                                                                                    Resources.RelationMemoForPurchaseSourceDescription,
                                                                                                    Resources.RelationMemoForPurchaseTargetDescription, false, false, true, true);
      memoForPurchase.IsSystem = false;
      memoForPurchase.Mapping.Clear();
      var memoForPurchaseRow = memoForPurchase.Mapping.AddNew();
      memoForPurchaseRow.Target = Sungero.Docflow.Memos.Info;
      memoForPurchaseRow.Source = sline.CustomModule.PurchaseRequests.Info;
      memoForPurchase.Save();
      
      InitializationLogger.Debug("Init: Create relation type Purchasentract");
      
      //Договор - Закупка
      var contractForPurchase = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRelationType(sline.CustomModule.Resources.ContractForPurchaseRelationName,
                                                                                                        sline.CustomModule.Resources.RelationContractForPurchaseSourceTitle,
                                                                                                        sline.CustomModule.Resources.RelationContractForPurchaseTargetTitle,
                                                                                                        sline.CustomModule.Resources.RelationContractForPurchaseSourceDescription,
                                                                                                        sline.CustomModule.Resources.RelationContractForPurchaseTartDescription,
                                                                                                        false, false, true, true);
      contractForPurchase.IsSystem = false;
      contractForPurchase.Mapping.Clear();
      var contractForPurchaseRow = contractForPurchase.Mapping.AddNew();
      contractForPurchaseRow.Target = Sungero.Contracts.Contracts.Info;
      contractForPurchaseRow.Source = sline.CustomModule.PurchaseRequests.Info;
      contractForPurchase.Save();
      
      //Техническое задание - Закупка
      var tsForPurchase = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRelationType(sline.CustomModule.Resources.TSForPurchaseRelationName,
                                                                                                  sline.CustomModule.Resources.RelationTSForPurchaseSourceTitle,
                                                                                                  sline.CustomModule.Resources.RelationTSForPurchaseTargetTitle,
                                                                                                  sline.CustomModule.Resources.RelationTSForPurchaseSourceDescription,
                                                                                                  sline.CustomModule.Resources.RelationTSForPurchaseTargetDescription,
                                                                                                  false, false, true, true);
      tsForPurchase.IsSystem = false;
      tsForPurchase.Mapping.Clear();
      var tsForPurchaseRow = tsForPurchase.Mapping.AddNew();
      tsForPurchaseRow.Target = anorsv.ContractsModule.TechnicalSpecifications.Info;
      tsForPurchaseRow.Source = sline.CustomModule.PurchaseRequests.Info;
      tsForPurchase.Save();
    }

    
  }
}
