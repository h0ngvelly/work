﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.PlanNeedsLine;

namespace sline.CustomModule
{
    partial class PlanNeedsLineAuthorPropertyFilteringServerHandler<T>
    {

        public override IQueryable<T> AuthorFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
        {
            query = base.AuthorFiltering(query, e);
            var users = Substitutions.ActiveSubstitutedUsers.ToList();
            if (!Users.Current.IncludedIn(Roles.Administrators) && users.Any())
            {
                users.Add(Users.Current);            
                query = query.Where(x => users.Contains(Sungero.CoreEntities.Users.As(x)));
            }
            return query;
        }
    }

}