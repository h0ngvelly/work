﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.IdentityPP;

namespace sline.CustomModule
{
    partial class IdentityPPServerHandlers
    {

        public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_obj.Code) || !string.IsNullOrWhiteSpace(_obj.Subject))
                _obj.Name = _obj.Code + "-" + _obj.Subject;
        }

        public override void Created(Sungero.Domain.CreatedEventArgs e)
        {
            _obj.Approved = false;
        }
    }

    partial class IdentityPPFRCPropertyFilteringServerHandler<T>
    {

        public virtual IQueryable<T> FRCFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
        {
            query = query.Where(x => x.Status != sline.CustomModule.IdentityPP.Status.Closed);
            return query;
        }
    }

}