using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.MyApprovalRoles;

namespace sline.CustomModule.Server
{
  partial class MyApprovalRolesFunctions
  {
    [Public]
    public override Sungero.Company.IEmployee GetRolePerformer(Sungero.Docflow.IApprovalTask task)
    {
      #region Ответственный за закупку
      if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.PurchRespons)
      {
        var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
        var purchaseRequest = PurchaseRequests.As(document);
        if (purchaseRequest != null)
          return purchaseRequest.ResponsibleEmployee;
        else
          return null;
      }
      #endregion
      
      #region Закупщик ДЗД
      if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.PurchaseDZD)
      {
        var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
        var purchaseRequest = PurchaseRequests.As(document);
        if (purchaseRequest != null)
          return purchaseRequest.EmployeeDZD;
        else
          return null;
      }
      #endregion
      
      #region Автор документа
      if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.DocAuthor)
      {
        var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
        if (document.Author != null)
          return Sungero.Company.Employees.As(document.Author);
        else
          return null;
      }
      #endregion

      #region Ответственный за ЦФО
      if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.ResponsibleFRC)
      {
        var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
        var purchaseRequest = PurchaseRequests.As(document);
        if (purchaseRequest != null)
        {
          if (purchaseRequest.Frc != null)
            return purchaseRequest.Frc.Responsible;
        }
        else
          return null;
      }
      #endregion
      
      #region Непосредственный руководитель по иерархии
      if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.AppManager)
      {
        var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
        
        if (document != null)
        {
          var employee = Sungero.Company.Employees.As(document.Author);
          if (employee == null)
            employee = Sungero.Company.Employees.GetAll().FirstOrDefault(x => x.Id == document.Author.Id);

          var department = sline.RSV.Departments.As(employee.Department);
          var manager = Sungero.Company.Employees.Null;
          var managerResult = Sungero.Company.Employees.Null;
          do
          {
            manager = GetManager(department);
            if (manager != null && manager != employee)
              managerResult = Sungero.Company.Employees.As(manager);
            if (department != null)
              department = department.HeadOffice;
          }
          while (department != null && managerResult == null);
          
          return managerResult;
        }
      }
      #endregion
      
      #region Непосредственный руководитель по иерархии до ЗГД
      if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagerGDDep)
      {
        var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
        
        if (document != null)
        {
          var employee = Sungero.Company.Employees.As(document.Author);
          if (employee == null)
            employee = Sungero.Company.Employees.GetAll().FirstOrDefault(x => x.Id == document.Author.Id);

          var department = sline.RSV.Departments.As(employee.Department);
          var manager = Sungero.Company.Employees.Null;
          var managerResult = Sungero.Company.Employees.Null;
          bool? departmentIsZGD = false;
          do
          {
            departmentIsZGD = (department.ZGDanorsv != null) ? department.ZGDanorsv : false;
            manager = GetManager(department);
            if (manager != null && manager != employee && departmentIsZGD == false)
              managerResult = Sungero.Company.Employees.As(manager);
            if (department != null)
              department = department.HeadOffice;
          }
          while (department != null && managerResult == null && departmentIsZGD == false);
          
          return managerResult;
        }
      }
      #endregion
      
      #region Заместитель Генерального директора
      if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.GDDeputy)
      {
        var managerZGD = Sungero.Company.Employees.Null;
        var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
        
        if (document != null)
        {
          var employee = Sungero.Company.Employees.As(document.Author);
          if (employee == null)
            employee = Sungero.Company.Employees.GetAll().FirstOrDefault(x => x.Id == document.Author.Id);
          
          if (employee != null)
          {
            var department = sline.RSV.Departments.As(employee.Department);
            var manager = Sungero.Company.Employees.Null;
            var departmentIsZGD = department.ZGDanorsv;
            
            do
            {
              manager = GetManager(department);
              department = department.HeadOffice;
              
              if (departmentIsZGD == true && manager != null)
                managerZGD = manager;
              else
                departmentIsZGD = (department != null) ? department.ZGDanorsv : null;
              
            }
            while (department != null && (managerZGD == null));
          }
        }
        
        return managerZGD;
      }
      #endregion
      
      return base.GetRolePerformer(task);
    }
    
    [Remote(IsPure = true), Public]
    public Sungero.Company.IEmployee GetRolePerformer1(Sungero.Docflow.IApprovalTask task)
    {
      return GetRolePerformer(task);
    }
    
    [Remote(IsPure = true), Public]
    public List<Sungero.Company.IEmployee> GetRolePerformers(Sungero.Docflow.IApprovalTask task)
    {
      var result = new List<Sungero.Company.IEmployee>();
      
      try
      {
        
        #region Регистратор документа
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.DocRegister)
        {
          var clerk = Sungero.Docflow.PublicFunctions.Module.Remote.GetClerk(task.DocumentGroup.OfficialDocuments.FirstOrDefault());
          if (clerk != null)
            result.Add(clerk);
          if (clerk == null && task.Author != null)
            result.Add(Sungero.Company.Employees.GetAll().Where(emp => Equals(emp.Login, task.Author.Login)).FirstOrDefault());
          
          return result;
        }
        #endregion
        
        #region Руководитель проекта
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.ProjectManager)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          if (document != null)
          {
            var purchaseRequest = PurchaseRequests.As(document);
            if (purchaseRequest != null)
            {
              if (purchaseRequest.Project != null)
                result.Add(purchaseRequest.Project.Manager);
            }
          }
          return result;
        }
        #endregion
        
        #region Ответственный за ЦФО
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.ResponsibleFRC)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var purchaseRequest = PurchaseRequests.As(document);
          if (purchaseRequest != null)
          {
            if (purchaseRequest.Frc != null)
              result.Add(purchaseRequest.Frc.Responsible);
          }
          
          return result;
        }
        #endregion
        
        #region Непосредственный руководитель по иерархии до ЗГД
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagerGDDep)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          
          if (document != null)
          {
            var employee = sline.RSV.Employees.As(document.Author);
            if (employee == null)
              employee = sline.RSV.Employees.GetAll().FirstOrDefault(x => x.Id == document.Author.Id);

            var department = employee.Department;
            var manager = sline.RSV.Employees.Null;
            var managerResult = Sungero.Company.Employees.Null;
            bool? departmentIsZGD = false;
            do
            {
              departmentIsZGD = (department.ZGDanorsv != null) ? department.ZGDanorsv : false;
              manager = GetManager(department);
              if (manager != null && manager != employee && departmentIsZGD == false)
                managerResult = Sungero.Company.Employees.As(manager);
              if (department != null)
                department = department.HeadOffice;
            }
            while (department != null && managerResult == null && departmentIsZGD == false);
            
            result.Add(managerResult);
            
            return result;
          }
        }
        #endregion
        
        #region Непосредственный руководитель по иерархии
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.AppManager)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          
          if (document != null)
          {
            var employee = sline.RSV.Employees.As(document.Author);
            if (employee == null)
              employee = sline.RSV.Employees.GetAll().FirstOrDefault(x => x.Id == document.Author.Id);

            var department = employee.Department;
            var manager = sline.RSV.Employees.Null;
            var managerResult = Sungero.Company.Employees.Null;
            do
            {
              manager = GetManager(department);
              if (manager != null && manager != employee)
                managerResult = Sungero.Company.Employees.As(manager);
              if (department != null)
                department = department.HeadOffice;
            }
            while (department != null && managerResult == null);
            
            result.Add(managerResult);
            
            return result;
          }
        }
        #endregion
        
        #region Закупщик ДЗД
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.PurchaseDZD)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var purchaseRequest = PurchaseRequests.As(document);
          if (purchaseRequest != null)
            result.Add(purchaseRequest.EmployeeDZD);
          
          return result;
        }
        #endregion
        
        #region Ответственный за закупку
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.PurchRespons)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var purchaseRequest = PurchaseRequests.As(document);
          if (purchaseRequest != null)
            result.Add(purchaseRequest.ResponsibleEmployee);
          
          return result;
        }
        #endregion
        
        #region Автор документа
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.DocAuthor)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          if (document.Author != null)
            result.Add(Sungero.Company.Employees.GetAll().Where(emp => Equals(emp.Login, document.Author.Login)).FirstOrDefault());
          
          return result;
        }
        #endregion
        
        #region Предварительно согласующие
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.PreApprovers)
        {
          var mainDocument = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var officialDocument = anorsv.OfficialDocument.OfficialDocuments.As(mainDocument);
          // Если указаны предварительные согласующие в тематике, то заполняем из тематики, иначе вручную
          var topic = officialDocument.Topicanorsv;
          if (topic != null && topic.PreApprovers.Any())
          {
            foreach (var preAppr in officialDocument.Topicanorsv.PreApprovers)
            {
              var employee = preAppr.Approver;
              if (employee != null && !result.Contains(employee))
                result.Add(employee);
            }
          }
          /*else
          {
            if (officialDocument.Topicanorsv != null && !officialDocument.Topicanorsv.PreApprovers.Any())
            {
              var taskSline = sline.RSV.ApprovalTasks.As(task);
              foreach (var preAppr in taskSline.PreApproversAnorsvanorsv)
              {
                var employee = preAppr.Approver;
                if (employee != null && !result.Contains(employee))
                  result.Add(employee);
              }
            }
          }*/
          
          var memo = sline.RSV.Memos.As(mainDocument);
          if (memo != null)
          {
            // Если указаны предварительные согласующие в тематике СЗ, то заполняем из тематики, иначе вручную
            if (memo.MemoTopicanorsv != null && memo.MemoTopicanorsv.PreApprovers.Any())
            {
              foreach (var preAppr in memo.MemoTopicanorsv.PreApprovers)
              {
                var employee = preAppr.Approver;
                if (employee != null && !result.Contains(employee))
                  result.Add(employee);
              }
            }
            /*else
            {
              if (memo.MemoTopicanorsv != null && !memo.MemoTopicanorsv.PreApprovers.Any())
              {
                var taskSline = sline.RSV.ApprovalTasks.As(task);
                foreach (var preAppr in taskSline.PreApproversAnorsvanorsv)
                {
                  var employee = preAppr.Approver;
                  if (employee != null && !result.Contains(employee))
                    result.Add(employee);
                }
              }
            }*/
            
          }
          
          // Если указаны предварительные согласующие в задаче вручную, то добавляем их в список
          var taskRSV = anorsv.ApprovalTask.ApprovalTasks.As(task);
          var taskPreApproversList = taskRSV.PreApproversAnorsvanorsv.Select(a => a.Approver).ToList();
          foreach (var employee in taskPreApproversList)
          {
            if (!result.Contains(employee))
              result.Add(Sungero.Company.Employees.As(employee));
          }
          
          return result;
        }
        #endregion
        
        #region Руководители инициатора
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagers)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          
          if (document != null)
          {
            var employee = Sungero.Company.Employees.As(document.Author);
            if (employee == null)
            {
              employee = Sungero.Company.Employees.GetAll().FirstOrDefault(x => x.Id == document.Author.Id);
            }
            var department = sline.RSV.Departments.As(employee.Department);
            var manager = Sungero.Company.Employees.Null;
            
            // Изменение вычисления согласующих до ЗГД - идти до признака в подразделении, а не до подразделения с отсутствием руководителя
            var departmentIsZGD = department.ZGDanorsv;
            
            // Если подразделение инициатора == ЗГД, добавить руководителя
            if (departmentIsZGD == true)
              result.Add(manager);
            
            // Если подразделение инициатора != ЗГД, идти до признака ЗГД в подразделении по иерархии
            if (departmentIsZGD != true)
            {
              do
              {
                manager = GetManager(department);
                if (manager != null && manager != employee)
                {
                  if (!result.Contains(manager))
                    result.Add(manager);
                }
                
                department = sline.RSV.Departments.As(department.HeadOffice);
                departmentIsZGD = (department != null) ? department.ZGDanorsv : null;
                // Если головное подразделение уже ЗГД и сам ЗГД уже был руководителем нижестоящего, то исключим его из списка
                if (departmentIsZGD == true)
                {
                  var managerZGD = GetManager(department);
                  if (managerZGD != null && result.Contains(managerZGD))
                    result.Remove(managerZGD);
                }
              }
              while (department != null && departmentIsZGD != true);
            }
            
            return result;
          }
        }
        #endregion
        
        #region Согласующие по тематикам ОРД
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.ApprORD)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var companyDirective = sline.RSV.CompanyDirectives.As(document);
          var order = sline.RSV.Orders.As(document);
          
          if (companyDirective != null)
          {
            return companyDirective.ApproversORDsline.Select(x => Sungero.Company.Employees.As(x.Approver)).ToList();
          }
          
          if (order != null)
          {
            return order.ApproversORDsline.Select(x => Sungero.Company.Employees.As(x.Approver)).ToList();
          }
        }
        #endregion
        
        #region Руководители инициатора (без непосредственного руководителя)
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.ManagLessFirst)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          
          if (document != null)
          {
            var employee = Sungero.Company.Employees.As(document.Author);
            
            if (employee == null)
            {
              employee = Sungero.Company.Employees.GetAll().FirstOrDefault(x => x.Id == document.Author.Id);
            }
            
            var department = sline.RSV.Departments.As(employee.Department);
            
            var manager = Sungero.Company.Employees.Null;
            
            // Изменение вычисления согласующих до ЗГД - идти до признака в подразделении, а не до подразделения с отсутствием руководителя
            var departmentIsZGD = department.ZGDanorsv;
            if (departmentIsZGD != true)
            {
              do
              {
                manager = GetManager(department);
                
                if (manager != null && manager != employee)
                {
                  var authorManager = Sungero.Docflow.PublicFunctions.Module.Remote.GetManager(document.Author);
                  if ((authorManager != null && manager != authorManager) || (authorManager == null))
                    if (!result.Contains(manager))
                      result.Add(manager);
                }
                
                department = department.HeadOffice;
                departmentIsZGD = (department != null) ? department.ZGDanorsv : null;
                
                // Если головное подразделение уже ЗГД и сам ЗГД уже был руководителем нижестоящего, то исключим его из списка
                if (departmentIsZGD == true)
                {
                  var managerZGD = GetManager(department);
                  if (managerZGD != null && result.Contains(managerZGD))
                    result.Remove(managerZGD);
                }
              }
              while (department != null && departmentIsZGD != true);
            }
            
            return result;
          }
        }
        #endregion
        
        #region Уведомляемые о замещении
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.SubstNotice)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var substRequest = anorsv.AnorsvSubstitution.SubstitutionRequestBases.As(document);
          
          if (substRequest != null)
          {
            if (substRequest.Employee != null && !result.Contains(substRequest.Employee))
              result.Add(substRequest.Employee);
            if (substRequest.Substitute != null && !result.Contains(substRequest.Substitute))
              result.Add(substRequest.Substitute);
            if (!result.Contains(task.Author))
              result.Add(Sungero.Company.Employees.GetAll().Where(emp => Equals(emp.Login, task.Author.Login)).FirstOrDefault());
          }
          
          return result;
        }
        #endregion
        
        #region Согласующие замещение
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.SubstApprov)
        {
          var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
          var substRequest = anorsv.AnorsvSubstitution.SubstitutionRequestBases.As(document);
          
          if (substRequest != null)
          {
            var manager = Sungero.Company.Employees.Null;
            var department = sline.RSV.Departments.Null;
            
            // Руководитель Замещаемого
            if (substRequest.Employee != null)
            {
              department = sline.RSV.Departments.As(substRequest.Employee.Department);
              if (department != null)
              {
                manager = GetManager(department);
                // Исправление ошибки при отсутствии руководителя подразделения
                if (manager != null && !result.Contains(manager))
                  result.Add(manager);
              }
            }
            
            // Руководитель Замещающего
            /*if (substRequest.Substitute != null)
                    {
                      department = sline.RSV.Departments.As(substRequest.Substitute.Department);
                      if (department != null)
                      {
                        manager = GetManager(department);
                        if (!result.Contains(manager))
                          result.Add(manager);
                      }
                    }*/
            
          }
          
          return result;
        }
        #endregion
        
        #region Заместитель ГД (кроме ПЗГД)
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.ZGDWithoutFirst)
        {
          var initiator = sline.RSV.Employees.As(task.Author);
          if (initiator == null)
            initiator = sline.RSV.Employees.GetAll().FirstOrDefault(x => x.Id == task.DocumentGroup.OfficialDocuments.FirstOrDefault().Author.Id);
          
          if (initiator != null)
          {
            var departmentGD = sline.RSV.Departments.GetAll()
              .Where(d => d.Manager != null && d.Manager.JobTitle != null && d.Manager.JobTitle.Name == "Генеральный директор")
              .FirstOrDefault();
            var rolePZGD = Roles.GetAll().Where(r => r.Sid == anorsv.DocflowModule.PublicConstants.Module.Roles.PZGD.RoleGuid).FirstOrDefault();
            
            var managersZGD = sline.RSV.Employees.GetAll()
              .Where(d => d.Status == sline.RSV.Employee.Status.Active)//активные сотрудники
              .Where(d => !d.IncludedIn(rolePZGD))//не является ПЗГД
              .Where(d => d.Department != null)//заполнено подразделение
              .Where(d => d.Department.ZGDanorsv == true)//в подразделении стоит галочка ЗГД
              .Where(d => d.Department.Manager != null)//в подразделении заполнен руководитель
              .Where(d => d.Department.Manager.Equals(d))//руководитель равен сотруднику
              .Where(d => d.Department.HeadOffice != null)//заполнено головное подразделение
              .Where(d => d.Department.HeadOffice.Equals(departmentGD))//головное подразделение равно ГД
              .Where(d => d.Department.Status == sline.RSV.Department.Status.Active)
              .ToList();//подразделение действубщее
            
            foreach (var manager in managersZGD)
              result.Add(manager);
          }
          
          return result;
        }
        #endregion
        
        #region Руководители прямого подчинения ГД
        if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.GDDirectSubord)
        {
          var initiator = sline.RSV.Employees.As(task.Author);
          if (initiator == null)
            initiator = sline.RSV.Employees.GetAll().FirstOrDefault(x => x.Id == task.DocumentGroup.OfficialDocuments.FirstOrDefault().Author.Id);
          
          if (initiator != null)
          {
            var departmentGD = sline.RSV.Departments.GetAll()
              .Where(d => d.Manager != null && d.Manager.JobTitle != null && d.Manager.JobTitle.Name == "Генеральный директор")
              .FirstOrDefault();
            
            var managers = sline.RSV.Employees.GetAll()
              .Where(d => d.Status == sline.RSV.Employee.Status.Active)//активные сотрудники
              .Where(d => d.Department != null)//заполнено подразделение
              .Where(d => d.Department.ZGDanorsv == false)//в подразделении не стоит галочка ЗГД
              .Where(d => d.Department.Manager != null)//в подразделении заполнен руководитель
              .Where(d => d.Department.Manager.Equals(d))//руководитель равен сотруднику
              .Where(d => d.Department.HeadOffice != null)//заполнено головное подразделение
              .Where(d => d.Department.HeadOffice.Equals(departmentGD))//головное подразделение равно ГД
              .Where(d => d.Department.Status == sline.RSV.Department.Status.Active)
              .ToList();//подразделение действубщее
            
            foreach (var manager in managers)
              result.Add(manager);
          }
          
          return result;
        }
        #endregion
        
        return result;
      }
      catch(Exception exc)
      {
        Logger.ErrorFormat(" >>> SOFTLINE >>> ERROR on GetRolePerformers >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
        return result;
      }
    }
    
    private sline.RSV.IEmployee GetManager(sline.RSV.IDepartment department)
    {
      try
      {
        var manager = department.Manager;
        if (manager != null)
        {
          var titleManager = manager.JobTitle;
          if (titleManager.Ignoresline == true)
            return null;
          else
            return manager;
        }
        else return null;
      }
      catch(Exception exc)
      {
        //Logger.ErrorFormat(" >>> SOFTLINE >>> Error on GetManager >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
        return null;
      }
    }
    
  }
}
