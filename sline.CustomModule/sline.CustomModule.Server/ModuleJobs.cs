﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace sline.CustomModule.Server
{
    public class ModuleJobs
    {

        /// <summary>
        /// 
        /// </summary>
        public virtual void NotifyToExecution()
        {
            Logger.Debug(" >>> SOFTLINE >>> Start NotifyToExecution");
            try
            {
                var timeDelay = sline.CustomModule.Settings.GetAll().FirstOrDefault().TimeToExecution;
                var timeToNotify = Calendar.UserToday.AddDays(Convert.ToDouble(timeDelay));
                
                var assigments = Sungero.RecordManagement.ActionItemExecutionAssignments.GetAll()
                    .Where(x => x.Deadline.Value.Date == timeToNotify.Date 
                           && x.Completed == null
                           && x.Status == Sungero.RecordManagement.ActionItemExecutionAssignment.Status.InProcess);
                    //.OrderBy(x => x.Performer)
                   // .ToDictionary(x => x.Performer);
                
                
                foreach (var assigment in assigments)
                {                    
                    var task = Sungero.Workflow.SimpleTasks.CreateWithNotices("Уведомление о приближающемся сроке по поручению", assigment.Performer);
                    task.ActiveText = "Уведомление о приближающемся сроке по поручению";
                    task.Attachments.Add(assigment);
                    task.Save();                    
                    task.Start();                    
                }
                Logger.Debug(" >>> SOFTLINE >>> Complete NotifyToExecution");
            }
            catch(Exception exc)
            {
                Logger.ErrorFormat(" >>> SOFTLINE >>> {0}{1}{2}", exc.Message, Environment.NewLine, exc.StackTrace);
            }
        }

    }
}