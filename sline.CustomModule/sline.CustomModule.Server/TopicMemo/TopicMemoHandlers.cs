﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.TopicMemo;

namespace sline.CustomModule
{
  partial class TopicMemoServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.PreAgreement = false;
      _obj.DITAgreement = false;
      _obj.NZIAgreement = false;
    }
  }

}