﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.PurchaseRequest;
using docRelation = anorsv.RelationModule;

namespace sline.CustomModule
{
  partial class PurchaseRequestAuthorPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> AuthorFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.AuthorFiltering(query, e);
      var users = Substitutions.ActiveSubstitutedUsers.ToList();
      if (!Users.Current.IncludedIn(Roles.Administrators) && users.Any())
      {
        users.Add(Users.Current);
        query = query.Where(x => users.Contains(Sungero.CoreEntities.Users.As(x)));
      }
      return query;
    }
  }

  partial class PurchaseRequestFilteringServerHandler<T>
  {

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      query = base.Filtering(query, e);
      
      if (_filter == null)
        return query;
      
      if ((_filter.Registered || _filter.Reserved || _filter.NotRegistered) &&
          !(_filter.Registered && _filter.Reserved && _filter.NotRegistered))
        query = query.Where(l => _filter.Registered && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Registered ||
                            _filter.Reserved && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Reserved ||
                            _filter.NotRegistered && l.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.NotRegistered);
      
      return query;
    }
  }

  partial class PurchaseRequestServerHandlers
  {

    public override void BeforeSaveHistory(Sungero.Content.DocumentHistoryEventArgs e)
    {
      base.BeforeSaveHistory(e);
      
      var isUpdateAction = e.Action == Sungero.CoreEntities.History.Action.Update;
      var isCreateAction = e.Action == Sungero.CoreEntities.History.Action.Create;

      // Изменять историю только при изменении и создании документа
      if (!isUpdateAction && !isCreateAction)
        return;
      
      // Свойства, для которых будем записывать расширенную историю
      var properties = _obj.State.Properties.Where(p =>
					p == _obj.State.Properties.EmployeeDZD);
      
      var propertiesType = sline.CustomModule.PurchaseRequests.Info.Properties.GetType();
      var objType = _obj.GetType();
      var operation = new Enumeration("ReqChange");
      
      // Обходим коллекцию свойств
      foreach (var property in properties)
      {
        var isChanged = (property as Sungero.Domain.Shared.IPropertyState).IsChanged; 
        if (isChanged)
        {
          // Извлечение локализованного наименования реквизита.
	        var propertyName = (property as Sungero.Domain.Shared.PropertyStateBase).PropertyName;
	        var propertyInfo = propertiesType.GetProperty(propertyName).GetValue(sline.CustomModule.PurchaseRequests.Info.Properties);
	        var name = propertyInfo.GetType().GetProperty("LocalizedName").GetValue(propertyInfo);
	  
          // Новое значение реквизита
          var newValue = objType.GetProperty(propertyName).GetValue(_obj); 
          // Старое значение реквизита
          var oldValue = property.GetType().GetProperty("OriginalValue").GetValue(property); 
          
          // Сверяем, что старое и новое значения не равны, отбрасываем изменения строковых свойств с null на пустую строку
  	      if (newValue == oldValue ||
  		        newValue != null && oldValue != null && Equals(newValue.ToString(), oldValue.ToString()) ||
  		        newValue != null && oldValue == null && string.IsNullOrEmpty(newValue.ToString()) ||
  		        oldValue != null && newValue == null && string.IsNullOrEmpty(oldValue.ToString()))
            continue;//return;
          
          var comment = string.Format("{0}. Новое значение: {1}. Прежнее значение: {2}", name, newValue, oldValue);
          e.Write(operation, null, comment);    
          
        }
      }
    }

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      base.Saving(e);
      
      if (!_obj.ProjectCollection.Any(p => p.ProjectProperty.Equals(_obj.Project)))
        _obj.ProjectCollection.AddNew().ProjectProperty = _obj.Project;
      
      foreach (var rowSource in _obj.ProjectCollection)
        if (!_obj.ProjectsCollectionanorsv.Any(p => p.Project.Equals(rowSource.ProjectProperty)))
          _obj.ProjectsCollectionanorsv.AddNew().Project = rowSource.ProjectProperty;
      
      // Проверить возможность изменения связей
      bool canModifyRelations = docRelation.PublicFunctions.Module.Remote.CanChangeDocumentRelations(_obj);
      e.Params.AddOrUpdate("AnoRsvCanModifyRelations", canModifyRelations);
    }

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      var name = string.Empty;
      
      if (_obj.PurchaseMethod != null)
        name += _obj.PurchaseMethod;
      
      if (!string.IsNullOrWhiteSpace(_obj.RegistrationNumber))
        name += " №" + _obj.RegistrationNumber;
      
      if (_obj.RegistrationDate != null)
        name +=  " от " + _obj.RegistrationDate.Value.ToString("d");
      
      if (_obj.Project != null)
      {
        name +=  " по " + _obj.Project;
      }
      
      _obj.Name = name;
      
      base.BeforeSave(e);
    }
  }

  partial class PurchaseRequestJustificationPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> JustificationFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(x => x.Status != sline.CustomModule.IdentityPP.Status.Closed);
      return query;
    }
  }

  partial class PurchaseRequestPurchaseMethodPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PurchaseMethodFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(x => x.Status != sline.CustomModule.IdentityPP.Status.Closed);
      return query;
    }
  }

  partial class PurchaseRequestSourceFinancingPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> SourceFinancingFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(x => x.Status != sline.CustomModule.IdentityPP.Status.Closed);
      return query;
    }
  }

  partial class PurchaseRequestIdentityPPPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> IdentityPPFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(x => x.Status != sline.CustomModule.IdentityPP.Status.Closed);
      return query;
    }
  }

}