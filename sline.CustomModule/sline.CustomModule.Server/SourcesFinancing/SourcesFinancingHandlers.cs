﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.SourcesFinancing;

namespace sline.CustomModule
{
    partial class SourcesFinancingServerHandlers
    {

        public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_obj.Code) || !string.IsNullOrWhiteSpace(_obj.Subject))
                _obj.Name = _obj.Code + "-" + _obj.Subject;
        }
    }

}