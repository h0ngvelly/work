using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Sungero.Core;
using Sungero.CoreEntities;

namespace sline.CustomModule.Server
{
  public class ModuleFunctions
  {
    
    [Public]
    public bool GetApprovalTasks(Sungero.Docflow.IOfficialDocument document)
    {
      var approvalTaskDocumentGroupGuid = Sungero.Docflow.PublicConstants.Module.TaskMainGroup.ApprovalTask;
      var appTasks = false;
      AccessRights.AllowRead(
        () =>
        {
          appTasks = Sungero.Docflow.ApprovalTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(t => t.AttachmentDetails
                   .Any(att => att.AttachmentId == document.Id &&
                        att.GroupId == approvalTaskDocumentGroupGuid))
            .Any();
        });
      return appTasks;
    }
    
    [Public]
    public bool GetFreeTasks(Sungero.Docflow.IOfficialDocument document)
    {
      var freeTasks = false;
      
      AccessRights.AllowRead(
        () =>
        {
          freeTasks = Sungero.Docflow.FreeApprovalTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(t => t.AttachmentDetails
                   .Any(att => att.AttachmentId == document.Id  &&
                        att.GroupId.ToString() == "cd77936e-884e-44bb-b869-9a60f9f5f2b4"))
            .Any();
        });
      return freeTasks;
    }
    
    [Public]
    public bool GetReviewTasks(Sungero.Docflow.IOfficialDocument document)
    {
      var reviewTaskDocumentGroupGuid = Sungero.Docflow.PublicConstants.Module.TaskMainGroup.DocumentReviewTask;
      
      var reviewTasks = false;
      AccessRights.AllowRead(
        () =>
        {
          reviewTasks = Sungero.RecordManagement.DocumentReviewTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(t => t.AttachmentDetails
                   .Any(att => att.AttachmentId == document.Id &&
                        att.GroupId == reviewTaskDocumentGroupGuid))
            .Any();
        });
      return reviewTasks;
    }
    
    [Public]
    public bool GetActionTasks(Sungero.Docflow.IOfficialDocument document)
    {
      var actionTaskDocumentGroupGuid = Sungero.Docflow.PublicConstants.Module.TaskMainGroup.ActionItemExecutionTask;
      
      var actionTasks = false;
      AccessRights.AllowRead(
        () =>
        {
          actionTasks = Sungero.RecordManagement.ActionItemExecutionTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(t => t.AttachmentDetails
                   .Any(att => att.AttachmentId == document.Id &&
                        att.GroupId == actionTaskDocumentGroupGuid))
            .Any();
        });
      return actionTasks;
    }
    
    [Public]
    public bool GetAcqTasks(Sungero.Docflow.IOfficialDocument document)
    {
      var acqTasks = false;
      AccessRights.AllowRead(
        () =>
        {
          acqTasks = Sungero.RecordManagement.AcquaintanceTasks.GetAll()
            .Where(t => t.Status == Sungero.Workflow.Task.Status.InProcess ||
                   t.Status == Sungero.Workflow.Task.Status.Suspended)
            .Where(t => t.AttachmentDetails
                   .Any(att => att.AttachmentId == document.Id &&
                        att.GroupId.ToString() == "19c1e8c9-e896-4d93-a1e8-4e22b932c1ce"))
            .Any();
        });
      return acqTasks;
    }

    #region Закупочный процесс
    
    #region Интеграция с 1с УХ
    
    [Public(WebApiRequestType = RequestType.Post)]
    public static List<Structures.Module.IApprovalStatus> GetStatuses(List<int> documentIds)
    {
      var result = new List<Structures.Module.IApprovalStatus>();
      
      foreach(int documentId in documentIds)
      {

        if (documentId < 1)
        {
          result.Add(
            Structures.Module.ApprovalStatus.Create(documentId, -1, "", Resources.ErrorVildateIdMustGreaterThen0, false)
           );
        }
        else
        {
          var document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentId).FirstOrDefault();
          
          if (document == null)
          {
            result.Add(
              Structures.Module.ApprovalStatus
              .Create(documentId, -1, "", Resources.ErrorDocumentByIdNotFoundFormat(documentId), false)
             );
          }
          else
          {
            var foundedTaskId = Sungero.Docflow.ApprovalTasks
              .GetAll(t => t.Status != Sungero.Workflow.Task.Status.Draft)
              .Where(
                t => t.AttachmentDetails
                .Any(
                  att => att.AttachmentId == document.Id
                  && att.GroupId == Sungero.Docflow.PublicConstants.Module.TaskMainGroup.ApprovalTask))
              .OrderByDescending(t => t.Started)
              .Select(t => t.Id)
              .FirstOrDefault();
            
            int taskId = foundedTaskId == 0 ? -1 : (int) foundedTaskId;
            string status = document.InternalApprovalState.HasValue ? document.InternalApprovalState.Value.ToString() : "";
            
            result.Add(
              Structures.Module.ApprovalStatus.Create(documentId, taskId, status, "", true)
             );
          }
        }
        
      }
      
      return result;
    }
    
    /// <summary>
    /// Отправка документа на согласование по регламенту
    /// </summary>
    /// <param name="documentId"></param>
    /// <returns></returns>
    [Public(WebApiRequestType = RequestType.Post)]
    public static int SendDocumentOnApprovalTask(int documentId)
    {
      int taskId = -1;
      var document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentId).FirstOrDefault();
      
      if (document != null)
      {
        int foundedTaskId = Sungero.Docflow.ApprovalTasks
          .GetAll(t => t.Status == Sungero.Workflow.Task.Status.InProcess)
          .Where(
            t => t.AttachmentDetails
            .Any(
              att => att.AttachmentId == document.Id
              && att.GroupId == Sungero.Docflow.PublicConstants.Module.TaskMainGroup.ApprovalTask))
          .OrderByDescending(t => t.Started)
          .Select(t => t.Id)
          .FirstOrDefault();
        
        if (foundedTaskId > 0)
        {
          taskId = foundedTaskId;
        }
        else
        {
          var planNeedsLine = sline.CustomModule.PlanNeedsLines.As(document);
          var purchaseRequest = sline.CustomModule.PurchaseRequests.As(document);
          var taskAuthor = Sungero.Company.Employees.Null;
          
          if (planNeedsLine != null || purchaseRequest != null)
          {
            if (planNeedsLine != null)
            {
              taskAuthor = planNeedsLine.ResponsibleEmployee;
            }
            else
            {
              taskAuthor = purchaseRequest.ResponsibleEmployee;
            }
            
            var newApprovalTask = anorsv.ApprovalTask.ApprovalTasks.Create();
            newApprovalTask.DocumentGroup.OfficialDocuments.Add(document);
            newApprovalTask.ApprovalRule = Sungero.Docflow.ApprovalRules
              .GetAll(ar => ar.DocumentKinds.Any(dk => dk.DocumentKind.Equals(document.DocumentKind)))
              .Where(ar => ar.Status == Sungero.Docflow.ApprovalRule.Status.Active)
              .OrderByDescending(ar => ar.IsDefaultRule)
              .ThenByDescending(ar => ar.Priority)
              .FirstOrDefault();
            newApprovalTask.ActiveText = Resources.ApprovalTaskStartingActiveText;
            newApprovalTask.Created = Calendar.Now;
            newApprovalTask.Author = taskAuthor;
            newApprovalTask.Subject = Resources.ApprovalTaskSubjectFormat(document.Name);
            newApprovalTask.Importance = Sungero.Workflow.Task.Importance.Normal;
            newApprovalTask.Save();
            
            // Для cнижения вероятности наличия блокировки на недавно созданном документе, сделаем несколько поыток получить блокировку
            var documentIsLocked = Locks.TryLock(document);
            var maxTryCount = 10;
            var tryCount = 0;
            while (!documentIsLocked && tryCount < maxTryCount)
            {
              Thread.Sleep(100*(tryCount + 1));
              tryCount++;
            }

            newApprovalTask.Start();

            if (documentIsLocked)
            {
              Locks.Unlock(document);
            }

            taskId = newApprovalTask.Id;
          }
        }
      }
      
      return taskId;
    }
    
    #endregion
    
    #endregion

  }
}