﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.MinutesPurchase;

namespace sline.CustomModule
{
    partial class MinutesPurchasePropertyCollectionSharedCollectionHandlers
    {

        public virtual void PropertyCollectionAdded(Sungero.Domain.Shared.CollectionPropertyAddedEventArgs e)
        {
            _added.FrameContract = false;
        }
    }

    partial class MinutesPurchaseSharedHandlers
    {

    }
}