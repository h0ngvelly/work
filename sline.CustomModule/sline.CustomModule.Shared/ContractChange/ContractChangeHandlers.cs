﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.ContractChange;

namespace sline.CustomModule
{
    partial class ContractChangeSharedHandlers
    {

        public virtual void LeadingContractChanged(sline.CustomModule.Shared.ContractChangeLeadingContractChangedEventArgs e)
        {
            var doc = _obj.LeadingContract;
            if (doc != null)
            {
                _obj.Counterparty = doc.Counterparty;
                _obj.CounterpartySignatory = doc.CounterpartySignatory;
                _obj.Contact = doc.Contact;
                
                _obj.BusinessUnit = doc.BusinessUnit;
                _obj.Department = doc.Department;
                _obj.OurSignatory = doc.OurSignatory;
                _obj.ResponsibleEmployee = doc.ResponsibleEmployee;
            }
        }

    }
}