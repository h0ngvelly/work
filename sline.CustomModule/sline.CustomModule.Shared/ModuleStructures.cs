﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace sline.CustomModule.Structures.Module
{

  /// <summary>
  /// Структура описывающая статус согласования
  /// </summary>
  [Public]
  partial class ApprovalStatus
  {
    public int DocId { get; set; }
    
    public int TaskId { get; set; }
    
    public string Status { get; set; }
    
    public string Comment { get; set; }
    
    public bool Successfull { get; set; }
  }

  /// <summary>
  /// 
  /// </summary>
  partial class ExceptionsStruct
  {
    public string ErrorType { get; set; }
    public string Message{ get; set; }
  }
  
  
}