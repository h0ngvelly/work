﻿using System;
using Sungero.Core;

namespace sline.CustomModule.Constants
{
  public static class Module
  {

    public const string Error = "Error";
    public const string Warn = "Warn";
    public const string Debug = "Debug";

    public const string BusinessUnits = "НашиОрганизации";
    public const string Departments = "Подразделения";
    public const string Employees = "Пользователи";
    
    public static class DocKind
    {
      /// <summary>
      /// Договор расходный
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid ExpendableContractKind = Guid.Parse("E0D61AB9-ECBD-42EE-BF44-0DF4B08863F8");
      /// <summary>
      /// Служебные записки по закупочной деятельности
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid ProcurementActivitiesKind = Guid.Parse("CAEB4E82-4C1A-4DE3-A53B-A3314B878194");
      /// <summary>
      /// Закупка у единственного поставщика
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid SinglePurchaseKind = Guid.Parse("E3BBE8FE-7793-4E37-8FC6-2CDFF316E328");
      /// <summary>
      /// Конкурентная закупка
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid CompetitivePurchaseKind = Guid.Parse("0687C982-EE6D-49D5-AE58-0BAE9891B4D7");
      /// <summary>
      /// Служебные записки в ДИТ
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid memoDITKind = Guid.Parse("68AB0E90-137A-4030-8D38-3D710EC070CA");
      /// Закупка по счету
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid PurchaseByInvoiceKind = Guid.Parse("5BCDE84E-9998-42E1-922D-C9888D4CBC89");
    }

    [Public]
    public static class Roles
    {
      [Public]
      public static class DateChangingRole
      {
        [Public]
        public const string RoleName = "АНОРСВ. Делопроизводители с правом изменения ожидаемой даты регистрации";
        
        [Public]
        public const string RoleDescription = "Делопроизводители, имеющие право на изменение Ожидаемой даты регистрации";
        
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("EA712C69-F072-4CBE-9893-7B548AE55D0B");
      }
    }

  }
}
