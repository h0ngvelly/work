using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.PurchaseRequest;

namespace sline.CustomModule
{
  partial class PurchaseRequestProjectCollectionSharedCollectionHandlers
  {

    public virtual void ProjectCollectionDeleted(Sungero.Domain.Shared.CollectionPropertyDeletedEventArgs e)
    {
      if (_deleted.ProjectProperty != null && !_obj.ProjectCollection.Any(p => p.ProjectProperty.Equals(_deleted.ProjectProperty)))
        foreach(var rowProject in _obj.ProjectsCollectionanorsv.Where(p => p.Project.Equals(_deleted.ProjectProperty)))
          _obj.ProjectsCollectionanorsv.Remove(rowProject);
    }
  }


    partial class PurchaseRequestSharedHandlers
    {

        public override void RegistrationDateChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
        {
            base.RegistrationDateChanged(e);
            
            var name = string.Empty;
            
            if (_obj.PurchaseMethod != null)
                name += _obj.PurchaseMethod;
            
            if (!string.IsNullOrWhiteSpace(_obj.RegistrationNumber))
                name += " №" + _obj.RegistrationNumber;
            
            if (_obj.RegistrationDate != null)
                name +=  " от " + _obj.RegistrationDate.Value.ToString("d");
            
            if (_obj.Project != null)
            {
                name +=  " по " + _obj.Project;
            }
            
            _obj.Name = name;
        }

        public override void RegistrationNumberChanged(Sungero.Domain.Shared.StringPropertyChangedEventArgs e)
        {
            base.RegistrationNumberChanged(e);
            
            var name = string.Empty;
            
            if (_obj.PurchaseMethod != null)
                name += _obj.PurchaseMethod;
            
            if (!string.IsNullOrWhiteSpace(_obj.RegistrationNumber))
                name += " №" + _obj.RegistrationNumber;
            
            if (_obj.RegistrationDate != null)
                name +=  " от " + _obj.RegistrationDate.Value.ToString("d");
            
            if (_obj.Project != null)
            {
                name +=  " по " + _obj.Project;
            }
            
            _obj.Name = name;
        }

        public virtual void PurchaseMethodChanged(sline.CustomModule.Shared.PurchaseRequestPurchaseMethodChangedEventArgs e)
        {
            if (_obj.PurchaseMethod != null)
                _obj.DocumentKind = _obj.PurchaseMethod.DocumentKind;
            else
                _obj.DocumentKind = null;
            
            var name = string.Empty;
            
            if (_obj.PurchaseMethod != null)
                name += _obj.PurchaseMethod;
            
            if (!string.IsNullOrWhiteSpace(_obj.RegistrationNumber))
                name += " №" + _obj.RegistrationNumber;
            
            if (_obj.RegistrationDate != null)
                name +=  " от " + _obj.RegistrationDate.Value.ToString("d");
            
            if (_obj.Project != null)
            {
                name +=  " по " + _obj.Project;
            }
            
            _obj.Name = name;
        }

    }
}