﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.PurchaseRequest;

namespace sline.CustomModule.Shared
{
    partial class PurchaseRequestFunctions
    {

        public override void ChangeRegistrationPaneVisibility(bool needShow, bool repeatRegister)
        {
            base.ChangeRegistrationPaneVisibility(needShow, repeatRegister);
            
            _obj.State.Properties.Created.IsVisible = needShow;
        }
        
        public override void FillName()
        {
            var name = string.Empty;
            
            if (_obj.PurchaseMethod != null)
                name += _obj.PurchaseMethod;
            
            if (!string.IsNullOrWhiteSpace(_obj.RegistrationNumber))
                name += " №" + _obj.RegistrationNumber;
            
            if (_obj.RegistrationDate != null)
                name +=  " от " + _obj.RegistrationDate.Value.ToString("d");
            
            if (_obj.Project != null)
            {
                name +=  " по " + _obj.Project;
            }
            
            if (_obj.IdentityPP != null)
                name += " " + _obj.IdentityPP.Name;
            
            _obj.Name = name;
        }
    }
}