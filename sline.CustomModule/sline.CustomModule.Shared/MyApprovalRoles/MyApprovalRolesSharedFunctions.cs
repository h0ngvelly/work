﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.MyApprovalRoles;

namespace sline.CustomModule.Shared
{
    partial class MyApprovalRolesFunctions
    {
        public override List<Sungero.Docflow.IDocumentKind> Filter(List<Sungero.Docflow.IDocumentKind> kinds)
        {
            var query = base.Filter(kinds);

            /*if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagers)
                query = query.Where(k => k.DocumentType.DocumentTypeGuid == "58cca102-1e97-4f07-b6ac-fd866a8b7cb1").ToList();*/
            
            if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.PurchaseDZD ||
                _obj.Type == sline.CustomModule.MyApprovalRoles.Type.PurchRespons || 
                _obj.Type == sline.CustomModule.MyApprovalRoles.Type.ResponsibleFRC)
                query = query.Where(k => k.DocumentType.DocumentTypeGuid == "247bdd6c-d233-4253-9adf-fc04f905283a").ToList();
            
            if (_obj.Type == sline.CustomModule.MyApprovalRoles.Type.ApprORD)
                query = query.Where(k => k.DocumentType.DocumentTypeGuid == "846a6a7b-e177-423f-9adb-c991d9b96662" ||
                                    k.DocumentType.DocumentTypeGuid == "21c749e2-a57f-4d91-bfde-5131ee8b5a42" ||
                                    k.DocumentType.DocumentTypeGuid == "9570e517-7ab7-4f23-a959-3652715efad3" ||
                                    k.DocumentType.DocumentTypeGuid == "264ada4e-b272-4ecc-a115-1246c9556bfa").ToList();

            return query;
        }
    }
}