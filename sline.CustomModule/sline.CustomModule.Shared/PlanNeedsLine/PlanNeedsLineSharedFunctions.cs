﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.PlanNeedsLine;

namespace sline.CustomModule.Shared
{
    partial class PlanNeedsLineFunctions
    {
        public override void ChangeRegistrationPaneVisibility(bool needShow, bool repeatRegister)
        {
            base.ChangeRegistrationPaneVisibility(needShow, repeatRegister);
            
            _obj.State.Properties.Created.IsVisible = needShow;
        }
    }
}