﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.MinutesPurchase;

namespace sline.CustomModule
{
  partial class MinutesPurchaseClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      bool res1 = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = res1;

      _obj.State.Properties.ProjectsCollectionanorsv.IsEnabled = false;
      _obj.State.Properties.Project.IsVisible = true;
    }

  }
}