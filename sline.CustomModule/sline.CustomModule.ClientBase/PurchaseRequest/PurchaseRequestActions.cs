﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.PurchaseRequest;
using docRelation = anorsv.RelationModule;

namespace sline.CustomModule.Client
{
  partial class PurchaseRequestVersionsActions
  {
    public override void ImportVersion(Sungero.Domain.Client.ExecuteChildCollectionActionArgs e)
    {
      base.ImportVersion(e);
    }

    public override bool CanImportVersion(Sungero.Domain.Client.CanExecuteChildCollectionActionArgs e)
    {
      return false;
      //return base.CanImportVersion(e);
    }

  }

    partial class PurchaseRequestActions
    {
    public virtual void ShowSuppStopList(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      sline.CustomModule.PublicFunctions.Module.ShowStopSuppList();
    }

    public virtual bool CanShowSuppStopList(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public virtual void ShowPotentialSuppList(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      sline.CustomModule.PublicFunctions.Module.ShowPotentialSuppList();
    }

    public virtual bool CanShowPotentialSuppList(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public override void ImportInLastVersion(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ImportInLastVersion(e);
    }

    public override bool CanImportInLastVersion(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return false;
      //return base.CanImportInLastVersion(e);
    }

    public override void ImportInNewVersion(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ImportInNewVersion(e);
    }

    public override bool CanImportInNewVersion(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return false;
      //return base.CanImportInNewVersion(e);
    }

        public virtual void DeleteRelation(Sungero.Domain.Client.ExecuteActionArgs e)
        {
          docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(_obj);
        }

        public virtual bool CanDeleteRelation(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
          bool canModifyRelation = false;
          if (e.Params.Contains("AnoRsvCanModifyRelations"))
            if (e.Params.TryGetValue("AnoRsvCanModifyRelations", out canModifyRelation))
              return canModifyRelation;  
                
          return canModifyRelation;
        }

        public virtual void ChangeRelation(Sungero.Domain.Client.ExecuteActionArgs e)
        {
          docRelation.PublicFunctions.Module.ChangeDocumentLinkDialog(_obj);
        }
    
        public virtual bool CanChangeRelation(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
          bool canModifyRelation = false;
          if (e.Params.Contains("AnoRsvCanModifyRelations"))
            if (e.Params.TryGetValue("AnoRsvCanModifyRelations", out canModifyRelation))
              return canModifyRelation;  
                
          return canModifyRelation;
        }

        public override void ShowRegistrationPane(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            base.ShowRegistrationPane(e);
            
            _obj.State.Properties.Created.IsVisible = !_obj.State.Properties.Created.IsVisible;
        }

        public override bool CanShowRegistrationPane(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanShowRegistrationPane(e);
        }

        public override void ApprovalForm(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            base.ApprovalForm(e);
        }

        public override bool CanApprovalForm(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanApprovalForm(e);
        }

        public override void SendForReview(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            var createdTasks = sline.CustomModule.PublicFunctions.Module.GetReviewTasks(_obj);
            
            if (createdTasks)
            {
                e.AddError("Документ уже отправлен на рассмотрение!");
                return;
            }
            else
            {
                base.SendForReview(e);
            }
        }

        public override bool CanSendForReview(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanSendForReview(e);
        }

        public override void SendActionItem(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            var createdTasks = sline.CustomModule.PublicFunctions.Module.GetActionTasks(_obj);
            
            if (createdTasks)
            {
                e.AddError("Документ уже отправлен на исполнение!");
                return;
            }
            else
            {
                base.SendActionItem(e);
            }
        }

        public override bool CanSendActionItem(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanSendActionItem(e);
        }

        public override void SendForAcquaintance(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            var createdTasks = sline.CustomModule.PublicFunctions.Module.GetAcqTasks(_obj);
            
            if (createdTasks)
            {
                e.AddError("Документ уже отправлен на ознакомление!");
                return;
            }
            else
            {
                base.SendForAcquaintance(e);
            }
        }

        public override bool CanSendForAcquaintance(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanSendForAcquaintance(e);
        }

        public override void SendForFreeApproval(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            var createdTasks = sline.CustomModule.PublicFunctions.Module.GetFreeTasks(_obj);
            
            if (createdTasks)
            {
                e.AddError("Документ уже отправлен на свободное согласование!");
                return;
            }
            else
            {
                base.SendForFreeApproval(e);
            }
        }

        public override bool CanSendForFreeApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanSendForFreeApproval(e);
        }

        public override void SendForApproval(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            var createdTasks = sline.CustomModule.PublicFunctions.Module.GetApprovalTasks(_obj);
            
            if (createdTasks)
            {
                e.AddError("Документ уже отправлен на согласование по регламенту!");
                return;
            }
            else
            {
                base.SendForApproval(e);
            }
        }

        public override bool CanSendForApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanSendForApproval(e);
        }

        public override void ConvertToPdf(Sungero.Domain.Client.ExecuteActionArgs e)
        {
            base.ConvertToPdf(e);
        }

        public override bool CanConvertToPdf(Sungero.Domain.Client.CanExecuteActionArgs e)
        {
            return base.CanConvertToPdf(e);
        }
    }
}