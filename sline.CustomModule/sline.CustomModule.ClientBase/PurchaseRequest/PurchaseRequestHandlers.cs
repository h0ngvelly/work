using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.PurchaseRequest;
using docRelation = anorsv.RelationModule;

namespace sline.CustomModule
{
  partial class PurchaseRequestClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);

      bool res = _obj.State.IsInserted;
      _obj.State.Properties.DocumentKind.IsEnabled = res;
      _obj.State.Properties.ResponsibleEmployee.IsEnabled = res;
      _obj.State.Properties.Department.IsEnabled = res;
      _obj.State.Properties.Curator.IsEnabled = res;
      _obj.State.Properties.PreparedBy.IsEnabled = res;
      _obj.State.Properties.Sum.IsEnabled = res;
      _obj.State.Properties.SumIF.IsEnabled = res;
      _obj.State.Properties.Project.IsEnabled = res;
      _obj.State.Properties.Subject.IsEnabled = res;
      _obj.State.Properties.Result.IsEnabled = res;
      _obj.State.Properties.EmployeeDZD.IsEnabled = res;  
      _obj.State.Properties.SourcesFinancing.IsEnabled = res;
      _obj.State.Properties.PurchaseMethod.IsEnabled = res;
      _obj.State.Properties.Justification.IsEnabled = res;
      _obj.State.Properties.IdentityPP.IsEnabled = res;
      _obj.State.Properties.NMCD.IsEnabled = res;
      _obj.State.Properties.Frc.IsEnabled = res;
      
      bool res1 = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = res1;

      _obj.State.Properties.ProjectsCollectionanorsv.IsEnabled = false;
      _obj.State.Properties.Project.IsVisible = true;
      
      // Проверить возможность изменения связей
      bool canModifyRelations = docRelation.PublicFunctions.Module.Remote.CanChangeDocumentRelations(_obj);
      e.Params.AddOrUpdate("AnoRsvCanModifyRelations", canModifyRelations);
      
      var singlePurchaseKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.SinglePurchaseKind);
      var purchaseByInvoiceKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.PurchaseByInvoiceKind);
      if (_obj.DocumentKind == null || (!_obj.DocumentKind.Equals(singlePurchaseKind) && !_obj.DocumentKind.Equals(purchaseByInvoiceKind)))
      {
        e.AddInformation(sline.CustomModule.Resources.SuppliersListInfo, _obj.Info.Actions.ShowPotentialSuppList);
        e.AddInformation(sline.CustomModule.Resources.SuppliersStopListInfo, _obj.Info.Actions.ShowSuppStopList);
      }
    }

    public virtual void NMCDValueInput(Sungero.Presentation.DoubleValueInputEventArgs e)
    {
      if (e.NewValue.Value > _obj.Sum)
      {
        e.NewValue = e.OldValue;
        e.AddWarning(PurchaseRequests.Resources.NmcdWarningExceededAmountPlan);
      }
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);

      bool res = _obj.State.IsInserted;
      _obj.State.Properties.DocumentKind.IsEnabled = res;
      _obj.State.Properties.ResponsibleEmployee.IsEnabled = res;
      _obj.State.Properties.Department.IsEnabled = res;
      _obj.State.Properties.Curator.IsEnabled = res;
      _obj.State.Properties.PreparedBy.IsEnabled = res;
      _obj.State.Properties.Sum.IsEnabled = res;
      _obj.State.Properties.SumIF.IsEnabled = res;
      _obj.State.Properties.Project.IsEnabled = res;
      _obj.State.Properties.Subject.IsEnabled = res;
      _obj.State.Properties.Result.IsEnabled = res;
      _obj.State.Properties.EmployeeDZD.IsEnabled = res; 
      _obj.State.Properties.SourcesFinancing.IsEnabled = res;
      _obj.State.Properties.PurchaseMethod.IsEnabled = res;
      _obj.State.Properties.Justification.IsEnabled = res;
      _obj.State.Properties.IdentityPP.IsEnabled = res;
      
      bool res1 = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = res1;

      _obj.State.Properties.ProjectsCollectionanorsv.IsEnabled = false;
      _obj.State.Properties.Project.IsVisible = true;
      
    }
  }
}