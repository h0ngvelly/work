﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace sline.CustomModule.Client
{
  public class ModuleFunctions
  {

    /// <summary>
    /// Открыть на чтение последнюю версию Реестра потенциальных поставщиков (задан в настройках модуля Управление закупками)
    /// </summary>
    [Public]
    public virtual void ShowPotentialSuppList()
    {
      if (sline.CustomModule.Settings.GetAll().Any())
      {
        var documentID = sline.CustomModule.Settings.GetAll().FirstOrDefault().SupplierListID;
        if (documentID.HasValue)
        {
          var document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentID).FirstOrDefault();
          if (document != null)
          {
            var version = document.Versions.LastOrDefault();
            version.Open();
          }
        }
      }
      
    }
    
    /// <summary>
    /// Открыть на чтение последнюю версию Стоп-листа (задан в настройках модуля Управление закупками)
    /// </summary>
    [Public]
    public virtual void ShowStopSuppList()
    {
      if (sline.CustomModule.Settings.GetAll().Any())
      {
        var documentID = sline.CustomModule.Settings.GetAll().FirstOrDefault().SuppStopListID;
        if (documentID.HasValue)
        {
          var document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentID).FirstOrDefault();
          if (document != null)
          {
            var version = document.Versions.LastOrDefault();
            version.Open();
          }
        }
      }
      
    }
    
    public virtual void ShowSetting()
    {
      sline.CustomModule.Settings.GetAll().FirstOrDefault().ShowModal();
    }
  }
}