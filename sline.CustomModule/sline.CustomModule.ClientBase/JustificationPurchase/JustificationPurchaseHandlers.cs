﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.JustificationPurchase;

namespace sline.CustomModule
{
    partial class JustificationPurchaseClientHandlers
    {

        public virtual void CodeValueInput(Sungero.Presentation.StringValueInputEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_obj.Code) || !string.IsNullOrWhiteSpace(_obj.Subject))
                _obj.Name = _obj.Code + "-" + _obj.Subject;
        }

        public override void Closing(Sungero.Presentation.FormClosingEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_obj.Code) || !string.IsNullOrWhiteSpace(_obj.Subject))
                _obj.Name = _obj.Code + "-" + _obj.Subject;
        }

        public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_obj.Code) || !string.IsNullOrWhiteSpace(_obj.Subject))
                _obj.Name = _obj.Code + "-" + _obj.Subject;
        }

        public virtual void SubjectValueInput(Sungero.Presentation.StringValueInputEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_obj.Code) || !string.IsNullOrWhiteSpace(_obj.Subject))
                _obj.Name = _obj.Code + "-" + _obj.Subject;
        }

        public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_obj.Code) || !string.IsNullOrWhiteSpace(_obj.Subject))
                _obj.Name = _obj.Code + "-" + _obj.Subject;
        }

    }
}