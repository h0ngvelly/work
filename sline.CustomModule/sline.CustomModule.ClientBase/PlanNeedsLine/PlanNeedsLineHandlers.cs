﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.PlanNeedsLine;

namespace sline.CustomModule
{
  partial class PlanNeedsLineClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      var prop = _obj.State.Properties;
      foreach (var pr in prop)
      {
        pr.IsEnabled = _obj.State.IsInserted;
      }
      
      bool res1 = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = res1;

      _obj.State.Properties.ProjectsCollectionanorsv.IsEnabled = false;
      _obj.State.Properties.Project.IsVisible = true;
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      var prop = _obj.State.Properties;
      foreach (var pr in prop)
      {
        pr.IsEnabled = _obj.State.IsInserted;
      }
      
      bool res1 = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = res1;

      _obj.State.Properties.ProjectsCollectionanorsv.IsEnabled = false;
      _obj.State.Properties.Project.IsVisible = true;
    }

  }
}