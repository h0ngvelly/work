﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.CustomModule.TopicMemo;

namespace sline.CustomModule
{
  partial class TopicMemoClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      _obj.State.Properties.PreApprovers.IsEnabled = (_obj.PreAgreement.Equals(true)) ? true : false;
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      _obj.State.Properties.PreApprovers.IsEnabled = (_obj.PreAgreement.Equals(true)) ? true : false;
    }

  }
}