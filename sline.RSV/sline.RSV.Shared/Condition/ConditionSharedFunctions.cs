﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Condition;

namespace sline.RSV.Shared
{
  partial class ConditionFunctions
  {
    // вычисление условия
    public override Sungero.Docflow.Structures.ConditionBase.ConditionResult CheckCondition(Sungero.Docflow.IOfficialDocument document, Sungero.Docflow.IApprovalTask task)
    {
      //Условие Сравнения исполнителя роли и ЗГД
      if (_obj.ConditionType == ConditionType.ZGD)
      {
        //подразделения с признаком ЗГД
        var departments = sline.RSV.Departments.GetAll(d => d.ZGDanorsv == true);
        //список всех сотрудников, которые являются руководителями подразделений с отмеченным ЗГД
        var compareEmployees = Sungero.Company.Employees.GetAll(d => departments.Contains(d.Department)).Where(d => d == d.Department.Manager).ToList();
        //роль согласования
        var roleEmployees = new List<Sungero.Company.IEmployee>();
        
        var prole = anorsv.AnorsvMainSolution.ApprovalRoleBases.As(_obj.ApprovalRole);
        var arole = anorsv.AnorsvMainSolution.ApprovalRoles.As(_obj.ApprovalRole);
        var role = sline.CustomModule.MyApprovalRoleses.As(_obj.ApprovalRole);
        var roleRecipientsList = new List<Sungero.CoreEntities.IRecipient>();
        if (prole != null)
          roleRecipientsList.Add(anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRoleBase.GetRolePerformer(prole, task));
        if (role != null)
          roleRecipientsList.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, task));
        if (arole != null)
          roleRecipientsList.Add(anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRole.GetRolePerformer(arole, task));
        

        // Сотрудники, по которым проводим сравнение
        foreach (var recipient in roleRecipientsList)
        {
          var employee = Sungero.Company.Employees.As(recipient);
          var crole = Sungero.CoreEntities.Roles.As(recipient);

          if (employee != null && !roleEmployees.Contains(employee))
            roleEmployees.Add(employee);
          else if (crole != null)
          {
            var recepientList = crole.RecipientLinks.Select(r => r.Member).Where(m => Sungero.Company.Employees.Is(m)).ToList();
            foreach (var roleRecipient in recepientList)
            {
              var roleEmployee = Sungero.Company.Employees.As(roleRecipient);
              if (roleEmployee != null && !roleEmployees.Contains(roleEmployee))
                roleEmployees.Add(roleEmployee);
            }
          }
        }
        // Реципиенты, по которым проводим сравнение
        var compareRecipientsList = _obj.Recipientsanorsv.Select(r => r.Recipient).ToList();
        var approvalRolesList = _obj.ApprovalRolesanorsv.Select(r => r.Role).ToList();
        foreach (var appRole in approvalRolesList)
        {
          var appRoleAno = anorsv.AnorsvMainSolution.ApprovalRoles.As(appRole);
          var appRoleBase = sline.CustomModule.MyApprovalRoleses.As(appRole);
          if (appRoleBase != null)
            compareRecipientsList.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(appRoleBase, task));
          else
            compareRecipientsList.Add(anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRole.GetRolePerformer(appRoleAno, task));
        }
        // Сотрудники, c которыми проводим сравнение
        foreach (var recipient in compareRecipientsList)
        {
          var employee = Sungero.Company.Employees.As(recipient);
          var crole = Sungero.CoreEntities.Roles.As(recipient);

          if (employee != null && !compareEmployees.Contains(employee))
            compareEmployees.Add(employee);
          if (crole != null)
          {
            var recepientList = crole.RecipientLinks.Select(r => r.Member).Where(m => Sungero.Company.Employees.Is(m)).ToList();
            foreach (var roleRecipient in recepientList)
            {
              var roleEmployee = Sungero.Company.Employees.As(roleRecipient);
              if (roleEmployee != null && !compareEmployees.Contains(roleEmployee))
                compareEmployees.Add(roleEmployee);
            }
          }
        }
        if (compareEmployees.Any() && roleEmployees.Any())
        {
          var compareCount = compareEmployees.Count;
          var roleCount = roleEmployees.Count;
          
          //если исполнитель роли = 1 сотрудник
          if (roleCount == 1)
          {
            bool findRecipient = roleEmployees.Where(r => compareEmployees.Contains(r)).Any();
            if (findRecipient == true)
              return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(findRecipient, string.Empty);
            else
              return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
          }
          
          //если исполнитель роли = список сотрудников и он меньше или равен списку руководителей ЗГД
          if (roleCount <= compareCount)
          {
            foreach (var employee in roleEmployees)
            {
              if (!compareEmployees.Contains(employee))
                return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
            }
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
          }
          
          //если исполнитель роли больше списка руководителей ЗГД
          if (roleCount >= compareCount)
          {
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
          }
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вичислено. Отсутствует исполнитель роли для сравнения"); // Исполнители для сравнения не указаны
      }
      
      
      // подписание ЭП
      if (_obj.ConditionType == ConditionType.SingES)
      {
        // Предыдущая реализация sline
        /*var outgLetter = sline.RSV.OutgoingLetters.As(document);
        var order = sline.RSV.Orders.As(document);
        var directive = sline.RSV.CompanyDirectives.As(document);
        
        if (outgLetter != null)
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(outgLetter.SignESsline, string.Empty);
        if (order != null)
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(order.SignESsline, string.Empty);
        if (directive != null)
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(directive.SignESsline, string.Empty);
         
        return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Поддерживаются: Исходящие письма, Приказы, Распоряжения.");*/
        
        var officialDoc = anorsv.OfficialDocument.OfficialDocuments.As(document);
        if (officialDoc != null)
        {
          var signES = (officialDoc.SignESanorsv.Equals(anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES)) ? (true) : (false);
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(signES, string.Empty);
        }
        
        return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. У документа отсутствует Способ подписания.");
      }
      
      // обоснования закупки для закупки
      if (_obj.ConditionType == ConditionType.JustPurchase)
      {
        var purchase = sline.CustomModule.PurchaseRequests.As(document);
        if (purchase != null)
        {
          if (_obj.JustificationsPurchasesline.Any(x => x.JustificationPurchase == purchase.Justification))
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Допустимые типы: Заявка на закупку.");
      }
      
      // условие суммы план
      if (_obj.ConditionType == ConditionType.SumPlan)
      {
        var purchase = sline.CustomModule.PurchaseRequests.As(document);
        if (purchase != null)
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(this.CheckSum(purchase.Sum), string.Empty);
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Допустимые типы: Заявка на закупку, либо в документе не заполнена сумма (план).");
      }
      
      if (_obj.ConditionType == ConditionType.SumContract)
      {
        var planNeeds = sline.CustomModule.PlanNeedsLines.As(document);
        if (planNeeds != null)
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(this.CheckAmount(planNeeds.Sum), string.Empty);
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Допустимые типы: Строка плана потребностей, либо в документе не заполнена сумма договора.");
      }
      
      // условие предварительного согласования
      if (_obj.ConditionType == ConditionType.Preagreement)
      {
        /*var memoDoc = sline.RSV.Memos.As(document);
        if (memoDoc != null)
        {
          if (memoDoc.MemoTopicanorsv != null)
          {
            var memoTopic = sline.CustomModule.TopicMemos.As(memoDoc.MemoTopicanorsv);
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(memoTopic.PreAgreement, string.Empty);
          }
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Поддерживаются: Служебные записки.");*/
        
        // изменение условие для совместного учета Тематик СЗ и Тематик общих
        bool needPreagreement = false;
        var memoDoc = sline.RSV.Memos.As(document);
        var officialDoc = anorsv.OfficialDocument.OfficialDocuments.As(document);
        
        if (memoDoc != null)
          if (memoDoc.MemoTopicanorsv != null  && memoDoc.MemoTopicanorsv.PreAgreement == true)
            needPreagreement = true;
        
        if (officialDoc != null)
          if (officialDoc.Topicanorsv != null && officialDoc.Topicanorsv.PreAgreement == true)
            needPreagreement = true;

        if (memoDoc != null || officialDoc != null)
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(needPreagreement, string.Empty);
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. У документа отсутствует Тематика.");
      }
      
      // условие согласования с ДИТ
      if (_obj.ConditionType == ConditionType.DITagreement)
      {
        var memoDoc = sline.RSV.Memos.As(document);
        if (memoDoc != null)
        {
          if (memoDoc.MemoTopicanorsv != null)
          {
            var memoTopic = sline.CustomModule.TopicMemos.As(memoDoc.MemoTopicanorsv);
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(memoTopic.DITAgreement, string.Empty);
          }
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Поддерживаются: Служебные записки.");
      }
      
      // условие согласования с НЗИ
      if (_obj.ConditionType == ConditionType.NZIagreement)
      {
        var memoDoc = sline.RSV.Memos.As(document);
        if (memoDoc != null)
        {
          if (memoDoc.MemoTopicanorsv != null)
          {
            var memoTopic = sline.CustomModule.TopicMemos.As(memoDoc.MemoTopicanorsv);
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(memoTopic.NZIAgreement, string.Empty);
          }
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Поддерживаются: Служебные записки.");
      }
      
      // условие настройки замещения в 1С ДО
      if (_obj.ConditionType == ConditionType.Substitution1C)
      {
        var substRequestDoc = anorsv.AnorsvSubstitution.VacationSubstitutionRequests.As(document);
        if (substRequestDoc != null)
        {
          if (substRequestDoc.Substitution1CDO == true)
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Поддерживаются: Заявки на замещение.");
      }
      
      // условие настройки типового договора
      if (_obj.ConditionType == ConditionType.IsStandard)
      {
        // тип договор
        var contractdoc = anorsv.Contracts.Contracts.As(anorsv.ApprovalTask.ApprovalTasks.As(task).ContractGroupanorsv.Contracts.FirstOrDefault());
        if (contractdoc != null)
        {
          // вид "Расходные договоры"
          var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
          if(contractdoc.DocumentKind == expendableContractKind)
          {
            if (contractdoc.IsStandard == true)
              return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
            else
              return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
          }
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, sline.RSV.Conditions.Resources.ConditionsCannotBeMet);
      }
      
      // условие настройки типового ТЗ
      if (_obj.ConditionType == ConditionType.IsStandardTS)
      {
        // ТЗ
        var docTS = anorsv.ContractsModule.TechnicalSpecifications.As(anorsv.ApprovalTask.ApprovalTasks.As(task).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.FirstOrDefault());
        if (docTS != null)
        {
          if (docTS.IsStandard == true)
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, sline.RSV.Conditions.Resources.TS);
      }
      
      //Условия настройки типового договора и типового ТЗ
      if (_obj.ConditionType == ConditionType.IsStandardConTS)
      {
        // ТЗ
        var docTS = anorsv.ContractsModule.TechnicalSpecifications.As(anorsv.ApprovalTask.ApprovalTasks.As(task).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.FirstOrDefault());
        // тип договор
        var contractdoc = anorsv.Contracts.Contracts.As(anorsv.ApprovalTask.ApprovalTasks.As(task).ContractGroupanorsv.Contracts.FirstOrDefault());
        if (docTS != null && contractdoc != null)
        {
          // вид "Расходные договоры"
          var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
          if(contractdoc.DocumentKind == expendableContractKind)
          {
            if (docTS.IsStandard == true && contractdoc.IsStandard == true)
              return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
            else
              return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
          }
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, sline.RSV.Conditions.Resources.StandardContractAndTS);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, sline.RSV.Conditions.Resources.StandardContractAndTS);
      }
      
      // тематика договора?
      if (_obj.ConditionType == ConditionType.TopicContract)
      {
        
        var contractdoc = anorsv.Contracts.Contracts.As(anorsv.ApprovalTask.ApprovalTasks.As(task).ContractGroupanorsv.Contracts.FirstOrDefault());
        if (contractdoc != null)
        {
          // вид "Расходные договоры"
          var expendableContractKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ExpendableContractKind);
          if(contractdoc.DocumentKind == expendableContractKind)
          {
            if (_obj.Topicanorsv.Any( t =>t.Topic == contractdoc.GeneralTopicanorsv))
              return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
            else
              return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
          }
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, sline.RSV.Conditions.Resources.ConditionsCannotBeMet);
      }
      // тематика ТЗ?
      if (_obj.ConditionType == ConditionType.TopicTS)
      {
        // ТЗ
        var docTS = anorsv.ContractsModule.TechnicalSpecifications.As(anorsv.ApprovalTask.ApprovalTasks.As(task).TechnicalSpecificationGroupanorsv.TechnicalSpecifications.FirstOrDefault());
        if (docTS != null)
        {
          if (_obj.Topicanorsv.Any( t =>t.Topic == docTS.GeneralTopic))
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, sline.RSV.Conditions.Resources.TS);
      }
      
      // условие по списку подразделений
      if (_obj.ConditionType == ConditionType.DepCompareranorsv)
      {
        var arole = anorsv.AnorsvMainSolution.ApprovalRoles.As(_obj.ApprovalRole);
        var role = sline.CustomModule.MyApprovalRoleses.As(_obj.ApprovalRole);
        var recipients = new List<Sungero.CoreEntities.IRecipient>();
        
        if (role != null)
          recipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, task));
        else
          recipients.Add(anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRole.GetRolePerformer(arole, task));
        
        // Подразделения, которые требуется исключить из сравнения
        var excludeDepList = _obj.ExcludeDepartmentsanorsv.Select(d => d.Department).ToList();
        if (_obj.ExcludeSubdepartmentsanorsv == true)
        {
          // Собрать все нижестоящие подразделения-исключения
          var mainDepartmentExclList = Sungero.Company.Departments.GetAll(d => excludeDepList.Contains(d)).ToList();
          var subDepartmentExclList = Sungero.Company.Departments.GetAll(d => mainDepartmentExclList.Contains(d.HeadOffice)).ToList();
          while (subDepartmentExclList.Any())
          {
            foreach (var department in subDepartmentExclList)
            {
              if (!excludeDepList.Contains(department))
                excludeDepList.Add(department);
            }
            
            mainDepartmentExclList.Clear();
            mainDepartmentExclList = Sungero.Company.Departments.GetAll(d => subDepartmentExclList.Contains(d)).ToList();
            subDepartmentExclList.Clear();
            subDepartmentExclList = Sungero.Company.Departments.GetAll(d => mainDepartmentExclList.Contains(d.HeadOffice)).ToList();
          }
        }
        
        // Подразделения, по которым требуется провести сравнение
        var departmentList = _obj.Departmentsanorsv.Select(d => d.Department).ToList();
        if (_obj.Subdepartmentanorsv == true)
        {
          //Собрать все нижестоящие подразделения
          var mainDepartmentList = Sungero.Company.Departments.GetAll(d => departmentList.Contains(d)).ToList();
          var subDepartmentList = Sungero.Company.Departments.GetAll(d => mainDepartmentList.Contains(d.HeadOffice)).ToList();
          while (subDepartmentList.Any())
          {
            foreach (var department in subDepartmentList)
            {
              if (!departmentList.Contains(department) && !excludeDepList.Contains(department))
                departmentList.Add(department);
            }
            
            mainDepartmentList.Clear();
            mainDepartmentList = Sungero.Company.Departments.GetAll(d => subDepartmentList.Contains(d)).ToList();
            subDepartmentList.Clear();
            subDepartmentList = Sungero.Company.Departments.GetAll(d => mainDepartmentList.Contains(d.HeadOffice)).ToList();
          }
        }
        
        try
        {
          var findDepartment = false;
          foreach (var res in recipients)
          {
            if (res != null)
            {
              var emp = sline.RSV.Employees.As(res);
              if (emp != null)
              {
                var empDepartment = emp.Department;
                
                // Поиск подразделения в перечне настройки условия
                /*foreach (var depString in _obj.Departmentsanorsv)
                    findDepartment = (Equals(depString.Department, empDepartment)) ? true : findDepartment;*/
                
                // Поиск подразделения в построенном списке с учетом исключений
                findDepartment = (departmentList.Contains(empDepartment)) ? true : findDepartment;
              }
              
            }
          }
          
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(findDepartment, string.Empty);
        }
        catch(Exception exc)
        {
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
        }
      }
      
      // условие по списку тематик СЗ
      if (_obj.ConditionType == ConditionType.TopicMemoCompar)
      {
        var memoDoc = sline.RSV.Memos.As(document);
        if (memoDoc != null)
        {
          if (memoDoc.MemoTopicanorsv != null)
          {
            var findTopic = false;
            foreach (var topicString in _obj.TopicMemoanorsv)
              findTopic = (Equals(topicString.TopicMemo, memoDoc.MemoTopicanorsv)) ? true : findTopic;
            
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(findTopic, string.Empty);
          }
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty); // Тематика не указана
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, "Условие не может быть вычислено. Поддерживаются: Служебные записки.");
      }
      else if(_obj.ConditionType == ConditionType.TopicCompar)
      {
        var officialDocument = anorsv.OfficialDocument.OfficialDocuments.As(document);
        if (officialDocument != null)
        {
          if (officialDocument.Topicanorsv != null)
          {
            var findTopic = false;
            foreach (var topicString in _obj.Topicanorsv)
              findTopic = (Equals(topicString.Topic, officialDocument.Topicanorsv)) ? true : findTopic;
            
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(findTopic, string.Empty);
          }
          else
            return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty); // Тематика не указана
        }
      }
      
      // условие по списку исполнителей
      if (_obj.ConditionType == ConditionType.EmpInRoleInclud)
      {
        var roleEmployees = new List<Sungero.Company.IEmployee>();
        var compareEmployees = new List<Sungero.Company.IEmployee>();
        
        var arole = anorsv.AnorsvMainSolution.ApprovalRoles.As(_obj.ApprovalRole);
        var role = sline.CustomModule.MyApprovalRoleses.As(_obj.ApprovalRole);
        var roleRecipientsList = new List<Sungero.CoreEntities.IRecipient>();
        if (role != null)
          roleRecipientsList.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, task));
        else
          roleRecipientsList.Add(anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRole.GetRolePerformer(arole, task));
        
        // Сотрудники, по которым проводим сравнение
        foreach (var recipient in roleRecipientsList)
        {
          var employee = Sungero.Company.Employees.As(recipient);
          var crole = Sungero.CoreEntities.Roles.As(recipient);
          
          if (employee != null && !roleEmployees.Contains(employee))
            roleEmployees.Add(employee);
          else if (crole != null)
          {
            var recepientList = crole.RecipientLinks.Select(r => r.Member).Where(m => Sungero.Company.Employees.Is(m)).ToList();
            foreach (var roleRecipient in recepientList)
            {
              var roleEmployee = Sungero.Company.Employees.As(roleRecipient);
              if (roleEmployee != null && !roleEmployees.Contains(roleEmployee))
                roleEmployees.Add(roleEmployee);
            }
          }
        }
        
        // Реципиенты, по которым проводим сравнение
        var compareRecipientsList = _obj.Recipientsanorsv.Select(r => r.Recipient).ToList();
        var approvalRolesList = _obj.ApprovalRolesanorsv.Select(r => r.Role).ToList();
        foreach (var appRole in approvalRolesList)
        {
          var appRoleAno = anorsv.AnorsvMainSolution.ApprovalRoles.As(appRole);
          var appRoleBase = sline.CustomModule.MyApprovalRoleses.As(appRole);
          if (appRoleBase != null)
            compareRecipientsList.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(appRoleBase, task));
          else
            compareRecipientsList.Add(anorsv.AnorsvMainSolution.PublicFunctions.ApprovalRole.GetRolePerformer(appRoleAno, task));
        }
        
        // Сотрудники, c которыми проводим сравнение
        foreach (var recipient in compareRecipientsList)
        {
          var employee = Sungero.Company.Employees.As(recipient);
          var crole = Sungero.CoreEntities.Roles.As(recipient);
          
          if (employee != null && !compareEmployees.Contains(employee))
            compareEmployees.Add(employee);
          if (crole != null)
          {
            var recepientList = crole.RecipientLinks.Select(r => r.Member).Where(m => Sungero.Company.Employees.Is(m)).ToList();
            foreach (var roleRecipient in recepientList)
            {
              var roleEmployee = Sungero.Company.Employees.As(roleRecipient);
              if (roleEmployee != null && !compareEmployees.Contains(roleEmployee))
                compareEmployees.Add(roleEmployee);
            }
          }
        }
        
        if (compareEmployees.Any() && roleEmployees.Any())
        {
          var includedEmployees = roleEmployees.Where(r => compareEmployees.Contains(r)).ToList();
          bool findRecipient = roleEmployees.Where(r => compareEmployees.Contains(r)).Any();
          
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(findRecipient, string.Empty);
        }
        else
          return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty); // Исполнители для сравнения не указаны
      }
      
      return base.CheckCondition(document, task);
    }
    
    // проверка возможности использования условия
    public override System.Collections.Generic.Dictionary<string, List<Enumeration?>> GetSupportedConditions()
    {
      var baseSupport = base.GetSupportedConditions();
      foreach (var key in baseSupport.Keys)
      {
        baseSupport[key].AddRange(new List<Enumeration?> { ConditionType.SingES });
        baseSupport[key].AddRange(new List<Enumeration?> { ConditionType.DepCompareranorsv });
        baseSupport[key].AddRange(new List<Enumeration?> { ConditionType.EmpInRoleInclud });
      }
      
      // получить все типы - наследники служебных записок
      var typesMemos = Sungero.Docflow.PublicFunctions.DocumentKind.GetDocumentGuids(typeof(Sungero.Docflow.IMemo));
      
      // доступность условия для заявки на закупку
      baseSupport["247bdd6c-d233-4253-9adf-fc04f905283a"].Add(ConditionType.JustPurchase);
      baseSupport["247bdd6c-d233-4253-9adf-fc04f905283a"].Add(ConditionType.SumPlan);
      baseSupport["247bdd6c-d233-4253-9adf-fc04f905283a"].Add(ConditionType.ZGD);
      baseSupport["247bdd6c-d233-4253-9adf-fc04f905283a"].Add(ConditionType.IsStandard);
      baseSupport["247bdd6c-d233-4253-9adf-fc04f905283a"].Add(ConditionType.IsStandardTS);
      baseSupport["247bdd6c-d233-4253-9adf-fc04f905283a"].Add(ConditionType.TopicContract);
      baseSupport["247bdd6c-d233-4253-9adf-fc04f905283a"].Add(ConditionType.TopicTS);
      baseSupport["247bdd6c-d233-4253-9adf-fc04f905283a"].Add(ConditionType.IsStandardConTS);
      
      // доступность условия для строки плана потребностей
      baseSupport["0fcd58ce-53d7-4b3c-b317-35995c48ca7f"].Add(ConditionType.SumContract);
      
      // доступность условия для служебных записок
      foreach (var type in typesMemos)
      {
        baseSupport[type].AddRange(new List<Enumeration?> {ConditionType.Preagreement});
        baseSupport[type].AddRange(new List<Enumeration?> {ConditionType.DITagreement});
        baseSupport[type].AddRange(new List<Enumeration?> {ConditionType.NZIagreement});
        baseSupport[type].AddRange(new List<Enumeration?> {ConditionType.TopicMemoCompar});
      }
      
      // доступность условия для заявки на замещение
      baseSupport["145cf65a-ce44-4db7-8dd4-67aa8111da5c"].Add(ConditionType.Substitution1C);
      
      var officialDocs = Sungero.Docflow.PublicFunctions.DocumentKind.GetDocumentGuids(typeof(Sungero.Docflow.IOfficialDocument));
      
      foreach (var doc in officialDocs)
      {
        baseSupport[doc].AddRange(new List<Enumeration?> {ConditionType.ZGD});
      }
      
      var contractDocs = Sungero.Docflow.PublicFunctions.DocumentKind.GetDocumentGuids(typeof(Sungero.Contracts.IContractualDocument));
      
      foreach (var doc in contractDocs)
      {
        baseSupport[doc].AddRange(new List<Enumeration?> {ConditionType.ZGD});
      }
      // Отключено как непубличный функционал
      //      var types = Sungero.Docflow.PublicFunctions.DocumentKind.GetDocumentGuids(typeof(anorsv.OfficialDocument.IOfficialDocument));
      //      foreach (var type in types)
      //        baseSupport[type].Add(ConditionType.TopicCompar);
      
      return baseSupport;
    }
    
    // видимость и обязательность контролов
    public override void ChangePropertiesAccess()
    {
      base.ChangePropertiesAccess();

      var isZGD = _obj.ConditionType == ConditionType.ZGD;
      if (isZGD)
      {
        _obj.State.Properties.ApprovalRole.IsVisible = true;
        _obj.State.Properties.ApprovalRole.IsRequired = true;
      }
      
      var isJustPurchase = _obj.ConditionType == ConditionType.JustPurchase;

      _obj.State.Properties.JustificationsPurchasesline.IsVisible = isJustPurchase;
      _obj.State.Properties.JustificationsPurchasesline.IsRequired = isJustPurchase;
      
      var isSumPlan = _obj.ConditionType == ConditionType.SumPlan;

      _obj.State.Properties.SumPlanOperatorsline.IsVisible = isSumPlan;
      _obj.State.Properties.SumPlanOperatorsline.IsRequired = isSumPlan;
      _obj.State.Properties.SumPlansline.IsVisible = isSumPlan;
      _obj.State.Properties.SumPlansline.IsRequired = isSumPlan;
      
      if (_obj.ConditionType != ConditionType.AmountIsMore)
      {
        var isSumContract = _obj.ConditionType == ConditionType.SumContract;

        _obj.State.Properties.Amount.IsVisible = isSumContract;
        _obj.State.Properties.Amount.IsRequired = isSumContract;
        _obj.State.Properties.AmountOperator.IsVisible = isSumContract;
        _obj.State.Properties.AmountOperator.IsRequired = isSumContract;
      }
      
      var isDepartmentCompare = _obj.ConditionType == ConditionType.DepCompareranorsv;
      _obj.State.Properties.Departmentsanorsv.IsVisible = isDepartmentCompare;
      _obj.State.Properties.Subdepartmentanorsv.IsVisible = isDepartmentCompare;
      _obj.State.Properties.ExcludeDepartmentsanorsv.IsVisible = isDepartmentCompare;
      _obj.State.Properties.ExcludeDepartmentsanorsv.IsEnabled = (_obj.Subdepartmentanorsv == true);
      _obj.State.Properties.ExcludeSubdepartmentsanorsv.IsVisible = isDepartmentCompare;
      _obj.State.Properties.ExcludeSubdepartmentsanorsv.IsEnabled = (_obj.Subdepartmentanorsv == true);
      if (isDepartmentCompare)
      {
        _obj.State.Properties.ApprovalRole.IsVisible = true;
        _obj.State.Properties.ApprovalRole.IsRequired = true;
      }
      
      var isTopicMemoCompare = _obj.ConditionType == ConditionType.TopicMemoCompar;
      _obj.State.Properties.TopicMemoanorsv.IsVisible = isTopicMemoCompare;
      
      var isTopicGeneralCompare = _obj.ConditionType == ConditionType.TopicCompar;
      var isTopicContract = _obj.ConditionType == ConditionType.TopicContract;
      var isTopicTS = _obj.ConditionType == ConditionType.TopicTS;
      
      _obj.State.Properties.Topicanorsv.IsVisible = isTopicGeneralCompare || isTopicContract || isTopicTS;
      
      var isEmpInRoleIncluded = _obj.ConditionType == ConditionType.EmpInRoleInclud;
      _obj.State.Properties.Recipientsanorsv.IsVisible = isEmpInRoleIncluded;
      _obj.State.Properties.ApprovalRolesanorsv.IsVisible = isEmpInRoleIncluded;
      if (isEmpInRoleIncluded)
      {
        _obj.State.Properties.ApprovalRole.IsVisible = true;
        _obj.State.Properties.ApprovalRole.IsRequired = true;
      }

    }

    // очистка неиспользуемых контролов
    public override void ClearHiddenProperties()
    {
      base.ClearHiddenProperties();

      if (!_obj.State.Properties.ApprovalRole.IsVisible)
        _obj.ApprovalRole = null;
      
      if (!_obj.State.Properties.JustificationsPurchasesline.IsVisible)
        _obj.JustificationsPurchasesline.Clear();
      
      if (!_obj.State.Properties.SumPlansline.IsVisible)
        _obj.SumPlansline = null;
      
      if (!_obj.State.Properties.Topicanorsv.IsVisible && _obj.Topicanorsv.Any())
      {
        foreach (var topicRow in _obj.Topicanorsv)
        {
          _obj.Topicanorsv.Remove(topicRow);
        }
      }
      
      if (!_obj.State.Properties.TopicMemoanorsv.IsVisible && _obj.TopicMemoanorsv.Any())
      {
        foreach (var topicMemoRow in _obj.TopicMemoanorsv)
        {
          _obj.TopicMemoanorsv.Remove(topicMemoRow);
        }
      }
    }
    
    // вычисляем сумму
    private bool? CheckSum(double? documentAmount)
    {
      if (_obj.SumPlanOperatorsline == AmountOperator.GreaterThan)
        return documentAmount > _obj.SumPlansline;
      if (_obj.SumPlanOperatorsline == AmountOperator.GreaterOrEqual)
        return documentAmount >= _obj.SumPlansline;
      if (_obj.SumPlanOperatorsline == AmountOperator.LessThan)
        return documentAmount < _obj.SumPlansline;
      if (_obj.SumPlanOperatorsline == AmountOperator.LessOrEqual)
        return documentAmount <= _obj.SumPlansline;
      
      return null;
    }
    private bool? CheckAmount(double? documentAmount)
    {
      if (_obj.AmountOperator == AmountOperator.GreaterThan)
        return documentAmount > _obj.Amount;
      if (_obj.AmountOperator == AmountOperator.GreaterOrEqual)
        return documentAmount >= _obj.Amount;
      if (_obj.AmountOperator == AmountOperator.LessThan)
        return documentAmount < _obj.Amount;
      if (_obj.AmountOperator == AmountOperator.LessOrEqual)
        return documentAmount <= _obj.Amount;
      
      return null;
    }
    
  }
}
