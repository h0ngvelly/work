using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Memo;

namespace sline.RSV
{
  partial class MemoSharedHandlers
  {

    public override void LeadingDocumentChanged(Sungero.Docflow.Shared.OfficialDocumentLeadingDocumentChangedEventArgs e)
    {
      base.LeadingDocumentChanged(e);
    }

    public override void TopicanorsvChanged(anorsv.OfficialDocument.Shared.OfficialDocumentTopicanorsvChangedEventArgs e)
    {
      base.TopicanorsvChanged(e);
    }

    public virtual void EmployeeanorsvChanged(sline.RSV.Shared.MemoEmployeeanorsvChangedEventArgs e)
    {
      if (_obj.Employeeanorsv != null)
        _obj.Subdivisionanorsv = _obj.Employeeanorsv.Department;
    }

    public override void DocumentKindChanged(Sungero.Docflow.Shared.OfficialDocumentDocumentKindChangedEventArgs e)
    {
      base.DocumentKindChanged(e);
      
      // Если обновили/очистили Вид документа, повторно проверить Тематику
      if (e.NewValue == null || e.NewValue != null && !e.NewValue.Equals(e.OldValue) && _obj.MemoTopicanorsv != null)
        _obj.MemoTopicanorsv = null;
    }

    public virtual void MemoTopicanorsvChanged(sline.RSV.Shared.MemoMemoTopicanorsvChangedEventArgs e)
    {
      if (e.NewValue != null)
      {
        if (_obj.MemoTopicanorsv.Name != "Тематика не обозначена")
        {
          // Предзаполнить Содержание
          if (!string.IsNullOrEmpty(_obj.MemoTopicanorsv.SubjectTemplate))
            _obj.Subject = _obj.MemoTopicanorsv.SubjectTemplate;
          // Предзаполнить Адресата
          if (_obj.MemoTopicanorsv.Addressee != null)
            _obj.Addressee = _obj.MemoTopicanorsv.Addressee;
          // Предзаполнить Подписанта
          if (_obj.MemoTopicanorsv.OurSignatory != null)
          {
            // Если Любой работник, то заполнить инициатора
            if (_obj.MemoTopicanorsv.OurSignatory.Equals(sline.CustomModule.TopicMemo.OurSignatory.AnyEmployee))
            {
              _obj.OurSignatory = _obj.PreparedBy;
            }
            else
            {
              // Если РП, то заполнить непосредственного руководителя
              if (_obj.MemoTopicanorsv.OurSignatory.Equals(sline.CustomModule.TopicMemo.OurSignatory.DepManager))
                _obj.OurSignatory = _obj.Department.Manager;
            }
          }
        }
      }

      // Указание подписанта обязательно для если явно не указано обратное в настроках тематики
      _obj.State.Properties.OurSignatory.IsRequired = e.NewValue == null || e.NewValue.OurSignatory != sline.CustomModule.TopicMemo.OurSignatory.NotRequired;
      
      if (_obj.MemoTopicanorsv != null)
      {
        if (_obj.MemoTopicanorsv.ObjectMemo == sline.CustomModule.TopicMemo.ObjectMemo.NotRequired || _obj.MemoTopicanorsv.ObjectMemo  == null)
        {
          _obj.Employeeanorsv = null;
          _obj.Subdivisionanorsv = null;
          
          _obj.State.Properties.Employeeanorsv.IsVisible = false;
          _obj.State.Properties.Subdivisionanorsv.IsVisible = false;
          
          _obj.State.Properties.Employeeanorsv.IsRequired = false;
          _obj.State.Properties.Subdivisionanorsv.IsRequired = false;
        }
        
        if (_obj.MemoTopicanorsv.ObjectMemo == sline.CustomModule.TopicMemo.ObjectMemo.Subdivision)
        {
          _obj.Employeeanorsv = null;
          _obj.Subdivisionanorsv = null;
          
          _obj.State.Properties.Employeeanorsv.IsVisible = false;
          _obj.State.Properties.Subdivisionanorsv.IsVisible = true;
          _obj.State.Properties.Subdivisionanorsv.IsEnabled = true;
          _obj.State.Properties.Subdivisionanorsv.IsRequired = true;
          _obj.State.Properties.Employeeanorsv.IsRequired = false;
        }
        
        if (_obj.MemoTopicanorsv.ObjectMemo == sline.CustomModule.TopicMemo.ObjectMemo.Employee)
        {
          _obj.Employeeanorsv = null;
          _obj.Subdivisionanorsv = null;
          
          _obj.State.Properties.Employeeanorsv.IsVisible = true;
          _obj.State.Properties.Employeeanorsv.IsEnabled = true;
          _obj.State.Properties.Employeeanorsv.IsRequired = true;
          _obj.State.Properties.Subdivisionanorsv.IsVisible = true;
          _obj.State.Properties.Subdivisionanorsv.IsEnabled = false;
          _obj.State.Properties.Subdivisionanorsv.IsRequired = false;
        }
      }
      
      if (_obj.MemoTopicanorsv != null && (_obj.MemoTopicanorsv.ObjectMemo == sline.CustomModule.TopicMemo.ObjectMemo.Subdivision))
      {
        _obj.State.Properties.Employeeanorsv.IsVisible = false;
        _obj.State.Properties.Subdivisionanorsv.IsVisible = true;
        _obj.State.Properties.Subdivisionanorsv.IsEnabled = true;
        _obj.State.Properties.Subdivisionanorsv.IsRequired = true;
        _obj.State.Properties.Employeeanorsv.IsRequired = false;
        
        _obj.Employeeanorsv = null;
        _obj.Subdivisionanorsv = null;
      }
      
      if (_obj.MemoTopicanorsv != null && (_obj.MemoTopicanorsv.ObjectMemo == sline.CustomModule.TopicMemo.ObjectMemo.Employee))
      {
        _obj.State.Properties.Employeeanorsv.IsVisible = true;
        _obj.State.Properties.Employeeanorsv.IsEnabled = true;
        _obj.State.Properties.Employeeanorsv.IsRequired = true;
        _obj.State.Properties.Subdivisionanorsv.IsVisible = true;
        _obj.State.Properties.Subdivisionanorsv.IsEnabled = false;
        _obj.State.Properties.Subdivisionanorsv.IsRequired = false;
        
        _obj.Employeeanorsv = null;
        _obj.Subdivisionanorsv = null;
      }
      
    }

  }
}
