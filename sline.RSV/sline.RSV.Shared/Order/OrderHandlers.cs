using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution;
using sline.RSV.Order;

namespace sline.RSV
{
    partial class OrderSharedHandlers
    {

      public override void SignESanorsvChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
      {
        base.SignESanorsvChanged(e);
        // Синхронизироват значения Подписать ЭП и Способ подписания
        _obj.SignESsline = (_obj.SignESanorsv.Equals(anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES)) ? (true) : (false);
      }

      public override void DocumentKindChanged(Sungero.Docflow.Shared.OfficialDocumentDocumentKindChangedEventArgs e)
      {
        if (e.NewValue != e.OldValue && _obj.Topicanorsv != null)
          _obj.Topicanorsv = null;
        
        base.DocumentKindChanged(e);
          
          var ord = sline.CustomModule.TopicOrds.GetAll().FirstOrDefault(x => x.DocumentKind == _obj.DocumentKind);
          if (ord != null)
          {
              _obj.ApproversORDsline.Clear();
              foreach(var approver in ord.Approvers)
              {
                _obj.ApproversORDsline.AddNew().Approver = Recipients.As(approver.Approver);
              }
          }
            
          if (e.NewValue != null)
          {
            var docKind = DocumentKinds.As(e.NewValue);
              
            // Заполнить Подписанта по Основному подписанту из Вида документа
            _obj.OurSignatory = docKind.DocKindSignatoryanorsv;
            // Заполнить Способ подписания из Вида документа
            if (docKind.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignESanorsv))
              _obj.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES;
            if (docKind.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignOnPaper))
              _obj.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignOnPaper;
            
            // Проверить возможность изменения вида
            bool canModifyDocKind = anorsv.DocflowModule.PublicFunctions.Module.Remote.CanChangeDocumentKind(_obj);
            e.Params.AddOrUpdate("AnoRsvCanModifyDocKind", canModifyDocKind);
          }

      }
    }
}