﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvMainSolution;
using sline.RSV.OutgoingLetter;

namespace sline.RSV
{
  partial class OutgoingLetterSharedHandlers
  {

    public override void DocumentKindChanged(Sungero.Docflow.Shared.OfficialDocumentDocumentKindChangedEventArgs e)
    {
      base.DocumentKindChanged(e);
      
      if (e.NewValue != null)
      {
        var docKind = DocumentKinds.As(e.NewValue);
        
        // Заполнить Подписанта по Основному подписанту из Вида документа
        _obj.OurSignatory = docKind.DocKindSignatoryanorsv;
        // Заполнить Способ подписания из Вида документа
        if (docKind.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignESanorsv))
          _obj.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES;
        if (docKind.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignOnPaper))
          _obj.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignOnPaper;
        
      }
     
    }

    public override void SignESanorsvChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
    {
      base.SignESanorsvChanged(e);
      // Синхронизироват значения Подписать ЭП и Способ подписания
      _obj.SignESsline = (_obj.SignESanorsv.Equals(anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES)) ? (true) : (false);
    }

  }
}