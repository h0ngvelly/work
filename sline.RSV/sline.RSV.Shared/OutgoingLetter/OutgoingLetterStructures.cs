﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace sline.RSV.Structures.RecordManagement.OutgoingLetter
{
    partial class StagesEmployees
    {
        public sline.RSV.IApprovalStage Stage { get; set; }
        
        public int? Number { get; set; }
        
        public Sungero.Core.Enumeration? StageType { get; set; }
        
        public List<sline.RSV.IEmployee> Employees { get; set; }
    }
}