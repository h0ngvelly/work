﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Addendum;

namespace sline.RSV
{
  partial class AddendumSharedHandlers
  {

    public override void LeadingDocumentChanged(Sungero.Docflow.Shared.OfficialDocumentLeadingDocumentChangedEventArgs e)
    {
      if (Equals(e.NewValue, e.OldValue))
        return;
      
      if (e.NewValue != null)
      {
        var document = e.NewValue;
        if (document.BusinessUnit != null)
          _obj.BusinessUnit = document.BusinessUnit;
        
        Sungero.Docflow.PublicFunctions.OfficialDocument.CopyProjects(e.NewValue, _obj);
      }

      FillName();
      
      if (e.OldValue != null && e.NewValue == null)
        _obj.Relations.RemoveFrom(Sungero.Docflow.PublicConstants.Module.AddendumRelationName, e.OldValue);
      else
        _obj.Relations.AddFromOrUpdate(Sungero.Docflow.PublicConstants.Module.AddendumRelationName, e.OldValue, e.NewValue);
    }

  }
}