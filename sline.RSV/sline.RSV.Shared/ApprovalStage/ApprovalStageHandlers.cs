﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalStage;

namespace sline.RSV
{
  partial class ApprovalStageSharedHandlers
  {

    public override void StageTypeChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
    {
      base.StageTypeChanged(e);
      
      if (_obj.StageType == StageType.SimpleAgr)
        _obj.State.Properties.TaskTextanorsv.IsVisible = true;
      else
      {
        _obj.TaskTextanorsv = null;
        _obj.State.Properties.TaskTextanorsv.IsVisible = false;
      }
    }

  }
}