using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalStage;

namespace sline.RSV.Shared
{
  partial class ApprovalStageFunctions
  {
    public override List<Enumeration?> GetPossibleRoles()
    {
      var baseRoles = base.GetPossibleRoles();

      if (_obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers ||
          _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.SimpleAgr ||
          _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Notice)
      {

        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.PurchaseDZD);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.AppManagers);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.PurchRespons);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.ApprORD);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.ManagLessFirst);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.PreApprovers);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.SubstNotice);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.SubstApprov);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.GDDeputy);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.DocAuthor);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.ResponsibleFRC);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.AppManager);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.AppManagerGDDep);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.ZGDWithoutFirst);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.GDDirectSubord);
      }
      if (_obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Manager)
      {
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.AppManager);
        baseRoles.Add(sline.CustomModule.MyApprovalRoles.Type.AppManagerGDDep);
      }

      return baseRoles;
    }
    
  }
}