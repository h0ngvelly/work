﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.IncomingLetter;

namespace sline.RSV.Shared
{
  partial class IncomingLetterFunctions
  {

    /// <summary>
    /// Зпаолняет наименование входящего документа по формату [Вид документа] от [корреспондент]  №[исходящий номер контрагента] от [дата] "[содержание]" (входящий №[номер] от [дата])
    /// </summary>
    public override void FillName()
    {
      // Не автоформируемое имя.
      if (_obj != null && _obj.DocumentKind != null && !_obj.DocumentKind.GenerateDocumentName.Value)
      {
        if (_obj.Name == Sungero.Docflow.Resources.DocumentNameAutotext)
          _obj.Name = string.Empty;
        
        if (_obj.VerificationState != null && string.IsNullOrWhiteSpace(_obj.Name))
          _obj.Name = _obj.DocumentKind.ShortName;
      }
      
      if (_obj.DocumentKind == null || !_obj.DocumentKind.GenerateDocumentName.Value)
        return;
      
      // Автоформируемое имя.
      string name = string.Empty;
      string registrationData = string.Empty;
      string subject = string.Empty;
      bool registrationNumberFilled = _obj.RegistrationNumber != null && !string.IsNullOrWhiteSpace(Sungero.Docflow.PublicFunctions.Module.TrimSpecialSymbols(_obj.RegistrationNumber));
      bool registrationDateFilled = _obj.RegistrationDate != null;
      
      /* Имя в формате:
        <Вид документа> от <корреспондент> №<исходящий номер контрагента> от <дата> "<содержание>" (№<входящий номер> от <дата>) .
       */
      name = Sungero.Docflow.PublicFunctions.Module.TrimSpecialSymbols(_obj.DocumentKind.ShortName + name);

      using (TenantInfo.Culture.SwitchTo())
      {
        
        if (_obj.Correspondent != null)
          name += Sungero.Docflow.PublicFunctions.Module.TrimSpecialSymbols(IncomingLetters.Resources.CorrespondentFrom + _obj.Correspondent.DisplayValue);
        
        if (!string.IsNullOrWhiteSpace(_obj.InNumber))
          name += Sungero.Docflow.PublicFunctions.Module.TrimSpecialSymbols(Sungero.Docflow.OfficialDocuments.Resources.Number + _obj.InNumber);
        
        if (_obj.Dated != null)
          name += Sungero.Docflow.OfficialDocuments.Resources.DateFrom + _obj.Dated.Value.ToString("d");
        
        if (registrationNumberFilled || registrationDateFilled)
        {
          string registrationNumber = _obj.RegistrationNumber != null ? Sungero.Docflow.PublicFunctions.Module.TrimSpecialSymbols(_obj.RegistrationNumber) : string.Empty;
          string registrationDate = _obj.RegistrationDate != null ? _obj.RegistrationDate.Value.ToString("d") : string.Empty;
          
          registrationData = IncomingLetters.Resources.IncomingRegistrationDataFormat(registrationNumber, registrationDate);
        }
        
        if (!string.IsNullOrWhiteSpace(_obj.Subject))
          name += TrimSubject(name, " \"" + _obj.Subject + "\"", registrationData, _obj);
        
        name += registrationData;
      }
      
      if (_obj.Name == null || !_obj.Name.Equals(name))
      {
        _obj.Name = name;
      }
    }
    
    public virtual string TrimSubject(string beforePart, string subject, string afterPart, IIncomingLetter document)
    {
      string partsWithoutSubject = beforePart + afterPart;
      string partsWithSubject = beforePart + subject + afterPart;
      int substringLength = document.Info.Properties.Name.Length - partsWithoutSubject.Length - 4;
      
      return partsWithSubject.Length > document.Info.Properties.Name.Length ?
        subject.Substring(0, substringLength > 0 ? substringLength : 1) + "...\"" :
        subject;
    }

  }
}
