using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Employee;

namespace sline.RSV
{
  partial class EmployeeSharedHandlers
  {

    public virtual void ManuallyanorsvChanged(Sungero.Domain.Shared.BooleanPropertyChangedEventArgs e)
    {
      // Если Указать вручную = True, дать изменять администратору значение <Отображать в шаблоне>
      _obj.State.Properties.TemplateJobanorsv.IsEnabled = (_obj.Manuallyanorsv == true);
    }

    public override void JobTitleChanged(Sungero.Company.Shared.EmployeeJobTitleChangedEventArgs e)
    {
      base.JobTitleChanged(e);
      
      // Обновить реквизит для отображения Должности-Подразделения в шаблонах
      sline.RSV.PublicFunctions.Employee.UpdateTemplateJob(_obj);
    }

    public override void DepartmentChanged(Sungero.Company.Shared.EmployeeDepartmentChangedEventArgs e)
    {
      base.DepartmentChanged(e);
      
      // Обновить реквизит для отображения Должности-Подразделения в шаблонах
      sline.RSV.PublicFunctions.Employee.UpdateTemplateJob(_obj);      
    }

  }
}