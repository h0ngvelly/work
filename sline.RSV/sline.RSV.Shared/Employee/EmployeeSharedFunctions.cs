﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Employee;

namespace sline.RSV.Shared
{
  partial class EmployeeFunctions
  {

    /// <summary>
    /// Обновление реквизита для отображения Должности-Подразделения в шаблонах
    /// </summary>    
    [Public]    
    public static void UpdateTemplateJob(sline.RSV.IEmployee employee)
    {
      if (employee.Manuallyanorsv != true)
      {
        var jobTitle = (employee.JobTitle != null) ? employee.JobTitle.Name : string.Empty;
        var departmentTitle = (employee.Department != null) ? employee.Department.Name : string.Empty;
        var forTemplate = string.Empty;
      
        if (employee.NotUpdateAutomaticallyanorsv == true)
        {
          // Если не обновляем автоматически, то Подразделение фиктивное и в шаблон берем только Должность
          forTemplate = jobTitle;
        }
        else
        {
          // Если обновляем, то проверяем на дубли в конце Должности и начале Подразделения
          var lastWordJob = (jobTitle.LastIndexOf(" ") > 0) ? jobTitle.Substring(jobTitle.LastIndexOf(" ") + 1) : jobTitle; 
          var firstWordDep = (departmentTitle.IndexOf(" ") > 0) ? departmentTitle.Substring(0, departmentTitle.IndexOf(" ")) : departmentTitle; 
          
          var lastWordJobNom = CaseConverter.ConvertDepartmentTitleToTargetDeclension(lastWordJob, DeclensionCase.Nominative).ToUpper();
          var firstWordDepNom = CaseConverter.ConvertDepartmentTitleToTargetDeclension(firstWordDep, DeclensionCase.Nominative).ToUpper();
          var lastWordJobPart = lastWordJobNom;
          var firstWordDepPart = firstWordDepNom;
          if (firstWordDepNom.Length > 0 && lastWordJobNom.Length > 0 && firstWordDepNom.Length != lastWordJobNom.Length)
          {
            if (firstWordDepNom.Length < lastWordJobNom.Length)
              lastWordJobPart = lastWordJobNom.Substring(0, firstWordDepNom.Length);
            if (lastWordJobNom.Length < firstWordDepNom.Length)
              firstWordDepPart = firstWordDepNom.Substring(0, lastWordJobNom.Length);
          }
          
          if (!string.IsNullOrWhiteSpace(lastWordJob) && !string.IsNullOrWhiteSpace(firstWordDep) && 
              (lastWordJobNom == firstWordDepNom || lastWordJobPart == firstWordDepPart))
          {
            // Если последнее слово Должности совпадает с первым словом Подразделения, то оставляем только подразделение
            forTemplate = jobTitle.Substring(0, jobTitle.LastIndexOf(" ")) + " \n" + CaseConverter.ConvertDepartmentTitleToTargetDeclension(departmentTitle, DeclensionCase.Genitive);
          }
          else
          {
            // Если слова не совпадают, то оставляем целиком Должность и Подразделение
            forTemplate = jobTitle + " \n" + CaseConverter.ConvertDepartmentTitleToTargetDeclension(departmentTitle, DeclensionCase.Genitive);
          } 
        }
        
        employee.TemplateJobanorsv = forTemplate;
      }
    }

  }
}