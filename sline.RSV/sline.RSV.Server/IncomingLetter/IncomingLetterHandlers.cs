using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.IncomingLetter;

namespace sline.RSV
{
  partial class IncomingLetterServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);

      #region Заполнить имя

      Functions.IncomingLetter.FillName(_obj);

      #endregion
    }
  }

}