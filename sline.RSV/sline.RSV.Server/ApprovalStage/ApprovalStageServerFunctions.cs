using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalStage;

namespace sline.RSV.Server
{
  partial class ApprovalStageFunctions
  {
    
    public override List<IRecipient> GetStageRecipients(Sungero.Docflow.IApprovalTask task, List<IRecipient> additionalApprovers)
    {
      var baseRecipients = base.GetStageRecipients(task, additionalApprovers);
      
      var role0 = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.PreApprovers)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (role0 != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role0, task));
      
      var role = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagers)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (role != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role, task));

      var role2 = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.ApprORD)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (role2 != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role2, task));
      
      var role3 = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.ManagLessFirst)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (role3 != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role3, task));

      var role4 = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.SubstNotice)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (role4 != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role4, task));
      
      var role5 = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.SubstApprov)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (role5 != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role5, task));
      
      var role6 = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.GDDeputy)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (role6 != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role6, task));
      
      // Автор документа
      var roleDocAuthor = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.DocAuthor)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (roleDocAuthor != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(roleDocAuthor, task));
      
      // Ответственный за ЦФО
      var roleResponsibleFRC = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.ResponsibleFRC)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (roleResponsibleFRC != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(roleResponsibleFRC, task));
      
      // Непосредственный руководитель по иерархии
      var roleAppManager = _obj.ApprovalRoles.Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManager)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (roleAppManager != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(roleAppManager, task));
      
      // Непосредственный руководитель по иерархии до ЗГД
      var roleAppManagerLessGDDeputy = _obj.ApprovalRoles
        .Where(r => r.ApprovalRole != null && r.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.AppManagerGDDep)
        .Select(r => sline.CustomModule.MyApprovalRoleses.As(r.ApprovalRole))
        .Where(r => r != null)
        .SingleOrDefault();
      
      if (roleAppManagerLessGDDeputy != null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(roleAppManagerLessGDDeputy, task));
      
      // Заместитель ГД (кроме ПЗГД)
      var role7 = _obj.ApprovalRoles
        .Where(x => x.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.ZGDWithoutFirst)
        .Select(x => sline.CustomModule.MyApprovalRoleses.As(x.ApprovalRole))
        .Where(x => x!= null)
        .SingleOrDefault();
      
      if (role7!= null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role7, task));
      
      // Руководители прямого подчинения ГД
      var role8 = _obj.ApprovalRoles
        .Where(x => x.ApprovalRole.Type == sline.CustomModule.MyApprovalRoles.Type.GDDirectSubord)
        .Select(x => sline.CustomModule.MyApprovalRoleses.As(x.ApprovalRole))
        .Where(x => x!= null)
        .SingleOrDefault();
      
      if (role8!= null)
        baseRecipients.AddRange(sline.CustomModule.PublicFunctions.MyApprovalRoles.Remote.GetRolePerformers(role8, task));
      
      return baseRecipients;
    }
    
    
  }
}