﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalStage;

namespace sline.RSV
{
    partial class ApprovalStageServerHandlers
    {

        public override void Created(Sungero.Domain.CreatedEventArgs e)
        {
            _obj.IgnoreStagessline = false;
            _obj.ZGDsline = false;
            _obj.CreateApprovalSheetsline = false;
            _obj.NoSecondApprovesline = false;
            _obj.SubtasksIsAllowedanorsv = true;
            _obj.SubtaskInitiatorByDefaultanorsv = SubtaskInitiatorByDefaultanorsv.Performer;
            _obj.BCHPanorsv = false;
            _obj.PreliminaryApprovalBlockanorsv = false;
            _obj.AgreeWithoutFinalVerificationanorsv = false;
            _obj.SkipStageWithoutFinalVerificationanorsv = false;
            _obj.ApprovalPurhaseanorsv = false;
            _obj.NeedAttachMemoanorsv = false;
            _obj.NeedAttachContractDraftanorsv = false;
            base.Created(e);
        }
    }

}