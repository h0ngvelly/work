using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.CompanyDirective;

namespace sline.RSV
{
    partial class CompanyDirectiveORDslinePropertyFilteringServerHandler<T>
    {

        public virtual IQueryable<T> ORDslineFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
        {
            query = query.Where(x => Equals(_obj.DocumentKind, x.DocumentKind));
            return query;
        }
    }

    partial class CompanyDirectiveServerHandlers
    {

        public override void Created(Sungero.Domain.CreatedEventArgs e)
        {
            base.Created(e);
            _obj.SignESsline = true;
        }
    }

}