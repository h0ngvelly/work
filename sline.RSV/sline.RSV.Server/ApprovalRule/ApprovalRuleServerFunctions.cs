﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalRule;

namespace sline.RSV.Server
{
    partial class ApprovalRuleFunctions
    {

        // Проверка возможности существования ветки при добавлении в нее нового условия.
        public override bool CheckRoutePossibility(List<Sungero.Docflow.Structures.ApprovalRuleBase.RouteStep> route,
                                                   List<Sungero.Docflow.Structures.ApprovalRuleBase.ConditionRouteStep> ruleConditions,
                                                   Sungero.Docflow.Structures.ApprovalRuleBase.RouteStep conditionStep)
        {

            // Получить список допустимых веток по базовой логике.
            var possibleStage = base.CheckRoutePossibility(route, ruleConditions, conditionStep);
            var conditionType = _obj.Conditions.First(c => c.Number == conditionStep.StepNumber).Condition.ConditionType;

            // Проверить условия по виду закупки.
            if (conditionType == sline.RSV.Condition.ConditionType.JustPurchase)
            {
                // Получить все условия по виду закупки из ветки.
                var purchaseKindConditions = this.GetJustPurchaseConditionsInRoute(route).Where(c => c.StepNumber != conditionStep.StepNumber).ToList();
                // Определить допустимость ветки с точки зрения условий по виду закупки.
                possibleStage = this.CheckJustPurchaseConditions(purchaseKindConditions, conditionStep);
            }
            
            /*if (conditionType == sline.RSV.Condition.ConditionType.SingES)
            {
                // Получить все условия по виду закупки из ветки.
                var purchaseKindConditions = this.GetSingESConditionsInRoute(route).Where(c => c.StepNumber != conditionStep.StepNumber).ToList();
                // Определить допустимость ветки с точки зрения условий по виду закупки.
                possibleStage = this.CheckSingESConditions(purchaseKindConditions, conditionStep);
            }*/
            
             
            return possibleStage;
        }
        
        // Получить все условия по обоснованиям закупки в данной ветке.
        public List<Sungero.Docflow.Structures.ApprovalRuleBase.RouteStep> GetJustPurchaseConditionsInRoute(List<Sungero.Docflow.Structures.ApprovalRuleBase.RouteStep> route)
        {
            return route.Where(e => _obj.Conditions.Any(c => Equals(c.Number, e.StepNumber) && c.Condition.ConditionType ==
                                                        sline.RSV.Condition.ConditionType.JustPurchase)).ToList();
        }
        /*// Получить все условия по подписанию ЭП в данной ветке.
        public List<Structures.ApprovalRuleBase.RouteStep> GetSingESConditionsInRoute(List<Structures.ApprovalRuleBase.RouteStep> route)
        {
            return route.Where(e => _obj.Conditions.Any(c => Equals(c.Number, e.StepNumber) && c.Condition.ConditionType ==
                                                        sline.RSV.Condition.ConditionType.SingES)).ToList();
        }*/
            
            // Сравнить условия в схеме и проверить,
            // какие ветки правила согласования нужно валидировать.
            public bool CheckJustPurchaseConditions(List<Sungero.Docflow.Structures.ApprovalRuleBase.RouteStep> allConditions, Sungero.Docflow.Structures.ApprovalRuleBase.RouteStep condition)
        {
            var conditionItem = _obj.Conditions.Where(x => x.Number == condition.StepNumber).FirstOrDefault();
            var contractCondition = sline.RSV.Conditions.As(conditionItem.Condition);
            var justificationsPurchase = contractCondition.JustificationsPurchasesline;

            // Проверить непротиворечивость условия с предыдущими условиями этого же типа в ветке.
            foreach (var previousCondition in allConditions.TakeWhile(x => !Equals(x, condition)))
            {
                var previousConditionItem = _obj.Conditions.Where(x => x.Number == previousCondition.StepNumber).FirstOrDefault();
                var previousContractCondition = sline.RSV.Conditions.As(previousConditionItem.Condition);
                var previousJustificationsPurchase = previousContractCondition.JustificationsPurchasesline;
                
                // Если вид закупки в предыдущем условии такой же, а переход отличается – ветка не валидна.
                if (justificationsPurchase == previousJustificationsPurchase && previousCondition.Branch != condition.Branch)
                    return false;

            }

            return true;
        }
    }
}