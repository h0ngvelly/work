﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalRule;

namespace sline.RSV
{
  partial class ApprovalRuleServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      // Оключено как не публичнй функционал
      _obj.ShowSubstitutesanorsv = false;
    }

    public override void AfterSave(Sungero.Domain.AfterSaveEventArgs e)
    {
      base.AfterSave(e);
      
      // Отключено как не публичный функционал
//      var rule = anorsv.DocflowModule.PublicFunctions.Module.Remote.GetApprovalTaskAccessRightsRule(_obj);
//      if(rule != null)
//        anorsv.DocflowModule.PublicFunctions.Module.Remote.CreateApprovalTaskAccessRightsRule(_obj);
    }
  }

}