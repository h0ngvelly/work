﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Condition;

namespace sline.RSV.Server
{
  partial class ConditionFunctions
  {
    public override string GetConditionName()
    {
      using (TenantInfo.Culture.SwitchTo())
      {
        if (_obj.ConditionType == ConditionType.IsStandardConTS)
          return sline.RSV.Conditions.Resources.StandardContractAndTSName;
        
        if (_obj.ConditionType == ConditionType.SingES)
          return "Подписать ЭП?";
        
        if (_obj.ConditionType == ConditionType.JustPurchase)
        {
          string txt = string.Empty;
          foreach (var justPurchase in _obj.JustificationsPurchasesline)
          {
            txt += justPurchase.JustificationPurchase + ", ";
          }
          txt = txt.Remove(txt.Count() - 2);
          
          return string.Format("Обоснование закупки содержит {0}?", txt);
        }
        
        if (_obj.ConditionType == ConditionType.SumPlan)
          return string.Format("Сумма (план) {0} {1}?", _obj.Info.Properties.SumPlanOperatorsline.GetLocalizedValue(_obj.SumPlanOperatorsline), _obj.SumPlansline);
        
        if (_obj.ConditionType == ConditionType.SumContract)
          return string.Format("Сумма договора (Строка ПП) {0} {1}?", _obj.Info.Properties.AmountOperator.GetLocalizedValue(_obj.AmountOperator), _obj.Amount);
        
        if (_obj.ConditionType == ConditionType.Preagreement)
          return "Требуется предварительное согласование?";
        
        if (_obj.ConditionType == ConditionType.DITagreement)
          return "Требуется согласование ДИТ?";
        
        if (_obj.ConditionType == ConditionType.NZIagreement)
          return "Требуется согласование НЗИ?";
        
        if (_obj.ConditionType == ConditionType.Substitution1C)
          return "Требуется замещение в 1С ДО?";
        
        if (_obj.ConditionType == ConditionType.IsStandard)
          return "Договор расходный типовой?";
        
        if (_obj.ConditionType == ConditionType.IsStandardTS)
          return "Типовое техническое задание?";
        
        if (_obj.ConditionType == ConditionType.DepCompareranorsv)
        {
          string txt = string.Empty;
          foreach (var department in _obj.Departmentsanorsv)
          {
            txt += department.Department.Name + ", ";
          }
          txt = txt.Remove(txt.Count() - 2);
          
          return string.Format("{0} в составе подразделений {1}?", _obj.ApprovalRole.Name, txt);
        }
        
        if (_obj.ConditionType == ConditionType.TopicMemoCompar)
        {
          string txt = string.Empty;
          foreach (var topic in _obj.TopicMemoanorsv)
          {
            txt += topic.TopicMemo.Name + ", ";
          }
          txt = txt.Remove(txt.Count() - 2);
          
          return string.Format("Тематика СЗ - {0}?", txt);
        }
        
        if (_obj.ConditionType == ConditionType.TopicContract)
        {
          string txt = string.Empty;
          foreach (var topic in _obj.Topicanorsv)
          {
            txt += topic.Topic.Name + ", ";
          }
          txt = txt.Remove(txt.Count() - 2);
          return string.Format("Тематика договора - {0}?", txt);
        }
        if (_obj.ConditionType == ConditionType.TopicTS)
        {
          string txt = string.Empty;
          foreach (var topic in _obj.Topicanorsv)
          {
            txt += topic.Topic.Name + ", ";
          }
          txt = txt.Remove(txt.Count() - 2);
          return string.Format("Тематика ТЗ - {0}?", txt);
        }
        
        if (_obj.ConditionType == ConditionType.EmpInRoleInclud)
        {
          string txt = string.Empty;
          foreach (var recipient in _obj.Recipientsanorsv)
          {
            txt += recipient.Recipient.Name + ", ";
          }
          foreach (var appRole in _obj.ApprovalRolesanorsv)
          {
            txt += appRole.Role.Name + ", ";
          }
          txt = txt.Remove(txt.Count() - 2);
          
          return string.Format("{0} в составе исполнителей {1}?", _obj.ApprovalRole.Name, txt);
        }
      }

      return base.GetConditionName();
    }
    
  }
}
