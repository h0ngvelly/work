using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Order;

namespace sline.RSV
{
  partial class OrderDocumentKindSearchPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> DocumentKindSearchDialogFiltering(IQueryable<T> query, Sungero.Domain.PropertySearchDialogFilteringEventArgs e)
    {
      query = base.DocumentKindSearchDialogFiltering(query, e);
      
      // Исключить Приказы о введении в действие ЛНА для всех, кроме Администраторов и Аудиторов
      if (!Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Administrators) && !Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Auditors))
      {
        var orderImplLNAEntityId = anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid;   
        var orderImplLNAExternalLink = Sungero.Docflow.PublicFunctions.Module.GetExternalLink(anorsv.DocflowModule.PublicConstants.Module.DocKinds.DocKind, orderImplLNAEntityId);
        if (orderImplLNAExternalLink != null)
          query = query.Where(k => k.Id != orderImplLNAExternalLink.EntityId);
      }
      
      return query;
    }
  }

  partial class OrderDocumentKindPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> DocumentKindFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.DocumentKindFiltering(query, e);
      
      // Исключить Приказы о введении в действие ЛНА для всех, кроме Администраторов и Аудиторов
      if (!Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Administrators) && !Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Auditors))
      {
        var orderImplLNAEntityId = anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid;   
        var orderImplLNAExternalLink = Sungero.Docflow.PublicFunctions.Module.GetExternalLink(anorsv.DocflowModule.PublicConstants.Module.DocKinds.DocKind, orderImplLNAEntityId);
        if (orderImplLNAExternalLink != null)
          query = query.Where(k => k.Id != orderImplLNAExternalLink.EntityId);
      }
      
      return query;
    }
  }

  partial class OrderORDslinePropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ORDslineFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(x => Equals(_obj.DocumentKind, x.DocumentKind));
      return query;
    }
  }

  partial class OrderServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      _obj.SignESsline = true;
    }
    
    
  }

}