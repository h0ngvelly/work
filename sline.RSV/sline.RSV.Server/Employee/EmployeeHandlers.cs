﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Employee;

namespace sline.RSV
{
  partial class EmployeeServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);      
      _obj.NotUpdateAutomaticallyanorsv = false;
      _obj.Manuallyanorsv = false;
    }
  }

}