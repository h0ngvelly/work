﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Addendum;

namespace sline.RSV
{
  partial class AddendumServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      var leadingDocumentChanged = !Equals(_obj.LeadingDocument, _obj.State.Properties.LeadingDocument.OriginalValue);
      if (leadingDocumentChanged)
      {
        // Проверить, доступен ли для изменения ведущий документ.
        var isLeadingDocumentDisabled = Sungero.Docflow.PublicFunctions.OfficialDocument.NeedDisableLeadingDocument(_obj);
        if (isLeadingDocumentDisabled)
          e.AddError(Sungero.Docflow.OfficialDocuments.Resources.RelationPropertyDisabled);
        
        if (_obj.LeadingDocument != null)
          if (Sungero.Docflow.PublicFunctions.OfficialDocument.IsProjectDocument(_obj.LeadingDocument))
            e.Params.AddOrUpdate(Sungero.Docflow.Constants.OfficialDocument.GrantAccessRightsToProjectDocument, true);
      }  
      
      if (_obj.LeadingDocument != null && leadingDocumentChanged)
      {
        var accessRightsLimit = anorsv.OfficialDocument.PublicFunctions.OfficialDocument.GetAvailableAccessRights(_obj);
        if (accessRightsLimit != Guid.Empty)
          anorsv.OfficialDocument.PublicFunctions.OfficialDocument.CopyAccessRightsToDocument(anorsv.OfficialDocument.OfficialDocuments.As(_obj.LeadingDocument), _obj, accessRightsLimit);
      }
      
      if (_obj.LeadingDocument != null && _obj.LeadingDocument.AccessRights.CanRead() &&
          !_obj.Relations.GetRelatedFrom(Sungero.Docflow.Constants.Module.AddendumRelationName).Contains(_obj.LeadingDocument))
        _obj.Relations.AddFromOrUpdate(Sungero.Docflow.Constants.Module.AddendumRelationName, _obj.State.Properties.LeadingDocument.OriginalValue, _obj.LeadingDocument);
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      if (CallContext.CalledDirectlyFrom(anorsv.OfficialDocument.OfficialDocuments.Info))
      {
        int leadingDocumentId = CallContext.GetCallerEntityId(anorsv.OfficialDocument.OfficialDocuments.Info);
        
        anorsv.OfficialDocument.IOfficialDocument leadingDocument = anorsv.OfficialDocument.OfficialDocuments.GetAll(d => d.Id == leadingDocumentId).SingleOrDefault();
        
        Logger.DebugFormat("Addendum.Created: Created in context anorsv.OfficialDocument.OfficialDocuments with Id = {0}", leadingDocumentId);
        
        if (leadingDocument != null)
        {
          _obj.LeadingDocument = Sungero.Docflow.OfficialDocuments.As(leadingDocument);
          
          foreach (var projectRow in leadingDocument.ProjectsCollectionanorsv)
          {
            _obj.ProjectsCollectionanorsv.AddNew().Project = projectRow.Project;
          }
          
          _obj.ConfidentialDocumentanorsv = leadingDocument.ConfidentialDocumentanorsv;
        }
      }
      
      base.Created(e);
    }

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      anorsv.OfficialDocument.IOfficialDocument leadingDocument =
        _obj.LeadingDocument != null ? anorsv.OfficialDocument.OfficialDocuments.As(_obj.LeadingDocument) : anorsv.OfficialDocument.OfficialDocuments.Null;
      
      // Синхронизируем настройку конфиденциальности с ведущим документом
      if (leadingDocument != null && leadingDocument.ConfidentialDocumentanorsv != _obj.ConfidentialDocumentanorsv)
      {
        _obj.ConfidentialDocumentanorsv = leadingDocument.ConfidentialDocumentanorsv;
      }

      base.Saving(e);
      
      // Синхронизируем проекты в приложении с проектами ведущего документа
      if (leadingDocument != null)
      {
        if (_obj.ProjectsCollectionanorsv.Sum(p => p.Project.Id) != leadingDocument.ProjectsCollectionanorsv.Sum(p => p.Project.Id))
        {
          _obj.ProjectsCollectionanorsv.Clear();
          
          foreach (var sourceProjectRow in leadingDocument.ProjectsCollectionanorsv)
          {
            if (sourceProjectRow.Project != null)
              _obj.ProjectsCollectionanorsv.AddNew().Project = sourceProjectRow.Project;
          }
        }
      }
      
      // Синхронизируем права доступа с ведущим документом
      if (_obj.LeadingDocument != null)
      {
        foreach (var acr in _obj.LeadingDocument.AccessRights.Current)
        {
          if (_obj.AccessRights.CanGrant(acr.AccessRightsType))
            _obj.AccessRights.Grant(acr.Recipient, acr.AccessRightsType);
        }
      }
    }
  }

}