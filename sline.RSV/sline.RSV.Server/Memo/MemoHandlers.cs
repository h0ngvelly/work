﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Memo;

namespace sline.RSV
{
  partial class MemoServerHandlers
  {

    public override void BeforeSaveHistory(Sungero.Content.DocumentHistoryEventArgs e)
    {
      base.BeforeSaveHistory(e);
      
      var isChangeRelationAction = e.Action == Sungero.CoreEntities.History.Action.ChangeRelation;
      if (isChangeRelationAction)
      {
        if (e.OperationDetailed.ToString().Equals(anorsv.RelationModule.PublicConstants.Module.SystemOperations.RemoveRelation))
        {
          // При изменении связи проверить для СЗ по закупке наличие обязательной связи
          var procurementActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ProcurementActivitiesKind);
          if (_obj.DocumentKind.Equals(procurementActivitiesKind))
          {
            if (_obj.LeadingDocument != null && _obj.Purchaseanorsv != null)  
              anorsv.RelationModule.PublicFunctions.Module.CheckSpecialRelations(_obj.Purchaseanorsv.Id, _obj.Id);
          }
        } 
      }
      
    }
  }



  partial class MemoCreatingFromServerHandler
  {

    public override void CreatingFrom(Sungero.Domain.CreatingFromEventArgs e)
    {
      e.Without(_info.Properties.Purchaseanorsv);
      base.CreatingFrom(e);
    }
  }


  partial class MemoDocumentKindSearchPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> DocumentKindSearchDialogFiltering(IQueryable<T> query, Sungero.Domain.PropertySearchDialogFilteringEventArgs e)
    {
      query = base.DocumentKindSearchDialogFiltering(query, e);
      // Служебные записки по закупочной деятельности
      var procurementActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ProcurementActivitiesKind);
      // Отфильтровать внутренние виды документов.
      if (procurementActivitiesKind != null)
        return query.Where(k => k.Id != procurementActivitiesKind.Id );
      else
        return query;
    }
  }

  partial class MemoDocumentKindPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> DocumentKindFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.DocumentKindFiltering(query, e);
      // Служебные записки по закупочной деятельности
      var procurementActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ProcurementActivitiesKind);
      // Отфильтровать внутренние виды документов.
      if (procurementActivitiesKind != null)
        return query.Where(k => k.Id != procurementActivitiesKind.Id );
      else
        return query;
    }
  }




  partial class MemoMemoTopicanorsvPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> MemoTopicanorsvFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(x => Equals(_obj.DocumentKind, x.DocumentKind));
      return query;
    }
  }

  partial class MemoAuthorPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> AuthorFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.AuthorFiltering(query, e);
      var users = Substitutions.ActiveSubstitutedUsers.ToList();
      if (!Users.Current.IncludedIn(Roles.Administrators) && users.Any())
      {
        users.Add(Users.Current);
        query = query.Where(x => users.Contains(Sungero.CoreEntities.Users.As(x)));
      }
      return query;
    }
  }

}