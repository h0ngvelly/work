﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.OutgoingLetter;
using sline.CustomModule;

namespace sline.RSV.Server
{
  partial class OutgoingLetterFunctions
  {
    
    /// <summary>
    /// ИОФ Руководителя.
    /// </summary>
    /// <param name="contractualDocument">Исходящее письмо.</param>
    /// <returns>ИОФ Руководителя.</returns>
    [Converter("IOFHead")]
    public static string IOFHead(IOutgoingLetter outgoingLetter)
    {
      var bchp = outgoingLetter.BCHPDocParamsanorsv;
      var headLastName = bchp.LastName;
      var headFirstName = bchp.FirstName;
      var headMiddleName = bchp.MiddleName;
      var ioFirstName = String.Format("{0}. {1}. {2}", headFirstName.ToArray()[0], headMiddleName.ToArray()[0], headLastName);
      
      return ioFirstName;
    }
    
    /// <summary>
    /// ИО Руководителя.
    /// </summary>
    /// <param name="contractualDocument">Исходящее письмо.</param>
    /// <returns>ИОФ Руководителя.</returns>
    [Converter("IOHead")]
    public static string IOHead(IOutgoingLetter outgoingLetter)
    {
      var bchp = outgoingLetter.BCHPDocParamsanorsv;
      
      var headFirstName = bchp.FirstName;
      var headMiddleName = bchp.MiddleName;
      var ioHead = String.Format("{0} {1}", headFirstName, headMiddleName);
      
      return ioHead;
    }
    
    /// <summary>
    /// Обращение к руководителю.
    /// </summary>
    /// <param name="contractualDocument">Исходящее письмо.</param>
    /// <returns>Уважаемый/Уважаемая</returns>
    [Converter("AppealManager")]
    public static string AppealManager(IOutgoingLetter outgoingLetter)
    {
      var bchp = outgoingLetter.BCHPDocParamsanorsv;
      var appealManager = String.Empty;
      if (bchp.Gender == anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.Gender.Male)
        appealManager = "Уважаемый";
      else
        appealManager = "Уважаемая";
      return appealManager;
    }
    
    /// <summary>
    /// Подготовил.
    /// </summary>
    /// <param name="contractualDocument">Исходящее письмо.</param>
    /// <returns>Подготовил</returns>
    [Converter("Prepared")]
    public static string Prepared(IOutgoingLetter outgoingLetter)
    {
      //Иванов Петр Петрович,
      //руководитель проекта,
      //+7 (495) 198-88-92, petr.ivanov@rsv.ru
      var author = Sungero.Company.Employees.As(outgoingLetter.Author);
      var prepared = String.Empty;
      prepared += author.Name + "," + Environment.NewLine;
      
      if(author.JobTitle != null)
        prepared += author.JobTitle.Name + "," + Environment.NewLine;
      
      var phoneEmail = String.Empty;
      
      if(author.Phone != null)
        phoneEmail += author.Phone + ", ";
      
      if(author.Email != null)
        phoneEmail += author.Email;
      
      prepared += phoneEmail;
      
      return prepared;
    }
    // возвращаем все этапы по регламенту
    [Public]
    public List<IApprovalStage> GetStages(IApprovalRule rule)
    {
      var result = new List<IApprovalStage>() { };
      bool canDefineConditions = false;

      // Вычисление первого этапа.
      var currentStageNumber = rule.Transitions.Select(x => x.SourceStage).FirstOrDefault(s => !rule.Transitions.Any(t => t.TargetStage.Equals(s)));
      
      if (rule.Stages.Any(x => x.Number == currentStageNumber))
      {
        var stage = rule.Stages.Single(s => s.Number == currentStageNumber);
        //var myStage = sline.RSV.ApprovalStages.As(stage);
        
        result.Add(sline.RSV.ApprovalStages.As(stage.Stage)/*, GetApproversFromStage(sline.RSV.ApprovalStages.As(stage))*/);
        canDefineConditions = true;
      }
      
      var nextStageNumber = GetNextStageNumber(rule, currentStageNumber);
      var error = nextStageNumber.Message;
      
      if (nextStageNumber != null)
        canDefineConditions = nextStageNumber.Number != -1;
      
      while (nextStageNumber.Number != null && nextStageNumber.Number >= 0)
      {
        currentStageNumber = nextStageNumber.Number.Value;
        nextStageNumber = GetNextStageNumber(rule, currentStageNumber);
        if (nextStageNumber.Number == -1)
        {
          error = nextStageNumber.Message;
          canDefineConditions = false;
          break;
        }
        
        var stage = rule.Stages.Single(s => s.Number == currentStageNumber);
        result.Add(sline.RSV.ApprovalStages.As(stage.Stage)/*, GetApproversFromStage(sline.RSV.ApprovalStages.As(stage))*/);
      }
      
      Logger.DebugFormat("Done GetStages");
      return result;
    }
 
    // вычисляем, какой следующий этап
    public virtual Sungero.Docflow.Structures.ApprovalRuleBase.NextStageNumber GetNextStageNumber(Sungero.Docflow.IApprovalRule rule, int? currentStageNumber)
    {
      if (rule.Transitions.Any())
      {
        if (currentStageNumber == null)
        {
          // Вычисление первого этапа.
          currentStageNumber = rule.Transitions.Select(x => x.SourceStage).FirstOrDefault(s => !rule.Transitions.Any(t => t.TargetStage == s));
          
          // Если этот этап условный, находим следующий обычный этап.
          if (!rule.Stages.Any(s => s.Number == currentStageNumber))
            return this.GetNextStageNumber(rule, currentStageNumber);
          
          return Sungero.Docflow.Structures.ApprovalRuleBase.NextStageNumber.Create(currentStageNumber, string.Empty);
        }
        
        var nextTransition = rule.Transitions.FirstOrDefault(t => t.SourceStage == currentStageNumber);
        
        if (nextTransition != null)
        {
          var isStage = rule.Stages.Any(s => s.Number == nextTransition.TargetStage);
          if (!isStage)
            return this.GetNextStageNumber(rule, nextTransition.TargetStage.Value);
          
          return nextTransition != null ?
            Sungero.Docflow.Structures.ApprovalRuleBase.NextStageNumber.Create(nextTransition.TargetStage, string.Empty) :
            Sungero.Docflow.Structures.ApprovalRuleBase.NextStageNumber.Create(null, string.Empty);
        }
      }
      else if (rule.Stages.Count == 1)
      {
        var singleNumber = rule.Stages.Single().Number;
        return Sungero.Docflow.Structures.ApprovalRuleBase.NextStageNumber.Create(singleNumber == currentStageNumber ? null : singleNumber, string.Empty);
      }
      
      return Sungero.Docflow.Structures.ApprovalRuleBase.NextStageNumber.Create(null, string.Empty);
    }
    
    // вычисляем исполнителей этапа
    [Public]
    public List<IRecipient> GetApproversFromStage(IApprovalStage stage, sline.RSV.IApprovalRule rule)
    {
      var recipients = new List<IRecipient>();
      // Сотрудники/группы.
      if (stage.Recipients.Any())
        recipients.AddRange(stage.Recipients
                            .Where(rec => rec.Recipient != null)
                            .Select(rec => rec.Recipient)
                            .ToList());
      
      // Роли согласования.
      if (stage.ApprovalRoles.Any())
        recipients.AddRange(stage.ApprovalRoles
                            .Where(r => r.ApprovalRole != null && r.ApprovalRole.Type != Sungero.Docflow.ApprovalRoleBase.Type.Approvers)
                            .Select(r => GetRolePerformerFromDocument(r.ApprovalRole, _obj, rule))
                            .Where(r => r != null)
                            .ToList());
      
      if (stage.ApprovalRole != null)
      {
        recipients.Add(GetRolePerformerFromDocument(stage.ApprovalRole, _obj, rule));
      }
      if (stage.Assignee != null)
      {
        /*if (stage.AssigneeType == sline.RSV.ApprovalStage.AssigneeType.Role)
                    recipients.Add(GetRolePerformerFromDocument(stage.Assignee, _obj, rule));
                else*/
        recipients.Add(stage.Assignee);
      }
      
      return recipients;
    }
    
    public sline.RSV.IEmployee GetRolePerformerFromDocument(Sungero.Docflow.IApprovalRoleBase role, Sungero.Docflow.IOfficialDocument document, sline.RSV.IApprovalRule rule)
    {
      if (role.Type == sline.CustomModule.MyApprovalRoles.Type.PurchRespons)
      {
        var purchaseRequest = PurchaseRequests.As(document);
        if (purchaseRequest != null)
          return purchaseRequest.ResponsibleEmployee;
        else
          return null;
      }
      
      if (role.Type == sline.CustomModule.MyApprovalRoles.Type.PurchaseDZD)
      {
        var purchaseRequest = PurchaseRequests.As(document);
        if (purchaseRequest != null)
          return purchaseRequest.EmployeeDZD;
        else
          return null;
      }
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.DocRegister)
        return this.GetPerformerRegisterFromDocument(document);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.OutDocRegister)
        return this.GetPerformerOutDocumentRegisterFromDocument(document);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.Initiator)
        return this.GetPerformerInitiatorFromDocument(document);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.InitManager)
        return this.GetPerformerInitiatorManagerFromDocument(document);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.PrintResp)
        return this.GetPerformerPrintResponsibleFromDocument(document, rule);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.SignAssistant)
        return this.GetPerformerSignatoryAssistantFromDocument(document);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.Signatory)
        return this.GetPerformerSignatoryFromDocument(document);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.CompResponsible)
        return this.GetPerformerCompanyResponsibleFromDocument(document);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.DepartManager)
        return this.GetPerformerDepartmentManagerFromDocument(document);
      
      if (role.Type == Sungero.Docflow.ApprovalRoleBase.Type.DocDepManager)
        return this.GetPerformerDocumentDepartmentManagerFromDocument(document);
      
      return null;
    }
    
    #region Вычисление ролей для регламента по документу
    
    /// <summary>
    /// Получить регистратора.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerRegisterFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var employee = Sungero.Docflow.PublicFunctions.Module.Remote.GetClerk(document);
      if (employee != null)
        return sline.RSV.Employees.As(employee);
      else
        return sline.RSV.Employees.As(document.Author);
      
    }
    
    /// <summary>
    /// Получить регистратора исходящих документов.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerOutDocumentRegisterFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      if (document.DocumentKind.DocumentFlow == null && document.DocumentRegister != null && document.DocumentRegister.RegistrationGroup != null)
        return sline.RSV.Employees.As(document.DocumentRegister.RegistrationGroup.ResponsibleEmployee);
      
      var department = document.Department;
      if (department == null)
        return null;
      
      var userRegistrationGroup = GetRegistrationGroupByDocument(document);
      
      if (userRegistrationGroup == null)
        return null;
      
      // Ответственный за группу регистрации.
      return sline.RSV.Employees.As(userRegistrationGroup.ResponsibleEmployee);
    }
    public static Sungero.Docflow.IRegistrationGroup GetRegistrationGroupByDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var documentFlow = document.DocumentKind.DocumentFlow;
      
      // Если есть настройка регистрации, учесть это при фильтрации.
      var settingGroup = Sungero.Docflow.RegistrationSettings.GetAll()
        .Where(s => s.SettingType == Sungero.Docflow.RegistrationSetting.SettingType.Registration &&
               s.Status == Sungero.CoreEntities.DatabookEntry.Status.Active && Equals(s.DocumentFlow, documentFlow) &&
               (!s.DocumentKinds.Any() || s.DocumentKinds.Any(k => Equals(k.DocumentKind, document.DocumentKind))) &&
               (!s.BusinessUnits.Any() || s.BusinessUnits.Any(u => Equals(u.BusinessUnit, document.BusinessUnit))) &&
               (!s.Departments.Any() || s.Departments.Any(d => Equals(d.Department, document.Department))))
        .Select(s => s.DocumentRegister.RegistrationGroup)
        .Where(s => !s.Departments.Any() || s.Departments.Any(d => Equals(d.Department, document.Department)))
        .FirstOrDefault();
      if (settingGroup != null)
        return settingGroup;

      var regGroups = Sungero.Docflow.RegistrationGroups.GetAll(g => g.Status == Sungero.CoreEntities.DatabookEntry.Status.Active);

      if (documentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Contracts)
        regGroups = regGroups.Where(g => g.CanRegisterContractual == true);
      else if (documentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Incoming)
        regGroups = regGroups.Where(g => g.CanRegisterIncoming == true);
      else if (documentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Inner)
        regGroups = regGroups.Where(g => g.CanRegisterInternal == true);
      else if (documentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Outgoing)
        regGroups = regGroups.Where(g => g.CanRegisterOutgoing == true);
      
      // Получить группы регистрации, обслуживающие это подразделение.
      var departmentRegGroup = regGroups.Where(g => g.Departments.Any(d => Equals(d.Department, document.Department))).FirstOrDefault();
      if (departmentRegGroup != null)
        return departmentRegGroup;
      
      // Если таких групп регистрации нет, то выбрать те, которые обслуживают все подразделения.
      return regGroups.FirstOrDefault(g => !g.Departments.Any());
    }
    
    /// <summary>
    /// Получить инициатора согласования (обычно - автор документа).
    /// </summary>
    private sline.RSV.IEmployee GetPerformerInitiatorFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      return sline.RSV.Employees.As(document.Author);
    }
    
    /// <summary>
    /// Получить руководителя автора документа.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerInitiatorManagerFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var manager = Sungero.Docflow.PublicFunctions.Module.Remote.GetManager(document.Author);
      return sline.RSV.Employees.As(manager);
    }
    
    /// <summary>
    /// Получить руководителя подразделения автора документа согласования.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerDepartmentManagerFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var employee = sline.RSV.Employees.GetAll().FirstOrDefault(u => Equals(u, document.Author));
      
      if (employee == null)
        return null;
      
      var department = employee.Department;
      if (sline.RSV.Departments.Is(department))
        return department != null ? department.Manager : null;
      else
        return department != null ? sline.RSV.Employees.As(department.Manager) : null;
    }
    
    /// <summary>
    /// Получить руководителя подразделения, указанного в документе.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerDocumentDepartmentManagerFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var manager = document != null && document.Department != null ? sline.RSV.Employees.As(document.Department.Manager) : null;
      return manager == null ? this.GetPerformerDepartmentManagerFromDocument(document) : manager;
    }
    
    /// <summary>
    /// Получить сотрудника, ответственного за печать.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerPrintResponsibleFromDocument(Sungero.Docflow.IOfficialDocument document, sline.RSV.IApprovalRule rule)
    {
      //var stages = GetStages(rule);
      
      var reviewStageIndex = GetIndexOfStage(Sungero.Docflow.ApprovalStage.StageType.Review, rule);
      var signStageIndex = GetIndexOfStage(Sungero.Docflow.ApprovalStage.StageType.Sign, rule);
      var printStageIndex = GetIndexOfStage(Sungero.Docflow.ApprovalStage.StageType.Print, rule);
      
      // Для печати перед подписанием или рассмотрением исполнителем взять помощника.
      var performer = Sungero.Company.Employees.Null;
      if (printStageIndex < signStageIndex)
      {
        var manager = Sungero.Company.Employees.As(GetPerformerSignatoryFromDocument(document));
        performer = Sungero.Company.ManagersAssistants
          .GetAllCached(m => manager.Equals(m.Manager))
          .Where(m => m.Status == Sungero.CoreEntities.DatabookEntry.Status.Active && m.Assistant.Status == Sungero.CoreEntities.DatabookEntry.Status.Active)
          .Select(m => m.Assistant)
          .FirstOrDefault();
      }
      
      if (performer == null)
        performer = Sungero.Docflow.PublicFunctions.Module.Remote.GetClerk(document);
      
      return sline.RSV.Employees.As(performer) ?? this.GetPerformerInitiatorFromDocument(document);
    }
    public int GetIndexOfStage(Enumeration type, sline.RSV.IApprovalRule rule)
    {
      var stages = GetStages(rule);
      var stage = stages.Where(s => s.StageType == type).FirstOrDefault();
      return stages.IndexOf(stage);
    }

    /// <summary>
    /// Получить подписывающего.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerSignatoryFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      return sline.RSV.Employees.As(document.OurSignatory);
    }
    /// <summary>
    /// Получить помощника подписывающего.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerSignatoryAssistantFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var signatory = Sungero.Company.Employees.As(this.GetPerformerSignatoryFromDocument(document));
      var secretary = Sungero.Company.ManagersAssistants
        .GetAllCached(m => signatory.Equals(m.Manager))
        .Where(m => m.Status == Sungero.CoreEntities.DatabookEntry.Status.Active && m.Assistant.Status == Sungero.CoreEntities.DatabookEntry.Status.Active)
        .Select(m => m.Assistant)
        .FirstOrDefault();
      return sline.RSV.Employees.As(secretary) ?? sline.RSV.Employees.As(signatory);
    }
    
    /// <summary>
    /// Получить ответственного за контрагента.
    /// </summary>
    private sline.RSV.IEmployee GetPerformerCompanyResponsibleFromDocument(Sungero.Docflow.IOfficialDocument document)
    {
      var performer = this.GetPerformerInitiatorFromDocument(document);
      if (document != null)
      {
        var counterparties = Sungero.Docflow.PublicFunctions.OfficialDocument.GetCounterparties(document);
        if (counterparties != null && counterparties.Count == 1)
        {
          var company = Sungero.Parties.Companies.As(counterparties.FirstOrDefault());
          if (company != null && company.Responsible != null)
            performer = sline.RSV.Employees.As(company.Responsible);
        }
      }
      
      return performer;
    }
    
    #endregion
    
  }
}