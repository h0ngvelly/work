﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.OutgoingLetter;
using sline.RSV;

namespace sline.RSV
{
  partial class OutgoingLetterDeliveryMethodPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> DeliveryMethodFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.DeliveryMethodFiltering(query, e);
      return query;
    }
  }


  partial class OutgoingLetterAnyApproversslineStagePropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> AnyApproversslineStageFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      var stages = _root.ReqApproverssline.Select(x => x.Stage);
      query = query.Where(x => stages.Contains(x));
      return query;
    }
  }

  partial class OutgoingLetterServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      // Получим регламент
      var approvalRule = Sungero.Docflow.PublicFunctions.OfficialDocument.Remote.GetApprovalRules(_obj).FirstOrDefault();
      if (approvalRule != null)
      {
        var rule = ApprovalRules.As(approvalRule);
        UpdateReglamentApprovers(rule);
      }
      else
      {
        _obj.ReqApproverssline.Clear();
      }
      
      base.BeforeSave(e);
    }
    
    public void UpdateReglamentApprovers(IApprovalRule rule)
    {
      var stages = sline.RSV.PublicFunctions.OutgoingLetter.GetStages(_obj, rule);
      _obj.ReqApproverssline.Clear();
      foreach(var stage in stages)
      {
        var approvers = sline.RSV.PublicFunctions.OutgoingLetter.GetApproversFromStage(_obj, stage, rule);
        foreach (var approver in approvers)
        {
          var n = _obj.ReqApproverssline.AddNew();
          n.Stage = stage;
          n.Approver = sline.RSV.Employees.As(approver);
        }
      }
    }
    
    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      _obj.SignESsline = true;
    }
  }

}