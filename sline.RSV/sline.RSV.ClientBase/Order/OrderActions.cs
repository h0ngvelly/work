﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Order;
using docRelation = anorsv.RelationModule;

namespace sline.RSV.Client
{

  partial class OrderActions
  {
    public override void DeleteRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Только для Приказа о введении в действие ЛНА по общей деятельности организации
      //      var orderImplLNAKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
      //        if (orderImplLNAKind != null && _obj.DocumentKind.Equals(orderImplLNAKind))
      //        {
      //        var availableTypeList = RelationTypes.GetAll()
      //          .Where(r => r.Name == anorsv.DocflowModule.PublicConstants.Module.PutIntoActionRelationName)
      //          .ToList();
      //        docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(_obj, availableTypeList);
      //      }
      //      else
      //      {
      base.DeleteRelationanorsv(e);
      //      }
    }

    public override bool CanDeleteRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanDeleteRelationanorsv(e);
    }



    public override void CreateManyAddendum(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.CreateManyAddendum(e);
    }

    public override bool CanCreateManyAddendum(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true; //base.CanCreateManyAddendum(e);
    }

    public override void SendForApproval(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var createdTasks = sline.CustomModule.PublicFunctions.Module.GetApprovalTasks(_obj);
      
      if (createdTasks)
      {
        e.AddError("Документ уже отправлен на согласование по регламенту!");
        return;
      }
      else
      {
        // Приказ о введении в действие ЛНА по общей деятельности организации, связанный с ЛНА связью Вводит в действие
        var orderImplLNAKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.OrderLNAImplKindGuid);
        if (orderImplLNAKind != null && _obj.DocumentKind.Equals(orderImplLNAKind))
        {
          e.AddError(sline.RSV.Orders.Resources.ApprovalFromLNAText);
          return;
        }
        
        base.SendForApproval(e);
      }
    }

    public override bool CanSendForApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForApproval(e);
    }

  }

}