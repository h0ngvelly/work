﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Order;

namespace sline.RSV
{
  partial class OrderClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      bool res1 = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = res1;
      
      bool canModifyDocKind = true;
      if (e.Params.Contains("AnoRsvCanModifyDocKind"))
        e.Params.TryGetValue("AnoRsvCanModifyDocKind", out canModifyDocKind);
      _obj.State.Properties.DocumentKind.IsEnabled = canModifyDocKind;
      
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      // Для ЛНА по общ.деятельности Тематика (переименована в Направление деятельности) обязательная для заполнения
      // TODO MED: Убрать после полной конвертации ЛНА в новый тип
      var lnaGeneralActivityKind = Sungero.Docflow.DocumentKinds.GetAll(k => k.Name.Equals("Локальный нормативный акт по общей деятельности")).FirstOrDefault();
      if (lnaGeneralActivityKind != null)
        _obj.State.Properties.ORDsline.IsRequired = (_obj.DocumentKind.Equals(lnaGeneralActivityKind));
      
      bool canModifyDocKind = anorsv.DocflowModule.PublicFunctions.Module.Remote.CanChangeDocumentKind(_obj);
      e.Params.AddOrUpdate("AnoRsvCanModifyDocKind", canModifyDocKind);
      _obj.State.Properties.DocumentKind.IsEnabled = canModifyDocKind;
      
    }

  }
}