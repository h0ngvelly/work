using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Addendum;

namespace sline.RSV
{
  partial class AddendumClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);

      _obj.State.Properties.ProjectsCollectionanorsv.IsEnabled = false;
    }

  }
}