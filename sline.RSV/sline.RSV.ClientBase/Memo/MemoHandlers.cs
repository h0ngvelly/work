using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Memo;

namespace sline.RSV
{
  partial class MemoClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      // Указание подписанта обязательно для если явно не указано обратное в настроках тематики
      _obj.State.Properties.OurSignatory.IsRequired = _obj.MemoTopicanorsv == null || _obj.MemoTopicanorsv.OurSignatory != sline.CustomModule.TopicMemo.OurSignatory.NotRequired;
      
      // Отобразить реквизит Закупка, если Служебные записки по закупочной деятельности 
      var procurementActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ProcurementActivitiesKind);
      if (_obj.DocumentKind != null && _obj.DocumentKind.Equals(procurementActivitiesKind))
      {
        _obj.State.Properties.Purchaseanorsv.IsVisible = true;
        _obj.State.Properties.DocumentKind.IsEnabled = false;
        _obj.State.Properties.MemoTopicanorsv.IsEnabled = false;
        _obj.State.Properties.Topicanorsv.IsEnabled = false;
        // Предупреждение о невозможности изменения связей с Закупкой из карточки СЗ
        if (_obj.Purchaseanorsv == null)
          e.AddInformation(anorsv.RelationModule.Resources.RelationProhibitionForMemoPurchase);
      }
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      bool res1 = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = res1;
      
      // Отобразить реквизит Закупка, если Служебные записки по закупочной деятельности 
      var procurementActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.ProcurementActivitiesKind);
      if (_obj.DocumentKind != null && _obj.DocumentKind.Equals(procurementActivitiesKind))
      {
        _obj.State.Properties.Purchaseanorsv.IsVisible = true;
        _obj.State.Properties.DocumentKind.IsEnabled = false;
        _obj.State.Properties.MemoTopicanorsv.IsEnabled = false;
        _obj.State.Properties.Topicanorsv.IsEnabled = false;
        // Предупреждение о невозможности изменения связей с Закупкой из карточки СЗ
        if (_obj.Purchaseanorsv == null)
          e.AddInformation(anorsv.RelationModule.Resources.RelationProhibitionForMemoPurchase);
      }
      
      //Скрыть поле "Подписал", если вид документа Служебная записка в ДИТ
      if (_obj.DocumentKind == Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(sline.CustomModule.PublicConstants.Module.DocKind.memoDITKind))
      {
        _obj.State.Properties.OurSignatory.IsRequired = false;
        _obj.State.Properties.OurSignatory.IsVisible = false;
      }
      else
      {
        _obj.State.Properties.OurSignatory.IsRequired = true;
        _obj.State.Properties.OurSignatory.IsVisible = true;
      }
    }

  }
}
