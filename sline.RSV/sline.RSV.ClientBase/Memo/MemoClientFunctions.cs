﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Memo;

namespace sline.RSV.Client
{
  partial class MemoFunctions
  {
    #region Выбор шаблона
    
    /// <summary>
    /// Выбор шаблона с фильтрацией по Тематикам
    /// </summary>
    /// <param name="document">Документ.</param>
    public static void SelectDocumentTemplate(sline.RSV.IMemo document)
    {
      if (document != null)
      {
        if (document.MemoTopicanorsv != null)
        {
          var templateList = new List<Sungero.Docflow.IDocumentTemplate>();
          foreach (var templateStr in document.MemoTopicanorsv.Templates)
            templateList.Add(templateStr.Template);
            
          var dialog = Dialogs.CreateInputDialog(sline.RSV.Memos.Resources.ChooseTemplateDialogTitle);
          dialog.Text = sline.RSV.Memos.Resources.ChooseTemplateDialogText;
            
          var template = dialog.AddSelect("Шаблон документа", true, templateList.FirstOrDefault()).From(templateList);
            
          if (dialog.Show() == DialogButtons.Ok)
          {
            var chosedTemplate = Sungero.Content.ElectronicDocumentTemplates.As(template.Value);
            using (var body = chosedTemplate.LastVersion.Body.Read())
            {        
                var newVersion = document.CreateVersionFrom(body, chosedTemplate.AssociatedApplication.Extension);
                var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)document;
                internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = chosedTemplate.Id;
                document.Save();
            }  
          }
          
        }
        else
        {
          return;
        }
      }
      
    }
    
    #endregion
    
  }
}