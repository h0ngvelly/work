﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.OutgoingLetter;

namespace sline.RSV
{
  partial class OutgoingLetterClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      var clerks = Sungero.Docflow.PublicFunctions.DocumentRegister.Remote.GetClerks();
      if (clerks != null)
      {
        if (Users.Current.IncludedIn(clerks))
        {
          _obj.State.Properties.ReqApproverssline.IsEnabled = true;
          _obj.State.Properties.AnyApproverssline.IsEnabled = true;
        }
        else
        {
          _obj.State.Properties.ReqApproverssline.IsEnabled = false;
          _obj.State.Properties.AnyApproverssline.IsEnabled = false;
        }
      }
      else
      {
        _obj.State.Properties.ReqApproverssline.IsEnabled = false;
        _obj.State.Properties.AnyApproverssline.IsEnabled = false;
      }
    }

  }
}