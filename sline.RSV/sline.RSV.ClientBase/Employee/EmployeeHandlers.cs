﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Employee;

namespace sline.RSV
{
  partial class EmployeeClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      // Отображать <Обновлять автоматически>, <Отображать в шаблоне>, <Указать вручную> только для администратора
      var isAdmin = Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Administrators);
      _obj.State.Properties.TemplateJobanorsv.IsVisible = isAdmin;
      _obj.State.Properties.NotUpdateAutomaticallyanorsv.IsVisible = isAdmin;
      _obj.State.Properties.Manuallyanorsv.IsVisible = isAdmin;
      
      // Если Указать вручную = True, дать изменять администратору значение <Отображать в шаблоне>
      _obj.State.Properties.TemplateJobanorsv.IsEnabled = isAdmin && (_obj.Manuallyanorsv == true);
      
      base.Showing(e);
    }
  }

}