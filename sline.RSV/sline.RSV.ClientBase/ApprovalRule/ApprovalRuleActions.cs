﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalRule;

namespace sline.RSV.Client
{
  partial class ApprovalRuleActions
  {
    public virtual void CreateApprovalTaskAccessRightsRuleanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var rule = anorsv.DocflowModule.PublicFunctions.Module.Remote.CreateApprovalTaskAccessRightsRule(_obj);
      rule.Show();
    }

    public virtual bool CanCreateApprovalTaskAccessRightsRuleanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public virtual void GetApprovalTaskAccessRightsRuleanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var rule = anorsv.DocflowModule.PublicFunctions.Module.Remote.GetApprovalTaskAccessRightsRule(_obj);
      rule.Show();
      
      
    }

    public virtual bool CanGetApprovalTaskAccessRightsRuleanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}