﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalRule;

namespace sline.RSV
{
  partial class ApprovalRuleClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      //Проверка и информирование блока без финальной проверки
      if(_obj.Stages.Any())
      {
        var skipStage = _obj.Stages.Where(s => sline.RSV.ApprovalStages.As(s.Stage)?.SkipStageWithoutFinalVerificationanorsv == true);
        var agreeWithoutStage = _obj.Stages.Where(s => sline.RSV.ApprovalStages.As(s.Stage)?.AgreeWithoutFinalVerificationanorsv == true);
        if(skipStage != null && agreeWithoutStage != null)
        {
          if(!skipStage.Any() && agreeWithoutStage.Any())
            e.AddError(sline.RSV.ApprovalRules.Resources.NoStageInApprovalWithoutFinalVerification);
          else if(skipStage.Any() && !agreeWithoutStage.Any())
            e.AddError(sline.RSV.ApprovalRules.Resources.NoStageSkipInApprovalWithoutFinalVerification);
        }
        
        if (anorsv.DocflowModule.ApprovalTaskAccessRightsRules.AccessRights.CanCreate(Users.Current))
        {
          var rightRule = anorsv.DocflowModule.PublicFunctions.Module.Remote.GetApprovalTaskAccessRightsRule(_obj);
          if (rightRule != null)
          {
            if(!rightRule.IsAccess.Value)
              e.AddInformation(sline.RSV.ApprovalRules.Resources.ConfigureApprovalRuleAccessRight, _obj.Info.Actions.GetApprovalTaskAccessRightsRuleanorsv);
          }
          else
            e.AddInformation(sline.RSV.ApprovalRules.Resources.NotApprovalRuleAccessRights, _obj.Info.Actions.CreateApprovalTaskAccessRightsRuleanorsv);
        }
      }
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
    }

  }
}