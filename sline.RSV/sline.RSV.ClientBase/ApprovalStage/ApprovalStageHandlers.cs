﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.ApprovalStage;

namespace sline.RSV
{
  partial class ApprovalStageClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      _obj.State.Properties.CreateApprovalSheetsline.IsVisible = _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Sign;
//      _obj.State.Properties.BCHPanorsv.IsVisible = _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Notice;
      _obj.State.Properties.AgreeWithoutFinalVerificationanorsv.IsVisible = _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers;        
      _obj.State.Properties.SkipStageWithoutFinalVerificationanorsv.IsVisible = _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers
        || _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.SimpleAgr;
      
      if (_obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers)
      {
        _obj.State.Properties.ZGDsline.IsVisible = true;
        _obj.State.Properties.PreliminaryApprovalBlockanorsv.IsVisible = true;        
      }
      else
      {
        _obj.State.Properties.ZGDsline.IsVisible = false;
        _obj.ZGDsline = false;
        _obj.State.Properties.PreliminaryApprovalBlockanorsv.IsVisible = false;
        _obj.PreliminaryApprovalBlockanorsv = false;
      }

      base.Refresh(e);
    }

  }
}