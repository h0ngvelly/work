﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using sline.RSV.Condition;

namespace sline.RSV
{
  partial class ConditionClientHandlers
  {

    public virtual void SubdepartmentanorsvValueInput(Sungero.Presentation.BooleanValueInputEventArgs e)
    {
      // Заблокировать исключение подразделений из списка, если условие вычисляется без учета нижестоящих подразделений
      if (e.NewValue == true)
      {
        _obj.State.Properties.ExcludeDepartmentsanorsv.IsEnabled = true;
        _obj.State.Properties.ExcludeSubdepartmentsanorsv.IsEnabled = true;
      }
      else
      {
        _obj.ExcludeDepartmentsanorsv.Clear();
        _obj.State.Properties.ExcludeDepartmentsanorsv.IsEnabled = false;
        _obj.ExcludeSubdepartmentsanorsv = false;
        _obj.State.Properties.ExcludeSubdepartmentsanorsv.IsEnabled = false;
      }
      
    }

  }
}