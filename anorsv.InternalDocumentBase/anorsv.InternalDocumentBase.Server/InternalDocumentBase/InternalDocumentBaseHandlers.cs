﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.InternalDocumentBase.InternalDocumentBase;

namespace anorsv.InternalDocumentBase
{
  partial class InternalDocumentBaseFilteringServerHandler<T>
  {

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      query = base.Filtering(query, e);
      
      if (_filter == null)
        return query;
      
      if (_filter.Employee != null)
        query = query.Where(q => Sungero.Docflow.Memos.Is(q) && sline.RSV.Memos.As(q).Employeeanorsv.Equals(_filter.Employee));
      
      if (_filter.Subdivision != null)
        query = query.Where(q => Sungero.Docflow.Memos.Is(q) && sline.RSV.Memos.As(q).Subdivisionanorsv.Equals(_filter.Subdivision));
      
      return query;
    }
  }

}