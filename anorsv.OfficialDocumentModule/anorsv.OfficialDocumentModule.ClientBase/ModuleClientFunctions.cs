﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Shared;
using Sungero.Metadata;
using docRelation = anorsv.RelationModule;

namespace anorsv.OfficialDocumentModule.Client
{
  public class ModuleFunctions
  {   
    /// <summary>
    /// Диалог массового добавления приложений из файлов.
    /// </summary>
    /// <param name="document">Документ, для которого создаются приложения.</param>
    [Public]
    public virtual void AddManyAddendumDialog(Sungero.Docflow.IOfficialDocument document)
    {
      var dialog = Dialogs.CreateInputDialog(Sungero.Docflow.OfficialDocuments.Resources.ManyAddendumDialogTitle);
      //dialog.Text = Sungero.Docflow.OfficialDocuments.Resources.ManyAddendumDialogText;
      dialog.Text = anorsv.OfficialDocument.OfficialDocuments.Resources.ManyAddendumDialogText;
      if (ClientApplication.ApplicationType == ApplicationType.Web)
        dialog.Width = 400;
      
      var fileSelectors = new List<CommonLibrary.IFileSelectDialogValue>();
      for (int i = 0; i < Constants.Module.ManyAddendumDialogLimit; i++)
      {
        var fileSelector = dialog.AddFileSelect(string.Format("{0}.", i + 1), false);
        fileSelector.IsVisible = i == 0;
        fileSelector.MaxFileSize(Constants.Module.ManyAddendumDialogMaxFileSize);
        fileSelectors.Add(fileSelector);
      }
      var importButton = dialog.Buttons.AddCustom(Sungero.Docflow.OfficialDocuments.Resources.AddendumAddFiles);
      var cancelButton = dialog.Buttons.AddCustom(Sungero.Docflow.Resources.Dialog_Close);
      
      var isImportingAddenda = false;
      var addendaCreatedSuccessfully = true;
      var successfullyCreatedAddendaCount = 0;
      var errorList = new List<string>();
      var errorFileSelectors = new List<CommonLibrary.IFileSelectDialogValue>();
      
      Action<CommonLibrary.InputDialogRefreshEventArgs> refresh = (b) =>
      {
        if (isImportingAddenda)
        {
          importButton.IsVisible = false;
          cancelButton.Name = Sungero.Docflow.Resources.Dialog_Close;
          foreach (var fileSelector in fileSelectors)
          {
            fileSelector.IsEnabled = false;
            if (fileSelector.Value == null || fileSelector.Value.Name == string.Empty)
              fileSelector.IsVisible = false;
          }
        }
        else
        {
          cancelButton.Name = Sungero.Docflow.Resources.ExportDialog_Cancel;
          var lastVisibleFileSelector = fileSelectors.Last(f => f.IsVisible);
          if (lastVisibleFileSelector.Value != null &&
              lastVisibleFileSelector.Value.Name != string.Empty &&
              fileSelectors.Any(f => !f.IsVisible))
            fileSelectors.FirstOrDefault(f => !f.IsVisible).IsVisible = true;
          
          foreach (var fileSelector in fileSelectors.Where(f => f.Value != null && f.Value.Name != string.Empty))
          {
            var equalFileSelectors = fileSelectors.Where(f => f.Value != null && f.Value.Name != string.Empty)
              .Where(f => !Equals(f, fileSelector) && Equals(f.Value.Name, fileSelector.Value.Name)).ToList();
            if (equalFileSelectors.Any())
            {
              equalFileSelectors.Add(fileSelector);
              b.AddError(Sungero.Docflow.Resources.ErrorEqualsFileAddManyAddendums, equalFileSelectors.ToArray());
            }
          }
          
          if (!fileSelectors.Any(f => f.Value != null && f.Value.Name != string.Empty))
            importButton.IsEnabled = false;
          else
            importButton.IsEnabled = true;
        }
        
      };
      dialog.SetOnRefresh(refresh);
      dialog.SetOnButtonClick(
        b =>
        {
          if (b.Button == cancelButton)
            return;
          if (b.Button == importButton && b.IsValid)
          {
            try
            {
              if (isImportingAddenda)
                return;

              if (!b.IsValid)
                return;
              
              isImportingAddenda = true;
              
              foreach (var fileSelector in fileSelectors.Where(f => f.Value != null && f.Value.Name != string.Empty))
              {
                try
                {
                  Logger.DebugFormat("AddManyAddendumDialog: Before creating addendum with LeadingDocumentId = {0}", document.Id);
                  CreateAddendum(fileSelector.Value.Name, document, fileSelector.Value.Content);
                  Logger.DebugFormat("AddManyAddendumDialog: after creating addendum with LeadingDocumentId = {0}", document.Id);
                  successfullyCreatedAddendaCount++;
                }
                catch (AppliedCodeException ae)
                {
                  errorFileSelectors.Add(fileSelector);
                  errorList.Add(ae.Message);
                  addendaCreatedSuccessfully = false;
                }
                catch (Sungero.Domain.Shared.Validation.ValidationException ex)
                {
                  Logger.Error(ex.Message, ex);
                  errorFileSelectors.Add(fileSelector);
                  errorList.Add(ex.Message);
                  addendaCreatedSuccessfully = false;
                }
                catch (Exception ex)
                {
                  Logger.Error(ex.Message, ex);
                  errorFileSelectors.Add(fileSelector);
                  errorList.Add(Sungero.Docflow.Resources.InternalServerError);
                  addendaCreatedSuccessfully = false;
                }
              }
              
              if (addendaCreatedSuccessfully && successfullyCreatedAddendaCount > 0)
                Dialogs.NotifyMessage(Functions.Module.GetManyAddendumDialogSuccessfulNotify(successfullyCreatedAddendaCount));
              else
              {
                var errorMessage = string.Empty;
                if (successfullyCreatedAddendaCount > 0)
                  errorMessage += Functions.Module.GetManyAddendumDialogSuccessfulNotify(successfullyCreatedAddendaCount);

                errorMessage += Sungero.Docflow.Resources
                  .ErrorAddManyAddendumsFormat(successfullyCreatedAddendaCount > 0 ?
                                               Sungero.Docflow.Resources.ErrorAddManyAddendumsOther : string.Empty);
                foreach (var error in errorList.Distinct())
                {
                  errorMessage += string.Format(Sungero.Docflow.Resources.ErrorList, error);
                }
                b.AddError(errorMessage, errorFileSelectors.ToArray());
                refresh.Invoke(null);
              }
            }
            catch (AppliedCodeException ae)
            {
              b.AddError(ae.Message);
            }
            catch (Exception ex)
            {
              Logger.Error(ex.Message, ex);
              b.AddError(Sungero.Docflow.Resources.InternalServerError);
            }
          }
        });
      dialog.Show();
    }
    
    /// <summary>
    /// Создать приложение к документу.
    /// </summary>
    /// <param name="addendumName">Имя документа.</param>
    /// <param name="leadingDocument">Ведущий документ.</param>
    /// <param name="addendumContent">Тело документа.</param>
    public virtual void CreateAddendum(string addendumName, Sungero.Docflow.IOfficialDocument leadingDocument, byte[] addendumContent)
    {
      var addendum = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.AddendumCreate();
      addendum.LeadingDocument = leadingDocument;
      if (addendum.State.Properties.Name.IsEnabled)
        addendum.Name = System.IO.Path.GetFileNameWithoutExtension(addendumName);
      else
        addendum.Subject = System.IO.Path.GetFileNameWithoutExtension(addendumName);
      if (addendum.DocumentKind == null)
        addendum.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetAvailableDocumentKinds(typeof(Sungero.Docflow.IAddendum)).FirstOrDefault();
      
      using (var fileStream = new System.IO.MemoryStream(addendumContent))
      {
        addendum.CreateVersionFrom(fileStream, System.IO.Path.GetExtension(addendumName));
        addendum.Save();
        Logger.DebugFormat("CreateAddendum: After creating addendum with LeaddingDocumentId = {0}, AddendumId = {1}", leadingDocument.Id, addendum.Id);
      }

      return;
    }

    /// <summary>
    /// Получить сообщение об успешном создании приложений из файлов.
    /// </summary>
    /// <param name="addendaCount">Количество приложений.</param>
    /// <returns>Сообщение об успешном создании приложений из файлов.</returns>
    public virtual string GetManyAddendumDialogSuccessfulNotify(int addendaCount)
    {
      var addendumName = Sungero.Docflow.Resources.AddendumNameForOneDocument;
      if (addendaCount > 1 && addendaCount < 5)
        addendumName = Sungero.Docflow.Resources.AddendumNameLessFiveDocument;
      else if (addendaCount >= 5)
        addendumName = Sungero.Docflow.Resources.AddendumNameForManyDocument;
      
      return Sungero.Docflow.OfficialDocuments.Resources.AddendaCreatedSuccesfullyFormat(addendaCount, addendumName);
    }
    
    #region Регистрация
    
    /// <summary>
    /// Вызвать диалог регистрации.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="dialogParams">Параметры диалога.</param>
    /// <returns>Результаты диалога регистрации.</returns>
    [Public]
    public static anorsv.OfficialDocumentModule.Structures.Module.IDialogResultMod RunRegistrationDialoganorsv(anorsv.OfficialDocument.IOfficialDocument document, anorsv.OfficialDocumentModule.Structures.Module.IDialogParamsMod dialogParams)
    {
      var leadDocumentId = dialogParams.LeadId;
      var leadDocumentNumber = dialogParams.LeadNumber;
      var currentRegistrationNumber = dialogParams.CurrentRegistrationNumber;
      var hasCurrentNumber = !string.IsNullOrWhiteSpace(currentRegistrationNumber);
      var currentRegistrationDate = dialogParams.CurrentRegistrationDate;
      var defaultDate = currentRegistrationDate.HasValue ? currentRegistrationDate.Value : Calendar.UserToday;
      var numberValidationDisabled = dialogParams.IsNumberValidationDisabled;
      var departmentId = dialogParams.DepartmentId;
      var departmentCode = dialogParams.DepartmentCode;
      var businessUnitCode = dialogParams.BusinessUnitCode;
      var businessUnitId = dialogParams.BusinessUnitId;
      var docKindCode = dialogParams.DocKindCode;
      var caseFileIndex = dialogParams.CaseFileIndex;
      var isClerk = dialogParams.IsClerk;
      var operation = dialogParams.Operation;
      var counterpartyCode = dialogParams.CounterpartyCode;
      
      var buttonName = string.Empty;
      var dialogTitle = string.Empty;
      var registrationNumberLabel = string.Empty;
      var helpCode = string.Empty;
      if (Equals(operation.Value, Sungero.Docflow.RegistrationSetting.SettingType.Reservation.Value))
      {
        buttonName = Sungero.Docflow.OfficialDocuments.Resources.ReservationButtonName;
        dialogTitle = Sungero.Docflow.OfficialDocuments.Resources.ReservationTitle;
        registrationNumberLabel = Sungero.Docflow.OfficialDocuments.Resources.RegistrationNumber;
        helpCode = anorsv.OfficialDocumentModule.Constants.Module.HelpCode.Reservation;
      }
      else if (Equals(operation.Value, Sungero.Docflow.RegistrationSetting.SettingType.Numeration.Value))
      {
        buttonName = Sungero.Docflow.OfficialDocuments.Resources.AlternativeOkButtonName;
        dialogTitle = Sungero.Docflow.OfficialDocuments.Resources.AlternativeTitle;
        registrationNumberLabel = Sungero.Docflow.OfficialDocuments.Resources.AlternativeRegistrationNumber;
        helpCode = anorsv.OfficialDocumentModule.Constants.Module.HelpCode.Numeration;
      }
      else
      {
        buttonName = Sungero.Docflow.OfficialDocuments.Resources.RegistrationButtonName;
        dialogTitle = Sungero.Docflow.OfficialDocuments.Resources.RegistrationTitle;
        registrationNumberLabel = Sungero.Docflow.OfficialDocuments.Resources.RegistrationNumber;
        helpCode = anorsv.OfficialDocumentModule.Constants.Module.HelpCode.Registration;
      }
      
      var dialog = Dialogs.CreateInputDialog(dialogTitle);
      dialog.HelpCode = helpCode;
      // Добавить в диалог выбор Дела
      var caseFileList = anorsv.AnorsvMainSolution.CaseFiles.GetAll().Where(x => Equals(x.BusinessUnit, document.BusinessUnit) || x.BusinessUnit == null).ToList();
      var caseFile = dialog.AddSelect(anorsv.OfficialDocument.OfficialDocuments.Resources.DialogRegistrationCaseFile, true, dialogParams.DefaultCaseFile).From(caseFileList);
      var register = dialog.AddSelect( Sungero.Docflow.OfficialDocuments.Resources.DialogRegistrationLog, true, dialogParams.DefaultRegister).From(dialogParams.Registers);
      var date = dialog.AddDate( Sungero.Docflow.OfficialDocuments.Resources.DialogRegistrationDate, true, defaultDate);
      var isManual = dialog.AddBoolean( Sungero.Docflow.OfficialDocuments.Resources.IsManualNumber, hasCurrentNumber);
      var number = dialog.AddString(registrationNumberLabel, true)
        .WithLabel(Sungero.Docflow.OfficialDocuments.Resources.IsPreliminaryNumber)
        .MaxLength(document.Info.Properties.RegistrationNumber.Length);
      number.Value = hasCurrentNumber ? currentRegistrationNumber : dialogParams.NextNumber;
      number.IsLabelVisible = !hasCurrentNumber;
      var hyperlink = dialog.AddHyperlink(Sungero.Docflow.OfficialDocuments.Resources.LogNumberList);
      var button = dialog.Buttons.AddCustom(buttonName);
      dialog.Buttons.Default = button;
      dialog.Buttons.AddCancel();
      
      // Отчет доступен в документах с валидацией регномера при регистрации или резервировании делопроизводителем.
      // 
      hyperlink.IsVisible = operation == Sungero.Docflow.RegistrationSetting.SettingType.Registration ||
        (operation == Sungero.Docflow.RegistrationSetting.SettingType.Reservation && isClerk);
      
      // Номер по умолчанию недоступен для изменения.
      number.IsEnabled = hasCurrentNumber;
      
      // Журнал выбирать может только делопроизводитель, остальные видят, но менять не могут.
      register.IsEnabled = isClerk;
      register.IsVisible = operation == Sungero.Docflow.RegistrationSetting.SettingType.Registration ||
        operation == Sungero.Docflow.RegistrationSetting.SettingType.Reservation;
      
      // Дело не доступно для изменения
      caseFile.IsEnabled = false;
      
      dialog.SetOnRefresh(
        (e) =>
        {
          if (caseFile.Value == null)
          {
            e.AddError(string.Format(anorsv.OfficialDocument.OfficialDocuments.Resources.SetMainCaseFile, document.DocumentKind.Name) , caseFile);
            return;
          }
          if (date.Value != null && date.Value < Calendar.SqlMinValue)
          {
            e.AddError(Sungero.Docflow.OfficialDocuments.Resources.SetCorrectDate, date);
            return;
          }
          hyperlink.IsEnabled = register.Value != null;
          if (register.Value == null || !date.Value.HasValue)
            return;
          if (number.Value != null && number.Value.Length > document.Info.Properties.RegistrationNumber.Length)
          {
            var message = string.Format(Sungero.Docflow.Resources.PropertyLengthError,
                                        document.Info.Properties.RegistrationNumber.LocalizedName,
                                        document.Info.Properties.RegistrationNumber.Length);
            e.AddError(message, number);
            return;
          }
          var numberSectionsError = anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.CheckDocumentRegisterSections(anorsv.AnorsvMainSolution.DocumentRegisters.As(register.Value), document);
          var hasSectionsError = !string.IsNullOrWhiteSpace(numberSectionsError);
          // Возможен корректировочный постфикс или нет (возможен, если необходимо проверять на уникальность).
          var correctingPostfixInNumberIsAvailable = anorsv.OfficialDocument.PublicFunctions.OfficialDocument.CheckRegistrationNumberUnique(document);
          var numberFormatError = hasSectionsError
            ? string.Empty
            : anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister
            .CheckRegistrationNumberFormat(anorsv.AnorsvMainSolution.DocumentRegisters.As(register.Value), date.Value, number.Value,
                                           departmentCode, businessUnitCode, caseFileIndex, docKindCode, counterpartyCode,
                                           leadDocumentNumber, correctingPostfixInNumberIsAvailable);
          var hasFormatError = !string.IsNullOrWhiteSpace(numberFormatError);
          
          if (!hasSectionsError && !hasFormatError)
            return;
          
          var error = hasSectionsError ? numberSectionsError : numberFormatError;
          
          if (numberValidationDisabled && isManual.Value.Value)
            e.AddWarning(error);
          else
            e.AddError(error, number);
        });
      
      
      dialog.SetOnButtonClick(
        (e) =>
        {
          if (!Equals(e.Button, button))
            return;
          
          if (e.IsValid && isManual.Value.Value && date.Value.HasValue && register.Value != null)
          {
            if (!Sungero.Docflow.PublicFunctions.DocumentRegister.Remote
                .IsRegistrationNumberUnique(register.Value, document, number.Value, 0,
                                            date.Value.Value, departmentCode, businessUnitCode,
                                            caseFileIndex, docKindCode, counterpartyCode, leadDocumentId))
              e.AddError(Sungero.Docflow.OfficialDocuments.Resources.RegistrationNumberIsNotUnique, number);
          }
        });

      register.SetOnValueChanged(
        (e) =>
        {
          hyperlink.IsEnabled = e.NewValue != null && date.Value.HasValue;
          
          number.IsEnabled = isManual.Value.Value && e.NewValue != null && date.Value.HasValue;
          
          if (e.NewValue != null)
          {
            var previewDate = date.Value ?? Calendar.UserToday;
            var previewNumber = anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.Remote
              .GetNextNumber(anorsv.AnorsvMainSolution.DocumentRegisters.As(e.NewValue), previewDate, leadDocumentId, document, leadDocumentNumber, departmentId,
                             businessUnitId, caseFileIndex, docKindCode, anorsv.OfficialDocumentModule.Constants.Module.DefaultIndexLeadingSymbol);
            number.Value = previewNumber;
          }
          else
            number.Value = string.Empty;
        });
      
      date.SetOnValueChanged(
        (e) =>
        {
          if (e.NewValue != null && e.NewValue < Calendar.SqlMinValue)
            return;
          
          hyperlink.IsEnabled = e.NewValue != null && register.Value != null;
          
          number.IsEnabled = isManual.Value.Value && register.Value != null && e.NewValue.HasValue;
          
          if (!isManual.Value.Value)
          {
            if (register.Value != null)
            {
              var previewDate = e.NewValue ?? Calendar.UserToday;
              var previewNumber = anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.Remote
                .GetNextNumber(anorsv.AnorsvMainSolution.DocumentRegisters.As(register.Value), previewDate, leadDocumentId, document, leadDocumentNumber, departmentId,
                               businessUnitId, caseFileIndex, docKindCode, anorsv.OfficialDocumentModule.Constants.Module.DefaultIndexLeadingSymbol);
              number.Value = previewNumber;
            }
            else
              number.Value = string.Empty;
          }
        });
      
      isManual.SetOnValueChanged(
        (e) =>
        {
          number.IsEnabled = e.NewValue.Value && register.Value != null && date.Value.HasValue;
          
          if (register.Value != null)
          {
            var previewDate = date.Value ?? Calendar.UserToday;
            var previewNumber = anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.Remote
              .GetNextNumber(anorsv.AnorsvMainSolution.DocumentRegisters.As(register.Value), previewDate, leadDocumentId, document, leadDocumentNumber, departmentId,
                             businessUnitId, caseFileIndex, docKindCode, anorsv.OfficialDocumentModule.Constants.Module.DefaultIndexLeadingSymbol);
            number.Value = previewNumber;
          }
          else
            number.Value = string.Empty;
          number.IsLabelVisible = !e.NewValue.Value;
        });
      
      hyperlink.SetOnExecute(
        () =>
        {
          var report = Sungero.Docflow.Reports.GetSkippedNumbersReport();
          report.DocumentRegisterId = register.Value.Id;
          report.RegistrationDate = date.Value;
          
          // Если в журнале есть разрез по ведущему документу, подразделению или НОР, то заполнить из документа соответствующее свойство отчёта.
          if (register.Value.NumberingSection == Sungero.Docflow.DocumentRegister.NumberingSection.LeadingDocument)
            report.LeadingDocument = document.LeadingDocument;
          
          if (register.Value.NumberingSection == Sungero.Docflow.DocumentRegister.NumberingSection.Department)
            report.Department = document.Department;
          
          if (register.Value.NumberingSection == Sungero.Docflow.DocumentRegister.NumberingSection.BusinessUnit)
            report.BusinessUnit = document.BusinessUnit;
          
          report.Open();
        });
      
      if (dialog.Show() == button)
      {
        return anorsv.OfficialDocumentModule.Structures.Module.DialogResultMod.Create(register.Value, date.Value.Value, isManual.Value.Value ? number.Value : string.Empty, caseFile.Value);
      }
      
      return null;
    }
    
    /// <summary>
    /// Зарегистрировать документ.
    /// </summary>
    /// <param name="document"></param>
    [Public]
    public void Registeranorsv(anorsv.OfficialDocument.IOfficialDocument document) 
    {
      // Список доступных журналов.
      var dialogParams = anorsv.OfficialDocumentModule.PublicFunctions.Module.GetRegistrationDialogParamsanorsv(document, Sungero.Docflow.RegistrationSetting.SettingType.Registration);
      
      // Проверить возможность выполнения действия.
      if (dialogParams.Registers == null || !dialogParams.Registers.Any())
      {
        Dialogs.NotifyMessage(Sungero.Docflow.Resources.NoDocumentRegistersAvailable);
        return;
      }

      // Вызвать диалог.
      var result = anorsv.OfficialDocumentModule.PublicFunctions.Module.RunRegistrationDialoganorsv(document, dialogParams);
      
      if (result != null)
      {
        // Заполнить Дело в документе, чтобы не сработало заполнение делом из инд.настроек регистратора
        document.CaseFile = result.CaseFile;
        document.PlacedToCaseFileDate = Calendar.UserToday;
        
        Sungero.Docflow.PublicFunctions.OfficialDocument.RegisterDocument(document, result.Register, result.Date, result.Number, false, true);
        
        Dialogs.NotifyMessage(Sungero.Docflow.Resources.SuccessRegisterNotice);
      }
      return;
    }
    
    #endregion
    
  }
}