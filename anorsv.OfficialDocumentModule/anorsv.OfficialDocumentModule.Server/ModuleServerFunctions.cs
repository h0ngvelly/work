﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Shared;
using Sungero.Metadata;
using ServiceFunctions = anorsv.ServiceLibrary.PublicFunctions;
using anorsv.OfficialDocument.OfficialDocument;
using docRelation = anorsv.RelationModule;
//using Sungero.Docflow.DocumentTemplate;

namespace anorsv.OfficialDocumentModule.Server
{
  public class ModuleFunctions
  {
    #region Регистрация
    
    /// Получить все данные для отображения диалога регистрации.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="operation">Операция.</param>
    /// <returns>Параметры диалога.</returns>
    // Remote(IsPure = true)
    [Public]
    public static anorsv.OfficialDocumentModule.Structures.Module.IDialogParamsMod GetRegistrationDialogParamsanorsv(anorsv.OfficialDocument.IOfficialDocument document, Enumeration operation)
    {
      var leadDocumentId = anorsv.OfficialDocument.PublicFunctions.OfficialDocument.GetLeadDocumentId(document);
      var leadDocumentNumber = anorsv.OfficialDocument.PublicFunctions.OfficialDocument.GetLeadDocumentNumber(document);
      var numberValidationDisabled = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.IsNumberValidationDisabledanorsv(document); //anorsv.OfficialDocument.PublicFunctions.OfficialDocument.Remote.IsNumberValidationDisabled(document);
      var departmentId = document.Department != null ? document.Department.Id : 0;
      var departmentCode = document.Department != null ? document.Department.Code : string.Empty;
      var businessUnitId = document.BusinessUnit != null ? document.BusinessUnit.Id : 0;
      var businessUnitCode = document.BusinessUnit != null ? document.BusinessUnit.Code : string.Empty;
      var docKindCode = document.DocumentKind != null ? document.DocumentKind.Code : string.Empty;
      //var caseFileIndex = document.CaseFile != null ? document.CaseFile.Index : string.Empty;
      var caseFileIndex = string.Empty;
      var caseFileIsMain = false;
      // Заполнить Дело по Основному делу для Вида документа
      var docKindanorsv = anorsv.AnorsvMainSolution.DocumentKinds.As(document.DocumentKind);
      //var mainCaseFile = anorsv.AnorsvMainSolution.CaseFiles.GetAll().Where(c => c.DocKindCollectionanorsv.Any(d => d.MainForDocKind == true &&
      //                                                                                 d.DocKind.Equals(docKindanorsv))).FirstOrDefault();
      var mainCaseFile = anorsv.AnorsvMainSolution.CaseFiles.GetAll().Where(c => c.DocKindCollectionanorsv.Any(d => d.DocKind.Equals(docKindanorsv))).FirstOrDefault();
      if (mainCaseFile != null)
      {
        caseFileIndex = mainCaseFile.Index;
        caseFileIsMain = true;
      }
      else
      {
        caseFileIndex = (document.CaseFile != null) ? document.CaseFile.Index : string.Empty;
      }
      var defaultCaseFile = anorsv.AnorsvMainSolution.CaseFiles.GetAll().Where(f => f.Index == caseFileIndex).FirstOrDefault();

      var isClerk = document.AccessRights.CanRegister();
      var counterpartyCode = Sungero.Docflow.PublicFunctions.OfficialDocument.GetCounterpartyCode(document);
      var currentRegistrationDate = document.RegistrationDate ?? Calendar.UserToday;
      
      var registers = Sungero.Docflow.PublicFunctions.OfficialDocument.GetDocumentRegistersByDocument(document, operation);
      var defaultDocumentRegister = anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.GetDefaultDocRegister(document, registers, operation);
      string nextNumber = string.Empty;
      if (defaultDocumentRegister != null)
        nextNumber = anorsv.AnorsvMainSolution.PublicFunctions.DocumentRegister.Remote.GetNextNumber(defaultDocumentRegister, currentRegistrationDate, leadDocumentId, document,
                                                                                                     leadDocumentNumber, departmentId, businessUnitId, caseFileIndex, docKindCode,
                                                                                                     anorsv.OfficialDocumentModule.Constants.Module.DefaultIndexLeadingSymbol);
      
      return anorsv.OfficialDocumentModule.Structures.Module.DialogParamsMod.Create(registers, operation, defaultDocumentRegister,
                                                                                    document.RegistrationNumber, currentRegistrationDate, nextNumber,
                                                                                    leadDocumentId, leadDocumentNumber, numberValidationDisabled,
                                                                                    departmentId, departmentCode, businessUnitCode, businessUnitId,
                                                                                    caseFileIndex, docKindCode, counterpartyCode, isClerk, defaultCaseFile, caseFileIsMain); //anorsv.OfficialDocument.Structures.Docflow.OfficialDocument.DialogParams
    }
    
    #endregion
    
    /// <summary>
    /// Создать приложение к документу.
    /// </summary>
    /// <returns>Приложение.</returns>
    [Public, Remote(IsPure = true)]
    public static Sungero.Docflow.IAddendum AddendumCreate()
    {
      return Sungero.Docflow.Addendums.Create();
    }
    
    [Public]
    public static IQueryable<Sungero.Workflow.IAssignment> GetAssigmentsForReportByProcess(Sungero.Docflow.IOfficialDocument document)
    {
      IQueryable<Sungero.Workflow.IAssignment> assignments = null;
      Sungero.Core.AccessRights.AllowRead(
        () => {
          List<Guid> groupIds = new List<Guid>() {
            anorsv.OfficialDocumentModule.PublicConstants.Module.TaskMainGroup.ApprovalTask
              , anorsv.OfficialDocumentModule.PublicConstants.Module.TaskMainGroup.FreeApprovalTask
          };
          var setting = Sungero.Docflow.PublicFunctions.PersonalSetting.GetPersonalSettings(null);
          var showNotApproveSign = setting != null ? setting.ShowNotApproveSign == true : false;
          var approvalTaks = Sungero.Workflow.Tasks
            .GetAll(task => (
              Sungero.Docflow.FreeApprovalTasks.Is(task)
              || Sungero.Docflow.ApprovalTasks.Is(task)
             )
                    && task.AttachmentDetails.Any(
                      a => a.AttachmentTypeGuid == document.GetEntityMetadata().GetOriginal().NameGuid
                      && a.AttachmentId == document.Id
                      && a.GroupId.HasValue
                      && groupIds.Contains(a.GroupId.Value)
                     )
                   );
          
          assignments = Sungero.Workflow.Assignments
            .GetAll(a => approvalTaks.Contains(a.Task)
                    && (
                      Sungero.Docflow.ApprovalAssignments.Is(a) && Sungero.Docflow.ApprovalAssignments.As(a).Result != Sungero.Docflow.ApprovalAssignment.Result.Forward
                      || Sungero.Docflow.ApprovalManagerAssignments.Is(a)
                      || Sungero.Docflow.FreeApprovalAssignments.Is(a) && Sungero.Docflow.FreeApprovalAssignments.As(a).Result != Sungero.Docflow.FreeApprovalAssignment.Result.Forward
                      || Sungero.Docflow.ApprovalSigningAssignments.Is(a)
                     )
                    && a.Status == Sungero.Workflow.Assignment.Status.Completed
                   )
            .OrderBy(a => a.Completed);
          
        }
       );
      
      return assignments;
    }
    
    [Public, Remote(IsPure = true)]
    public static bool ApprovalSheetBySignsIsValid(Sungero.Docflow.IOfficialDocument document)
    {
      return ApprovalSheetBySignsIsValidLocal(document);
    }
    
    [Public, Remote(IsPure = true)]
    public static bool ApprovalSheetBySignsIsValidCached(Sungero.Docflow.IOfficialDocument document)
    {
      string key = Resources.CaheKeyApprovalSheetBySignsIsValidFormat(document.Id);
      bool isValid = false;
      
      if (!Cache.TryGetValue(key, out isValid))
      {
        isValid = ApprovalSheetBySignsIsValidLocal(document);
        Cache.AddOrUpdate(key, isValid, Calendar.Now.AddMinutes(30));
      }
      
      return isValid;      
    }
    
    [Public]
    public static bool ApprovalSheetBySignsIsValidLocal(Sungero.Docflow.IOfficialDocument document)
    {
      // Если версий у документа нет, то сформировать "Лист согласования" нельзя
      if (!document.HasVersions)
      {
        return false;
      }
      
      var minVersionCreateDate = document.Versions.Min(v => v.Created);
      var assignments = GetAssigmentsForReportByProcess(document).Select(a => a.Completed).ToList();
      
      return assignments.Count == 0 || assignments.Min() > minVersionCreateDate;
    }

    [Public, Remote(IsPure = true)]
    public static bool IsConfiditionalDocumentManager(IRecipient recipient)
    {
      IRole confiditionalDocumentManagers =
        Roles
        .GetAll(
          r => r.Sid == anorsv.OfficialDocumentModule.PublicConstants.Module.ConfidentialConfiguration.ConfidentialAdministratorRoleGuid
         ).FirstOrDefault();
      
      return recipient.IncludedIn(confiditionalDocumentManagers);
    }
    
    /// <summary>
    /// Признак того, что формат номера не надо валидировать.
    /// </summary>
    /// <returns>True, если формат номера неважен.</returns>
    [Public, Remote(IsPure = true)]
    public static bool IsNumberValidationDisabledanorsv(anorsv.OfficialDocument.IOfficialDocument document)
    {
      // Для всех контрактных документов валидация отключена (в т.ч. - для автонумеруемых).
      if (document.DocumentKind != null && document.DocumentKind.DocumentFlow == Sungero.Docflow.DocumentKind.DocumentFlow.Contracts)
        return true;
      
      // Для автонумеруемых неконтрактных документов валидация включена.
      if (document.DocumentKind != null && document.DocumentKind.AutoNumbering.Value)
        return false;
      
      // Для неавтонумеруемых финансовых документов валидация отключена.
      return Sungero.Docflow.AccountingDocumentBases.Is(document);
    }


    /// <summary>
    /// Удаляет из прав доступа роль "Все пользователи", если документ является конфиденциальным
    /// </summary>
    /// <param name="document"></param>
    [Public]
    public static void RevokeAccessRightsAllUsersForConfidentialDocument(Sungero.Docflow.IOfficialDocument document)
    {
      anorsv.OfficialDocument.IOfficialDocument officialDocument = anorsv.OfficialDocument.OfficialDocuments.As(document);
      bool isConfidential = officialDocument.ConfidentialDocumentanorsv.HasValue ? officialDocument.ConfidentialDocumentanorsv.Value : false;
      
      if (isConfidential)
      {
        var allUsers = Roles.AllUsers;
        bool allUsersAccessGranted = officialDocument.AccessRights.Current.Any(a => a.Recipient.Equals(allUsers));
        
        if (allUsersAccessGranted)
        {
          officialDocument.AccessRights.RevokeAll(allUsers);
          officialDocument.AccessRights.Save();
        }
      }
    }
    
    /// <summary>
    /// Синхронизирует настройки конфиденциальности для приложений.
    /// </summary>
    /// <param name="leadingDocument">Ведущий документ.</param>
    [Public]
    public static void SynchronizeAddendumsConfidentiality(Sungero.Docflow.IOfficialDocument leadingDocument)
    {
      var currentLeadingDocument = anorsv.OfficialDocument.OfficialDocuments.As(leadingDocument);
      
      if (currentLeadingDocument.HasRelations)
      {
        IRelationType relationTypeAddendum = RelationTypes.GetAll(relation => relation.Name == "Addendum").FirstOrDefault();
        
        if (relationTypeAddendum != null)
        {
          var addendums = Sungero
            .Content
            .DocumentRelations
            .GetAll(relation => relation.Source.Equals(currentLeadingDocument) && relation.RelationType.Equals(relationTypeAddendum) && sline.RSV.Addendums.Is(relation.Target))
            .Select(relation => relation.Target)
            .Cast<sline.RSV.IAddendum>()
            .ToList();
          
          foreach (var addendum in addendums)
          {
            SynchronizeAddendumConfidentiality(currentLeadingDocument, addendum, true);
          }
        }
      }
    }
    
    /// <summary>
    /// Синхронизирует настройки конфиденциальности для приложений.
    /// </summary>
    /// <param name="leadingDocumentId">ИД ведущего документа</param>
    [Public]
    public static void SynchronizeAddendumsConfidentiality(int leadingDocumentId)
    {
      var leadingDocument = Sungero.Docflow.OfficialDocuments.Get(leadingDocumentId);
      
      SynchronizeAddendumsConfidentiality(leadingDocument);
    }
    
    /// <summary>
    /// Синхронизирует настройки конфиденциальности для приложения.
    /// </summary>
    /// <param name="leadingDocument">Векдущий документ</param>
    /// <param name="addendum">Приложение</param>
    /// <param name="needSave">Нужно ли сохранять изменения в приложении</param>
    [Public]
    public static void SynchronizeAddendumConfidentiality(anorsv.OfficialDocument.IOfficialDocument leadingDocument, sline.RSV.IAddendum addendum, bool needSave = true)
    {
      var allUsers = Roles.AllUsers;
      bool needSynchronize  = addendum.ConfidentialDocumentanorsv != leadingDocument.ConfidentialDocumentanorsv;
      bool allUsersAccessGranted = addendum.AccessRights.Current.Any(a => a.Recipient.Equals(allUsers));
      bool addendumModified = false;

      if (needSynchronize)
      {
        addendum.ConfidentialDocumentanorsv = leadingDocument.ConfidentialDocumentanorsv;
        addendumModified = true;
      }
      
      if (addendum.ConfidentialDocumentanorsv == true && allUsersAccessGranted)
      {
        anorsv.OfficialDocumentModule.PublicFunctions.Module.RevokeAccessRightsAllUsersForConfidentialDocument(addendum);
        addendumModified = true;
      }
      
      if (needSave && addendumModified)
      {
        addendum.Save();
      }
    }
    
  }
}
