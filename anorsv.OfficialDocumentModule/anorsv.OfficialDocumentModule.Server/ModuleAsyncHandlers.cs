﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.OfficialDocumentModule.Server
{
  public class ModuleAsyncHandlers
  {

    public virtual void SynchronizeAddendumsConfidentiality(anorsv.OfficialDocumentModule.Server.AsyncHandlerInvokeArgs.SynchronizeAddendumsConfidentialityInvokeArgs args)
    {
      Functions.Module.SynchronizeAddendumsConfidentiality(args.LeadingDocumentId);
    }

    public virtual void SynchronizeAddendumsProjects(anorsv.OfficialDocumentModule.Server.AsyncHandlerInvokeArgs.SynchronizeAddendumsProjectsInvokeArgs args)
    {
      bool successfull = true;
      var leadingDocument = anorsv.OfficialDocument.OfficialDocuments.GetAll(document => document.Id == args.LeadingDocumentId).FirstOrDefault();
      
      if (leadingDocument != null)
      {
        if (leadingDocument.HasRelations)
        {
          IRelationType relationTypeAddendum = RelationTypes.GetAll(relation => relation.Name == "Addendum").FirstOrDefault();
          if (relationTypeAddendum != null)
          {
            var addendums = Sungero
              .Content
              .DocumentRelations
              .GetAll(relation => relation.Source.Equals(leadingDocument) && relation.RelationType.Equals(relationTypeAddendum) && sline.RSV.Addendums.Is(relation.Target))
              .Select(relation => relation.Target)
              .Cast<sline.RSV.IAddendum>()
              .ToList();
            
            foreach (var addendum in addendums)
            {
              if (addendum.ProjectsCollectionanorsv.Count() != leadingDocument.ProjectsCollectionanorsv.Count()
                  || addendum.ProjectsCollectionanorsv.Sum(row => row.Project.Id) != leadingDocument.ProjectsCollectionanorsv.Sum(row => row.Project.Id))
              {
                addendum.ProjectsCollectionanorsv.Clear();
                foreach (var projectRow in leadingDocument.ProjectsCollectionanorsv)
                {
                  addendum.ProjectsCollectionanorsv.AddNew().Project = projectRow.Project;
                }
                try
                {
                  addendum.Save();
                }
                catch
                {
                  successfull = false;
                }
              }
            }
          }
        }
      }
      
      if (!successfull)
      {
        args.Retry = true;
      }
    }

  }
}
