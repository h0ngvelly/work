﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;
using Sungero.Domain.Shared;
using Sungero.Metadata;

namespace anorsv.OfficialDocumentModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      CreateProjectKinds();
      CreateDefaultProject();
      CreateDocumentAccessRights();
      CreateRoles();
      //ApplyDefaultProjectToAlldDocumentsWithoutProject();
      //SynchronizeProjectFromProjectProperties();
      //SynchronizeAddendumsCofidentiality();
    }
    
    public static void ApplyDefaultProjectToAlldDocumentsWithoutProject()
    {
      Sungero.Projects.IProject defaultProject = sline.RSV.Projects.GetAll(project => project.GUIDsline == PublicConstants.Module.DefaulProjectGuid).FirstOrDefault();
      int completedDoucumentCount = 0;
      int lastLoggedCompletedDocumentCount = 0;
      
      InitializationLogger.DebugFormat("Init: Try apply default project to documents without any project");
      
      var queue = anorsv
        .OfficialDocument
        .OfficialDocuments
        .GetAll(
          d => d.ProjectsCollectionanorsv.Count() == 0
          && !(sline.RSV.Addendums.Is(d)
               || sline.CustomModule.ContractChanges.Is(d)
               || sline.CustomModule.MinutesPurchases.Is(d)
               || sline.CustomModule.PlanNeedsLines.Is(d)
               || sline.CustomModule.PurchaseRequests.Is(d)
              )
         );
      
      int totalDocument = queue.Count();
      
      foreach (var document in queue)
      {
        
        document.ProjectsCollectionanorsv.AddNew().Project = defaultProject;
        document.Save();
        Locks.Unlock(document);

        Logger.DebugFormat("Init: Successfull applied default project to document with id = {0}.", document.Id);
        
        completedDoucumentCount++;
        
        if (completedDoucumentCount > lastLoggedCompletedDocumentCount + 99)
        {
          InitializationLogger.DebugFormat("Init: Successfull applied default project to {0} of {1} documents.", completedDoucumentCount, totalDocument);
          lastLoggedCompletedDocumentCount = completedDoucumentCount;
        }
      }
      
      if (completedDoucumentCount > lastLoggedCompletedDocumentCount)
        InitializationLogger.DebugFormat("Init: Successfull applied default project to {0} of {1} documents.", completedDoucumentCount, totalDocument);
    }
    
    public static void SynchronizeProjectFromProjectProperties()
    {
      int completedDoucumentCount = 0;
      int lastLoggedCompletedDocumentCount = 0;
      
      InitializationLogger.DebugFormat("Init: Try synchronize project to documents with filled default project properties and empty project collection propertie.");
      
      var queue = anorsv
        .OfficialDocument
        .OfficialDocuments
        .GetAll(
          d => (sline.RSV.Addendums.Is(d)
                || sline.CustomModule.ContractChanges.Is(d)
                || sline.CustomModule.MinutesPurchases.Is(d)
                || sline.CustomModule.PlanNeedsLines.Is(d)
                || sline.CustomModule.PurchaseRequests.Is(d)
               )
          && d.Project != null
          && (d.ProjectsCollectionanorsv.Count() == 0
              || !d.ProjectsCollectionanorsv.Any(p => p.Project == d.Project)
             )
         );
      
      int totalDocument = queue.Count();
      
      foreach (var document in queue)
      {
        
        document.ProjectsCollectionanorsv.AddNew().Project = document.Project;
        document.Save();
        Locks.Unlock(document);

        Logger.DebugFormat("Init: Successfull synchronize project to document with id = {0}.", document.Id);
        
        completedDoucumentCount++;
        
        if (completedDoucumentCount == lastLoggedCompletedDocumentCount + 100)
        {
          InitializationLogger.DebugFormat("Init: Successfull synchronize project to {0} of {1} documents.", completedDoucumentCount, totalDocument);
          lastLoggedCompletedDocumentCount = completedDoucumentCount;
        }
      }
      
      if (completedDoucumentCount > lastLoggedCompletedDocumentCount)
        InitializationLogger.DebugFormat("Init: Successfull synchronize project to {0} of {1} documents.", completedDoucumentCount, totalDocument);
    }
    
    /// <summary>
    /// Создать виды проектов.
    /// </summary>
    public static void CreateProjectKinds()
    {
      CreateProjectKind(Resources.ProjectKindNameGeneral);
    }
    
    /// <summary>
    /// Создает запись в справочнике "Виды проектов".
    /// </summary>
    /// <param name="name"></param>
    public static void CreateProjectKind(string name)
    {
      var projectKind = Sungero.Projects.ProjectKinds.GetAll(kind => kind.Name == name).FirstOrDefault();
      
      if (projectKind != null)
        return;
      
      InitializationLogger.DebugFormat("Init: Create project kind '{0}'.", name);

      projectKind = Sungero.Projects.ProjectKinds.Create();
      projectKind.Name = name;
      projectKind.Save();
    }
    
    /// <summary>
    /// Создать проект по умолчанию "ВнеПроекта".
    /// </summary>
    public static void CreateDefaultProject()
    {
      var project = sline.RSV.Projects.GetAll(prj => prj.GUIDsline == Constants.Module.DefaulProjectGuid).FirstOrDefault();
      
      if (project != null)
        return;
      
      InitializationLogger.DebugFormat("Init: Create project '{0}' with guid '{1}'.", Constants.Module.DefaultProjectName, Constants.Module.DefaulProjectGuid);
      
      project = sline.RSV.Projects.Create();
      project.Name = Constants.Module.DefaultProjectName;
      project.GUIDsline = Constants.Module.DefaulProjectGuid;
      project.StartDate = Calendar.GetDate(2018, 1, 1);
      project.ActualStartDate = Calendar.GetDate(2018, 1, 1);
      project.ProjectKind = Sungero.Projects.ProjectKinds.GetAll(kind => kind.Name == Resources.ProjectKindNameGeneral).FirstOrDefault();
      project.ShortName = Constants.Module.DefaultProjectName;
      project.Stage = sline.RSV.Project.Stage.Execution;
      project.Status = sline.RSV.Project.Status.Active;
      project.Manager = sline.RSV.Employees.As(Roles.Administrators.RecipientLinks.FirstOrDefault(e => Sungero.Company.Employees.Is(e.Member)).Member);
      project.Save();
    }
    
    public static void CreateApprovalSheetLinkType()
    {
      /*
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateRelationType()
      InitializationLogger.DebugFormat("Init: Create project kind '{0}'.", name);
      
      var projectKind = ProjectKinds.Create();
      projectKind.Name = name;
      projectKind.Save();
      
      Docflow.PublicFunctions.Module.CreateExternalLink(projectKind, entityId);
       */
    }
    
    public static void CreateRoles()
    {
      IRole confidentialAdministratorRoleName = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(
        Constants.Module.ConfidentialConfiguration.ConfidentialAdministratorRoleName
        , Constants.Module.ConfidentialConfiguration.ConfidentialAdministratorRoleDescription
        , Constants.Module.ConfidentialConfiguration.ConfidentialAdministratorRoleGuid
       );
      
      Sungero.Docflow.OfficialDocuments.AccessRights.Grant(confidentialAdministratorRoleName, Constants.Module.DefaultAccessRightsTypeSid.RightsManagement);
      
    }

    /// <summary>
    /// Создать типы прав для регистрации и работы с документами.
    /// </summary>
    public static void CreateDocumentAccessRights()
    {
      InitializationLogger.Debug("Init: Create access rights for document type OfficialDocument");
      
      CreateAccessRightsForDocumentType(new Guid("58cca102-1e97-4f07-b6ac-fd866a8b7cb1"));
    }

    /// <summary>
    /// Создать права для типа документа.
    /// </summary>
    /// <param name="entityTypeGuid">Guid типа документа.</param>
    public static void CreateAccessRightsForDocumentType(Guid entityTypeGuid)
    {
      // Создать тип прав "Регистрация".
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateAccessRightsType(
        entityTypeGuid.ToString()
        , Resources.RightsManagementRightTypeName.ToString()
        , Sungero.Docflow.OfficialDocumentOperations.Manage
        , Sungero.Docflow.OfficialDocumentOperations.Manage
        , Sungero.CoreEntities.AccessRightsType.AccessRightsTypeArea.Both
        , Constants.Module.DefaultAccessRightsTypeSid.RightsManagement
        , false
        , Resources.RightsManagementRightTypeDescription.ToString()
       );
    }

    /// <summary>
    /// Синхронизировать настройки конфиденциальности для приложений
    /// </summary>
    public static void SynchronizeAddendumsCofidentiality()
    {
      int completedDoucumentCount = 0;
      int lastLoggedCompletedDocumentCount = 0;
      
      InitializationLogger.DebugFormat("Init: Try synchronize confidentiality for addendums.");
            
      var queue = sline.RSV.Addendums
        .GetAll(
          d => d.LeadingDocument != null
          && anorsv.OfficialDocument.OfficialDocuments.As(d.LeadingDocument).ConfidentialDocumentanorsv != d.ConfidentialDocumentanorsv);
      
      int totalDocument = queue.Count();
      
      foreach (var addenda in queue)
      {
        var leadingDocument = anorsv.OfficialDocument.OfficialDocuments.As(addenda.LeadingDocument);
        
        Functions.Module.SynchronizeAddendumConfidentiality(leadingDocument, addenda, true);
        
        Logger.DebugFormat("Init: Successfull synchronize confidentiality to addendum with id = {0}.", addenda.Id);
        
        completedDoucumentCount++;
        
        if (completedDoucumentCount == lastLoggedCompletedDocumentCount + 100)
        {
          InitializationLogger.DebugFormat("Init: Successfull synchronize confidentiality to {0} of {1} addendums.", completedDoucumentCount, totalDocument);
          lastLoggedCompletedDocumentCount = completedDoucumentCount;
        }
      }
      
      if (completedDoucumentCount > lastLoggedCompletedDocumentCount)
        InitializationLogger.DebugFormat("Init: Successfull synchronize confidentiality to {0} of {1} addendums.", completedDoucumentCount, totalDocument);
    }
  }
}
