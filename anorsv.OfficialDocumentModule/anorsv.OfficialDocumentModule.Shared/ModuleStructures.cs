﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.OfficialDocumentModule.Structures.Module
{
  [Public]
  partial class DialogResultMod
  {
    public Sungero.Docflow.IDocumentRegister Register { get; set; }
    
    public DateTime Date { get; set; }
    
    public string Number { get; set; }
    
    public Sungero.Docflow.ICaseFile CaseFile { get; set; }
  }
  
  [Public]
  partial class DialogParamsMod
  {
    public List<Sungero.Docflow.IDocumentRegister> Registers { get; set; }
    
    public Sungero.Core.Enumeration Operation { get; set; }
    
    public Sungero.Docflow.IDocumentRegister DefaultRegister { get; set; }
    
    public string CurrentRegistrationNumber { get; set; }
    
    public DateTime? CurrentRegistrationDate { get; set; }
    
    public string NextNumber { get; set; }
    
    public int LeadId { get; set; }
    
    public string LeadNumber { get; set; }
    
    public bool IsNumberValidationDisabled { get; set; }
    
    public int DepartmentId { get; set; }
    
    public string DepartmentCode { get; set; }
    
    public string BusinessUnitCode { get; set; }
    
    public int BusinessUnitId { get; set; }
    
    public string CaseFileIndex { get; set; }
    
    public string DocKindCode { get; set; }
    
    public string CounterpartyCode { get; set; }
    
    public bool IsClerk { get; set; }
    
    public anorsv.AnorsvMainSolution.ICaseFile DefaultCaseFile {get; set; }
    
    public bool CaseFileIsMain { get; set; }
    
  } 
}