﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp;

namespace anorsv.OfficialDocumentModule.Shared
{
  partial class DocumentExtraParamOtgoingLetterBchpFunctions
  {
    /// <summary>
    /// Заполнить имя записи справочника.
    /// </summary>
    [Public]
    public virtual void FillName()
    {
      var name = "Исходящее письмо ";
      
      /* Имя в формате:
        Исходящее письмо  для < Участник > на период поездки в < Направление > с <Дата начала> по <Дата окончания>
       */
      using (TenantInfo.Culture.SwitchTo())
      {
        if (_obj.Participant != null)
          name += "для " + CaseConverter.ConvertPersonFullNameToTargetDeclension(_obj.Participant.LastName, _obj.Participant.FirstName, _obj.Participant.MiddleName, DeclensionCase.Genitive, Sungero.Core.Gender.Masculine);
        
        if (_obj.Direction != null)
          name += " на период поездки в " + _obj.Direction;
        
        if (_obj.DateFrom != null)
          name += " с " + _obj.DateFrom.Value.ToString("d");
        
        if (_obj.DateTo != null)
          name += " по " + _obj.DateTo.Value.ToString("d");
      }
      
      name = Sungero.Docflow.PublicFunctions.Module.TrimSpecialSymbols(name);
      
      _obj.Name = name;
    }
  }
}