﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp;

namespace anorsv.OfficialDocumentModule
{
  partial class DocumentExtraParamOtgoingLetterBchpSharedHandlers
  {

    public virtual void DateToChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      Functions.DocumentExtraParamOtgoingLetterBchp.FillName(_obj);
    }

    public virtual void DateFromChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      Functions.DocumentExtraParamOtgoingLetterBchp.FillName(_obj);
    }

    public virtual void DirectionChanged(anorsv.OfficialDocumentModule.Shared.DocumentExtraParamOtgoingLetterBchpDirectionChangedEventArgs e)
    {
      Functions.DocumentExtraParamOtgoingLetterBchp.FillName(_obj);
    }

    public virtual void GenderChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
    {
      Functions.DocumentExtraParamOtgoingLetterBchp.FillName(_obj);
    }

  }
}