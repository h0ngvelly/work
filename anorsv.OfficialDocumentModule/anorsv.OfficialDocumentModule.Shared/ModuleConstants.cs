﻿using System;
using Sungero.Core;

namespace anorsv.OfficialDocumentModule.Constants
{
  public static class Module
  {

    /// <summary>
    /// Регистрация документа
    /// </summary>
    [Public]
    public static class HelpCode
    {
      public const string Reservation = "Sungero_ReservationDialog";
      public const string Numeration = "Sungero_NumerationDialog";
      public const string Registration = "Sungero_RegistrationDialog";
      public const string Return = "Sungero_ReturnDialog";
      public const string Deliver = "Sungero_DeliverDialog";
      public const string ReturnFromCounterparty = "Sungero_ReturnFromCounterpartyDialog";
      // Код диалога создания поручений по документу.
      public const string CreateActionItems = "Sungero_Meetings_CreateActionItemsDialog";
      public const string SendByEmail = "Sungero_SendByEmailDialog";
    }

    public const string DefaultIndexLeadingSymbol = "0";

    /// <summary>
    /// Наименование проекта по умолчанию для документов у которых не указан не один проект.
    /// </summary>
    [Public]
    public const string DefaultProjectName = "ВнеПроекта";

    /// <summary>
    /// Guid проекта по умолчанию "ВнеПроекта" для документов у которых не указан ни один проект.
    /// </summary>
    [Public]
    public const string DefaulProjectGuid = "df2777ad-f2a6-11ea-8ab4-00155dc87603";

    // Максимальное количество контролов для добавления приложений из диалога.
    [Public]
    public const int ManyAddendumDialogLimit = 10;

    // Максимальный размер файла для добавления приложений из диалога.
    [Public]
    public const int ManyAddendumDialogMaxFileSize = 2147483646; // 2Гб
    
    #region Тип связи "Лист согласования"
    
    /// <summary>
    /// Guid типа связи "Лист согласования"
    /// </summary>
    [Public]
    public const string ApprovalSheetLinkTypeGuid = "43688FA8-1A99-4143-9820-B1CD713CFDFC";
    
    /// <summary>
    /// Имя типа связи "Лист согласования"
    /// </summary>
    [Public]
    public const string ApprovalSheetLinkTypeName = "ApprovalSheet";
    
    public const bool ApprovalSheetLinkTypeSourceHierarhyLevelIsUpper = true;
    
    public const bool AprovalSheetLinkTypeChangeAccessRightIsNeeded = false;
    
    public const string ApprovalSheetLinkTypeSourceName = "Документ";
    
    public const string ApprovalSheetLinkTypeSourceInFormName = "Основной документ";
    
    public const bool ApprovaSheetLinkTypeSourceInFormNameShow = false;
    
    public const string ApprovalSheetLinkTypeDestinationName = "Лист согласования";
    
    public const string ApprovalSheetLinkTypeDestinationInFormName = "Лист согласования";
    
    public const bool ApprovaSheetLinkTypeDestinationInFormNameShow = true;
    
    #endregion
    
    [Public]
    public static class TaskMainGroup
    {
      [Public]
      public static readonly Guid ApprovalTask = Guid.Parse("08e1ef90-521f-41a1-a13f-c6f175007e54");

      [Public]
      public static readonly Guid DocumentReviewTask = Guid.Parse("88ec82fb-d8a8-4a36-a0d8-5c0bf42ff820");

      [Public]
      public static readonly Guid ActionItemExecutionTask = Guid.Parse("804f50fe-f3da-411b-bb2e-e5373936e029");
      
      [Public]
      public static readonly Guid FreeApprovalTask = Guid.Parse("cd77936e-884e-44bb-b869-9a60f9f5f2b4");
    }
    
    [Public]
    public static class ConfidentialConfiguration
    {
      [Public]
      public const string ConfidentialAdministratorRoleName = "АНОРСВ. Ответственные за конфиденциальные документы";
      
      [Public]
      public const string ConfidentialAdministratorRoleDescription = "Работники, имеющие право на управление конфиденциальностью документов.";
      
      [Public]
      public static readonly Guid ConfidentialAdministratorRoleGuid = Guid.Parse("750A6895-8E5A-4597-94CF-3EB84831F928");
    }

    #region Права
    
    /// <summary>
    /// GUID прав.
    /// </summary>
    public static class DefaultAccessRightsTypeSid
    {
      /// <summary>
      /// Регистрация.
      /// </summary>
      public static readonly Guid RightsManagement = Guid.Parse("D4D681EB-054C-47A9-8932-992794A264A9");

    }
    
    #endregion
    
    #region Связи
    
    // Имя типа связи "Вносит изменения"
    [Sungero.Core.PublicAttribute]
    public const string AmendingRelationName = "Amending";
    
    #endregion
  }
}