﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.OfficialDocumentModule.Shared
{
  public class ModuleFunctions
  {

    [Public]
    public static bool IsDocumentSignedApprovalSignature(Sungero.Docflow.IOfficialDocument document)
    {
      return Signatures.Get(document.LastVersion).Any(s => s.SignatureType == Sungero.Core.SignatureType.Approval);
    }
    
  }
}