﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvCoreModule.AnorsvInternalEntityTypeBase;

namespace anorsv.AnorsvCoreModule.Client
{
  partial class AnorsvInternalEntityTypeBaseActions
  {
    public override void GenerateEntityTypeId(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.GenerateEntityTypeId(e);
    }

    public override bool CanGenerateEntityTypeId(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return false;
    }

  }

}