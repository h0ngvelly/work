﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvCoreModule.AnorsvSystem;

namespace anorsv.AnorsvCoreModule.Client
{
  partial class AnorsvSystemActions
  {
    public virtual void GenerateSystemId(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (_obj.SystemId != null)
      {
        if (!Dialogs.CreateConfirmDialog("Идентификатор системы уже заполнен. Вы уверены, что зотите сгенрировать новый и заменить существующий?").Show())
          return;
      }
      
      _obj.SystemId = Guid.NewGuid().ToString();
    }

    public virtual bool CanGenerateSystemId(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canUpdate = _obj.AccessRights.CanUpdate();
      bool isInserted = _obj.State.IsInserted;
      
      return (isInserted && canUpdate);
    }

  }

}