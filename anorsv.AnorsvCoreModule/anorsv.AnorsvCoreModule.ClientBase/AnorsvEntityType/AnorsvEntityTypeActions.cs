﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvCoreModule.AnorsvEntityType;

namespace anorsv.AnorsvCoreModule.Client
{
  partial class AnorsvEntityTypeActions
  {
    public virtual void GenerateEntityTypeId(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (_obj.EntityTypeId != null)
      {
        if (!Dialogs.CreateConfirmDialog("Идентификатор типа сущности уже заполнен. Вы уверены, что зотите сгенрировать новый и заменить существующий?").Show())
          return;
      }
      
      _obj.EntityTypeId = Guid.NewGuid().ToString();
    }

    public virtual bool CanGenerateEntityTypeId(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canUpdate = _obj.AccessRights.CanUpdate();
      
      return _obj.State.IsInserted || canUpdate;
    }

  }

}