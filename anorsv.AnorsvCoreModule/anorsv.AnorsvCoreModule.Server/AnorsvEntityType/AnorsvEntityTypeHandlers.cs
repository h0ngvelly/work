﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvCoreModule.AnorsvEntityType;

namespace anorsv.AnorsvCoreModule
{
  partial class AnorsvEntityTypeServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.Name = anorsv.EntityModule.Resources.PropertDeafultTextAutoGenerated;
    }

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      if (_obj.Name != _obj.HumanReadableName)
      {
        _obj.Name = _obj.HumanReadableName;
      }
      
      if (!string.IsNullOrEmpty(_obj.EntityTypeId))
      {
        string valueInLowerCase = _obj.EntityTypeId.ToLower();
        if (_obj.EntityTypeId != valueInLowerCase)
        {
          _obj.EntityTypeId = valueInLowerCase;
        }
      }
    }
  }

}