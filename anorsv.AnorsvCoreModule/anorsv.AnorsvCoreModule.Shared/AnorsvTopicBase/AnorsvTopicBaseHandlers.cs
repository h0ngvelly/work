using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AnorsvCoreModule.AnorsvTopicBase;

namespace anorsv.AnorsvCoreModule
{





  partial class AnorsvTopicBaseSharedHandlers
  {

    public virtual void PreAgreementChanged(Sungero.Domain.Shared.BooleanPropertyChangedEventArgs e)
    {
      if (_obj.PreAgreement != true && _obj.PreApprovers.Any())
        _obj.PreApprovers.Clear();
    }

  }
}