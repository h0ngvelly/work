﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using docRelation = anorsv.RelationModule;

namespace anorsv.AuditModule.Server
{
  public class ModuleFunctions
  {

    /// <summary>
    /// Создание задачи с уведомлением об удалении связи документов
    /// </summary>
    [Public, Remote(PackResultEntityEagerly = true)]
    public void CreateDeletedRelationNotificationTask(int documentId, int selectedDocumentId, List<int> taskIdList, string selectedLinkType)
    {
      var document = Sungero.Content.ElectronicDocuments.GetAll(d => d.Id == documentId).FirstOrDefault();
      var selectedDocument = Sungero.Content.ElectronicDocuments.GetAll(d => d.Id == selectedDocumentId).FirstOrDefault();
      if (document != null && selectedDocument != null)
      {
        bool isPurchaseRequest = sline.CustomModule.PurchaseRequests.Is(document);
        // Вычислить уведомляемых в зависимости от типа документа
        var roleCurrentDoc = Roles.Null;
        if (isPurchaseRequest == true)
          roleCurrentDoc = Roles.GetAll(x => x.Sid == PublicConstants.Module.Roles.PRequestDocChangingInformRole.RoleGuid).FirstOrDefault();
        else
          roleCurrentDoc = Roles.GetAll(x => x.Sid == PublicConstants.Module.Roles.OtherDocChangingInformRole.RoleGuid).FirstOrDefault();
        
        if (roleCurrentDoc.RecipientLinks.Any()) 
        {
          // Создать уведоляющую задачу           
          INotificationTask createdTask = anorsv.AuditModule.NotificationTasks.Create();
                    
          createdTask.Author = Users.Current;
          //var performer = createdTask.Performers.AddNew();
          //performer.Perfomer = roleCurrentDoc;
          var members = roleCurrentDoc.RecipientLinks.Select(r => r.Member).ToList().Select(m => Users.As(m)).Where(m => m != null).ToList();
          foreach (var member in members)
          {
            var performer = createdTask.Performers.AddNew();
            performer.Perfomer = member;
          }
          
          createdTask.Subject = string.Format(anorsv.AuditModule.Resources.NoticeDeletedDocRelationTitle,
                                              document.Name);
          createdTask.ActiveText = string.Format(anorsv.AuditModule.Resources.NoticeDeletedDocRelationText,
                                              selectedLinkType,
                                              selectedDocument.Name,
                                              document.Name);
          // Добавить вложения
          createdTask.DocumentGroup.All.Add(document);
          createdTask.DocumentGroup.All.Add(selectedDocument);
          foreach (int taskId in taskIdList)
          {
            var approvalTask = Sungero.Docflow.ApprovalTasks.GetAll(t => t.Id == taskId).FirstOrDefault();
            var formalizedTask = anorsv.FormalizedSubTask.FormalizedSubTasks.GetAll(t => t.Id == taskId).FirstOrDefault();
            
            if (approvalTask != null)
              createdTask.TaskGroup.All.Add(approvalTask);
            if (formalizedTask != null)
              createdTask.TaskGroup.All.Add(formalizedTask);
          }
          
          createdTask.Save();
          createdTask.Start();
        }
      }
    }

  }
}