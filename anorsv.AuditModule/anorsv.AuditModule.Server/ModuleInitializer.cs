﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.AuditModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      // Выдача прав всем пользователям.
      var allUsers = Roles.AllUsers;
      if (allUsers != null)
      {
        // Задачи.
        GrantRightsOnTasks(allUsers);
        // Папки потока.
        GrantRightOnFolders(allUsers);
      }
      CreateRoles();
    }
    
    /// <summary>
    /// Выдать права всем пользователям на задачи.
    /// </summary>
    /// <param name="allUsers">Группа "Все пользователи".</param>
    public static void GrantRightsOnTasks(IRole allUsers)
    {
      // Задачи модуля "Аудит"
      if (!anorsv.AuditModule.NotificationTasks.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Create, allUsers))
      {
        InitializationLogger.Debug("Audit Init: Grant rights on NotificationTasks to all users.");
        anorsv.AuditModule.NotificationTasks.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Create);
        anorsv.AuditModule.NotificationTasks.AccessRights.Save();
      }
    }
    
    /// <summary>
    /// Выдать права на спец. папки модуля.
    /// </summary>
    /// <param name="allUsers">Группа "Все пользователи".</param>
    public static void GrantRightOnFolders(IRole allUsers)
    {
      Logger.Debug("Audit Init: Grant right on shell special folders to all users.");
      
      anorsv.AuditModule.SpecialFolders.DeletionInfo.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      anorsv.AuditModule.SpecialFolders.DeletionInfo.AccessRights.Save();
      
    }
    
    /// <summary>
    /// Создать роли для информирования об удалении связи.
    /// </summary>
    public void CreateRoles()
    {
      var prequestDocChangingInformRole = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(
        Constants.Module.Roles.PRequestDocChangingInformRole.RoleName
        , Constants.Module.Roles.PRequestDocChangingInformRole.RoleDescription
        , Constants.Module.Roles.PRequestDocChangingInformRole.RoleGuid);     
      prequestDocChangingInformRole.IsSystem = true;
      prequestDocChangingInformRole.Save();
      
      var otherDocChangingInformRole = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(
        Constants.Module.Roles.OtherDocChangingInformRole.RoleName
        , Constants.Module.Roles.OtherDocChangingInformRole.RoleDescription
        , Constants.Module.Roles.OtherDocChangingInformRole.RoleGuid);
      otherDocChangingInformRole.IsSystem = true;
      otherDocChangingInformRole.Save();
    }
  }
}
