using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.AuditModule.NotificationTask;

namespace anorsv.AuditModule
{
  partial class NotificationTaskServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      _obj.NeedsReview = false;
    }
  }

  partial class NotificationTaskPerformersPerfomerPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PerformersPerfomerFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(c => c.Status == Sungero.CoreEntities.DatabookEntry.Status.Active);
      
      // Отфильтровать всех пользователей.
      query = query.Where(x => x.Sid != Sungero.Domain.Shared.SystemRoleSid.AllUsers);
      
      // Отфильтровать служебные роли.
      return (IQueryable<T>)Sungero.RecordManagement.PublicFunctions.Module.ObserversFiltering(query);
    }
  }

}