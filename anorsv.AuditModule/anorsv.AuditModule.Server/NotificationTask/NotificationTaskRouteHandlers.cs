﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.AuditModule.NotificationTask;

namespace anorsv.AuditModule.Server
{
  partial class NotificationTaskRouteHandlers
  {

    public virtual void StartBlock4(anorsv.AuditModule.Server.AuditNoticeArguments e)
    {
      foreach (var performerRow in _obj.Performers)
      {
        var performer = performerRow.Perfomer;
        if (!e.Block.Performers.Contains(performer))
            e.Block.Performers.Add(performer);
      }
    }
  }

}