﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using docRelation = anorsv.RelationModule;

namespace anorsv.AuditModule.Server
{
  partial class DeletionInfoFolderHandlers
  {

    public virtual bool IsDeletionInfoVisible()
    {
      bool result = false;
      bool substUsersPRequest = false;
      bool substUsersOther = false;
      
      var rolePRequestDoc = Roles.GetAll(x => x.Sid == PublicConstants.Module.Roles.PRequestDocChangingInformRole.RoleGuid).FirstOrDefault();
      var membersPRequest = rolePRequestDoc.RecipientLinks.Select(r => r.Member).ToList().Select(m => Users.As(m)).Where(m => m != null).ToList();
      foreach (var member in membersPRequest)
      {
        if (Substitutions.UsersWhoSubstitute(member).Contains(Users.Current))
          substUsersPRequest = true;
      }
      
      var roleOtherDoc = Roles.GetAll(x => x.Sid == PublicConstants.Module.Roles.OtherDocChangingInformRole.RoleGuid).FirstOrDefault();
      var membersOther = roleOtherDoc.RecipientLinks.Select(r => r.Member).ToList().Select(m => Users.As(m)).Where(m => m != null).ToList();
      foreach (var member in membersOther)
      {
        if (Substitutions.UsersWhoSubstitute(member).Contains(Users.Current))
          substUsersOther = true;
      }
      
      // Отображать папку потока для ролей: Администраторы, Аудиторы, 
      // Уведомляемые об изменении связей в документах закупок (и их замещающих),
      // Уведомляемые об изменении связей в документах кроме закупок (и их замещающих) 
      result = (Sungero.Docflow.PublicFunctions.Module.Remote.IsAdministratorOrAdvisor() ||
                Users.Current.IncludedIn(rolePRequestDoc) || 
                Users.Current.IncludedIn(roleOtherDoc) || 
                substUsersPRequest ||
                substUsersOther);
      
      return result;
    }

    public virtual IQueryable<anorsv.AuditModule.IAuditNotice> DeletionInfoDataQuery(IQueryable<anorsv.AuditModule.IAuditNotice> query)
    {
      // Фильтр по статусу.
      if (_filter == null)
        return query;
      
      if (_filter.IsNotRead)
        return query.Where(n => n.IsRead != true);

      // Фильтр по периоду.
      var filterDate = Calendar.Now;
      if (_filter.Last30Days)
        filterDate = Calendar.Today.AddDays(-30);
      else if (_filter.Last90Days)
        filterDate = Calendar.Today.AddDays(-90);
      else if (_filter.Last180Days)
        filterDate = Calendar.Today.AddDays(-180);
      
      return query.Where(n => n.Created >= filterDate);
    }
  }

  partial class AuditModuleHandlers
  {
  }
}