﻿using System;
using Sungero.Core;

namespace anorsv.AuditModule.Constants
{
  public static class Module
  {
    [Public]
    public static class Roles
    {
      [Public]
      public static class PRequestDocChangingInformRole
      {
        [Public]
        public const string RoleName = "АНОРСВ. Уведомляемые об изменении связей в документах закупок";          
        [Public]
        public const string RoleDescription = "Уведомляемые об изменении связей в документах закупок";          
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("60FE5EA8-2EC6-45F0-BE67-B7E684E5E005");
      }
      
      [Public]
      public static class OtherDocChangingInformRole
      {
        [Public]
        public const string RoleName = "АНОРСВ. Уведомляемые об изменении связей в документах кроме закупок";          
        [Public]
        public const string RoleDescription = "Уведомляемые об изменении связей в документах кроме закупок";          
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("67100D92-827A-44CB-8B48-B619C52F3A8E");
      }
    }        
    
  }
}