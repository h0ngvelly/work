﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Company;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.OfficialDocument
{
  partial class ApprovalSheetByProcessReportServerHandlers
  {
    public override void AfterExecute(Sungero.Reporting.Server.AfterExecuteEventArgs e)
    {
      Sungero.Docflow.PublicFunctions.Module.DeleteReportData(Constants.ApprovalSheetByProcessReport.SourceTableName, ApprovalSheetByProcessReport.ReportSessionId);
    }

    public override void BeforeExecute(Sungero.Reporting.Server.BeforeExecuteEventArgs e)
    {
      ApprovalSheetByProcessReport.ReportSessionId = Guid.NewGuid().ToString();
      anorsv.OfficialDocument.PublicFunctions.OfficialDocument.UpdateApprovalSheetByPtocessReportTable(ApprovalSheetByProcessReport.Document, ApprovalSheetByProcessReport.ReportSessionId);
      ApprovalSheetByProcessReport.HasRespEmployee = false;
      
      var document = anorsv.OfficialDocument.OfficialDocuments.As(ApprovalSheetByProcessReport.Document);
      if (document == null)
        return;
      
      // Наименование отчета.
      ApprovalSheetByProcessReport.DocumentName = Sungero.Docflow.PublicFunctions.Module.FormatDocumentNameForReport(document, false);
      
      // НОР.
      var ourOrg = document.BusinessUnit;
      if (ourOrg != null)
        ApprovalSheetByProcessReport.OurOrgName = ourOrg.Name;
      
      // Дата отчета.
      ApprovalSheetByProcessReport.CurrentDate = Calendar.Now;
      
      // Ответственный.
      var responsibleEmployee = Sungero.Docflow.PublicFunctions.OfficialDocument.GetDocumentResponsibleEmployee(document);
      
      if (responsibleEmployee != null &&
          responsibleEmployee.IsSystem != true)
      {
        var respEmployee = string.Format("{0}: {1}",
                                         Reports.Resources.ApprovalSheetByProcessReport.ResponsibleEmployee,
                                         responsibleEmployee.Person.ShortName);
        
        if (responsibleEmployee.JobTitle != null)
          respEmployee = string.Format("{0} ({1})", respEmployee, responsibleEmployee.JobTitle.DisplayValue.Trim());
        
        ApprovalSheetByProcessReport.RespEmployee = respEmployee;
        
        ApprovalSheetByProcessReport.HasRespEmployee = true;
      }
      
      // Распечатал.
      if (Employees.Current == null)
      {
        ApprovalSheetByProcessReport.Clerk = Users.Current.Name;
      }
      else
      {
        var clerkPerson = Employees.Current.Person;
        ApprovalSheetByProcessReport.Clerk = clerkPerson.ShortName;
      }
      
      // Проекты.
      if (document.ProjectsCollectionanorsv.Count() > 0)
      {
        List<string> projects = new List<string>() {string.Format("{0}:", Reports.Resources.ApprovalSheetByProcessReport.Projects)};
        
        foreach (var projectRow in document.ProjectsCollectionanorsv)
        {
          if (projectRow.Project != null)
          {
            projects.Add(string.Format("{0}. {1}", projects.Count, projectRow.Project.Name));
          }
        }
        
        if (projects.Count > 1)
        {
          ApprovalSheetByProcessReport.Projects = string.Join(Environment.NewLine, projects);
        }
      }
    }
  }
}