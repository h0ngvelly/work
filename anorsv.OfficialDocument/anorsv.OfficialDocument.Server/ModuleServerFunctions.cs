﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.OfficialDocument.Server
{
  public class ModuleFunctions
  {
    /// <summary>
    /// Получение списка проектов к которым относится документ.
    /// </summary>
    /// <param name="document">Документ для которого опеределяется список проектов к которым он относится.</param>
    /// <returns>Список проектов</returns>
    [Public]
    public virtual List<Sungero.Docflow.IProjectBase> GetProjects(Sungero.Docflow.IOfficialDocument document)
    {
      List<Sungero.Docflow.IProjectBase> projects = new List<Sungero.Docflow.IProjectBase>();
      bool isExtendedDocument = anorsv.OfficialDocument.OfficialDocuments.Is(document);
      
      if (document.Project != null)
        projects.Add(document.Project);
      
      if (isExtendedDocument)
      {
        anorsv.OfficialDocument.IOfficialDocument extendedDocument = anorsv.OfficialDocument.OfficialDocuments.As(document);
        projects.AddRange(extendedDocument.ProjectsCollectionanorsv
                          .Where(p => p.Project != null)
                          .Select(p => p.Project)
                         );
      }
      
      return projects;
    }
  }
}