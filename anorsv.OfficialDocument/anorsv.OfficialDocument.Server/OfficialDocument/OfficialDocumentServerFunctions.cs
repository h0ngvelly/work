﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.OfficialDocument.OfficialDocument;
using Sungero.Domain.Shared;
using Sungero.Metadata;
using anorsv.AnorsvMainSolution.CaseFile;

namespace anorsv.OfficialDocument.Server
{
  partial class OfficialDocumentFunctions
  {
    /// <summary>
    /// Скопировать права из текущего документа в указанный.
    /// </summary>
    /// <param name="document">Документ, в который копируются права.</param>
    /// <param name="accessRightsLimit">Максимальный тип прав, который может быть выдан. Guid.Empty, если устанавливать максимальный уровень прав не требуется.</param>
    [Public]
    public override void CopyAccessRightsToDocument(Sungero.Docflow.IOfficialDocument document, Guid accessRightsLimit)
    {
      if (document == null)
        return;
      
      foreach (var accessRight in _obj.AccessRights.Current)
      {
        var rightsTypeToGrant = accessRight.AccessRightsType;
        /* Тип прав Forbidden является самым высоким типом прав.
         * С типом прав Change нельзя выдать Forbidden и FullAccess.
         * С типом прав FullAccess можно выдать Forbidden.
         */
        if (accessRightsLimit == DefaultAccessRightsTypes.Change &&
            rightsTypeToGrant == DefaultAccessRightsTypes.Forbidden)
          continue;
        
        if (accessRightsLimit != Guid.Empty &&
            rightsTypeToGrant != DefaultAccessRightsTypes.Forbidden &&
            anorsv.AnorsvMainSolution.Module.Docflow.PublicFunctions.Module.CompareInstanceAccessRightsTypes(rightsTypeToGrant, accessRightsLimit) > 0)
          rightsTypeToGrant = accessRightsLimit;
        
        Sungero.Docflow.PublicFunctions.Module.GrantAccessRightsOnEntity(document, accessRight.Recipient, rightsTypeToGrant);
        Logger.DebugFormat("Grant Access Rights ({0}) For document ({1}), employee: ({2})", rightsTypeToGrant, document.Id, accessRight.Recipient.Id);
      }
    }
    
    /// <summary>
    /// Получить максимальный тип прав на документ, которые текущий пользователь может выдать.
    /// </summary>
    /// <returns>Guid типа прав. Guid.Empty, если текущий пользователь не может выдавать права на документ.</returns>
    [Public]
    public override Guid GetAvailableAccessRights()
    {
      // Dmitriev_IA: Проверять на администратора раньше, чем на запрещающие права. Bug 159165.
      if (Users.Current.IsSystem == true || Sungero.Docflow.PublicFunctions.Module.Remote.IsAdministrator())
        return DefaultAccessRightsTypes.FullAccess;
      
      var currentUser = Users.Current;
      
      if (_obj.AccessRights.IsGranted(DefaultAccessRightsTypes.Forbidden, currentUser))
        return Guid.Empty;
      
      if (_obj.AccessRights.IsGranted(DefaultAccessRightsTypes.FullAccess, currentUser))
        return DefaultAccessRightsTypes.FullAccess;
      
      if (_obj.AccessRights.IsGranted(DefaultAccessRightsTypes.Change, currentUser))
        return DefaultAccessRightsTypes.Change;
      
      if (_obj.AccessRights.IsGranted(DefaultAccessRightsTypes.Read, currentUser))
        return Guid.Empty;
      
      return Guid.Empty;
    }
    
    /// <summary>
    /// Актуализируем отметку Изданы изменения
    /// Если есть связь "Вносит изменения" и действующий приказ, то у связанного приказа Изданы изменения = Да
    /// Если есть связь "Изменен на основании" с действующим приказом, то Изданы изменения = Да
    /// Иначе Изданы изменения = Нет
    /// </summary>
    public void UpdateChangesMade()
    {
      // Связи типа Изменен на основании
      var haveChangeRelations = _obj.Relations.GetRelated(anorsv.OfficialDocumentModule.PublicConstants.Module.AmendingRelationName);
      var changesMadeValue = anorsv.OfficialDocument.OfficialDocument.ChangesMadeanorsv.No;
      
      if (haveChangeRelations.Any())
        if (haveChangeRelations.Any(d => anorsv.OfficialDocument.OfficialDocuments.As(d).LifeCycleState.Equals(anorsv.OfficialDocument.OfficialDocument.LifeCycleState.Active)))
          changesMadeValue = anorsv.OfficialDocument.OfficialDocument.ChangesMadeanorsv.Yes;
        else
          changesMadeValue = anorsv.OfficialDocument.OfficialDocument.ChangesMadeanorsv.No;
      
      // Актуализировать, только если текущее значение не совпадает с устанавливаемым
      if (_obj.ChangesMadeanorsv != changesMadeValue && changesMadeValue != null)
        Sungero.Core.AccessRights.AllowRead(() => _obj.ChangesMadeanorsv = changesMadeValue);
    }
    
    [Public, Remote(IsPure = true)]
    public override bool IsNumberValidationDisabled()
    {
      return base.IsNumberValidationDisabled();
    }
    
    /// <summary>
    /// Заполнить SQL таблицу для формирования отчета "Лист согласования".
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="reportSessionId">Идентификатор отчета.</param>
    [Public]
    public static void UpdateApprovalSheetByPtocessReportTable(Sungero.Docflow.IOfficialDocument document, string reportSessionId)
    {
      var assignments = anorsv.OfficialDocumentModule.PublicFunctions.Module.GetAssigmentsForReportByProcess(document).ToList();
      
      using (var connection = SQL.CreateConnection())
      {
        foreach (var assignment in assignments)
        {
          string employeeName = string.Empty;
          string performer = string.Empty;
          string performerAdditionalInfo = string.Empty;
          string completedBy = string.Empty;
          string completedByAdditionalInfo = string.Empty;
          string resultString = string.Empty;
          string signType = string.Empty;
          Sungero.Docflow.IApprovalAssignment approvalAssignment = Sungero.Docflow.ApprovalAssignments.As(assignment);
          Sungero.Docflow.IApprovalManagerAssignment approvalManagerAssignment = Sungero.Docflow.ApprovalManagerAssignments.As(assignment);
          Sungero.Docflow.IFreeApprovalAssignment freeApprovalAssignment = Sungero.Docflow.FreeApprovalAssignments.As(assignment);
          Sungero.Docflow.IApprovalSigningAssignment approvalSigningAssignment = Sungero.Docflow.ApprovalSigningAssignments.As(assignment);
          
          if (Sungero.Company.Employees.Is(assignment.Performer))
          {
            var performerEmployee = Sungero.Company.Employees.As(assignment.Performer);
            performerAdditionalInfo = string.Format(
              "<b>{0} ({1})</b>"
              , performerEmployee.JobTitle != null ? performerEmployee.JobTitle.Name : string.Empty
              , performerEmployee.Department.Name
             );
          }
          
          performer = string.Format("{0}{1}", anorsv.ServiceLibrary.PublicFunctions.Module.AddEndOfLine(performerAdditionalInfo), assignment.Performer.Name);
          
          if (!assignment.Performer.Equals(assignment.CompletedBy))
          {
            if (Sungero.Company.Employees.Is(assignment.CompletedBy))
            {
              var completedByEmployee = Sungero.Company.Employees.As(assignment.CompletedBy);
              completedByAdditionalInfo = string.Format("<b>{0} ({1})</b>", completedByEmployee.JobTitle.Name, completedByEmployee.Department.Name);
            }
          }
          
          completedBy = string.Format("{0}{1}", anorsv.ServiceLibrary.PublicFunctions.Module.AddEndOfLine(completedByAdditionalInfo), assignment.CompletedBy.Name);
          
          if (assignment.Performer.Equals(assignment.CompletedBy))
          {
            employeeName = performer;
          }
          else
          {
            var onBehalfOfText = anorsv.ServiceLibrary.PublicFunctions.Module.AddEndOfLine(anorsv.OfficialDocument.OfficialDocuments.Resources.OnBehalfOf);
            employeeName = string.Format("{0}{1}{2}", completedBy, onBehalfOfText, performer);
          }
          
          if (approvalAssignment != null)
          {
            if (approvalAssignment.Result == Sungero.Docflow.ApprovalAssignment.Result.Approved)
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormEndorsed;
            }
            else
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormNotEndorsed;
            }
          } else if (approvalManagerAssignment != null)
          {
            if (approvalManagerAssignment.Result == Sungero.Docflow.ApprovalManagerAssignment.Result.Approved)
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormEndorsed;
            }
            else
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormNotEndorsed;
            }
          } else if (freeApprovalAssignment != null)
          {
            if (freeApprovalAssignment.Result == Sungero.Docflow.FreeApprovalAssignment.Result.Approved)
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormEndorsed;
            }
            else
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormNotEndorsed;
            }
          } else if (approvalSigningAssignment != null)
          {
            if (approvalSigningAssignment.Result == Sungero.Docflow.ApprovalSigningAssignment.Result.Abort)
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormNotEndorsed;
            } else if (approvalSigningAssignment.Result == Sungero.Docflow.ApprovalSigningAssignment.Result.ConfirmSign)
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormApproved;
            } else if (approvalSigningAssignment.Result == Sungero.Docflow.ApprovalSigningAssignment.Result.Sign)
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormApproved;
            } else
            {
              resultString = Sungero.Docflow.ApprovalTasks.Resources.ApprovalFormNotEndorsed;
            }
          }
          
          using (var command = connection.CreateCommand())
          {
            string commandText = Queries.OfficialDocument.InsertIntoApprovalSheetReportTable;
            command.CommandText = "insert into Sungero_Reports_ApprSheet(ReportSessionId, EmployeeName, SignType, Comment, Version, Date) values (@reportSessionId, @employeeName, @resultString, @comment, -1, @SignatureDate)";
            SQL.AddParameter(command, "@reportSessionId",  reportSessionId, System.Data.DbType.String);
            SQL.AddParameter(command, "@employeeName",  employeeName, System.Data.DbType.String);
            SQL.AddParameter(command, "@resultString",  resultString, System.Data.DbType.String);
            SQL.AddParameter(command, "@comment",  assignment.ActiveText, System.Data.DbType.String);
            SQL.AddParameter(command, "@SignatureDate",  assignment.Completed, System.Data.DbType.DateTime);
            
            command.ExecuteNonQuery();
          }
        }
      }
    }
    
  }
}