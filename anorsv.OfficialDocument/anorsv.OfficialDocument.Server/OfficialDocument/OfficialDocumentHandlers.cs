﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.OfficialDocument.OfficialDocument;

namespace anorsv.OfficialDocument
{
  partial class OfficialDocumentAuthorPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> AuthorFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = base.AuthorFiltering(query, e);
      var users = Substitutions.ActiveSubstitutedUsers.ToList();
      if (!Users.Current.IncludedIn(Roles.Administrators) && users.Any())
      {
        users.Add(Users.Current);
        query = query.Where(x => users.Contains(Sungero.CoreEntities.Users.As(x)));
      }
      return query;
    }
  }

  partial class OfficialDocumentTopicanorsvPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> TopicanorsvFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(x => Equals(_obj.DocumentKind, x.DocumentKind));
      return query;
    }
  }

  partial class OfficialDocumentServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);
    }

    public override void BeforeSaveHistory(Sungero.Content.DocumentHistoryEventArgs e)
    {
      base.BeforeSaveHistory(e);
      
      var isUpdateAction = e.Action == Sungero.CoreEntities.History.Action.Update;
      var isCreateAction = e.Action == Sungero.CoreEntities.History.Action.Create;
      var isChangeRelationAction = e.Action == Sungero.CoreEntities.History.Action.ChangeRelation;
      
      // Изменить признак Изданы изменения только при измененеии связей
      if (isChangeRelationAction)
      {
        // Актуализировать Изданы изменения только для приказов (с видом не ЛНА) и для ЛНА
        if (Sungero.RecordManagement.Orders.Is(_obj))
        {
          var docKindLNAList = Sungero.Docflow.DocumentKinds
            .GetAll(k => k.Name.Contains("Локальный нормативный акт"))
            .Select(k => k.Id)
            .ToList();
          if (!docKindLNAList.Contains(_obj.DocumentKind.Id))
          {
            anorsv.OfficialDocument.Functions.OfficialDocument.UpdateChangesMade(_obj);
          }
        }
        
        if (anorsv.DocflowModule.LNAs.Is(_obj))
        {
          anorsv.OfficialDocument.Functions.OfficialDocument.UpdateChangesMade(_obj);
        }
      }
      
      // Изменять историю только при изменении и создании документа
      if (!isUpdateAction && !isCreateAction)
        return;
      
      // Свойства, для которых будем записывать расширенную историю
      var properties = _obj.State.Properties.Where(p =>
                                                   p == _obj.State.Properties.SignESanorsv);
      
      var propertiesType = anorsv.OfficialDocument.OfficialDocuments.Info.Properties.GetType();
      var objType = _obj.GetType();
      var operation = new Enumeration("ReqChange");
      
      // Обходим коллекцию свойств
      foreach (var property in properties)
      {
        var isChanged = (property as Sungero.Domain.Shared.IPropertyState).IsChanged;
        if (isChanged)
        {
          // Извлечение локализованного наименования реквизита.
          var propertyName = (property as Sungero.Domain.Shared.PropertyStateBase).PropertyName;
          var propertyInfo = propertiesType.GetProperty(propertyName).GetValue(anorsv.OfficialDocument.OfficialDocuments.Info.Properties);
          var name = propertyInfo.GetType().GetProperty("LocalizedName").GetValue(propertyInfo);
          
          // Новое значение реквизита
          var newValue = objType.GetProperty(propertyName).GetValue(_obj);
          // Старое значение реквизита
          var oldValue = property.GetType().GetProperty("OriginalValue").GetValue(property);
          
          // Сверяем, что старое и новое значения не равны, отбрасываем изменения строковых свойств с null на пустую строку
          if (newValue == oldValue ||
              newValue != null && oldValue != null && Equals(newValue.ToString(), oldValue.ToString()) ||
              newValue != null && oldValue == null && string.IsNullOrEmpty(newValue.ToString()) ||
              oldValue != null && newValue == null && string.IsNullOrEmpty(oldValue.ToString()))
            continue;
          
          if (propertyName.Equals("SignESanorsv"))
          {
            if (newValue != null && !string.IsNullOrEmpty(newValue.ToString()))
              newValue = (newValue.Equals(SignESanorsv.SignES))
                ? OfficialDocuments.Info.Properties.SignESanorsv.GetLocalizedValue(SignESanorsv.SignES)
                : OfficialDocuments.Info.Properties.SignESanorsv.GetLocalizedValue(SignESanorsv.SignOnPaper);
            if (oldValue != null && !string.IsNullOrEmpty(oldValue.ToString()))
              oldValue = (oldValue.Equals(anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES))
                ? OfficialDocuments.Info.Properties.SignESanorsv.GetLocalizedValue(SignESanorsv.SignES)
                : OfficialDocuments.Info.Properties.SignESanorsv.GetLocalizedValue(SignESanorsv.SignOnPaper);
          }
          
          var comment = OfficialDocuments.Resources.HistoryEnumerationProperyChangedFormat(name, newValue, oldValue);
          e.Write(operation, null, comment);
          
        }
      }
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      // Заполним отметку об конфиденциальности
      _obj.ConfidentialDocumentanorsv = false;
    }

    public override void AfterSave(Sungero.Domain.AfterSaveEventArgs e)
    {
      base.AfterSave(e);
      
      if (_obj.LeadingDocument == null)
      {
        var asyncProjectSynchronizeHandler = anorsv.OfficialDocumentModule.AsyncHandlers.SynchronizeAddendumsProjects.Create();
        asyncProjectSynchronizeHandler.LeadingDocumentId = _obj.Id;
        asyncProjectSynchronizeHandler.ExecuteAsync();
        
        if (sline.RSV.Addendums.GetAll(a => a.LeadingDocument.Equals(_obj) && a.ConfidentialDocumentanorsv != _obj.ConfidentialDocumentanorsv).Any())
        {
          var asyncSynchronizeAddendumsConfidentiality = anorsv.OfficialDocumentModule.AsyncHandlers.SynchronizeAddendumsConfidentiality.Create();
          asyncSynchronizeAddendumsConfidentiality.LeadingDocumentId = _obj.Id;
          asyncSynchronizeAddendumsConfidentiality.ExecuteAsync();
        }
      }
      
    }

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      base.Saving(e);
      
      // Синхронизируем коробочный реквизит Проект и коллекцию с проектами.
      if (_obj.Project != null && !_obj.ProjectsCollectionanorsv.Any(p => p.Project != null && p.Project.Equals(_obj.Project)))
        _obj.ProjectsCollectionanorsv.AddNew().Project = _obj.Project;
      
      // Заполним отметку об конфиденциальности
      if (!_obj.ConfidentialDocumentanorsv.HasValue)
      {
        _obj.ConfidentialDocumentanorsv = false;
      }
      
      // Контроль за наличием группы "Все пользователи" с уровнем прав "Чтение", если документ не конфиденциальный и отсутствием, если он кофиденциальный
      if (!(_obj.ConfidentialDocumentanorsv == true) && _obj.AccessRights.CanManage())
      {
        if (Roles.AllUsers != null)
        {
          if (!_obj.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Read, Roles.AllUsers))
          {
            if (_obj.AccessRights.Current.Any(a => a.Recipient.Equals(Roles.AllUsers)))
            {
              _obj.AccessRights.RevokeAll(Roles.AllUsers);
            }
            _obj.AccessRights.Grant(Roles.AllUsers, DefaultAccessRightsTypes.Read);
          }
        }
      }
      else
      {
        anorsv.OfficialDocumentModule.PublicFunctions.Module.RevokeAccessRightsAllUsersForConfidentialDocument(_obj);
      }
      
    }
  }

}