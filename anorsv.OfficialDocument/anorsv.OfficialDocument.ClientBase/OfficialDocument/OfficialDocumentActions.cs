﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.OfficialDocument.OfficialDocument;
using docRelation = anorsv.RelationModule;

namespace anorsv.OfficialDocument.Client
{
  partial class OfficialDocumentVersionsActions
  {

    public override void DeleteVersion(Sungero.Domain.Client.ExecuteChildCollectionActionArgs e)
    {
      base.DeleteVersion(e);
    }

    public override bool CanDeleteVersion(Sungero.Domain.Client.CanExecuteChildCollectionActionArgs e)
    {
      var isAdmin = Sungero.CoreEntities.Users.Current.IncludedIn(Roles.Administrators);
      // сюда не заходит при открытии карточки, списка версий, контекстного меню версии
      if (isAdmin)
        return true;
      else
        return base.CanDeleteVersion(e);
    }

  }

  partial class OfficialDocumentCollectionActions
  {
    public override void SendByMail(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      //base.SendByMail(e);
      var anyOutgoingLetter = false;
      var emailToDefault = string.Empty;
      var documentList = new List<Sungero.Docflow.IOfficialDocument>() { };//System.Collections.Generic.List<Sungero.Docflow.IOfficialDocument>;
      foreach (var doc in _objs)
      {
        documentList.Add(Sungero.Docflow.OfficialDocuments.As(doc));
        
        if (sline.RSV.OutgoingLetters.Is(doc))
        {
          anyOutgoingLetter = true;
          var outgoingLetterDoc = sline.RSV.OutgoingLetters.As(doc);
          foreach (var addresseeRow in outgoingLetterDoc.Addressees)
          {
            if ((addresseeRow.Addressee != null) && (addresseeRow.Addressee.Email != null && !addresseeRow.Addressee.Email.Equals(string.Empty))
                && (!emailToDefault.Contains(addresseeRow.Addressee.Email)))
            {
              emailToDefault += addresseeRow.Addressee.Email + "; ";
            }
            else
            {
              if ((addresseeRow.Correspondent != null) && (addresseeRow.Correspondent.Email != null && !addresseeRow.Correspondent.Email.Equals(string.Empty))
                  && (!emailToDefault.Contains(addresseeRow.Correspondent.Email)))
                emailToDefault += addresseeRow.Correspondent.Email + "; ";
            }
          }
        }
      }
      
      if (anyOutgoingLetter == true)
      {
        if (this.Entities.Count() != 1)
        {
          anorsv.OfficialDocument.Functions.OfficialDocument.CreateEmailanorsv(anorsv.OfficialDocument.OfficialDocuments.As(_objs.FirstOrDefault()), documentList, string.Empty);
          return;
        }
        
        var document = anorsv.OfficialDocument.OfficialDocuments.As(_objs.FirstOrDefault());
        //documentList.Add(document);
        var relations = anorsv.OfficialDocument.Functions.OfficialDocument.GetRelatedDocumentsWithVersions(document);
        if (relations.Count > 0)
          anorsv.OfficialDocument.Functions.OfficialDocument.SelectRelatedDocumentsAndCreateEmail(document, relations);
        else
          anorsv.OfficialDocument.Functions.OfficialDocument.CreateEmailanorsv(document, documentList, emailToDefault);
        //base.SendByMail(e);
      }
      else
        base.SendByMail(e);
    }

    public override bool CanSendByMail(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendByMail(e);
    }

  }


  partial class OfficialDocumentActions
  {
    public override void SendForAcquaintance(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var activeAcquaintanceTasksExist = anorsv.TaskModule.PublicFunctions.Module.Remote.ActiveAcquaintanceTaskExist(_obj);
      
      if (activeAcquaintanceTasksExist)
      {
        e.AddError(OfficialDocuments.Resources.ErrorAcquaintanceTaskExist);
        return;
      }
      else
      {
        base.SendForAcquaintance(e);
      }
    }

    public override bool CanSendForAcquaintance(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForAcquaintance(e);
    }

    public override void SendForReview(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var activeReviewTasksExist = anorsv.TaskModule.PublicFunctions.Module.Remote.ActiveReviewTaskExist(_obj);
      
      if (activeReviewTasksExist)
      {
        e.AddError(OfficialDocuments.Resources.ErrorReviewTaskExist);
        return;
      }
      else
      {
        base.SendForReview(e);
      }
    }

    public override bool CanSendForReview(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForReview(e);
    }

    public override void SendForFreeApproval(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var activeFreeApprovalTasksExist = anorsv.TaskModule.PublicFunctions.Module.Remote.ActiveFreeApprovalTaskExist(_obj);
      
      if (activeFreeApprovalTasksExist)
      {
        e.AddError(OfficialDocuments.Resources.ErrorFreeApprovalTaskExist);
        return;
      }
      else
      {
        base.SendForFreeApproval(e);
      }
    }

    public override bool CanSendForFreeApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForFreeApproval(e);
    }

    public override void SendActionItem(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var activeActionItemExecuteTasksExist = anorsv.TaskModule.PublicFunctions.Module.Remote.ActiveActionItemExecutionTaskExist(_obj);
      
      if (activeActionItemExecuteTasksExist)
      {
        e.AddError(OfficialDocuments.Resources.ErrorActionItemExeteTaskExist);
        return;
      }
      else
      {
        base.SendActionItem(e);
      }
    }

    public override bool CanSendActionItem(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendActionItem(e);
    }

    public override void SendForApproval(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var activeApprovalTasksExist = anorsv.TaskModule.PublicFunctions.Module.Remote.ActiveApprovalTaskExist(_obj);
      
      if (activeApprovalTasksExist)
      {
        e.AddError(OfficialDocuments.Resources.ErrorActiveApprovalTasksExist);
        return;
      }
      else
      {
        base.SendForApproval(e);
      }
    }

    public override bool CanSendForApproval(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanSendForApproval(e);
    }

    public virtual void SendByMailanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var mail = MailClient.CreateMail();
      mail.Subject = Sungero.Docflow.OfficialDocuments.Resources.SendByEmailSubjectPrefixFormat(_obj.Name);
      var attachmentBytes = Convert.FromBase64String(anorsv.RecordManagement.Module.RecordManagement.PublicFunctions.Module.Remote.CreateZip(_obj));
      var zip = new System.IO.MemoryStream(attachmentBytes);
      
      var doc = Sungero.Docflow.SimpleDocuments.Create();
      doc.DocumentKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Sungero.Docflow.PublicConstants.Module.Initialize.SimpleDocumentKind);
      doc.Name = _obj.Name;
      doc.CreateVersionFrom(zip, "ZIP");
      
      doc.Save();
      mail.AddAttachment(doc.LastVersion);
      mail.Show();
      Sungero.Docflow.SimpleDocuments.Delete(doc);
    }

    public virtual bool CanSendByMailanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return anorsv.OfficialDocumentModule.PublicFunctions.Module.IsDocumentSignedApprovalSignature(_obj);
    }
    
    public override void CreateManyAddendum(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(_obj);
    }

    public override bool CanCreateManyAddendum(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;

    }

    public virtual void AddDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      docRelation.PublicFunctions.Module.AddDocumentLinkDialog(_obj, null);
    }

    public virtual bool CanAddDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canAddRelation = false;
      if (e.Params.Contains("AnoRsvCanAddRelations"))
        if (e.Params.TryGetValue("AnoRsvCanAddRelations", out canAddRelation))
          return canAddRelation;
      
      return canAddRelation;
    }

    public virtual void DeleteRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(_obj);
    }

    public virtual bool CanDeleteRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelations"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelations", out canModifyRelation))
          return canModifyRelation;
      
      return canModifyRelation;
    }

    public virtual void ChangeRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      docRelation.PublicFunctions.Module.ChangeDocumentLinkDialog(_obj);
    }

    public virtual bool CanChangeRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelations"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelations", out canModifyRelation))
          return canModifyRelation;
      
      return canModifyRelation;
    }

    public override void ChangeDocumentType(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ChangeDocumentType(e);
    }

    public override bool CanChangeDocumentType(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanChangeDocumentType(e);
    }

    public override void Register(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var originalNumber = _obj.State.Properties.RegistrationNumber.OriginalValue;
      var originalDate = _obj.State.Properties.RegistrationDate.OriginalValue;
      
      anorsv.OfficialDocument.Functions.OfficialDocument.RemoveNeedValidateRegisterFormatParameter(_obj, e);
      anorsv.OfficialDocument.Functions.OfficialDocument.Registeranorsv(_obj, e);
      
      if (_obj.RegistrationNumber != originalNumber)
        _obj.State.Properties.RegistrationNumber.HighlightColor = Colors.Empty;
      if (_obj.RegistrationDate != originalDate)
        _obj.State.Properties.RegistrationDate.HighlightColor = Colors.Empty;
    }

    public override bool CanRegister(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanRegister(e);
    }

    public override void ApprovalForm(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.ApprovalForm(e);
    }

    public override bool CanApprovalForm(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValidCached(_obj);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return approvalSheetBySignsIsValid;
    }

    public virtual void ApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var report = Reports.GetApprovalSheetByProcessReport();
      report.Document = _obj;
      report.Open();
    }

    public virtual bool CanApprovalSheetByProcessReportanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool approvalSheetBySignsIsValid = false;
      
      if (e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.TryGetValue("RsvApprovalSheetBySignsIsValid", out approvalSheetBySignsIsValid);
      }
      else
      {
        approvalSheetBySignsIsValid = anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValidCached(_obj);
        e.Params.Add("RsvApprovalSheetBySignsIsValid", approvalSheetBySignsIsValid);
      }
      
      return !approvalSheetBySignsIsValid;
    }

    public virtual void GoToProjectPageanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      _obj.State.Pages.ProjectsPageanorsv.Activate();
    }

    public virtual bool CanGoToProjectPageanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}
