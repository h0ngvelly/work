﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.OfficialDocument.OfficialDocument;
using docRelation = anorsv.RelationModule;

namespace anorsv.OfficialDocument
{
  partial class OfficialDocumentClientHandlers
  {

    public override void DocumentKindValueInput(Sungero.Docflow.Client.OfficialDocumentDocumentKindValueInputEventArgs e)
    {
      base.DocumentKindValueInput(e);
      
      // Если выбран вид документа, установить конфиденциальность в соответствии с настройками вида
      if (e.NewValue != null)
      {
        var docKindConfidential = anorsv.AnorsvMainSolution.DocumentKinds.Get(e.NewValue.Id).Confidentialanorsv;
        if (docKindConfidential != null)
          _obj.ConfidentialDocumentanorsv = docKindConfidential;
      }
      else
        _obj.ConfidentialDocumentanorsv = false;
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      if (!e.Params.Contains("RsvApprovalSheetBySignsIsValid"))
      {
        e.Params.Add("RsvApprovalSheetBySignsIsValid", anorsv.OfficialDocumentModule.PublicFunctions.Module.Remote.ApprovalSheetBySignsIsValid(_obj));
      }
      
      bool projectAccounting = _obj.DocumentKind != null && (_obj.DocumentKind.ProjectsAccounting != null ? _obj.DocumentKind.ProjectsAccounting.Value : false);
      /*
      bool projectExists = false;
      int projectRowCount = _obj.ProjectsCollectionanorsv.Count();
      
      if (_obj.ProjectsCollectionanorsv != null)
        e.AddInformation(string.Format("projectCollection.Count = {0}, projectCollection.Count() = {1}, projectSum = {2}", _obj.ProjectsCollectionanorsv.Count, _obj.ProjectsCollectionanorsv.Count(), _obj.ProjectsCollectionanorsv.Sum(p => p.Project != null ? p.Project.Id : 0)));
      
      foreach (var projectRow in _obj.ProjectsCollectionanorsv)
      {
        if (projectRow.Project != null)
        {
          projectExists = true;
          break;
        }
      }
      
      if (!projectExists)
        e.AddWarning(OfficialDocuments.Resources.ProjectCollectionDontBeEmpty);
       */

      _obj.State.Pages.ProjectsPageanorsv.IsVisible = projectAccounting;
      
      _obj.State.Properties.Project.IsEnabled = false;
      _obj.State.Properties.Project.IsVisible = false;
      
      // Доступность для редактирования свойства "Конфиденциальный документ"
      bool canModifyConfidentialDocument = false;
      if (e.Params.Contains("AnoRsvCanModifyConfidentialDocument"))
      {
        e.Params.TryGetValue("AnoRsvCanModifyConfidentialDocument", out canModifyConfidentialDocument);
      }
      else
      {
        e.Params.AddOrUpdate(
          "AnoRsvCanModifyConfidentialDocument"
          , OfficialDocumentModule.PublicFunctions.Module.Remote.IsConfiditionalDocumentManager(Recipients.As(Sungero.Company.Employees.Current))
         );
      }
      _obj.State.Properties.ConfidentialDocumentanorsv.IsEnabled = canModifyConfidentialDocument;
      
      // Скрыть Изданы изменения для всех кроме приказов и ЛНА
      if (Sungero.RecordManagement.Orders.Is(_obj) || anorsv.DocflowModule.LNAs.Is(_obj))
        _obj.State.Properties.ChangesMadeanorsv.IsVisible = true;
      
      // Проверить возможность изменения связей
      bool canModifyRelations = docRelation.PublicFunctions.Module.Remote.CanChangeDocumentRelations(_obj);
      e.Params.AddOrUpdate("AnoRsvCanModifyRelations", canModifyRelations);
      
      // Проверить возможность добавления связей
      bool canAddRelations = docRelation.PublicFunctions.Module.Remote.CanAddDocumentRelations(_obj);
      e.Params.AddOrUpdate("AnoRsvCanAddRelations", canAddRelations);
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      
      bool projectAccounting = _obj.DocumentKind != null && (_obj.DocumentKind.ProjectsAccounting != null ? _obj.DocumentKind.ProjectsAccounting.Value : false);
      
      /* Запрет сохранять документы без указания проекта/проектов.
      bool projectExists = _obj.ProjectsCollectionanorsv.Any(project => project.Project != null);
      int projectRowCount = _obj.ProjectsCollectionanorsv.Count();
      
      if (_obj.ProjectsCollectionanorsv != null)
        e.AddInformation(string.Format("projectCollection.Count = {0}, projectCollection.Count() = {1}, projectSum = {2}", _obj.ProjectsCollectionanorsv.Count, _obj.ProjectsCollectionanorsv.Count(), _obj.ProjectsCollectionanorsv.Sum(p => p.Project != null ? p.Project.Id : 0)));
      
      foreach (var projectRow in _obj.ProjectsCollectionanorsv)
      {
        if (projectRow.Project != null)
        {
          projectExists = true;
          break;
        }
      }
      
      if (!projectExists)
        e.AddWarning(OfficialDocuments.Resources.ProjectCollectionDontBeEmpty);
       */

      _obj.State.Pages.ProjectsPageanorsv.IsVisible = projectAccounting;
      
      _obj.State.Properties.Project.IsEnabled = false;
      _obj.State.Properties.Project.IsVisible = false;
      
      // Доступность для редактирования свойства "Конфиденциальный документ"
      bool canModifyConfidentialDocument = false;
      if (e.Params.Contains("AnoRsvCanModifyConfidentialDocument"))
      {
        e.Params.TryGetValue("AnoRsvCanModifyConfidentialDocument", out canModifyConfidentialDocument);
      }
      else
      {
        e.Params.AddOrUpdate(
          "AnoRsvCanModifyConfidentialDocument"
          , OfficialDocumentModule.PublicFunctions.Module.Remote.IsConfiditionalDocumentManager(Recipients.As(Sungero.Company.Employees.Current))
         );
      }
      _obj.State.Properties.ConfidentialDocumentanorsv.IsEnabled = canModifyConfidentialDocument;
      
      // Проверить возможность изменения связей.
      bool canModifyRelations = docRelation.PublicFunctions.Module.Remote.CanChangeDocumentRelations(_obj);
      e.Params.AddOrUpdate("AnoRsvCanModifyRelations", canModifyRelations);
      
      // Разрешаем менять автора администраторам или есть активные замещения у текущего пользователя.
      bool authorChangeAllowed = Users.Current.IncludedIn(Roles.GetAll().Where(r => r.Sid == Sungero.Docflow.PublicConstants.Module.RoleGuid.ClerksRole).FirstOrDefault()); //(Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = authorChangeAllowed;
      _obj.State.Properties.PreparedBy.IsEnabled = authorChangeAllowed;
    }
  }
}