﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.OfficialDocument.OfficialDocument;

namespace anorsv.OfficialDocument.Client
{
  partial class OfficialDocumentFunctions
  {
       
    /// <summary>
    /// 
    /// </summary>      
    public bool CheckDocumentLock()
    {
      bool isLocked = Sungero.Docflow.PublicFunctions.Module.IsLocked(_obj);
      bool isVersionLocked = Sungero.Docflow.PublicFunctions.Module.VersionIsLocked(_obj.Versions.ToList());
      if (isLocked || isVersionLocked)
      {
        Dialogs.ShowMessage(Sungero.Docflow.ExchangeDocuments.Resources.ChangeDocumentTypeLockError,
                            MessageType.Error);
      }
      
      return (isLocked || isVersionLocked); 
    }
    
    #region Отправка по email
    
    public override List<Sungero.Docflow.IOfficialDocument> GetRelatedDocumentsWithVersions()
    {
      return base.GetRelatedDocumentsWithVersions();
    }
    
    /// <summary>
    /// Создание письма с вложенными документами.
    /// </summary>
    /// <param name="attachments">Список вложений.</param>
    public virtual void CreateEmailanorsv(List<Sungero.Docflow.IOfficialDocument> attachments, string emailToString)
    {
      //base.CreateEmail(attachments);
      
      var mail = MailClient.CreateMail();
      if (_obj != null)
      {
        mail.Subject = Sungero.Docflow.OfficialDocuments.Resources.SendByEmailSubjectPrefixFormat(_obj.Name);
        mail.AddAttachment(_obj.LastVersion);
      }
      if (attachments != null)
      {
        foreach (var relation in attachments)
          if (relation.HasVersions)
            mail.AddAttachment(relation.LastVersion);
      }
      if (!emailToString.Equals(string.Empty) && emailToString != null)
        mail.To.Add(emailToString);
      mail.Show();
    }
    
    /// <summary>
    /// Выбор связанных документов для отправки и создания письма.
    /// </summary>
    /// <param name="relatedDocuments">Связанные документы.</param>
    public override void SelectRelatedDocumentsAndCreateEmail(List<Sungero.Docflow.IOfficialDocument> relatedDocuments)
    {
      //base.SelectRelatedDocumentsAndCreateEmail(relatedDocuments);
      var emailToDefault = string.Empty;
          
      if (relatedDocuments == null || relatedDocuments.Count == 0)
      {
        if (!this.HaveLastVersionLocks(new List<Sungero.Docflow.IOfficialDocument>() { _obj }))
        {
          if (sline.RSV.OutgoingLetters.Is(_obj))
          {
            var mainDoc = sline.RSV.OutgoingLetters.As(_obj);
            foreach (var addresseeRow in mainDoc.Addressees)
            {
              if ((addresseeRow.Addressee != null) && (addresseeRow.Addressee.Email != null && !addresseeRow.Addressee.Email.Equals(string.Empty)) 
                 && (!emailToDefault.Contains(addresseeRow.Correspondent.Email)))
              {
                emailToDefault += addresseeRow.Addressee.Email + "; ";
              }
              else
              {
                if ((addresseeRow.Correspondent != null) && (addresseeRow.Correspondent.Email != null && !addresseeRow.Correspondent.Email.Equals(string.Empty))
                   && (!emailToDefault.Contains(addresseeRow.Correspondent.Email)))
                  emailToDefault += addresseeRow.Correspondent.Email + "; ";
              }
            }
          }
          this.CreateEmailanorsv(relatedDocuments, emailToDefault);
        }
        
        return;
      }
      
      var dialog = Dialogs.CreateInputDialog(Sungero.Docflow.OfficialDocuments.Resources.SendByEmailDialogTitle);
      dialog.HelpCode = anorsv.OfficialDocument.Constants.Docflow.OfficialDocument.HelpCode.SendByEmail;
      dialog.Text = Sungero.Docflow.OfficialDocuments.Resources.SendByEmailDialogText;
      
      // Заполнить список email по умолчанию для Исходящих
      if (sline.RSV.OutgoingLetters.Is(_obj))
      {
        var mainDoc = sline.RSV.OutgoingLetters.As(_obj);
        foreach (var addresseeRow in mainDoc.Addressees)
        {
          if ((addresseeRow.Addressee != null) && (addresseeRow.Addressee.Email != null && !addresseeRow.Addressee.Email.Equals(string.Empty)) 
              && (!emailToDefault.Contains(addresseeRow.Addressee.Email)))
            emailToDefault += addresseeRow.Addressee.Email + "; ";
          else
            if ((addresseeRow.Correspondent != null) && (addresseeRow.Correspondent.Email != null && !addresseeRow.Correspondent.Email.Equals(string.Empty)) 
                && (!emailToDefault.Contains(addresseeRow.Correspondent.Email)))
              emailToDefault += addresseeRow.Correspondent.Email + "; ";
        }
      }
      
      var emailToString = dialog.AddString(anorsv.OfficialDocument.OfficialDocuments.Resources.SetMailTo, false, emailToDefault);
      emailToString.IsEnabled = true;
      var mainDocument = dialog.AddSelect(Sungero.Docflow.OfficialDocuments.Resources.SendByEmailDialogMainDocument, true, _obj);
      mainDocument.IsEnabled = false;
      var selectedRelations = dialog
        .AddSelectMany(Sungero.Docflow.OfficialDocuments.Resources.SendByEmailDialogAttachments, false, Sungero.Docflow.OfficialDocuments.Null)
        .From(relatedDocuments);
      
      if (dialog.Show() == DialogButtons.Ok)
      {
        var allDocs = selectedRelations.Value.ToList();
        allDocs.Add(_obj);
        if (!this.HaveLastVersionLocks(allDocs))
          this.CreateEmailanorsv(selectedRelations.Value.ToList(), emailToString.Value);
      }
    }
    
    #endregion
    
    #region Регистрация

    /// Удалить параметр NeedValidateRegisterFormat.
    /// </summary>
    /// <param name="document">Документ.</param>
    /// <param name="e">Аргумент действия.</param>
    public static void RemoveNeedValidateRegisterFormatParameter(Sungero.Docflow.IOfficialDocument document,
                                                                 Sungero.Domain.Client.ExecuteActionArgs e)
    {
      // Если документ в процессе верификации, то игнорировать изменение полей регистрационных данных.
      if (document.VerificationState == OfficialDocument.VerificationState.InProcess)
        e.Params.Remove(Sungero.Docflow.Constants.OfficialDocument.NeedValidateRegisterFormat);
    }
    
    /// <summary>
    /// Зарегистрировать документ.
    /// </summary>
    /// <param name="e">Аргумент действия.</param>
    public void Registeranorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if (!e.Validate())
        return;
      
      // Регистрация документа с зарезервированным номером.
      if (_obj.RegistrationState == RegistrationState.Reserved)
      {
        this.RegisterWithReservedNumber(e);
        return;
      }
      
      anorsv.OfficialDocumentModule.PublicFunctions.Module.Registeranorsv(_obj);
      
      return;
    }
    
    #endregion

  }
}