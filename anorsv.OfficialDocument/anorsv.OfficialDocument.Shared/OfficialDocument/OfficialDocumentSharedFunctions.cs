﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Docflow;
using anorsv.OfficialDocument.OfficialDocument;

namespace anorsv.OfficialDocument.Shared
{
  partial class OfficialDocumentFunctions
  {
    
    /// <summary>
    /// Изменение состояния документа для ненумеруемых документов.
    /// Добавлено исключение для типа Приказ.
    /// </summary>
    public override void SetLifeCycleState()
    {
      var documentKind = _obj.DocumentKind;
      var isNotNumerable = documentKind != null &&
        documentKind.NumberingType == Sungero.Docflow.DocumentKind.NumberingType.NotNumerable;
      var isAutoNumerable = documentKind != null &&
        documentKind.NumberingType == Sungero.Docflow.DocumentKind.NumberingType.Numerable &&
        documentKind.AutoNumbering == true;
      var isDraft = _obj.LifeCycleState == null ||
        _obj.LifeCycleState == Sungero.Docflow.OfficialDocument.LifeCycleState.Draft;
      
      // Документ ненумеруемого или автонумеруемого вида сделать действующим, если раньше был черновиком.
      if ((isNotNumerable || isAutoNumerable) && isDraft)
      {
        // Если тип приказ - оставить в разработке
        if (!Sungero.RecordManagement.Orders.Is(_obj))
          _obj.LifeCycleState = LifeCycleState.Active;
      }
      
      // Для нумеруемого или регистрируемого сделать черновиком. Кроме автонумеруемых.
      if (!isNotNumerable && !isDraft && !isAutoNumerable)
        _obj.LifeCycleState = LifeCycleState.Draft;
      
    }

    
    #region Регистрация. 

    [Public]
    public override int GetLeadDocumentId()
    {
      return base.GetLeadDocumentId();
    }
    [Public]
    public override string GetLeadDocumentNumber()
    {
      return base.GetLeadDocumentNumber();
    }
    [Public]
    public override bool CheckRegistrationNumberUnique()
    {
      return base.CheckRegistrationNumberUnique(); 
    }
    
    #endregion

  }
}