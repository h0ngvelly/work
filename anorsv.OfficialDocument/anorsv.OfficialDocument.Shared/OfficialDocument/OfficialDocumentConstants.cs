﻿using System;
using Sungero.Core;

namespace anorsv.OfficialDocument.Constants.Docflow
{
  public static class OfficialDocument
  {

    /// <summary>
    /// Отправка вложением в письмо
    /// </summary>
    public static class HelpCode
    {
      public const string SendByEmail = "Sungero_SendByEmailDialog";
    }

  }
}