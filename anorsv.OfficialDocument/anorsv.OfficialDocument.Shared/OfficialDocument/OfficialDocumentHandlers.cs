using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.OfficialDocument.OfficialDocument;

namespace anorsv.OfficialDocument
{
  partial class OfficialDocumentSharedHandlers
  {

    public override void DocumentKindChanged(Sungero.Docflow.Shared.OfficialDocumentDocumentKindChangedEventArgs e)
    {
      base.DocumentKindChanged(e);
      
      // Если обновили/очистили Вид документа, повторно проверить Тематику
      if (e.NewValue == null || e.NewValue != null && !e.NewValue.Equals(e.OldValue) && _obj.Topicanorsv != null)
        _obj.Topicanorsv = null;
    }

    public virtual void TopicanorsvChanged(anorsv.OfficialDocument.Shared.OfficialDocumentTopicanorsvChangedEventArgs e)
    {
      if (e.NewValue != null && !e.NewValue.Equals(e.OldValue))
      {
        if (_obj.Topicanorsv.Name != "Тематика не обозначена")
        {
          // Предзаполнить Содержание
          if (!string.IsNullOrEmpty(_obj.Topicanorsv.SubjectTemplate))
            _obj.Subject = _obj.Topicanorsv.SubjectTemplate;
          
          // Предзаполнить Подписанта
          if (_obj.Topicanorsv.OurSignatory != null)
          {
            // Если Любой работник, то заполнить инициатора
            if (_obj.Topicanorsv.OurSignatory.Equals(anorsv.AnorsvCoreModule.AnorsvTopicBase.OurSignatory.AnyEmployee))
            {
              _obj.OurSignatory = _obj.PreparedBy;
            }
            else
            {
              // Если РП, то заполнить непосредственного руководителя
              if (_obj.Topicanorsv.OurSignatory.Equals(anorsv.AnorsvCoreModule.AnorsvTopicBase.OurSignatory.DepManager))
                _obj.OurSignatory = _obj.Department.Manager;
            }
          }
        }
      }
      
      // Указание подписанта обязательно если явно не указано обратное в настроках тематики
      _obj.State.Properties.OurSignatory.IsRequired = e.NewValue == null || e.NewValue.OurSignatory != anorsv.DocflowModule.AnorsvGenericTopic.OurSignatory.NotRequired;
    }
  }
}