﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO.Compression;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Shared;
using Sungero.Docflow;
using Newtonsoft.Json;
using sline.RSV;
using anorsv.RecordManagement.Module.RecordManagement;


namespace anorsv.RecordManagement.Module.RecordManagement.Server
{
  partial class ModuleFunctions
  {
    #region БЧП
    /// <summary>
    /// Получить роль Администратор БЧП
    /// </summary>
    /// <returns></returns>
    [Remote(IsPure = true), Public]
    public static IRole GetBCHPAdministratorRole()
    {
      return Roles.GetAll().SingleOrDefault(r => r.Sid == Constants.Module.RolesGuid.BCHPAdministrator);
    }
    
    /// <summary>
    /// Получить значение из параметра в docflow_params.
    /// </summary>
    /// <param name="paramName">Наименование параметра.</param>
    /// <returns>Значение параметра. Тип: bool.</returns>
    [Remote(IsPure = true), Public]
    public static bool GetDocflowParamsBooleanValue(string paramName)
    {
      bool result = false;
      var paramValue = Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(paramName);
      if (!(paramValue is DBNull) && paramValue != null)
        bool.TryParse(paramValue.ToString(), out result);
      return result;
    }

    /// <summary>
    /// Получить значение из параметра в docflow_params.
    /// </summary>
    /// <param name="paramName">Наименование параметра.</param>
    /// <returns>Значение параметра.</returns>
    [Public, Remote(IsPure = true)]
    public static string GetDocflowParamsStringValue(string paramName)
    {
      var paramValue = Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(paramName);
      return !(paramValue is DBNull) && paramValue != null ? paramValue.ToString() : string.Empty;
    }
    
    /// <summary>
    /// Получить значение параметра из Sungero_Docflow_Params.
    /// </summary>
    /// <param name="paramName">Наименование параметра.</param>
    /// <returns>Значение параметра. Тип: int.</returns>
    [Public, Remote(IsPure = true)]
    public static int GetDocflowParamsNumbericValue(string paramName)
    {
      int result = 0;
      var paramValue = Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(paramName);
      if (!(paramValue is DBNull) && paramValue != null)
        int.TryParse(paramValue.ToString(), out result);
      return result;
    }
    
    /// <summary>
    /// Сохранить параметры БЧП.
    /// </summary>
    /// <param name="connectParams">Структура со значениями параметров.</param>
    [Public, Remote]
    public static void SetBCHPParams(anorsv.RecordManagement.Module.RecordManagement.Structures.Module.IBCHPParams bchpParams)
    {
      Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.DefaultBCHPOperator, bchpParams.DefaultBCHPOperatorId.ToString());
      Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailServer, bchpParams.MailServer);
      Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailServerPort, bchpParams.MailServerPort.ToString());
      Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailTo, bchpParams.MailTo);
      Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailToPass, bchpParams.MailToPass);
      Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailToDisplayName, bchpParams.MailToDisplayName);
      Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZip, bchpParams.TestZip.ToString());
      Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZipURL, bchpParams.TestZipURL);
    }
    
    /// <summary>
    /// Получить параметры БЧП.
    /// </summary>
    /// <param name="connectParams">Структура со значениями параметров.</param>
    [Public, Remote]
    public static anorsv.RecordManagement.Module.RecordManagement.Structures.Module.IBCHPParams GetBCHPParams()
    {
      var bchpParams = Structures.Module.BCHPParams.Create();
      bchpParams.DefaultBCHPOperatorId = PublicFunctions.Module.Remote.GetDocflowParamsNumbericValue(Constants.Module.DefaultBCHPOperator);
      bchpParams.MailServer = PublicFunctions.Module.Remote.GetDocflowParamsStringValue(Constants.Module.MailServer);
      bchpParams.MailServerPort = PublicFunctions.Module.Remote.GetDocflowParamsNumbericValue(Constants.Module.MailServerPort);
      bchpParams.MailTo = PublicFunctions.Module.Remote.GetDocflowParamsStringValue(Constants.Module.MailTo);
      bchpParams.MailToPass = PublicFunctions.Module.Remote.GetDocflowParamsStringValue(Constants.Module.MailToPass);
      bchpParams.MailToDisplayName = PublicFunctions.Module.Remote.GetDocflowParamsStringValue(Constants.Module.MailToDisplayName);
      bchpParams.TestZip = PublicFunctions.Module.Remote.GetDocflowParamsBooleanValue(Constants.Module.TestZip);
      bchpParams.TestZipURL = PublicFunctions.Module.Remote.GetDocflowParamsStringValue(Constants.Module.TestZipURL);
      return bchpParams;
    }
    
    #region Отправка электронного письма
    /// <summary>
    /// Отправка электронного письма
    /// </summary>
    /// <param name="mailTo">кому отправляем</param>
    /// <param name="copyTo">Копии</param>
    /// <param name="mailSubject">тема письма</param>
    /// <param name="mailBody">текст письма</param>
    /// <param name="mailHtml">письмо представляет код html</param>
    /// <param name="attachments">вложения</param>
    public void SendMail(string mailTo, System.Net.Mail.MailAddressCollection copyTo, string mailSubject, string mailBody, bool mailHtml, System.Net.Mail.AttachmentCollection attachments)
    {
      var bCHPParams = GetBCHPParams();
      // отправитель - устанавливаем адрес и отображаемое в письме имя
      MailAddress from = new MailAddress(bCHPParams.MailTo, bCHPParams.MailToDisplayName);
      // кому отправляем
      MailAddress to = new MailAddress(mailTo);
      // создаем объект сообщения
      MailMessage message = new MailMessage(from, to);
      //Копии
      foreach (var copy in copyTo)
        message.CC.Add(copy);
      // тема письма
      message.Subject = mailSubject;
      // текст письма
      message.Body = mailBody;
      // письмо представляет код html
      message.IsBodyHtml = mailHtml;
      foreach (var attachment in attachments)
        message.Attachments.Add(attachment);
      // адрес smtp-сервера и порт, с которого будем отправлять письмо
      SmtpClient smtp = new SmtpClient(bCHPParams.MailServer, bCHPParams.MailServerPort);
      // логин и пароль
      smtp.Credentials = new NetworkCredential(bCHPParams.MailTo, bCHPParams.MailToPass);
      smtp.EnableSsl = true;
      smtp.Send(message);
    }
    #endregion
    
    #region Zip
    /// <summary>
    /// Получить тело документа в byte[]
    /// </summary>
    /// <param name="body">Тело документа</param>
    /// <returns>byte[]</returns>
    public byte[] GetByteDocumentBody(Sungero.Domain.Shared.IBinaryData body)
    {
      byte[] memoryArray = null;
      
      using (var memory = new System.IO.MemoryStream())
      {
        body.Read().CopyTo(memory);
        memoryArray = memory.ToArray();
      }
      
      return memoryArray;
    }
    
    /// <summary>
    /// Создать zip архив
    /// </summary>
    /// <param name="document"></param>
    /// <returns></returns>
    [Remote, Public]
    public virtual string CreateZip(IOfficialDocument document) //System.IO.MemoryStream
    {
      var files = new List<anorsv.DocflowModule.Structures.Module.IExportedFile>();
      Sungero.Content.IElectronicDocumentVersions docver;
      //Выгрузим файл последняя версия - 1
      var versions = document.Versions;
      if (versions.Count() > 1)
      {
        docver = versions.Where(v => v.Number == versions.Count - 1).FirstOrDefault();
        var docVerName = String.Format("{0}.{1}", document.Name, docver.AssociatedApplication.Extension.ToString());
        var docName = String.Format("{0}.{1}", document.Name, document.AssociatedApplication.Extension.ToString());
        var fileWord = anorsv.DocflowModule.Structures.Module.ExportedFile.Create(this.GetByteDocumentBody(docver.Body), docVerName,
                                                                                  docver.Created.Value, docver.PublicBody.Size);
        files.Add(fileWord);
        
        //Выгрузим последнюю версию документа
        var filePDF = anorsv.DocflowModule.Structures.Module.ExportedFile.Create(this.GetByteDocumentBody(document.LastVersion.PublicBody), docName,
                                                                                 document.LastVersion.Created.Value, document.LastVersion.PublicBody.Size);
        files.Add(filePDF);
      }
      else
      {
        docver = document.LastVersion;
        files.Add(anorsv.DocflowModule.Structures.Module.ExportedFile.Create(this.GetByteDocumentBody(docver.Body), String.Format("{0}.{1}", document.Name, docver.AssociatedApplication.Extension.ToString()),
                                                                                  docver.Created.Value, docver.Body.Size));
      }
      
      //Выгрузим подпись на последней версии документа
      var signature = Signatures.Get(document.LastVersion).FirstOrDefault();
      var signBody = signature.GetDataSignature();
      var signFullFileName = signature.SignatoryFullName + ".SGN";
      var fileSGN = anorsv.DocflowModule.Structures.Module.ExportedFile.Create(signBody, signFullFileName,
                                                                               signature.SignCertificate.NotBefore.Value, signBody.LongLength);
      files.Add(fileSGN);
      
      var zipModel = anorsv.DocflowModule.Structures.Module.ZipModel.Create();
      zipModel.Files = files;
      return anorsv.DocflowModule.PublicFunctions.Module.CreateZipArchive(zipModel, 6, document.Name);;
    }
    #endregion
    
    /// <summary>
    /// Отправка исходящего письма
    /// </summary>
    /// <param name="outgoingLetterDocument">Документ исходящее письмп. БЧП</param>
    [Public]
    public void BCHPMailSend(IOutgoingLetter outgoingLetterDocument)
    {
      try
      {
        var subject = anorsv.RecordManagement.Module.RecordManagement.Resources.BCHPMailSubjectFormat(outgoingLetterDocument.RegistrationNumber, outgoingLetterDocument.RegistrationDate.Value.Date.ToString("D"));
        var attachmentBytes = Convert.FromBase64String(this.CreateZip(Sungero.Docflow.OfficialDocuments.As(outgoingLetterDocument)));
        var zip = new System.IO.MemoryStream(attachmentBytes);
        MailAddressCollection copyList = new MailAddressCollection();
        copyList.Add(outgoingLetterDocument.BCHPDocParamsanorsv.Participant.Email);
        AttachmentCollection attList = new MailMessage().Attachments;
        attList.Add(new Attachment(zip, outgoingLetterDocument.Name + ".zip"));
        var preparedBy = outgoingLetterDocument.PreparedBy;
        var mailBody = String.Format(Encoding.UTF8.GetString(Convert.FromBase64String(Resources.MailBody)),preparedBy.Name,preparedBy.JobTitle, preparedBy.Phone, preparedBy.Email,Resources.RSVLogo,
                                     Resources.RSVtelegram, Resources.RSVVk, Resources.RSVOk, Resources.RSVYouTube, Resources.RSVRuTube);
        this.SendMail(outgoingLetterDocument.BCHPDocParamsanorsv.CompanyEmail, copyList, subject, mailBody, true, attList);
        Logger.Debug(anorsv.RecordManagement.Module.RecordManagement.Resources.BCHPLetterSentFormat(outgoingLetterDocument.Name));
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(anorsv.RecordManagement.Module.RecordManagement.Resources.BCHPMailSendErrorFormat(ex.Message, ex.StackTrace));
        var performerList = Roles.GetAll().FirstOrDefault(r => r.Sid == Constants.Module.RolesGuid.BCHPAdministrator).RecipientLinks;
        var task = Sungero.Workflow.SimpleTasks.Create();
        task.AssignmentType = Sungero.Workflow.SimpleTask.AssignmentType.Notice;
        task.Subject = anorsv.RecordManagement.Module.RecordManagement.Resources.ErrorSendingEmail;
        task.ActiveText = anorsv.RecordManagement.Module.RecordManagement.Resources.ErrorLogFormat(ex.Message, ex.StackTrace);
        var step = task.RouteSteps.AddNew();
        step.AssignmentType = task.AssignmentType;
        foreach (var performer in performerList)
        {
          step.Performer = performer.Member;
        }
        step.Deadline = null;
        task.Start();
      }
    }
    
    /// <summary>
    /// Получить должность руководителя по локализации
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    [Public]
    public Enumeration GetJobTitle(string name)
    {
      switch (name)
      {
        case "Исполняющий обязанности директора":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.ActingDirector;
        case "Исполняющий обязанности генерального директора":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.ActingGeneralDi;
        case "Исполняющий обязанности руководителя":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.ActingManager;
        case "Исполняющий обязанности ректора":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.ActingRector;
        case "Генеральный директор":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.CEO;
        case "Директор":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.Director;
        case "Начальник":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.Head;
        case "Ректор":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.Rector;
        case "Руководитель":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.JobTitle.Supervisor;
        default:
          throw new Exception("Invalid value for JobTitle");
      }
    }
    
    /// <summary>
    /// Получить статус участника БЧП
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    [Public]
    public Enumeration ParticipantStatus(string name)
    {
      switch (name)
      {
        case "Студент":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.ParticipantStatus.Student;
        case "Учащийся":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.ParticipantStatus.Learner;
        case "Сотрудник":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.ParticipantStatus.Employee;
        default:
          throw new Exception("Invalid value for ParticipantStatus");
      }
    }
    
    /// <summary>
    /// Получить перечисление по локализованному имени
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    [Public]
    public Enumeration Gender(string name)
    {
      switch (name)
      {
        case "Мужской":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.Gender.Male;
        case "Женский":
          return anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchp.Gender.Female;
        default:
          throw new Exception("Invalid value for Gender");
      }
    }
    
    /// <summary>
    /// Функция интеграции БЧП
    /// </summary>
    /// <param name="postData">base64string</param>
    [Public(WebApiRequestType = RequestType.Post)]
    public string BCHPOutgoingLetter(string postData)
    {
      try
      {
        var BCHPOutgoingLetterStructure = JsonConvert.DeserializeObject<Structures.Module.BCHPOutgoingLetter>(Encoding.UTF8.GetString(Convert.FromBase64String(postData)));
        
        var person = GetOrCreatePerson(BCHPOutgoingLetterStructure.CandidateData);
        
        //Создать запись справочник с данными исх. письма
        var docParam = anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchps.Create();
        docParam.CompanyName = BCHPOutgoingLetterStructure.DocData.orgName;
        docParam.CompanyEmail = BCHPOutgoingLetterStructure.DocData.orgEmail;
        var companyManagerName = BCHPOutgoingLetterStructure.DocData.orgHeaderFIO.Split(' ');
        docParam.LastName = companyManagerName[0];
        docParam.FirstName = companyManagerName[1];
        docParam.MiddleName = companyManagerName[2];
        docParam.JobTitle = this.GetJobTitle(BCHPOutgoingLetterStructure.DocData.orgHeaderJobTitle);
        docParam.Gender = this.Gender(BCHPOutgoingLetterStructure.DocData.gender);
        docParam.Participant = person;
        docParam.ParticipantStatus = this.ParticipantStatus(BCHPOutgoingLetterStructure.CandidateData.candidateJobTitle);
        docParam.GroupClass = BCHPOutgoingLetterStructure.CandidateData.studentGroup;
        docParam.ParticipantID = BCHPOutgoingLetterStructure.CandidateData.guid.ToString();
        docParam.Direction = Sungero.Commons.Cities.GetAll().FirstOrDefault(c => c.Name.Contains(BCHPOutgoingLetterStructure.DocData.travelСity));
        docParam.TravelPeriod = BCHPOutgoingLetterStructure.DocData.travelDates;
        docParam.DateFrom = DateTime.Parse(BCHPOutgoingLetterStructure.DocData.travelDateFrom);
        docParam.DateTo = DateTime.Parse(BCHPOutgoingLetterStructure.DocData.travelDatеTo);
        docParam.Save();
        
        //Создадим исходящее письмо
        var outgoingLetter = sline.RSV.OutgoingLetters.Create();
        var outgoingLetterKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.BCHPOutgoingLetterKind);
        outgoingLetter.DocumentKind = outgoingLetterKind;
        outgoingLetter.Subject = anorsv.RecordManagement.Module.RecordManagement.Resources.InformationMail;
        var bchpOperator = GetBCHPOperatorEmployee(BCHPOutgoingLetterStructure.DocData.bchpOperatorGuid);
        if(bchpOperator == null)
        {
          var employeeId = Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(Constants.Module.DefaultBCHPOperator).ToString();
          if (!String.IsNullOrEmpty(employeeId))
            bchpOperator = Sungero.Company.Employees.Get(int.Parse(employeeId));
        }
        outgoingLetter.Author = bchpOperator;
        outgoingLetter.Correspondent = Sungero.Parties.Counterparties.As(person);
        outgoingLetter.BCHPDocParamsanorsv = docParam;
        outgoingLetter.BusinessUnit = bchpOperator.Department.BusinessUnit;
        outgoingLetter.Department = bchpOperator.Department;
        outgoingLetter.PreparedBy = bchpOperator;
        outgoingLetter.SignESanorsv = sline.RSV.OutgoingLetter.SignESanorsv.SignES;
        outgoingLetter.DeliveryMethod = Sungero.Docflow.MailDeliveryMethods.GetAll(m => m.Name == Sungero.Docflow.MailDeliveryMethods.Resources.EmailMethod).FirstOrDefault();
        outgoingLetter.CaseFile = anorsv.AnorsvMainSolution.CaseFiles.GetAll().FirstOrDefault(c => c.DocKindCollectionanorsv.Any(d => Equals(d.DocKind, outgoingLetterKind)));
        outgoingLetter.Save();
        
        //Сформировать тело документа по шаблону
        var template = Sungero.Content.ElectronicDocumentTemplates.As(Sungero.Docflow.DocumentTemplates.GetAll().FirstOrDefault(t => t.DocumentKinds.Any(k => Equals(k.DocumentKind, outgoingLetterKind))
                                                                                                                && t.Status == Sungero.Docflow.DocumentTemplate.Status.Active));
        using (var body = template.LastVersion.Body.Read())
        {
          var newVersion = outgoingLetter.CreateVersionFrom(body, template.AssociatedApplication.Extension);
          var internalEntity = (Sungero.Domain.Shared.IExtendedEntity)outgoingLetter;
          internalEntity.Params[Sungero.Content.Shared.ElectronicDocumentUtils.FromTemplateIdKey] = template.Id;
          outgoingLetter.Save();
        }
        
        //Отправка на согласование
        var task = anorsv.ApprovalTask.ApprovalTasks.Create(); //Sungero.Docflow.PublicFunctions.Module.Remote.CreateApprovalTask(outgoingLetter);
        task.DocumentGroup.All.Add(outgoingLetter);
        var user = Users.GetAll().FirstOrDefault(u => Equals(u.Login, bchpOperator.Login));
        if (user == null)
          user = Users.Current;
        task.Author = user;
        task.Start();
        
        return "ok";
      }
      catch(Exception ex)
      {
        Logger.ErrorFormat(anorsv.RecordManagement.Module.RecordManagement.Resources.BCHPImportErrorFormat(ex.Message, ex.StackTrace));
        return ex.Message;
      }
    }
    
    #region External link
    /// <summary>
    /// Создать external link.
    /// </summary>
    /// <param name="entity">Сущность.</param>
    /// <param name="entityId">ИД экземпляра, созданного при инициализации.</param>
    [Public]
    public static void CreateExternalLink(IEntity entity, Guid entityId)
    {
      var link = Sungero.Commons.ExternalEntityLinks.Create();
      link.EntityId = entity.Id;
      link.EntityType = entity.GetEntityMetadata().NameGuid.ToString();
      link.ExtEntityId = entityId.ToString();
      link.ExtSystemId = Constants.Module.ExternalLinkSystem;
      link.Name = Constants.Module.ExternalLinkSystem;
      link.IsDeleted = false;
      link.Save();
      
      Logger.Debug(anorsv.RecordManagement.Module.RecordManagement.Resources.CreateExternalLinkFormat(link.Id, entityId));
    }
    
    /// <summary>
    /// Проверить наличие отметки о событии.
    /// </summary>
    /// <param name="eventId">Id события.</param>
    /// <param name="name">Имя события.</param>
    /// <returns>True, если отметка есть, иначе False.</returns>
    public static bool HasExternalLink(Guid eventId, string name)
    {
      return Sungero.Commons.ExternalEntityLinks.GetAll().Where(x => string.Equals(x.ExtEntityId, eventId.ToString()) &&  x.ExtSystemId == name).Any();
    }
    
    /// <summary>
    /// Получить внешнюю ссылку
    /// </summary>
    /// <param name="eventId">Id события.</param>
    /// <param name="name">Имя события.</param>
    /// <returns>Список внешних ссылок</returns>
    public IQueryable<Sungero.Commons.IExternalEntityLink> GetExternalLink(Guid eventId, string name)
    {
      return Sungero.Commons.ExternalEntityLinks.GetAll().Where(x => (x.ExtEntityId == eventId.ToString()) &&
                                                                (x.ExtSystemId == name));
    }
    
    /// <summary>
    /// Получить сотрудника(оператора БЧП)
    /// </summary>
    /// <param name="guid">Guid оператора</param>
    /// <returns>Сотрудник</returns>
    public Sungero.Company.IEmployee GetBCHPOperatorEmployee(Guid guid)
    {
      var links = GetExternalLink(guid, Constants.Module.EmpExternalLinkSystem).FirstOrDefault();
      var employee = Sungero.Company.Employees.Null;
      
      if (links != null)
        employee = Sungero.Company.Employees.GetAll().FirstOrDefault(emp => emp.Id == links.EntityId);
      
      
      return employee;
    }
    
    /// <summary>
    /// Получить или создать персону
    /// </summary>
    /// <param name="candidateData">Структура кандидата</param>
    /// <returns>Контрагент</returns>
    public Sungero.Parties.IPerson GetOrCreatePerson(Structures.Module.candidateData candidateData)
    {
      var links = GetExternalLink(candidateData.guid, Constants.Module.ExternalLinkSystem).FirstOrDefault();
      var person = Sungero.Parties.People.Null;
      if (links != null)
        person = Sungero.Parties.People.GetAll().FirstOrDefault(c => c.Id == links.EntityId);
      else
      {
        person = Sungero.Parties.People.Create();
        person.LastName = candidateData.lastName;
        person.FirstName = candidateData.fistName;
        person.MiddleName = candidateData.midleName;
        person.Email = candidateData.email;
        person.Phones = candidateData.phone;
        person.Save();
        
        CreateExternalLink(person, candidateData.guid);
        Logger.Debug(anorsv.RecordManagement.Module.RecordManagement.Resources.CreatedNewPersonFormat(person.Name));
      }
      
      return person;
    }
    #endregion
    
    #endregion
  }
}
