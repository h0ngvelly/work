﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace anorsv.RecordManagement.Module.RecordManagement.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      base.Initializing(e);
      
//      UpgradeDocumentKinds();
//      CreateRoles();
//      BchpAdministratorAccessRights();
//      BchpRecordManagerAccessRights();
      //BCHPDefaultParams();
    }
    
    
    public static void BCHPDefaultParams()
    {
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.DefaultBCHPOperator) == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.DefaultBCHPOperator, "0");
//      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZip) == null)      
//        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZip, false.ToString());
//      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZipURL) == null)      
//        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZipURL, "");
        
        //        var task = Sungero.Workflow.SimpleTasks.Create();
        //        task.Subject = "Оператор БЧП";
        //        task.Texts = "Необходимо установить оператора БЧП по умолчанию в модуле Делопроизводства";
        //        task.
      
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailServer) == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailServer, "");
      
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailServerPort) == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailServerPort, "");
      
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailTo) == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailTo, "");
      
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailToPass) == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailToPass, "");
      
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailToDisplayName) == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.MailToDisplayName, "");
      
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZip) == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZip, false.ToString());
      
      if(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZipURL) == null)
        Sungero.Docflow.PublicFunctions.Module.InsertOrUpdateDocflowParam(anorsv.RecordManagement.Module.RecordManagement.Constants.Module.TestZipURL, "");
      
      //        var task = Sungero.Workflow.SimpleTasks.Create();
      //        task.Subject = "Оператор БЧП";
      //        task.Texts = "Необходимо установить оператора БЧП по умолчанию в модуле Делопроизводства";
      //        task.
      
    }
    
    /// <summary>
    /// Обновление видов документов
    /// </summary>
    public static void UpgradeDocumentKinds()
    {
      // Больше чем путешествие
      var bCHPOutgoingLetterKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(Constants.Module.DocKind.BCHPOutgoingLetterKind);
      if (bCHPOutgoingLetterKind == null)
      {
        bCHPOutgoingLetterKind = Sungero.Docflow.DocumentKinds.GetAll().Where(k => k.Name == "Исходящие \"Больше, чем путешествие\"" && k.Status == Sungero.Docflow.DocumentKind.Status.Active).FirstOrDefault();
        if (bCHPOutgoingLetterKind != null)
          Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(bCHPOutgoingLetterKind, Constants.Module.DocKind.BCHPOutgoingLetterKind);
      }
      
    }
    
    /// <summary>
    /// Создание ролей
    /// </summary>
    public static void CreateRoles()
    {
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(anorsv.RecordManagement.Module.RecordManagement.Resources.BCHPAdministrator, anorsv.RecordManagement.Module.RecordManagement.Resources.BCHPAdministrator, Constants.Module.RolesGuid.BCHPAdministrator);
      //Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(anorsv.RecordManagement.Module.RecordManagement.Resources.BCHPSignatory, anorsv.RecordManagement.Module.RecordManagement.Resources.BCHPSignatory, Constants.Module.RolesGuid.BCHPSignatory);
    }
    
    /// <summary>
    /// Выдать полные права Администратору БЧП
    /// </summary>
    public static void BchpAdministratorAccessRights()
    {
      var role = Roles.GetAll().FirstOrDefault(r => r.Sid == Constants.Module.RolesGuid.BCHPAdministrator);
      anorsv.AnorsvCoreModule.DocumentExtraParamBases.AccessRights.Grant(role, DefaultAccessRightsTypes.FullAccess);
      anorsv.AnorsvCoreModule.DocumentExtraParamBases.AccessRights.Save();
      
      InitializationLogger.Debug(anorsv.RecordManagement.Module.RecordManagement.Resources.RightsBCHPAdministrator);
    }
    
    /// <summary>
    /// Выдать права Делопроизводителям на просмотр
    /// </summary>
    public static void BchpRecordManagerAccessRights()
    {
      var role = Sungero.Docflow.PublicFunctions.DocumentRegister.Remote.GetClerks();
      anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchps.AccessRights.Grant(role, DefaultAccessRightsTypes.Read);
      anorsv.OfficialDocumentModule.DocumentExtraParamOtgoingLetterBchps.AccessRights.Save();
      
      InitializationLogger.Debug(anorsv.RecordManagement.Module.RecordManagement.Resources.RightsClerksRole);
    }
  }
}
