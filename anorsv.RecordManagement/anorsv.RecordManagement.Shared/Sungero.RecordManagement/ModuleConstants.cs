﻿using System;
using Sungero.Core;

namespace anorsv.RecordManagement.Module.RecordManagement.Constants
{
  public static class Module
  {
    /// <summary>
    /// Код системы у external link для данных импорта данных.
    /// </summary>
    [Sungero.Core.Public]
    public const string ExternalLinkSystem = "importBchp";
    
    /// <summary>
    /// Код системы у external link для данных оператора БЧП.
    /// </summary>
    [Sungero.Core.Public]
    public const string EmpExternalLinkSystem = "EmpimportBchp";
    
    /// <summary>
    /// ИД сотрудника оператора БЧП по умолчанию.
    /// </summary>
    [Sungero.Core.Public]
    public const string DefaultBCHPOperator = "DefaultBCHPOperator";
    
    [Sungero.Core.Public]
    public const string TestZip = "TestZip";
    
    [Sungero.Core.Public]
    public const string TestZipURL = "TestZipURL";
    
    [Sungero.Core.Public]
    public const string MailServer = "MailServer";
    
    [Sungero.Core.Public]
    public const string MailServerPort = "MailServerPort";
    
    [Sungero.Core.Public]
    public const string MailTo = "MailTo";
    
    [Sungero.Core.Public]
    public const string MailToPass = "MailToPass";
    
    [Sungero.Core.Public]
    public const string MailToDisplayName = "MailToDisplayName";
    
    
    public static class DocKind
    {
      /// <summary>
      /// "Исходящее "Больше чем путешествие"
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid BCHPOutgoingLetterKind = Guid.Parse("AFFDAEAF-1641-4BBE-B620-67D40D717C01");
    }
    
    public static class RolesGuid
    {
      /// <summary>
      /// Администратор БЧП
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid BCHPAdministrator = Guid.Parse("F2D97BCE-9AFC-4DB6-B747-924E4DDDFF65");
      
      /// <summary>
      /// Подписывающий БЧП
      /// </summary>
      [Sungero.Core.Public]
      public static readonly Guid BCHPSignatory = Guid.Parse("9615B654-ACD2-4ED0-B728-8ADBA3705D23");
    }
  }
}