﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.RecordManagement.Module.RecordManagement.Structures.Module
{
  /// <summary>
  /// Параметры БЧП
  /// </summary>
  [Public]
  partial class BCHPParams
  {
    public int DefaultBCHPOperatorId { get; set; }
    
    public bool TestZip { get; set; }
    
    public string MailServer { get; set; }
    
    public string TestZipURL { get; set; }
    
    public int MailServerPort { get; set; }
    
    public string MailTo { get; set; }
    
    public string MailToPass { get; set; }
    
    public string MailToDisplayName { get; set; }
  }
  
  partial class BCHPOutgoingLetter
  {
    public anorsv.RecordManagement.Module.RecordManagement.Structures.Module.docData DocData { get; set; }
    public anorsv.RecordManagement.Module.RecordManagement.Structures.Module.candidateData CandidateData { get; set; }
  }  
  
  partial class docData
  {
    public string orgName { get; set; }
    public string orgHeaderFIO { get; set; }
    public string orgHeaderJobTitle { get; set; }
    public string orgEmail { get; set; }
    public string travelDates { get; set; }
    public string travelDateFrom { get; set; }
    public string travelDatеTo { get; set; }
    public string travelСity { get; set; }
    public string gender { get; set; }
    public Guid bchpOperatorGuid { get; set; }    
  }
  
  partial class candidateData
  {
    public string lastName { get; set; }
    public string fistName { get; set; }
    public string midleName { get; set; }
    public string studentGroup { get; set; }
    public string candidateJobTitle { get; set; }
    public string category { get; set; }
    public string phone { get; set; }
    public string email { get; set; }
    public Guid guid { get; set; }    
  }

}
