﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.PersonnelDocumentModule.PersonnelDocument;

namespace anorsv.PersonnelDocumentModule.Client
{
  partial class PersonnelDocumentActions
  {
    public virtual void ChangeTypeTest(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var docKind = anorsv.AnorsvMainSolution.DocumentKinds.GetAll()
        .Where(d => d.DocumentType.DocumentTypeGuid == "3396e2cb-a5d0-43f5-962e-299ab231d56e").First();
      
          _obj.ConvertTo(anorsv.PersonnelDocumentModule.StructuralUnitRegulations.Info);
          _obj.DocumentKind = docKind;
          if (docKind.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignESanorsv))
            _obj.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES;
          if (docKind.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignOnPaper))
            _obj.SignESanorsv = anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignOnPaper;
          _obj.Save();
            
    }

    public virtual bool CanChangeTypeTest(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public virtual void Action(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      
    }

    public virtual bool CanAction(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}