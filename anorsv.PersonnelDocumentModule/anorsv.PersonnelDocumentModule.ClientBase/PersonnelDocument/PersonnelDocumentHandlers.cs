﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.PersonnelDocumentModule.PersonnelDocument;

namespace anorsv.PersonnelDocumentModule
{
  partial class PersonnelDocumentClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      bool res1 = (Users.Current.IncludedIn(Roles.Administrators) || Substitutions.ActiveSubstitutedUsers.Any());
      _obj.State.Properties.Author.IsEnabled = res1;
    }

  }
}