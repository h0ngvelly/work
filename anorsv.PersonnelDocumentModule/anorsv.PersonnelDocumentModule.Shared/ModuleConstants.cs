﻿using System;
using Sungero.Core;

namespace anorsv.PersonnelDocumentModule.Constants
{
  public static class Module
  {
    public static class Initialize
    {
      // Виды документов
      [Sungero.Core.Public]
      public static readonly Guid DocKind = Guid.Parse("14a59623-89a2-4ea8-b6e9-2ad4365f358c");
      [Sungero.Core.Public]
      public static readonly Guid JobDescriptionKind = Guid.Parse("465037A1-9CCF-4134-A430-B2801363F25F");
      [Sungero.Core.Public]
      public static readonly Guid StructuralUnitRegulationKind = Guid.Parse("79AAF768-5FB5-43E4-A58C-61DB45D7DCE1");
     
    }
  }
}