﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.PersonnelDocumentModule.JobDescription;

namespace anorsv.PersonnelDocumentModule
{
  partial class JobDescriptionServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      var documentTypes = Sungero.Docflow.DocumentKinds.GetAll()
        .Where(d => d.DocumentType.DocumentTypeGuid == "3396e2cb-a5d0-43f5-962e-299ab231d56e");
      if (documentTypes.Count() == 1)
        _obj.DocumentKind = documentTypes.First();
    }
  }

}