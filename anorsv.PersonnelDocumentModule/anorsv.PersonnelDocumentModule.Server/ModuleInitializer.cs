﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;
using Sungero.Domain;
using Sungero.Metadata;

namespace anorsv.PersonnelDocumentModule.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      CreateDocumentTypes();
      UpdateDocumentKinds();
      GrantAccessRightsForDocuments();
      ChangePersonnelDocTypes();
    }
    
    public static void CreateDocumentTypes()
    {
      InitializationLogger.Debug("PersonnelDocument Init: create Personnel documents types.");
      
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Положение о структурном подразделении", StructuralUnitRegulation.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Inner, true);
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Должностная инструкция работника", JobDescription.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Inner, true);
    }
    
    public static void UpdateDocumentKinds()
    {
      InitializationLogger.Debug("PersonnelDocument Init: update Personnel documents kinds: Положение о структурном подразделении.");      
      var structuralUnitRegulationsEntityId = anorsv.PersonnelDocumentModule.Constants.Module.Initialize.StructuralUnitRegulationKind;
      var structuralUnitRegulationsExternalLink = Sungero.Docflow.PublicFunctions.Module.GetExternalLink(anorsv.PersonnelDocumentModule.Constants.Module.Initialize.DocKind, structuralUnitRegulationsEntityId);
      if (structuralUnitRegulationsExternalLink == null)
      {
        var docKindStructuralUnit = anorsv.AnorsvMainSolution.DocumentKinds
          .GetAll(k => k.Name == "Положение о структурном подразделении").FirstOrDefault();
        Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(docKindStructuralUnit, structuralUnitRegulationsEntityId);
      }
      
      InitializationLogger.Debug("PersonnelDocument Init: update Personnel documents kinds: Должностная инструкция работника.");      
      var jobDescriptionsEntityId = anorsv.PersonnelDocumentModule.Constants.Module.Initialize.JobDescriptionKind;      
      var jobDescriptionsExternalLink = Sungero.Docflow.PublicFunctions.Module.GetExternalLink(anorsv.PersonnelDocumentModule.Constants.Module.Initialize.DocKind, jobDescriptionsEntityId);
      if (jobDescriptionsExternalLink == null)
      {
        var docKindJobDescr = anorsv.AnorsvMainSolution.DocumentKinds
          .GetAll(k => k.Name == "Должностная инструкция работника").FirstOrDefault();
        Sungero.Docflow.PublicFunctions.Module.CreateExternalLink(docKindJobDescr, jobDescriptionsEntityId);
      }
    }
    
    public void GrantAccessRightsForDocuments()
    {
      InitializationLogger.Debug("PersonnelDocument Init: Grant access rights for documents.");
      var allUsers = Roles.AllUsers;
      if (allUsers != null)
      {
        // документы
        InitializationLogger.Debug("Init: Grant access rights for Положение о структурном подразделении.");
        if (!StructuralUnitRegulations.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Create, allUsers))
        {
          StructuralUnitRegulations.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Create);
          StructuralUnitRegulations.AccessRights.Save();
        }

        if (!JobDescriptions.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Create, allUsers))
        {
          InitializationLogger.Debug("Init: Grant access rights for Должностная инструкция работника.");
          JobDescriptions.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Create);
          JobDescriptions.AccessRights.Save();
        }
      }
    }
    
    public void ChangePersonnelDocTypes()
    {
      InitializationLogger.Debug("PersonnelDocument Init: change doc type in Document kinds.");
      
      // Изменить тип документа в карточках вида
      bool docKindCorrect = true;
      
      var jobDescriptionsEntityId = anorsv.PersonnelDocumentModule.Constants.Module.Initialize.JobDescriptionKind;   
      var jobDescriptionsExternalLink = Sungero.Docflow.PublicFunctions.Module.GetExternalLink(anorsv.PersonnelDocumentModule.Constants.Module.Initialize.DocKind, jobDescriptionsEntityId);
      var docKindJobDescr = anorsv.AnorsvMainSolution.DocumentKinds
        .GetAll(k => k.Id == jobDescriptionsExternalLink.EntityId).FirstOrDefault();
      //  .GetAll(k => k.Name == "Должностная инструкция работника").FirstOrDefault();
      var docTypeJobDescr = Sungero.Docflow.DocumentTypes
        .GetAll(t => t.DocumentTypeGuid == JobDescription.ClassTypeGuid.ToString()).FirstOrDefault();
      
      var structuralUnitRegulationsEntityId = anorsv.PersonnelDocumentModule.Constants.Module.Initialize.StructuralUnitRegulationKind;
      var structuralUnitRegulationsExternalLink = Sungero.Docflow.PublicFunctions.Module.GetExternalLink(anorsv.PersonnelDocumentModule.Constants.Module.Initialize.DocKind, structuralUnitRegulationsEntityId);
      var docKindStructuralUnit = anorsv.AnorsvMainSolution.DocumentKinds
        .GetAll(k => k.Id == structuralUnitRegulationsExternalLink.EntityId).FirstOrDefault();
      //  .GetAll(k => k.Name == "Положение о структурном подразделении").FirstOrDefault();
      var docTypeStructuralUnit = Sungero.Docflow.DocumentTypes
        .GetAll(t => t.DocumentTypeGuid == StructuralUnitRegulation.ClassTypeGuid.ToString()).FirstOrDefault();
      
      if (docKindJobDescr == null)
        docKindCorrect = false;
      else if (!docKindJobDescr.DocumentType.Equals(docTypeJobDescr))
      {
        try
        {
          docKindJobDescr.DocumentType = docTypeJobDescr;
          docKindJobDescr.Save();
          InitializationLogger.Debug("PersonnelDocument Init: '" + docKindJobDescr.Name + "' kind changed.");
        }
        catch(Exception ex)
        {
          docKindCorrect = false;
          InitializationLogger.Debug("Init EXCEPTION: error in type changing for document kind " + docKindJobDescr.Name + ": " + ex.Message);
        }
      }
      if (docTypeStructuralUnit == null) 
        docKindCorrect = false;
      else if (!docKindStructuralUnit.DocumentType.Equals(docTypeStructuralUnit))
      {
        try
        {
          docKindStructuralUnit.DocumentType = docTypeStructuralUnit;
          docKindStructuralUnit.Save();
          InitializationLogger.Debug("PersonnelDocument Init: '" + docKindStructuralUnit.Name + "' kind changed.");
        }
        catch(Exception ex)
        {
          docKindCorrect = false;
          InitializationLogger.Debug("Init EXCEPTION: error in type changing for document kind " + docKindStructuralUnit.Name + ": " + ex.Message);
        }
      }
      
      if (docKindCorrect == true)
      {
        InitializationLogger.Debug("PersonnelDocument Init: change doc type in Simple documents.");
        
        var signESStructuralUnit = (docKindStructuralUnit.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignESanorsv))
          ? anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES
          : anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignOnPaper;
        var signESJobDescr = (docKindJobDescr.SignESanorsv.Equals(anorsv.AnorsvMainSolution.DocumentKind.SignESanorsv.DefSignESanorsv))
          ? anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignES
          : anorsv.OfficialDocument.OfficialDocument.SignESanorsv.SignOnPaper;
        
        var strUnitDocumentIds = Sungero.Docflow.SimpleDocuments
          .GetAll(d => d.DocumentKind.Equals(docKindStructuralUnit))
          .Select(d => d.Id)
          .ToList();
        var jobDescrDocumentIds = Sungero.Docflow.SimpleDocuments
          .GetAll(d => d.DocumentKind.Equals(docKindJobDescr))
          .Select(d => d.Id)
          .ToList();
        
        int currentProcessedDocumentsCount = 0;
        int totalDocumentsCount = strUnitDocumentIds.Count + jobDescrDocumentIds.Count;
        int debugCount = 50;
        
        foreach (var documentId in strUnitDocumentIds)
        {
          try
          {
            var document = Sungero.Docflow.SimpleDocuments.Get(documentId);
            var convertedDocument = anorsv.PersonnelDocumentModule.PersonnelDocuments.As(document.ConvertTo(anorsv.PersonnelDocumentModule.StructuralUnitRegulations.Info));
            convertedDocument.DocumentKind = docKindStructuralUnit;
            convertedDocument.SignESanorsv = signESStructuralUnit;
            convertedDocument.Save();
            currentProcessedDocumentsCount++;
          }
          catch (Exception ex)
          {
            InitializationLogger.Debug("Init EXCEPTION: error in type changing for document " + documentId + ": " + ex.Message);
          }
          
          if (currentProcessedDocumentsCount == debugCount)
          {
            InitializationLogger.DebugFormat("{0} of {1} documents converted.", currentProcessedDocumentsCount, totalDocumentsCount);
            debugCount = debugCount + 50;
          }
        }
        
        foreach (var documentId in jobDescrDocumentIds)
        {
          try
          {
            var document = Sungero.Docflow.SimpleDocuments.Get(documentId);
            var convertedDocument = anorsv.PersonnelDocumentModule.PersonnelDocuments.As(document.ConvertTo(anorsv.PersonnelDocumentModule.JobDescriptions.Info));
            convertedDocument.DocumentKind = docKindJobDescr;
            convertedDocument.SignESanorsv = signESJobDescr;
            convertedDocument.Save();
            currentProcessedDocumentsCount++;
          }
          catch (Exception ex)
          {
            InitializationLogger.Debug("Init EXCEPTION: error in type changing for document " + documentId + ": " + ex.Message);
          }
          
          if (currentProcessedDocumentsCount == debugCount)
          {
            InitializationLogger.DebugFormat("{0} of {1} documents converted.", currentProcessedDocumentsCount, totalDocumentsCount);
            debugCount = debugCount + 50;
          }
        }
        if (currentProcessedDocumentsCount % 50 != 0)
        {
          InitializationLogger.DebugFormat("{0} of {1} documents converted.", currentProcessedDocumentsCount, totalDocumentsCount);
        }
        
        InitializationLogger.Debug("Init RESULT: " + currentProcessedDocumentsCount + " simple documents converted");
      }
    }
  }
}
