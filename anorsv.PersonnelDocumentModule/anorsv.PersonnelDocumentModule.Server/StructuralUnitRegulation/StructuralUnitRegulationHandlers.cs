﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.PersonnelDocumentModule.StructuralUnitRegulation;

namespace anorsv.PersonnelDocumentModule
{
  partial class StructuralUnitRegulationServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      var documentTypes = Sungero.Docflow.DocumentKinds.GetAll()
        .Where(d => d.DocumentType.DocumentTypeGuid == "ef3e511a-6dfa-4c94-9fed-1db4633412a7");
      if (documentTypes.Count() == 1)
        _obj.DocumentKind = documentTypes.First();
    }
  }

}