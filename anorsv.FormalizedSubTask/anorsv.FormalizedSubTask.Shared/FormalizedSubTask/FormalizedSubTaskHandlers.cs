﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.FormalizedSubTask.FormalizedSubTask;

namespace anorsv.FormalizedSubTask
{
  partial class FormalizedSubTaskSharedHandlers
  {

    public override void MaxDeadlineChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      if (_obj.Performers.Count > 0)
      {
        foreach (var rowPerformer in _obj.Performers)
        {
          rowPerformer.Deadline = _obj.MaxDeadline;
        }
      }
    }
  }

  partial class FormalizedSubTaskPerformersSharedCollectionHandlers
  {

    public virtual void PerformersAdded(Sungero.Domain.Shared.CollectionPropertyAddedEventArgs e)
    {
      _added.Deadline = _obj.MaxDeadline;
    }
  }
}