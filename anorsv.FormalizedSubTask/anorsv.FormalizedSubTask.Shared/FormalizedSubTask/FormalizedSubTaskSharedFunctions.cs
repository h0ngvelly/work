using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Shared;
using Sungero.Docflow;
using anorsv.FormalizedSubTask.FormalizedSubTask;
using sline.CustomModule;

namespace anorsv.FormalizedSubTask.Shared
{
  partial class FormalizedSubTaskFunctions
  {
    /// <summary>
    /// Сохранить права инициатора задачи перед отбором.
    /// </summary>
    [Public]
    public virtual void SaveTaskAttachmentsAccessRights()
    {
      var documents = new List<IEntity>();
      var performers = new List<IRecipient>();
      
      documents.AddRange(_obj.DocumentGroup.All);
      documents.AddRange(_obj.AddendaGroup.All);
      documents.AddRange(_obj.MemoGroup.All);
      documents.AddRange(_obj.ContractGroup.All);
      documents.AddRange(_obj.TechnicalSpecificationGroup.All);
      documents.AddRange(_obj.OrderGroup.All);
      
      performers = _obj.Performers.Where(p => p.Perfomer != null).Select(p => p.Perfomer).ToList();
      
      foreach (var document in documents.Where(d => Sungero.Content.ElectronicDocuments.Is(d)))
      {
        foreach (IRecipient performer in performers)
        {
          Enumeration? rightsType = null;
          var rightsBefore = _obj.AttachmentsAccessRightsBefore.SingleOrDefault(r => r.DocumentId == document.Id && r.UserId == performer.Id);

          if (document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.FullAccess, performer))
            rightsType = anorsv.FormalizedSubTask.FormalizedSubTaskAttachmentsAccessRightsBefore.RightType.FullAccess;
          else if (document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Change, performer))
            rightsType = anorsv.FormalizedSubTask.FormalizedSubTaskAttachmentsAccessRightsBefore.RightType.Edit;
          else if (document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Read, performer))
            rightsType = anorsv.FormalizedSubTask.FormalizedSubTaskAttachmentsAccessRightsBefore.RightType.Read;
          
          if (rightsType != null)
          {
            if (rightsBefore != null &&
                (rightsBefore.RightType == rightsType ||
                 rightsBefore.RightType == anorsv.FormalizedSubTask.FormalizedSubTaskAttachmentsAccessRightsBefore.RightType.FullAccess
                 || rightsBefore.RightType == anorsv.FormalizedSubTask.FormalizedSubTaskAttachmentsAccessRightsBefore.RightType.Edit
                 || rightsBefore.RightType == anorsv.FormalizedSubTask.FormalizedSubTaskAttachmentsAccessRightsBefore.RightType.Read))
            {
              continue;
            }
          }
          
          if (rightsBefore == null)
          {
            rightsBefore = _obj.AttachmentsAccessRightsBefore.AddNew();
            rightsBefore.DocumentId = document.Id;
            rightsBefore.UserId = performer.Id;
            rightsBefore.RightType = rightsType;
          }
        }
      }
    }

    /// <summary>
    /// Установить права на вложения.
    /// </summary>
    public virtual void SetAccessRightsForAttachments()
    {
      Enumeration? rightType = _obj.RightType;
      Guid accessRightType = DefaultAccessRightsTypes.Read;
      List<IRecipient> performers = _obj.Performers.Where(p => p.Perfomer != null).Select(p => p.Perfomer).ToList();

      if (rightType != null)
      {
        if (rightType == anorsv.FormalizedSubTask.FormalizedSubTask.RightType.Edit)
          accessRightType = DefaultAccessRightsTypes.Change;
        else if (rightType == anorsv.FormalizedSubTask.FormalizedSubTask.RightType.FullAccess)
          accessRightType = DefaultAccessRightsTypes.FullAccess;
        
        foreach (IRecipient performer in performers)
        {
          Functions.FormalizedSubTask.SetAccessRightsForAttachments(_obj, performer, accessRightType);
        }
      }
    }
    
    /// <summary>
    /// Установить права на вложения.
    /// </summary>
    /// <param name="recipient">Получатель прав.</param>
    /// <param name="accessRightsType">Тип прав.</param>
    public virtual void SetAccessRightsForAttachments(IRecipient recipient, Guid accessRightsType)
    {
      var documents = new List<Sungero.Docflow.IOfficialDocument>();
      documents.AddRange(_obj.DocumentGroup.OfficialDocuments);
      documents.AddRange(_obj.AddendaGroup.OfficialDocuments);
      documents.AddRange(_obj.MemoGroup.Memos);
      documents.AddRange(_obj.ContractGroup.Contracts);
      documents.AddRange(_obj.TechnicalSpecificationGroup.TechnicalSpecifications);
      documents.AddRange(_obj.OrderGroup.Orders);
      
      foreach (var document in documents)
      {
        if (!document.AccessRights.IsGrantedDirectly(accessRightsType, recipient))
        {
          document.AccessRights.RevokeAll(recipient);
          document.AccessRights.Grant(recipient, accessRightsType);
        }
      }
      
      foreach(var document in _obj.OtherGroup.All.Where(d => Sungero.Content.ElectronicDocuments.Is(d)))
      {
        if (!(document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Read, recipient)
            || document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.Change, recipient)
            || document.AccessRights.IsGrantedDirectly(DefaultAccessRightsTypes.FullAccess, recipient)))
        {
          document.AccessRights.Grant(recipient, DefaultAccessRightsTypes.Read);
        }
      }
    }
    
    /// <summary>
    /// Восстановить права на вложения.
    /// </summary>
    public virtual void RestoreAccessRightsForAttachments()
    {
      var storedRights = _obj.AttachmentsAccessRightsBefore;
      List<int> documentIds = storedRights.Select(sr => sr.DocumentId.Value).Distinct().ToList();
      List<int> performerIds  = storedRights.Select(sr => sr.UserId.Value).Distinct().ToList();
      var performers = Recipients.GetAll(r => performerIds.Contains(r.Id));
      
      foreach (int documentId in documentIds)
      {
        Sungero.Docflow.IOfficialDocument document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentId).SingleOrDefault();
        
        if (document != null)
        {
          bool accessRightChanged = false;
          
          foreach (IRecipient performer in performers)
          {
            Guid rightType = DefaultAccessRightsTypes.Read;
            var storedRight = storedRights.FirstOrDefault(sr => sr.DocumentId == documentId && sr.UserId == performer.Id);

            if (storedRight.RightType == anorsv.FormalizedSubTask.FormalizedSubTask.RightType.Edit)
              rightType = DefaultAccessRightsTypes.Change;
            else if (storedRight.RightType == anorsv.FormalizedSubTask.FormalizedSubTask.RightType.FullAccess)
              rightType = DefaultAccessRightsTypes.FullAccess;
            
            if (!document.AccessRights.IsGrantedDirectly(rightType, performer))
            {
              document.AccessRights.RevokeAll(performer);
              document.AccessRights.Grant(performer, rightType);
              accessRightChanged = true;
            }
          }
          
          if (accessRightChanged)
          {
            document.AccessRights.Save();
          }
        }
      }
    }

    /// <summary>
    /// Восстановить права на одно вложение.
    /// </summary>
    [Public]
    public virtual void RestoreAccessRightsForAttachment(int documentId)
    {
      var storedRights = _obj.AttachmentsAccessRightsBefore;
      List<int> performerIds  = storedRights.Select(sr => sr.UserId.Value).Distinct().ToList();
      var performers = Recipients.GetAll(r => performerIds.Contains(r.Id));
      
      Sungero.Docflow.IOfficialDocument document = Sungero.Docflow.OfficialDocuments.GetAll(d => d.Id == documentId).SingleOrDefault();
      
      if (document != null)
      {
        bool accessRightChanged = false;
        
        foreach (IRecipient performer in performers)
        {
          Guid rightType = DefaultAccessRightsTypes.Read;
          var storedRight = storedRights.FirstOrDefault(sr => sr.DocumentId == documentId && sr.UserId == performer.Id);
          
          if (storedRight != null)
          {
            if (storedRight.RightType == anorsv.FormalizedSubTask.FormalizedSubTask.RightType.Edit)
              rightType = DefaultAccessRightsTypes.Change;
            else if (storedRight.RightType == anorsv.FormalizedSubTask.FormalizedSubTask.RightType.FullAccess)
              rightType = DefaultAccessRightsTypes.FullAccess;
          }
          if (!document.AccessRights.IsGrantedDirectly(rightType, performer))
          {
            document.AccessRights.RevokeAll(performer);
            document.AccessRights.Grant(performer, rightType);
            accessRightChanged = true;
          }
        }
        
        if (accessRightChanged)
          document.AccessRights.Save();
      }

      document = Sungero.Docflow.OfficialDocuments.Null;
      
    }

    
    /// <summary>
    /// Синхронизирует вложения из формализованной подзадачи в ведущую задачу
    /// </summary>
    [Public]
    public virtual void SyncAttachmentmetsFromSubtaskToParentTask()
    {
      if (anorsv.FormalizedSubTask.FormalizedSubTasks.Is(_obj))
      {
        var parentTask = _obj.ParentAssignment.Task;
        var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();

        if (Sungero.Docflow.ApprovalTasks.Is(parentTask))
        {
          var approvalTask = anorsv.ApprovalTask.ApprovalTasks.As(parentTask);
          anorsv.ApprovalTask.PublicFunctions.ApprovalTask.RefreshAttachmentsInternal(approvalTask);
          approvalTask.Save();
        }
        else if (anorsv.FormalizedSubTask.FormalizedSubTasks.Is(parentTask))
        {
          anorsv.FormalizedSubTask.IFormalizedSubTask formalizedSubTask = anorsv.FormalizedSubTask.FormalizedSubTasks.As(parentTask);
          anorsv.Approvals.PublicFunctions.Module.RefreshAttachmentsInternal(formalizedSubTask);
          formalizedSubTask.Save();
        }
        else if (Sungero.Docflow.FreeApprovalTasks.Is(parentTask))
        {
          Sungero.Docflow.IFreeApprovalTask freeApprovalTask = Sungero.Docflow.FreeApprovalTasks.As(parentTask);
          Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(freeApprovalTask.AddendaGroup, document);
          Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, freeApprovalTask.OtherGroup);
          freeApprovalTask.Save();
        }
      }
    }
    
    public virtual void ClearStoredAccessRightsForAttachments()
    {
      _obj.AttachmentsAccessRightsBefore.Clear();
    }
    
    /// <summary>
    /// Определяет обязательность указания срока для формализованной подзадаче, анализируя тип основного документа.
    /// </summary>
    /// <returns></returns>
    public bool DeadlineIsRequired()
    {
      var document = _obj.DocumentGroup.OfficialDocuments.SingleOrDefault();
      
      return  ApprovalTasks.Is(_obj.MainTask) && (document == null || (document != null && !PurchaseRequests.Is(document)));
    }
    
    /// <summary>
    /// Определить для задания подзадачи его порядковый номер.
    /// </summary>
    /// <param name="assignment">Задание для которого формируется тема с учётом её порядкового номера.</param>
    public virtual void SetFormalizedSubtaskJobNumber(Sungero.Workflow.IAssignmentBase assignment)
    {
      // Проверить наличие записи в справочнике параметров для задания-родителя
      // Если записи нет, задание было сформировано до переноса, формируем имя по-прежнему
      anorsv.TaskModule.ITaskExtraParam subtaskTaskExtraParam = anorsv.TaskModule.TaskExtraParams.GetAll()
        .Where(t => t.TaskId == _obj.Id)
        .FirstOrDefault();
      
      if (subtaskTaskExtraParam != null)
      {
        // Если запись есть, блокируем ее и увеличиваем счетчик заданий
        if (Locks.TryLock(subtaskTaskExtraParam))
        {
          // Формируем имя подзадачи по новому шаблону
          anorsv.TaskModule.PublicFunctions.Module.UpdateSubtaskJobSubject(assignment, subtaskTaskExtraParam);
        }
        else
        {
          // Если запись уже заблокирована - повторяем попытку в асинхр.обработчике
          var asyncUpdateSubtaskJobNumber = anorsv.TaskModule.AsyncHandlers.UpdatingSubtaskJobNumber.Create();
          asyncUpdateSubtaskJobNumber.JobId = assignment.Id;
          asyncUpdateSubtaskJobNumber.SubtaskParamId = subtaskTaskExtraParam.Id;
          asyncUpdateSubtaskJobNumber.ExecuteAsync();
        }
        
      }
    }
    
    /// <summary>
    /// Определить для подзадачи ее порядковый номер.
    /// </summary>
    public virtual void SetFormalizedSubtaskNumber()
    {
      // Проверить наличие записи в справочнике параметров для задания-родителя
      // Если записи нет, задание было сформировано до переноса, формируем имя по-прежнему
      anorsv.TaskModule.ITaskExtraParam parentTaskExtraParam = anorsv.TaskModule.TaskExtraParams.GetAll()
        .Where(t => t.TaskId == _obj.ParentAssignment.Task.Id)
        .Where(t => t.JobId == _obj.ParentAssignment.Id)
        .FirstOrDefault();
      
      if (parentTaskExtraParam != null)
      {
        // Если запись есть, блокируем ее и увеличиваем счетчик подзадач
        if (Locks.TryLock(parentTaskExtraParam))
        {
          // Формируем имя подзадачи по новому шаблону
          anorsv.TaskModule.PublicFunctions.Module.UpdateSubtaskSubject(_obj, parentTaskExtraParam);
        }
        else
        {
          // Если запись уже заблокирована - повторяем попытку в асинхр.обработчике
          var asyncUpdateSubtaskNumber = anorsv.TaskModule.AsyncHandlers.UpdatingSubtaskNumber.Create();
          asyncUpdateSubtaskNumber.TaskId = _obj.Id;
          asyncUpdateSubtaskNumber.ParentTaskParamId = parentTaskExtraParam.Id;
          asyncUpdateSubtaskNumber.ExecuteAsync();
        }
      }
    }
    
  }
}