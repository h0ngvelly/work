﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.FormalizedSubTask.Shared
{
  public class ModuleFunctions
  {
    /// <summary>
    /// Определяет вхождение пользователя в роль предназначенную для тестирования формализованной подзадачи.
    /// </summary>
    /// <param name="recipient">Пользователь которого надо проверить на вхождение в тестовую проль.</param>
    /// <returns></returns>
    [Public]
    public virtual bool UserInTestRole(IRecipient recipient)
    {
      bool result = false;
      IRole testRole  = Roles.GetAll(r => r.Sid == Constants.Module.Roles.FormalizedSubtaskTestRole.RoleGuid).FirstOrDefault();
      
      if (testRole != null)
      {
        result = recipient.IncludedIn(testRole);
      }
      
      return result;
    }
  }
}