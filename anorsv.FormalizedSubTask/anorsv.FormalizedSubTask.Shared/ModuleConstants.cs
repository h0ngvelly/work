﻿using System;
using Sungero.Core;

namespace anorsv.FormalizedSubTask.Constants
{
  public static class Module
  {
    [Public]
    public static class Roles
    {
      [Public]
      public static class FormalizedSubtaskTestRole
      {
        [Public]
        public const string RoleName = "АНОРСВ. Тестирующие формализованную подзадачу";
        
        [Public]
        public const string RoleDescription = "Работники, участвующие в тестировании формализованной подзадачи";
        
        [Public]
        public static readonly Guid RoleGuid = Guid.Parse("830E8C53-F5BD-4B09-AF9B-4E9F5549E8B9");
      }
    }
  }
}