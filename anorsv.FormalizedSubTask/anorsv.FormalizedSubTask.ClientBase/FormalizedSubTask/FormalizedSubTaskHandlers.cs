﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.FormalizedSubTask.FormalizedSubTask;
using Sungero.Company;

namespace anorsv.FormalizedSubTask
{
  partial class FormalizedSubTaskClientHandlers
  {

    public virtual IEnumerable<Enumeration> RightTypeFiltering(IEnumerable<Enumeration> query)
    {
      Sungero.Docflow.IOfficialDocument document = _obj.DocumentGroup.OfficialDocuments.SingleOrDefault();
      
      List<Enumeration> accessRightsTypes = new List<Enumeration> {anorsv.FormalizedSubTask.FormalizedSubTask.RightType.Read};
      
      if (document.AccessRights.IsGranted(DefaultAccessRightsTypes.FullAccess, _obj.ParentAssignment.Performer)
          || document.AccessRights.IsGranted(DefaultAccessRightsTypes.Change, _obj.ParentAssignment.Performer))
        accessRightsTypes.Add(anorsv.FormalizedSubTask.FormalizedSubTask.RightType.Edit);
      
      query = query.Where(rightType => accessRightsTypes.Contains(rightType));
      return query;
    }

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      base.Refresh(e);
      
      _obj.State.Properties.MaxDeadline.IsRequired = Functions.FormalizedSubTask.DeadlineIsRequired(_obj);
      _obj.State.IsEnabled = _obj.Status.Equals(FormalizedSubTask.Status.Draft);
      
      //видимость вложений в зависимости от типа документа "Заявка на закупку"
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var isPurchaseRequest = contractAttGroupAvailable && sline.CustomModule.PurchaseRequests.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.MemoGroup.IsVisible = isPurchaseRequest; 
      _obj.State.Attachments.ContractGroup.IsVisible = isPurchaseRequest; 
      _obj.State.Attachments.TechnicalSpecificationGroup.IsVisible = isPurchaseRequest; 

      //видимость вложений в зависимости от типа и вида документа "ЛНА"
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      var isLNA = orderAttGroupAvailable && anorsv.DocflowModule.LNAs.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.OrderGroup.IsVisible = false;
      if (isLNA == true)
      {
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid); 
        _obj.State.Attachments.OrderGroup.IsVisible = (lnaGenActivitiesKind != null && _obj.DocumentGroup.OfficialDocuments.FirstOrDefault().DocumentKind.Equals(lnaGenActivitiesKind));
      }
      
      //      /*Рахимова. Вывод сообщения о замещаемых согласующих*/
      var approvers = new List <Sungero.Company.IEmployee> ();
      approvers.Add(Sungero.Company.Employees.As(_obj.Performer));
      if(_obj.Performers.Any())
        approvers.AddRange(_obj.Performers.Select(a=>Sungero.Company.Employees.As(a.Perfomer)).ToList());
      
      var messageList = anorsv.ApprovalTask.PublicFunctions.ApprovalTask.SubstituteMessage(approvers, _obj.MaxDeadline);
      var substituteCount = messageList.Count;
      //message = message.Split('%') [1];
      
      if (messageList.Any())
      {
        if (substituteCount < 4)
          e.AddInformation(string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
        else
        {
          e.Params.AddOrUpdate("ShowSubstituteMessage", string.Join(Environment.NewLine, messageList.Select(m => m.Message)));
          e.AddInformation(anorsv.ApprovalTask.ApprovalTasks.Resources.ShowSubstitutionsByTaskPerformers, _obj.Info.Actions.ShowSubstitutedMessageanorsv);
        }
      }
    }

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      base.Showing(e);
      
      _obj.State.Properties.MaxDeadline.IsRequired = Functions.FormalizedSubTask.DeadlineIsRequired(_obj);
      _obj.State.IsEnabled = _obj.Status.Equals(FormalizedSubTask.Status.Draft);
      
      //видимость вложений в зависимости от типа документа "Заявка на закупку"
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var isPurchaseRequest = contractAttGroupAvailable && sline.CustomModule.PurchaseRequests.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.MemoGroup.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.ContractGroup.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.TechnicalSpecificationGroup.IsVisible = isPurchaseRequest;
      
      //видимость вложений в зависимости от типа и вида документа "ЛНА"
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      var isLNA = orderAttGroupAvailable && anorsv.DocflowModule.LNAs.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.OrderGroup.IsVisible = false;
      if (isLNA == true)
      {
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid); 
        _obj.State.Attachments.OrderGroup.IsVisible = (lnaGenActivitiesKind != null && _obj.DocumentGroup.OfficialDocuments.FirstOrDefault().DocumentKind.Equals(lnaGenActivitiesKind));
      }
    }
  }
}
