﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.FormalizedSubTask.FormalizedSubTask;

namespace anorsv.FormalizedSubTask.Client
{
  partial class FormalizedSubTaskActions
  {
    public override void Start(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.Start(e);
    }

    public override bool CanStart(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanStart(e);
    }

    public virtual void ShowSubstitutedMessageanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var message = "";
      e.Params.TryGetValue("ShowSubstituteMessage", out message);
      Dialogs.ShowMessage(message, MessageType.Warning);
    }

    public virtual bool CanShowSubstitutedMessageanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;      
    }

    public override void Cancel(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.Cancel(e);
    }

    public override bool CanCancel(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanCancel(e);
    }

    public virtual void CreateManyAddendums(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
      }
    }

    public virtual bool CanCreateManyAddendums(Sungero.Domain.Client.CanExecuteActionArgs e)
    {      
      return false;
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.CreateSubtask(e);
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return false;
    }

  }

}