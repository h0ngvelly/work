﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.FormalizedSubTask.FormalizedSubtaskAssignment;
using docRelation = anorsv.RelationModule;

namespace anorsv.FormalizedSubTask.Client
{
  partial class FormalizedSubtaskAssignmentActions
  {
    public virtual void ExtendDeadline(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var task = Sungero.Docflow.PublicFunctions.DeadlineExtensionTask.Remote.GetDeadlineExtension(_obj);
      task.Show();
    }

    public virtual bool CanExtendDeadline(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess &&
        _obj.AccessRights.CanUpdate() &&
        _obj.DocumentGroup.OfficialDocuments.Any();
    }

    public virtual void AddDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.AddDocumentLinkDialog(document, _obj.Id);
    }

    public virtual bool CanAddDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      //bool canModifyRelation = false;
      //if (_obj.DocumentGroup.OfficialDocuments.Any() && e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
      //  if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
      //    return canModifyRelation;
      bool canModifyRelation = (_obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess && 
                                _obj.AccessRights.CanUpdate() && 
                                _obj.DocumentGroup.OfficialDocuments.Any());
      
      return canModifyRelation;
    }

    public virtual void DeleteDocRelationanorsv(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.DeleteDocumentLinkDialog(document);
    }

    public virtual bool CanDeleteDocRelationanorsv(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
          return canModifyRelation;  
                
      return canModifyRelation;
    }

    public virtual void ChangeDocRelation(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
        docRelation.PublicFunctions.Module.ChangeDocumentLinkDialog(document);
    }

    public virtual bool CanChangeDocRelation(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      bool canModifyRelation = false;
      if (e.Params.Contains("AnoRsvCanModifyRelationsFromTask"))
        if (e.Params.TryGetValue("AnoRsvCanModifyRelationsFromTask", out canModifyRelation))
          return canModifyRelation;  
                
      return canModifyRelation;
    }

    public virtual void CreateManyAddendum(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        anorsv.OfficialDocumentModule.PublicFunctions.Module.AddManyAddendumDialog(document);
        Sungero.Docflow.PublicFunctions.Module.SynchronizeAddendaAndAttachmentsGroup(_obj.AddendaGroup, document);
        Sungero.Docflow.PublicFunctions.OfficialDocument.AddRelatedDocumentsToAttachmentGroup(document, _obj.OtherGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshContractGroup(document, _obj.ContractGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshTechnicalSpecificationGroup(document, _obj.TechnicalSpecificationGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshMemoGroup(document, _obj.MemoGroup);
        anorsv.TaskModule.PublicFunctions.Module.RefreshOrderGroup(document, _obj.OrderGroup);
        _obj.Save();
        PublicFunctions.FormalizedSubTask.Remote.SaveTaskAttachmentsAccessRightsFromClient(_obj.Task.Id);
      }
    }

    public virtual bool CanCreateManyAddendum(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      subtask.ShowModal();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

    public virtual void Complete(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }
    }

    public virtual bool CanComplete(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}