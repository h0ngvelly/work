﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment;

namespace anorsv.FormalizedSubTask.Client
{
  partial class FormalizedSubtaskControlAssignmentActions
  {
    public override void CreateSubtask(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      anorsv.FormalizedSubTask.IFormalizedSubTask subtask = anorsv.FormalizedSubTask.PublicFunctions.Module.Remote.CreateFormalizedSubTask(_obj);
      subtask.ShowModal();
    }

    public override bool CanCreateSubtask(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

    public virtual void ToRework(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));
      
      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      if (!success)
      {
        e.Cancel();
      }
    }

    public virtual bool CanToRework(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

    public virtual void Complete(Sungero.Workflow.Client.ExecuteResultActionArgs e)
    {
      //bool userInTestRole = anorsv.FormalizedSubTask.PublicFunctions.Module.UserInTestRole(Recipients.As(Users.Current));

      bool success = anorsv.TaskModule.PublicFunctions.Module.AssignmentComplete(_obj.Id);
      
      if (!success)
      {
        e.Cancel();
      }

      bool leadingAssignmentInProcess =
        anorsv.TaskModule.PublicFunctions.Module.Remote.GetAssignmentStatus(_obj.Task.ParentAssignment.Id) == Sungero.Workflow.Assignment.Status.InProcess;

      while (leadingAssignmentInProcess)
      {
        Guid choice = anorsv.TaskModule.PublicFunctions.Module.LeadingAssignmentDialog(_obj.Task.ParentAssignment);
        if (choice == anorsv.TaskModule.PublicConstants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.Open)
        {
          _obj.Task.ParentAssignment.ShowModal();
          leadingAssignmentInProcess =
            anorsv.TaskModule.PublicFunctions.Module.Remote.GetAssignmentStatus(_obj.Task.ParentAssignment.Id) == Sungero.Workflow.Assignment.Status.InProcess;
          if (!leadingAssignmentInProcess)
          {
            _obj.Complete(anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment.Result.Complete);
            break;
          }
        }
        else if (choice == anorsv.TaskModule.PublicConstants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.Accept)
        {
          _obj.Complete(anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment.Result.Complete);
          break;
        }
        else if (choice == anorsv.TaskModule.PublicConstants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.AcceptAndComplete)
        {
          var parentFormalizedSubtaskAssignment = anorsv.FormalizedSubTask.FormalizedSubtaskAssignments.As(_obj.Task.ParentAssignment);
          var parentSimpleAssignment = Sungero.Workflow.SimpleAssignments.As(_obj.Task.ParentAssignment);

          if (parentFormalizedSubtaskAssignment != null)
          {
            parentFormalizedSubtaskAssignment.Complete(anorsv.FormalizedSubTask.FormalizedSubtaskAssignment.Result.Complete);
            leadingAssignmentInProcess = false;
          }
          else if (parentSimpleAssignment != null)
          {
            Enumeration? completeResult = null;
            parentSimpleAssignment.Complete(completeResult);
            leadingAssignmentInProcess = false;
          }
          _obj.Complete(anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment.Result.Complete);
          
        }
        else if (choice == anorsv.TaskModule.PublicConstants.Module.LeadingAssignmentDialog.LeadingAssigmnentDialogResult.Cancel)
        {
          e.Cancel();
        }
      }
    }

    public virtual bool CanComplete(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
    {
      return _obj.Status == Sungero.Workflow.AssignmentBase.Status.InProcess
        && anorsv.TaskModule.PublicFunctions.Module.IsPerformerOrSubstitute(_obj, Users.As(Sungero.Company.Employees.Current));
    }

  }

}