using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.FormalizedSubTask.FormalizedSubtaskControlAssignment;

namespace anorsv.FormalizedSubTask
{
  partial class FormalizedSubtaskControlAssignmentClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      //видимость вложений в зависимости от типа документа "Заявка на закупку"
      bool contractAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("ContractAttGroupAvailable"));
      var isPurchaseRequest = contractAttGroupAvailable && sline.CustomModule.PurchaseRequests.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault());
      _obj.State.Attachments.MemoGroup.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.ContractGroup.IsVisible = isPurchaseRequest;
      _obj.State.Attachments.TechnicalSpecificationGroup.IsVisible = isPurchaseRequest;
      
      //видимость вложений в зависимости от вида документ "ЛНА по общ.деятельности"
      bool orderAttGroupAvailable = Convert.ToBoolean(Sungero.Docflow.PublicFunctions.Module.GetDocflowParamsValue("OrderAttGroupAvailable"));
      _obj.State.Attachments.OrderGroup.IsVisible = false;
      if (orderAttGroupAvailable && anorsv.DocflowModule.LNAs.Is(_obj.DocumentGroup.OfficialDocuments.FirstOrDefault()))
      {
        var lnaGenActivitiesKind = Sungero.Docflow.PublicFunctions.DocumentKind.GetNativeDocumentKind(anorsv.DocflowModule.PublicConstants.Module.DocKinds.LNAGeneralActivitiesKindGuid);
        if (lnaGenActivitiesKind != null && _obj.DocumentGroup.OfficialDocuments.FirstOrDefault().DocumentKind.Equals(lnaGenActivitiesKind))
          _obj.State.Attachments.OrderGroup.IsVisible = true;
      }
    }

    public override void Closing(Sungero.Presentation.FormClosingEventArgs e)
    {
      base.Closing(e);
      
      anorsv.ServiceLibrary.PublicFunctions.Module.MarkSubtaskExistInAssignmentSubject(_obj);
    }

  }
}
