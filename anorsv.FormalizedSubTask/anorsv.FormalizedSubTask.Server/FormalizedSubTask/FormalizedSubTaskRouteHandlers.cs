﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using anorsv.FormalizedSubTask.FormalizedSubTask;

namespace anorsv.FormalizedSubTask.Server
{
  partial class FormalizedSubTaskRouteHandlers
  {

    public virtual void StartAssignment6(anorsv.FormalizedSubTask.IFormalizedSubtaskControlAssignment assignment, anorsv.FormalizedSubTask.Server.FormalizedSubtaskControlAssignmentArguments e)
    {
      Functions.FormalizedSubTask.SetFormalizedSubtaskJobNumber(_obj, assignment);
    }

    public virtual void CompleteAssignment6(anorsv.FormalizedSubTask.IFormalizedSubtaskControlAssignment assignment, anorsv.FormalizedSubTask.Server.FormalizedSubtaskControlAssignmentArguments e)
    {
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public virtual void CompleteAssignment5(anorsv.FormalizedSubTask.IFormalizedSubtaskAssignment assignment, anorsv.FormalizedSubTask.Server.FormalizedSubtaskAssignmentArguments e)
    {
      anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignment.Id, true);
    }

    public virtual void StartAssignment5(anorsv.FormalizedSubTask.IFormalizedSubtaskAssignment assignment, anorsv.FormalizedSubTask.Server.FormalizedSubtaskAssignmentArguments e)
    {
      var performerRow = FormalizedSubTasks.As(assignment.Task).Performers.FirstOrDefault(p => p.Perfomer.Equals(assignment.Performer));
      assignment.Deadline = performerRow != null ? performerRow.Deadline : _obj.MaxDeadline;
      
      Functions.FormalizedSubTask.SetFormalizedSubtaskJobNumber(_obj, assignment);
    }

    public virtual void StartBlock6(anorsv.FormalizedSubTask.Server.FormalizedSubtaskControlAssignmentArguments e)
    {
      e.Block.Performers.Add(_obj.Author);
      e.Block.AbsoluteDeadline = _obj.MaxDeadline != null ? _obj.MaxDeadline.Value : Calendar.AddWorkingDays(Calendar.Now, 1);
      e.Block.NewDeadline = _obj.MaxDeadline != null ? _obj.MaxDeadline.Value : Calendar.AddWorkingDays(Calendar.Now, 1);
      
    }

    public virtual void Script8Execute()
    {
      Functions.FormalizedSubTask.SyncAttachmentmetsFromSubtaskToParentTask(_obj);
      Functions.FormalizedSubTask.RestoreAccessRightsForAttachments(_obj);
    }

    public virtual void Script4Execute()
    {
      anorsv.Approvals.PublicFunctions.Module.RefreshAttachmentsInternal(_obj);
      Functions.FormalizedSubTask.SaveTaskAttachmentsAccessRights(_obj);
      Functions.FormalizedSubTask.SetAccessRightsForAttachments(_obj);
      anorsv.TaskModule.PublicFunctions.Module.CreateTaskExtraParams(_obj);
      Functions.FormalizedSubTask.SetFormalizedSubtaskNumber(_obj);
      
      // Выдать наблюдателям права на просмотр.
      List<Guid> validAccessRights = new List<Guid> {DefaultAccessRightsTypes.Read, DefaultAccessRightsTypes.Change, DefaultAccessRightsTypes.FullAccess};
      var allAttachments = _obj.DocumentGroup.All
        .Concat(_obj.AddendaGroup.All)
        .Concat(_obj.MemoGroup.All)
        .Concat(_obj.OtherGroup.All)
        .Concat(_obj.ContractGroup.All)
        .Concat(_obj.TechnicalSpecificationGroup.All)
        .Concat(_obj.OrderGroup.All)
        .ToList();
      var observers = _obj.Observers.Select(o => o.Observer).ToList();
      foreach (var recipient in observers)
      {
        var attachmentsWithoutAccess = allAttachments
          .Where(d => !d.AccessRights.Current.Any(a => a.Recipient.Equals(recipient) && validAccessRights.Contains(a.AccessRightsType)))
          .ToList();
        
        if (attachmentsWithoutAccess.Count > 0)
        {
          Sungero.Docflow.PublicFunctions.Module
            .GrantReadAccessRightsForAttachments(
              attachmentsWithoutAccess
              , new List<IRecipient> {recipient}
             );
        }
      }
    }

    public virtual void StartBlock5(anorsv.FormalizedSubTask.Server.FormalizedSubtaskAssignmentArguments e)
    {
      e.Block.IsParallel = true;
      
      if (_obj.MaxDeadline.HasValue)
        e.Block.AbsoluteDeadline = _obj.MaxDeadline.Value;

      foreach (var performerRow in _obj.Performers)
      {
        var performers = Functions.Module.GetUsersFromGroups(performerRow.Perfomer);
        foreach (var groupRecipient in performers)
        {
          if (!e.Block.Performers.Contains(groupRecipient))
            e.Block.Performers.Add(groupRecipient);
        }
      }
      
    }
  }
}