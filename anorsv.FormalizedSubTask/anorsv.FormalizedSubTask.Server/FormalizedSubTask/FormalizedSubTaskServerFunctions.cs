﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Shared;
using anorsv.FormalizedSubTask.FormalizedSubTask;

namespace anorsv.FormalizedSubTask.Server
{
  partial class FormalizedSubTaskFunctions
  {
    [Public, Remote(IsPure = false)]
    public static void SaveTaskAttachmentsAccessRightsFromClient(int taskId)
    {
      Sungero.Core.AccessRights.AllowRead(
        () => {
          var task = FormalizedSubTasks.Get(taskId);
          Functions.FormalizedSubTask.SaveTaskAttachmentsAccessRights(task);
          Functions.FormalizedSubTask.SetAccessRightsForAttachments(task);
          task.Save();
        }
       );
    }
  }
}