﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Company;
using Sungero.Core;
using Sungero.CoreEntities;
using anorsv.FormalizedSubTask.FormalizedSubTask;

namespace anorsv.FormalizedSubTask
{
  partial class FormalizedSubTaskAuthorPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> AuthorFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      
      return query.Where(u => _obj.ParentAssignment != null && u.Equals(_obj.ParentAssignment.Performer) || u.Equals(Sungero.Company.Employees.Current));
    }
  }

  partial class FormalizedSubTaskPerformersPerfomerPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> PerformersPerfomerFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      query = query.Where(c => c.Status == Sungero.CoreEntities.DatabookEntry.Status.Active);
      
      // Отфильтровать всех пользователей.
      query = query.Where(x => x.Sid != Sungero.Domain.Shared.SystemRoleSid.AllUsers);
      
      // Отфильтровать служебные роли.
      return (IQueryable<T>)Sungero.RecordManagement.PublicFunctions.Module.ObserversFiltering(query);
    }
  }

  partial class FormalizedSubTaskServerHandlers
  {

    public override void Saving(Sungero.Domain.SavingEventArgs e)
    {
      // Почистим группы вложений от дублей
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.AddendaGroup);
      anorsv.TaskModule.PublicFunctions.Module.OptimizeAttachementDocumentGroup(_obj.OtherGroup);
    }

    public override void BeforeAbort(Sungero.Workflow.Server.BeforeAbortEventArgs e)
    {
      base.BeforeAbort(e);
      
      // Дополнительно прекратим все дочерние подзадачи
      foreach (int assignmentId in Sungero.Workflow.Assignments.GetAll(a => a.Task.Equals(_obj)).Select(a => a.Id))
        anorsv.TaskModule.PublicFunctions.Module.AssignmentSubtasksAcceptAndAbortInternal(assignmentId, true);

      anorsv.Approvals.PublicFunctions.Module.RefreshAttachmentsInternal(_obj);
      Functions.FormalizedSubTask.SaveTaskAttachmentsAccessRights(_obj);
      Functions.FormalizedSubTask.RestoreAccessRightsForAttachments(_obj);
    }

    public override void BeforeRestart(Sungero.Workflow.Server.BeforeRestartEventArgs e)
    {
      base.BeforeRestart(e);
      
      Functions.FormalizedSubTask.ClearStoredAccessRightsForAttachments(_obj);
      anorsv.Approvals.PublicFunctions.Module.RefreshAttachmentsInternal(_obj);
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      base.Created(e);
      
      _obj.NeedsReview = false;
    }
  }


  partial class FormalizedSubTaskObserversObserverPropertyFilteringServerHandler<T>
  {

    public override IQueryable<T> ObserversObserverFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      return (IQueryable<T>)Sungero.RecordManagement.PublicFunctions.Module.ObserversFiltering(query);
    }
  }

}