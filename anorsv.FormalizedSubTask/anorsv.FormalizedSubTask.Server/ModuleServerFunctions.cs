﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using sline.RSV;
using sline.RSV.ApprovalStage;
using docRelation = anorsv.RelationModule;

namespace anorsv.FormalizedSubTask.Server
{
  public class ModuleFunctions
  {
    
    #region Изменение вложения связанного документа в формализованных подзадачах
    
    /// <summary>
    /// Добавление связанного документа в область вложений в форм.подзадаче
    /// </summary>
    [Public]
    public static void AddRelatedDocInAttachments(anorsv.FormalizedSubTask.IFormalizedSubTask task, Sungero.Content.IElectronicDocument relatedDocument, int? assignmentId)
    {
      bool changeInTask = true;
      var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        // Если возможно, изменяем вложения напрямую в задании
        Sungero.Workflow.IAssignmentBase assignment = Sungero.Workflow.AssignmentBases.Null;
        if (assignmentId != null)
        {
          assignment = Sungero.Workflow.AssignmentBases.GetAll(a => a.Id == assignmentId).FirstOrDefault();
          if (assignment != null && assignment.Task.Id == task.Id)
          {
            changeInTask = false;
            // Для каждого типа задания отдельная фукнция, чтобы вложения сразу визуально отобразились
            if (!anorsv.FormalizedSubTask.FormalizedSubtaskAssignments.Is(assignment))
              anorsv.FormalizedSubTask.Functions.Module.AddRelatedDocInFormAssignment(anorsv.FormalizedSubTask.FormalizedSubtaskAssignments.As(assignment), relatedDocument);
            else
              changeInTask = true;
          }
        }
        
        if (changeInTask == true)
        {
          // Если не возможно изменить вложения напрямую в задании, меняем через задачу
          var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
          if (documentAddenda.Contains(relatedDocument))
          {
            // Если связанный документ - Приложение, вложить в область Приложения
            if (!task.AddendaGroup.All.Contains(relatedDocument))
              task.AddendaGroup.All.Add(relatedDocument);
          }
          else
          {
            // Если связанный документ - не Приложение, вложить в область Дополнительно
            if (!task.OtherGroup.All.Contains(relatedDocument))
              task.OtherGroup.All.Add(relatedDocument);
          }
          task.Save();
        }
      }
    }
    
    /// <summary>
    /// Добавление связанного документа в область вложений в задание Исполнения форм.подзадачи
    /// </summary>
    public static void AddRelatedDocInFormAssignment(anorsv.FormalizedSubTask.IFormalizedSubtaskAssignment assignment, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = assignment.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (documentAddenda.Contains(relatedDocument))
        {
          // Если связанный документ - Приложение, вложить в область Приложения
          if (!assignment.AddendaGroup.All.Contains(relatedDocument))
            assignment.AddendaGroup.All.Add(relatedDocument);
        }
        else
        {
          // Если связанный документ - не Приложение, вложить в область Дополнительно
          if (!assignment.OtherGroup.All.Contains(relatedDocument))
            assignment.OtherGroup.All.Add(relatedDocument);
        }
        assignment.Save();
      }
    }

    
    /// <summary>
    /// Удаление связанного документа из области вложений в форм.подзадаче
    /// </summary>
    [Public]
    public static void DeleteRelatedDocInAttachments(anorsv.FormalizedSubTask.IFormalizedSubTask task, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null)
      {
        // Вернуть исходные права доступа для исполнителя
        anorsv.FormalizedSubTask.PublicFunctions.FormalizedSubTask.RestoreAccessRightsForAttachment(task, relatedDocument.Id);
        
        if (task.AddendaGroup.All.Contains(relatedDocument))
        {
          // Удалить документ из области Приложения
          task.AddendaGroup.All.Remove(relatedDocument);
        }
        else if (task.OrderGroup.All.Contains(relatedDocument))
        {
          // Удалить документ из области Приказ
          task.OrderGroup.All.Remove(relatedDocument);
        }
        else if (task.ContractGroup.All.Contains(relatedDocument))
        {
          // Удалить документ из области Проект договора
          task.ContractGroup.All.Remove(relatedDocument);
        }
        else if (task.TechnicalSpecificationGroup.All.Contains(relatedDocument))
        {
          // Удалить документ из области Техническое задание
          task.TechnicalSpecificationGroup.All.Remove(relatedDocument);
        }
        else if (task.MemoGroup.All.Contains(relatedDocument))
        {
          // Удалить документ из области Служебная записка
          task.MemoGroup.All.Remove(relatedDocument);
        }
        else if (task.OtherGroup.All.Contains(relatedDocument))
        {
          // Удалить документ из области Дополнительно
          task.OtherGroup.All.Remove(relatedDocument);
        }
        
        task.Save();
      }
    }
    
    /// <summary>
    /// Изменение области вложения связанного документа в форм.подзадаче
    /// </summary>
    [Public]
    public static void ChangeRelatedDocInAttachments(anorsv.FormalizedSubTask.IFormalizedSubTask task, Sungero.Content.IElectronicDocument relatedDocument)
    {
      var document = task.DocumentGroup.OfficialDocuments.FirstOrDefault();
      if (document != null  && task.AllAttachments.Contains(relatedDocument))
      {
        var documentAddenda = document.Relations.GetRelated(Sungero.Docflow.PublicConstants.Module.AddendumRelationName);
        if (documentAddenda.Contains(relatedDocument))
        {
          // Если связанный документ - Приложение
          if (task.OtherGroup.All.Contains(relatedDocument))
          {
            // Удалить документ из области Дополнительно
            task.OtherGroup.All.Remove(relatedDocument);
          }
          else if (task.OrderGroup.All.Contains(relatedDocument))
          {
            // Удалить документ из области Приказ
            task.OrderGroup.All.Remove(relatedDocument);
          }
          else if (task.ContractGroup.All.Contains(relatedDocument))
          {
            // Удалить документ из области Проект договора
            task.ContractGroup.All.Remove(relatedDocument);
          }
          else if (task.TechnicalSpecificationGroup.All.Contains(relatedDocument))
          {
            // Удалить документ из области Техническое задание
            task.TechnicalSpecificationGroup.All.Remove(relatedDocument);
          }
          else if (task.MemoGroup.All.Contains(relatedDocument))
          {
            // Удалить документ из области Служебная записка
            task.MemoGroup.All.Remove(relatedDocument);
          }
          
          if (!task.AddendaGroup.All.Contains(relatedDocument))
          {
            // Вложить документ в область Приложения
            task.AddendaGroup.All.Add(relatedDocument);
          }
        }
        else
        {
          // Если связанный документ - не Приложение
          if (task.AddendaGroup.All.Contains(relatedDocument))
          {
            // Удалить документ из области Приложения
            task.AddendaGroup.All.Remove(relatedDocument);
          }
          if (!task.OtherGroup.All.Contains(relatedDocument))
          {
            // Вложить документ в область Дополнительно
            task.OtherGroup.All.Add(relatedDocument);
          }
        }
        
        task.Save();
      }
    }
    #endregion
    
    #region Создание формализованной подзадачи
    
    [Public, Remote(PackResultEntityEagerly = true)]
    public static IFormalizedSubTask CreateFormalizedSubTask(IAssignmentBase parentAssignment)
    {
      bool isApprovalTask = anorsv.ApprovalTask.ApprovalTasks.Is(parentAssignment.Task);
      bool isFormalizedSubTask = anorsv.FormalizedSubTask.FormalizedSubTasks.Is(parentAssignment.Task);
      bool isFreeApprovalTask = Sungero.Docflow.FreeApprovalTasks.Is(parentAssignment.Task);
      bool isAcquaintanceTask = Sungero.RecordManagement.AcquaintanceTasks.Is(parentAssignment.Task);
      
      anorsv.ApprovalTask.IApprovalTask approvalTask = anorsv.ApprovalTask.ApprovalTasks.Null;
      anorsv.FormalizedSubTask.IFormalizedSubTask formalizedTask = anorsv.FormalizedSubTask.FormalizedSubTasks.Null;
      Sungero.Docflow.IFreeApprovalTask freeApprovalTask = Sungero.Docflow.FreeApprovalTasks.Null;
      Sungero.RecordManagement.IAcquaintanceTask acquaintanceTask = Sungero.RecordManagement.AcquaintanceTasks.Null;

      if (!(isApprovalTask || isFormalizedSubTask || isFreeApprovalTask || isAcquaintanceTask))
        throw AppliedCodeException.Create(Resources.CreateSubtaskForAllowedTasksOnly);
      
      // Создать запись для доп.параметров
      anorsv.TaskModule.PublicFunctions.Module.CreateTaskExtraParams(parentAssignment);
      
      IFormalizedSubTask createdTask = FormalizedSubTasks.CreateAsSubtask(parentAssignment);
      createdTask.MaxDeadline = parentAssignment.Deadline;
      createdTask.Author = parentAssignment.Performer;

      // Изменить тему созданной подзадачи, автономер будет изменен после старта на порядковый номер из справочника
      var formalizedSubTaskAuthor = anorsv.AnorsvMainSolution.Module.Docflow.PublicFunctions.Module.GeUserNameInGenitive(parentAssignment.Performer.Name);
      createdTask.Subject = Resources.FormalizedSubtaskDefaultSubjectTemplateFormat(formalizedSubTaskAuthor);
      
      if (isApprovalTask)
      {
        approvalTask = anorsv.ApprovalTask.ApprovalTasks.As(parentAssignment.Task);
        
        createdTask.DocumentGroup.OfficialDocuments.Add(approvalTask.DocumentGroup.OfficialDocuments.Single());

        foreach (Sungero.Domain.Shared.IEntity attachment in approvalTask.AddendaGroup.All)
          createdTask.AddendaGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in approvalTask.OrderGroupanorsv.All)
          createdTask.OrderGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in approvalTask.MemoGroupanorsv.All)
          createdTask.MemoGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in approvalTask.ContractGroupanorsv.All)
          createdTask.ContractGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in approvalTask.TechnicalSpecificationGroupanorsv.All)
          createdTask.TechnicalSpecificationGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in approvalTask.OtherGroup.All)
          createdTask.OtherGroup.All.Add(attachment);
      }
      else if (isFormalizedSubTask)
      {
        formalizedTask = anorsv.FormalizedSubTask.FormalizedSubTasks.As(parentAssignment.Task);
        
        createdTask.DocumentGroup.OfficialDocuments.Add(formalizedTask.DocumentGroup.OfficialDocuments.Single());

        foreach (Sungero.Domain.Shared.IEntity attachment in formalizedTask.AddendaGroup.All)
          createdTask.AddendaGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in formalizedTask.OrderGroup.All)
          createdTask.OrderGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in formalizedTask.MemoGroup.All)
          createdTask.MemoGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in formalizedTask.ContractGroup.All)
          createdTask.ContractGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in formalizedTask.TechnicalSpecificationGroup.All)
          createdTask.TechnicalSpecificationGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in formalizedTask.OtherGroup.All)
          createdTask.OtherGroup.All.Add(attachment);
      }
      else if (isFreeApprovalTask)
      {
        freeApprovalTask = Sungero.Docflow.FreeApprovalTasks.As(parentAssignment.Task);
        
        createdTask.DocumentGroup.OfficialDocuments.Add(Sungero.Docflow.OfficialDocuments.As(freeApprovalTask.ForApprovalGroup.ElectronicDocuments.Single()));

        foreach (Sungero.Domain.Shared.IEntity attachment in freeApprovalTask.AddendaGroup.All)
          createdTask.AddendaGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in freeApprovalTask.OtherGroup.All)
          createdTask.OtherGroup.All.Add(attachment);
      }
      else {
        acquaintanceTask = Sungero.RecordManagement.AcquaintanceTasks.As(parentAssignment.Task);
        
        createdTask.DocumentGroup.OfficialDocuments.Add(acquaintanceTask.DocumentGroup.OfficialDocuments.Single());

        foreach (Sungero.Domain.Shared.IEntity attachment in acquaintanceTask.AddendaGroup.All)
          createdTask.AddendaGroup.All.Add(attachment);
        
        foreach (Sungero.Domain.Shared.IEntity attachment in acquaintanceTask.OtherGroup.All)
          createdTask.OtherGroup.All.Add(attachment);
        
      }
      
      // Заполним выдаваемые права максимально возможными
      createdTask.RightType = GetMaximumPossibleAccessRight(createdTask);
      
      return createdTask;
    }
    
    [Public, Remote(PackResultEntityEagerly = true)]
    public static IFormalizedSubTask CreateFormalizedSubTask(Sungero.Workflow.IAssignmentBase parentAssignment, IApprovalStage stage)
    {
      IFormalizedSubTask createdTask = CreateFormalizedSubTask(parentAssignment);
      IUser initiator = parentAssignment.Performer;
      
      if (stage.SubtaskInitiatorByDefaultanorsv == SubtaskInitiatorByDefaultanorsv.CurrentUser)
      {
        initiator = Users.As(Sungero.Company.Employees.Current);
      }
      createdTask.Author = initiator;
      
      return createdTask;
    }
    
    #endregion

    [Public]
    /// <summary>
    /// Раскрывание групп в пользователей с дублями.
    /// </summary>
    /// <param name="recipient">Реципиент.</param>
    /// <returns>Список реципиентов.</returns>
    public static List<IRecipient> GetUsersFromGroups(IRecipient recipient)
    {
      var sourceGroup = Groups.As(recipient);
      var recipientList = new List<IRecipient>();
      
      if (sourceGroup == null)
      {
        recipientList.Add(recipient);
      }
      else
      {
        var users = Roles.GetAllUsersInGroup(sourceGroup).Where(x => x.Status == Sungero.CoreEntities.DatabookEntry.Status.Active && x.Login != null && x.IsSystem != true);
        foreach (var user in users)
          recipientList.Add(user);
      }
      
      return recipientList;
    }
    
    public static Enumeration GetMaximumPossibleAccessRight(IFormalizedSubTask task)
    {
      Sungero.Docflow.IOfficialDocument document = task.DocumentGroup.OfficialDocuments.SingleOrDefault();
      
      Enumeration accessRight = anorsv.FormalizedSubTask.FormalizedSubTask.RightType.Read;
      
      if (document.AccessRights.IsGranted(DefaultAccessRightsTypes.FullAccess, task.ParentAssignment.Performer)
          || document.AccessRights.IsGranted(DefaultAccessRightsTypes.Change, task.ParentAssignment.Performer))
        accessRight = anorsv.FormalizedSubTask.FormalizedSubTask.RightType.Edit;
      
      return accessRight;
    }
    
  }
}
