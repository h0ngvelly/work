﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace anorsv.FormalizedSubTask.Server
{
  partial class FormalizedSubTaskAssignmentFolderHandlers
  {

    public virtual bool IsFormalizedSubTaskAssignmentVisible()
    {
      return true;
    }

    public virtual IQueryable<Sungero.Workflow.IAssignmentBase> FormalizedSubTaskAssignmentDataQuery(IQueryable<Sungero.Workflow.IAssignmentBase> query)
    {
      // Фильтр по типу.
      var result = query
        .Where(a => anorsv.FormalizedSubTask.FormalizedSubtaskAssignments.Is(a));
      
      // Запрос непрочитанных без фильтра.
      if (_filter == null)
        return Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result);
      
      // Фильтры по статусу, замещению и периоду.
      result = Sungero.RecordManagement.PublicFunctions.Module.ApplyCommonSubfolderFilters(result, _filter.InProcess, _filter.Last30Days, _filter.Last90Days, _filter.Last180Days, false);
      
      return result;
    }
  }

  partial class FormalizedSubTaskHandlers
  {
  }
}