﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;
using anorsv.FormalizedSubTask.Constants;

namespace anorsv.FormalizedSubTask.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      SynchronizePeformersCollection();
      CreateRoles();
    }

    /// <summary>
    /// Для задач запущенных до появления множественных исполнителей заполняет коллекцию исполнителей.
    /// </summary>
    public void SynchronizePeformersCollection()
    {
      var needSynchronizeTasks =  FormalizedSubTasks.GetAll(t => t.Performer != null && t.Performers.Count() == 0);
      int needSynchronizeTasksCount = needSynchronizeTasks.Count();
      int synchronizedTasksCount = 0;
      int previousStepCount = 0;

      InitializationLogger.DebugFormat("Init: Update tasks. Total tasks count = {0}.", needSynchronizeTasksCount);
      
      foreach (IFormalizedSubTask task in needSynchronizeTasks)
      {
        var perfomerRow = task.Performers.AddNew();
        perfomerRow.Perfomer = task.Performer;
        perfomerRow.Deadline = task.MaxDeadline;
        
        foreach (var accessRow in task.AttachmentsAccessRightsBefore)
        {
          accessRow.UserId = task.Performer.Id;
        }
        
        task.Save();
        
        if (synchronizedTasksCount - previousStepCount == 100)
        {
          previousStepCount = synchronizedTasksCount;
          InitializationLogger.DebugFormat("Successfully updated {0} of {1} tasks.", synchronizedTasksCount, needSynchronizeTasksCount);
        }
      }
      
      if (needSynchronizeTasksCount > 0 && previousStepCount < synchronizedTasksCount)
      {
          InitializationLogger.DebugFormat("Successfully updated {0} of {1} tasks.", synchronizedTasksCount, needSynchronizeTasksCount);
      }
    }
    
    public void CreateRoles()
    {
      var role = Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole(
        Constants.Module.Roles.FormalizedSubtaskTestRole.RoleName
        , Constants.Module.Roles.FormalizedSubtaskTestRole.RoleDescription
        , Constants.Module.Roles.FormalizedSubtaskTestRole.RoleGuid
       );
      
      role.IsSystem = true;
      role.Save();
    }
    
  }
}
